#!/bin/sh
echo "Installing tinymce...";

cd build/src/core/App/public/ || exit 1;

arch_name=tinymce_6.1.2.zip;
wget https://download.tiny.cloud/tinymce/community/$arch_name;
unzip -o $arch_name;
rm $arch_name;

mv tinymce/js/tinymce/* tinymce/;

rm -r tinymce/js;

echo "tinymce has been successfully installed";
