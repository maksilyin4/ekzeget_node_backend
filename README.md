# ekzeget-backend

## Новый процесс деплоя. Все предыдущие инструкции теперь deprecated.

* Создать и заполнить `.env` и `prod.conf.json` файлы (пример в `ecosystem.config.js` в комменте). TODO добавить `.env.example`.
* `cp oauth2-strategies.example.json oauth2-strategies.json` - создать `oauth2-strategies.json`
* После этого запускаем приложение:

```shell
docker compose -f compose.prod.yml up mysql -d  # запуск контейнера mariadb
# на этом этапе нужно импортировать дамп бд. инструкции по менеджменту бд ниже.
docker compose -f compose.prod.yml up --build -d  # поднимает контейнер с миграциями и по завершении запускает приложение
docker compose -f compose.prod.yml logs app --tail 100 --follow  # просмотр логов приложения
docker compose -f compose.prod.yml logs mysql --tail 100 --follow  # просмотр логов бд
```

## Data management

### Доступ к консоли mariadb

В переменных окружения контейнера бд есть все необходимые данные, так что работает универсальная команда:

```shell
docker compose -f compose.prod.yml exec mysql /bin/sh -c 'mariadb -u "$MYSQL_USER" -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE"'
```

### Data import

```shell
docker compose -f compose.prod.yml exec -T mysql mariadb -u your_user_here -p'your_password_here' db_name_here < dump.sql
```

Импорт дампа может занимать очень долгое время так что имеет смысл запускать его в бекграунде. Для этого вышеупомянутую команду нужно обернуть в исполняемый import_db.sh:

```shell
#!/bin/bash
docker compose -f compose.prod.yml exec -T mysql mariadb -u your_user_here -p'your_password_here' db_name_here < dump.sql
```

После этого запускаем скрипт в бекграунде:

```shell
chmod +x import_db.sh  # делаем его исполняемым
nohup ./import_db.sh > import_log.txt 2>&1 &  # стартуем процесс в бекграунде
```

### Data export

```shell
docker compose -f compose.prod.yml exec -T mysql mariadb-dump -u your_user_here -p'your_password_here' db_name_here > dump.sql
```

# Документация ниже - легаси. Будет изменено/удалено в ближайшем будущем в пользу актуальной документации.

## Ссылки
* https://ekzeget.ru - production
* https://test.ekzeget.ru - test (deprecated)
- - - -


## Запуск проекта на локальной машине:
* Установить **docker** (если еще не установлен)
* Создать и заполнить `.env` файл (пример в `ecosystem.config.js` в комменте)
* `docker-compose up --build` - запуск контейнера с построением проекта

  (либо `docker-compose up` - если с прошлого запуска зависимости не менялись)
* Подключиться к базе и сделать `restore dump` для базы `ekzeget`

  (файл dump-а надо скачать с FTP, логин+пароль по запросу)
- - - -
## Восстановление БД из консоли для DevOps:
```bash
1. sudo apt install mariadb-client-core-10.3 pv -y
2. kubectl port-forward mariadb-ekzeget-env-0 3306:3306
3. pv ekzeget_prod_data-count.sql.gz | zcat | mysql -h 127.0.0.1 -P 3306 -u root -p ekzeget
```
- - - -
## Возможные ошибки при начале работы:

1. *(Ubuntu)* Ошибка инициализации mysql:
```
[ERROR] [MY-011096] [Server] No data dictionary version number found.
[ERROR] [MY-010020] [Server] Data Dictionary initialization failed.
[ERROR] [MY-010119] [Server] Aborting
```
Помогло изменение файла `docker-compose.yml`:
```yaml
  mysql:
    ...
    volumes:
      - db_data:/var/lib/mysql

  volumes:
    db_data:
```
на
```yaml
  mysql:
    ...
    volumes:
      - /var/lib/mysql/data:/var/lib/mysql
```
- - - -
2. Ошибка установки соединения к поднятому контейнеру MySQL:

При подключении к базе сторонними инструментами убедитесь что в настройках соединения поменяли порт с `3306` на `3006`
- - - -
3. Не удается сделать `restore dump`? Попробуйте **DataGrip**.
```bash
snap install datagrip --classic
```
- - - -
4. При старте бэкенда возникает ошибка соединения: `ER_NOT_SUPPORTED_AUTH_MODE`

Подключитесь к успешно поднятому контейнеру MySQL (команда `mysql --protocol=tcp -u root --password=secret` пароль для root берется из переменной окружения `MYSQL_ROOT_PASSWORD`) под пользователем `root`
и выполните следующие запросы (имя пользователя **ekzeget** взялось из переменной окружения `MYSQL_USER` и его пароль из `MYSQL_PASSWORD`):
```sql
ALTER USER 'ekzeget'@'%' IDENTIFIED WITH mysql_native_password BY 'XFqr7nu2aM';
FLUSH PRIVILEGES;
GRANT ALL PRIVILEGES ON ekzeget.* TO 'ekzeget'@'%';
```
После остановите все контейнеры и заново выполните `docker-compose up`

## Запуск проекта на [https://ekzeget.ru/](https://ekzeget.ru/):

>Установка с нуля:
```bash
# Клонируем код из репозитория и переходим в папку
git clone <репозиторий> ekzeget_node_backend/ # prod

# Переходим в папку, переключаемся на нужную ветку
# Название ветки варьируется в зависимости от даты релиза
git checkout release-xx-yy
chmod 755 build.sh # меняем права на build.sh
npm install # установка зависимостей
npm run build:prod # Построение проекта
npm run start:prod # Запуск проекта

# Далее (если это еще не делалось) надо накатить миграции для базы
# и 1 раз вызвать (можно даже через браузер)
# https://ekzeget.ru/api/v1/dev/updateArticleImageUrls
# для обновления ссылок на картинки в базе
```

>Обновление проекта на [https://ekzeget.ru/](https://ekzeget.ru/):
```bash
# (Находясь в папке проекта)
git fetch && git pull # Подтягиваем изменения (требуется логин и пароль от репозитория)
pm2 list # получаем список процессов в pm2
pm2 delete ID # Название процесса, где ID = id процесса ekzeget_backend
# После этой команды backend на проде перестает работать

rm -rf build # удаляем старый билд
rm -rf node_modules # удаляем старую папку модулей
npm install # установка зависимостей
npm run build:prod # делаем новый билд
npm run start:prod # запускаем процесс
pm2 save # для сохранения настроек автозапуска
```

```bash
# Для мониторинга процессов можно использовать либо
pm2 list # список процессов
pm2 monit # список процессов с детальной информацией и логами по ним
pm2 logs ekzeget_backend --err # отображать ошибки в реальном времени
```

P.S. Файлы логов лежат в `~/.pm2/logs/`
