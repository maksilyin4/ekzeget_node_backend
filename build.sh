#!/bin/sh
rm -rf build;
tsc;
cp ./*.conf.json build;
cp ecosystem.config.js build;
cp oauth2-strategies.json build;
cp -R src/core/App/public build/src/core/App/;
echo Done;
