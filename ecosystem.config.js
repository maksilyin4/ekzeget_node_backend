const env = process.argv[process.argv.indexOf('--env') + 1];
const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });

const APIApp = {
    script: 'build/src/index.js',
    name: 'ekzeget_backend',
    log_date_format: 'DD-MM-YYYY HH:mm:ss',
    max_memory_restart: '3G',
    instances: 2,
    ['env_' + env]: process.env,
};

const adminApp = {
    script: 'build/src/admin.js',
    name: 'ekzeget_admin',
    log_date_format: 'DD-MM-YYYY HH:mm:ss',
    max_memory_restart: '3G',
    ['env_' + env]: process.env,
};

if (env !== 'prod') {
    const devRunnerConfig = {
        script: './node_modules/.bin/ts-node',
        watch: true,
        instances: undefined,
        ignore_watch: ['.idea', '.adminbro', 'node_modules', '.git'],
    };
    Object.assign(APIApp, devRunnerConfig, {
        args: 'src/index.ts',
        node_args: ['--inspect=9229'],
    });

    Object.assign(adminApp, devRunnerConfig, {
        args: 'src/admin.ts',
        node_args: ['--inspect=9230'],
    });
}

module.exports = {
    apps: [APIApp, adminApp],
};

/*
example:

{
  "ENV": "production",
  "PORT": 4000,
  "ADMIN_PORT": 4001,
  "HOST": "localhost",
  "REST_HOST": "test1.ekzeget.ru",
  "PROTOCOL": "https",
  "API_PREFIX": "/api",
  "API_VERSION": "/v1",
  "GRAPHQL_API_VERSION": "/v2",
  "MYSQL_HOST": "localhost",
  "MYSQL_PORT": 3306,
  "MYSQL_USER": "ekzeget",
  "MYSQL_PASSWORD": "XFqr7nu2aM",
  "MYSQL_DATABASE": "ekzeget_prod",
  "MYSQL_ROOT_PASSWORD": "secret",
  "SESSION_SECRET": "6f5d37a0-37d3-40c9-9596-d78f5e909658",
  "SMTP_HOST": "smtp.gmail.com",
  "SMTP_PORT": 587,
  "SMTP_SOURCE_MAIL": "noreply@example.com",
  "SMTP_ADMIN_MAIL": "user@example.com",
  "SMTP_LOGIN": "info@ekzeget.ru",
  "SMTP_PASS": "Ekzeget_cool",
  "CHECK_CAPTCHA": 1,
  "STORAGE_HOST": "ftp.selcdn.ru",
  "STORAGE_FTP_USER": "73025_Honor",
  "STORAGE_FTP_PASS": "&KlT1anf2e",
  "STORAGE_ASSETS_DIR": "/files",
  "STORAGE_MEDIA_DIR": "/uploads",
  "STORAGE_MEDIA_CDN": "http://f0397b1f-ce92-46d4-8f89-86c525f77ab1.selcdn.net",
  "STORAGE_ASSETS_CDN": "https://251516.selcdn.ru",
  "OLD_BIBLE_CDN": "https://251516.selcdn.ru/mp3/",
  "MEDIA_CDN_UPLOADS_URL": "https://f0397b1f-ce92-46d4-8f89-86c525f77ab1.selcdn.net",
  "MEDIA_CDN_UPLOADS_DIR": "/uploads",
  "MEDIA_CDN_MP3_URL": "https://9c7f4e2b-86a9-4d73-b1db-6452d1399a9d.selcdn.net",
  "MEDIA_CDN_MP3_DIR": "/mp3",
  "MEDIA_CDN_MEDIA_URL": "https://dd838d92-2855-4037-b79c-8557991aa4fa.selcdn.net",
  "MEDIA_CDN_MEDIA_DIR": "/media",
  "OAUTH_GOOGLE_CLIENT_ID": "724062350074-m74ta8frctngedq2u52reakr42l5u5sa.apps.googleusercontent.com",
  "OAUTH_GOOGLE_SECRET": "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"}

 */
