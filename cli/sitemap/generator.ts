import Book from '../../src/entities/Book';
import Interpretation from '../../src/entities/Interpretation';
import DictionaryType from '../../src/entities/DictionaryType';
import EkzegetPerson from '../../src/entities/EkzegetPerson';
import LectureType from '../../src/entities/LectureType';
import Media from '../../src/entities/Media';
import Article from '../../src/entities/Article';
import QueezeQuestions from '../../src/entities/QueezeQuestions';
import CelebrationExcuse from '../../src/entities/CelebrationExcuse';
import { generateSlug } from '../../src/libs/Rus2Translit/generateSlug';
import BibleGroupNote from '../../src/entities/BibleGroupNote';
import BibleGroup from '../../src/entities/BibleGroup';
import fs from 'fs';
import * as XmlBuilder from 'xmlbuilder';
import { DateTime } from 'luxon';

const DEFAULT_DATE = DateTime.fromISO('2020-01-01T00:00:00.000+03:00');

export default async function generate(folder: string, baseUrl: string) {
    console.log('Start generation');

    if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder, { recursive: true });
    }

    const mainFile = XmlBuilder.create('sitemapindex');
    mainFile.attribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

    for (const [generatorName, generator] of Object.entries(generators)) {
        console.log('Generating ' + generatorName);

        const links = await generator();

        const sectionMap = XmlBuilder.create('urlset');
        sectionMap.attribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

        for (const link of links) {
            const locEl = sectionMap.ele('url').ele('loc', baseUrl + link.url);
            if (link.lastmod) {
                locEl.up().ele('lastmod', link.lastmod);
            }
            if (link.changefreq) {
                locEl.up().ele('changefreq', link.changefreq);
            }
            if (link.priority) {
                locEl.up().ele('priority', link.priority);
            }
        }

        saveFile(`${folder}/${generatorName}.xml`, sectionMap.end({ pretty: true }));
        addLinkTomMainFile(`/${generatorName}.xml`);
    }

    saveFile(`${folder}/sitemap.xml`, mainFile.end({ pretty: true }));

    function addLinkTomMainFile(url: string) {
        mainFile
            .ele('sitemap')
            .ele('loc', `${baseUrl}${url}`)
            .up()
            .ele('lastmod', DateTime.local().setZone('Europe/Moscow').toISODate())
            .up();
    }
}

function saveFile(fileName: string, content: string) {
    fs.writeFile(fileName, content, function (err) {
        if (err) {
            return console.error(err);
        }
        console.log(fileName + ' saved');
    });
}

const generators: { [key: string]: () => Promise<Array<{ url: string; lastmod?: string; changefreq?: string; priority?: string }>> } = {
    async Bible() {
        const books = await Book.find();
        const urls = [];

        for (const book of books) {
            for (let i = 0; i < book.parts; i++) {
                urls.push({
                    url: `/bible/${book.code}/glava-${i + 1}/`,
                });
            }
        }

        return urls;
    },

    async interpretations() {
        const comments_by_chapter_and_exegete = (await Interpretation.createQueryBuilder('i')
            .select([
                'GREATEST(MAX(i.added_at), MAX(i.edited_at)) AS lastmod',
                'b.code AS book_code',
                'chapter',
                'ekzeget_person.code AS exegete_code',
            ])
            .innerJoin('interpretation_verse', 'iv', 'iv.interpretation_id = i.id')
            .innerJoin('verse', 'v', 'iv.verse_id = v.id')
            .innerJoin('book', 'b', 'v.book_id = b.id')
            .innerJoin('ekzeget_person', 'ekzeget_person', 'ekzeget_person.id = i.ekzeget_id')
            .where('i.active = TRUE AND ekzeget_person.active = TRUE')
            .groupBy('ekzeget_person.code')
            .addGroupBy('v.book_id')
            .addGroupBy('v.chapter')
            .getRawMany()) as {
                lastmod: number;
                book_code: string;
                chapter: number;
                exegete_code: string;
            }[];

        const urls = [];

        for (const comment of comments_by_chapter_and_exegete) {
            urls.push({
                url: `/bible/${comment.book_code}/glava-${comment.chapter}/tolkovatel-${comment.exegete_code}`,
                lastmod: DateTime.fromSeconds(
                    comment.lastmod || DEFAULT_DATE.toSeconds()
                ).toISODate(),
                changefreq: 'weekly',
                priority: '0.5',
            });
        }
        return urls;
    },

    async dictionary() {
        const dictionaryTypes = await DictionaryType.find({
            where: { active: 1 },
            relations: ['dictionaries'],
        });

        const urls = [];

        for (const dictionaryType of dictionaryTypes) {
            for (const dictionaryArticle of dictionaryType.dictionaries) {
                if (dictionaryArticle.id == 10522) {
                    console.log(dictionaryArticle.code);
                }
                urls.push({
                    url: `/all-about-bible/dictionaries/${dictionaryType.code}/${dictionaryArticle.code}/`,
                    lastmod: DateTime.local().setZone('Europe/Moscow').toISODate(),
                    changefreq: 'weekly',
                    priority: '0.5',
                });
            }
        }

        return urls;
    },

    async exegetes() {
        const exegetes = await EkzegetPerson.createQueryBuilder('')
            .select('*')
            .getRawMany();

        const urls = [];

        for (const exegete of exegetes) {
            urls.push({
                url: `/all-about-bible/ekzegets/${exegete.code}/`,
                lastmod: DateTime.local().setZone('Europe/Moscow').toISODate(),
                changefreq: 'weekly',
                priority: '0.5',
            });
        }
        return urls;
    },

    async lectures() {
        const lectureTypes = await LectureType.find({
            where: { active: 1 },
            relations: ['lectures'],
        });

        const urls = [];

        for (const lectureType of lectureTypes) {
            for (const lecture of lectureType.lectures) {
                urls.push({
                    url: `/all-about-bible/about-bible/${lectureType.code}/${lecture.code}/`,
                    lastmod: DateTime.fromSeconds(
                        lecture.edited_at || lecture.added_at || DEFAULT_DATE.toSeconds()
                    ).toISODate(),
                    changefreq: 'weekly',
                    priority: '0.5',
                });
            }
        }
        return urls;
    },

    async medialib() {
        const media = await Media.find({
            join: {
                alias: 'media',
                innerJoinAndSelect: { mediaCategories: 'media.mediaCategories' },
            },
            where: { active: 1 },
        });

        const urls = [];

        for (const mediaItem of media) {
            urls.push({
                url:
                    mediaItem.mediaCategories[0].code === 'motivatory'
                        ? `/mediateka/motivatory/?motivator=${mediaItem.id}`
                        : `/mediateka/detail-${mediaItem.code}/`,
                lastmod: DateTime.fromSeconds(
                    mediaItem.edited_at || mediaItem.created_at || DEFAULT_DATE.toSeconds()
                ).toISODate(),
                changefreq: 'weekly',
                priority: '0.5',
            });
        }
        return urls;
    },

    async miscellaneous() {
        return [
            {
                url: '/about-us/',
                lastmod: DateTime.fromSeconds(DEFAULT_DATE.toSeconds()).toISODate(),
                changefreq: 'weekly',
                priority: '0.8',
            },
            {
                url: '/o-proekte/howuse/',
                lastmod: DateTime.fromSeconds(DEFAULT_DATE.toSeconds()).toISODate(),
                changefreq: 'weekly',
                priority: '0.8',
            },
        ];
    },

    async news() {
        const news = await Article.createQueryBuilder('')
            .where({
                status: 1,
            })
            .select('*')
            .getRawMany();

        const urls = [];

        for (const article of news) {
            urls.push({
                url: `/novosti-i-obnovleniya/novosti/${article.slug}/`,
                lastmod: DateTime.fromSeconds(
                    article.published_at || article.updated_at || article.added_at
                ).toISODate(),
                changefreq: 'weekly',
                priority: '0.8',
            });
        }

        return urls;
    },

    async quiz() {
        const quizQuestions = await QueezeQuestions.createQueryBuilder('')
            .select('*')
            .getRawMany();

        const urls = [];

        for (const question of quizQuestions) {
            urls.push({
                url: `/bibleyskaya-viktorina/voprosi/${question.code}`,
                // todo: create date entity field to add lastmod, fill approximate dates of questions entry
                lastmod: DateTime.local().setZone('Europe/Moscow').toISODate(),
                changefreq: 'weekly',
                priority: '0.5',
            });
        }

        return urls;
    },

    async preachings() {
        const celebrationTypes = await CelebrationExcuse.find({
            where: { active: 1 },
            relations: ['excuses', 'excuses.preachings', 'excuses.preachings.preacher'],
        });

        const urls = [];

        for (const celebrationType of celebrationTypes) {
            for (const occasion of celebrationType.excuses) {
                for (const preaching of occasion.preachings) {
                    urls.push({
                        url: `/all-about-bible/propovedi/${generateSlug(celebrationType.title)}/${occasion.code
                            }/${preaching.preacher.code}/${preaching.code}/`,
                        // todo: create date entity field(s) to add lastmod
                        lastmod: DateTime.local().setZone('Europe/Moscow').toISODate(),
                        changefreq: 'weekly',
                        priority: '0.5',
                    });
                }
            }
        }

        return urls;
    },

    async biblegroups() {
        const urls = [
            {
                url: `/bible-group/about/`,
                lastmod: DateTime.local().setZone('Europe/Moscow').toISODate(),
                changefreq: 'weekly',
                priority: '0.5', // todo: add lastmod = latest(bibleGroupNote, bibleGroup)
            },
            {
                url: `/bible-group/video/`,
                lastmod: DateTime.local().setZone('Europe/Moscow').toISODate(),
                changefreq: 'weekly',
                priority: '0.5',
            },
        ];

        const bibleGroupNotes = await BibleGroupNote.find({ active: 1 });
        for (const note of bibleGroupNotes) {
            urls.push({
                url: `/bible-group/for-leaders/${note.code}/`,
                lastmod: DateTime.local().setZone('Europe/Moscow').toISODate(), // todo: normalize note.data (that is, datE) to add lastmod
                changefreq: 'weekly',
                priority: '0.5',
            });
        }

        const bibleGroups = await BibleGroup.find({ active: 1 });
        for (const group of bibleGroups) {
            urls.push({
                url: `/bible-group/${group.code}/`,
                lastmod: DateTime.local().setZone('Europe/Moscow').toISODate(),
                changefreq: 'weekly',
                priority: '0.5', // todo: normalize group.data (that is, datE) to add lastmod
            });
        }

        return urls;
    },
};
