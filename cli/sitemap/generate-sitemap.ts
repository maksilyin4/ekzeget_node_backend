#!/usr/bin/env ts-node

import { join } from 'path';
import generate from './generator';
import run from '../runner';

const folder =
    process.argv.find((arg, i) => process.argv[i - 1] === '--folder') ||
    join(process.cwd(), './sitemaps');

const EnvironmentManager = require('../../src/managers/EnvironmentManager').default;
const { REST_HOST, PROTOCOL } = EnvironmentManager.envs;
run(async () => generate(folder, `${PROTOCOL}://${REST_HOST}`));
