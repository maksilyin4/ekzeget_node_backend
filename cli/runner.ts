import * as process from 'process';

let env;
if (!~process.argv.indexOf('--env')) {
    env = 'dev';
    process.argv.push('--env');
    process.argv.push('dev');
} else {
    env = process.argv[process.argv.indexOf('--env') + 1];
}

const appConf = require('../ecosystem.config');

Object.assign(process.env, appConf.apps[0]['env_' + env]);

const Database = require('../src/database').default;
const EnvironmentChecker = require('../src/core/EnvironmentChecker').default;

const checkedEnvironment = new EnvironmentChecker().getCheckedEnvironment();
const database = new Database(checkedEnvironment);

export default function run(command: () => Promise<void>) {
    database.init().then(async () => {
        await command();
        await database.connection.close();
    });
}
