FROM node:14.15.3-alpine3.12

COPY package.json \
     package-lock.json \
     tsconfig.json \
     oauth2-strategies.json \
     ecosystem.config.js \
     ormconfig.ts \
     /usr/src/app/

WORKDIR /usr/src/app/
RUN npm ci --quiet

ADD src /usr/src/app/src

CMD [ "npm", "run", "migrate" ]
