FROM node:14.15.3-alpine3.12 as base


FROM base as build

RUN apk --no-cache add g++ gcc libgcc libstdc++ linux-headers make python2

WORKDIR /usr/src/app/

COPY package.json package-lock.json \
     ecosystem.config.js \
     tsconfig.json \
     oauth2-strategies.json \
     build.sh install-tinymce.sh \
     /usr/src/app/

RUN npm ci --quiet

ADD src /usr/src/app/src
ADD cli /usr/src/app/cli

RUN chmod +x build.sh install-tinymce.sh
RUN npm run build:prod


FROM base as final

RUN apk --no-cache add g++ gcc libgcc libstdc++ linux-headers make python2

WORKDIR /usr/src/app/

COPY package.json package-lock.json \
     tsconfig.json \
     ecosystem.config.js \
     oauth2-strategies.json \
     docker-entrypoint.sh \
     /usr/src/app/

RUN chmod +x docker-entrypoint.sh
RUN npm ci --quiet --only=prod
RUN npm install pm2 -g

COPY --from=build /usr/src/app/build ./build

ENV NODE_ENV=production
CMD [ "./docker-entrypoint.sh" ]
