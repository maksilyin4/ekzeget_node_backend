import FtpManager from '../../src/managers/FtpManager';
import IFtpFile from '../../src/managers/FtpManager/types/IFtpFile';
import * as ftp from 'basic-ftp';
import EnvironmentManager from '../../src/managers/EnvironmentManager';

describe('FtpManager', () => {
    const mediaFiles = [
        { path: 'tests/files/Test.fb2', name: 'Test.fb2' },
        { path: 'tests/files/Test.mp3', name: 'Test.mp3' },
    ];

    describe('method uploadMediaFiles', () => {
        test('normal', async () => {
            await FtpManager.uploadMediaFilesToStore(mediaFiles as IFtpFile[], '/Test/Dir');

            const ftpConnect = new ftp.Client();
            await ftpConnect.access(EnvironmentManager.ftpStorageAccessConfig);

            const ftpList = await ftpConnect.list(
                EnvironmentManager.envs.STORAGE_MEDIA_DIR + '/Test/Dir'
            );

            expect(ftpList[0].name).toBe('Test.fb2');
            expect(ftpList[0].type).toBe(1);
            expect(ftpList[1].name).toBe('Test.mp3');
            expect(ftpList[1].type).toBe(1);

            await ftpConnect.removeDir(EnvironmentManager.envs.STORAGE_MEDIA_DIR + '/Test');
            await ftpConnect.close();
        });

        test('try double connect', async () => {
            const ftpConnect = new ftp.Client();
            await ftpConnect.access(EnvironmentManager.ftpStorageAccessConfig);

            await FtpManager.uploadMediaFilesToStore(mediaFiles as IFtpFile[], '/Test/Dir');

            const ftpList = await ftpConnect.list(
                EnvironmentManager.envs.STORAGE_MEDIA_DIR + '/Test/Dir'
            );

            expect(ftpList[0].name).toBe('Test.fb2');
            expect(ftpList[0].type).toBe(1);
            expect(ftpList[1].name).toBe('Test.mp3');
            expect(ftpList[1].type).toBe(1);

            await ftpConnect.removeDir(EnvironmentManager.envs.STORAGE_MEDIA_DIR + '/Test');
            await ftpConnect.close();
        });
    });

    describe('method getDirectoryContents', () => {
        test('get directory content', async () => {
            await FtpManager.uploadMediaFilesToStore(mediaFiles as IFtpFile[], '/Test/Dir');
            const content = await FtpManager.getDirectoryContents('/Test/Dir');

            expect(content).toEqual([
                { isLeaf: true, key: '/Test/Dir/Test.fb2', title: 'Test.fb2' },
                { isLeaf: true, key: '/Test/Dir/Test.mp3', title: 'Test.mp3' },
            ]);

            await FtpManager.removeDir('/Test');
        });
    });

    describe('method removeDir', () => {
        test('remove Dir', async () => {
            const ftpConnect = new ftp.Client();
            await ftpConnect.access(EnvironmentManager.ftpStorageAccessConfig);
            await ftpConnect.ensureDir(EnvironmentManager.envs.STORAGE_MEDIA_DIR + '/Test/Dir');

            await FtpManager.removeDir('/Test');

            const list = await ftpConnect.list(EnvironmentManager.envs.STORAGE_MEDIA_DIR + '/Test');
            expect(list).toEqual([]);

            await ftpConnect.close();
        });
    });

    describe('method uploadImageToStore', () => {
        const imageFile = { path: 'tests/files/Test.jpg', name: 'Test.jpg' };

        test('upload Image To Store', async () => {
            await FtpManager.uploadImageToStore(imageFile as IFtpFile, '/Test');

            const ftpConnect = new ftp.Client();
            await ftpConnect.access(EnvironmentManager.ftpStorageAccessConfig);

            const ftpList = await ftpConnect.list(
                EnvironmentManager.envs.STORAGE_ASSETS_DIR + '/Test'
            );

            expect(ftpList[0].name).toBe('Test.jpg');
            expect(ftpList[0].type).toBe(1);

            await ftpConnect.removeDir(EnvironmentManager.envs.STORAGE_ASSETS_DIR + '/Test');
            await ftpConnect.close();
        });
    });
});
