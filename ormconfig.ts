const env = process.env.NODE_ENV;
process.argv.push('--env', env);

import appConf from './ecosystem.config';

Object.assign(process.env, appConf.apps[0]['env_' + env]);

import connectionOptions from './src/database/ormconfig';

export = connectionOptions;
