import User from '../../../entities/User';
import { ServerError } from '../../../libs/ErrorHandler';
import authStrings from '../../../const/strings/auth';
import UserService from '../../../services/userServices/RegisterService';
import UserOauth2 from '../../../entities/UserOauth2';
import getUnixSec from '../../../libs/helpers/getUnixSec';
import { Profile } from 'passport';

export default async function(accessToken, refreshToken, profile, cb) {
    // eslint-disable-next-line prefer-const
    let { user, isExternalIdRegistered, email } = await getUser(profile);

    if (user) {
        if (user.status !== User.statuses.active) {
            throw new ServerError(authStrings.userIsNotActive, 401);
        }
    } else {
        const userService = new UserService();
        user = await userService.registerVerifiedUser({
            username: getUserName(profile),
            email,
            name: profile.name,
        });
    }

    if (!isExternalIdRegistered) {
        await UserOauth2.create({
            user_id: user.id,
            external_user_id: profile.id,
            provider: profile.provider,
            created_at: getUnixSec(),
        }).save();
    }

    return cb(null, user);
}

async function getUser(
    profile: Profile
): Promise<{ user: User; isExternalIdRegistered: boolean; email: string }> {
    const oauth2Entry = await UserOauth2.findOne({
        where: { external_user_id: profile.id, provider: profile.provider },
        relations: ['user'],
    });

    const email: string = profile?.emails?.length ? profile.emails[0].value : '';

    const user = oauth2Entry
        ? oauth2Entry.user
        : email
        ? await User.findOne({
              where: { email },
          })
        : null;

    return { user, isExternalIdRegistered: !!oauth2Entry, email };
}

function getUserName(profile: Profile): string {
    return profile?.emails?.length
        ? profile.emails[0].value.split('@')[0]
        : profile.name.givenName[0] + profile.name.familyName;
}
