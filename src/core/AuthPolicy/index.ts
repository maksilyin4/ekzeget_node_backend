import passport from 'passport';
import { Strategy as CustomStrategy } from 'passport-custom';
import UserToken from '../../entities/UserToken';
import EnvironmentDTO from '../EnvironmentChecker/EnvironmentDTO';

export default class AuthPolicy {
    constructor(private env: EnvironmentDTO) {}

    get plugin() {
        return passport.initialize();
    }

    get sessionConnectionPlugin() {
        return passport.session();
    }

    public init() {
        this.useStrategies();
        this.useSessionSerialization();
        return this;
    }

    private useStrategies() {
        passport.use('x-auth-token', this.xAuthTokenStrategyCreate());
    }

    private xAuthTokenStrategyCreate() {
        return new CustomStrategy(async function({ headers }, done) {
            const token = headers['x-authentication-token'];

            if (!token) return done(null, null);

            const userToken = await UserToken.findOne({
                where: { token },
                relations: ['user'],
            });
            if (!userToken) return done(null, undefined);

            const { user } = userToken;
            if (!user) return done(null, undefined);

            return done(null, user);
        });
    }

    private useSessionSerialization() {
        passport.serializeUser(function(user, done) {
            done(null, user['id']);
        });

        passport.deserializeUser(async function(userId, done) {
            done(null, userId);
        });
    }
}
