import express from 'express';
import bodyParser from 'body-parser';
import GraphQL from '../../api/GarphQL';
import cookieParser from 'cookie-parser';
import multer from 'multer';
import { graphqlHTTP } from 'express-graphql';

import SessionPolicy from '../SessionPolicy';
import AuthPolicy from '../AuthPolicy';

import RestApi from '../../api/RestApi';
import { EnvList } from '../EnvironmentChecker/EnvironmentDTO';
import IRestApi from './interfaces/IRestApi';
import info from '../../const/strings/logging/info';
import path from 'path';
import BaseApp from './BaseApp';
import passport from 'passport';
import Oauth2 from '../../services/userServices/AuthService/Oauth2';
import verifier from '../AuthPolicy/oauth2/verifier';

export default class App extends BaseApp {
    private expressApp: express.Application;

    public init() {
        this.create();
        this.plugStaticDir();
        this.plugRequestDataParser();
        this.plugSessionPolicy();
        this.plugAuthPolicy();
        this.plugCookieParser();
        this.plugRestApi();
        this.plugGraphQL();
        this.listen();
    }

    private create() {
        this.expressApp = express();
    }

    private plugRequestDataParser() {
        this.expressApp.use(bodyParser.json());
        this.expressApp.use(bodyParser.urlencoded({ extended: true }));
        this.expressApp.use(multer().none());
    }

    private plugStaticDir() {
        this.expressApp.use('/public', express.static(path.join(__dirname, 'public')));
    }

    private plugSessionPolicy() {
        this.expressApp.use(new SessionPolicy(this.connection).plugin);
    }

    private plugAuthPolicy() {
        const authPolicy = new AuthPolicy(this._envs).init();

        this.expressApp.use(authPolicy.plugin);
        this.expressApp.use(authPolicy.sessionConnectionPlugin);

        const oauth2 = new Oauth2(
            `${this._envs.PROTOCOL}://${this._envs.REST_HOST}`,
            this._envs.API_PREFIX + this._envs.API_VERSION + '/oauth2',
            require('../../../oauth2-strategies.json'),
            verifier
        );
        oauth2.useStrategies();
        oauth2.bindRoutes(this.expressApp);
    }

    private plugCookieParser() {
        this.expressApp.use(cookieParser());
    }
    // TODO tokenKey
    // todo ask for auth providers during build or inform at the end of the build or mention in installation section of README
    // TODO docs:::
    // token, install new provider (package, picture, config file), oauth2-providers.json, endpoint /list, other endpoints, base url, user names gen,  localStorage oauthReturnUrl
    // TODO fill in phone number, try to find oauth user by phone number as well as email
    // todo can't delete users: ER_ROW_IS_REFERENCED_2: Cannot delete or update a parent row: a foreign key constraint fails (`ekzeget_node`.`user_oauth2`, CONSTRAINT `FK_f6ac2dbbc73788bbbe1d8e92013` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`))
    // todo sphinx logs/bind
    private plugRestApi() {
        this.bindRoutes();
    }

    private plugGraphQL() {
        const graphQL = new GraphQL();
        graphQL.init();

        this.expressApp.use(
            `${this._envs.API_PREFIX}${this._envs.GRAPHQL_API_VERSION}/graphql`,
            graphqlHTTP({
                schema: graphQL.schema,
                graphiql: this._envs.ENV === EnvList.local,
            })
        );
    }

    private listen() {
        const server = this.expressApp.listen(+this._envs.PORT, this._envs.HOST, () => {
            console.info(
                info.serverGreeting,
                `${this._envs.PROTOCOL}://${this._envs.HOST}:${this._envs.PORT}`
            );
        });

        server.on('connection', socket => {
            socket.setTimeout(0);
        });
        server.keepAliveTimeout = 0;
    }

    private bindRoutes() {
        const restApi: IRestApi = new RestApi();
        const baseUrl = this._envs.API_PREFIX + this._envs.API_VERSION;

        restApi.routes.forEach(route => {
            const { method, controller, strategy, url } = route;

            const fullUrl = baseUrl + url;

            if (strategy) {
                this.expressApp[method](
                    fullUrl,
                    passport.authenticate(strategy),
                    restApi.wrapController(controller)
                );
            } else {
                this.expressApp[method](fullUrl, restApi.wrapController(controller));
            }
        });
    }
}
