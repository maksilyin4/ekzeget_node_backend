export interface IReadingPlanDetails {
    day: number;
    chapter: number;
    book_code: string;
    verse: string;
    short: string;
    branch?: number;
}
