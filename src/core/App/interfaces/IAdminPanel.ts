import express from 'express';

export default interface IAdminPanel {
    plugin: express.Router;
    pluginPath: string;
}
