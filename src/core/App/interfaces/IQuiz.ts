import QueezeQuestions from '../../../entities/QueezeQuestions';
import QueezeAnswers from '../../../entities/QueezeAnswers';
import Verse from '../../../entities/Verse';

export interface IQuiz {
    status: TQuizStatus;
    user_id: number;
    plan_id: number | null;
    id: number;
    code: string;
    current_question: number;
    current_question_code: string;
    created_at?: Date;
    updated_at?: Date;
    questions: Partial<IClientQuestion>[];
    wrong: string;
    introduction: string;
}

export interface IClientQuestion extends QueezeQuestions {
    status: TQuestionStatus;
    submittedAnswerId: number;
    audio: IAudioForQuiz;
    num: number;
    answers: IClientAnswer[];
}

export interface IAudioForQuiz {
    question: string;
    answer: string;
}

export interface IClientAnswer extends QueezeAnswers {
    is_correct: boolean;
}

export interface ICookieQuiz {
    queezes_codes: string[];
}

export interface IQuestionMediaAndAll extends Partial<QueezeQuestions> {
    verse: IVerseForMediaAndAll;
    answers?: IClientAnswer[];
}

export interface IVerseForMediaAndAll extends Verse {
    book_code: string;
    short: string;
}

export type TQuestionStatus = 'NA' | 'NO' | 'OK';

/**
 * R- актуальный
 * С- завершенный
 * Р- не завершенный, не актуальный
 */
export type TQuizStatus = 'R' | 'C' | 'P';

export type TQuizCriteria = {
    types: ('T' | 'I')[];
    books: number[];
    chapter_id?: number;
};
