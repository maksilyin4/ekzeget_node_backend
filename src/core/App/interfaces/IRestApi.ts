import { Request, Response, NextFunction } from 'express';
import Controller from '../../../api/RestApi/Controller';

export default interface IRestApi {
    routes: Array<IRoute>;
    wrapController: (controller: Controller) => THandler;
}

export interface IRoute {
    method: 'get' | 'post' | 'put' | 'delete' | 'patch';
    url: string;
    controller: Controller;
    description: string;
    strategy?: string;
}

export type THandler = (res: Request, req: Response, next: NextFunction) => void;
