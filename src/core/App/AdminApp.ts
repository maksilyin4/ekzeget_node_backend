import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import multer from 'multer';

import SessionPolicy from '../SessionPolicy';
import AuthPolicy from '../AuthPolicy';

import AdminBro from '../../admin-panel';
import IAdminPanel from './interfaces/IAdminPanel';
import info from '../../const/strings/logging/info';
import path from 'path';
import BaseApp from './BaseApp';

export default class AdminApp extends BaseApp {
    private expressApp: express.Application;

    public init() {
        this.create();
        this.plugStaticDir();
        this.plugAdminPanel(new AdminBro());
        this.plugRequestDataParser();
        this.plugSessionPolicy();
        this.plugAuthPolicy();
        this.plugCookieParser();
        this.listen();
    }

    private create() {
        this.expressApp = express();
    }

    private plugRequestDataParser() {
        this.expressApp.use(bodyParser.json());
        this.expressApp.use(bodyParser.urlencoded({ extended: true }));
        this.expressApp.use(multer().none());
    }

    private plugStaticDir() {
        this.expressApp.use('/public', express.static(path.join(__dirname, 'public')));
    }

    private plugSessionPolicy() {
        this.expressApp.use(new SessionPolicy(this.connection).plugin);
    }

    private plugAuthPolicy() {
        const authPolicy = new AuthPolicy(this._envs).init();

        this.expressApp.use(authPolicy.plugin);
        this.expressApp.use(authPolicy.sessionConnectionPlugin);
    }

    private plugCookieParser() {
        this.expressApp.use(cookieParser());
    }

    private plugAdminPanel(adminPanel: IAdminPanel) {
        this.expressApp.use(adminPanel.pluginPath, adminPanel.plugin);
    }

    private listen() {
        const server = this.expressApp.listen(+this._envs.ADMIN_PORT, this._envs.HOST, () => {
            console.info(
                info.serverGreeting,
                `${this._envs.PROTOCOL}://${this._envs.HOST}:${this._envs.ADMIN_PORT}`
            );
        });

        server.on('connection', socket => {
            socket.setTimeout(0);
        });
        server.keepAliveTimeout = 0;
    }
}
