import EnvironmentDTO from '../EnvironmentChecker/EnvironmentDTO';
import { Connection } from 'typeorm';

export interface AppConstructor {
    new (_envs: EnvironmentDTO, connection: Connection): BaseApp;
}

export default abstract class BaseApp {
    constructor(protected _envs: EnvironmentDTO, protected connection: Connection) {}
    abstract init(): void;
}
