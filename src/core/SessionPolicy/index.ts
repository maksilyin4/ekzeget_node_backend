import Session from '../../entities/Session';
import session from 'express-session';
import { TypeormStore } from 'connect-typeorm';
import EnvironmentManager from '../../managers/EnvironmentManager';
import sessionConst from '../../const/numbers/sessionConst';

export default class SessionPolicy {
    constructor(private dbConnectionForStore) {}

    private createOptions() {
        const { SESSION_SECRET } = EnvironmentManager.envs;
        const sessionRepository = this.dbConnectionForStore.getRepository(Session);

        return {
            store: new TypeormStore({
                cleanupLimit: 2,
                limitSubquery: false, // If using MariaDB.
                ttl: sessionConst.sessionTtl,
            }).connect(sessionRepository),
            secret: SESSION_SECRET,
            resave: true,
            rolling: true,
            saveUninitialized: false,
            cookie: {
                maxAge: sessionConst.sessionTtl,
                httpOnly: false,
            },
        };
    }

    get plugin() {
        const options = this.createOptions();
        return session(options);
    }
}
