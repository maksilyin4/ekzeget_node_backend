import * as dotenv from 'dotenv';
import error from '../../const/strings/logging/error';
import EnvironmentDTO from './EnvironmentDTO';

if (!process.env.PM2_HOME) {
    dotenv.config();
}

export default class EnvironmentChecker {
    private processEnvironment = process.env;

    public getCheckedEnvironment() {
        const expectedEnvironment = new EnvironmentDTO();

        for (const expectedEnvKey in expectedEnvironment) {
            expectedEnvironment[expectedEnvKey] = this.processEnvironment[expectedEnvKey];
        }

        this.checkUnsetEnvs(expectedEnvironment);

        return expectedEnvironment;
    }

    private checkUnsetEnvs(envs: object) {
        const unsetEnvs = Object.entries(envs).filter(([key, val]) => val === undefined);

        if (unsetEnvs.length) {
            console.error(
                error.unsetEnvs,
                unsetEnvs.map(el => el[0])
            );
            process.exit(9);
        }
    }
}
