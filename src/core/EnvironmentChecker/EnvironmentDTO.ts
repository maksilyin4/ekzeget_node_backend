export enum EnvList {
    local = 'local',
    dev = 'dev',
    prod = 'production',
}

export default class EnvironmentDTO {
    PORT: string = null;
    ADMIN_PORT: string = null;
    HOST: string = null;
    REST_HOST: string = null;
    PROTOCOL: string = null;
    ENV: EnvList = null;

    MYSQL_HOST: string = null;
    MYSQL_PORT: string = null;
    MYSQL_DATABASE: string = null;
    MYSQL_USER: string = null;
    MYSQL_PASSWORD: string = null;
    MYSQL_ROOT_PASSWORD: string = null;

    API_PREFIX: string = null;
    API_VERSION: string = null;

    GRAPHQL_API_VERSION: string = null;

    SESSION_SECRET: string = null;

    SMTP_HOST: string = null;
    SMTP_PORT: string = null;
    SMTP_SOURCE_MAIL: string = null;
    SMTP_ADMIN_MAIL: string = null;
    SMTP_LOGIN: string = null;
    SMTP_PASS: string = null;

    CHECK_CAPTCHA: string = null;

    STORAGE_HOST: string = null;
    STORAGE_FTP_USER: string = null;
    STORAGE_FTP_PASS: string = null;
    STORAGE_ASSETS_DIR: string = null;
    STORAGE_MEDIA_DIR: string = null;

    OLD_BIBLE_CDN: string = null;
    STORAGE_ASSETS_CDN: string = null;
    STORAGE_MEDIA_CDN: string = null;

    MEDIA_CDN_UPLOADS_URL: string = null;
    MEDIA_CDN_UPLOADS_DIR: string = null;
    MEDIA_CDN_MP3_URL: string = null;
    MEDIA_CDN_MP3_DIR: string = null;
    MEDIA_CDN_MEDIA_URL: string = null;
    MEDIA_CDN_MEDIA_DIR: string = null;
}
