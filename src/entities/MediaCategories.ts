import { BaseEntity, Entity, Index, PrimaryColumn } from 'typeorm';

@Index('category_id', ['category_id'], {})
@Index('media_categories_ibfk_1', ['category_id'], {})
@Index('media_categories_ibfk_2', ['media_id'], {})
@Entity('media_categories', { schema: 'ekzeget' })
export default class MediaCategories extends BaseEntity {
    @PrimaryColumn({ type: 'int', name: 'media_id' })
    media_id: number;

    @PrimaryColumn({ type: 'int', name: 'category_id' })
    category_id: number;
}
