import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import Article from './Article';

@Index('idx-slider-article_id', ['article_id'], {})
@Entity('slider', { schema: 'ekzeget' })
export default class Slider extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'article_id', nullable: true })
    article_id: number | null;

    @Column('varchar', { name: 'video_url', nullable: true, length: 512 })
    video_url: string | null;

    @Column('varchar', { name: 'image', nullable: true, length: 256 })
    image: string | null;

    @Column('varchar', { name: 'title', nullable: true, length: 512 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('varchar', { name: 'type', length: 16, default: () => "'small'" })
    type: string;

    @Column('smallint', { name: 'status', default: () => "'0'" })
    status: number;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @UpdateDateColumn({ name: 'updated_at', nullable: true, type: 'timestamp' })
    updated_at: number | null;

    @Column('varchar', { name: 'url', nullable: true, length: 512 })
    url: string | null;

    @ManyToOne(
        () => Article,
        article => article.sliders,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'article_id', referencedColumnName: 'id' }])
    article: Article;
}
