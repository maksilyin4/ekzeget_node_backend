import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import MediaBible from './MediaBible';
import MediaContent from './MediaContent';
import FileStorageItem from './FileStorageItem';
import Media from './Media';

@Index('media_content_files_content', ['contentId'], {})
@Index('media_content_files_files', ['fileId'], {})
@Index('media_content_files_media', ['mediaId'], {})
@Index('idx_media_content_files_downloads', ['downloads'], {})
@Entity('media_content_files', { schema: 'ekzeget' })
export default class MediaContentFiles extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'media_id', nullable: true })
    mediaId: number | null;

    @Column('int', { name: 'content_id', nullable: true })
    contentId: number | null;

    @Column('int', { name: 'file_id', nullable: true })
    fileId: number | null;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('varchar', { name: 'url', nullable: true, length: 255 })
    url: string | null;

    @Column('varchar', { name: 'download_url', nullable: true, length: 255 })
    downloadUrl: string | null;

    @Column('int', { name: 'is_full', nullable: true, default: () => "'0'" })
    isFull: number | null;

    @Column('int', { name: 'downloads', nullable: true, default: () => "'0'" })
    downloads: number | null;

    @Column('varchar', { name: 'type', nullable: true, length: 64 })
    type: string | null;

    @Column('varchar', { name: 'time', nullable: true, length: 64 })
    time: string | null;

    @OneToMany(
        () => MediaBible,
        mediaBible => mediaBible.content
    )
    mediaBibles: MediaBible[];

    @ManyToOne(
        () => MediaContent,
        mediaContent => mediaContent.mediaContentFiles,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'content_id', referencedColumnName: 'id' }])
    content: MediaContent;

    @ManyToOne(
        () => FileStorageItem,
        fileStorageItem => fileStorageItem.media_content_files,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'file_id', referencedColumnName: 'id' }])
    file: FileStorageItem;

    @ManyToOne(
        () => Media,
        media => media.mediaContentFiles,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;
}
