import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('idx_media_content_type_active', ['active'], {})
@Index('idx_media_content_type_type', ['type'], {})
@Entity('media_content_type', { schema: 'ekzeget' })
export default class MediaContentType extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('int', { name: 'type', nullable: true })
    type: number | null;

    @Column('varchar', { name: 'type_name', nullable: true, length: 255 })
    typeName: string | null;

    @Column('int', { name: 'active', nullable: true, default: () => "'1'" })
    active: number | null;
}
