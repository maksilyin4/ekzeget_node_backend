import {
    BaseEntity,
    Column,
    Entity,
    PrimaryGeneratedColumn,
    BeforeInsert,
    BeforeUpdate,
} from 'typeorm';

@Entity('meta_template', { schema: 'ekzeget' })
export default class MetaTemplate extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'url', length: 255, unique: true, nullable: false })
    url: string;

    @Column('varchar', { name: 'title', length: 512 })
    title: string;

    @Column('text', { name: 'description' })
    description: string;

    @Column('varchar', { name: 'h_one', length: 512 })
    h_one: string;

    @Column('varchar', { name: 'context_example' })
    context_example: string;

    @Column('int', { name: 'level' })
    level: number;

    @Column('timestamp', {
        name: 'created_at',
        nullable: true,
    })
    created_at: number | null;

    @Column('timestamp', {
        name: 'updated_at',
        nullable: true,
    })
    updated_at: number | null;

    @Column('text', { name: 'keywords' })
    keywords: string;

    @BeforeInsert()
    @BeforeUpdate()
    setLevel() {
        this.level = this.url.split('/').length;
    }
}
