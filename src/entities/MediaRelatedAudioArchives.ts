import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Media from './Media';

@Index('idx-media_related_audio_archives-hash', ['hash'], {})
@Entity('media_related_audio_archives', { schema: 'ekzeget' })
export default class MediaRelatedAudioArchives extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'media_id' })
    mediaId: number;

    @Column('varchar', { name: 'archive_name', length: 255 })
    archiveName: string;

    @Column('datetime', {
        name: 'created_at',
        nullable: true,
        default: () => 'CURRENT_TIMESTAMP',
    })
    createdAt: Date | null;

    @Column('datetime', { name: 'updated_at', nullable: true })
    updatedAt: Date | null;

    @Column('varchar', { name: 'hash', length: 255 })
    hash: string;

    @OneToOne(
        () => Media,
        media => media.mediaRelatedAudioArchives,
        {
            onDelete: 'CASCADE',
            onUpdate: 'NO ACTION',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;
}
