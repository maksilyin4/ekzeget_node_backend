import { BaseEntity, Column, Entity, OneToMany } from 'typeorm';
import RbacAuthItem from './RbacAuthItem';

@Entity('rbac_auth_rule', { schema: 'ekzeget' })
export default class RbacAuthRule extends BaseEntity {
    @Column('varchar', { primary: true, name: 'name', length: 64 })
    name: string;

    @Column('blob', { name: 'data', nullable: true })
    data: Buffer | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;

    @OneToMany(
        () => RbacAuthItem,
        rbacAuthItem => rbacAuthItem.ruleName2
    )
    rbacAuthItems: RbacAuthItem[];
}
