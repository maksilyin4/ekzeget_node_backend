import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateResult,
} from 'typeorm';
import Translate from './Translate';
import Verse from './Verse';

@Index('idx_code', ['code'], {})
@Index('fk_verse_translate_verse', ['verse_id'], {})
@Entity('verse_translate', { schema: 'ekzeget' })
export default class VerseTranslate extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'code', nullable: true, length: 32 })
    code: string | null;

    @Column('text', { name: 'text', nullable: true })
    text: string | null;

    @Column('int', { name: 'verse_id', nullable: true })
    verse_id: number | null;

    @ManyToOne(
        () => Translate,
        translate => translate.verseTranslates,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'code', referencedColumnName: 'code' }])
    code2: Translate;

    @ManyToOne(
        () => Verse,
        verse => verse.verse_translates,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;

    static async updateManyByVerseIdAndCodes(verseId: number, codes: Map<string, string>) {
        if (verseId === undefined) return;

        const updateRequests: Array<Promise<UpdateResult>> = [];

        codes.forEach((value, key) => {
            const updateQuery = this.createQueryBuilder()
                .update()
                .set({
                    text: value,
                })
                .where('code = :key', { key })
                .andWhere('verse_id = :verseId', { verseId })
                .execute();

            updateRequests.push(updateQuery);
        });

        await Promise.all(updateRequests);
    }

    static async insertManyByVerseIdAndParams(verseId: number | undefined, params: Map<string, string>) {
        const lastId = await Verse.findOne({
            order: {
                id: 'DESC',
            },
        });

        verseId = verseId ? verseId : lastId.id;

        const values: Array<Partial<VerseTranslate>> = [];
        params.forEach((value, key) => {
            values.push({ code: key, text: value, verse_id: verseId });
        });

        await this.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }

    static async deleteGroupByVerseId(id) {
        await this.createQueryBuilder('del')
            .delete()
            .where('verse_id = :id', { id })
            .execute();
    }
}
