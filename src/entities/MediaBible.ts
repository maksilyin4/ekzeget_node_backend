import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Chapters from './Chapters';
import MediaContentFiles from './MediaContentFiles';
import Book from './Book';
import Media from './Media';
import Verse from './Verse';
import { IRelationForMedia } from '../admin-panel/content/types/IRelationForMedia';

@Index('media_media_bible_to_media', ['media_id'], {})
@Index('media_media_bible_to_book', ['book_id'], {})
@Index('media_media_bible_to_verse', ['verse_id'], {})
@Index('idx_media_bible_chapter', ['chapter_id'], {})
@Index('fk_media_bible_to_content', ['content_id'], {})
@Entity('media_bible', { schema: 'ekzeget' })
export default class MediaBible extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'media_id', nullable: true })
    media_id: number | null;

    @Column('int', { name: 'book_id', nullable: true })
    book_id: number | null;

    @Column('int', { name: 'verse_id', nullable: true })
    verse_id: number | null;

    @Column('int', { name: 'chapter_id', nullable: true })
    chapter_id: number | null;

    @Column('int', { name: 'content_id', nullable: true })
    content_id: number | null;

    @ManyToOne(
        () => Chapters,
        chapters => chapters.media_bibles,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'chapter_id', referencedColumnName: 'id' }])
    chapter: Chapters;

    @ManyToOne(
        () => MediaContentFiles,
        mediaContentFiles => mediaContentFiles.mediaBibles,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'content_id', referencedColumnName: 'id' }])
    content: MediaContentFiles;

    @ManyToOne(
        () => Book,
        book => book.media_bibles,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
    book: Book;

    @ManyToOne(
        () => Media,
        media => media.media_bibles,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;

    @ManyToOne(
        () => Verse,
        verse => verse.media_bibles,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;

    static async deleteGroupByIds(ids: number[]) {
        await this.createQueryBuilder('del')
            .delete()
            .where('id IN (:...ids)', { ids })
            .execute();
    }

    static async insertMany(mediaId: number | undefined, relations: IRelationForMedia[]) {
        const lastId = await Media.findOne({
            order: {
                id: 'DESC',
            },
        });

        mediaId = mediaId ? mediaId : lastId.id;

        const values: Partial<MediaBible>[] = await this.prepareValuesForInsert(relations, mediaId);

        await this.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }

    private static async prepareValuesForInsert(relations: IRelationForMedia[], mediaId) {
        return Promise.all(
            relations.map(
                async (item): Promise<Partial<MediaBible>> => {
                    const bookId = await Book.findOne({
                        select: ['id'],
                        where: { short_title: item.relationBookShortRecord },
                    });

                    const chapterId = await Chapters.findOne({
                        select: ['id'],
                        where: {
                            book_id: bookId.id,
                            number: item.relationChapterNumber,
                        },
                    });

                    return {
                        media_id: mediaId,
                        book_id: bookId.id,
                        verse_id: item.relationVerseId,
                        chapter_id: chapterId ? chapterId.id : null,
                        content_id: null,
                    };
                }
            )
        );
    }
}
