import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';
import BibleMapsVerse from './BibleMapsVerse';

@Index('bible_maps_points_user', ['created_by'], {})
@Index('idx_mark_active', ['active'], {})
@Entity('bible_maps_points', { schema: 'ekzeget' })
export default class BibleMapsPoints extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('text', { name: 'image_id', nullable: true })
    image_id: string | null;

    @Column('float', { name: 'lon', nullable: true, precision: 12 })
    lon: number | null;

    @Column('float', { name: 'lat', nullable: true, precision: 12 })
    lat: number | null;

    @Column('int', { name: 'created_by', nullable: true })
    created_by: number | null;

    @Column('int', { name: 'edited_by', nullable: true })
    edited_by: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'edited_at', nullable: true })
    edited_at: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @ManyToOne(
        () => User,
        user => user.bibleMapsPoints,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'created_by', referencedColumnName: 'id' }])
    createdBy2: User;

    @OneToMany(
        () => BibleMapsVerse,
        bibleMapsVerse => bibleMapsVerse.point
    )
    bible_maps_verses: BibleMapsVerse[];
}
