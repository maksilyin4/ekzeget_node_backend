import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import EkzegetPerson from './EkzegetPerson';

@Index('ekzeget_type-code-idx', ['code'], { unique: true })
@Index('idx_type_active', ['active'], {})
@Entity('ekzeget_type', { schema: 'ekzeget' })
export default class EkzegetType extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @OneToMany(
        () => EkzegetPerson,
        ekzegetPerson => ekzegetPerson.ekzeget_type
    )
    ekzegetPeople: EkzegetPerson[];
}
