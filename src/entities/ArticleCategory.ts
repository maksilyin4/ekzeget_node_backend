import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Article from './Article';

@Index('fk_article_category_section', ['parent_id'], {})
@Entity('article_category', { schema: 'ekzeget' })
export default class ArticleCategory extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'slug', length: 1024 })
    slug: string;

    @Column('varchar', { name: 'title', length: 512 })
    title: string;

    @Column('text', { name: 'body', nullable: true })
    body: string | null;

    @Column('int', { name: 'parent_id', nullable: true })
    parent_id: number | null;

    @Column('smallint', { name: 'status', default: () => "'0'" })
    status: number;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updated_at: number | null;

    @OneToMany(
        () => Article,
        article => article.category
    )
    articles: Article[];

    @ManyToOne(
        () => ArticleCategory,
        articleCategory => articleCategory.articleCategories,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'parent_id', referencedColumnName: 'id' }])
    parent: ArticleCategory;

    @OneToMany(
        () => ArticleCategory,
        articleCategory => articleCategory.parent
    )
    articleCategories: ArticleCategory[];
}
