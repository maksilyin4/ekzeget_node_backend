import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';
import { VerseLinkParser } from '../libs/VerseLinkParser';
import { ParsedShortLink } from '../libs/VerseLinkParser/types';
import Book from './Book';
import Verse from './Verse';

@Index('user_mark_user', ['user_id'], {})
@Index('idx_mark_color', ['color'], {})
@Entity('user_mark', { schema: 'ekzeget' })
export default class UserMark extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'user_id', nullable: true })
    user_id: number | null;

    @Column('varchar', { name: 'color', nullable: true, length: 255 })
    color: string | null;

    @Column('varchar', { name: 'bookmark', nullable: true, length: 255 })
    bookmark: string | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @ManyToOne(
        () => User,
        user => user.userMarks,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;

    static getUserBookmarksByVerseLink = async (userBookmark: UserMark) => {
        const verseLinkParser = new VerseLinkParser();

        const { bookShortTitle, chapters }: ParsedShortLink = verseLinkParser.parseString(
            userBookmark.bookmark
        )[0];

        const book = await Book.findOne({ where: { short_title: bookShortTitle } });
        const { chapterNumber, verses } = chapters[0];
        const verseNumber = (verses[0] && verses[0].equal) || null;
        const verse =
            verseNumber &&
            (await Verse.findOne({
                where: { book_id: book.id, chapter: chapterNumber, number: verseNumber },
            }));

        return {
            ...userBookmark,
            info: {
                book,
                chapter: chapterNumber,
                number: verseNumber,
                verse,
            },
        };
    };
}
