import { ISession } from 'connect-typeorm';
import { Column, Entity, Index, PrimaryColumn } from 'typeorm';

@Entity({ name: 'session' })
export default class Session implements ISession {
    @PrimaryColumn('varchar', { length: 255 })
    public id = '';

    @Column('text')
    public json = '';

    @Index()
    @Column({ type: 'bigint', name: 'expired_at' })
    public expiredAt = Date.now();
}
