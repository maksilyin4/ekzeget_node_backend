import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Media from './Media';
import MediaAuthor from './MediaAuthor';
import MediaCategory from './MediaCategory';
import MediaContentFiles from './MediaContentFiles';
import MediaGenre from './MediaGenre';

@Entity('file_storage_item', { schema: 'ekzeget' })
export default class FileStorageItem extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'component', length: 255 })
    component: string;

    @Column('varchar', { name: 'base_url', length: 1024 })
    base_url: string;

    @Column('varchar', { name: 'path', length: 1024 })
    path: string;

    @Column('varchar', { name: 'type', nullable: true, length: 255 })
    type: string | null;

    @Column('int', { name: 'size', nullable: true })
    size: number | null;

    @Column('varchar', { name: 'name', nullable: true, length: 255 })
    name: string | null;

    @Column('varchar', { name: 'upload_ip', nullable: true, length: 15 })
    upload_ip: string | null;

    @Column('int', { name: 'created_at' })
    created_at: number;

    @Column('varchar', { name: 'alt', nullable: true, length: 255 })
    alt: string | null;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @OneToMany(
        () => Media,
        media => media.image
    )
    media: Media[];

    @OneToMany(
        () => MediaAuthor,
        mediaAuthor => mediaAuthor.image2
    )
    media_authors: MediaAuthor[];

    @OneToMany(
        () => MediaCategory,
        mediaCategory => mediaCategory.image2
    )
    media_categories: MediaCategory[];

    @OneToMany(
        () => MediaContentFiles,
        mediaContentFiles => mediaContentFiles.file
    )
    media_content_files: MediaContentFiles[];

    @OneToMany(
        () => MediaGenre,
        mediaGenre => mediaGenre.image2
    )
    media_genres: MediaGenre[];
}
