import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import CelebrationPreacherExcuse from './CelebrationPreacherExcuse';
import CelebrationExcuse from './CelebrationExcuse';
import Preaching from './Preaching';

@Index('excuse-code-idx', ['code'], { unique: true })
@Index('idx_excuse_active', ['active'], {})
@Index('fk_user_excuse_parent', ['parent_id'], {})
@Entity('excuse', { schema: 'ekzeget' })
export default class Excuse extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('int', { name: 'parent_id', nullable: true })
    parent_id: number | null;

    @Column('varchar', { name: 'parent', length: 255 })
    parent_str: string;

    @Column('varchar', { name: 'apostolic', length: 255 })
    apostolic: string;

    @Column('varchar', { name: 'gospel', length: 255 })
    gospel: string;

    @Column('int', { name: 'sort', nullable: true, default: () => "'0'" })
    sort: number | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @OneToMany(
        () => CelebrationPreacherExcuse,
        celebrationPreacherExcuse => celebrationPreacherExcuse.excuse
    )
    celebration_preacher_excuses: CelebrationPreacherExcuse[];

    @ManyToOne(
        () => CelebrationExcuse,
        celebrationExcuse => celebrationExcuse.excuses,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'parent_id', referencedColumnName: 'id' }])
    parent: CelebrationExcuse;

    @OneToMany(
        () => Preaching,
        preaching => preaching.excuse
    )
    preachings: Preaching[];
}
