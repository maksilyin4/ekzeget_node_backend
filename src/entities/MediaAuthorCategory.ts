import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import MediaAuthor from './MediaAuthor';

@Entity('media_author_category', { schema: 'ekzeget' })
export default class MediaAuthorCategory extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'slug', length: 1024 })
    slug: string;

    @Column('varchar', { name: 'title', length: 512 })
    title: string;

    @Column('text', { name: 'body', nullable: true })
    body: string | null;

    @Column('smallint', { name: 'status', default: () => "'0'" })
    status: number;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updated_at: number | null;

    @OneToMany(
        () => MediaAuthor,
        mediaAuthor => mediaAuthor.category
    )
    mediaAuthors: MediaAuthor[];
}
