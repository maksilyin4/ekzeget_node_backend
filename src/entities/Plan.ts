import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Book from './Book';
import Reading from './Reading';
import ReadingPlanComment from './ReadingPlanComment';
import QueezeApplications from './QueezeApplications';

@Index('fk_plan_reading', ['reading_id'], {})
@Index('fk_plan_book', ['book_id'], {})
@Index('idx_plan_day', ['day'], {})
@Entity('plan', { schema: 'ekzeget' })
export default class Plan extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    //@RelationId((plan: Plan) => plan.reading)
    @Column('int', { name: 'reading_id', nullable: true })
    reading_id: number | null;

    @Column('int', { name: 'branch', unsigned: true, nullable: true })
    branch: number | null;

    @Column('int', { name: 'day', nullable: true })
    day: number | null;

    //@RelationId((plan: Plan) => plan.book)
    @Column('int', { name: 'book_id', nullable: true })
    book_id: number | null;

    @Column('int', { name: 'chapter', nullable: true })
    chapter: number | null;

    @Column('varchar', { name: 'verse', nullable: true, length: 255 })
    verse: string | null;

    @ManyToOne(
        () => Book,
        book => book.plans,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        },
    )
    @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
    book: Book;

    @ManyToOne(
        () => Reading,
        reading => reading.plans,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        },
    )
    @JoinColumn([{ name: 'reading_id', referencedColumnName: 'id' }])
    reading: Reading;

    @OneToMany(
        () => QueezeApplications,
        application => application.plan,
    )
    quizApplicaitons: QueezeApplications[];

    equals(other: any): boolean {
        if (this === other)
            return true;
        return (this.branch ?? null) === (other.branch ?? null)
            && (this.day ?? null) === (other.day ?? null)
            && (this.book_id ?? null) === (other.book_id ?? null)
            && (this.chapter ?? null) === (other.chapter ?? null)
            && (this.verse ?? null) === (other.verse ?? null);
    }
}
