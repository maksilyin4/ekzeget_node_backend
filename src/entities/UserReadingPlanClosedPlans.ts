import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import Plan from './Plan';
import UserReadingPlan from './UserReadingPlan';

@Entity('user_reading_plan_closed_plans', { schema: 'ekzeget' })
export default class UserReadingPlanClosedPlans extends BaseEntity {
    @PrimaryColumn('int', { name: 'user_reading_plan_id' })
    user_reading_plan_id: number;

    @PrimaryColumn('int', { name: 'plan_id' })
    plan_id: number;

    @ManyToOne(() => UserReadingPlan)
    @JoinColumn({ name: 'user_reading_plan_id', referencedColumnName: 'id' })
    userReadingPlan: UserReadingPlan;

    @ManyToOne(() => Plan)
    @JoinColumn({ name: 'plan_id', referencedColumnName: 'id' })
    plan: Plan;
}
