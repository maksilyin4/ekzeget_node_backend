import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Media from './Media';

@Index('dni', ['day'], { unique: true })
@Index('active22', ['active'], {})
@Entity('apostolic_reading', { schema: 'ekzeget' })
export default class ApostolicReading extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'day', unique: true })
    day: number;

    @Column('text', { name: 'title' })
    title: string;

    @Column('text', { name: 'reading' })
    reading: string;

    @Column('text', { name: 'morning' })
    morning: string;

    @Column('text', { name: 'more' })
    more: string;

    @Column('int', { name: 'active', default: () => "'1'" })
    active: number;

    @OneToMany(
        () => Media,
        media => media.apostolicReading
    )
    media: Media;
}
