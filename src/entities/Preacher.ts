import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Preaching from './Preaching';

@Index('preacher-code-idx', ['code'], { unique: true })
@Index('idx_preacher_active', ['active'], {})
@Entity('preacher', { schema: 'ekzeget' })
export default class Preacher extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'name', nullable: true, length: 255 })
    name: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @OneToMany(
        () => Preaching,
        preaching => preaching.preacher
    )
    preachings: Preaching[];
}
