import {
    BaseEntity,
    Column,
    Entity,
    Index, ManyToMany,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Article from './Article';
import BibleMapsPoints from './BibleMapsPoints';
import GroupReadingPlan from './GroupReadingPlan';
import Interpretation from './Interpretation';
import Media from './Media';
import MediaAuthor from './MediaAuthor';
import MediaCategory from './MediaCategory';
import MediaComment from './MediaComment';
import MediaContent from './MediaContent';
import MediaGenre from './MediaGenre';
import MediaSubscribe from './MediaSubscribe';
import MediaTag from './MediaTag';
import UserBookmark from './UserBookmark';
import UserConfirmEmail from './UserConfirmEmail';
import UserInterpretationFav from './UserInterpretationFav';
import UserMark from './UserMark';
import UserNote from './UserNote';
import UserProfile from './UserProfile';
import UserReadingPlan from './UserReadingPlan';
import UserResetPassword from './UserResetPassword';
import UserToken from './UserToken';
import UserVerseFav from './UserVerseFav';
import ReadingPlan from './ReadingPlan';
import ReadingPlanComment from './ReadingPlanComment';
import ReadingArchive from './ReadingArchive';

@Index('idx_phone', ['phone'], {})
@Entity('user', { schema: 'ekzeget' })
export default class User extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'username', length: 32, unique: true })
    username: string;

    @Column('varchar', { name: 'password_hash', nullable: true, length: 255 })
    password_hash: string;

    @Column('varchar', { name: 'email', length: 255, unique: true })
    email: string;

    @Column('smallint', { name: 'status', default: () => "'2'" })
    status: number;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updated_at: number | null;

    @Column('int', { name: 'logged_at', nullable: true })
    logged_at: number | null;

    @Column('varchar', { name: 'phone', length: 16 })
    phone: string;

    @Column('int', { name: 'show_email', nullable: true, default: () => "'0'" })
    show_email: number | null;

    @OneToMany(
        () => Article,
        article => article.createdBy2
    )
    articles: Article[];

    @OneToMany(
        () => Article,
        article => article.updatedBy2
    )
    articles2: Article[];

    @OneToMany(
        () => BibleMapsPoints,
        bibleMapsPoints => bibleMapsPoints.createdBy2
    )
    bibleMapsPoints: BibleMapsPoints[];

    @OneToMany(
        () => Interpretation,
        interpretation => interpretation.added_by
    )
    interpretations: Interpretation[];

    @OneToMany(
        () => Interpretation,
        interpretation => interpretation.edited_by
    )
    interpretations2: Interpretation[];

    @OneToMany(
        () => Media,
        media => media.createdBy2
    )
    media: Media[];

    @OneToMany(
        () => MediaAuthor,
        mediaAuthor => mediaAuthor.createdBy2
    )
    mediaAuthors: MediaAuthor[];

    @OneToMany(
        () => MediaCategory,
        mediaCategory => mediaCategory.createdBy2
    )
    mediaCategories: MediaCategory[];

    @OneToMany(
        () => MediaComment,
        mediaComment => mediaComment.createdBy2
    )
    mediaComments: MediaComment[];

    @OneToMany(
        () => MediaContent,
        mediaContent => mediaContent.createdBy2
    )
    mediaContents: MediaContent[];

    @OneToMany(
        () => MediaGenre,
        mediaGenre => mediaGenre.createdBy2
    )
    mediaGenres: MediaGenre[];

    @OneToMany(
        () => MediaSubscribe,
        mediaSubscribe => mediaSubscribe.user
    )
    mediaSubscribes: MediaSubscribe[];

    @OneToMany(
        () => MediaTag,
        mediaTag => mediaTag.createdBy2
    )
    mediaTags: MediaTag[];

    @OneToMany(
        () => UserBookmark,
        userBookmark => userBookmark.user
    )
    userBookmarks: UserBookmark[];

    @OneToMany(
        () => UserConfirmEmail,
        userConfirmEmail => userConfirmEmail.user
    )
    userConfirmEmails: UserConfirmEmail[];

    @OneToMany(
        () => UserInterpretationFav,
        userInterpretationFav => userInterpretationFav.user
    )
    userInterpretationFavs: UserInterpretationFav[];

    @OneToMany(
        () => UserMark,
        userMark => userMark.user
    )
    userMarks: UserMark[];

    @OneToMany(
        () => UserNote,
        userNote => userNote.user
    )
    userNotes: UserNote[];

    @OneToOne(
        () => UserProfile,
        userProfile => userProfile.user
    )
    userProfile: UserProfile;

    @OneToMany(
        () => ReadingPlan,
        plan => plan.creator,
    )
    readingPlans: ReadingPlan[];

    @OneToMany(
        () => UserReadingPlan,
        userReadingPlan => userReadingPlan.user
    )
    userReadingPlans: UserReadingPlan[];

    @OneToMany(
        () => GroupReadingPlan,
        groupReadingPlan => groupReadingPlan.mentor
    )
    supervisedReadingPlanGroups: GroupReadingPlan[];

    @ManyToMany(
        () => GroupReadingPlan,
        groupReadingPlan => groupReadingPlan.users
    )
    readingPlanGroups: GroupReadingPlan[];

    @OneToMany(
        () => UserResetPassword,
        userResetPassword => userResetPassword.user
    )
    userResetPasswords: UserResetPassword[];

    @OneToMany(
        () => UserToken,
        userToken => userToken.user
    )
    userTokens: UserToken[];

    @OneToMany(
        () => UserVerseFav,
        userVerseFav => userVerseFav.user
    )
    userVerseFavs: UserVerseFav[];

    @OneToMany(
        () => ReadingPlanComment,
        comment => comment.author,
    )
    readingPlanComments: ReadingPlanComment[];

    @OneToMany(
        () => ReadingArchive,
        (archived: ReadingArchive) => archived.user,
    )
    readingArchive: ReadingArchive[];

    @OneToMany(
        () => ReadingArchive,
        (archived: ReadingArchive) => archived.mentor,
    )
    mentoredReadingArchive: ReadingArchive[];

    // custom
    static statuses = {
        not_active: 1,
        active: 2,
        deleted: 3,
    };

    static roles = {
        administrator: 'administrator',
        manager: 'manager',
        user: 'user',
    };

    static findByLogin = async (login: string): Promise<User> =>
        User.createQueryBuilder('user')
            .select('user')
            .where('user.username = :login', { login })
            .orWhere('user.email = :login', { login })
            .getOne();

    static async isNameUnique(username: string) {
        return !(await this.findOne({
            username,
        }));
    }

    static async generateUniqueName(seedName: string) {
        let username = seedName;
        let attempt = 1;

        while (!(await this.isNameUnique(username))) {
            username = seedName + attempt;
            attempt++;
        }

        return username;
    }
}
