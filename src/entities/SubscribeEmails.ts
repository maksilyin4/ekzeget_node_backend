import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('subscribe_emails', { schema: 'ekzeget' })
export default class SubscribeEmails extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'email', length: 255 })
    email: string;

    @Column('text', { name: 'sections' })
    sections: string;

    @Column('tinyint', {
        name: 'is_send',
        nullable: true,
        width: 1,
        default: () => "'0'",
    })
    isSend: boolean | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;
}
