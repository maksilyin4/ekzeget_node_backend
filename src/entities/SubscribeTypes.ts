import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import SubscribeUsers from './SubscribeUsers';

@Entity('subscribe_types', { schema: 'ekzeget' })
export default class SubscribeTypes extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'name', length: 255 })
    name: string;

    @Column('varchar', { name: 'slug', length: 255 })
    slug: string;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;

    @OneToMany(
        () => SubscribeUsers,
        subscribeUsers => subscribeUsers.subscribeType
    )
    subscribeUsers: SubscribeUsers[];
}
