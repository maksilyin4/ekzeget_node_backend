import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';

@Index('idx_user_reset_password_user', ['userId'], {})
@Entity('user_reset_password', { schema: 'ekzeget' })
export default class UserResetPassword extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'user_id', nullable: true })
    userId: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('varchar', { name: 'secret', nullable: true, length: 64 })
    secret: string | null;

    @ManyToOne(
        () => User,
        user => user.userResetPasswords,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;
}
