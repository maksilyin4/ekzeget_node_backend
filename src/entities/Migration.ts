import { BaseEntity, Column, Entity } from 'typeorm';

@Entity('migration', { schema: 'ekzeget' })
export default class Migration extends BaseEntity {
    @Column('varchar', { primary: true, name: 'version', length: 180 })
    version: string;

    @Column('int', { name: 'apply_time', nullable: true })
    applyTime: number | null;
}
