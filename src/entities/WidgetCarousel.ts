import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import WidgetCarouselItem from './WidgetCarouselItem';

@Entity('widget_carousel', { schema: 'ekzeget' })
export default class WidgetCarousel extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'key', length: 255 })
    key: string;

    @Column('smallint', { name: 'status', nullable: true, default: () => "'0'" })
    status: number | null;

    @OneToMany(
        () => WidgetCarouselItem,
        widgetCarouselItem => widgetCarouselItem.carousel
    )
    widgetCarouselItems: WidgetCarouselItem[];
}
