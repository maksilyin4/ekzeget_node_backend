import { BaseEntity, Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import I18nSourceMessage from './I18nSourceMessage';

@Entity('i18n_message', { schema: 'ekzeget' })
export default class I18nMessage extends BaseEntity {
    @Column('int', { primary: true, name: 'id' })
    id: number;

    @Column('varchar', { primary: true, name: 'language', length: 16 })
    language: string;

    @Column('text', { name: 'translation', nullable: true })
    translation: string | null;

    @ManyToOne(
        () => I18nSourceMessage,
        i18nSourceMessage => i18nSourceMessage.i18nMessages,
        { onDelete: 'CASCADE', onUpdate: 'NO ACTION' }
    )
    @JoinColumn([{ name: 'id', referencedColumnName: 'id' }])
    I18nSourceMessage: I18nSourceMessage;
}
