import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Motivator from './Motivator';

@Index('idx-motivator_images-motivator_id', ['motivatorId'], {})
@Entity('motivator_images', { schema: 'ekzeget' })
export default class MotivatorImages extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'motivator_id', nullable: true })
    motivatorId: number | null;

    @Column('varchar', { name: 'path', nullable: true, length: 255 })
    path: string | null;

    @Column('int', { name: 'width', nullable: true })
    width: number | null;

    @Column('int', { name: 'height', nullable: true })
    height: number | null;

    @Column('varchar', { name: 'type', nullable: true, length: 255 })
    type: string | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;

    @ManyToOne(
        () => Motivator,
        motivator => motivator.motivatorImages,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'motivator_id', referencedColumnName: 'id' }])
    motivator: Motivator;
}
