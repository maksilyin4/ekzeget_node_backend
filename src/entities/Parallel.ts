import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Verse from './Verse';

@Index('fk_parallel_verse_parent', ['parentVerseId'], {})
@Index('fk_parallel_verse', ['verseId'], {})
@Entity('parallel', { schema: 'ekzeget' })
export default class Parallel extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'parent_verse_id', nullable: true })
    parentVerseId: number | null;

    @Column('int', { name: 'verse_id', nullable: true })
    verseId: number | null;

    @ManyToOne(
        () => Verse,
        verse => verse.parallels,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;

    @ManyToOne(
        () => Verse,
        verse => verse.parallels2,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'parent_verse_id', referencedColumnName: 'id' }])
    parentVerse: Verse;

    static async deleteByParentAndVerseArray(parentVerseId, verseIds: number[]) {
        await this.createQueryBuilder()
            .delete()
            .where('parentVerseId = :parentVerseId', { parentVerseId })
            .andWhere('verseId IN (:...verseIds)', { verseIds })
            .execute();
    }

    static async insertManyByParentAndVerseArray(
        parentVerseId: number | undefined,
        verseIds: number[]
    ) {
        const lastId = await Verse.findOne({
            order: {
                id: 'DESC',
            },
        });

        parentVerseId = parentVerseId ? parentVerseId : lastId.id;

        const values: Partial<Parallel>[] = [];
        verseIds.forEach(item => {
            values.push({ parentVerseId: parentVerseId, verseId: item });
        });

        await this.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }

    static async deleteGroupByVerseId(id) {
        await this.createQueryBuilder('del')
            .delete()
            .where('parentVerseId = :id', { id })
            .execute();
    }
}
