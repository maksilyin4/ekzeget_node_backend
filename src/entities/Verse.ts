import {
    BaseEntity,
    Column,
    Entity,
    FindManyOptions,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    RelationId,
} from 'typeorm';
import BibleMapsVerse from './BibleMapsVerse';
import DictionaryVerse from './DictionaryVerse';
import InterpretationVerse from './InterpretationVerse';
import InterpretationVerseLink from './InterpretationVerseLink';
import MediaBible from './MediaBible';
import MotivatorVerse from './MotivatorVerse';
import Parallel from './Parallel';
import PreachingVerse from './PreachingVerse';
import QueezeQuestions from './QueezeQuestions';
import UserVerseFav from './UserVerseFav';
import Book from './Book';
import VerseTranslate from './VerseTranslate';
import { IVerseForMediaAndAll } from '../core/App/interfaces/IQuiz';
import { ParsedShortLink } from '../libs/VerseLinkParser/types';
import { VerseLinkParser } from '../libs/VerseLinkParser';
import { valueOrRangeToFindOperator } from '../libs/VerseLinkParser/valueOrRangeToFindOperator';

@Index('idx_chapter', ['chapter'], {})
@Index('fk_verse_book', ['book_id'], {})
@Index('idx-verse-chapter_id', ['chapter_id'], {})
@Entity('verse', { schema: 'ekzeget' })
export default class Verse extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'number', nullable: true })
    number: number | null;

    //@RelationId((verse: Verse) => verse.book)
    @Column('int', { name: 'book_id', nullable: true })
    book_id: number | null;

    @Column('text', { name: 'text', nullable: true })
    text: string | null;

    @Column('int', { name: 'chapter', nullable: true })
    chapter: number | null;

    @Column('int', { name: 'chapter_id' })
    chapter_id: number;

    @OneToMany(
        () => BibleMapsVerse,
        bibleMapsVerse => bibleMapsVerse.verse
    )
    bible_maps_verses: BibleMapsVerse[];

    @OneToMany(
        () => DictionaryVerse,
        dictionaryVerse => dictionaryVerse.verse
    )
    dictionary_verses: DictionaryVerse[];

    @OneToMany(
        () => InterpretationVerse,
        interpretationVerse => interpretationVerse.verse
    )
    interpretation_verses: InterpretationVerse[];

    @OneToMany(
        () => InterpretationVerseLink,
        interpretationVerseLink => interpretationVerseLink.verse
    )
    interpretation_verse_links: InterpretationVerseLink[];

    @OneToMany(
        () => MediaBible,
        mediaBible => mediaBible.verse
    )
    media_bibles: MediaBible[];

    @OneToMany(
        () => MotivatorVerse,
        motivatorVerse => motivatorVerse.verse
    )
    motivator_verses: MotivatorVerse[];

    @OneToMany(
        () => Parallel,
        parallel => parallel.verse
    )
    parallels: Parallel[];

    @OneToMany(
        () => Parallel,
        parallel => parallel.parentVerse
    )
    parallels2: Parallel[];

    @OneToMany(
        () => PreachingVerse,
        preachingVerse => preachingVerse.verse
    )
    preaching_verses: PreachingVerse[];

    @OneToMany(
        () => QueezeQuestions,
        queezeQuestions => queezeQuestions.verse
    )
    queeze_questions: QueezeQuestions[];

    @OneToMany(
        () => UserVerseFav,
        userVerseFav => userVerseFav.verse
    )
    user_verse_favs: UserVerseFav[];

    @ManyToOne(
        () => Book,
        book => book.verses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
    book: Book;

    @OneToMany(
        () => VerseTranslate,
        verseTranslate => verseTranslate.verse
    )
    verse_translates: VerseTranslate[];

    short_title?: string;

    static async getVerseWithBookInfo(verseId: number): Promise<IVerseForMediaAndAll> {
        const verse = await this.createQueryBuilder('verse')
            .select(
                'verse.id, verse.number, verse.book_id, verse.text, verse.chapter, verse.chapter_id, book.code AS book_code, book.short_title AS short'
            )
            .from(Book, 'book')
            .where('verse.id = (:vid)', { vid: verseId })
            .andWhere('book.id = verse.book_id')
            .getRawOne();

        verse.short += '. ' + verse.chapter + ':' + verse.number;
        return verse;
    }

    static async getByShortLink(
        verseShortLink: string,
        options?: FindManyOptions<Verse>
    ): Promise<Verse[]> {
        const parsed: ParsedShortLink[] = new VerseLinkParser().parseString(verseShortLink);
        if (!parsed.length) {
            return [];
        }

        const byBookPromises: Promise<Verse[]>[] = parsed.map(
            async ({ bookShortTitle, chapters }) => {
                const book = await Book.findOne({ where: { short_title: bookShortTitle } });

                if (!chapters.length) {
                    return Verse.find({
                        ...options,
                        where: {
                            book_id: book.id,
                        },
                        relations: ['book'],
                    });
                }

                const byChaptersPromises: Promise<Verse[]>[] = chapters.map(
                    async ({ chapterNumber, verses }) => {
                        if (!verses.length) {
                            return await Verse.find({
                                ...options,
                                where: {
                                    book_id: book.id,
                                    chapter: chapterNumber,
                                },
                                relations: ['book'],
                            });
                        }

                        const byVerses = await Promise.all(
                            verses.map(valueOrRange =>
                                Verse.find({
                                    ...options,
                                    where: {
                                        book_id: book.id,
                                        chapter: chapterNumber,
                                        number: valueOrRangeToFindOperator(valueOrRange),
                                    },
                                    relations: ['book'],
                                })
                            )
                        );

                        return byVerses.flat();
                    }
                );
                const byChapters = await Promise.all(byChaptersPromises);

                return byChapters.flat();
            }
        );

        const byBook = await Promise.all(byBookPromises);

        return byBook.flat();
    }

    static async getVersesByBookId(book_id: number) {
        return await Verse.createQueryBuilder('')
            .where('book_id = :book_id', { book_id })
            .getMany();
    }
}
