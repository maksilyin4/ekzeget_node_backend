import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';
import Verse from './Verse';

@Index('fk_user_verse_fav_user', ['userId'], {})
@Index('fk_user_verse_fav_verse', ['verseId'], {})
@Entity('user_verse_fav', { schema: 'ekzeget' })
export default class UserVerseFav extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'user_id', nullable: true })
    userId: number | null;

    @Column('int', { name: 'verse_id', nullable: true })
    verseId: number | null;

    @Column('varchar', { name: 'color', nullable: true, length: 255 })
    color: string | null;

    @Column('varchar', { name: 'tags', nullable: true, length: 255 })
    tags: string | null;

    @ManyToOne(
        () => User,
        user => user.userVerseFavs,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;

    @ManyToOne(
        () => Verse,
        verse => verse.user_verse_favs,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;

    static async getByUser({
        userId,
        tag,
        page,
        perPage,
    }: {
        userId: number;
        tag?: string;
        page: number;
        perPage: number;
    }): Promise<UserVerseFav[]> {
        let query = UserVerseFav.createQueryBuilder('fav')
            .select(['fav.id', 'fav.color', 'fav.tags'])
            .leftJoinAndSelect('fav.verse', 'verse')
            .leftJoinAndSelect('verse.book', 'book')
            .where('fav.userId = :userId', { userId });

        if (tag) {
            query = query.andWhere(`fav.tags LIKE :like`, { like: `%${tag}%` });
        }

        return query
            .skip(perPage * page)
            .take(perPage)
            .getMany();
    }

    static async getOneByUser({
        userId,
        verseId,
    }: {
        userId: number;
        verseId: number;
    }): Promise<UserVerseFav> {
        return UserVerseFav.createQueryBuilder('fav')
            .select('fav')
            .where('fav.userId = :userId', { userId })
            .andWhere('fav.verse_id = :verseId', { verseId })
            .getOne();
    }
}
