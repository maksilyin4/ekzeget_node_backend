import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import MediaLib from './MediaLib';

@Index('media_playlist-code-idx', ['code'], { unique: true })
@Entity('media_playlist', { schema: 'ekzeget' })
export default class MediaPlaylist extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('int', { name: 'century', nullable: true })
    century: number | null;

    @Column('int', { name: 'type_author', nullable: true })
    typeAuthor: number | null;

    @Column('int', { name: 'ganre_artwork', nullable: true })
    ganreArtwork: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('varchar', { name: 'artist', length: 255 })
    artist: string;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @OneToMany(
        () => MediaLib,
        mediaLib => mediaLib.playlist2
    )
    mediaLibs: MediaLib[];
}
