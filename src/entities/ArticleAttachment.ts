import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Article from './Article';

@Index('fk_article_attachment_article', ['article_id'], {})
@Entity('article_attachment', { schema: 'ekzeget' })
export default class ArticleAttachment extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'article_id' })
    article_id: number;

    @Column('varchar', { name: 'path', length: 255 })
    path: string;

    @Column('varchar', { name: 'base_url', nullable: true, length: 255 })
    base_url: string | null;

    @Column('varchar', { name: 'type', nullable: true, length: 255 })
    type: string | null;

    @Column('int', { name: 'size', nullable: true })
    size: number | null;

    @Column('varchar', { name: 'name', nullable: true, length: 255 })
    name: string | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'order', nullable: true })
    order: number | null;

    @ManyToOne(
        () => Article,
        article => article.article_attachments,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'article_id', referencedColumnName: 'id' }])
    article: Article;
}
