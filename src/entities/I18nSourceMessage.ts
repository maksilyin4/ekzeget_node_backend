import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import I18nMessage from './I18nMessage';

@Entity('i18n_source_message', { schema: 'ekzeget' })
export default class I18nSourceMessage extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'category', nullable: true, length: 32 })
    category: string | null;

    @Column('text', { name: 'message', nullable: true })
    message: string | null;

    @OneToMany(
        () => I18nMessage,
        i18nMessage => i18nMessage.I18nSourceMessage
    )
    i18nMessages: I18nMessage[];
}
