import { BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import Media from './Media';

@Entity('media_statistics', { schema: 'ekzeget' })
export default class MediaStatistics extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'media_id' })
    media_id: number;

    @Column()
    view_cnt: number = 0;

    @Column()
    download_read_cnt: number = 0;

    @Column()
    download_listen_cnt: number = 0;

    @OneToOne(
        () => Media,
        media => media.media_statistics,
        {
            onDelete: 'CASCADE',
            onUpdate: 'NO ACTION',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;

    static async findOrCreate(media_id: number): Promise<MediaStatistics> {
        let stat = await MediaStatistics.findOne(media_id);
        if (!stat) {
            stat = MediaStatistics.create({ media_id });
        }
        return stat;
    }
}
