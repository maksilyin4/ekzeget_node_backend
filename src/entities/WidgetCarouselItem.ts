import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import WidgetCarousel from './WidgetCarousel';

@Index('fk_item_carousel', ['carouselId'], {})
@Entity('widget_carousel_item', { schema: 'ekzeget' })
export default class WidgetCarouselItem extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'carousel_id' })
    carouselId: number;

    @Column('varchar', { name: 'base_url', nullable: true, length: 1024 })
    baseUrl: string | null;

    @Column('varchar', { name: 'path', nullable: true, length: 1024 })
    path: string | null;

    @Column('varchar', { name: 'type', nullable: true, length: 255 })
    type: string | null;

    @Column('varchar', { name: 'url', nullable: true, length: 1024 })
    url: string | null;

    @Column('varchar', { name: 'caption', nullable: true, length: 1024 })
    caption: string | null;

    @Column('smallint', { name: 'status', default: () => "'0'" })
    status: number;

    @Column('int', { name: 'order', nullable: true, default: () => "'0'" })
    order: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;

    @ManyToOne(
        () => WidgetCarousel,
        widgetCarousel => widgetCarousel.widgetCarouselItems,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'carousel_id', referencedColumnName: 'id' }])
    carousel: WidgetCarousel;
}
