import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn, RelationId,
} from 'typeorm';
import MetaSection from './MetaSection';

@Index('meta-section-fk', ['section_id'], {})
@Entity('meta', { schema: 'ekzeget' })
export default class Meta extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    //@RelationId((meta: Meta) => meta.section)
    @Column('int', { name: 'section_id', nullable: true, default: () => "'1'" })
    section_id: number | null;

    @Column('varchar', { name: 'category', length: 128 })
    category: string;

    @Column('text', { name: 'code' })
    code: string;

    @Column('varchar', { name: 'title', length: 512 })
    title: string;

    @Column('text', { name: 'description' })
    description: string;

    @Column('text', { name: 'h_one' })
    h_one: string;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updated_at: number | null;

    @Column('text', { name: 'keywords' })
    keywords: string;

    @ManyToOne(
        () => MetaSection,
        metaSection => metaSection.metas,
        {
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        }
    )
    @JoinColumn([{ name: 'section_id', referencedColumnName: 'id' }])
    section: MetaSection;
}
