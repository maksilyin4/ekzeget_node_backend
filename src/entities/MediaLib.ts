import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Book from './Book';
import MediaPlaylist from './MediaPlaylist';

@Index('media_lib-code-idx', ['code'], { unique: true })
@Index('idx_media_lib_active', ['active'], {})
@Index('idx_media_lib_type', ['type'], {})
@Index('idx_media_lib_chapter', ['chapter'], {})
@Index('fk_media_lib_book', ['bookId'], {})
@Index('fk_media_playlist', ['playlist'], {})
@Index('idx-media_lib-chapter_id', ['chapterId'], {})
@Entity('media_lib', { schema: 'ekzeget' })
export default class MediaLib extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'author', nullable: true, length: 255 })
    author: string | null;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('int', { name: 'type', nullable: true })
    type: number | null;

    @Column('varchar', { name: 'comment', nullable: true, length: 255 })
    comment: string | null;

    @Column('varchar', { name: 'path', nullable: true, length: 255 })
    path: string | null;

    @Column('int', { name: 'book_id', nullable: true })
    bookId: number | null;

    @Column('int', { name: 'chapter', nullable: true })
    chapter: number | null;

    @Column('varchar', { name: 'keywords', nullable: true, length: 255 })
    keywords: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('int', { name: 'playlist', nullable: true, default: () => "'0'" })
    playlist: number | null;

    @Column('int', { name: 'created_at', nullable: true, default: () => "'0'" })
    createdAt: number | null;

    @Column('varchar', { name: 'length', nullable: true, length: 64 })
    length: string | null;

    @Column('int', { name: 'chapter_id' })
    chapterId: number;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @ManyToOne(
        () => Book,
        book => book.media_libs,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
    book: Book;

    @ManyToOne(
        () => MediaPlaylist,
        mediaPlaylist => mediaPlaylist.mediaLibs,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'playlist', referencedColumnName: 'id' }])
    playlist2: MediaPlaylist;
}
