import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn, ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';
import EkzegetPerson from './EkzegetPerson';
import InterpretationVerse from './InterpretationVerse';
import InterpretationVerseLink from './InterpretationVerseLink';
import UserInterpretationFav from './UserInterpretationFav';
import getUnixSec from '../libs/helpers/getUnixSec';
import QueezeQuestions from "./QueezeQuestions";

@Index('idx_interpretation_active', ['active'], {})
@Index('idx_interpretation_investigated', ['investigated'], {})
@Index('fk_interpretation_exeget', ['ekzeget_id'], {})
@Index('fk_interpretation_added_at', ['added_at'], {})
@Index('fk_interpretation_edited_at', ['edited_at'], {})
@Index('comment_fulltext', ['comment'], { fulltext: true })
@Entity('interpretation', { schema: 'ekzeget' })
export default class Interpretation extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'ekzeget_id', nullable: true })
    ekzeget_id: number | null;

    @Column('mediumtext', { name: 'comment', nullable: true })
    comment: string | null;

    @Column('int', { name: 'added_by', nullable: true })
    added_by: number | null;

    @Column('int', { name: 'edited_by', nullable: true })
    edited_by: number | null;

    @Column('int', { name: 'added_at', nullable: true })
    added_at: number | null = getUnixSec();

    @Column('int', { name: 'edited_at', nullable: true })
    edited_at: number | null;

    @Column('int', { name: 'investigated', nullable: true })
    investigated: number | null = 0;

    @Column('int', { name: 'active', nullable: true })
    active: number | null = 0;

    @Column('text', { name: 'parsed_text', nullable: true })
    parsed_text: string | null;

    @ManyToOne(
        () => User,
        user => user.interpretations,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'added_by', referencedColumnName: 'id' }])
    added_by_user: User;

    @ManyToOne(
        () => User,
        user => user.interpretations2,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'edited_by', referencedColumnName: 'id' }])
    edited_by_user: User;

    @ManyToOne(
        () => EkzegetPerson,
        ekzegetPerson => ekzegetPerson.interpretations,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'ekzeget_id', referencedColumnName: 'id' }])
    ekzeget: EkzegetPerson;

    @OneToMany(
        () => InterpretationVerse,
        interpretationVerse => interpretationVerse.interpretation
    )
    interpretation_verses: InterpretationVerse[];

    @OneToMany(
        () => InterpretationVerseLink,
        interpretationVerseLink => interpretationVerseLink.interpretation
    )
    interpretation_verse_links: InterpretationVerseLink[];

    @OneToMany(
        () => UserInterpretationFav,
        userInterpretationFav => userInterpretationFav.interpretation
    )
    user_interpretation_favs: UserInterpretationFav[];

    @ManyToMany(
        () => QueezeQuestions,
        queezeQuestions => queezeQuestions.interpretations
    )
    quiz_questions: QueezeQuestions[];

    static getInterpretationVersesAndCount = async ({
        added_by,
        book_code,
        chapter,
        number,
        verse_id,
        ekzeget_id,
        page,
        perPage,
        sort,
    }: {
        added_by?: number;
        book_code?: string;
        chapter?: number;
        number?: number;
        verse_id?: number;
        ekzeget_id?: number;
        page?: number;
        perPage?: number;
        sort?: number;
    }): Promise<[Interpretation[], number]> => {
        let query = Interpretation.createQueryBuilder('i')
            .select('i')
            .leftJoinAndSelect('i.interpretation_verses', 'iv', 'iv.interpretation_id = i.id')
            .leftJoinAndSelect('iv.verse', 'v')
            .leftJoinAndSelect('v.book', 'b')
            .leftJoinAndSelect('v.dictionary_verses', 'dv')
            .leftJoinAndSelect('dv.dictionary', 'd')

            .leftJoinAndSelect('v.bible_maps_verses', 'bmv');

        if (verse_id) {
            query = query.andWhere('v.id = :verse_id', { verse_id });
        }

        if (book_code) {
            query = query.andWhere('b.code = :book_code', { book_code });

            if (chapter) {
                query = query.andWhere('v.chapter = :chapter', { chapter });

                if (number) {
                    query = query.andWhere('v.number = :number', { number });
                }
            }
        }

        if (ekzeget_id) {
            query = query
                .leftJoinAndSelect('i.ekzeget', 'e')
                .leftJoinAndSelect('e.ekzeget_type', 'et')
                .andWhere('i.ekzeget_id = :ekzeget_id', { ekzeget_id })
                .andWhere('i.active = 1');
        }

        if (page !== undefined && perPage) {
            query = query.limit(perPage).offset(perPage * page);
        }

        if (sort) {
            query = query.orderBy('i.added_at', 'DESC');
        }

        if (added_by) {
            query = query.andWhere('i.added_by = :added_by', { added_by });
        }

        // Запрос с минимум join-ов
        const [result, count] = await query.getManyAndCount();

        // Добавление недостающих полей
        const getUsersMap = async (
            interpretations: Interpretation[]
        ): Promise<Map<number, User>> => {
            const userIds = [
                ...new Set([
                    ...interpretations.map(i => i.added_by),
                    ...interpretations.map(i => i.edited_by),
                ]),
            ];

            const users = await User.createQueryBuilder('user')
                .leftJoinAndSelect('user.userProfile', 'userProfile')
                .whereInIds(userIds)
                .getMany();

            return new Map(
                users.map(u => {
                    return [u.id, u];
                })
            );
        };
        const usersMap = await getUsersMap(result);

        let ekzegetMap;
        if (!ekzeget_id) {
            const getEkzegetMap = async (
                interpretations: Interpretation[]
            ): Promise<Map<number, EkzegetPerson>> => {
                const ids = interpretations.map(i => i.ekzeget_id);

                const ekzegets = await EkzegetPerson.createQueryBuilder('e')
                    .leftJoinAndSelect('e.ekzeget_type', 'et')
                    .whereInIds(ids)
                    .getMany();

                return new Map(
                    ekzegets.map(ekzeget => {
                        return [ekzeget.id, ekzeget];
                    })
                );
            };
            ekzegetMap = await getEkzegetMap(result);
        }

        result.forEach(r => {
            r.added_by_user = usersMap.get(r.added_by);
            r.edited_by_user = usersMap.get(r.edited_by);
            if (ekzegetMap) r.ekzeget = ekzegetMap.get(r.ekzeget_id);
        });

        return [result, count];
    };
}
