import {
    BaseEntity,
    Column,
    Entity,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne, OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import ReadingPlan from './ReadingPlan';
import User from './User';
import Translate from './Translate';
import ReadingPlanComment from './ReadingPlanComment';
import UserReadingPlan from './UserReadingPlan';
import ReadingArchive from './ReadingArchive';

/**
 * Represents a group of users that read the bible with a plan and supervised by a mentor.
 */
// @Index('fk_user_reading_plan_user', ['user_id'], {})
// @Index('fk_user_reading_plan_plan', ['plan_id'], {})
// @Index('idx_user_reading_plan_s', ['subscribe'], {})
// @Index('idx_user_reading_plantranslate_id', ['translate_id'], {})
@Entity('group_reading_plan', { schema: 'ekzeget' })
export default class GroupReadingPlan extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'mentor_id', nullable: true })
    mentor_id: number | null;

    @Column('int', { name: 'plan_id', nullable: true })
    plan_id: number | null;

    @Column('date', { name: 'start_date', nullable: true })
    start_date: string | null;

    @Column('date', { name: 'stop_date', nullable: true })
    stop_date: string | null;

    @Column('text', { name: 'schedule', nullable: true })
    schedule: string | null;

    @Column('date', { name: 'check_date', nullable: true })
    check_date: string | null;

    @ManyToOne(
        () => ReadingPlan,
        readingPlan => readingPlan.groupReadingPlans,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        },
    )
    @JoinColumn([{ name: 'plan_id', referencedColumnName: 'id' }])
    plan: ReadingPlan;

    // creator of the group
    @ManyToOne(
        () => User,
        user => user.supervisedReadingPlanGroups,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        },
    )
    @JoinColumn([{ name: 'mentor_id', referencedColumnName: 'id' }])
    mentor: User;

    @OneToMany(
        () => UserReadingPlan,
        userPlan => userPlan.group,
    )
    userPlans: UserReadingPlan[];

    @ManyToMany(
        () => User,
        user => user.readingPlanGroups,
    )
    @JoinTable({
        name: 'user_reading_plan',
        joinColumns: [{ name: 'group_id', referencedColumnName: 'id' }],
        inverseJoinColumns: [{ name: 'user_id', referencedColumnName: 'id' }],
        schema: 'ekzeget',
    })
    users: User[];

    @OneToMany(
        () => ReadingPlanComment,
        comment => comment.group,
    )
    comments: ReadingPlanComment;
}
