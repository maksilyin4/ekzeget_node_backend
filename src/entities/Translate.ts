import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import UserReadingPlan from './UserReadingPlan';
import VerseTranslate from './VerseTranslate';
import GroupReadingPlan from './GroupReadingPlan';

@Index('code', ['code'], { unique: true })
@Entity('translate', { schema: 'ekzeget' })
export default class Translate extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'number' })
    number: number;

    @Column('varchar', { name: 'code', unique: true, length: 32 })
    code: string;

    @Column('char', { name: 'title', length: 100 })
    title: string;

    @Column('char', { name: 'select', length: 100 })
    select: string;

    @Column('char', { name: 'search', length: 100 })
    search: string;

    @OneToMany(
        () => UserReadingPlan,
        userReadingPlan => userReadingPlan.translate
    )
    userReadingPlans: UserReadingPlan[];

    @OneToMany(
        () => VerseTranslate,
        verseTranslate => verseTranslate.code2
    )
    verseTranslates: VerseTranslate[];
}
