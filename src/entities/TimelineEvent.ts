import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';
import getUnixSec from '../libs/helpers/getUnixSec';
import Interpretation from './Interpretation';
import User from './User';
import Verse from './Verse';
import InterpretationVerse from './InterpretationVerse';
import EkzegetPerson from './EkzegetPerson';

@Index('idx_created_at', ['createdAt'], {})
@Entity('timeline_event', { schema: 'ekzeget' })
export default class TimelineEvent extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'application', length: 64 })
    application: string;

    @Column('varchar', { name: 'category', length: 64 })
    category: string;

    @Column('varchar', { name: 'event', length: 64 })
    event: string;

    @Column('text', { name: 'data', nullable: true })
    data: string | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null = getUnixSec();

    static async getDataByInterpretation(
        interpretation: Pick<Interpretation, 'id' | 'ekzeget_id' | 'added_at'>,
        user: Pick<User, 'id' | 'username'>
    ): Promise<IInterpretationData> {
        const verses = await this.getVerses(interpretation.id);

        return {
            public_identity: user.username,
            user_id: user.id,
            created_at: interpretation.added_at,
            interpretation_id: interpretation.id,
            verses: verses,
            ekzeget: {
                id: interpretation.ekzeget_id,
                name: (
                    await EkzegetPerson.findOne({
                        select: ['name'],
                        where: { id: interpretation.ekzeget_id },
                    })
                ).name,
            },
        };
    }

    private static async getVerses(interpretationId: number) {
        const verse_ids = await InterpretationVerse.find({
            select: ['verse_id'],
            where: { interpretation_id: interpretationId },
        });

        const verses = await Promise.all(
            verse_ids.map(async verse => {
                const verseWithBookInfo = await Verse.getVerseWithBookInfo(verse.verse_id);
                return { id: verseWithBookInfo.id, short: verseWithBookInfo.short };
            })
        );

        return verses;
    }
}

export interface IInterpretationData {
    public_identity: string;
    user_id: number;
    created_at: number;
    interpretation_id: number;
    verses: Array<{ id: number; short: string }>;
    ekzeget: { id: number; name: string };
}
