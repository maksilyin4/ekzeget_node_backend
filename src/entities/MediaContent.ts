import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Media from './Media';
import User from './User';
import MediaContentFiles from './MediaContentFiles';

@Index('media_content_to_user', ['createdBy'], {})
@Index('media_content_to_type', ['typeId'], {})
@Index('idx_media_content_active', ['active'], {})
@Index('fk_media_content_to_media', ['mediaId'], {})
@Entity('media_content', { schema: 'ekzeget' })
export default class MediaContent extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('int', { name: 'type_id', nullable: true })
    typeId: number | null;

    @Column('varchar', { name: 'time', nullable: true, length: 255 })
    time: string | null;

    @Column('int', { name: 'created_by', nullable: true })
    createdBy: number | null;

    @Column('int', { name: 'edited_by', nullable: true })
    editedBy: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'edited_at', nullable: true })
    editedAt: number | null;

    @Column('int', { name: 'active', nullable: true, default: () => "'1'" })
    active: number | null;

    @Column('int', { name: 'media_id', nullable: true })
    mediaId: number | null;

    @ManyToOne(
        () => Media,
        media => media.mediaContents,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;

    @ManyToOne(
        () => User,
        user => user.mediaContents,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'created_by', referencedColumnName: 'id' }])
    createdBy2: User;

    @OneToMany(
        () => MediaContentFiles,
        mediaContentFiles => mediaContentFiles.content
    )
    mediaContentFiles: MediaContentFiles[];
}
