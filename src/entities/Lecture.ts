import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn, RelationId,
} from 'typeorm';
import LectureType from './LectureType';

@Index('lecture-code-idx', ['code'], { unique: true })
@Index('idx_lecture_active', ['active'], {})
@Index('fk_lecture_type', ['type_id'], {})
@Entity('lecture', { schema: 'ekzeget' })
export default class Lecture extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('mediumtext', { name: 'text', nullable: true })
    text: string | null;

    //@RelationId((lecture: Lecture) => lecture.type)
    @Column('int', { name: 'type_id', nullable: true })
    type_id: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('int', { name: 'added_at', nullable: true })
    added_at: number | null;

    @Column('int', { name: 'edited_at', nullable: true })
    edited_at: number | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @ManyToOne(
        () => LectureType,
        lectureType => lectureType.lectures,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'type_id', referencedColumnName: 'id' }])
    type: LectureType;
}
