import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('floating_chten', { schema: 'ekzeget' })
export default class FloatingReading extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('text', { name: 'name' })
    name: string;

    @Column('text', { name: 'apostol_chten' })
    apostol_chten: string;

    @Column('text', { name: 'evang_chten' })
    evang_chten: string;

    @Column('text', { name: 'dop_chten' })
    dop_chten: string;

    @Column('text', { name: 'utr_chten' })
    utr_chten: string;

    @Column('int', { name: 'exist_mineja', default: () => "'1'" })
    exist_mineja: number;

    @Column('int', { name: 'exist_common_chten', default: () => "'0'" })
    exist_common_chten: number;

    @Column('int', { name: 'location_common_chten', default: () => "'0'" })
    location_common_chten: number;

    @Column('text', { name: 'color' })
    color: string;
}
