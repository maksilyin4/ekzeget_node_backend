import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Dictionary from './Dictionary';

@Index('dictionary_type-code-idx', ['code'], { unique: true })
@Index('idx_dictionary_type_active', ['active'], {})
@Entity('dictionary_type', { schema: 'ekzeget' })
export default class DictionaryType extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('varchar', { name: 'alias', nullable: true, length: 255 })
    alias: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @OneToMany(
        () => Dictionary,
        dictionary => dictionary.type
    )
    dictionaries: Dictionary[];
}
