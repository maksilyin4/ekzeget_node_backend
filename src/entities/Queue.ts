import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('channel', ['channel'], {})
@Index('reserved_at', ['reservedAt'], {})
@Index('priority', ['priority'], {})
@Entity('queue', { schema: 'ekzeget' })
export default class Queue extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'channel', length: 255 })
    channel: string;

    @Column('blob', { name: 'job' })
    job: Buffer;

    @Column('int', { name: 'pushed_at' })
    pushedAt: number;

    @Column('int', { name: 'ttr' })
    ttr: number;

    @Column('int', { name: 'delay', default: () => "'0'" })
    delay: number;

    @Column('int', { name: 'priority', unsigned: true, default: () => "'1024'" })
    priority: number;

    @Column('int', { name: 'reserved_at', nullable: true })
    reservedAt: number | null;

    @Column('int', { name: 'attempt', nullable: true })
    attempt: number | null;

    @Column('int', { name: 'done_at', nullable: true })
    doneAt: number | null;
}
