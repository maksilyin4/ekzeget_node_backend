import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';

@Index('idx_token', ['token'], {})
@Index('idx_device_hash', ['device_hash'], {})
@Index('idx_user_token_id', ['user_id'], {})
@Entity('user_token', { schema: 'ekzeget' })
export default class UserToken extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'user_id' })
    user_id: number;

    @Column('varchar', { name: 'type', length: 255 })
    type: string;

    @Column('varchar', { name: 'token', length: 40 })
    token: string;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updated_at: number | null;

    @Column('varchar', { name: 'device_info', length: 255 })
    device_info: string;

    @Column('varchar', { name: 'device_hash', length: 128 })
    device_hash: string;

    @ManyToOne(
        () => User,
        user => user.userTokens,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;

    static types = {
        reg: 'reg',
        web: 'web',
    };
}
