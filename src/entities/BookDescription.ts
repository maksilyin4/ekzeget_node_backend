import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    RelationId,
} from 'typeorm';
import Book from './Book';
import BookDescriptor from './BookDescriptor';

@Index('idx_book_description_active', ['active'], {})
@Index('fk_book_description_book', ['book_id'], {})
@Index('fk_book_description_descriptor', ['descriptor_id'], {})
@Entity('book_description', { schema: 'ekzeget' })
export default class BookDescription extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    //@RelationId((bookDescription: BookDescription) => bookDescription.book)
    @Column('int', { name: 'book_id', nullable: true })
    book_id: number | null;

    //@RelationId((bookDescription: BookDescription) => bookDescription.descriptor)
    @Column('int', { name: 'descriptor_id', nullable: true })
    descriptor_id: number | null;

    @Column('mediumtext', { name: 'text', nullable: true })
    text: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @ManyToOne(
        () => Book,
        book => book.book_descriptions,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
    book: Book;

    @ManyToOne(
        () => BookDescriptor,
        bookDescriptor => bookDescriptor.book_descriptions,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'descriptor_id', referencedColumnName: 'id' }])
    descriptor: BookDescriptor;
}
