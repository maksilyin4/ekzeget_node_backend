import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Lecture from './Lecture';

@Index('lecture_type-code-idx', ['code'], { unique: true })
@Index('idx_lecture_type_active', ['active'], {})
@Entity('lecture_type', { schema: 'ekzeget' })
export default class LectureType extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @OneToMany(
        () => Lecture,
        lecture => lecture.type
    )
    lectures: Lecture[];
}
