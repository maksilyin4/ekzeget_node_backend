import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import MotivatorImages from './MotivatorImages';
import MotivatorVerse from './MotivatorVerse';

@Entity('motivator', { schema: 'ekzeget' })
export default class Motivator extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('smallint', { name: 'status', default: () => "'0'" })
    status: number;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;

    @OneToMany(
        () => MotivatorImages,
        motivatorImages => motivatorImages.motivator
    )
    motivatorImages: MotivatorImages[];

    @OneToMany(
        () => MotivatorVerse,
        motivatorVerse => motivatorVerse.motivator
    )
    motivatorVerses: MotivatorVerse[];
}
