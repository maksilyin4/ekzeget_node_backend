import {
    BaseEntity,
    Brackets,
    Column,
    Entity,
    Index, JoinColumn, ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    SelectQueryBuilder,
} from 'typeorm';
import { nanoid } from 'nanoid';
import { TQuizCriteria, TQuizStatus } from '../core/App/interfaces/IQuiz';
import QuizSubmission from './QuizSubmission';
import Plan from './Plan';

@Index('queeze_applications-code-idx', ['code'], { unique: true })
@Index('user_id', ['userId'], {})
@Index('status', ['status'], {})
@Entity('queeze_applications', { schema: 'ekzeget' })
export default class QueezeApplications extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id', unsigned: true })
    id: number;

    @Column('int', { name: 'user_id', nullable: true })
    userId: number | null;

    @Column('int', { name: 'plan_id', nullable: true })
    planId: number | null;

    @Column('varchar', { name: 'status', length: 1 })
    status: TQuizStatus;

    @Column('datetime', {
        name: 'created_at',
        default: () => 'CURRENT_TIMESTAMP',
    })
    createdAt: Date;

    @Column('datetime', { name: 'updated_at', nullable: true })
    updatedAt: Date | null;

    @Column('varchar', { name: 'code', nullable: true, unique: true, length: 10 })
    code: string | null;

    @Column('json', { name: 'criteria' })
    criteria: TQuizCriteria;

    @ManyToOne(
        () => Plan,
        plan => plan.quizApplicaitons,
    )
    @JoinColumn({ name: 'plan_id', referencedColumnName: 'id' })
    plan: Plan;

    @OneToMany(
        () => QuizSubmission,
        submission => submission.application,
    )
    submissions: QuizSubmission[];

    get description(): string {
        if (this.criteria.types.includes('I')) {
            if (this.criteria.types.includes('T')) {
                return 'По тексту и толкованиям';
            } else {
                return 'По толкованиям';
            }
        }
        return 'По тексту';
    }

    /**
     * Создает запись с новой викториной в базе,
     * возвращает уникальный код и id викторины
     * @return Promise<[number, string]
     */
    static async setNewQuiz(
        userId?: number,
        criteria?: TQuizCriteria,
        planId?: number,
    ): Promise<[number, string, QueezeApplications['planId']]> {
        let code: string = null;

        while (!code) {
            code = nanoid(8);
            const checkOnAvalible = await this.getQuizByCode(code);
            if (checkOnAvalible !== undefined) {
                code = null;
            }
        }
        await this.createQueryBuilder('quiz')
            .insert()
            .values({ status: 'R', updatedAt: null, code: code, userId, criteria, planId })
            .execute();

        const quizId = await this.getQuizByCode(code);

        return [quizId.id, code, quizId.planId];
    }

    static async getQuizByCode(code: string): Promise<QueezeApplications> {
        const quiz = await this.createQueryBuilder('quiz')
            .where('quiz.code = :code', { code })
            .getOne();
        return quiz;
    }

    static async getQuizById(id: number): Promise<QueezeApplications> {
        const quiz = await this.createQueryBuilder('quiz')
            .where('quiz.id = :id', { id })
            .getOne();
        return quiz;
    }

    static async changeQuizStatus(id: number, status: TQuizStatus) {
        await this.createQueryBuilder('quiz')
            .update()
            .set({
                status: status,
            })
            .where('id = :id', { id: id })
            .execute();
    }

    static async getLastActiveQuiz(codes?: string[], userId?: number): Promise<QueezeApplications> {
        return this.prepareUserQuizzesQuery(codes, userId)
            ?.andWhere({ status: 'R' })
            .getOne();
    }

    static async getUserQuizzes(codes?: string[], userId?: number, planId?: number): Promise<QueezeApplications[]> {
        let query = this.prepareUserQuizzesQuery(codes, userId);
        if (planId)
            query = query?.andWhere('plan_id = :planId', { planId });
        return query?.getMany();
    }

    static async closeAllUserQuizzes(codes?: string[], userId?: number): Promise<void> {
        await this.prepareUserQuizzesQuery(codes, userId)
            ?.andWhere('status = "R"')
            .update()
            .set({ status: 'P' })
            .execute();
    }

    private static prepareUserQuizzesQuery(
        codes?: string[],
        userId?: number
    ): SelectQueryBuilder<QueezeApplications> | null {
        const where: string[] = [];
        if (codes && codes.length) {
            where.push('code in (:...codes)');
        }
        if (!isNaN(userId)) {
            where.push('user_id = :userId');
        }

        if (!where.length) {
            return null;
        }

        return this.createQueryBuilder().where(
            new Brackets(qb => qb.where(where.join(' OR '), { userId, codes }))
        );
    }
}
