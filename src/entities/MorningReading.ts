import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Media from './Media';

@Entity('morning_reading', { schema: 'ekzeget' })
export default class MorningReading extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'dni' })
    dni: number;

    @Column('char', { name: 'utr', length: 50 })
    utr: string;

    @OneToMany(
        () => Media,
        media => media.morningReading
    )
    media: Media;

    get title() {
        return `${this.dni} (${this.utr})`;
    }
}
