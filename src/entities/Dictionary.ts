import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn, RelationId,
} from 'typeorm';
import DictionaryType from './DictionaryType';
import DictionaryVerse from './DictionaryVerse';

@Index('fk_dictionary_type', ['type_id'], {})
@Index('dictionary-code-idx', ['code'], { unique: true })
@Index('idx_dictionary_active', ['active'], {})
@Index('idx_dictionary_letter', ['letter'], {})
@Index('idx_dictionary_word', ['word'], {})
@Entity('dictionary', { schema: 'ekzeget' })
export default class Dictionary extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'type_id', nullable: true })
    //@RelationId((dict: Dictionary) => dict.type)
    type_id: number | null;

    @Column('varchar', { name: 'word', nullable: true, length: 255 })
    word: string | null;

    @Column('varchar', { name: 'letter', nullable: true, length: 255 })
    letter: string | null;

    @Column('mediumtext', { name: 'description', nullable: true })
    description: string | null;

    @Column('text', { name: 'variant', nullable: true })
    variant: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @ManyToOne(
        () => DictionaryType,
        dictionaryType => dictionaryType.dictionaries,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'type_id', referencedColumnName: 'id' }])
    type: DictionaryType;

    @OneToMany(
        () => DictionaryVerse,
        dictionaryVerse => dictionaryVerse.dictionary
    )
    dictionary_verses: DictionaryVerse[];
}
