import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';

@Index('idx_user_bookmark_user', ['userId'], {})
@Entity('user_bookmark', { schema: 'ekzeget' })
export default class UserBookmark extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'user_id', nullable: true })
    userId: number | null;

    @Column('varchar', { name: 'red', nullable: true, length: 255 })
    red: string | null;

    @Column('varchar', { name: 'orange', nullable: true, length: 255 })
    orange: string | null;

    @Column('varchar', { name: 'green', nullable: true, length: 255 })
    green: string | null;

    @Column('varchar', { name: 'blue', nullable: true, length: 255 })
    blue: string | null;

    @Column('varchar', { name: 'fuchsia', nullable: true, length: 255 })
    fuchsia: string | null;

    @ManyToOne(
        () => User,
        user => user.userBookmarks,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;
}
