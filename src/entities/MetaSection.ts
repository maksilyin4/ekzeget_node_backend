import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Meta from './Meta';

@Entity('meta_section', { schema: 'ekzeget' })
export default class MetaSection extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'entity', nullable: true, length: 255 })
    entity: string | null;

    @Column('varchar', { name: 'name', length: 255 })
    name: string;

    @Column('datetime', {
        name: 'created_at',
        nullable: true,
        default: () => 'CURRENT_TIMESTAMP',
    })
    createdAt: Date | null;

    @Column('datetime', {
        name: 'updated_at',
        nullable: true,
        default: () => 'CURRENT_TIMESTAMP',
    })
    updatedAt: Date | null;

    @Column('varchar', { name: 'title', nullable: true, length: 512 })
    title: string | null;

    @Column('varchar', { name: 'h_one', nullable: true, length: 128 })
    hOne: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('text', { name: 'keywords', nullable: true })
    keywords: string | null;

    @OneToMany(
        () => Meta,
        meta => meta.section
    )
    metas: Meta[];
}
