import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Media from './Media';

@Index('day', ['day'], { unique: true })
@Index('active33', ['active'], {})
@Entity('post_reading', { schema: 'ekzeget' })
export default class PostReading extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'day', unique: true })
    day: number;

    @Column('text', { name: 'title' })
    title: string;

    @Column('text', { name: 'apostle' })
    apostle: string;

    @Column('text', { name: 'gospel' })
    gospel: string;

    @Column('text', { name: 'vz' })
    vz: string;

    @Column('text', { name: 'morning' })
    morning: string;

    @Column('int', { name: 'active', default: () => "'1'" })
    active: number;

    @OneToMany(
        () => Media,
        media => media.postReading
    )
    media: Media;
}
