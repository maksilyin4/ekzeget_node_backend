import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn, RelationId,
} from 'typeorm';
import Media from './Media';
import FileStorageItem from './FileStorageItem';
import User from './User';

@Index('media_genre-code-idx', ['code'], { unique: true })
@Index('media_genre_user', ['created_by'], {})
@Index('fk_media_media_genre_image', ['image'], {})
@Index('idx_media_genre_active', ['active'], {})
@Entity('media_genre', { schema: 'ekzeget' })
export default class MediaGenre extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('int', { name: 'image', nullable: true })
    image: number | null;

    //@RelationId((genre: MediaGenre) => genre.createdBy2)
    @Column('int', { name: 'created_by', nullable: true })
    created_by: number | null;

    @Column('int', { name: 'edited_by', nullable: true })
    edited_by: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'edited_at', nullable: true })
    edited_at: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @OneToMany(
        () => Media,
        media => media.genre
    )
    media: Media[];

    @ManyToOne(
        () => FileStorageItem,
        fileStorageItem => fileStorageItem.media_genres,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'image', referencedColumnName: 'id' }])
    image2: FileStorageItem;

    @ManyToOne(
        () => User,
        user => user.mediaGenres,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'created_by', referencedColumnName: 'id' }])
    createdBy2: User;
}
