import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Dictionary from './Dictionary';
import Verse from './Verse';

@Index('idx-dictionary_verse-dictionary_id', ['dictionaryId'], {})
@Index('idx-dictionary_verse-verse_id', ['verseId'], {})
@Entity('dictionary_verse', { schema: 'ekzeget' })
export default class DictionaryVerse extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'dictionary_id', nullable: true })
    dictionaryId: number | null;

    @Column('int', { name: 'verse_id', nullable: true })
    verseId: number | null;

    @ManyToOne(
        () => Dictionary,
        dictionary => dictionary.dictionary_verses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'dictionary_id', referencedColumnName: 'id' }])
    dictionary: Dictionary;

    @ManyToOne(
        () => Verse,
        verse => verse.dictionary_verses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;

    static async getRelationDictionaryArticlesForAdminBroCustomComponents(verseId: number) {
        const relationDictionaryArticles: Pick<
            Dictionary,
            'id' | 'word'
        >[] = await Dictionary.createQueryBuilder('dict')
            .select('dict.id, dict.word')
            .where(db => {
                const subQuery = db
                    .subQuery()
                    .select('dictionary_id')
                    .from(DictionaryVerse, 'dictVerse')
                    .where('verse_id = :verseId', { verseId })
                    .getQuery();
                return 'dict.id IN' + subQuery;
            })
            .getRawMany();
        return relationDictionaryArticles;
    }

    static async deleteByVerseIdAndDictionaryIdArray(verseId, dictionaryIds: number[]) {
        await this.createQueryBuilder()
            .delete()
            .where('verseId = :verseId', { verseId })
            .andWhere('dictionaryId IN (:...dictionaryIds)', { dictionaryIds })
            .execute();
    }

    static async insertManyByVerseIdAndDictionaryIdArray(
        verseId: number | undefined,
        dictionaryIds: number[]
    ) {
        const lastId = await Verse.findOne({
            order: {
                id: 'DESC',
            },
        });

        verseId = verseId ? verseId : lastId.id;

        const values: Array<Partial<DictionaryVerse>> = [];
        dictionaryIds.forEach(item => {
            values.push({ verseId: verseId, dictionaryId: item });
        });

        await this.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }

    static async deleteGroupByVerseId(id) {
        await this.createQueryBuilder('del')
            .delete()
            .where('verseId = :id', { id })
            .execute();
    }
}
