import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import BookDescription from './BookDescription';

@Index('idx_book_descriptor_active', ['active'], {})
@Entity('book_descriptor', { schema: 'ekzeget' })
export default class BookDescriptor extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @OneToMany(
        () => BookDescription,
        bookDescription => bookDescription.descriptor
    )
    book_descriptions: BookDescription[];
}
