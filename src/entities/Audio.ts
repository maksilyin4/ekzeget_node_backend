import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinTable,
    ManyToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Chapters from './Chapters';

@Index('code', ['src'], { unique: true })
@Index('idx-audio-code', ['src'], {})
@Index('idx-chapter_id', ['chapterId'], {})
@Entity('audio', { schema: 'ekzeget' })
export default class Audio extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'src', unique: true, length: 100 })
    src: string;

    @Column('double', { name: 'duration', precision: 22 })
    duration: number;

    @Column('int', { name: 'chapter_id' })
    chapterId: number;

    @ManyToMany(
        () => Chapters,
        chapters => chapters.audio
    )
    @JoinTable({
        name: 'chapters_to_audios',
        joinColumns: [{ name: 'audio_id', referencedColumnName: 'id' }],
        inverseJoinColumns: [{ name: 'chapter_id', referencedColumnName: 'id' }],
        schema: 'ekzeget',
    })
    chapters: Chapters[];
}
