import {
    BaseEntity,
    BeforeInsert,
    BeforeUpdate,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
    RelationId,
} from 'typeorm';
import MediaCategory from './MediaCategory';
import MediaAuthor from './MediaAuthor';
import MediaGenre from './MediaGenre';
import FileStorageItem from './FileStorageItem';
import User from './User';
import MediaBible from './MediaBible';
import MediaComment from './MediaComment';
import MediaContent from './MediaContent';
import MediaContentFiles from './MediaContentFiles';
import MediaFtpProcessing from './MediaFtpProcessing';
import MediaRelated from './MediaRelated';
import MediaRelatedAudioArchives from './MediaRelatedAudioArchives';
import MediaRelatedFb2Book from './MediaRelatedFb2Book';
import MediaStatistics from './MediaStatistics';
import MediaSubscribe from './MediaSubscribe';
import MediaTags from './MediaTags';
import QueezeQuestions from './QueezeQuestions';
import QueezeAnswers from './QueezeAnswers';
import Celebration from './Celebration';
import ApostolicReading from './ApostolicReading';
import GospelReading from './GospelReading';
import PostReading from './PostReading';
import MorningReading from './MorningReading';
import Mineja from './Mineja';
import { transliterate } from 'transliteration';
import latinToCyrillic from 'latin-to-cyrillic';

@Index('media-code-idx', ['code'], { unique: true })
@Index('media_item_user', ['created_by'], {})
@Index('media_item_media_genre', ['genre_id'], {})
@Index('media_item_media_image', ['image_id'], {})
@Index('media_item_media_author', ['author_id'], {})
@Index('idx_media_item_active', ['active'], {})
@Index('media-type-idx', ['type'], {})
@Index('media-template-idx', ['template'], {})
@Index('media-sort-idx', ['sort'], {})
@Index('media-is_primary-idx', ['is_primary'], {})
@Index('fk_main_category', ['main_category'], {})
@Index('media_daily_media_idx', ['daily_media'], { unique: true })
@Index('media_reading_apostolic_id_idx', ['reading_apostolic_id'], {})
@Index('media_reading_gospel_id_idx', ['reading_gospel_id'], {})
@Index('media_reading_post_id_idx', ['reading_post_id'], {})
@Index('media_reading_morning_id_idx', ['reading_morning_id'], {})
@Index('media_mineja_id_idx', ['mineja_id'], {})
@Entity('media', { schema: 'ekzeget' })
export default class Media extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('varchar', { name: 'title_for_sort', nullable: true, length: 255 })
    title_for_sort: string;

    @Column('varchar', { name: 'social_description', length: '200', nullable: true })
    social_description: string | null;

    //@RelationId((media: Media) => media.author)
    @Column('int', { name: 'author_id', nullable: true })
    author_id: number | null;

    //@RelationId((media: Media) => media.genre)
    @Column('int', { name: 'genre_id', nullable: true })
    genre_id: number | null;

    @Column('int', { name: 'image_id', nullable: true })
    image_id: number | null;

    @Column('int', { name: 'created_by', nullable: true })
    created_by: number | null;

    @Column('int', { name: 'edited_by', nullable: true })
    edited_by: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'edited_at', nullable: true })
    edited_at: number | null;

    @Column('int', {
        name: 'allow_comment',
        nullable: true,
        default: () => "'1'",
    })
    allow_comment: number | null;

    @Column('int', { name: 'active', nullable: true, default: () => "'1'" })
    active: number | null;

    @Column('varchar', { name: 'year', nullable: true, length: 255 })
    year: string | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @Column('varchar', { name: 'type', nullable: true, length: 2 })
    type: string | null;

    @Column('longtext', { name: 'text', nullable: true })
    text: string | null;

    @Column('varchar', { name: 'location', nullable: true, length: 255 })
    location: string | null;

    @Column('varchar', { name: 'era', nullable: true, length: 255 })
    era: string | null;

    @Column('text', { name: 'properties', nullable: true })
    properties: string | null;

    @Column('varchar', { name: 'grouping_name', nullable: true, length: 255, default: null })
    grouping_name: string | null;

    @Column('varchar', {
        name: 'template',
        nullable: true,
        length: 2,
        default: () => "'M'",
    })
    template: string | null;

    @Column('int', { name: 'sort', nullable: true, default: () => "'200'" })
    sort: number | null;

    @Column('smallint', {
        name: 'is_primary',
        nullable: true,
        default: () => "'1'",
    })
    is_primary: number | null;

    @Column('varchar', { name: 'art_title', nullable: true, length: 255 })
    art_title: string | null;

    @Column('varchar', { name: 'art_created_date', nullable: true, length: 255 })
    art_created_date: string | null;

    @Column('varchar', { name: 'art_director', nullable: true, length: 255 })
    art_director: string | null;

    @Column('varchar', { name: 'length', nullable: true, length: 255 })
    length: string | null;

    // @RelationId((media: Media) => media.main_category_category)
    @Column()
    main_category: number;

    @ManyToOne(
        () => MediaCategory,
        mediaCategory => mediaCategory.media,
        {
            onDelete: 'NO ACTION',
            onUpdate: 'NO ACTION',
        }
    )
    @JoinColumn([{ name: 'main_category', referencedColumnName: 'id' }])
    main_category_category: MediaCategory;

    @ManyToOne(
        () => MediaAuthor,
        mediaAuthor => mediaAuthor.media,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'author_id', referencedColumnName: 'id' }])
    author: MediaAuthor;

    @ManyToOne(
        () => MediaGenre,
        mediaGenre => mediaGenre.media,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'genre_id', referencedColumnName: 'id' }])
    genre: MediaGenre;

    @ManyToOne(
        () => FileStorageItem,
        fileStorageItem => fileStorageItem.media,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'image_id', referencedColumnName: 'id' }])
    image: FileStorageItem;

    @ManyToOne(
        () => User,
        user => user.media,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'created_by', referencedColumnName: 'id' }])
    createdBy2: User;

    @OneToMany(
        () => MediaBible,
        mediaBible => mediaBible.media
    )
    media_bibles: MediaBible[];

    @ManyToMany(
        () => MediaCategory,
        mediaCategory => mediaCategory.media2
    )
    mediaCategories: MediaCategory[];

    @OneToMany(
        () => MediaComment,
        mediaComment => mediaComment.media
    )
    mediaComments: MediaComment[];

    @OneToMany(
        () => MediaContent,
        mediaContent => mediaContent.media
    )
    mediaContents: MediaContent[];

    @OneToMany(
        () => MediaContentFiles,
        mediaContentFiles => mediaContentFiles.media
    )
    mediaContentFiles: MediaContentFiles[];

    @OneToMany(
        () => MediaFtpProcessing,
        mediaFtpProcessing => mediaFtpProcessing.media
    )
    mediaFtpProcessings: MediaFtpProcessing[];

    @OneToMany(
        () => MediaRelated,
        mediaRelated => mediaRelated.related
    )
    mediaRelateds: MediaRelated[];

    @OneToMany(
        () => MediaRelated,
        mediaRelated => mediaRelated.media
    )
    mediaRelateds2: MediaRelated[];

    @OneToOne(
        () => MediaRelatedAudioArchives,
        mediaRelatedAudioArchives => mediaRelatedAudioArchives.media
    )
    mediaRelatedAudioArchives: MediaRelatedAudioArchives;

    @OneToOne(
        () => MediaRelatedFb2Book,
        mediaRelatedFb2Book => mediaRelatedFb2Book.media
    )
    mediaRelatedFb2Book: MediaRelatedFb2Book;

    @OneToOne(
        () => MediaStatistics,
        mediaStatistics => mediaStatistics.media
    )
    media_statistics: MediaStatistics;

    @OneToOne(
        () => QueezeQuestions,
        QueezeQuestions => QueezeQuestions.mediaQuestion,
        {
            onDelete: 'SET NULL',
        }
    )
    quizQuestions: QueezeQuestions;

    @OneToMany(
        () => MediaSubscribe,
        mediaSubscribe => mediaSubscribe.media
    )
    mediaSubscribes: MediaSubscribe[];

    @OneToMany(
        () => MediaTags,
        mediaTags => mediaTags.media
    )
    media_tags: MediaTags[];

    @Column('date', { name: 'daily_media', nullable: true })
    daily_media: string | null;

    @OneToOne(
        () => Celebration,
        celebration => celebration.media,
        {
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'celebration_id', referencedColumnName: 'id' }])
    celebration: Celebration;

    @Column('int', { nullable: true })
    celebration_id: number | null;

    @ManyToOne(
        () => ApostolicReading,
        apostolicReading => apostolicReading.media,
        {
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'reading_apostolic_id', referencedColumnName: 'id' }])
    apostolicReading: ApostolicReading;
    @Column('int', { nullable: true })
    reading_apostolic_id: number | null;

    @ManyToOne(
        () => GospelReading,
        gospelReading => gospelReading.media,
        {
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'reading_gospel_id', referencedColumnName: 'id' }])
    gospelReading: GospelReading;
    @Column('int', { nullable: true })
    reading_gospel_id: number | null;

    @ManyToOne(
        () => PostReading,
        postReading => postReading.media,
        {
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'reading_post_id', referencedColumnName: 'id' }])
    postReading: PostReading;
    @Column('int', { nullable: true })
    reading_post_id: number | null;

    @ManyToOne(
        () => MorningReading,
        morningReading => morningReading.media,
        {
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'reading_morning_id', referencedColumnName: 'id' }])
    morningReading: MorningReading;
    @Column('int', { nullable: true })
    reading_morning_id: number | null;

    @ManyToOne(
        () => Mineja,
        mineja => mineja.media,
        {
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'mineja_id', referencedColumnName: 'id' }])
    mineja: Mineja;
    @Column('int', { nullable: true })
    mineja_id: number | null;

    public static async insertManyAndGetId(media: Array<Partial<Media>>): Promise<Array<number>> {
        return (
            await this.createQueryBuilder()
                .insert()
                .values(media)
                .execute()
        ).identifiers.map(({ id }) => id);
    }

    public static mediaFilePathAsJson(type: string, path: string) {
        switch (type) {
            case 'application/epub+zip':
                return JSON.stringify({
                    epub: path,
                    pdf: '',
                    fb2: '',
                });
            case 'audio/mpeg':
                return JSON.stringify({
                    audio_href: path,
                });
            case 'application/pdf':
                return JSON.stringify({
                    pdf: path,
                    fb2: '',
                    epub: '',
                });
            case 'application/x-fictionbook+xml':
                return JSON.stringify({
                    fb2: path,
                    pdf: '',
                    epub: '',
                });
        }
    }

    @BeforeInsert()
    fillDefaultSocialDescription() {
        if (!this.social_description) {
            this.social_description = this.text.replace(/(<([^>]+)>)/g, '').substring(0, 200);
        }
    }

    @BeforeInsert()
    @BeforeUpdate()
    makeSortable() {
        this.title_for_sort = this.title.replace(/^[^\p{Letter}]+/u, '');
        if (!/^[а-я]{2}/i.test(this.title_for_sort)) {
            this.title_for_sort = latinToCyrillic(transliterate(this.title_for_sort));
        }
    }
}
