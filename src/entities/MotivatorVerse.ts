import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Book from './Book';
import Motivator from './Motivator';
import Testament from './Testament';
import Verse from './Verse';

@Index('idx-motivator_verse-motivator_id', ['motivatorId'], {})
@Index('idx-motivator_verse-testament_id', ['testamentId'], {})
@Index('idx-motivator_verse-book_id', ['bookId'], {})
@Index('idx-motivator_verse-verse_id', ['verseId'], {})
@Index('idx-motivator_verse-chapter', ['chapter'], {})
@Entity('motivator_verse', { schema: 'ekzeget' })
export default class MotivatorVerse extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'motivator_id', nullable: true })
    motivatorId: number | null;

    @Column('int', { name: 'testament_id', nullable: true })
    testamentId: number | null;

    @Column('int', { name: 'book_id', nullable: true })
    bookId: number | null;

    @Column('int', { name: 'verse_id', nullable: true })
    verseId: number | null;

    @Column('int', { name: 'chapter', nullable: true })
    chapter: number | null;

    @ManyToOne(
        () => Book,
        book => book.motivator_verses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
    book: Book;

    @ManyToOne(
        () => Motivator,
        motivator => motivator.motivatorVerses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'motivator_id', referencedColumnName: 'id' }])
    motivator: Motivator;

    @ManyToOne(
        () => Testament,
        testament => testament.motivatorVerses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'testament_id', referencedColumnName: 'id' }])
    testament: Testament;

    @ManyToOne(
        () => Verse,
        verse => verse.motivator_verses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;
}
