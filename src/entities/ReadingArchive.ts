import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import User from './User';
import UserReadingPlan from './UserReadingPlan';
import GroupReadingPlan from './GroupReadingPlan';
import { ServerError } from '../libs/ErrorHandler';
import { StatisticsService } from '../services/userServices/StatisticsService';
import * as Joi from '@hapi/joi';
import ReadingPlanComment from './ReadingPlanComment';

/**
 * Represents archived reading (reading that has been finished).
 * Could be created either from a user_reading_plan of from a group_reading_plan.
 */
@Entity('reading_archive', { schema: 'ekzeget' })
export default class ReadingArchive extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'record_type', nullable: false, length: 63 })
    record_type: ReadingArchiveRecordType;

    @Column('int', { name: 'user_id', nullable: false })
    user_id: User['id'];

    @Column('int', { name: 'mentor_id', nullable: false })
    mentor_id: User['id'];

    @Column('varchar', { name: 'plan_description', nullable: false, length: 255 })
    plan_description: string;

    @Column('text', { name: 'plan_comment', nullable: false })
    plan_comment: string;

    @Column('int', { name: 'plan_length', nullable: false })
    plan_length: number;

    @Column('date', { name: 'start_date', nullable: true })
    start_date: string | null;

    @Column('date', { name: 'stop_date', nullable: true })
    stop_date: string | null;

    @Column('int', { name: 'members_count', nullable: false })
    members_count: number;

    @Column('int', { name: 'comments_count', nullable: false })
    comments_count: number;

    @Column('float', { name: 'answers_percent', nullable: false })
    answers_percent: number;

    @Column('float', { name: 'correct_answers_percent', nullable: false })
    correct_answers_percent: number;

    /**
     * Группа, к которой план когда-то принадлежал. Значение отсюда не всегда представлено в БД.
     */
    @Column('int', { name: 'group_id', nullable: true, unsigned: true })
    group_id: number | null;

    @ManyToOne(
        () => User,
        (user: User) => user.readingArchive,
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;

    @ManyToOne(
        () => User,
        (user: User) => user.mentoredReadingArchive,
    )
    @JoinColumn([{ name: 'mentor_id', referencedColumnName: 'id' }])
    mentor: User;

    static async fromUserReadingPlan(urp: UserReadingPlan): Promise<ReadingArchive> {
        // reload entity due to errors using this method in loops
        urp = await UserReadingPlan.findOne(urp.id, {
            relations: ['plan', 'group', 'group.userPlans'],
        });
        if (!urp.user_id)
            throw new ServerError('Provided user reading plan does not have user_id.', 500);

        let members_count = (urp.group?.users ?? urp.group?.userPlans)?.length || 1;
        if (urp.group) {
            const userPlanArchives = await ReadingArchive.count({ where: { group_id: urp.group_id } });
            members_count += userPlanArchives;
        }

        const statistics = await StatisticsService.getQuizStatistics(urp);
        const created = await ReadingArchive.create({
            record_type: urp.group_id
                ? ReadingArchiveRecordType.GROUP_USER
                : ReadingArchiveRecordType.SOLO_USER,
            user_id: urp.user_id,
            mentor_id: urp.group?.mentor_id ?? urp.user_id,
            plan_description: urp.plan.description,
            plan_comment: urp.plan.comment,
            plan_length: urp.plan.lenght,
            start_date: urp.start_date,
            stop_date: urp.stop_date,
            members_count,
            comments_count: urp.group
                ? await ReadingPlanComment.count({ where: { group_id: urp.group_id } })
                : 0,
            answers_percent: statistics?.allTime?.completedPercent ?? 0,
            correct_answers_percent: statistics?.allTime?.accuracy ?? 0,
            group_id: urp.group_id,
        }).save();
        return ReadingArchive.findOne(created.id, { relations: ['user', 'mentor'] });
    }

    static async fromGroup(group: GroupReadingPlan): Promise<ReadingArchive> {
        // reload entity due to errors using this method in loops
        group = await GroupReadingPlan.findOne(group.id, { relations: ['plan', 'userPlans'] });

        const statistics = await Promise.all(
            group.userPlans.map(urp => StatisticsService.getQuizStatistics(urp)),
        );
        const userPlanArchives = await ReadingArchive.find({ where: { group_id: group.id } });
        const urpsTotal = userPlanArchives.length + statistics.length;
        let answersPercentTotal = 0;
        let accuracyTotal = 0;

        for (const archivedUrp of userPlanArchives) {
            answersPercentTotal += archivedUrp.answers_percent;
            accuracyTotal += archivedUrp.correct_answers_percent;
        }
        for (const s of statistics) {
            answersPercentTotal += s?.allTime?.completedPercent ?? 0;
            accuracyTotal += s?.allTime?.accuracy ?? 0;
        }

        const created = await ReadingArchive.create({
            record_type: ReadingArchiveRecordType.GROUP_MENTOR,
            user_id: group.mentor_id,
            mentor_id: group.mentor_id,
            plan_description: group.plan.description,
            plan_comment: group.plan.comment,
            plan_length: group.plan.lenght,
            start_date: group.start_date,
            stop_date: group.stop_date,
            members_count: urpsTotal || 1,
            comments_count: await ReadingPlanComment.count({ where: { group_id: group.id } }),
            answers_percent: answersPercentTotal / urpsTotal || 0, // in case of zero division
            correct_answers_percent: accuracyTotal / urpsTotal || 0,
            group_id: group.id,
        }).save();
        return ReadingArchive.findOne(created.id, { relations: ['user', 'mentor'] });
    }
}

export enum ReadingArchiveRecordType {
    SOLO_USER = 'soloUser',
    GROUP_USER = 'groupUser',
    GROUP_MENTOR = 'groupMentor',
}

export const ReadingArchiveSchema = Joi.object({
    id: Joi.number().required(),
    record_type: Joi.string().valid(...Object.values(ReadingArchiveRecordType)).required(),
    user_id: Joi.number().required(),
    mentor_id: Joi.number().required(),
    plan_description: Joi.string().required(),
    plan_comment: Joi.string().required(),
    plan_length: Joi.number().integer().min(0).required(),
    start_date: Joi.string().allow(null).optional(),
    stop_date: Joi.string().allow(null).optional(),
    members_count: Joi.number().integer().min(1).required(),
    comments_count: Joi.number().integer().min(0).required(),
    answers_percent: Joi.number()/*.min(0).max(1)*/.required(),
    correct_answers_percent: Joi.number()/*.min(0).max(1)*/.required(),
    user: Joi.object().unknown().allow(null).optional(),
    mentor: Joi.object().unknown().allow(null).optional(),
    group_id: Joi.number().integer().min(0).allow(null).optional(),
}).required().description('Архивированная статистика плана чтения пользователя.');
