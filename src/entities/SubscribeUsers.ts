import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import SubscribeTypes from './SubscribeTypes';

@Index('idx-subscribe_users-subscribe_type_id', ['subscribeTypeId'], {})
@Entity('subscribe_users', { schema: 'ekzeget' })
export default class SubscribeUsers extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'name', length: 255 })
    name: string;

    @Column('varchar', { name: 'email', length: 255 })
    email: string;

    @Column('int', { name: 'subscribe_type_id', nullable: true })
    subscribeTypeId: number | null;

    @Column('tinyint', {
        name: 'is_confirm',
        nullable: true,
        width: 1,
        default: () => "'0'",
    })
    isConfirm: boolean | null;

    @Column('int', { name: 'confirmed_at', nullable: true })
    confirmedAt: number | null;

    @Column('int', { name: 'canceled_at', nullable: true })
    canceledAt: number | null;

    @Column('varchar', { name: 'hash_for_one', nullable: true, length: 32 })
    hashForOne: string | null;

    @Column('varchar', { name: 'hash_for_all', nullable: true, length: 32 })
    hashForAll: string | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;

    @ManyToOne(
        () => SubscribeTypes,
        subscribeTypes => subscribeTypes.subscribeUsers,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'subscribe_type_id', referencedColumnName: 'id' }])
    subscribeType: SubscribeTypes;
}
