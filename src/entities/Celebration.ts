import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany, OneToOne,
    PrimaryGeneratedColumn, RelationId,
} from 'typeorm';
import CelebrationExcuse from './CelebrationExcuse';
import CelebrationPreacherExcuse from './CelebrationPreacherExcuse';
import Media from './Media';

@Index('idx_celebration_active', ['active'], {})
@Index('fk_celebration_excuse', ['excuse_id'], {})
@Entity('celebration', { schema: 'ekzeget' })
export default class Celebration extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    //@RelationId((cel: Celebration) => cel.excuse)
    @Column('int', { name: 'excuse_id', nullable: true })
    excuse_id: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @ManyToOne(
        () => CelebrationExcuse,
        celebrationExcuse => celebrationExcuse.celebrations,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'excuse_id', referencedColumnName: 'id' }])
    excuse: CelebrationExcuse;

    @OneToMany(
        () => CelebrationPreacherExcuse,
        celebrationPreacherExcuse => celebrationPreacherExcuse.celebration
    )
    celebrationPreacherExcuses: CelebrationPreacherExcuse[];

    @OneToOne(
        () => Media,
        media => media.celebration
    )
    media: Media;
}
