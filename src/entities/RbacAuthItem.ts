import { BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import RbacAuthAssignment from './RbacAuthAssignment';
import RbacAuthRule from './RbacAuthRule';
import RbacAuthItemChild from './RbacAuthItemChild';

@Index('rule_name', ['ruleName'], {})
@Index('idx-auth_item-type', ['type'], {})
@Entity('rbac_auth_item', { schema: 'ekzeget' })
export default class RbacAuthItem extends BaseEntity {
    @Column('varchar', { primary: true, name: 'name', length: 64 })
    name: string;

    @Column('smallint', { name: 'type' })
    type: number;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('varchar', { name: 'rule_name', nullable: true, length: 64 })
    ruleName: string | null;

    @Column('blob', { name: 'data', nullable: true })
    data: Buffer | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;

    @OneToMany(
        () => RbacAuthAssignment,
        rbacAuthAssignment => rbacAuthAssignment.itemName2
    )
    rbacAuthAssignments: RbacAuthAssignment[];

    @ManyToOne(
        () => RbacAuthRule,
        rbacAuthRule => rbacAuthRule.rbacAuthItems,
        {
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'rule_name', referencedColumnName: 'name' }])
    ruleName2: RbacAuthRule;

    @OneToMany(
        () => RbacAuthItemChild,
        rbacAuthItemChild => rbacAuthItemChild.parent2
    )
    rbacAuthItemChildren: RbacAuthItemChild[];

    @OneToMany(
        () => RbacAuthItemChild,
        rbacAuthItemChild => rbacAuthItemChild.child2
    )
    rbacAuthItemChildren2: RbacAuthItemChild[];
}
