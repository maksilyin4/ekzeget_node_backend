import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Interpretation from './Interpretation';
import User from './User';

@Index('fk_user_interpretation_fav_user', ['user_id'], {})
@Index('fk_user_interpretation_fav_interpretation', ['interpretation_id'], {})
@Entity('user_interpretation_fav', { schema: 'ekzeget' })
export default class UserInterpretationFav extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'user_id', nullable: true })
    user_id: number | null;

    @Column('int', { name: 'interpretation_id', nullable: true })
    interpretation_id: number | null;

    @ManyToOne(
        () => Interpretation,
        interpretation => interpretation.user_interpretation_favs,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'interpretation_id', referencedColumnName: 'id' }])
    interpretation: Interpretation;

    @ManyToOne(
        () => User,
        user => user.userInterpretationFavs,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;
}
