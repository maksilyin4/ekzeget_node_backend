import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import BibleMapsPoints from './BibleMapsPoints';
import Verse from './Verse';
import { type } from 'os';

@Index('bible_maps_verse_point', ['point_id'], {})
@Index('bible_maps_verse_verse', ['verse_id'], {})
@Entity('bible_maps_verse', { schema: 'ekzeget' })
export default class BibleMapsVerse extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'point_id', nullable: true })
    point_id: number | null;

    @Column('int', { name: 'verse_id', nullable: true })
    verse_id: number | null;

    @ManyToOne(
        () => BibleMapsPoints,
        bibleMapsPoints => bibleMapsPoints.bible_maps_verses,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'point_id', referencedColumnName: 'id' }])
    point: BibleMapsPoints;

    @ManyToOne(
        () => Verse,
        verse => verse.bible_maps_verses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;

    static async deleteByPointAndVerseArray(pointId, verseIds: number[]) {
        await this.createQueryBuilder()
            .delete()
            .where('point_id = :pointId', { pointId })
            .andWhere('verse_id IN (:...verseIds)', { verseIds })
            .execute();
    }

    static async insertManyByPointAndVerseArray(pointId: number | undefined, verseIds: number[]) {
        const lastId = await BibleMapsPoints.findOne({
            order: {
                id: 'DESC',
            },
        });

        pointId = pointId | lastId.id;

        const values: Partial<BibleMapsVerse>[] = [];
        verseIds.forEach(item => {
            values.push({ point_id: pointId, verse_id: item });
        });

        await this.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }

    static async deleteGroupByPointId(id) {
        await this.createQueryBuilder('del')
            .delete()
            .where('point_id = :id', { id: id })
            .execute();
    }
}
