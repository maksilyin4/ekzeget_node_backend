import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Testament from './Testament';
import BookDescription from './BookDescription';
import MediaBible from './MediaBible';
import MediaLib from './MediaLib';
import MotivatorVerse from './MotivatorVerse';
import Plan from './Plan';
import UserNote from './UserNote';
import Verse from './Verse';
import QueezeQuestions from './QueezeQuestions';

@Index('idx_key_storage_item_key', ['testament_id'], {})
@Index('idx_book_code', ['code'], {})
@Index('idx_book_gospel', ['gospel'], {})
@Index('idx_book_sort', ['sort'], {})
@Index('idx-book-legacy_code', ['legacy_code'], {})
@Entity('book', { schema: 'ekzeget' })
export default class Book extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'testament_id' })
    // @RelationId((book: Book) => book.testament)
    testament_id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('varchar', { name: 'short_title', nullable: true, length: 32 })
    short_title: string | null;

    @Column('int', { name: 'parts', nullable: true })
    parts: number | null;

    @Column('varchar', { name: 'code', length: 50 })
    code: string;

    @Column('varchar', { name: 'legacy_code', length: 12 })
    legacy_code: string;

    @Column('varchar', { name: 'menu', length: 64 })
    menu: string;

    @Column('varchar', { name: 'author', length: 255 })
    author: string;

    @Column('varchar', { name: 'year', length: 64 })
    year: string;

    @Column('varchar', { name: 'place', length: 255 })
    place: string;

    @Column('int', { name: 'ext_id', nullable: true, default: () => "'0'" })
    ext_id: number | null;

    @Column('int', { name: 'gospel', nullable: true, default: () => "'0'" })
    gospel: number | null;

    @Column('int', { name: 'sort', nullable: true, default: () => "'0'" })
    sort: number | null;

    @ManyToOne(
        () => Testament,
        (tes: Testament) => tes.books,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn({ name: 'testament_id', referencedColumnName: 'id' })
    testament: Testament;

    @OneToMany(
        () => BookDescription,
        bookDescription => bookDescription.book
    )
    book_descriptions: BookDescription[];

    @OneToMany(
        () => MediaBible,
        mediaBible => mediaBible.book
    )
    media_bibles: MediaBible[];

    @OneToMany(
        () => MediaLib,
        mediaLib => mediaLib.book
    )
    media_libs: MediaLib[];

    @OneToMany(
        () => MotivatorVerse,
        motivatorVerse => motivatorVerse.book
    )
    motivator_verses: MotivatorVerse[];

    @OneToMany(
        () => Plan,
        plan => plan.book
    )
    plans: Plan[];

    @OneToMany(
        () => UserNote,
        userNote => userNote.book
    )
    user_notes: UserNote[];

    @OneToMany(
        () => Verse,
        verse => verse.book
    )
    verses: Verse[];

    static async getBookByVerseId(verseId) {
        return await this.createQueryBuilder('book')
            .select('book.title, book.parts, book.id')
            .where(db => {
                const subQuery = db
                    .subQuery()
                    .select('book_id')
                    .from(Verse, 'verse')
                    .where('verse.id = :id', { id: verseId })
                    .getQuery();
                return 'book.id = ' + subQuery;
            })
            .getRawOne();
    }

    static async getBooksForQuiz() {
        return await Book.createQueryBuilder('book')
            .where(db => {
                const subQuery = db
                    .subQuery()
                    .select('verse.book_id')
                    .from(QueezeQuestions, 'quest')
                    .innerJoin(Verse, 'verse', 'quest.verse_id = verse.id')
                    .distinct(true)
                    .orderBy('book_id')
                    .getQuery();
                return 'book.id IN ' + subQuery;
            })
            .getMany();
    }

    static async getRelationBooksByQuestIds(questionIds: number[]): Promise<Partial<Book>[]> {
        return await Book.createQueryBuilder('book')
            .select('book.id, book.title, book.code')
            .where(db => {
                const subQuery = db
                    .subQuery()
                    .select('book_id')
                    .from(QueezeQuestions, 'quest')
                    .where('quest.id IN (:...ids)', { ids: questionIds })
                    .innerJoin(Verse, 'verse', 'quest.verse_id = verse.id')
                    .distinct(true)
                    .orderBy('book_id')
                    .getQuery();
                return 'book.id IN ' + subQuery;
            })
            .getRawMany();
    }

    static async findBookByCodeOrId(bookCodeOrId: string): Promise<Book> {
        const isCode = !+bookCodeOrId;
        if (isCode) {
            return Book.createQueryBuilder('book')
                .select('book')
                .leftJoinAndSelect('book.testament', 'testament')
                .where('`book`.`code` = :bookCode', { bookCode: bookCodeOrId })
                .orWhere('`book`.`legacy_code` = :bookCode', { bookCode: bookCodeOrId })
                .getOne();
        }

        return Book.createQueryBuilder('book')
            .select('book')
            .leftJoinAndSelect('book.testament', 'testament')
            .where('`book`.`id` = :bookId', { bookId: +bookCodeOrId })
            .getOne();
    }
}
