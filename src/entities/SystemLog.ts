import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('idx_log_level', ['level'], {})
@Index('idx_log_category', ['category'], {})
@Entity('system_log', { schema: 'ekzeget' })
export default class SystemLog extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
    id: string;

    @Column('int', { name: 'level', nullable: true })
    level: number | null;

    @Column('varchar', { name: 'category', nullable: true, length: 255 })
    category: string | null;

    @Column('double', { name: 'log_time', nullable: true, precision: 22 })
    logTime: number | null;

    @Column('text', { name: 'prefix', nullable: true })
    prefix: string | null;

    @Column('text', { name: 'message', nullable: true })
    message: string | null;
}
