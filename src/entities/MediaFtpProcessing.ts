import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Media from './Media';

@Index('fk_media_id_media', ['mediaId'], {})
@Entity('media_ftp_processing', { schema: 'ekzeget' })
export default class MediaFtpProcessing extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'media_id', nullable: true })
    mediaId: number | null;

    @Column('tinyint', { name: 'status', nullable: true })
    status: number | null;

    @Column('datetime', {
        name: 'created_at',
        nullable: true,
        default: () => 'CURRENT_TIMESTAMP',
    })
    createdAt: Date | null;

    @Column('text', { name: 'data', nullable: true })
    data: string | null;

    @ManyToOne(
        () => Media,
        media => media.mediaFtpProcessings,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;
}
