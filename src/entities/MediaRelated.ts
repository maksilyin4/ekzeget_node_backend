import { BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import Media from './Media';
import { IRelationRecordInfo } from '../admin-panel/content/types/IRelatedRecordInfo';
import { IRelationForMedia } from '../admin-panel/content/types/IRelationForMedia';

@Index('related_id', ['related_id'], {})
@Entity('media_related', { schema: 'ekzeget' })
export default class MediaRelated extends BaseEntity {
    @Column('int', { primary: true, name: 'media_id' })
    media_id: number;

    @Column('int', { primary: true, name: 'related_id' })
    related_id: number;

    @Column('int', { name: 'sort', nullable: true })
    sort: number | null;

    @ManyToOne(
        () => Media,
        media => media.mediaRelateds,
        {
            onDelete: 'CASCADE',
            onUpdate: 'NO ACTION',
        }
    )
    @JoinColumn([{ name: 'related_id', referencedColumnName: 'id' }])
    related: Media;

    @ManyToOne(
        () => Media,
        media => media.mediaRelateds2,
        {
            onDelete: 'CASCADE',
            onUpdate: 'NO ACTION',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;

    static async getInfoAboutRelatedRecords(mediaId: number): Promise<Array<IRelationRecordInfo>> {
        const info: IRelationRecordInfo[] = await this.createQueryBuilder('related')
            .select('related.related_id, related.sort, media.title AS title')
            .from(Media, 'media')
            .where('related.media_id = (:mediaId)', { mediaId })
            .andWhere('media.id = related.related_id')
            .orderBy('sort')
            .getRawMany();

        return info;
    }

    static async deleteGroupByIds(ids: number[], mediaId: number) {
        await this.createQueryBuilder('del')
            .delete()
            .where('related_id IN (:...ids)', { ids })
            .andWhere('media_id = :mediaId', { mediaId })
            .execute();
    }

    static async insertMany(mediaId: number | undefined, relations: Partial<MediaRelated>[]) {
        const lastId = await Media.findOne({
            order: {
                id: 'DESC',
            },
        });

        const values: Partial<MediaRelated>[] = mediaId
            ? relations
            : relations.map(item => {
                  item.media_id = lastId.id;
                  return item;
              });

        await this.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }

    static async updateMany(relations: Partial<MediaRelated>[]) {
        await Promise.all(
            relations.map(async item => {
                await this.createQueryBuilder()
                    .update()
                    .set({ sort: item.sort })
                    .where('media_id = :id', { id: item.media_id })
                    .andWhere('related_id = :related_id', { related_id: item.related_id })
                    .execute();
            })
        );
    }

    static async getLastSort(mediaId) {
        const lastSort = await this.findOne({
            where: {
                media_id: mediaId,
            },
            order: { sort: 'DESC' },
        });

        return lastSort?.sort | 0;
    }
}
