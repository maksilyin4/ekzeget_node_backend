import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    RelationId,
} from 'typeorm';
import EkzegetType from './EkzegetType';
import Interpretation from './Interpretation';

@Index('idx_ekzeget_person_century', ['century'], {})
@Index('idx_ekzeget_person_active', ['active'], {})
@Index('fk_ekzeget_person_ekzeget_type', ['ekzeget_type_id'], {})
@Index('idx-ekzeget_person-code', ['code'], {})
@Entity('ekzeget_person', { schema: 'ekzeget' })
export default class EkzegetPerson extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'name', nullable: true, length: 255 })
    name: string | null;

    @Column('varchar', { name: 'code', length: 60 })
    code: string;

    @Column('int', { name: 'century', nullable: true })
    century: number | null;

    @Column('text', { name: 'info', nullable: true })
    info: string | null;

    //@RelationId((person: EkzegetPerson) => person.ekzeget_type)
    @Column('int', { name: 'ekzeget_type_id', nullable: true })
    ekzeget_type_id: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @ManyToOne(
        () => EkzegetType,
        ekzegetType => ekzegetType.ekzegetPeople,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'ekzeget_type_id', referencedColumnName: 'id' }])
    ekzeget_type: EkzegetType;

    @OneToMany(
        () => Interpretation,
        interpretation => interpretation.ekzeget
    )
    interpretations: Interpretation[];

    static getInterpreterId = async (code: string): Promise<number> => {
        if (!code) return;

        const isNumeric = str => !isNaN(str);
        if (isNumeric(code)) return Number(code);

        const ep = await EkzegetPerson.findOne({
            where: {
                code,
            },
        });

        if (!ep) {
            return undefined;
        }

        return ep.id;
    };
}
