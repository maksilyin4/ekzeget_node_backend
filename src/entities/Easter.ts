import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('idx_easter_active', ['active'], {})
@Index('idx_easter_date', ['date'], {})
@Entity('easter', { schema: 'ekzeget' })
export default class Easter extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('date', { name: 'date', nullable: true })
    date: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;
}
