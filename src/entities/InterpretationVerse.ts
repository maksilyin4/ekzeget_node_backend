import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Interpretation from './Interpretation';
import Verse from './Verse';
import { IBookParameters } from '../admin-panel/content/types/IBookParameters';

@Index('fk_iinterpretation_verse_interpretation', ['interpretation_id'], {})
@Index('fk_interpretation_verse_verse', ['verse_id'], {})
@Entity('interpretation_verse', { schema: 'ekzeget' })
export default class InterpretationVerse extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'interpretation_id', nullable: true })
    interpretation_id: number | null;

    @Column('int', { name: 'verse_id', nullable: true })
    verse_id: number | null;

    @ManyToOne(
        () => Interpretation,
        interpretation => interpretation.interpretation_verses,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'interpretation_id', referencedColumnName: 'id' }])
    interpretation: Interpretation;

    @ManyToOne(
        () => Verse,
        verse => verse.interpretation_verses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;

    static async getInterpretationIdsByBookParameters(bookParameters: IBookParameters) {
        const query = await InterpretationVerse.createQueryBuilder('interVerse').leftJoin(
            Verse,
            'verse',
            'verse.id =  interVerse.verse_id'
        );

        if (bookParameters.bookId) {
            query.andWhere('verse.book_id = :bookId', { bookId: bookParameters.bookId });
        }

        if (bookParameters.chapter) {
            query.andWhere('verse.chapter = :chapter', { chapter: bookParameters.chapter });
        }

        if (bookParameters.number) {
            query.andWhere('verse.number = :number', { number: bookParameters.number });
        }

        const interpretations = await query.getMany();

        return interpretations.map(interpretationVerse => interpretationVerse.interpretation_id);
    }

    static async deleteByInterpretationAndVerseArray(interpretationId, verseIds: number[]) {
        await this.createQueryBuilder()
            .delete()
            .where('interpretation_id = :interpretationId', { interpretationId })
            .andWhere('verse_id IN (:...verseIds)', { verseIds })
            .execute();
    }

    static async insertManyByInterpretationAndVerseArray(
        interpretationId: number,
        verseIds: number[]
    ) {
        const values: Partial<InterpretationVerse>[] = [];
        verseIds.forEach(item => {
            values.push({ interpretation_id: interpretationId, verse_id: item });
        });

        await this.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }
}
