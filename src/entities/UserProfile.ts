import { BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import User from './User';

@Entity('user_profile', { schema: 'ekzeget' })
export default class UserProfile extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'user_id' })
    user_id: number;

    @Column('varchar', { name: 'firstname', nullable: true, length: 255 })
    firstname: string | null;

    @Column('varchar', { name: 'middlename', nullable: true, length: 255 })
    middlename: string | null;

    @Column('varchar', { name: 'lastname', nullable: true, length: 255 })
    lastname: string | null;

    @Column('varchar', { name: 'avatar_path', nullable: true, length: 255 })
    avatar_path: string | null;

    @Column('varchar', { name: 'avatar_base_url', nullable: true, length: 255 })
    avatar_base_url: string | null;

    @Column('varchar', { name: 'locale', length: 32 })
    locale: string;

    @Column('smallint', { name: 'gender', nullable: true })
    gender: number | null;

    @Column('date', { name: 'birthday', nullable: true })
    birthday: string | null;

    @Column('text', { name: 'about', nullable: true })
    about: string | null;

    @Column('varchar', { name: 'country', length: 255 })
    country: string = '';

    @Column('varchar', { name: 'city', length: 255 })
    city: string = '';

    @OneToOne(
        () => User,
        user => user.userProfile,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;
}
