import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Celebration from './Celebration';
import Excuse from './Excuse';

@Index('fk_celebration_preacher_excuse_celebration', ['celebrationId'], {})
@Index('fk_celebration_preacher_excuse_excuse', ['excuseId'], {})
@Entity('celebration_preacher_excuse', { schema: 'ekzeget' })
export default class CelebrationPreacherExcuse extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'celebration_id', nullable: true })
    celebrationId: number | null;

    @Column('int', { name: 'excuse_id', nullable: true })
    excuseId: number | null;

    @ManyToOne(
        () => Celebration,
        celebration => celebration.celebrationPreacherExcuses,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'celebration_id', referencedColumnName: 'id' }])
    celebration: Celebration;

    @ManyToOne(
        () => Excuse,
        excuse => excuse.celebration_preacher_excuses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'excuse_id', referencedColumnName: 'id' }])
    excuse: Excuse;
}
