import {
    BaseEntity,
    Column,
    Entity,
    Index,
    ManyToMany,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Audio from './Audio';
import MediaBible from './MediaBible';

@Index('idx-book_id', ['book_id'], {})
@Index('idx-chapter', ['book_id', 'number'], {})
@Entity('chapters', { schema: 'ekzeget' })
export default class Chapters extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'book_id' })
    book_id: number;

    @Column('int', { name: 'number' })
    number: number;

    @ManyToMany(
        () => Audio,
        audio => audio.chapters
    )
    audio: Audio[];

    @OneToMany(
        () => MediaBible,
        mediaBible => mediaBible.chapter
    )
    media_bibles: MediaBible[];
}
