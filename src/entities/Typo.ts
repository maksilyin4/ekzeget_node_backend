import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';

@Entity('typo', { schema: 'ekzeget' })
export default class Typo extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'comment', nullable: true, length: 255 })
    comment: string | null;

    @Column('varchar', { name: 'url', length: 1024 })
    url: string;

    @Column('varchar', { name: 'selection', length: 1024 })
    selection: string;

    @Column('boolean', { name: 'is_report_helpful', nullable: true })
    is_report_helpful: boolean | null;

    @Column('boolean', { name: 'processed', default: false })
    processed: boolean;

    @Column('int', { name: 'created_at' })
    created_at: number;

    @Column('int', { name: 'user_id', nullable: true })
    user_id: number | null;

    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;
}
