import {BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import User from './User';

@Index('idx_user_oauth2_external_user_id', ['external_user_id'], {})
@Entity('user_oauth2', { schema: 'ekzeget' })
export default class UserOauth2 extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'user_id' })
    user_id: number;

    @Column('varchar', { name: 'external_user_id', length: 255 })
    external_user_id: string;

    @Column('varchar', { name: 'provider', length: 32 })
    provider: string;

    @Column('int', { name: 'created_at' })
    created_at: number;

    @ManyToOne(() => User)
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;
}
