import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import QueezeAnswers from './QueezeAnswers';
import Verse from './Verse';
import { IClientQuestion, IVerseForMediaAndAll } from '../core/App/interfaces/IQuiz';
import quizConstants from '../const/numbers/quiz/quizConstants';
import Media from './Media';
import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import inputJoiSchema from '../api/RestApi/Controller/controllers/Queeze/QueezeCreate/inputJoiSchema';
import Interpretation from './Interpretation';

@Index('verse_id', ['verseId'], {})
@Index('type', ['type'], {})
@Index('queeze_questions-active-idx', ['active'], {})
@Index('code', ['code'], {})
@Entity('queeze_questions', { schema: 'ekzeget' })
export default class QueezeQuestions extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id', unsigned: true })
    id: number;

    @Column('int', { name: 'verse_id' })
    verseId: number;

    @Column('varchar', {
        name: 'type',
        comment: 'Вопрос по толкованию или вопрос по тексту',
        length: 1,
    })
    type: string;

    @Column('varchar', { name: 'title', length: 255 })
    title: string;

    @Column('text', { name: 'description' })
    description: string;

    @Column('varchar', { name: 'active', length: 1, default: () => "'n'" })
    active: string;

    @Column('varchar', { name: 'code', nullable: true, length: 255 })
    code: string | null;

    @Column('int', { name: 'media_question', default: null })
    media_question: number;

    @Column('int', { name: 'media_answer', default: null })
    media_answer: number;

    @OneToMany(
        () => QueezeAnswers,
        queezeAnswers => queezeAnswers.question
    )
    answers: QueezeAnswers[];

    @ManyToOne(
        () => Verse,
        verse => verse.queeze_questions,
        {
            onDelete: 'CASCADE',
            onUpdate: 'NO ACTION',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;

    @OneToOne(
        () => Media,
        Media => Media.quizQuestions
    )
    @JoinColumn([{ name: 'media_question', referencedColumnName: 'id' }])
    mediaQuestion: string | null;

    @JoinColumn([{ name: 'media_answer', referencedColumnName: 'id' }])
    mediaAnswer: string | null;

    @ManyToMany(
        () => Interpretation,
        interpretation => interpretation.quiz_questions
    )
    @JoinTable({
        name: 'quiz_question_interpretation',
        joinColumns: [{ name: 'quiz_question_id', referencedColumnName: 'id' }],
        inverseJoinColumns: [{ name: 'interpretation_id', referencedColumnName: 'id' }],
        schema: 'ekzeget',
    })
    interpretations: Interpretation[];

    static async getRandQuestions(
        questionsFilter: Joi.extractType<typeof inputJoiSchema>
    ): Promise<IClientQuestion[]> {
        const questionsTypes = [];
        if (questionsFilter.is_related_to_text === '1') {
            questionsTypes.push('T');
        }
        if (questionsFilter.is_related_to_interpretations === '1') {
            questionsTypes.push('I');
        }

        const questions = await this.createQueryBuilder('quiz')
            .select([
                'quiz.id as id',
                'quiz.description as description',
                'quiz.title as title',
                'quiz.code as code',
                'mediaQuestion.properties as mediaQuestion',
                'mediaAnswer.properties as mediaAnswer',
            ])
            .leftJoin('media', 'mediaQuestion', 'mediaQuestion.id = quiz.media_question')
            .leftJoin('media', 'mediaAnswer', 'mediaAnswer.id = quiz.media_answer')
            .where(db => {
                const subQuery = db
                    .subQuery()
                    .select('verse.id')
                    .from(Verse, 'verse')
                    .where('verse.book_id IN (:...bid)', { bid: questionsFilter.book_ids })
                    .getQuery();
                return 'quiz.verse_id IN ' + subQuery;
            })
            .andWhere("quiz.active = 'y'")
            .andWhere('quiz.type IN (:...questionsTypes)', { questionsTypes })
            .orderBy('RAND()')
            .limit(quizConstants.QUESTIONS_COUNT)
            .getRawMany();

        await this.loadRelations(questions);

        return await Promise.all(
            questions.map(async question => {
                question.mediaQuestion = await this.getAudioFromJson(question.mediaQuestion);
                question.mediaAnswer = await this.getAudioFromJson(question.mediaAnswer);
                return question;
            })
        );
    }

    static async getQuestionsByBookAndChapter(
        bookId: number,
        chapterId: number,
        type?: string
    ): Promise<Partial<QueezeQuestions>[]> {
        const questions: Partial<QueezeQuestions>[] = await this.createQueryBuilder('quest')
            .select('quest.id, quest.title, quest.code, quest.verse_id')
            .where(db => {
                const subQuery = db
                    .subQuery()
                    .select('verse.id')
                    .from(Verse, 'verse')
                    .where('verse.book_id = :bid', { bid: bookId })
                    .andWhere('verse.chapter_id = :chid', { chid: chapterId })
                    .getQuery();
                return 'quest.verse_id IN ' + subQuery;
            })
            .andWhere("quest.active = 'y'")
            .andWhere(type ? 'quest.type = :type' : 'TRUE', { type })
            .getRawMany();
        return questions;
    }

    static async getQuestionsWithAnswersByBookAndChapter(
        questionsFilter: Joi.extractType<typeof inputJoiSchema>
    ): Promise<IClientQuestion[]> {
        const questionsTypes = [];
        if (questionsFilter.is_related_to_text === '1') {
            questionsTypes.push('T');
        }
        if (questionsFilter.is_related_to_interpretations === '1') {
            questionsTypes.push('I');
        }

        const questions = await this.createQueryBuilder('quiz')
            .select([
                'quiz.id as id',
                'quiz.description as description',
                'quiz.title as title',
                'quiz.code as code',
                'mediaQuestion.properties as mediaQuestion',
                'mediaAnswer.properties as mediaAnswer',
            ])
            .leftJoin('media', 'mediaQuestion', 'mediaQuestion.id = quiz.media_question')
            .leftJoin('media', 'mediaAnswer', 'mediaAnswer.id = quiz.media_answer')
            .where(db => {
                const subQuery = db
                    .subQuery()
                    .select('verse.id')
                    .from(Verse, 'verse')
                    .where('verse.book_id = :bid', { bid: questionsFilter.book_ids })
                    .andWhere('verse.chapter_id = :chid', { chid: questionsFilter.chapter_id })
                    .getQuery();
                return 'quiz.verse_id IN ' + subQuery;
            })
            .andWhere('quiz.type IN (:...questionsTypes)', { questionsTypes })
            .getRawMany();

        let indexSelectedQuestion = 0;
        for (const item of questions) {
            if (item.id === questionsFilter.question_id) {
                break;
            }
            indexSelectedQuestion++;
        }

        const bufferForQuestions = questions[indexSelectedQuestion];
        questions[indexSelectedQuestion] = questions[0];
        questions[0] = bufferForQuestions;

        await this.loadRelations(questions);

        return await Promise.all(
            questions.map(async question => {
                question.mediaQuestion = await this.getAudioFromJson(question.mediaQuestion);
                question.mediaAnswer = await this.getAudioFromJson(question.mediaAnswer);
                return question;
            })
        );
    }

    static async getQuestionById(id: number): Promise<IClientQuestion> {
        const question: IClientQuestion = <IClientQuestion>await this.createQueryBuilder('question')
            .select([
                'question.id',
                'question.description',
                'question.title',
                'question.code',
                'interpretations.id',
                'interpretations.parsed_text',
                'ekzeget.id',
                'ekzeget.name',
                'ekzeget.code',
                'mediaQuestion.properties as mediaQuestion',
                'mediaAnswer.properties as mediaAnswer',
            ])
            .leftJoin('media', 'mediaQuestion', 'mediaQuestion.id = question.media_question')
            .leftJoin('media', 'mediaAnswer', 'mediaAnswer.id = question.media_answer')
            .leftJoin(
                'question.interpretations',
                'interpretations',
                'interpretations.active = true'
            )
            .innerJoinAndSelect('question.verse', 'verse')
            .innerJoinAndSelect('verse.book', 'book')
            .leftJoinAndSelect('question.answers', 'answers')
            .leftJoin('interpretations.ekzeget', 'ekzeget')
            .where('question.id = :id', { id })
            .getOne();

        question.num = null;
        question.status = null;
        question.mediaQuestion = await this.getAudioFromJson(question.mediaQuestion);
        question.mediaAnswer = await this.getAudioFromJson(question.mediaAnswer);

        return question;
    }

    static async getInterpretationsById(id: number): Promise<Interpretation[]> {
        const commentedQuestion = <IClientQuestion>await this.createQueryBuilder('question')
            .select([
                'interpretations.id',
                'ekzeget.id',
                'ekzeget.name',
                'ekzeget.code',
                'question.id',
            ])
            .leftJoin(
                'question.interpretations',
                'interpretations',
                'interpretations.active = true'
            )
            .leftJoin('interpretations.ekzeget', 'ekzeget')
            .innerJoinAndSelect('interpretations.interpretation_verses', 'interpretation_verses')
            .innerJoinAndSelect('interpretation_verses.verse', 'verse')
            .where('question.id = :id', { id })
            .getOne();

        if (!commentedQuestion) {
            return [];
        }

        const interpretations = commentedQuestion.interpretations as (Interpretation & {
            verse: IVerseForMediaAndAll;
        })[];
        for (const interpretation of interpretations) {
            interpretation.verse = await Verse.getVerseWithBookInfo(
                interpretation.interpretation_verses[0].verse.id
            );
            delete interpretation.interpretation_verses;
        }
        return interpretations;
    }

    static async getCodeById(id: number): Promise<string> {
        const questionsCode = await this.createQueryBuilder('quest')
            .select('quest.code')
            .where('quest.id = :id', { id })
            .getRawOne();
        return questionsCode['quest_code'];
    }

    static async getAllQuestionsByPaginator(
        perPage: number,
        page: number
    ): Promise<Partial<QueezeQuestions>[]> {
        const shift = perPage * (page - 1);

        const questions: Partial<QueezeQuestions>[] = await this.createQueryBuilder('quest')
            .select('quest.id, quest.title, quest.code, quest.verse_id')
            .skip(shift)
            .take(perPage)
            .getRawMany();
        return questions;
    }

    static async getCountActiveQuestions() {
        const count = await this.createQueryBuilder('quest')
            .select()
            .where("quest.active = 'y'")
            .getCount();

        return count;
    }

    static async getOneQuestion(questionCode: string): Promise<QueezeQuestions> {
        return await this.createQueryBuilder('question')
            .addSelect([
                'interpretations.id',
                'interpretations.parsed_text',
                'ekzeget.id',
                'ekzeget.name',
                'ekzeget.code',
            ])
            .leftJoin(
                'question.interpretations',
                'interpretations',
                'interpretations.active = true'
            )
            .leftJoin('interpretations.ekzeget', 'ekzeget')
            .leftJoinAndSelect('question.answers', 'answers')
            .where('question.code = :code', { code: questionCode })
            .getOne();
    }

    static async updateVerseId(id, newVerseId) {
        await this.createQueryBuilder('quest')
            .update()
            .set({ verseId: newVerseId })
            .where('id = :id', { id: id })
            .execute();
    }

    private static async getAudioFromJson(audioJson): Promise<string | null> {
        if (audioJson === null) {
            return null;
        }

        try {
            const audioHref = (await JSON.parse(audioJson)).audio_href;
            return audioHref;
        } catch (err) {
            return null;
        }
    }

    private static async loadRelations(rawQuestions: IClientQuestion[]) {
        const relations = (
            await this.createQueryBuilder('question')
                .select([
                    'interpretations.id',
                    'interpretations.parsed_text',
                    'ekzeget.id',
                    'ekzeget.name',
                    'ekzeget.code',
                    'question.id',
                ])
                .leftJoin(
                    'question.interpretations',
                    'interpretations',
                    'interpretations.active = true'
                )
                .leftJoin('interpretations.ekzeget', 'ekzeget')
                .leftJoinAndSelect('question.answers', 'answers')
                .innerJoinAndSelect('question.verse', 'verse')
                .innerJoinAndSelect('verse.book', 'book')
                .where('question.id IN (:...questionIds)', {
                    questionIds: rawQuestions.map(question => question.id),
                })
                .getMany()
        ).reduce((relations, question) => {
            relations[question.id] = question;
            return relations;
        }, {});

        let i = 1;
        for (const question of rawQuestions) {
            question.interpretations = relations[question.id].interpretations;
            question.verse = relations[question.id].verse;
            question.answers = relations[question.id].answers;
            question.status = 'NA';
            question.num = i;
            i++;
        }
    }
}
