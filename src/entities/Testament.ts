import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Book from './Book';
import MotivatorVerse from './MotivatorVerse';

@Entity('testament', { schema: 'ekzeget' })
export default class Testament extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 32 })
    title: string | null;

    @OneToMany(
        () => Book,
        book => book.testament
    )
    books: Book[];

    @OneToMany(
        () => MotivatorVerse,
        motivatorVerse => motivatorVerse.testament
    )
    motivatorVerses: MotivatorVerse[];
}
