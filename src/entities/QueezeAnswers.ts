import {
    AfterLoad,
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import QueezeQuestions from './QueezeQuestions';
import { IClientAnswer } from '../core/App/interfaces/IQuiz';

@Index('question_id', ['question_id'], {})
@Entity('queeze_answers', { schema: 'ekzeget' })
export default class QueezeAnswers extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id', unsigned: true })
    id: number;

    @Column('int', { name: 'question_id', unsigned: true })
    question_id: number;

    @Column('tinyint', { name: 'is_correct', width: 1 })
    is_correct: boolean;

    @AfterLoad()
    castIsCorrectToBool() {
        if (this.is_correct !== undefined) {
            this.is_correct = !!this.is_correct;
        }
    }

    @Column('text', { name: 'text' })
    text: string;

    @ManyToOne(
        () => QueezeQuestions,
        queezeQuestions => queezeQuestions.answers,
        { onDelete: 'CASCADE', onUpdate: 'NO ACTION' }
    )
    @JoinColumn([{ name: 'question_id', referencedColumnName: 'id' }])
    question: QueezeQuestions;

    static async getAnswers(questionId: number): Promise<IClientAnswer[]> {
        return await this.createQueryBuilder('answers')
            .select('answers.id, answers.question_id, answers.is_correct, answers.text')
            .where(`answers.question_id = '${questionId}'`)
            .getRawMany();
    }

    static async isCorrect(answerId: number): Promise<boolean> {
        const answer = await this.findOne({ id: answerId });
        return answer.is_correct;
    }

    static async setIsCorrectAnswersFromGroup(questionId, answerId) {
        await this.createQueryBuilder('ans')
            .update()
            .set({ is_correct: false })
            .where('question_id = :qid', { qid: questionId })
            .andWhere('is_correct = true')
            .execute();

        await this.createQueryBuilder('ans')
            .update()
            .set({ is_correct: true })
            .where('id = :aid', { aid: answerId })
            .execute();
    }

    static async updateAnswerTextById(id, text) {
        await this.createQueryBuilder('ans')
            .update()
            .set({ text: text })
            .where('id = :id', { id: id })
            .execute();
    }

    static async deleteGroupByQuestionId(id) {
        await this.createQueryBuilder('ans')
            .delete()
            .where('question_id = :id', { id: id })
            .execute();
    }
}
