import { BaseEntity, Entity, Index, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import Media from './Media';
import MediaTag from './MediaTag';
import Verse from './Verse';

@Index('media_tags_to_media', ['media_id'], {})
@Index('media_tags_to_tags', ['tag_id'], {})
@Entity('media_tags', { schema: 'ekzeget' })
export default class MediaTags extends BaseEntity {
    @PrimaryColumn('int', { name: 'tag_id' })
    tag_id: number | null;

    @PrimaryColumn('int', { name: 'media_id' })
    media_id: number | null;

    @ManyToOne(
        () => Media,
        media => media.media_tags,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;

    @ManyToOne(
        () => MediaTag,
        mediaTag => mediaTag.mediaTags,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'tag_id', referencedColumnName: 'id' }])
    tag: MediaTag;

    static async deleteByMediaIdAndTagsIdArray(mediaId, tagsIds: number[]) {
        await this.createQueryBuilder()
            .delete()
            .where('media_id = :mediaId', { mediaId })
            .andWhere('tag_id IN (:...tagsIds)', { tagsIds })
            .execute();
    }

    static async insertManyByMediaIdAndTagsIdArray(mediaId: number | undefined, tagsIds: number[]) {
        const lastId = await Media.findOne({
            order: {
                id: 'DESC',
            },
        });

        mediaId = mediaId ? mediaId : lastId.id;

        const values: Array<Partial<MediaTags>> = [];
        tagsIds.forEach(item => {
            values.push({ media_id: mediaId, tag_id: item });
        });

        await this.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }
}
