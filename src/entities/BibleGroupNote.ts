import {BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, RelationId} from 'typeorm';
import Testament from "./Testament";
import BibleGroupType from "./BibleGroupType";

@Index('bible_group_note-code-idx', ['code'], { unique: true })
@Index('act222', ['active'], {})
@Entity('bible_group_note', { schema: 'ekzeget' })
export default class BibleGroupNote extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('char', { name: 'data', length: 50 })
    data: string;

    @Column('text', { name: 'title' })
    title: string;

    @Column('longtext', { name: 'text' })
    text: string;

    @Column('char', { name: 'views', length: 30 })
    views: string;

    //@RelationId((note: BibleGroupNote) => note.groupType)
    @Column('char', { name: 'type' })
    type: string;

    @Column('text', { name: 'tags' })
    tags: string;

    @Column('int', { name: 'active', default: () => "'1'" })
    active: number;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @ManyToOne(
        () => BibleGroupType,
        (type: BibleGroupType) => type.title,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn({ name: 'type', referencedColumnName: 'id' })
    groupType: Testament;

    static async findByType(type: number): Promise<BibleGroupNote[]> {
        const where = {
            active: 1,
        } as any;
        if (type) where.type = type;

        return BibleGroupNote.find({ where });
    }
}
