import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Plan from './Plan';
import { IReadingPlanDetails } from '../core/App/interfaces/IReadingPlanDetails';
import { getBookShortCode } from '../api/RestApi/Controller/presenters/Book/getBookShortCode';

@Entity('reading', { schema: 'ekzeget' })
export default class Reading extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', length: 50 })
    title: string;

    @Column('longtext', { name: 'plan' })
    plan: string;

    @Column('varchar', { name: 'comment', length: 255 })
    comment: string;

    @OneToMany(
        () => Plan,
        plan => plan.reading
    )
    plans: Plan[];

    static async getDetailsByReadingsAndDay(
        readings: string[],
        day: number
    ): Promise<Array<IReadingPlanDetails>> {
        const details = await this.createQueryBuilder('reading')
            .leftJoin('reading.plans', 'plan')
            .leftJoin('plan.book', 'book')
            .select('plan.id, plan.branch, plan.chapter, plan.verse, book.short_title, book.code')
            .where('plan.day = :day', { day })
            .andWhere('reading.title IN (:...readings)', { readings })
            .getRawMany();

        return details.map(item => {
            const short = getBookShortCode(item, item.chapter, item.verse);

            return {
                plan_id: item.id,
                branch: item.branch,
                day,
                chapter: item.chapter,
                book_code: item.code,
                verse: item.verse,
                short,
            };
        });
    }
}
