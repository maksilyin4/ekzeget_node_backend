import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import BibleGroupsTypes from './BibleGroupsTypes';

@Index('idx_bible_group_type_active', ['active'], {})
@Entity('bible_group_type', { schema: 'ekzeget' })
export default class BibleGroupType extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @OneToMany(
        () => BibleGroupsTypes,
        bibleGroupsTypes => bibleGroupsTypes.type
    )
    bibleGroupsTypes: BibleGroupsTypes[];

    static async getGroupTypeAsString(ids: number[]): Promise<string> {
        const bgTypes = await BibleGroupType.find();
        return ids.map(id => bgTypes.find(bgt => bgt.id === id).title).join(', ');
    }
}
