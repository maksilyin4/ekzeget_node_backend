import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import BibleGroup from './BibleGroup';
import BibleGroupType from './BibleGroupType';

@Index('fk_bible_groups_types_group', ['groupId'], {})
@Index('fk_bible_groups_types_type', ['typeId'], {})
@Entity('bible_groups_types', { schema: 'ekzeget' })
export default class BibleGroupsTypes extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'group_id', nullable: true })
    groupId: number | null;

    @Column('int', { name: 'type_id', nullable: true })
    typeId: number | null;

    @ManyToOne(
        () => BibleGroup,
        bibleGroup => bibleGroup.bibleGroupsTypes,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'group_id', referencedColumnName: 'id' }])
    group: BibleGroup;

    @ManyToOne(
        () => BibleGroupType,
        bibleGroupType => bibleGroupType.bibleGroupsTypes,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'type_id', referencedColumnName: 'id' }])
    type: BibleGroupType;

    static async deleteByGroupId(groupId: number): Promise<void> {
        await BibleGroupsTypes.createQueryBuilder('bgt')
            .delete()
            .from(BibleGroupsTypes)
            .where(`bgt.group_id = :groupId`, { groupId })
            .execute();
    }

    static async createBibleGroupsTypesForBibleGroup({
        bibleGroupId,
        typeIds,
    }: {
        bibleGroupId: number;
        typeIds: number[];
    }): Promise<BibleGroupsTypes[]> {
        const promises = typeIds.map((typeId: number) =>
            BibleGroupsTypes.create({
                typeId: typeId,
                groupId: bibleGroupId,
            }).save()
        );
        return Promise.all(promises);
    }
}
