import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('idx_widget_text_key', ['key'], {})
@Entity('widget_text', { schema: 'ekzeget' })
export default class WidgetText extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'key', length: 255 })
    key: string;

    @Column('varchar', { name: 'title', length: 255 })
    title: string;

    @Column('text', { name: 'body' })
    body: string;

    @Column('smallint', { name: 'status', nullable: true })
    status: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;
}
