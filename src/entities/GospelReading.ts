import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Media from './Media';

@Index('dni', ['day'], { unique: true })
@Index('active11', ['active'], {})
@Entity('gospel_reading', { schema: 'ekzeget' })
export default class GospelReading extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'day', unique: true })
    day: number;

    @Column('text', { name: 'title' })
    title: string;

    @Column('text', { name: 'reading' })
    reading: string;

    @Column('int', { name: 'active', default: () => "'1'" })
    active: number;

    @OneToMany(
        () => Media,
        media => media.gospelReading
    )
    media: Media;
}
