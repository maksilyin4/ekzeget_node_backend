import {
    AfterLoad,
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';
import ArticleCategory from './ArticleCategory';
import ArticleAttachment from './ArticleAttachment';
import Slider from './Slider';
import FileStorageItem from './FileStorageItem';
import { RecordPresenter } from '../admin-panel/content/resources/processing/actions/recordProseccing/RecordPresenter';

@Index('fk_article_author', ['created_by'], {})
@Index('fk_article_updater', ['updated_by'], {})
@Index('fk_article_category', ['category_id'], {})
@Entity('article', { schema: 'ekzeget' })
export default class Article extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'slug', length: 1024 })
    slug: string;

    @Column('varchar', { name: 'title', length: 512 })
    title: string;

    @Column('mediumtext', { name: 'body' })
    body: string;

    @Column('varchar', { name: 'view', nullable: true, length: 255 })
    view: string | null;

    //@RelationId((article: Article) => article.category)
    @Column('int', { name: 'category_id', nullable: true })
    category_id: number | null;

    @Column('varchar', {
        name: 'thumbnail_base_url',
        nullable: true,
        length: 1024,
    })
    thumbnail_base_url: string | null;

    @Column('varchar', { name: 'thumbnail_path', nullable: true, length: 1024 })
    thumbnail_path: string | null;

    @Column('smallint', { name: 'status', default: () => "'0'" })
    status: number;

    @Column('int', { name: 'created_by', nullable: true })
    created_by: number | null;

    @Column('int', { name: 'updated_by', nullable: true })
    updated_by: number | null;

    @Column('int', { name: 'published_at', nullable: true })
    published_at: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updated_at: number | null;

    @Column('int', { name: 'image_id', nullable: true })
    image_id: number;

    @ManyToOne(
        () => User,
        user => user.articles,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'created_by', referencedColumnName: 'id' }])
    createdBy2: User;

    @ManyToOne(
        () => ArticleCategory,
        articleCategory => articleCategory.articles,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'category_id', referencedColumnName: 'id' }])
    category: ArticleCategory;

    @ManyToOne(
        () => User,
        user => user.articles2,
        {
            onDelete: 'SET NULL',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'updated_by', referencedColumnName: 'id' }])
    updatedBy2: User;

    @OneToMany(
        () => ArticleAttachment,
        articleAttachment => articleAttachment.article
    )
    article_attachments: ArticleAttachment[];

    @OneToMany(
        () => Slider,
        slider => slider.article
    )
    sliders: Slider[];

    @ManyToOne(() => FileStorageItem)
    @JoinColumn([{ name: 'image_id', referencedColumnName: 'id' }])
    private image_file: FileStorageItem;

    public image: string;

    @AfterLoad()
    setImage() {
        if (this.image_file) {
            this.image = this.image_file.base_url + '/' + this.image_file.path;
            delete this.image_file;
        }
    }
}
