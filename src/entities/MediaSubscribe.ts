import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Media from './Media';
import User from './User';

@Index('fk_media_subscribe_user', ['userId'], {})
@Index('fk_media_media_subscribe_media', ['mediaId'], {})
@Index('idx_media_subscribe_active', ['active'], {})
@Entity('media_subscribe', { schema: 'ekzeget' })
export default class MediaSubscribe extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'media_id', nullable: true })
    mediaId: number | null;

    @Column('varchar', { name: 'username', nullable: true, length: 255 })
    username: string | null;

    @Column('varchar', { name: 'user_email', nullable: true, length: 255 })
    userEmail: string | null;

    @Column('int', { name: 'user_id', nullable: true })
    userId: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('text', { name: 'back_url', nullable: true })
    backUrl: string | null;

    @ManyToOne(
        () => Media,
        media => media.mediaSubscribes,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;

    @ManyToOne(
        () => User,
        user => user.mediaSubscribes,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;
}
