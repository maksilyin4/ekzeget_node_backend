import { BaseEntity, Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import RbacAuthItem from './RbacAuthItem';

@Entity('rbac_auth_assignment', { schema: 'ekzeget' })
export default class RbacAuthAssignment extends BaseEntity {
    @Column('varchar', { primary: true, name: 'item_name', length: 64 })
    item_name: string;

    @Column('varchar', { primary: true, name: 'user_id', length: 64 })
    user_id: string;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @ManyToOne(
        () => RbacAuthItem,
        rbacAuthItem => rbacAuthItem.rbacAuthAssignments,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'item_name', referencedColumnName: 'name' }])
    itemName2: RbacAuthItem;
}
