import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Media from './Media';

@Index('den', ['den'], {})
@Index('color', ['color'], {})
@Index('idx_mineja_active', ['active'], {})
@Entity('mineja', { schema: 'ekzeget' })
export default class Mineja extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('char', { name: 'den', length: 10 })
    den: string;

    @Column('text', { name: 'title' })
    title: string;

    @Column('char', { name: 'znak', length: 10 })
    znak: string;

    @Column('text', { name: 'utr_chten' })
    utrChten: string;

    @Column('text', { name: 'apostol_chten' })
    apostolChten: string;

    @Column('text', { name: 'evang_chten' })
    evangChten: string;

    @Column('text', { name: 'dop_chten' })
    dopChten: string;

    @Column('char', { name: 'color', length: 20 })
    color: string;

    @Column('char', { name: 'abbr', length: 30 })
    abbr: string;

    @Column('int', { name: 'active', default: () => "'1'" })
    active: number;

    @OneToMany(
        () => Media,
        media => media.mineja
    )
    media: Media;
}
