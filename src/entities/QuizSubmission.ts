import { BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { IClientQuestion, IQuiz, TQuestionStatus } from '../core/App/interfaces/IQuiz';
import QueezeApplications from './QueezeApplications';

@Index('unique', ['applicationId', 'questionId', 'status'], { unique: true })
@Index('answer_id', ['status'], {})
@Index('question_id', ['questionId'], {})
@Entity('quiz_submission', { schema: 'ekzeget' })
export default class QuizSubmission extends BaseEntity {
    @Column('int', { primary: true, name: 'application_id', unsigned: true })
    applicationId: number;

    @Column('int', { primary: true, name: 'question_id', unsigned: true })
    questionId: number;

    @Column('int', { name: 'answer_id', nullable: true, unsigned: true })
    answerId: number;

    @Column('varchar', { name: 'status', length: 2, default: () => "'NA'" })
    status: string;

    @Column('int', { name: 'num', default: () => "'0'" })
    num: number;

    @ManyToOne(
        () => QueezeApplications,
        application => application.submissions,
    )
    @JoinColumn([{name: 'application_id', referencedColumnName: 'id'}])
    application: QueezeApplications;

    static async saveQuiz(quiz: IQuiz) {
        const values: QuizSubmission[] = [];

        for (const item of quiz.questions) {
            values.push({
                applicationId: quiz.id,
                questionId: item.id,
                status: item.status,
                num: item.num,
            } as QuizSubmission);
        }

        await this.createQueryBuilder('app')
            .insert()
            .values(values)
            .execute();
    }

    static async getQuestionsByQuizId(id: number): Promise<Partial<IClientQuestion>[]> {
        const questionsList = await this.createQueryBuilder('quest')
            .select(
                'quest.questionId as id, quest.status, quest.num, quest.answerId as submittedAnswerId'
            )
            .where('quest.applicationId = :id', { id })
            .orderBy('quest.num')
            .getRawMany();

        return questionsList;
    }

    static async updateStatus(
        applicationId: number,
        questionId: number,
        newStatus: TQuestionStatus,
        answerId
    ): Promise<void> {
        await this.createQueryBuilder()
            .update()
            .set({
                status: newStatus,
                answerId
            })
            .where('applicationId = :applicationId', { applicationId })
            .andWhere('questionId = :questionId', { questionId })
            .execute();
    }
}
