import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn, RelationId,
} from 'typeorm';
import Media from './Media';
import MediaAuthorCategory from './MediaAuthorCategory';
import FileStorageItem from './FileStorageItem';
import User from './User';

@Index('media_media_author_user', ['created_by'], {})
@Index('idx_media_author_active', ['active'], {})
@Index('media_media_author_image', ['image'], {})
@Index('fk_media_author_category', ['category_id'], {})
@Entity('media_author', { schema: 'ekzeget' })
export default class MediaAuthor extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('int', { name: 'image', nullable: true })
    image: number | null;

    //@RelationId((authors: MediaAuthor) => authors.createdBy2)
    @Column('int', { name: 'created_by', nullable: true })
    created_by: number | null;

    @Column('int', { name: 'edited_by', nullable: true })
    edited_by: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'edited_at', nullable: true })
    edited_at: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    //@RelationId((authors: MediaAuthor) => authors.category)
    @Column('int', { name: 'category_id', nullable: true })
    category_id: number | null;

    @OneToMany(
        () => Media,
        media => media.author
    )
    media: Media[];

    @ManyToOne(
        () => MediaAuthorCategory,
        mediaAuthorCategory => mediaAuthorCategory.mediaAuthors,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'category_id', referencedColumnName: 'id' }])
    category: MediaAuthorCategory;

    @ManyToOne(
        () => FileStorageItem,
        fileStorageItem => fileStorageItem.media_authors,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'image', referencedColumnName: 'id' }])
    image2: FileStorageItem;

    @ManyToOne(
        () => User,
        user => user.mediaAuthors,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'created_by', referencedColumnName: 'id' }])
    createdBy2: User;
}
