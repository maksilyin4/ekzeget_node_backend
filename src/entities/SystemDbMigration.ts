import { BaseEntity, Column, Entity } from 'typeorm';

@Entity('system_db_migration', { schema: 'ekzeget' })
export default class SystemDbMigration extends BaseEntity {
    @Column('varchar', { primary: true, name: 'version', length: 180 })
    version: string;

    @Column('int', { name: 'apply_time', nullable: true })
    applyTime: number | null;
}
