import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn, RelationId,
} from 'typeorm';
import Excuse from './Excuse';
import Preacher from './Preacher';
import PreachingVerse from './PreachingVerse';

@Index('preaching-code-idx', ['code'], { unique: true })
@Index('idx_preaching_active', ['active'], {})
@Index('fk_preaching_preacher', ['preacher_id'], {})
@Index('fk_preaching_excuse', ['excuse_id'], {})
@Entity('preaching', { schema: 'ekzeget' })
export default class Preaching extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    //@RelationId((preaching: Preaching) => preaching.preacher)
    @Column('int', { name: 'preacher_id', nullable: true })
    preacher_id: number | null;

    //@RelationId((preaching: Preaching) => preaching.excuse)
    @Column('int', { name: 'excuse_id', nullable: true })
    excuse_id: number | null;

    @Column('varchar', { name: 'theme', nullable: true, length: 255 })
    theme: string | null;

    @Column('mediumtext', { name: 'text', nullable: true })
    text: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @ManyToOne(
        () => Excuse,
        excuse => excuse.preachings,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'excuse_id', referencedColumnName: 'id' }])
    excuse: Excuse;

    @ManyToOne(
        () => Preacher,
        preacher => preacher.preachings,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'preacher_id', referencedColumnName: 'id' }])
    preacher: Preacher;

    @OneToMany(
        () => PreachingVerse,
        preachingVerse => preachingVerse.preaching
    )
    preaching_verses: PreachingVerse[];
}
