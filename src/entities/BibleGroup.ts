import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import BibleGroupsTypes from './BibleGroupsTypes';

@Index('bible_group-code-idx', ['code'], { unique: true })
@Entity('bible_group', { schema: 'ekzeget' })
export default class BibleGroup extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('text', { name: 'data' })
    data: string;

    @Column('varchar', { name: 'name', comment: 'Название', length: 255 })
    name: string;

    @Column('text', { name: 'foto' })
    foto: string;

    @Column('text', { name: 'history', comment: 'История' })
    history: string;

    @Column('text', {
        name: 'location',
        comment: 'Адрес: Страна, область, нас.пункт, улица, дом',
    })
    location: string;

    @Column('text', { name: 'days', comment: 'В какие дни проходят занятия' })
    days: string;

    @Column('text', { name: 'times', comment: 'Время начала' })
    times: string;

    @Column('text', { name: 'vedet', comment: 'Ведущий' })
    vedet: string;

    @Column('text', { name: 'priester', comment: 'Духовник' })
    priester: string;

    @Column('text', { name: 'phone', comment: 'телефон' })
    phone: string;

    @Column('text', { name: 'email', comment: 'email' })
    email: string;

    @Column('text', { name: 'web', comment: 'сайт' })
    web: string;

    @Column('text', { name: 'we_reads', comment: 'что мы читаем?' })
    we_reads: string;

    @Column('text', { name: 'kak_prohodit', comment: 'как проходят занятия?' })
    kak_prohodit: string;

    @Column('text', { name: 'type', comment: 'тип группы' })
    type: string;

    @Column('varchar', { name: 'coords', comment: 'Координаты', length: 255 })
    coords: string;

    @Column('varchar', { name: 'country', length: 255 })
    country: string;

    @Column('varchar', { name: 'region', length: 255 })
    region: string;

    @Column('int', { name: 'active', default: () => "'1'" })
    active: number;

    @Column('varchar', {
        name: 'code',
        nullable: false,
        unique: true,
        length: 255,
    })
    code: string;

    @OneToMany(
        () => BibleGroupsTypes,
        bibleGroupsTypes => bibleGroupsTypes.group
    )
    bibleGroupsTypes: BibleGroupsTypes[];
}
