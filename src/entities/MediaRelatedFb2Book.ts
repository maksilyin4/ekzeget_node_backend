import { BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import Media from './Media';

@Entity('media_related_fb2_book', { schema: 'ekzeget' })
export default class MediaRelatedFb2Book extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'media_id' })
    mediaId: number;

    @Column('varchar', { name: 'name', length: 255 })
    name: string;

    @Column('datetime', {
        name: 'created_at',
        nullable: true,
        default: () => 'CURRENT_TIMESTAMP',
    })
    createdAt: Date | null;

    @Column('datetime', { name: 'updated_at', nullable: true })
    updatedAt: Date | null;

    @Column('int', { name: 'status' })
    status: number;

    @OneToOne(
        () => Media,
        media => media.mediaRelatedFb2Book,
        {
            onDelete: 'CASCADE',
            onUpdate: 'NO ACTION',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;
}
