import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('widget_menu', { schema: 'ekzeget' })
export default class WidgetMenu extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'key', length: 32 })
    key: string;

    @Column('varchar', { name: 'title', length: 255 })
    title: string;

    @Column('text', { name: 'items' })
    items: string;

    @Column('smallint', { name: 'status', default: () => "'0'" })
    status: number;
}
