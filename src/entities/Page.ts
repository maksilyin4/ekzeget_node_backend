import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('page', { schema: 'ekzeget' })
export default class Page extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'slug', length: 2048 })
    slug: string;

    @Column('varchar', { name: 'title', length: 512 })
    title: string;

    @Column('text', { name: 'body' })
    body: string;

    @Column('varchar', { name: 'view', nullable: true, length: 255 })
    view: string | null;

    @Column('smallint', { name: 'status' })
    status: number;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;
}
