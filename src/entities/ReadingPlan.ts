import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    In,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import UserReadingPlan from './UserReadingPlan';
import GroupReadingPlan from './GroupReadingPlan';
import User from './User';
import Reading from './Reading';

@Index('active', ['active'], {})
@Index('idx_reading_plan_s', ['sort'], {})
@Entity('reading_plan', { schema: 'ekzeget' })
export default class ReadingPlan extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', length: 50 })
    title: string;

    @Column('varchar', { name: 'description', length: 255 })
    description: string;

    @Column('int', { name: 'lenght' })
    lenght: number;

    @Column('varchar', { name: 'plan', length: 255 })
    plan: string;

    @Column('text', { name: 'comment' })
    comment: string;

    @Column('int', { name: 'active', default: () => '\'1\'' })
    active: number;

    @Column('int', { name: 'sort', nullable: true })
    sort: number | null;

    @Column('int', { name: 'creator_id', nullable: true })
    creator_id: User['id'];

    @CreateDateColumn({ type: 'datetime', name: 'created_at', default: () => 'CURRENT_TIMESTAMP' })
    created_at: Date;

    @OneToMany(
        () => UserReadingPlan,
        userReadingPlan => userReadingPlan.plan,
    )
    userReadingPlans: UserReadingPlan[];

    @OneToMany(
        () => GroupReadingPlan,
        groupReadingPlan => groupReadingPlan.plan,
    )
    groupReadingPlans: GroupReadingPlan[];

    @ManyToOne(
        () => User,
        user => user.readingPlans,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn([{ name: 'creator_id', referencedColumnName: 'id' }])
    creator: User;

    getReadings(): Promise<Array<Reading>> {
        const readingTitles = this.plan.split(',').map(it => it.trim());
        return Reading.find({ where: { title: In(readingTitles) } });
    }
}
