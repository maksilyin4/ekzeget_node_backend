import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';
import MediaTags from './MediaTags';
import QueezeQuestions from './QueezeQuestions';
import Verse from './Verse';

@Index('media_tag-code-idx', ['code'], { unique: true })
@Index('media_tag_user', ['createdBy'], {})
@Index('idx_media_tag_active', ['active'], {})
@Index('media_tag-title-idx', ['title'], {})
@Entity('media_tag', { schema: 'ekzeget' })
export default class MediaTag extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('int', { name: 'created_by', nullable: true })
    createdBy: number | null;

    @Column('int', { name: 'edited_by', nullable: true })
    editedBy: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'edited_at', nullable: true })
    editedAt: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @ManyToOne(
        () => User,
        user => user.mediaTags,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'created_by', referencedColumnName: 'id' }])
    createdBy2: User;

    @OneToMany(
        () => MediaTags,
        mediaTags => mediaTags.tag
    )
    mediaTags: MediaTags[];

    static async getRelationTagsByMediaId(mediaId: number) {
        const relationTags: Array<Pick<MediaTag, 'id' | 'title'>> = await this.createQueryBuilder('tag')
            .select('id, title')
            .where(db => {
                const subQuery = db
                    .subQuery()
                    .select('tag_id')
                    .from(MediaTags, 'mediaTags')
                    .where('mediaTags.media_id = (:mediaId)', { mediaId })
                    .getQuery();
                return 'tag.id IN ' + subQuery;
            })
            .getRawMany();

        return relationTags;
    }
}
