import {BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn} from 'typeorm';
import EnvironmentManager from '../managers/EnvironmentManager/index';

@Index('idx-config-name', ['name'], {})
@Entity('config', {schema: 'ekzeget'})
export default class Config extends BaseEntity {
    @PrimaryGeneratedColumn({type: 'int', name: 'id'})
    id: number;

    @Column('varchar', {name: 'name', length: 255})
    name: string;

    @Column('text', {name: 'value', nullable: true})
    value: string | null;

    static async getTutorialForQuiz() {
        const tutorial = await Config.findOne({
            where: {name: 'queeze_tutorial_video_url'},
        });

        return tutorial;
    }

    static async getPathToAudioForQuiz() {
        const config = await Config.createQueryBuilder()
            .where(
                'name IN ("quiz_wrong_audio", "quiz_introduction_audio", "quiz_questions_audio_dir")'
            )
            .getMany();

        const paths: Partial<{
            quiz_wrong_audio: string;
            quiz_introduction_audio: string;
            quiz_questions_audio_dir: string;
        }> = {};

        config.map(item => (paths[item.name] = item.value));

        const cdnPath = EnvironmentManager.envs.STORAGE_MEDIA_CDN;

        return {
            wrong: cdnPath + paths.quiz_questions_audio_dir + '/' + paths.quiz_wrong_audio,
            introduction:
                cdnPath + paths.quiz_questions_audio_dir + '/' + paths.quiz_introduction_audio,
        };
    }

    static async getRankedVideoSources() {
        return (
            await Config.findOne({
                select: ['value'],
                where: {
                    name: 'ranked_video_sources',
                },
                cache: true,
            })
        ).value
            .replace(/ /g, '')
            .split(',');
    }
}
