import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';
import Media from './Media';

@Index('fk_media_comment_user', ['created_by'], {})
@Index('fk_media_media_comment_media', ['media_id'], {})
@Index('idx_media_comment_active', ['active'], {})
@Entity('media_comment', { schema: 'ekzeget' })
export default class MediaComment extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('text', { name: 'comment', nullable: true })
    comment: string | null;

    @Column('int', { name: 'media_id', nullable: true })
    media_id: number | null;

    @Column('varchar', { name: 'username', nullable: true, length: 255 })
    username: string | null;

    @Column('varchar', { name: 'user_email', nullable: true, length: 255 })
    user_email: string | null;

    @Column('int', { name: 'created_by', nullable: true })
    created_by: number | null;

    @Column('int', { name: 'edited_by', nullable: true })
    edited_by: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'edited_at', nullable: true })
    edited_at: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('int', { name: 'activated_at', nullable: true })
    activated_at: number | null;

    @ManyToOne(
        () => User,
        user => user.mediaComments,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'created_by', referencedColumnName: 'id' }])
    createdBy2: User;

    @ManyToOne(
        () => Media,
        media => media.mediaComments,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'media_id', referencedColumnName: 'id' }])
    media: Media;
}
