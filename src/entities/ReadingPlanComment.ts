import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import User from './User';
import GroupReadingPlan from './GroupReadingPlan';

@Entity('reading_plan_comment', { schema: 'ekzeget' })
export default class ReadingPlanComment extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'author_id', nullable: false })
    author_id: number;

    @Column('int', { name: 'day', unsigned: true, nullable: false })
    day: number;

    @Column('int', { name: 'group_id', unsigned: true, nullable: true })
    group_id: number;

    @Column('text', { name: 'content' })
    content: string;

    @Column('int', { name: 'reply_to_comment_id', nullable: true })
    reply_to_comment_id: number;

    @CreateDateColumn({
        type: 'timestamp',
        name: 'created_at',
        default: () => 'CURRENT_TIMESTAMP',
    })
    created_at: Date;

    @UpdateDateColumn({
        type: 'timestamp',
        name: 'updated_at',
        default: () => 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        onUpdate: 'CURRENT_TIMESTAMP',
    })
    updated_at: Date;

    @ManyToOne(
        () => User,
        user => user.readingPlanComments,
    )
    @JoinColumn([{ name: 'author_id', referencedColumnName: 'id' }])
    author: User;

    @ManyToOne(
        () => GroupReadingPlan,
        group => group.comments,
    )
    @JoinColumn([{ name: 'group_id', referencedColumnName: 'id' }])
    group: GroupReadingPlan;

    @ManyToOne(
        () => ReadingPlanComment,
        comment => comment.replies,
        {},
    )
    @JoinColumn([{ name: 'reply_to_comment_id', referencedColumnName: 'id' }])
    replyTo: ReadingPlanComment;

    @OneToMany(
        () => ReadingPlanComment,
        comment => comment.replyTo,
    )
    replies: ReadingPlanComment[];
}
