import { BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import RbacAuthItem from './RbacAuthItem';

@Index('child', ['child'], {})
@Entity('rbac_auth_item_child', { schema: 'ekzeget' })
export default class RbacAuthItemChild extends BaseEntity {
    @Column('varchar', { primary: true, name: 'parent', length: 64 })
    parent: string;

    @Column('varchar', { primary: true, name: 'child', length: 64 })
    child: string;

    @ManyToOne(
        () => RbacAuthItem,
        rbacAuthItem => rbacAuthItem.rbacAuthItemChildren,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'parent', referencedColumnName: 'name' }])
    parent2: RbacAuthItem;

    @ManyToOne(
        () => RbacAuthItem,
        rbacAuthItem => rbacAuthItem.rbacAuthItemChildren2,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'child', referencedColumnName: 'name' }])
    child2: RbacAuthItem;
}
