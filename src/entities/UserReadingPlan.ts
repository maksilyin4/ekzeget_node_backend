import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn, JoinTable, ManyToMany,
    ManyToOne, OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import ReadingPlan from './ReadingPlan';
import User from './User';
import Translate from './Translate';
import { unserialize } from 'php-serialize';
import Reading from './Reading';
import Plan from './Plan';
import GroupReadingPlan from './GroupReadingPlan';
import UserReadingPlanClosedPlans from './UserReadingPlanClosedPlans';

@Index('fk_user_reading_plan_user', ['user_id'], {})
@Index('fk_user_reading_plan_group', ['group_id'], {})
@Index('fk_user_reading_plan_plan', ['plan_id'], {})
@Index('idx_user_reading_plan_s', ['subscribe'], {})
@Index('idx_user_reading_plantranslate_id', ['translate_id'], {})
@Entity('user_reading_plan', { schema: 'ekzeget' })
export default class UserReadingPlan extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'group_id', nullable: true, default: null })
    group_id: number | null = null;

    @Column('int', { name: 'user_id', nullable: true })
    user_id: number | null;

    @Column('int', { name: 'plan_id', nullable: true })
    plan_id: number | null;

    @Column('date', { name: 'start_date', nullable: true })
    start_date: string | null;

    @Column('date', { name: 'stop_date', nullable: true })
    stop_date: string | null;

    @Column('int', { name: 'subscribe', nullable: true, default: () => "'0'" })
    subscribe: number | null;

    @Column('int', { name: 'translate_id', nullable: true })
    translate_id: number | null;

    @Column('text', { name: 'schedule', nullable: true })
    schedule: string | null;

    @Column('date', { name: 'check_date', nullable: true })
    check_date: string | null;

    @ManyToOne(
        () => GroupReadingPlan,
        group => group.users,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
    )
    @JoinColumn([{ name: 'group_id', referencedColumnName: 'id' }])
    group: GroupReadingPlan;

    @ManyToOne(
        () => ReadingPlan,
        readingPlan => readingPlan.userReadingPlans,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'plan_id', referencedColumnName: 'id' }])
    plan: ReadingPlan;

    @ManyToOne(
        () => User,
        user => user.userReadingPlans,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;

    @ManyToOne(
        () => Translate,
        translate => translate.userReadingPlans,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'translate_id', referencedColumnName: 'id' }])
    translate: Translate;

    @ManyToMany(() => Plan)
    @JoinTable({
        name: 'user_reading_plan_closed_plans',
        joinColumn: { name: 'user_reading_plan_id', referencedColumnName: 'id' },
        inverseJoinColumn: { name: 'plan_id', referencedColumnName: 'id' },
    })
    closedPlans: Plan[];

    reading;
    public async setFieldsForClient(day?: number) {
        const defaultSchedule = { Mon: 1, Tue: 1, Wed: 1, Thu: 1, Fri: 1, Sat: 1, Sun: 1 };

        const currentDay = +((Date.now() - Date.parse(this.start_date)) / 1000 / 86400).toFixed();
        const readingDay = day || currentDay;

        this.schedule = this.schedule ? unserialize(this.schedule) : defaultSchedule;

        const planTitles = this.plan.plan.split(',').map(it => it.trim());

        if (!this.closedPlans) {
            const closedPlans = await UserReadingPlanClosedPlans.find({
                where: { user_reading_plan_id: this.id },
                relations: ['plan'],
            });
            this.closedPlans = closedPlans.map(it => it.plan);
        }

        const getPlan = async planTitle => {
            const reading = await Reading.findOne({ where: { title: planTitle.trim() } });
            const plans = await Plan.find({
                where: { reading_id: reading.id, day: readingDay },
                relations: ['book'],
            });

            return plans.map(plan => {
                let short = `${plan.book.short_title}.`;
                if (plan.chapter)
                    short += ` ${plan.chapter}`;
                if (plan.verse)
                    short += `:${plan.verse}`;
                return {
                    ...plan,
                    short,
                    book_code: plan.book.code,
                    is_closed: Boolean(this.closedPlans.find(it => it.id === plan.id)),
                };
            });
        };

        this.reading = await Promise.all(planTitles.map(getPlan));
    }
}
