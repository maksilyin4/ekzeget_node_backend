import {
    BaseEntity,
    Column,
    DeleteResult,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Interpretation from './Interpretation';
import Verse from './Verse';

@Index('fk_interpretation_verse_link_interpretation', ['interpretationId'], {})
@Index('fk_interpretation_verse_link_verse', ['verseId'], {})
@Entity('interpretation_verse_link', { schema: 'ekzeget' })
export default class InterpretationVerseLink extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'verse_id', nullable: true })
    verseId: number | null;

    @Column('int', { name: 'interpretation_id', nullable: true })
    interpretationId: number | null;

    @ManyToOne(
        () => Interpretation,
        interpretation => interpretation.interpretation_verse_links,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'interpretation_id', referencedColumnName: 'id' }])
    interpretation: Interpretation;

    @ManyToOne(
        () => Verse,
        verse => verse.interpretation_verse_links,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;

    public static async newMentionedVerses(text: string, interpretationId: number): Promise<void> {
        const mentionedVerses: number[] = await this.getMentionedVersesByText(text);
        const saveMentionedVersesPromises: Promise<InterpretationVerseLink>[] = mentionedVerses.map(
            (mentionedVerseId: number) =>
                InterpretationVerseLink.create({
                    verseId: mentionedVerseId,
                    interpretationId: interpretationId,
                }).save()
        );

        await Promise.all(saveMentionedVersesPromises);
    }

    public static async editMentionedVerses(text: string, interpretationId: number): Promise<void> {
        const mentionedVerses: Set<number> = new Set(await this.getMentionedVersesByText(text));

        const oldMentionedVerses: Set<number> = new Set(
            (
                await InterpretationVerseLink.find({
                    select: ['verseId'],
                    where: {
                        interpretationId,
                    },
                })
            ).map(value => value.verseId)
        );

        for (const mentionedVerse of mentionedVerses) {
            if (oldMentionedVerses.has(mentionedVerse)) {
                mentionedVerses.delete(mentionedVerse);
                oldMentionedVerses.delete(mentionedVerse);
            }
        }

        const saveMentionedVersesPromises: Promise<InterpretationVerseLink>[] = [
            ...mentionedVerses,
        ].map((mentionedVerseId: number) =>
            InterpretationVerseLink.create({
                verseId: mentionedVerseId,
                interpretationId: interpretationId,
            }).save()
        );

        const deleteOldMentionedVersesPromises: Promise<DeleteResult>[] = [
            ...oldMentionedVerses,
        ].map((verseId: number) => InterpretationVerseLink.delete({ interpretationId, verseId }));

        await Promise.all([saveMentionedVersesPromises, deleteOldMentionedVersesPromises]);
    }

    /**
     * Возвращает только короткие коды ссылок на стихи
     */
    private static async getMentionedVersesByText(text: string) {
        function detectMentionedVerses(originalText: string): string[] {
            const regExp: RegExp = /{{{(.*?)}}}/;
            const matchedSubstrings = originalText.match(new RegExp(regExp, 'gsm'));
            if (!matchedSubstrings) return [];

            return matchedSubstrings.map(str => str.substring(3, str.length - 3));
        }

        async function getMentionedVerses(links: string[]): Promise<number[]> {
            const versesPromises: Promise<Verse[]>[] = links.map(async link =>
                Verse.getByShortLink(link)
            );

            const verses: Verse[][] = await Promise.all(versesPromises);
            const versesIds: number[] = verses.flat().map(v => v.id);

            const verseIdSet = new Set(versesIds);
            return [...verseIdSet.values()];
        }

        const mentionedVersesLinks: string[] = detectMentionedVerses(text);
        return await getMentionedVerses(mentionedVersesLinks);
    }
}
