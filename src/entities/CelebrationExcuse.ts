import { BaseEntity, Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import Celebration from './Celebration';
import Excuse from './Excuse';

@Index('idx_celebration_excuse_active', ['active'], {})
@Entity('celebration_excuse', { schema: 'ekzeget' })
export default class CelebrationExcuse extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @OneToMany(
        () => Celebration,
        celebration => celebration.excuse
    )
    celebrations: Celebration[];

    @OneToMany(
        () => Excuse,
        excuse => excuse.parent
    )
    excuses: Excuse[];
}
