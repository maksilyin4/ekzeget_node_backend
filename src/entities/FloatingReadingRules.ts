import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Index('day_of_week', ['day_of_week'], { unique: true })
@Index('loating_chten_id', ['floating_chten_id'], {})
@Entity('floating_chten_rules', { schema: 'ekzeget' })
export default class FloatingReadingRules extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('text', { name: 'date' })
    date: string;

    @Column('int', { name: 'day_of_week', default: () => "'0'" })
    day_of_week: number;

    @Column('int', { name: 'floating_chten_id', default: () => "'0'" })
    floating_chten_id: number;
}
