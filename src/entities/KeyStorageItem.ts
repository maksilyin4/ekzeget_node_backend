import { BaseEntity, Column, Entity, Index } from 'typeorm';

@Index('idx_key_storage_item_key', ['key'], { unique: true })
@Entity('key_storage_item', { schema: 'ekzeget' })
export default class KeyStorageItem extends BaseEntity {
    @Column('varchar', { primary: true, name: 'key', length: 128 })
    key: string;

    @Column('text', { name: 'value' })
    value: string;

    @Column('text', { name: 'comment', nullable: true })
    comment: string | null;

    @Column('int', { name: 'updated_at', nullable: true })
    updatedAt: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    createdAt: number | null;
}
