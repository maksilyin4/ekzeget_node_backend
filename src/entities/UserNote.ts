import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Book from './Book';
import User from './User';

@Index('idx_user_note_user', ['userId'], {})
@Index('idx_user_note_book', ['bookId'], {})
@Entity('user_note', { schema: 'ekzeget' })
export default class UserNote extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'user_id', nullable: true })
    userId: number | null;

    @Column('int', { name: 'book_id', nullable: true })
    bookId: number | null;

    @Column('varchar', { name: 'chapter', nullable: true, length: 255 })
    chapter: string | null;

    @Column('varchar', { name: 'number', nullable: true, length: 255 })
    number: string | null;

    @Column('text', { name: 'text', nullable: true })
    text: string | null;

    @ManyToOne(
        () => Book,
        book => book.user_notes,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'book_id', referencedColumnName: 'id' }])
    book: Book;

    @ManyToOne(
        () => User,
        user => user.userNotes,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;
}
