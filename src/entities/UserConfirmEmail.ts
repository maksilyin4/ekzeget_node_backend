import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';

@Index('idx_user_confirm_email', ['user_id'], {})
@Entity('user_confirm_email', { schema: 'ekzeget' })
export default class UserConfirmEmail extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'user_id', nullable: true })
    user_id: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('varchar', { name: 'secret', nullable: true, length: 64 })
    secret: string | null;

    @ManyToOne(
        () => User,
        user => user.userConfirmEmails,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
    user: User;
}
