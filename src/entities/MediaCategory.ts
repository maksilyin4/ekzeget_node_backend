import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn, RelationId,
} from 'typeorm';
import Media from './Media';
import User from './User';
import FileStorageItem from './FileStorageItem';

@Index('media_category-code-idx', ['code'], { unique: true })
@Index('media_category_user', ['created_by'], {})
@Index('media_media_category_image', ['image'], {})
@Index('idx_media_category_active', ['active'], {})
@Index('media_category-parent_id-idx', ['parent_id'], {})
@Entity('media_category', { schema: 'ekzeget' })
export default class MediaCategory extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'title', nullable: true, length: 255 })
    title: string | null;

    @Column('text', { name: 'description', nullable: true })
    description: string | null;

    @Column('int', { name: 'image', nullable: true })
    image: number | null;

    //@RelationId((mediaCategory: MediaCategory) => mediaCategory.createdBy2)
    @Column('int', { name: 'created_by', nullable: true })
    created_by: number | null;

    @Column('int', { name: 'edited_by', nullable: true })
    edited_by: number | null;

    @Column('int', { name: 'created_at', nullable: true })
    created_at: number | null;

    @Column('int', { name: 'edited_at', nullable: true })
    edited_at: number | null;

    @Column('int', { name: 'active', nullable: true })
    active: number | null;

    @Column('int', { name: 'parent_id', default: () => "'0'" })
    parent_id: number;

    @Column('varchar', {
        name: 'code',
        nullable: true,
        unique: true,
        length: 255,
    })
    code: string | null;

    @Column('int', { name: 'single_template', nullable: true })
    single_template: number | null;

    @Column('varchar', { name: 'template_code', nullable: true, length: 2 })
    template_code: string | null;

    @OneToMany(
        () => Media,
        media => media.main_category_category
    )
    media: Media[];

    @ManyToMany(
        () => Media,
        media => media.mediaCategories
    )
    @JoinTable({
        name: 'media_categories',
        joinColumns: [{ name: 'category_id', referencedColumnName: 'id' }],
        inverseJoinColumns: [{ name: 'media_id', referencedColumnName: 'id' }],
        schema: 'ekzeget',
    })
    media2: Media[];

    @ManyToOne(
        () => User,
        user => user.mediaCategories,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'created_by', referencedColumnName: 'id' }])
    createdBy2: User;

    @ManyToOne(
        () => FileStorageItem,
        fileStorageItem => fileStorageItem.media_categories,
        { onDelete: 'CASCADE', onUpdate: 'CASCADE' }
    )
    @JoinColumn([{ name: 'image', referencedColumnName: 'id' }])
    image2: FileStorageItem;
}
