import {
    BaseEntity,
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import Preaching from './Preaching';
import Verse from './Verse';

@Index('fk_preaching_verse_preaching', ['preachingId'], {})
@Index('fk_preaching_verse_verse', ['verseId'], {})
@Entity('preaching_verse', { schema: 'ekzeget' })
export default class PreachingVerse extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('int', { name: 'preaching_id', nullable: true })
    preachingId: number | null;

    @Column('int', { name: 'verse_id', nullable: true })
    verseId: number | null;

    @ManyToOne(
        () => Preaching,
        preaching => preaching.preaching_verses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'preaching_id', referencedColumnName: 'id' }])
    preaching: Preaching;

    @ManyToOne(
        () => Verse,
        verse => verse.preaching_verses,
        {
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
        }
    )
    @JoinColumn([{ name: 'verse_id', referencedColumnName: 'id' }])
    verse: Verse;
}
