import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('text_block', { schema: 'ekzeget' })
export default class TextBlock extends BaseEntity {
    @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
    id: number;

    @Column('varchar', { name: 'code', length: 512 })
    code: string;

    @Column('varchar', { name: 'title', length: 512 })
    title: string;

    @Column('text', { name: 'text' })
    text: string;

    @Column('boolean', { name: 'active' })
    active: number;
}