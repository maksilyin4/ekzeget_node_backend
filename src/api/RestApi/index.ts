import IRestApi, { IRoute, THandler } from '../../core/App/interfaces/IRestApi';
import Controller from './Controller';
import { IResponseBody, TextResponseStatus } from './types';
import listRoutes from './routes';
import ErrorHandler from '../../libs/ErrorHandler';
import getUnixSec from '../../libs/helpers/getUnixSec';
import doFunctionWithTiming from '../../libs/helpers/doFunctionWithTiming';
import User from '../../entities/User';

export default class RestApi implements IRestApi {
    private _routes: IRoute[] = listRoutes.call(this);

    wrapController(controller: Controller): THandler {
        return async (req, res, next) => {
            const inputParams = {
                ...req.query,
                ...req.params,
                ...req.body,
            };

            try {
                const responseBody = {} as IResponseBody;
                let finishRightAfter = false;

                const { result, timing } = await doFunctionWithTiming(async () => {
                    controller.inputValidate(inputParams);

                    const result = await controller.handle(inputParams, {
                        session: req.session,
                        user: req.user as User,
                        cookie: { ...req.cookies },
                        clientHost: req.headers.origin,
                        finishResponseFn: () => {
                            finishRightAfter = true;
                        },
                        redirectFn: url => {
                            res.redirect(url);
                        },
                        getResponseFn: () => res,
                    });

                    cookieProcessor(result);
                    controller.outputValidate(result);

                    return result;
                });

                if (finishRightAfter) {
                    return;
                }

                responseBody.controller_working_time = timing;
                responseBody.status = TextResponseStatus.ok;
                responseBody.server_time = getUnixSec().toString();

                noCacheForQuiz();
                res.setHeader('Content-Type', 'application/json');

                res.end(JSON.stringify(Object.assign(responseBody, result)));
            } catch (err) {
                ErrorHandler.handleFailedRequest(req, res, err);

                res.setHeader('Content-Type', 'application/json');
                res.end(
                    JSON.stringify({
                        status: 'error',
                        error: {
                            name: err.message,
                            message: err.message,
                            code: err.statusCode,
                        },
                    })
                );
            }

            function noCacheForQuiz() {
                if (req.url.includes('/api/v1/queeze/')) {
                    res.setHeader('Cache-Control', ['no-cache', 'no-store', 'must-revalidate']);
                }
            }

            function cookieProcessor(processResult) {
                if (typeof processResult === 'object' && processResult != null) {
                    if ('cookie' in processResult) {
                        for (const key in processResult.cookie) {
                            res.cookie(key, processResult.cookie[key], {
                                secure: false,
                                httpOnly: false,
                                expires: new Date(Date.now() + 60 * 60 * 24 * 365 * 1000),
                            });
                        }
                        delete processResult['cookie'];
                    }
                }
            }
        };
    }

    get routes() {
        return this._routes;
    }
}
