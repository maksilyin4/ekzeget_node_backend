import ArticleDetail from './Controller/controllers/Article/ArticleDetail';
import ArticleArticle from './Controller/controllers/Article/ArticleArticle';
import BibleGroupBibleGroup from './Controller/controllers/BibleGroup/BibleGroupBibleGroup';
import BibleGroupNote from './Controller/controllers/BibleGroup/BibleGroupNote';
import BibleGroupType from './Controller/controllers/BibleGroup/BibleGroupType';
import BibleGroupNoteDetail from './Controller/controllers/BibleGroup/BibleGroupNoteDetail';
import BibleGroupAdd from './Controller/controllers/BibleGroup/BibleGroupAdd';
import BibleGroupDelete from './Controller/controllers/BibleGroup/BibleGroupDelete';
import BibleGroupEdit from './Controller/controllers/BibleGroup/BibleGroupEdit';
import BibleMapsList from './Controller/controllers/BibleMaps/BibleMapsList';
import BookList from './Controller/controllers/Book/BookList';
import BookInfo from './Controller/controllers/Book/BookInfo';
import BookTranslate from './Controller/controllers/Book/BookTranslate';
import CelebrationExcuse from './Controller/controllers/Celebration/CelebrationExcuse';
import DictionaryDictionary from './Controller/controllers/Dictionary/DictionaryDictionary';
import DictionaryDetail from './Controller/controllers/Dictionary/DictionaryDetail';
import DictionaryList from './Controller/controllers/Dictionary/DictionaryList';
import EkzegetEkzeget from './Controller/controllers/Ekzeget/EkzegetEkzeget';
import EkzegetListForChapter from './Controller/controllers/Ekzeget/EkzegetListForChapter';
import EkzegetVerseList from './Controller/controllers/Ekzeget/EkzegetVerseList';
import InterpretationInterpretation from './Controller/controllers/Interpretation/InterpretationInterpretation';
import InterpretationLink from './Controller/controllers/Interpretation/InterpretationLink';
import LectureList from './Controller/controllers/Lecture/LectureList';
import LectureIndex from './Controller/controllers/Lecture/LectureIndex';
import LectureCategory from './Controller/controllers/Lecture/LectureCategory';
import LectureDetail from './Controller/controllers/Lecture/LectureDetail';
import MediaGetMediaAudioArchive from './Controller/controllers/Media/MediaGetMediaAudioArchive';
import MediaGetStatisticsViewsCount from './Controller/controllers/Media/MediaGetStatisticsViewsCount';
import MediaGetStatisticsViewsIncrease from './Controller/controllers/Media/MediaGetStatisticsViewsIncrease';
import MediaGetStatisticsDownloadsCount from './Controller/controllers/Media/MediaGetStatisticsDownloadsCount';
import MediaGetStatisticsDownloadsIncrease from './Controller/controllers/Media/MediaGetStatisticsDownloadsIncrease';
import MetaMeta from './Controller/controllers/Meta/MetaMeta';
import GetMediaAutocomplete from './Controller/controllers/Media/GetMediaAutocomplete';
import GetTagsAutocomplete from './Controller/controllers/Media/GetTagsAutocomplete';
import GetFtpList from './Controller/controllers/Admin/GetFtpList';
import GetPathToAudio from './Controller/controllers/Admin/GetPathToAudio';
import PreachingPreaching from './Controller/controllers/Preaching/PreachingPreaching';
import PreachingExcuse from './Controller/controllers/Preaching/PreachingExcuse';
import PreachingPreacher from './Controller/controllers/Preaching/PreachingPreacher';
import PreachingPreachingByBookAndChapter from './Controller/controllers/Preaching/PreachingPreachingByBookAndChapter';
import QueezeContinue from './Controller/controllers/Queeze/QueezeContinue';
import QueezeGetList from './Controller/controllers/Queeze/QueezeGetList/index';
import QueezeGetTutorial from './Controller/controllers/Queeze/QueezeGetTutorial';
import QueezeCurrent from './Controller/controllers/Queeze/QueezeCurrent';
import QueezeGetBooks from './Controller/controllers/Queeze/QueezeGetBooks';
import QueezeCreate from './Controller/controllers/Queeze/QueezeCreate';
import QueezeAnswer from './Controller/controllers/Queeze/QueezeAnswer';
import QueezeQuestionList from './Controller/controllers/Queeze/QueezeQuestionList';
import QueezeComplete from './Controller/controllers/Queeze/QueezeComplete';
import QueezeQuestion from './Controller/controllers/Queeze/QueezeQuestion';
import QueezeCramAudio from './Controller/controllers/Queeze/QueezeCramAudio';
import ReadingIndex from './Controller/controllers/Reading/ReadingIndex';
import ReadingPlan from './Controller/controllers/Reading/ReadingPlan';
import ReadingPlanDetail from './Controller/controllers/Reading/ReadingPlanDetail';
import SiteContact from './Controller/controllers/Site/SiteContact';
import SiteCaptcha from './Controller/controllers/Site/SiteCaptcha';
import SiteEaster from './Controller/controllers/Site/SiteEaster';
import SiteFileExist from './Controller/controllers/Site/SiteFileExist';
import SliderSlider from './Controller/controllers/Slider/SliderSlider';
import UserUser from './Controller/controllers/User/UserUser';
import UserUpdate from './Controller/controllers/User/UserUpdate';
import UserInterpretationFavGet from './Controller/controllers/User/UserInterpretationFavGet';
import UserInterpretationFavPut from './Controller/controllers/User/UserInterpretationFavPut';
import UserInterpretationFavDelete from './Controller/controllers/User/UserInterpretationFavDelete';
import UserVerseFavGet from './Controller/controllers/User/UserVerseFavGet';
import UserVerseFavPost from './Controller/controllers/User/UserVerseFavPost';
import UserVerseFavPut from './Controller/controllers/User/UserVerseFavPut';
import UserVerseFavDelete from './Controller/controllers/User/UserVerseFavDelete';
import UserPlanPut from './Controller/controllers/User/UserPlanPut';
import UserPlanPost from './Controller/controllers/User/UserPlanPost';
import UserPlanGet from './Controller/controllers/User/UserPlanGet';
import UserPlanDelete from './Controller/controllers/User/UserPlanDelete';
import UserBookmarkGet from './Controller/controllers/User/UserBookmarkGet';
import UserBookmarkPut from './Controller/controllers/User/UserBookmarkPut';
import UserBookmarkDelete from './Controller/controllers/User/UserBookmarkDelete';
import UserNoteGet from './Controller/controllers/User/UserNoteGet';
import UserNotePost from './Controller/controllers/User/UserNotePost';
import UserNotePut from './Controller/controllers/User/UserNotePut';
import UserNoteDelete from './Controller/controllers/User/UserNoteDelete';
import UserInterpretationAdd from './Controller/controllers/User/UserInterpretationAdd';
import UserInterpretationGet from './Controller/controllers/User/UserInterpretationGet';
import UserCreate from './Controller/controllers/User/UserCreate';
import UserLogin from './Controller/controllers/User/UserLogin';
import UserResetPassword from './Controller/controllers/User/UserResetPassword';
import UserRegistrationConfirm from './Controller/controllers/User/UserRegistrationConfirm';
import VerseVerse from './Controller/controllers/Verse/VerseVerse';
import VerseDetailByNumber from './Controller/controllers/Verse/VerseDetailByNumber';
import GetWordsAutocomplete from './Controller/controllers/Verse/GetWordsAutocomplete';
import VerseInfo from './Controller/controllers/Verse/VerseInfo';
import VerseParallel from './Controller/controllers/Verse/VerseParallel';
import Config from './Controller/controllers/Config/getParameter';
import DevUpdateArticles from './Controller/controllers/_Dev/updateArticles';
import DevResetCache from './Controller/controllers/_Dev/resetCache';
import DevSendReadings from './Controller/controllers/_Dev/sendReadings';
import DevRegenerateFb2Chapters from './Controller/controllers/_Dev/regenerateFb2Chapters';
import { IRoute } from '../../core/App/interfaces/IRestApi';
import EkzegetBookList from './Controller/controllers/Ekzeget/EkzegetBookList';
import EkzegetChaptersByBook from './Controller/controllers/Ekzeget/EkzegetChaptersByBook';
import EkzegetVersesByChapter from './Controller/controllers/Ekzeget/EkzegetVersesByChapter';
import SiteTypo from './Controller/controllers/Site/SiteTypo';
import GroupReadingPlanMentorGet from './Controller/controllers/GroupReadingPlan/GroupReadingPlanMentorGet';
import GroupReadingPlanCreate from './Controller/controllers/GroupReadingPlan/GroupReadingPlanCreate';
import GroupReadingPlanJoin from './Controller/controllers/GroupReadingPlan/GroupReadingPlanJoin';
import GroupReadingPlanUserGet from './Controller/controllers/GroupReadingPlan/GroupReadingPlanUserGet';
import ReadingPlanCreate from './Controller/controllers/Reading/ReadingPlanCreate';
import ReadingPlanEdit from './Controller/controllers/Reading/ReadingPlanEdit';
import ReadingPlanPreview from './Controller/controllers/Reading/ReadingPlanPreview';
import ReadingPlanCommentCreate from './Controller/controllers/ReadingPlanComment/ReadingPlanCommentCreate';
import ReadingPlanCommentGet from './Controller/controllers/ReadingPlanComment/ReadingPlanCommentGet';
import ReadingPlanCommentDelete from './Controller/controllers/ReadingPlanComment/ReadingPlanCommentDelete';
import GroupReadingPlanDelete from './Controller/controllers/GroupReadingPlan/GroupReadingPlanDelete';
import GroupReadingPlanKickMember from './Controller/controllers/GroupReadingPlan/GroupReadingPlanKickMember';
import GroupReadingPlanEdit from './Controller/controllers/GroupReadingPlan/GroupReadingPlanEdit';
import GroupReadingPlanPreview from './Controller/controllers/GroupReadingPlan/GroupReadingPlanPreview';
import ReadingPlanDelete from './Controller/controllers/Reading/ReadingPlanDelete';
import GroupReadingPlanGetInfo from './Controller/controllers/GroupReadingPlan/GroupReadingPlanGetInfo';
import UserPlanStatisticsGet from './Controller/controllers/User/UserPlanStatisticsGet';
import ReadingArchiveGet from './Controller/controllers/ReadingArchive/ReadingArchiveGet';
import UserPlanPatch from './Controller/controllers/User/UserPlanPatch';
import TextBlock from './Controller/controllers/TextBlock/TextBlock';

export default function(): IRoute[] {
    return [
        // interpretation
        {
            method: 'get',
            url: '/interpretation/:book_code?/',
            controller: InterpretationInterpretation,
            description: 'Список толкований',
        },
        // translate
        {
            method: 'get',
            url: '/book/translate-list',
            controller: BookTranslate,
            description: 'Список переводов',
        },
        // meta
        {
            method: 'get',
            url: '/meta/get-by-code',
            controller: MetaMeta,
            description: 'Мета-тег по коду (квери параметр)',
        },
        {
            method: 'get',
            url: '/meta/get-by-code/:code',
            controller: MetaMeta,
            description: 'Мета-тег по коду (параметр адреса)',
        },
        // verse
        {
            method: 'get',
            url: '/verse/info/:verse',
            controller: VerseInfo,
            description: 'Информация о стихе по короткому названию',
        },
        // slider
        {
            method: 'get',
            url: '/slider',
            controller: SliderSlider,
            description: 'Список записей',
        },
        // book
        {
            method: 'get',
            url: '/book/list/:testament?',
            controller: BookList,
            description: 'Список книг',
        },
        // lecture
        {
            method: 'get',
            url: '/lecture',
            controller: LectureList,
            description: 'Список статей',
        },
        {
            method: 'get',
            url: '/lecture/category',
            controller: LectureCategory,
            description: 'Список категорий лектория',
        },
        {
            method: 'get',
            url: '/lecture/detail/:code',
            controller: LectureDetail,
            description: 'Статья',
        },
        {
            method: 'get',
            url: '/lecture/:code',
            controller: LectureIndex,
            description: 'Список статей',
        },
        // article
        {
            method: 'get',
            url: '/article/detail/:slug',
            controller: ArticleDetail,
            description: 'Детали записи',
        },
        {
            method: 'get',
            url: '/article/:category_id',
            controller: ArticleArticle,
            description: 'Список записей',
        },
        // site
        {
            method: 'get',
            url: '/site/captcha',
            controller: SiteCaptcha,
            description: 'Обратная связь',
        },
        // ekzeget
        {
            method: 'get',
            url: '/ekzeget',
            controller: EkzegetEkzeget,
            description: 'Список экзегетов',
        },
        {
            method: 'get',
            url: '/ekzeget/verse-list/:interpretator_id',
            controller: EkzegetVerseList,
            description: 'Список стихов, на которые писал толкования данный толкователь',
        },
        {
            method: 'get',
            url: '/ekzeget/:interpreter_id/book-list',
            controller: EkzegetBookList,
            description: 'A list of books that were observed by the interpreter.',
        },
        {
            method: 'get',
            url: '/ekzeget/:interpreter_id/chapters-by-book/:book_id',
            controller: EkzegetChaptersByBook,
            description: 'A list of chapters of the book that were observed by the interpreter.',
        },
        {
            method: 'get',
            url: '/ekzeget/:interpreter_id/verses-by-chapter/:chapter_id',
            controller: EkzegetVersesByChapter,
            description: 'A list of verses of the book that were observed by the interpreter.',
        },
        // celebration
        {
            method: 'get',
            url: '/celebration/excuse/',
            controller: CelebrationExcuse,
            description: 'Список праздничных поводов',
        },
        // preaching
        {
            method: 'get',
            url: '/preaching/excuse',
            controller: PreachingExcuse,
            description: 'Список поводов',
        },
        {
            method: 'get',
            url: '/preaching/preacher',
            controller: PreachingPreacher,
            description: 'Список проповедников',
        },
        // dictionary
        {
            method: 'get',
            url: '/dictionary/list',
            controller: DictionaryList,
            description: 'Список слов',
        },
        {
            method: 'get',
            url: '/dictionary/detail/:codeOrId/',
            controller: DictionaryDetail,
            description: 'Детальное описание слова',
        },
        {
            method: 'get',
            url: '/dictionary/:type_id/:letter',
            controller: DictionaryDictionary,
            description: 'Список слов на заданную букву в заданном словаре',
        },
        // site
        {
            method: 'post',
            url: '/site/easter/',
            controller: SiteEaster,
            description: 'Год в формате YYYY',
        },
        // map
        {
            method: 'get',
            url: '/bible-maps/:testament_id?',
            controller: BibleMapsList,
            description: 'Список точек',
        },
        // {
        //     method: 'get',
        //     url: '/article/category/',
        //     controller: ArticleCategory,
        //     description: 'Список категорий',
        // },
        {
            method: 'get',
            url: '/bible-group/',
            controller: BibleGroupBibleGroup,
            description: 'Список библейских групп',
        },
        {
            method: 'get',
            url: '/bible-group/note/:type?/',
            controller: BibleGroupNote,
            description: 'Информация/описания',
        },
        {
            method: 'get',
            url: '/bible-group/type/',
            controller: BibleGroupType,
            description: 'Информация/описания',
        },
        {
            method: 'get',
            url: '/bible-group/note-detail/:id/',
            controller: BibleGroupNoteDetail,
            description: 'Информация/описания',
        },
        {
            method: 'post',
            url: '/bible-group/add/',
            controller: BibleGroupAdd,
            description: 'Добавить библейскую группу',
        },
        {
            method: 'post',
            url: '/bible-group/',
            controller: BibleGroupEdit,
            description: 'Обновить библейскую группу',
        },
        {
            method: 'delete',
            url: '/bible-group/',
            controller: BibleGroupDelete,
            description: 'Удалить библейскую группу',
        },
        // {
        //     method: 'get',
        //     url:
        //         '/bible-maps/points-by-book/:book/?chapter=:chapter&number=:number&page=:page&per-page=:per-page',
        //     controller: BibleMapsPointsByBook,
        //     description: 'Список точек по книге, главе и стиху',
        // },
        // {
        //     method: 'get',
        //     url: '/bible-maps/search/?q=:q&page=:page&per-page=:per-page',
        //     controller: BibleMapsSearch,
        //     description: 'Поиск точек',
        // },
        {
            method: 'get',
            url: '/book/info/:book/:chapter?/:verse?/',
            controller: BookInfo,
            description: 'Детальная информация о книге',
        },
        // {
        //     method: 'get',
        //     url: '/book/description/:book_code/',
        //     controller: BookDescription,
        //     description: 'Список описаний книги',
        // },
        // {
        //     method: 'get',
        //     url: '/celebration/:excuse_id/',
        //     controller: CelebrationCelebration,
        //     description: 'Список праздников',
        // },
        // {
        //     method: 'get',
        //     url: '/dictionary/search/?q=:q&page=:page&per-page=:per-page',
        //     controller: DictionarySearch,
        //     description: 'Глобальный поиск по словарям',
        // },
        // {
        //     method: 'get',
        //     url: '/ekzeget/type/',
        //     controller: EkzegetType,
        //     description: 'Список типов экзегетов',
        // },
        {
            method: 'get',
            url: '/ekzeget/list-for-chapter/:book/:chapter?/:verse?/',
            controller: EkzegetListForChapter,
            description: 'Список экзегетов по главе',
        },
        {
            method: 'get',
            url: '/interpretation/link/:verse_id',
            controller: InterpretationLink,
            description: 'Список толкований в которых упоминается стих',
        },
        // {
        //     method: 'post',
        //     url: '/interpretation/add/',
        //     controller: InterpretationAdd,
        //     description: 'Список толкований в которых упоминается стих',
        // },
        // {
        //     method: 'get',
        //     url: '/interpretation/search/?q=:q&page=:page&per-page=:per-page',
        //     controller: InterpretationSearch,
        //     description: 'Глобальный поиск по толкованиям',
        // },
        // {
        //     method: 'get',
        //     url: '/media/video-preview/:testament_id/?book_id=:book_id',
        //     controller: MediaVideoPreview,
        //     description: 'Возвращет список видео по 1 из каждого плейлиста',
        // },
        // {
        //     method: 'get',
        //     url:
        //         '/media/video/:playlist_id/?book_id=:book_id&chapter=:chapter&page=:page&per-page=:per-page',
        //     controller: MediaVideo,
        //     description: 'Список видео',
        // },
        // {
        //     method: 'get',
        //     url: '/media/video-playlist/:book_id/?page=:page&per-page=:per-page',
        //     controller: MediaVideoPlaylist,
        //     description: 'Список плейлистов',
        // },
        // {
        //     method: 'get',
        //     url: '/media/last-video/:testament_id',
        //     controller: MediaLastVideo,
        //     description: 'Возвращет список последних видео в завете',
        // },
        {
            method: 'get',
            url: '/media/download-media-audio-archive/',
            controller: MediaGetMediaAudioArchive,
            description: 'Возвращает архив аудио',
        },
        {
            method: 'get',
            url: '/media/statistics-views-count/',
            controller: MediaGetStatisticsViewsCount,
            description: 'Возвращает статистику кол-ва просмотров по медиа',
        },
        {
            method: 'get',
            url: '/media/statistics-views-increase/',
            controller: MediaGetStatisticsViewsIncrease,
            description: 'Увеличивает счетчик кол-ва просмотров медиа',
        },
        {
            method: 'get',
            url: '/media/statistics-downloads-count/',
            controller: MediaGetStatisticsDownloadsCount,
            description: 'Возвращает статистику кол-ва скачиваний по медиа',
        },
        {
            method: 'get',
            url: '/media/statistics-downloads-increase/',
            controller: MediaGetStatisticsDownloadsIncrease,
            description: 'Увеличивает счетчик кол-ва скачиваний медиа',
        },
        {
            method: 'get',
            url: '/media/get-media-autocomplete/:search',
            controller: GetMediaAutocomplete,
            description: 'Автодополнение связанных записей в медиатеке',
        },
        {
            method: 'get',
            url: '/media/get-tags-autocomplete/:search',
            controller: GetTagsAutocomplete,
            description: 'Автодополнение связанных тегов в медиатеке',
        },
        // {
        //     method: 'get',
        //     url:
        //         '/mediateka/:category_id/?author_id=:author_id&genre_id=:genre_id&tags=:tags&page=:page&per-page=:per-page',
        //     controller: MediatekaIndex,
        //     description: 'Список материалов',
        // },
        // {
        //     method: 'get',
        //     url: '/mediateka/detail/:id/',
        //     controller: MediatekaDetail,
        //     description: 'Детальный материал',
        // },
        // {
        //     method: 'get',
        //     url: '/mediateka/bible-content/:id/',
        //     controller: MediatekaBibleContent,
        //     description: 'Получить Библейски стихи привязанные к записи',
        // },
        // {
        //     method: 'get',
        //     url: '/mediateka/categories/',
        //     controller: MediatekaCategories,
        //     description: 'Список категорий медиатеки',
        // },
        // {
        //     method: 'get',
        //     url: '/mediateka/genre/',
        //     controller: MediatekaGenre,
        //     description: 'Список жанров медиатеки',
        // },
        // {
        //     method: 'get',
        //     url: '/mediateka/authors/',
        //     controller: MediatekaAuthors,
        //     description: 'Список авторов медиатеки',
        // },
        // {
        //     method: 'get',
        //     url: '/mediateka/tags/',
        //     controller: MediatekaTags,
        //     description: 'Список тегов медиатеки',
        // },
        // {
        //     method: 'get',
        //     url: '/mediateka/comment/:media_id/?page=:page&per-page=:per-page',
        //     controller: MediatekaComment,
        //     description: 'Список комментариев для записи медиатеки',
        // },
        // {
        //     method: 'post',
        //     url: '/mediateka/add-comment/',
        //     controller: MediatekaAddComment,
        //     description: 'Список комментариев для записи медиатеки.',
        // },
        // {
        //     method: 'delete',
        //     url: '/mediateka/subscribe/',
        //     controller: MediatekaDeleteSubscribe,
        //     description:
        //         'Отменить подписку на расслыку новых комментариев (только для авторизированых пользователей)',
        // },
        // {
        //     method: 'post',
        //     url: '/mediateka/subscribe/',
        //     controller: MediatekaAddSubscribe,
        //     description:
        //         'Оформить подписку на расслыку новых комментариев (только для авторизированых пользователей)',
        // },
        // {
        //     method: 'get',
        //     url: '/mediateka/search/:category_id/?q=:q',
        //     controller: MediatekaSearch,
        //     description: 'Поиск материалов',
        // },
        // {
        //     method: 'post',
        //     url: '/mediateka/download/',
        //     controller: MediatekaDownload,
        //     description: 'Пометить контент как скаченый',
        // },
        // {
        //     method: 'get',
        //     url: '/mediateka/bible-links/:category_id/',
        //     controller: MediatekaBibleLinks,
        //     description: 'Библейский материал привязанный к медиаконтенту',
        // },
        // {
        //     method: 'get',
        //     url: '/motivator/:verse_id',
        //     controller: MotivatorMotivator,
        //     description: 'Список записей',
        // },
        {
            method: 'get',
            url: '/preaching/',
            controller: PreachingPreaching,
            description: 'Список поводов',
        },
        // {
        //     method: 'get',
        //     url: '/preaching/search/?q=:q&page=:page&per-page=:per-page',
        //     controller: PreachingSearch,
        //     description: 'Глобальный поиск по проповедям',
        // },
        {
            method: 'get',
            url: '/preaching/by-book-and-chapter/',
            controller: PreachingPreachingByBookAndChapter,
            description: 'Список поводов по книге и главе',
        },
        {
            method: 'get',
            url: '/queeze/continue/:quizId',
            controller: QueezeContinue,
            description: 'Списк вопросов викторины',
        },
        {
            method: 'get',
            url: '/queeze/list/:pageNumber?',
            controller: QueezeGetList,
            description: 'Списк вопросов викторины (с пагинацией)',
        },
        {
            method: 'get',
            url: '/queeze/tutorial',
            controller: QueezeGetTutorial,
            description: 'Ссылка на видеоинструкцию',
        },
        {
            method: 'get',
            url: '/queeze/current',
            controller: QueezeCurrent,
            description: 'Текущая попытка',
        },
        {
            method: 'get',
            url: '/queeze/get-books',
            controller: QueezeGetBooks,
            description: 'Получить список книг для викторины',
        },
        {
            method: 'post',
            url: '/queeze/create/',
            controller: QueezeCreate,
            description: 'Создать викторину',
        },
        {
            method: 'get',
            url: '/queeze/answer/:questionId/:answerId',
            controller: QueezeAnswer,
            description: 'Ответ на вопрос викторины',
        },
        {
            method: 'get',
            url: '/queeze/question-list/',
            controller: QueezeQuestionList,
            description: 'Ответ на вопрос викторины',
        },
        {
            method: 'get',
            url: '/queeze/complete/',
            controller: QueezeComplete,
            description: 'Завершить викторину',
        },
        {
            method: 'get',
            url: '/queeze/question/:questionCode',
            controller: QueezeQuestion,
            description: 'Просмотр одного вопроса',
        },
        {
            method: 'get',
            url: '/queeze/cram-audio-into-base',
            controller: QueezeCramAudio,
            description: 'Добавить озвучку викторины с FTP в базу',
        },
        {
            method: 'get',
            url: '/reading/plan/',
            controller: ReadingPlan,
            description: 'Список чтений',
        },
        {
            method: 'post',
            url: '/reading/plan/',
            controller: ReadingPlanCreate,
            description: 'Создать кастомный план чтений',
            strategy: 'x-auth-token',
        },
        {
            method: 'put',
            url: '/reading/plan/:planId',
            controller: ReadingPlanEdit,
            description: 'Изменить кастомный план чтений',
            strategy: 'x-auth-token',
        },
        {
            method: 'delete',
            url: '/reading/plan/:planId',
            controller: ReadingPlanDelete,
            description: 'Удалить кастомный план чтений',
            strategy: 'x-auth-token',
        },
        // reading
        {
            method: 'get',
            url: '/reading/archive/',
            controller: ReadingArchiveGet,
            description: 'Получить архив чтений',
            strategy: 'x-auth-token',
        },
        {
            method: 'get',
            url: '/reading/:date',
            controller: ReadingIndex,
            description: 'Список чтений',
        },
        {
            method: 'get',
            url: '/reading/plan-detail/:plan_id',
            controller: ReadingPlanDetail,
            description: 'Список чтений',
        },
        {
            method: 'post',
            url: '/reading/plan-detail/preview/',
            controller: ReadingPlanPreview,
            description: 'Превью создаваемого плана чтений',
        },
        {
            method: 'post',
            url: '/site/contact/',
            controller: SiteContact,
            description: 'Обратная связь',
        },
        {
            method: 'get',
            url: '/reading/group/mentor/',
            controller: GroupReadingPlanMentorGet,
            description: 'Группа с планом чтения',
            strategy: 'x-auth-token',
        },
        {
            method: 'get',
            url: '/reading/group/user/',
            controller: GroupReadingPlanUserGet,
            description: 'Группа с планом чтения',
            strategy: 'x-auth-token',
        },
        {
            method: 'get',
            url: '/reading/group/:group_id',
            controller: GroupReadingPlanJoin,
            description: 'Присоединиться к группе с планом чтения',
            strategy: 'x-auth-token',
        },
        {
            method: 'get',
            url: '/reading/group/:group_id/info',
            controller: GroupReadingPlanGetInfo,
            description: 'Информация о группе с планом чтения',
            strategy: 'x-auth-token',
        },
        {
            method: 'get',
            url: '/reading/group/:group_id/preview/',
            controller: GroupReadingPlanPreview,
            description: 'Предпросмотр группы с планом чтения',
            strategy: 'x-auth-token',
        },
        {
            method: 'post',
            url: '/reading/group/',
            controller: GroupReadingPlanCreate,
            description: 'Создать группу с планом чтения',
            strategy: 'x-auth-token',
        },
        {
            method: 'put',
            url: '/reading/group/:group_id?',
            controller: GroupReadingPlanEdit,
            description: 'Редактировать группу с планом чтения',
            strategy: 'x-auth-token',
        },
        {
            method: 'delete',
            url: '/reading/group/',
            controller: GroupReadingPlanDelete,
            description: 'Удалить группу с планом чтения',
            strategy: 'x-auth-token',
        },
        {
            method: 'delete',
            url: '/reading/group/users/:member_id?',
            controller: GroupReadingPlanKickMember,
            description: 'Удалить пользователя из группы с планом чтения',
            strategy: 'x-auth-token',
        },
        {
            method: 'get',
            url: '/reading/plan/:day/comment/',
            controller: ReadingPlanCommentGet,
            description: 'Комментарии ко дню плана чтений',
        },
        {
            method: 'post',
            url: '/reading/plan/:day/comment/',
            controller: ReadingPlanCommentCreate,
            description: 'Добавить комментарий ко дню плана чтений',
            strategy: 'x-auth-token',
        },
        {
            method: 'delete',
            url: '/reading/plan/comment/:id',
            controller: ReadingPlanCommentDelete,
            description: 'Удалить комментарий ко дню плана чтений',
            strategy: 'x-auth-token',
        },
        {
            method: 'post',
            url: '/site/typo/',
            controller: SiteTypo,
            description: 'Typo report',
        },
        {
            method: 'post',
            url: '/site/file-exist/',
            controller: SiteFileExist,
            description: 'Проверка наличия аудиофайла',
        },
        // {
        //     method: 'get',
        //     url: '/subscribe/types',
        //     controller: SubscribeTypes,
        //     description: 'Список типов подписок',
        // },
        // {
        //     method: 'post',
        //     url: '/subscribe/add',
        //     controller: SubscribeAdd,
        //     description: 'Подписаться на рассылку',
        // },
        // {
        //     method: 'get',
        //     url: '/subscribe/unsubscribe?hash=:hash',
        //     controller: SubscribeUnsubscribe,
        //     description: 'Подписаться на рассылку',
        // },
        // {
        //     method: 'post',
        //     url: '/user/avatar-upload/',
        //     controller: UserAvatarUpload,
        //     description: 'Обновить аватар текущего пользователя.',
        // },
        {
            method: 'get',
            url: '/user/',
            controller: UserUser,
            description: 'Получить данные текущего пользователя',
            strategy: 'x-auth-token',
        },
        {
            method: 'post',
            url: '/user/update/',
            controller: UserUpdate,
            description: 'Обновить данные текущего пользователя, все поля не обязательные',
            strategy: 'x-auth-token',
        },
        // {
        //     method: 'post',
        //     url: '/user/logout/',
        //     controller: UserLogout,
        //     description: 'Выход из профиля',
        // },
        {
            method: 'get',
            url: '/user/interpretation-fav/',
            controller: UserInterpretationFavGet,
            strategy: 'x-auth-token',
            description: 'Избранные толкования текущего пользователя - получение',
        },
        {
            method: 'put',
            url: '/user/interpretation-fav/',
            controller: UserInterpretationFavPut,
            strategy: 'x-auth-token',
            description: 'Избранные толкования текущего пользователя - добавление',
        },
        {
            method: 'delete',
            url: '/user/interpretation-fav/',
            controller: UserInterpretationFavDelete,
            strategy: 'x-auth-token',
            description: 'Избранные толкования текущего пользователя - удаление',
        },
        {
            method: 'get',
            url: '/user/verse-fav/',
            controller: UserVerseFavGet,
            description: 'Избранные стихи текущего пользователя - получение',
            strategy: 'x-auth-token',
        },
        {
            method: 'post',
            url: '/user/verse-fav/',
            controller: UserVerseFavPost,
            description: 'Избранные стихи текущего пользователя - добавление',
            strategy: 'x-auth-token',
        },
        {
            method: 'put',
            url: '/user/verse-fav/',
            controller: UserVerseFavPut,
            description: 'Избранные стихи текущего пользователя - редактирование',
            strategy: 'x-auth-token',
        },
        {
            method: 'delete',
            url: '/user/verse-fav/',
            controller: UserVerseFavDelete,
            description: 'Избранные стихи текущего пользователя - удаление',
            strategy: 'x-auth-token',
        },
        {
            method: 'put',
            url: '/user/plan/',
            controller: UserPlanPut,
            strategy: 'x-auth-token',
            description: 'Планы чтения текущего пользователя - добавить',
        },
        {
            method: 'get',
            url: '/user/plan/statistics/:plan_id?',
            controller: UserPlanStatisticsGet,
            strategy: 'x-auth-token',
            description: 'Планы чтения текущего пользователя - получить статистику',
        },
        {
            method: 'get',
            url: '/user/plan/:day?',
            controller: UserPlanGet,
            strategy: 'x-auth-token',
            description: 'Планы чтения текущего пользователя - получить',
        },
        {
            method: 'delete',
            url: '/user/plan/',
            controller: UserPlanDelete,
            strategy: 'x-auth-token',
            description: 'Планы чтения текущего пользователя - удалить',
        },
        {
            method: 'post',
            url: '/user/plan/',
            controller: UserPlanPost,
            strategy: 'x-auth-token',
            description: 'Планы чтения текущего пользователя',
        },
        {
            method: 'patch',
            url: '/user/plan/',
            controller: UserPlanPatch,
            strategy: 'x-auth-token',
            description: 'Планы чтения текущего пользователя',
        },
        {
            method: 'get',
            url: '/user/bookmark/',
            controller: UserBookmarkGet,
            description: 'Закладки текущего пользователя - получить',
            strategy: 'x-auth-token',
        },
        {
            method: 'put',
            url: '/user/bookmark/',
            controller: UserBookmarkPut,
            description: 'Закладки текущего пользователя - создать',
            strategy: 'x-auth-token',
        },
        {
            method: 'delete',
            url: '/user/bookmark/',
            controller: UserBookmarkDelete,
            description: 'Закладки текущего пользователя - удалить',
            strategy: 'x-auth-token',
        },
        {
            method: 'get',
            url: '/user/note/',
            controller: UserNoteGet,
            description: 'Заметки текущего пользователя - получение',
            strategy: 'x-auth-token',
        },
        {
            method: 'post',
            url: '/user/note/',
            controller: UserNotePost,
            description: 'Заметки текущего пользователя - добавление',
            strategy: 'x-auth-token',
        },
        {
            method: 'put',
            url: '/user/note/',
            controller: UserNotePut,
            description: 'Заметки текущего пользователя - редактирование',
            strategy: 'x-auth-token',
        },
        {
            method: 'delete',
            url: '/user/note/',
            controller: UserNoteDelete,
            description: 'Заметки текущего пользователя - удаление',
            strategy: 'x-auth-token',
        },
        {
            method: 'post',
            url: '/user/interpretation-add/',
            controller: UserInterpretationAdd,
            strategy: 'x-auth-token',
            description: 'Добавить толкование',
        },
        {
            method: 'get',
            url: '/user/interpretation/',
            controller: UserInterpretationGet,
            strategy: 'x-auth-token',
            description: 'Получить/отредактировать толкования добавленые пользователем',
        },
        // {
        //     method: 'put',
        //     url: '/user/interpretation/',
        //     controller: UserInterpretationPut,
        //     strategy: 'x-auth-token',
        //     description: 'Получить/отредактировать толкования добавленые пользователем',
        // },
        // {
        //     method: 'post',
        //     url: '/user/plan-schedule/',
        //     controller: UserPlanSchedule,
        //     description: 'Подписаться / отписаться от дня недели',
        // },
        {
            method: 'post',
            url: '/user-profile/create/',
            controller: UserCreate,
            description: 'Регистрация пользователя',
        },
        {
            method: 'post',
            url: '/user-profile/login/',
            controller: UserLogin,
            description: 'Аутентификация пользователя',
        },
        {
            method: 'post',
            url: '/user-profile/reset-password/',
            controller: UserResetPassword,
            description:
                'Восстановление пароля пользователя. После отправки письма на почту, ссылка действительна в течении суток',
        },
        // {
        //     method: 'get',
        //     url: '/user-profile/valid-reset-secret/?secret=:secret',
        //     controller: UserValidResetSecret,
        //     description: 'Проверка ключа на валидность',
        // },
        // {
        //     method: 'get',
        //     url: '/user-profile/valid-confirm-secret/?secret=:secret',
        //     controller: UserValidConfirmSecret,
        //     description: 'Проверка ключа на валидность',
        // },
        {
            method: 'post',
            url: '/user-profile/registration-confirm',
            controller: UserRegistrationConfirm,
            description: 'Подтверждение регистрации',
        },
        /**
         * Роут "/verse/parallel/:verse_id" должен добавляться в массив раньше,
         * чем роут "/verse/:book_id/:chapter/"
         */
        {
            method: 'get',
            url: '/verse/parallel/:verse_id',
            controller: VerseParallel,
            description: 'Параллельные стихи с переводами',
        },
        {
            method: 'get',
            url: '/verse/:book_id/:chapter/',
            controller: VerseVerse,
            description: 'Списки стихов',
        },
        // {
        //     method: 'get',
        //     url: '/verse/detail/:id',
        //     controller: VerseDetail,
        //     description: 'Списки стихов',
        // },
        {
            method: 'get',
            url: '/bible/:book_code/:chapter/:number/',
            controller: VerseDetailByNumber,
            description: 'Списки стихов',
        },
        {
            method: 'get',
            url: '/verse/detail-by-number/:book_code/:chapter/:number',
            controller: VerseDetailByNumber,
            description: 'Списки стихов',
        },
        {
            method: 'get',
            url: '/verse/get-words-autocomlete/:id/:search',
            controller: GetWordsAutocomplete,
            description: 'Авто-дополнение связанных словарных терминов',
        },
        {
            method: 'get',
            url: '/admin/get-ftp-list',
            controller: GetFtpList,
            description: 'Запрос содержимого ftp',
        },
        {
            method: 'get',
            url: '/admin/get-audio-path/:media_id',
            controller: GetPathToAudio,
            description: 'Запрос на путь к аудиофайлу',
        },
        {
            method: 'get',
            url: '/config',
            controller: Config,
            description: 'Запрос на получение данных из таблицы config',
        },
        // {
        //     method: 'get',
        //     url: '/verse/search/?q=:q&page=:page&per-page=:per-page',
        //     controller: VerseSearch,
        //     description: 'Глобальный поиск по стихам',
        // },
        {
            method: 'get',
            url: '/dev/updateArticleImageUrls',
            controller: DevUpdateArticles,
            description: 'Обновляет ссылки на картинки',
        },
        {
            method: 'get',
            url: '/dev/resetCache',
            controller: DevResetCache,
            description: 'Сбрасывает весь кэш',
        },
        {
            method: 'get',
            url: '/dev/sendReadings',
            controller: DevSendReadings,
            description: 'Запускает рассылку писем с планами чтений',
        },
        {
            method: 'get',
            url: '/dev/regenerateFb2Chapters',
            controller: DevRegenerateFb2Chapters,
            description: 'Повторно парсит FB2 книгу и добавляет главы оттуда в базу',
        },
        {
            method: 'get',
            url: '/textBlock/:code/',
            controller: TextBlock,
            description: 'Получаем текстовый блок',
        },
    ];
}