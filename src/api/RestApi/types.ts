import { ServerError } from '../../libs/ErrorHandler';

export enum TextResponseStatus {
    ok = 'ok',
    error = 'error',
}

export interface IResponseBody {
    server_time: string;
    status: TextResponseStatus;
    controller_working_time: string;
    error?: Error | ServerError;
    [x: string]: any;
}
