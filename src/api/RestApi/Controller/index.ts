import * as Joi from '@hapi/joi';
import session from 'express-session';
import { ServerError } from '../../../libs/ErrorHandler';
import User from '../../../entities/User';
import { Response } from 'express';

export interface HandlerOptionalParams {
    cookie?: object;
    redirectFn?: (url: string) => void;
    finishResponseFn?: () => void;
    getResponseFn?: () => Response;
    session?: session.Session & Partial<session.SessionData> & { [key: string]: any };
    user?: User;
    clientHost?: string;
}

export default class Controller {
    constructor(
        public handle: (params: object, optional?: HandlerOptionalParams) => any,
        public inputValidationScheme: Joi.ObjectSchema,
        public outputValidationScheme: Joi.ObjectSchema
    ) {}

    public inputValidate(inputParams: object) {
        const result = this.inputValidationScheme.validate(inputParams);
        if (result.error) throw new ServerError(`Input ${result.error}`, 400);
    }

    public outputValidate(outputParams: object) {
        const result = this.outputValidationScheme.validate(outputParams);
        if (result.error) throw new ServerError(`Output ${result.error}`, 500);
    }
}
