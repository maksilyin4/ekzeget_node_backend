import * as Joi from '@hapi/joi';

export default Joi.object({
    view_count: Joi.number().description('Кол-во просмотров'),
    status: Joi.string().description('Статус ответа'),
    server_time: Joi.number().description('Серверное время'),
}).required();
