/**
 * @api {get} /media/statistics-views-increase/?mediaId=:mediaId
 * @apiVersion 0.1.0
 * @apiName get-statistics-views-increase
 * @apiGroup Media
 *
 *
 * @apiDescription Увеличивает счетчик кол-ва просмотров медиа
 * @apiPermission All
 *
 * @apiParam   {Integer}    mediaId         ID медиа
 *
 * @apiSuccess {Integer}    read_cnt        Кол-во скачиваний
 * @apiSuccess {Integer}    listen_cnt      Кол-во скачиваний
 * @apiSuccess {String}     status          Статус ответа
 * @apiSuccess {Integer}    server_time     Серверное время
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
