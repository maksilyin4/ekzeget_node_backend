import * as Joi from '@hapi/joi';

export default Joi.object({
    playlist_id: Joi.number().description('ID плейлиста (опционально)'),
    book_id: Joi.number().description('ID книги (опционально)'),
    chapter: Joi.number().description('Номер главы (опционально)'),
    code: Joi.string().description('Код видио (slug)'),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
}).required();
