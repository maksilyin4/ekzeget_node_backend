/**
 * @api {get} /media/video/:playlist_id/?book_id=:book_id&chapter=:chapter&page=:page&per-page=:per-page         Список видео
 * @apiVersion 0.1.0
 * @apiName video
 * @apiGroup Media
 *
 *
 * @apiDescription Список видео
 * @apiPermission All
 *
 * @apiParam {Integer}  playlist_id                     ID плейлиста (опционально)
 * @apiParam {Integer}  book_id                         ID книги (опционально)
 * @apiParam {Integer}  chapter                         Номер главы (опционально)
 * @apiParam {String}   code                            Код видио (slug)
 * @apiParam {Integer}  page                            Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page                        Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} video                       Список
 * @apiSuccess {Integer}  video.id                    ID
 * @apiSuccess {String}   video.title                 Название
 * @apiSuccess {String}   video.author                Чтец
 * @apiSuccess {String}   video.description           Описание
 * @apiSuccess {String}   video.comment               Комментарий
 * @apiSuccess {String}   video.path                  Ссылка на youtube
 * @apiSuccess {String}   video.keywords              Ключевые слова
 * @apiSuccess {String}   video.length                Длительность медиафайла
 * @apiSuccess {Integer}  video.created_at            Дата добавления (unixtimestamp)
 * @apiSuccess {Integer}  video.book_id               ID Книги
 * @apiSuccess {Integer}  video.chapter               Глава
 * @apiSuccess {Integer}  video.playlist              ID Плейлиста
 * @apiSuccess {Integer}  video.book_chapters         Количество глав в книге
 * @apiSuccess {String}   video.author_code           Код Автора (Чтеца slug)
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
