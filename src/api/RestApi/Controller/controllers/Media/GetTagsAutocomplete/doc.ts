/**
 * @api {get} /media/get-tags-autocomplete/:search
 * @apiVersion 0.1.0
 * @apiName media
 * @apiGroup Media
 *
 *
 * @apiDescription Список связанных тегов
 * @apiPermission All
 *
 * @apiParam {Integer}  search      Строка для поиска
 *
 *
 * @apiSuccess {Object[]} tags                        Список терминов
 * @apiSuccess {Integer}  tags.id                     ID термина
 * @apiSuccess {String}   tags.title                  Название

 *
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
