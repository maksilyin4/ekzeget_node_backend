import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import { Like } from 'typeorm';
import MediaTag from '../../../../../../entities/MediaTag';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { search } = validatedParams;
    const tags: Pick<MediaTag, 'id' | 'title'>[] = await MediaTag.find({
        select: ['id', 'title'],
        where: {
            title: Like(`%${search}%`),
        },
    });

    return { tags: tags };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
