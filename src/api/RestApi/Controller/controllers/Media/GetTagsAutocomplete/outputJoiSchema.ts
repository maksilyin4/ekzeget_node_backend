import * as Joi from '@hapi/joi';

export default Joi.object({
    tags: Joi.array().items({
        id: Joi.number(),
        title: Joi.string(),
    }),
});
