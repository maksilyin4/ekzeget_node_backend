import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import MediaStatistics from '../../../../../../entities/MediaStatistics';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const stat = await MediaStatistics.findOne({
        where: { media_id: validatedParams.mediaId },
    });

    return { view_count: stat?.view_cnt | 0 };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
