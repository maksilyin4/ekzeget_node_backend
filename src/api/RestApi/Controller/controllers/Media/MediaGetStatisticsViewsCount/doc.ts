/**
 * @api {get} /media/statistics-views-count/?mediaId=:mediaId
 * @apiVersion 0.1.0
 * @apiName get-statistics-views-count
 * @apiGroup Media
 *
 *
 * @apiDescription Возвращет статистику кол-ва просмотров по медиа
 * @apiPermission All
 *
 * @apiParam   {Integer}    mediaId         ID медиа
 *
 * @apiSuccess {Integer}    view_count      Кол-во просмотров
 * @apiSuccess {String}     status          Статус ответа
 * @apiSuccess {Integer}    server_time     Серверное время
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
