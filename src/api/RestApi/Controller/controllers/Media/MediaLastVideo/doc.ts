/**
 * @api {get} /media/last-video/:testament_id                Последние видео
 * @apiVersion 0.1.0
 * @apiName last-video
 * @apiGroup Media
 *
 *
 * @apiDescription Возвращет список последних видео в завете
 * @apiPermission All
 *
 * @apiParam   {Integer}  testament_id                ID завета (1 - ветхий, 2 - новый)
 *
 * @apiSuccess {Object[]} video                       Список
 * @apiSuccess {Integer}  video.id                    ID
 * @apiSuccess {String}   video.title                 Название
 * @apiSuccess {String}   video.author                Чтец
 * @apiSuccess {String}   video.description           Описание
 * @apiSuccess {String}   video.comment               Комментарий
 * @apiSuccess {String}   video.path                  Ссылка на youtube
 * @apiSuccess {String}   video.keywords              Ключевые слова
 * @apiSuccess {String}   video.length                Длительность медиафайла
 * @apiSuccess {Integer}  video.created_at            Дата добавления (unixtimestamp)
 * @apiSuccess {Integer}  video.book_id               ID Книги
 * @apiSuccess {Integer}  video.chapter               Глава
 * @apiSuccess {Integer}  video.playlist              ID Плейлиста
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 *
 * @param $testament_id
 * @return array
 */
