import * as Joi from '@hapi/joi';

export default Joi.object({
    testament_id: Joi.number().description('ID завета (1 - ветхий, 2 - новый)'),
}).required();
