/**
 * @api {get} /media/video-preview/:testament_id/?book_id=:book_id                  Превью плейлистов
 * @apiVersion 0.1.0
 * @apiName video-preview
 * @apiGroup Media
 *
 *
 * @apiDescription Возвращет список видео по 1 из каждого плейлиста
 * @apiPermission All
 *
 * @apiParam   {Integer}  testament_id                ID завета (1 - ветхий, 2 - новый)
 * @apiParam   {Integer}  book_id                     ID книги (опционально)
 *
 * @apiSuccess {Object[]} video                       Список
 * @apiSuccess {Integer}  video.id                    ID
 * @apiSuccess {String}   video.title                 Название
 * @apiSuccess {String}   video.author                Чтец
 * @apiSuccess {String}   video.description           Описание
 * @apiSuccess {String}   video.comment               Комментарий
 * @apiSuccess {String}   video.path                  Ссылка на youtube
 * @apiSuccess {String}   video.keywords              Ключевые слова
 * @apiSuccess {String}   video.length                Длительность медиафайла
 * @apiSuccess {Integer}  video.created_at            Дата добавления (unixtimestamp)
 * @apiSuccess {Integer}  video.book_id               ID Книги
 * @apiSuccess {Integer}  video.chapter               Глава
 * @apiSuccess {Integer}  video.playlist              ID Плейлиста
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
