import * as Joi from '@hapi/joi';

export default Joi.object({
    video: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            title: Joi.string().description('Название'),
            author: Joi.string().description('Чтец'),
            description: Joi.string().description('Описание'),
            comment: Joi.string().description('Комментарий'),
            path: Joi.string().description('Ссылка на youtube'),
            keywords: Joi.string().description('Ключевые слова'),
            length: Joi.string().description('Длительность медиафайла'),
            created_at: Joi.number().description('Дата добавления (unixtimestamp)'),
            book_id: Joi.number().description('ID Книги'),
            chapter: Joi.number().description('Глава'),
            playlist: Joi.number().description('ID Плейлиста'),
        })
        .description('Список'),
}).required();
