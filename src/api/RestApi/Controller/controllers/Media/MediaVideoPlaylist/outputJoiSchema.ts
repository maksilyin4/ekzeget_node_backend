import * as Joi from '@hapi/joi';

export default Joi.object({
    playlist: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            title: Joi.string().description('Название'),
            century: Joi.number()
                .allow(null)
                .description('Век'),
            description: Joi.string().description('Описание'),
        })
        .description('Список'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    }).description('Информация о возможной постраничной навигации'),
}).required();
