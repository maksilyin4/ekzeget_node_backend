import * as Joi from '@hapi/joi';

export default Joi.object({
    book_id: Joi.number().description(
        'ID книги, по которой необходимо получить плейлисты (опционально)'
    ),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
}).required();
