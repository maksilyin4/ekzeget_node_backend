/**
 * @api {get} /media/video-playlist/:book_id/?page=:page&per-page=:per-page         Список плейлистов
 * @apiVersion 0.1.0
 * @apiName video-playlist
 * @apiGroup Media
 *
 *
 * @apiDescription Список плейлистов
 * @apiPermission All
 *
 * @apiParam {Integer}  book_id                         ID книги, по которой необходимо получить плейлисты (опционально)
 * @apiParam {Integer}  page                            Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page                        Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} playlist                       Список
 * @apiSuccess {Integer}  playlist.id                    ID
 * @apiSuccess {String}   playlist.title                 Название
 * @apiSuccess {Integer}  playlist.century               Век
 * @apiSuccess {String}   playlist.description           Описание
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
