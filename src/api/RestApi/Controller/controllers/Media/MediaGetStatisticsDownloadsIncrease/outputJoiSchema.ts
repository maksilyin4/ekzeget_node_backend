import * as Joi from '@hapi/joi';

export default Joi.object({
    read_cnt: Joi.number().description('Кол-во скачиваний'),
    listen_cnt: Joi.number().description('Кол-во скачиваний'),
    status: Joi.string().description('Статус ответа'),
    server_time: Joi.number().description('Серверное время'),
}).required();
