import * as Joi from '@hapi/joi';

export default Joi.object({
    mediaId: Joi.number()
        .required()
        .description('ID медиа'),
    type: Joi.string()
        .required()
        .valid('read', 'listen')
        .description('Тип вкладки (read, listen)'),
}).required();
