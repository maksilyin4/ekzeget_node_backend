import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import MediaStatistics from '../../../../../../entities/MediaStatistics';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const stat = await MediaStatistics.findOrCreate(validatedParams.mediaId);

    switch (validatedParams.type) {
        case 'read':
            stat.download_read_cnt++;
            break;
        case 'listen':
            stat.download_listen_cnt++;
            break;
    }

    await MediaStatistics.save(stat);

    return {
        read_cnt: stat.download_read_cnt,
        listen_cnt: stat.download_listen_cnt,
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
