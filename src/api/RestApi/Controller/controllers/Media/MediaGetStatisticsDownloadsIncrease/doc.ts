/**
 * @api {get} /media/statistics-downloads-increase/?mediaId=:mediaId&type=:type
 * @apiVersion 0.1.0
 * @apiName get-statistics-downloads-increase
 * @apiGroup Media
 *
 *
 * @apiDescription Увеличивает счетчик кол-ва скачиваний медиа
 * @apiPermission All
 *
 * @apiParam   {Integer}    mediaId         ID медиа
 * @apiParam   {String}     type            Тип вкладки (read, listen)
 *
 * @apiSuccess {Integer}    download_count  Кол-во просмотров (новое значение)
 * @apiSuccess {String}     status          Статус ответа
 * @apiSuccess {Integer}    server_time     Серверное время
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
