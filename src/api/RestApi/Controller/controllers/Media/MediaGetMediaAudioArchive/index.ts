import * as Joi from '@hapi/joi';
import { dirname } from 'path';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import MediaRelated from '../../../../../../entities/MediaRelated';
import Media from '../../../../../../entities/Media';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import EnvironmentChecker from '../../../../../../core/EnvironmentChecker';

const { basename } = require('path');

const getMediaPathMap = () => {
    const envs = new EnvironmentChecker().getCheckedEnvironment();
    return new Map<string, string>([
        [envs.MEDIA_CDN_UPLOADS_URL, envs.MEDIA_CDN_UPLOADS_DIR],
        [envs.MEDIA_CDN_MP3_URL, envs.MEDIA_CDN_MP3_DIR],
        [envs.MEDIA_CDN_MEDIA_URL, envs.MEDIA_CDN_MEDIA_DIR],
    ]);
};

type FileInfo = { filename: string; filepath: string; original: string };

const getRelativeFilepath = (path: string, mediaPathMap: Map<string, string>): string => {
    if (!path) {
        throw new ServerError('Empty filepath');
    }

    const { origin } = new URL(path);
    const folder = mediaPathMap.get(origin);
    if (!folder) {
        throw new ServerError(`Неизвестное хранилище файлов: ${origin}`);
    }

    return `${folder}${path.replace(origin, '')}`;
};

const getRelatedFiles = async (media_id: number): Promise<FileInfo[]> => {
    const mediaRelated = await MediaRelated.find({
        where: { media_id },
        select: ['related_id', 'sort'],
        order: { sort: 'DESC' },
    });
    if (!mediaRelated.length) return [];

    const audioFiles: Media[] = await Media.createQueryBuilder('media')
        .where('media.id IN (:...relatedIds)', {
            relatedIds: mediaRelated.map(mr => mr.related_id),
        })
        .andWhere('media.type = :type', { type: 'A' })
        .getMany();
    if (!audioFiles.length) return [];

    const mediaPathMap = getMediaPathMap();
    return audioFiles.map(({ properties }: Media) => {
        const { audio_href } = JSON.parse(properties);
        return {
            filename: basename(audio_href),
            filepath: getRelativeFilepath(audio_href, mediaPathMap),
            original: audio_href,
        };
    });
};

const cutPreFolders = (path: string) =>
    ['/uploads', '/mp3', '/media', '/music']
        .map(prePath => {
            if (path.startsWith(prePath)) {
                return path.replace(prePath, '');
            }
        })
        .filter(Boolean)[0];

const handler = async (
    { mediaId }: Joi.extractType<typeof inputJoiSchema>,
    { finishResponseFn, redirectFn }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const [media, files]: [Media, FileInfo[]] = await Promise.all([
        Media.findOne({ where: { id: mediaId } }),
        getRelatedFiles(mediaId),
    ]);
    if (!media) {
        throw new ServerError(`Не удалось найти медиа с ID = ${mediaId}`, 404);
    }
    if (!files.length) {
        throw new ServerError(`Не удалось найти аудио-файлы для ${mediaId}`, 404);
    }

    const host = new URL(files[0].original).origin;
    const folder = dirname(cutPreFolders(files[0].filepath));
    const postfix = '?download-all-as-zip=';

    const link = host + folder + postfix + folder.replace('/', '') + '.zip';
    redirectFn(link);
    finishResponseFn();
    return { file: null };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
