import * as Joi from '@hapi/joi';

export default Joi.object({
    mediaId: Joi.number().description('ID медиа'),
}).required();
