import * as Joi from '@hapi/joi';

export default Joi.object({
    file: Joi.any(),
}).required();
