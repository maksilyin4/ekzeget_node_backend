/**
 * @api {get} /media/download-media-audio-archive/              Скачать архив медиа аудио
 * @apiVersion 0.1.0
 * @apiName get-media-audio-archive
 * @apiGroup Media
 *
 *
 * @apiDescription Возвращет архив аудио
 * @apiPermission All
 *
 * @apiParam   {Integer}  media_id                    ID медиа
 *
 * @apiSuccess {Download} file
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
