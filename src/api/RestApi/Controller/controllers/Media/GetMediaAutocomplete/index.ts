import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import { Like } from 'typeorm';
import Media from '../../../../../../entities/Media';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { search } = validatedParams;
    const words: Pick<Media, 'id' | 'title'>[] = await Media.find({
        select: ['id', 'title'],
        where: {
            title: Like(`%${search}%`),
        },
    });

    return { words: words };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
