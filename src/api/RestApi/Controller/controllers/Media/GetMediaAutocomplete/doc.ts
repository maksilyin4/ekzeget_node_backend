/**
 * @api {get} /media/get-media-autocomplete/:search
 * @apiVersion 0.1.0
 * @apiName verse
 * @apiGroup Verse
 *
 *
 * @apiDescription Список связанных записей медиатеки
 * @apiPermission All
 *
 * @apiParam {Integer}  search      Строка для поиска
 *
 *
 * @apiSuccess {Object[]} words                        Список терминов
 * @apiSuccess {Integer}  words.id                     ID термина
 * @apiSuccess {String}   words.title                  Название

 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
