import * as Joi from '@hapi/joi';

export default Joi.object({
    search: Joi.string()
        .description('Строка поиска')
        .allow(null, ''),
});
