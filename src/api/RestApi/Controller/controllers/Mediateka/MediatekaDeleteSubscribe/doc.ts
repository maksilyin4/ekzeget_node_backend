/**
 * @api {delete} /mediateka/subscribe/                Отменить подписку
 * @apiVersion 0.1.0
 * @apiName  delete-subscribe
 * @apiGroup Mediateka
 *
 * @apiDescription                                    Отменить подписку на расслыку новых комментариев (только для авторизированых пользователей)
 * @apiPermission Authorized client
 *
 * @apiParam {Integer}    media_id                    ID записи
 *
 * @apiSuccess {String}   status          статус
 *
 * @apiError (4xx) {ApiException} error
 */
