/**
 * @api {get} /mediateka/categories/                    Категории медиатеки
 * @apiVersion 0.1.0
 * @apiName  categories
 * @apiGroup Mediateka
 *
 *
 * @apiDescription                                      Список категорий медиатеки
 * @apiPermission All
 *
 * @apiSuccess {Object[]} categories                        Список стихов
 * @apiSuccess {Integer}  categories.id                     ID
 * @apiSuccess {String}   categories.title                  Название
 * @apiSuccess {String}   categories.description            Описание
 * @apiSuccess {Object}   categories.image                  Картинка
 *
 * @apiError (4xx) {ApiException} error
 */
