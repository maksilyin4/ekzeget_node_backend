/**
 * @api {post} /mediateka/subscribe/                  Оформить подписку
 * @apiVersion 0.1.0
 * @apiName  add-subscribe
 * @apiGroup Mediateka
 *
 * @apiDescription                                    Оформить подписку на расслыку новых комментариев (только для авторизированых пользователей)
 * @apiPermission Authorized client
 *
 * @apiParam {Integer}    media_id                    ID записи
 *
 * @apiSuccess {String}   status          статус
 *
 * @apiError (4xx) {ApiException} error
 */
