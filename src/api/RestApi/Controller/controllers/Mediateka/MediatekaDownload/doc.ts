/**
 * @api {post} /mediateka/download/                  Скачать
 * @apiVersion 0.1.0
 * @apiName  download
 * @apiGroup Mediateka
 *
 * @apiDescription                                   Пометить контент как скаченый
 * @apiPermission All
 *
 * @apiParam {Integer}    id                    ID контента
 *
 * @apiSuccess {String}   status          статус
 *
 * @apiError (4xx) {ApiException} error
 */
