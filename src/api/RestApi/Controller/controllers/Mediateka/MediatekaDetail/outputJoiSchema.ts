import * as Joi from '@hapi/joi';

export default Joi.object({
    item: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            title: Joi.string().description('Название'),
            description: Joi.string().description('Описание'),
            allow_comment: Joi.number().description('Разрешены комментарии (0/1)'),
            year: Joi.string().description('Год книги'),
            image: Joi.object({}).description('Картинка'),
            author: Joi.object({
                id: Joi.number().description('ID автора'),
                title: Joi.string().description('Имя автора'),
                description: Joi.string().description('Описание автора'),
                image: Joi.object({}).description('Картинка автора'),
            }).description('Автор'),
            genre: Joi.object({
                id: Joi.number().description('ID жанра'),
                title: Joi.string().description('Название жанра'),
                description: Joi.string().description('Описание жанра'),
                image: Joi.object({}).description('Картинка жанра'),
            }).description('Жанр'),
            categories: Joi.array()
                .items({
                    id: Joi.number().description('ID жанра'),
                    title: Joi.string().description('Название категории'),
                    description: Joi.string().description('Описание категории'),
                    image: Joi.object({}).description('Картинка категории'),
                })
                .description('Массив категорий'),
            tags: Joi.array()
                .items({
                    id: Joi.number().description('ID тега'),
                    title: Joi.string().description('Название тега'),
                })
                .description('Теги'),
            ebook: Joi.object({
                id: Joi.number().description('ID'),
                title: Joi.string().description('Название книги'),
                description: Joi.string().description('Описание книги'),
                files: Joi.array()
                    .items({
                        id: Joi.number().description('ID'),
                        url: Joi.string().description('Внешнаяя ссылка'),
                        download_url: Joi.string().description('Ссылка на скачивание файла'),
                        downloads: Joi.number().description('Количество загрузок файла'),
                        book_type: Joi.string().description('Тип книги (fb2/epub/pdf)'),
                        file: Joi.object({}).description('Информация о файле'),
                    })
                    .description('Файлы'),
            }).description('Электронные книги'),
            audio: Joi.object({
                id: Joi.number().description('ID'),
                title: Joi.string().description('Название аудио'),
                description: Joi.string().description('Описание аудио'),
                files: Joi.array()
                    .items({
                        id: Joi.number().description('ID'),
                        title: Joi.string().description('Название конкретного файла'),
                        description: Joi.string().description('Описание конкретного файла'),
                        url: Joi.string().description('Внешнаяя ссылка'),
                        download_url: Joi.string().description('Ссылка на скачивание файла'),
                        downloads: Joi.number().description('Количество загрузок файла'),
                        is_full: Joi.number().description(
                            'является ли файл полной версией книги для скачивания (0/1)'
                        ),
                        bible_relations: Joi.array()
                            .items({})
                            .description('Связи главы с библией'),
                    })
                    .description('Файлы'),
            }).description('Аудио'),
            video: Joi.object({
                id: Joi.number().description('ID'),
                title: Joi.string().description('Название видео'),
                description: Joi.string().description('Описание видео'),
                files: Joi.array()
                    .items({
                        id: Joi.number().description('ID'),
                        title: Joi.string().description('Название конкретного файла'),
                        description: Joi.string().description('Описание конкретного файла'),
                        url: Joi.string().description('Внешнаяя ссылка'),
                        download_url: Joi.string().description('Ссылка на скачивание файла'),
                        downloads: Joi.number().description('Количество загрузок файла'),
                        is_full: Joi.number().description(
                            'является ли файл полной версией книги для скачивания (0/1)'
                        ),
                        bible_relations: Joi.array()
                            .items({})
                            .description('Связи главы с библией'),
                    })
                    .description('Файлы'),
            }).description('Видео'),
            bible_relations: Joi.array()
                .items({
                    book_id: Joi.number().description('Id книги'),
                    verse_id: Joi.number().description('ID стиха'),
                    chapter: Joi.number().description('Номер главы'),
                    verse_number: Joi.number().description('Номер стиха'),
                    book_code: Joi.number().description('Код книги'),
                    book_short_title: Joi.number().description('Короткое название книги'),
                    short: Joi.number().description('Короткое название сущности'),
                })
                .description('Связи с библией'),
            downloads: Joi.number().description('Общее количество загрузок файлов'),
        })
        .description('Запись'),
}).required();
