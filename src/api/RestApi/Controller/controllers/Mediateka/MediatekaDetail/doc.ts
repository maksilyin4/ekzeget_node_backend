/**
 * @api {get} /mediateka/detail/:id/  Детальный материал
 * @apiVersion 0.1.0
 * @apiName detail
 * @apiGroup Mediateka
 *
 *
 * @apiDescription Детальный материал
 * @apiPermission All
 *
 * @apiParam {Integer}  id                   ID материала
 *
 *
 * @apiSuccess {Object[]} item                           Запись
 * @apiSuccess {Integer}  item.id                       ID
 * @apiSuccess {String}   item.title                    Название
 * @apiSuccess {String}   item.description              Описание
 * @apiSuccess {Integer}  item.allow_comment            Разрешены комментарии (0/1)
 * @apiSuccess {String}   item.year                     Год книги
 * @apiSuccess {Object}   item.image                    Картинка
 *
 * @apiSuccess {Object}   item.author                   Автор
 * @apiSuccess {Integer}  item.author.id                ID автора
 * @apiSuccess {String}   item.author.title             Имя автора
 * @apiSuccess {String}   item.author.description       Описание автора
 * @apiSuccess {Object}   item.author.image             Картинка автора
 *
 * @apiSuccess {Object}   item.genre                   Жанр
 * @apiSuccess {Integer}  item.genre.id                ID жанра
 * @apiSuccess {String}   item.genre.title             Название жанра
 * @apiSuccess {String}   item.genre.description       Описание жанра
 * @apiSuccess {Object}   item.genre.image             Картинка жанра
 *
 * @apiSuccess {Object[]} item.categories            Массив категорий
 * @apiSuccess {Integer}  item.category.id             ID жанра
 * @apiSuccess {String}   item.category.title          Название категории
 * @apiSuccess {String}   item.category.description    Описание категории
 * @apiSuccess {Object}   item.category.image          Картинка категории
 *
 * @apiSuccess {Object[]} item.tags                    Теги
 * @apiSuccess {Integer}  item.tags.id                 ID тега
 * @apiSuccess {String}   item.tags.title              Название тега
 *
 * @apiSuccess {Object}   item.ebook                   Электронные книги
 * @apiSuccess {Integer}  item.ebook.id                ID
 * @apiSuccess {String}   item.ebook.title             Название книги
 * @apiSuccess {String}   item.ebook.description       Описание книги
 * @apiSuccess {Object[]} item.ebook.files             Файлы
 * @apiSuccess {Integer}  item.ebook.files.id          ID
 * @apiSuccess {String}   item.ebook.files.url         Внешнаяя ссылка
 * @apiSuccess {String}   item.ebook.files.download_url Ссылка на скачивание файла
 * @apiSuccess {Integer}  item.ebook.files.downloads    Количество загрузок файла
 * @apiSuccess {String}   item.ebook.files.book_type   Тип книги (fb2/epub/pdf)
 * @apiSuccess {Object}   item.ebook.files.file        Информация о файле
 *
 * @apiSuccess {Object}   item.audio                   Аудио
 * @apiSuccess {Integer}  item.audio.id                ID
 * @apiSuccess {String}   item.audio.title             Название аудио
 * @apiSuccess {String}   item.audio.description       Описание аудио
 * @apiSuccess {Object[]} item.audio.files             Файлы
 * @apiSuccess {Integer}  item.audio.files.id          ID
 * @apiSuccess {String}   item.audio.files.title       Название конкретного файла
 * @apiSuccess {String}   item.audio.files.description Описание конкретного файла
 * @apiSuccess {String}   item.audio.files.url         Внешнаяя ссылка
 * @apiSuccess {String}   item.audio.files.download_url Ссылка на скачивание файла
 * @apiSuccess {Integer}  item.audio.files.downloads    Количество загрузок файла
 * @apiSuccess {Integer}  item.audio.files.is_full     является ли файл полной версией книги для скачивания (0/1)
 * @apiSuccess {Object[]} item.audio.files.bible_relations        Связи главы с библией
 *
 * @apiSuccess {Object}   item.video                   Видео
 * @apiSuccess {Integer}  item.video.id                ID
 * @apiSuccess {String}   item.video.title             Название видео
 * @apiSuccess {String}   item.video.description       Описание видео
 * @apiSuccess {Object[]} item.video.files             Файлы
 * @apiSuccess {Integer}  item.video.files.id          ID
 * @apiSuccess {String}   item.video.files.title       Название конкретного файла
 * @apiSuccess {String}   item.video.files.description Описание конкретного файла
 * @apiSuccess {String}   item.video.files.url         Внешнаяя ссылка
 * @apiSuccess {String}   item.video.files.download_url Ссылка на скачивание файла
 * @apiSuccess {Integer}  item.video.files.downloads    Количество загрузок файла
 * @apiSuccess {Integer}  item.video.files.is_full     является ли файл полной версией книги для скачивания (0/1)
 * @apiSuccess {Object[]} item.video.files.bible_relations      Связи главы с библией
 *
 * @apiSuccess {Object[]}  item.bible_relations                Связи с библией
 * @apiSuccess {Integer}   item.bible_relations.book_id        Id книги
 * @apiSuccess {Integer}   item.bible_relations.verse_id       ID стиха
 * @apiSuccess {Integer}   item.bible_relations.chapter        Номер главы
 * @apiSuccess {Integer}   item.bible_relations.verse_number   Номер стиха
 * @apiSuccess {Integer}   item.bible_relations.book_code      Код книги
 * @apiSuccess {Integer}   item.bible_relations.book_short_title  Короткое название книги
 * @apiSuccess {Integer}   item.bible_relations.short          Короткое название сущности
 *
 *
 * @apiSuccess {Integer}   item.downloads               Общее количество загрузок файлов
 *
 *
 * @apiError (4xx) {ApiException} error
 */
