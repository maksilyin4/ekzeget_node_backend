import * as Joi from '@hapi/joi';

export default Joi.object({
    media_id: Joi.number().description('ID записи'),
    comment: Joi.string().description('Текст комментария'),
    username: Joi.string().description('Имя пользователя'),
    email: Joi.string().description('email'),
}).required();
