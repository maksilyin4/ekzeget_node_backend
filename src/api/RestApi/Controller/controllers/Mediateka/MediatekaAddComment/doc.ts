/**
 * @api {post} /mediateka/add-comment/                Добавить комментарий
 * @apiVersion 0.1.0
 * @apiName  add-comment
 * @apiGroup Mediateka
 *
 * @apiDescription                                    Список комментариев для записи медиатеки.
 *                                                    Если не указаны username и email, необходима авторизация пользователя.
 *                                                    В противном случае будет 401 ошибка
 * @apiPermission All
 *
 * @apiParam {Integer}    media_id                    ID записи
 * @apiParam {String}     comment                     Текст комментария
 * @apiParam {String}     [username]                  Имя пользователя
 * @apiParam {String}     [email]                     email
 *
 * @apiSuccess {String}   status          статус
 *
 * @apiError (4xx) {ApiException} error
 */
