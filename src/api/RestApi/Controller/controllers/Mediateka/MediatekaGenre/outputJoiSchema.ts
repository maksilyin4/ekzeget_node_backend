import * as Joi from '@hapi/joi';

export default Joi.object({
    genre: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            title: Joi.string().description('Название'),
            description: Joi.string().description('Описание'),
            image: Joi.object({}).description('Картинка'),
        })
        .description('Список стихов'),
}).required();
