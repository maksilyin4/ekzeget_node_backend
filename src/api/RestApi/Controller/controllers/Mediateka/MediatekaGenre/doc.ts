/**
 * @api {get} /mediateka/genre/                        Жанры медиатеки
 * @apiVersion 0.1.0
 * @apiName  genre
 * @apiGroup Mediateka
 *
 *
 * @apiDescription                                      Список жанров медиатеки
 * @apiPermission All
 *
 * @apiSuccess {Object[]} genre                        Список стихов
 * @apiSuccess {Integer}  genre.id                     ID
 * @apiSuccess {String}   genre.title                  Название
 * @apiSuccess {String}   genre.description            Описание
 * @apiSuccess {Object}   genre.image                  Картинка
 *
 * @apiError (4xx) {ApiException} error
 */
