import * as Joi from '@hapi/joi';

export default Joi.object({
    category_id: Joi.number().description('ID категории'),
}).required();
