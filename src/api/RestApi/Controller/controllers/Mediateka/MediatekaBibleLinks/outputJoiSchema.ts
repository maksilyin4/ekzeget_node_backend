import * as Joi from '@hapi/joi';

export default Joi.object({
    books: Joi.array()
        .items({
            id: Joi.number().description('Id книги'),
            title: Joi.string().description('Название'),
            short_title: Joi.string().description('Короткое название'),
            parts: Joi.number().description('Колличество глав в книге'),
            code: Joi.string().description('Символьный код'),
            testament: Joi.object({}).description('Завет'),
            chapters: Joi.array()
                .items({
                    chapter: Joi.number().description('Номер главы'),
                    verses: Joi.array()
                        .items({
                            verse_id: Joi.number().description('id стиха'),
                            number: Joi.number().description('номер стиха'),
                        })
                        .description('Стихи'),
                })
                .description('Список глав'),
        })
        .description('Список книг'),
}).required();
