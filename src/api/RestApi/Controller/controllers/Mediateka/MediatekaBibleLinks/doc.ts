/**
 * @api {get} /mediateka/bible-links/:category_id/               Библейский материал
 * @apiVersion 0.1.0
 * @apiName  bible-links
 * @apiGroup Mediateka
 *
 * @apiDescription                                  Библейский материал привязанный к медиаконтенту
 * @apiPermission All
 *
 * @apiParam {Integer}    [category_id]                    ID категории
 *
 * @apiSuccess {Object[]}   books                   Список книг
 * @apiSuccess {Integer}    books.id                Id книги
 * @apiSuccess {String}     books.title             Название
 * @apiSuccess {String}     books.short_title       Короткое название
 * @apiSuccess {Integer}    books.parts             Колличество глав в книге
 * @apiSuccess {String}     books.code              Символьный код
 * @apiSuccess {Object}     books.testament         Завет
 * @apiSuccess {Object[]}   books.chapters          Список глав
 * @apiSuccess {Integer}    books.chapters.chapter  Номер главы
 * @apiSuccess {Object[]}   books.chapters.chapter.verses  Стихи
 * @apiSuccess {Integer}    books.chapters.chapter.verses.verse_id  id стиха
 * @apiSuccess {Integer}    books.chapters.chapter.verses.number    номер стиха
 *
 * @apiError (4xx) {ApiException} error
 */
