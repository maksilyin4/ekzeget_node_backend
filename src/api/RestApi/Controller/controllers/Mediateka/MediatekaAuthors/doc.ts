/**
 * @api {get} /mediateka/authors/                        Авторы
 * @apiVersion 0.1.0
 * @apiName  authors
 * @apiGroup Mediateka
 *
 *
 * @apiDescription                                      Список авторов медиатеки
 * @apiPermission All
 *
 * @apiSuccess {Object[]} authors                        Список стихов
 * @apiSuccess {Integer}  authors.id                     ID
 * @apiSuccess {String}   authors.title                  Название
 * @apiSuccess {String}   authors.description            Описание
 * @apiSuccess {Object}   authors.image                  Картинка
 *
 * @apiError (4xx) {ApiException} error
 */
