/**
 * @api {get} /mediateka/tags/                          Список тегов
 * @apiVersion 0.1.0
 * @apiName  tags
 * @apiGroup Mediateka
 *
 *
 * @apiDescription                                      Список тегов медиатеки
 * @apiPermission All
 *
 *
 * @apiSuccess {Object[]} tags                        Список стихов
 * @apiSuccess {Integer}  tags.id                     ID
 * @apiSuccess {String}   tags.title                  Название
 *
 * @apiError (4xx) {ApiException} error
 */
