import * as Joi from '@hapi/joi';

export default Joi.object({
    tags: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            title: Joi.string().description('Название'),
        })
        .description('Список стихов'),
}).required();
