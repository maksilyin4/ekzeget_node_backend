import * as Joi from '@hapi/joi';

export default Joi.object({
    comments: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            comment: Joi.string().description('Комментарий'),
            username: Joi.string().description('Имя пользователя'),
            user_email: Joi.string().description('email'),
            created_at: Joi.number().description('Дата комментария'),
        })
        .description('Список комментариев'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    }).description('Информация о возможной постраничной навигации'),
}).required();
