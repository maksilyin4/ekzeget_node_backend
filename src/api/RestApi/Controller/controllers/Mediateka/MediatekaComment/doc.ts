/**
 * @api {get} /mediateka/comment/:media_id/?page=:page&per-page=:per-page            Список комментариев
 * @apiVersion 0.1.0
 * @apiName  comment
 * @apiGroup Mediateka
 *
 *
 * @apiDescription                                      Список комментариев для записи медиатеки
 * @apiPermission All
 *
 * @apiParam {Integer}    media_id                    ID записи
 *
 * @apiSuccess {Object[]} comments                    Список комментариев
 * @apiSuccess {Integer}  comments.id                     ID
 * @apiSuccess {String}   comments.comment                Комментарий
 * @apiSuccess {String}   comments.username               Имя пользователя
 * @apiSuccess {String}   comments.user_email             email
 * @apiSuccess {Integer}  comments.created_at             Дата комментария
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (4xx) {ApiException} error
 */
