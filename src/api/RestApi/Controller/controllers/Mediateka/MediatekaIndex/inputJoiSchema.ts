import * as Joi from '@hapi/joi';

export default Joi.object({
    category_id: Joi.number().description('ID категории'),
    tags: Joi.string().description('ID тегов через запятую (tags=1,2,5)'),
    genre_id: Joi.number().description('ID жанра'),
    author_id: Joi.number().description('ID автора'),
    testament_id: Joi.number().description('Завет (1 - ветхий / 2 - новый)'),
    book_id: Joi.number().description('id книги'),
    chapter: Joi.number().description('Номер главы'),
    verse_id: Joi.number().description('id стиха'),
    file_type: Joi.number().description('Формат файла (1 - ebook, 2 - аудио, 3 - video)'),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
}).required();
