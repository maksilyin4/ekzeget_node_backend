/**
 * @api {get} /mediateka/:category_id/?author_id=:author_id&genre_id=:genre_id&tags=:tags&page=:page&per-page=:per-page  Список материалов
 * @apiVersion 0.1.0
 * @apiName index
 * @apiGroup Mediateka
 *
 *
 * @apiDescription Список материалов
 * @apiPermission All
 *
 * @apiParam {Integer}  [category_id]                    ID категории
 * @apiParam {String}   [tags]                           ID тегов через запятую (tags=1,2,5)
 * @apiParam {Integer}  [genre_id]                       ID жанра
 * @apiParam {Integer}  [author_id]                      ID автора
 * @apiParam {Integer}  [testament_id]                   Завет (1 - ветхий / 2 - новый)
 * @apiParam {Integer}  [book_id]                        id книги
 * @apiParam {Integer}  [chapter]                        Номер главы
 * @apiParam {Integer}  [verse_id]                       id стиха
 * @apiParam {Integer}   [file_type]                     Формат файла (1 - ebook, 2 - аудио, 3 - video)
 *
 * @apiParam {Integer}  page                            Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page                        Количество элементов на странице (опционально, по умолчанию 20)
 *
 *
 * @apiSuccess {Object[]} items                          Список
 * @apiSuccess {Integer}  items.id                       ID
 * @apiSuccess {String}   items.title                    Название
 * @apiSuccess {String}   items.description              Описание
 * @apiSuccess {Integer}  items.allow_comment            Разрешены комментарии (0/1)
 * @apiSuccess {String}   items.year                     Год книги
 * @apiSuccess {Object}   items.image                    Картинка
 *
 * @apiSuccess {Object}   items.author                   Автор
 * @apiSuccess {Integer}  items.author.id                ID автора
 * @apiSuccess {String}   items.author.title             Имя автора
 * @apiSuccess {String}   items.author.description       Описание автора
 * @apiSuccess {Object}   items.author.image             Картинка автора
 *
 * @apiSuccess {Object}   items.genre                   Жанр
 * @apiSuccess {Integer}  items.genre.id                ID жанра
 * @apiSuccess {String}   items.genre.title             Название жанра
 * @apiSuccess {String}   items.genre.description       Описание жанра
 * @apiSuccess {Object}   items.genre.image             Картинка жанра
 *
 * @apiSuccess {Object[]} items.categories            Массив категорий
 * @apiSuccess {Integer}  items.category.id             ID жанра
 * @apiSuccess {String}   items.category.title          Название категории
 * @apiSuccess {String}   items.category.description    Описание категории
 * @apiSuccess {Object}   items.category.image          Картинка категории
 *
 * @apiSuccess {Object[]} items.tags                    Теги
 * @apiSuccess {Integer}  items.tags.id                 ID тега
 * @apiSuccess {String}   items.tags.title              Название тега
 *
 * @apiSuccess {Object}   items.ebook                   Электронные книги
 * @apiSuccess {Integer}  items.ebook.id                ID
 * @apiSuccess {String}   items.ebook.title             Название книги
 * @apiSuccess {String}   items.ebook.description       Описание книги
 * @apiSuccess {Object[]} items.ebook.files             Файлы
 * @apiSuccess {Integer}  items.ebook.files.id          ID
 * @apiSuccess {String}   items.ebook.files.url         Внешнаяя ссылка
 * @apiSuccess {String}   items.ebook.files.download_url Ссылка на скачивание файла
 * @apiSuccess {Integer}  items.ebook.files.downloads    Количество загрузок файла
 * @apiSuccess {String}   items.ebook.files.book_type   Тип книги (fb2/epub/pdf)
 * @apiSuccess {Object}   items.ebook.files.file        Информация о файле
 *
 * @apiSuccess {Object}   items.audio                   Аудио
 * @apiSuccess {Integer}  items.audio.id                ID
 * @apiSuccess {String}   items.audio.title             Название аудио
 * @apiSuccess {String}   items.audio.description       Описание аудио
 * @apiSuccess {Object[]} items.audio.files             Файлы
 * @apiSuccess {Integer}  items.audio.files.id          ID
 * @apiSuccess {String}   items.audio.files.title       Название конкретного файла
 * @apiSuccess {String}   items.audio.files.description Описание конкретного файла
 * @apiSuccess {String}   items.audio.files.url         Внешнаяя ссылка
 * @apiSuccess {String}   items.audio.files.download_url Ссылка на скачивание файла
 * @apiSuccess {Integer}  items.audio.files.downloads    Количество загрузок файла
 * @apiSuccess {Integer}  items.audio.files.is_full     является ли файл полной версией книги для скачивания (0/1)
 * @apiSuccess {Object[]}   items.audio.files.bible_relations        Связи главы с библией
 *
 * @apiSuccess {Object}   items.video                   Видео
 * @apiSuccess {Integer}  items.video.id                ID
 * @apiSuccess {String}   items.video.title             Название видео
 * @apiSuccess {String}   items.video.description       Описание видео
 * @apiSuccess {Object[]} items.video.files             Файлы
 * @apiSuccess {Integer}  items.video.files.id          ID
 * @apiSuccess {String}   items.video.files.title       Название конкретного файла
 * @apiSuccess {String}   items.video.files.description Описание конкретного файла
 * @apiSuccess {String}   items.video.files.url         Внешнаяя ссылка
 * @apiSuccess {String}   items.video.files.download_url Ссылка на скачивание файла
 * @apiSuccess {Integer}  items.video.files.downloads    Количество загрузок файла
 * @apiSuccess {Integer}  items.video.files.is_full     является ли файл полной версией книги для скачивания (0/1)
 * @apiSuccess {Object[]}   items.video.files.bible_relations      Связи главы с библией
 *
 * @apiSuccess {Object[]}  items.bible_relations                Связи с библией
 * @apiSuccess {Integer}  items.bible_relations.book_id        Id книги
 * @apiSuccess {Integer}   items.bible_relations.verse_id       ID стиха
 * @apiSuccess {Integer}   items.bible_relations.chapter        Номер главы
 * @apiSuccess {Integer}   items.bible_relations.verse_number   Номер стиха
 * @apiSuccess {Integer}   items.bible_relations.book_code      Код книги
 * @apiSuccess {Integer}   items.bible_relations.book_short_title  Короткое название книги
 * @apiSuccess {Integer}   items.bible_relations.short          Короткое название сущности
 *
 * @apiSuccess {Integer}  items.downloads               Общее количество загрузок файлов
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (4xx) {ApiException} error
 */
