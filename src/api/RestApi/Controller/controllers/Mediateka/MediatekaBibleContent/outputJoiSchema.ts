import * as Joi from '@hapi/joi';

export default Joi.object({
    verses: Joi.array()
        .items({
            id: Joi.number().description('ID стиха'),
            number: Joi.number().description('Номер стиха'),
            text: Joi.string().description('Текст стиха'),
            chapter: Joi.number().description('Номер главы'),
            book: Joi.object({
                id: Joi.number().description('ID Книги'),
                testament_id: Joi.number().description('Завет, 1 - Ветхий, 2 - Новый'),
                title: Joi.string().description('Название'),
                short_title: Joi.string().description('Короткое название'),
                parts: Joi.number().description('Количество глав в книге'),
                code: Joi.string().description('Код книги'),
                short: Joi.string().description('Сокращенное написание стиха'),
            }).description('Информация о книге'),
        })
        .description('Список стихов'),
}).required();
