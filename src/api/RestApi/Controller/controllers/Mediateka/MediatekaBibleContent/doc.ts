/**
 * @api {get} /mediateka/bible-content/:id/             Библейски стихи записи
 * @apiVersion 0.1.0
 * @apiName bible-content
 * @apiGroup Mediateka
 *
 *
 * @apiDescription Получить Библейски стихи привязанные к записи
 * @apiPermission All
 *
 * @apiParam {Integer}    id                            ID медиазаписи
 *
 * @apiSuccess {Object[]} verses                        Список стихов
 * @apiSuccess {Integer}  verses.id                     ID стиха
 * @apiSuccess {Integer}  verses.number                 Номер стиха
 * @apiSuccess {String}   verses.text                   Текст стиха
 * @apiSuccess {Integer}  verses.chapter                Номер главы
 * @apiSuccess {Object}   verses.book                   Информация о книге
 * @apiSuccess {Integer}  verses.book.id                ID Книги
 * @apiSuccess {Integer}  verses.book.testament_id      Завет, 1 - Ветхий, 2 - Новый
 * @apiSuccess {String}   verses.book.title             Название
 * @apiSuccess {String}   verses.book.short_title       Короткое название
 * @apiSuccess {Integer}  verses.book.parts             Количество глав в книге
 * @apiSuccess {String}   verses.book.code              Код книги
 * @apiSuccess {String}   verses.book.short             Сокращенное написание стиха
 *
 * @apiError (4xx) {ApiException} error
 */
