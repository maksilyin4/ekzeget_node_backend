import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Lecture from '../../../../../../entities/Lecture';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const [lectures, totalCount] = await Lecture.findAndCount();
    return { lecture: lectures, pages: returnPaginationObject({ totalCount }) };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
