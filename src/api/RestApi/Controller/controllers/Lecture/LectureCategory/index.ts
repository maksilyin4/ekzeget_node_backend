import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import LectureType from '../../../../../../entities/LectureType';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const categories = await LectureType.find();
    return { categories };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
