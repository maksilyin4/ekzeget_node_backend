import * as Joi from '@hapi/joi';

export default Joi.object({
    categories: Joi.array()
        .items(
            Joi.object({
                id: Joi.number().description('id категории'),
                title: Joi.string().description('Название категории'),
            }).unknown()
        )
        .description('Список'),
}).required();
