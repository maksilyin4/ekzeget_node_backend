/**
 * @api {get} /lecture/category/ Список категорий лектория
 * @apiVersion 0.1.0
 * @apiName category
 * @apiGroup Lecture
 *
 *
 * @apiDescription Список категорий лектория
 * @apiPermission All
 *
 * @apiSuccess {Object[]} categories                    Список
 * @apiSuccess {Integer}  categories.id                 id категории
 * @apiSuccess {String}   categories.title              Название категории
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
