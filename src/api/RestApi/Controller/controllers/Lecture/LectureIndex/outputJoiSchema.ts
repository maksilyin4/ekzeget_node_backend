import * as Joi from '@hapi/joi';

export default Joi.object({
    lecture: Joi.array()
        .items(
            Joi.object({
                id: Joi.number().description('ID'),
                title: Joi.string().description('Название'),
                added_at: Joi.number().description('Дата добавления (unixtimestamp)'),
                category: Joi.object({}).description('Категория'),
            }).unknown()
        )
        .description('Список'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    })
        .unknown()
        .description('Информация о возможной постраничной навигации'),
}).required();
