/**
 * @api {get} /lecture/:category_id/?page=:page&per-page=:per-page Список статей
 * @apiVersion 0.1.0
 * @apiName index
 * @apiGroup Lecture
 *
 *
 * @apiDescription Список статей
 * @apiPermission All
 *
 * @apiParam {Integer}  category_id                     ID категории записи (опционально)
 * @apiParam {Integer}  page        Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page    Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} lecture                       Список
 * @apiSuccess {Integer}  lecture.id                    ID
 * @apiSuccess {String}   lecture.title                 Название
 * @apiSuccess {Integer}  lecture.added_at              Дата добавления (unixtimestamp)
 * @apiSuccess {Object}   lecture.category              Категория
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
