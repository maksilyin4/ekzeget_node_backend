import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import LectureType from '../../../../../../entities/LectureType';
import Lecture from '../../../../../../entities/Lecture';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import TextFormatter from '../../../../../../libs/TextFormatter';
import CacheManager from '../../../../../../managers/CacheManager';
import { CacheManagerTTL } from '../../../../../../managers/CacheManager/enum';

type LectureExtended = Lecture & { text: string };
const addText = async (lecture: Lecture): Promise<LectureExtended> => {
    lecture.text = await new TextFormatter(lecture.text).format();
    return lecture;
};

const handler = async ({
    code,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const cacheKey = `LectureIndex-${code}`;
    const cache = await CacheManager.get(cacheKey);
    if (cache) return cache;

    let lectures: Lecture[], totalCount: number;
    switch (typeof code) {
        case 'string':
            const lectureType = await LectureType.findOne({ where: { code } });
            lectures = await Lecture.find({
                where: { type_id: lectureType.id },
            });
            totalCount = lectures.length;
            break;
        case 'number':
            lectures = await Lecture.find({
                where: { type_id: code },
            });
            totalCount = lectures.length;
            break;
    }

    const formattedLectures = await Promise.all(lectures.map(async l => addText(l)));

    const result = { lecture: formattedLectures, pages: returnPaginationObject({ totalCount }) };

    await CacheManager.set(cacheKey, result, CacheManagerTTL.day);

    return result;
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
