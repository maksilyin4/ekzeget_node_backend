import * as Joi from '@hapi/joi';

export default Joi.object({
    lecture: Joi.object({
        id: Joi.number().description('ID'),
        title: Joi.string().description('Название'),
        description: Joi.string().description('Описание'),
        text: Joi.string().description('Текст'),
        added_at: Joi.number().description('Дата добавления (unixtimestamp)'),
        category: Joi.object({}).description('Категория'),
    })
        .unknown()
        .description('Список'),
}).required();
