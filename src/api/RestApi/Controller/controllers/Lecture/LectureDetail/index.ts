import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Lecture from '../../../../../../entities/Lecture';
import TextFormatter from '../../../../../../libs/TextFormatter';
import CacheManager from '../../../../../../managers/CacheManager';
import { CacheManagerTTL } from '../../../../../../managers/CacheManager/enum';

const handler = async ({
    code,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const cacheKey = `LectureDetail-${code}`;
    const cache = await CacheManager.get(cacheKey);
    if (cache) return cache;

    const lecture = await Lecture.findOne({ where: { code } });

    lecture.text = await new TextFormatter(lecture.text).format();
    await CacheManager.set(cacheKey, { lecture }, CacheManagerTTL.day);

    return { lecture };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
