/**
 * @api {get} /lecture/detail/:id/                      Статья
 * @apiVersion 0.1.0
 * @apiName detail
 * @apiGroup Lecture
 *
 *
 * @apiDescription Статья
 * @apiPermission All
 *
 * @apiParam {Integer}  id                              ID записи
 *
 * @apiSuccess {Object} lecture                       Список
 * @apiSuccess {Integer}  lecture.id                    ID
 * @apiSuccess {String}   lecture.title                 Название
 * @apiSuccess {String}   lecture.description           Описание
 * @apiSuccess {String}   lecture.text                  Текст
 * @apiSuccess {Integer}  lecture.added_at              Дата добавления (unixtimestamp)
 * @apiSuccess {Object}   lecture.category              Категория
 *
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
