import * as Joi from '@hapi/joi';

export default Joi.object({
    meta: Joi.object({
        id: Joi.number().description('ID'),
        title: Joi.string()
            .allow('', null)
            .description('Title'),
        description: Joi.string()
            .allow('', null)
            .description('Description'),
        h_one: Joi.string()
            .allow('', null)
            .description('H1'),
        keywords: Joi.string()
            .allow('', null)
            .description('Теги'),
    })
        .allow(null)
        .unknown(true)
        .description('Мета-тег'),
}).required();
