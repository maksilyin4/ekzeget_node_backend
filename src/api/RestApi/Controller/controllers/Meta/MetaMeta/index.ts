import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import { Raw } from 'typeorm';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Meta from '../../../../../../entities/Meta';
import MetaSection from '../../../../../../entities/MetaSection';
import MetaTemplate from '../../../../../../entities/MetaTemplate';
import CacheManager from '../../../../../../managers/CacheManager';
import { CacheManagerTTL } from '../../../../../../managers/CacheManager/enum';

type metaType = Meta | MetaTemplate;

const handler = async ({
    code,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const cacheKey = `MetaMeta-${code}`;
    const cache = await CacheManager.get(cacheKey);
    if (cache) return cache;

    let meta: metaType =
        (await Meta.findOne({
            select: ['id', 'title', 'description', 'h_one', 'keywords', 'section_id'],
            where: { code },
        })) || null;

    if (meta) {
        const metaSection = await MetaSection.findOne({ where: { id: meta.section_id } });
        meta.keywords += metaSection.keywords ? ' ' + metaSection.keywords : '';
        meta.h_one += metaSection.hOne ? ' ' + metaSection.hOne : '';
        meta.title += metaSection.title ? ' ' + metaSection.title : '';
    } else {
        let chunkUrl = code;
        while (chunkUrl.length > 1 && !meta) {
            chunkUrl = chunkUrl.substring(0, chunkUrl.lastIndexOf('/'));
            const chunkCode = chunkUrl + '/';
            meta =
                (await MetaTemplate.findOne({
                    where: {
                        url: Raw(column => `:code LIKE ${column}`, { code: chunkCode }),
                    },
                    order: {
                        level: 'DESC',
                    },
                })) || null;
        }
    }

    await CacheManager.set(cacheKey, { meta }, CacheManagerTTL.day);

    return { meta };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
