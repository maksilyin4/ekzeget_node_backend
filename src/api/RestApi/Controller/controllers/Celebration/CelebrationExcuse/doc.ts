/**
 * @api {get} /celebration/excuse/                      Список правздничных поводов
 * @apiVersion 0.1.0
 * @apiName excuse
 * @apiGroup Celebration
 *
 *
 * @apiDescription Список правздничных поводов
 * @apiPermission All
 *
 * @apiSuccess {Object[]} excuse                         Поводы
 * @apiSuccess {Integer}  excuse.id                      ID повода
 * @apiSuccess {String}   excuse.title                   Название повода
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
