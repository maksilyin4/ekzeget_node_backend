import * as Joi from '@hapi/joi';

export default Joi.object({
    excuse: Joi.array()
        .items({
            id: Joi.number().description('ID повода'),
            title: Joi.string().description('Название повода'),
            active: Joi.number(),
            code: Joi.string().description('Название повода транслитом'),
        })
        .description('Поводы'),
}).required();
