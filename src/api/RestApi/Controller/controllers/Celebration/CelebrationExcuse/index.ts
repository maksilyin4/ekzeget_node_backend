import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import CelebrationExcuse from '../../../../../../entities/CelebrationExcuse';
import { generateSlug } from '../../../../../../libs/Rus2Translit/generateSlug';

const handler = async (): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const celebrationExcuses = await CelebrationExcuse.find();
    return {
        excuse: celebrationExcuses.map(ce => ({
            ...ce,
            code: generateSlug(ce.title),
        })),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
