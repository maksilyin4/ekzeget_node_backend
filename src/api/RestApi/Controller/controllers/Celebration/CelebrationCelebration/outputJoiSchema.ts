import * as Joi from '@hapi/joi';

export default Joi.object({
    celebration: Joi.array()
        .items({
            id: Joi.number().description('ID празника'),
            title: Joi.string().description('Название празника'),
            reading: Joi.array()
                .items({
                    verse_id: Joi.number().description('ID стиха'),
                    short: Joi.string().description('Короткое название стиха'),
                })
                .description('Чтения на празник, возвращает массив содержащий стихи для чтения'),
        })
        .description('Правздники'),
}).required();
