/**
 * @api {get} /celebration/:excuse_id/                  Список праздников
 * @apiVersion 0.1.0
 * @apiName celebration
 * @apiGroup Celebration
 *
 * @apiDescription Список праздников
 * @apiPermission All
 *
 * @apiParam {Integer}  excuse_id                       ID повода празника
 *
 * @apiSuccess {Object[]} celebration                   Правздники
 * @apiSuccess {Integer}  celebration.id                ID празника
 * @apiSuccess {String}   celebration.title             Название празника
 * @apiSuccess {Object[]} celebration.reading           Чтения на празник, возвращает массив содержащий стихи для чтения
 * @apiSuccess {Integer}  celebration.reading.verse_id  ID стиха
 * @apiSuccess {String}   celebration.reading.short     Короткое название стиха
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
