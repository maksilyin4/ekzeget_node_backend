import * as Joi from '@hapi/joi';

export default Joi.object({
    excuse_id: Joi.number().description('ID повода празника'),
}).required();
