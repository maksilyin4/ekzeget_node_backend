/**
 * @api {get} /preaching/search/?q=:q&page=:page&per-page=:per-page  Поиск по проповедям
 * @apiVersion 0.1.0
 * @apiName search
 * @apiGroup Preaching
 *
 *
 * @apiDescription Глобальный поиск по проповедям
 * @apiPermission All
 *
 * @apiParam {String}   q           Строка поиска
 * @apiParam {Integer}  page        Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page    Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} preaching               Проповеди
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
