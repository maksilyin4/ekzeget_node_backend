import * as Joi from '@hapi/joi';
import { paginationObjectScheme } from '../../../../../../libs/helpers/pagination';

const preacherScheme = Joi.object({
    id: Joi.number().description('ID проповедника'),
    name: Joi.string().description('Имя проповедника'),
    active: Joi.number(),
    code: Joi.string(),
}).description('Проповедник');

const excuseParentScheme = Joi.object({
    id: Joi.number(),
    title: Joi.string(),
    active: Joi.number(),
    code: Joi.string(),
});

const excuseScheme = Joi.object({
    id: Joi.number().description('ID проповедника'),
    title: Joi.string(),
    active: Joi.number(),
    parent: excuseParentScheme,
    apostolic: Joi.string(),
    gospel: Joi.string(),
    sort: Joi.number(),
    code: Joi.string(),
}).description('Повод');

export default Joi.object({
    preaching: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            theme: Joi.string().description('Тема проповеди'),
            text: Joi.string().description('Текст проповеди'),
            active: Joi.number(),
            code: Joi.string(),
            preacher: preacherScheme,
            excuse: excuseScheme,
        })
        .description('Список'),
    pages: paginationObjectScheme,
}).required();
