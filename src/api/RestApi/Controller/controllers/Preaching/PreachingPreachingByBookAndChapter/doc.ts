/**
 * @api {get} /preaching/by-book-and-chapter/?book=:book&chapter=:chapter&page=:page&per-page=:per-page Список проповедей по книге и главе
 * @apiVersion 0.1.0
 * @apiName PreachingByBookAndChapter
 * @apiGroup Preaching
 *
 *
 * @apiDescription Список поводов по книге и главе
 * @apiPermission All
 *
 * @apiParam {String}   book        Код книги
 * @apiParam {Integer}  chapter     Глава
 * @apiParam {Integer}  page        Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page    Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} preaching                       Список
 * @apiSuccess {Integer}  preaching.id                    ID
 * @apiSuccess {String}   preaching.theme                 Тема проповеди
 * @apiSuccess {String}   preaching.text                  Текст проповеди
 * @apiSuccess {Object}   preaching.preacher              Проповедник
 * @apiSuccess {Object}   preaching.excuse                Повод
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
