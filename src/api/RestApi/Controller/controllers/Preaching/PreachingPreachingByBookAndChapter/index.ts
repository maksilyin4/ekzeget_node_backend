import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Preaching from '../../../../../../entities/Preaching';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import { paginationParamsWrapper } from '../../../../../../libs/helpers/pagination';
import { preachingByBookAndChapterPresenter } from './preachingByBookAndChapterPresenter';
import PreachingVerse from '../../../../../../entities/PreachingVerse';
import makeArrayUnique from '../../../../../../libs/helpers/makeArrayUnique';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { book = 0, chapter = 0 } = validatedParams;
    const { page, perPage } = paginationParamsWrapper(validatedParams);
    const emptyResponse = {
        preaching: [],
        pages: returnPaginationObject({
            page,
            'per-page': perPage,
            totalCount: 0,
        }),
    };

    if (!book && !chapter) {
        return emptyResponse;
    }

    const preachingVerses: PreachingVerse[] = await PreachingVerse.createQueryBuilder('pv')
        .select('pv.preachingId')
        .leftJoin('pv.verse', 'verse')
        .leftJoin('verse.book', 'book')
        .where('book.code = :book', { book })
        .andWhere('verse.chapter = :chapter', { chapter })
        .getMany();

    if (!preachingVerses.length) {
        return emptyResponse;
    }

    const preachingIds = preachingVerses.map(pv => pv.preachingId).filter(makeArrayUnique);

    const [data, total] = await Preaching.createQueryBuilder('preaching')
        .leftJoinAndSelect('preaching.preacher', 'preacher')
        .leftJoinAndSelect('preaching.excuse', 'excuse')
        .leftJoinAndSelect('excuse.parent', 'parent')
        .where('preaching.id in (:...preachingIds)', { preachingIds })
        .andWhere('preaching.active = :active', { active: 1 })
        .skip((page - 1) * perPage)
        .take(perPage)
        .orderBy('preacher.name', 'ASC')
        .getManyAndCount();

    return {
        preaching: await preachingByBookAndChapterPresenter(data),
        pages: returnPaginationObject({
            page,
            'per-page': perPage,
            totalCount: +total,
        }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
