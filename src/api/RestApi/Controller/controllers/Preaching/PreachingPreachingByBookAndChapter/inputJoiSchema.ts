import * as Joi from '@hapi/joi';

export default Joi.object({
    book: Joi.string().description('Код книги'),
    chapter: Joi.number().description('Глава'),
    page: Joi.number()
        .optional()
        .description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number()
        .optional()
        .description('Количество элементов на странице (опционально, по умолчанию 20)'),
}).required();
