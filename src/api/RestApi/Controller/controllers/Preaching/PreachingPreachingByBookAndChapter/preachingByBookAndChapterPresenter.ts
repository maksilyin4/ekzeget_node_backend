import Preaching from '../../../../../../entities/Preaching';
import { generateSlug } from '../../../../../../libs/Rus2Translit/generateSlug';
import TextFormatter from '../../../../../../libs/TextFormatter';

export const preachingByBookAndChapterPresenter = async (data: Preaching[]) => {
    const promises = data.map(async preaching => {
        const formattedText: Promise<string> = new TextFormatter(preaching.text).format();
        return {
            id: preaching.id,
            theme: preaching.theme,
            text: await formattedText,
            active: preaching.active,
            code: preaching.code,
            preacher: {
                id: preaching.preacher.id,
                name: preaching.preacher.name,
                active: preaching.preacher.active,
                code: preaching.preacher.code,
            },
            excuse: {
                id: preaching.excuse.id,
                title: preaching.excuse.title,
                active: preaching.excuse.active,
                parent: {
                    id: preaching.excuse.parent.id,
                    title: preaching.excuse.parent.title,
                    active: preaching.excuse.parent.active,
                    code: generateSlug(preaching.excuse.parent.title),
                },
                apostolic: preaching.excuse.apostolic,
                gospel: preaching.excuse.gospel,
                sort: preaching.excuse.sort,
                code: preaching.excuse.code,
            },
        };
    });
    return await Promise.all(promises);
};
