import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Preaching from '../../../../../../entities/Preaching';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import Excuse from '../../../../../../entities/Excuse';
import Preacher from '../../../../../../entities/Preacher';
import { distinctByKey } from '../../../../../../libs/helpers/distinctByKey';
import { sortByKeyASC } from '../../../../../../libs/helpers/sort';

const preachersPresenter = (preachings: Preaching[]): Preacher[] => {
    const preachers = preachings.map(p => p.preacher);
    const distinct: Preacher[] = distinctByKey(preachers, 'id');

    return distinct.sort((a, b) => sortByKeyASC(a, b, 'name'));
};

const handler = async ({
    excuse_id,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { id } = (await Excuse.findOne({
        where: {
            code: excuse_id,
        },
    })) || { id: null };
    const preachings = await Preaching.find({
        where: {
            excuse_id: id,
            active: 1,
        },
        relations: ['preacher'],
    });
    const preachers = preachersPresenter(preachings);

    return {
        preacher: preachers,
        pages: returnPaginationObject({ totalCount: preachers.length }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
