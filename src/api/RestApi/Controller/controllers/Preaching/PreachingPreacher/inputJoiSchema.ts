import * as Joi from '@hapi/joi';

export default Joi.object({
    excuse_id: Joi.string()
        .optional()
        .description('ID повода'),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
}).required();
