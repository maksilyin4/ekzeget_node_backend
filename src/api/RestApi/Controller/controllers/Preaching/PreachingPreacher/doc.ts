/**
 * @api {get} /preaching/preacher/?excuse_id=:excuse_id&page=:page&per-page=:per-page Список проповедников
 * @apiVersion 0.1.0
 * @apiName Preacher
 * @apiGroup Preaching
 *
 *
 * @apiDescription Список проповедников
 * @apiPermission All
 *
 * @apiParam {Integer}  excuse_id   id повода (опционально)
 * @apiParam {Integer}  page        Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page    Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} preacher                       Список
 * @apiSuccess {Integer}  preacher.id                    ID
 * @apiSuccess {String}   preacher.name                  Имя проповедника
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
