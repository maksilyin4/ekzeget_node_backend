import * as Joi from '@hapi/joi';
import { paginationObjectScheme } from '../../../../../../libs/helpers/pagination';

export default Joi.object({
    preacher: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            name: Joi.string().description('Имя проповедника'),
            active: Joi.number(),
            code: Joi.string(),
        })
        .description('Список'),
    pages: paginationObjectScheme,
}).required();
