import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import { paginationParamsWrapper } from '../../../../../../libs/helpers/pagination';
import Preaching from '../../../../../../entities/Preaching';
import Preacher from '../../../../../../entities/Preacher';
import Excuse from '../../../../../../entities/Excuse';
import Verse from '../../../../../../entities/Verse';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import { generateSlug } from '../../../../../../libs/Rus2Translit/generateSlug';
import { getVerseShortCode } from '../../../presenters/Verse/getVerseShortCode';
import PreachingVerse from '../../../../../../entities/PreachingVerse';
import returnCodeOrIdConditionObject from '../../../../../../libs/helpers/returnCodeOrIdConditionObject';
import TextFormatter from '../../../../../../libs/TextFormatter';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { preacher_id, excuse_id, verse_id } = validatedParams;

    const { page, perPage } = paginationParamsWrapper(validatedParams);

    const query = Preaching.createQueryBuilder('p')
        .select('p')
        .leftJoinAndSelect('p.preacher', 'preacher')
        .leftJoinAndSelect('p.excuse', 'excuse')
        .leftJoinAndSelect('excuse.parent', 'celebrationExcuse')
        .where('1 = 1'); // Хак для избежания проверок наличия where перед andWhere

    if (preacher_id) {
        const preacher = await Preacher.findOne({
            where: returnCodeOrIdConditionObject(preacher_id),
        });
        if (!preacher) {
            return {
                preaching: [],
                excuse: null,
                gospel: [],
                apostolic: [],
                pages: returnPaginationObject({
                    ...validatedParams,
                    totalCount: 0,
                }),
            };
        }

        query.andWhere('p.preacher_id = :preacher_id', { preacher_id: preacher.id });
    }

    if (excuse_id) {
        const excuse = await Excuse.findOne({ where: returnCodeOrIdConditionObject(excuse_id) });
        if (!excuse) {
            throw new ServerError('Данные не найдены', 404);
        }

        query.andWhere('p.excuse_id = :excuse_id', { excuse_id: excuse.id });
    }

    let preaching: Preaching[], totalCount: number;
    if (!verse_id) {
        [preaching, totalCount] = await query
            .take(perPage)
            .skip(page)
            .orderBy('p.id', 'ASC')
            .getManyAndCount();
    } else {
        const preachingVerses = await PreachingVerse.createQueryBuilder('pv')
            .select('pv')
            .leftJoinAndSelect('pv.preaching', 'preaching')
            .where('pv.verseId = :verse_id', { verse_id })
            .getMany();
        const preachingIds = preachingVerses.map(pv => pv.preaching.id);

        [preaching, totalCount] = await query
            .andWhereInIds(preachingIds)
            .take(perPage)
            .skip(page)
            .orderBy('preacher.name', 'ASC')
            .getManyAndCount();
    }

    const gospelVerses = preaching.length
        ? await Verse.getByShortLink(preaching[0].excuse.gospel)
        : [];

    const apostolicVerses = preaching.length
        ? await Verse.getByShortLink(preaching[0].excuse.apostolic)
        : [];

    const extractExcuse = (p: Preaching) => ({
        id: p.excuse.id,
        title: p.excuse.title,
        active: p.excuse.active,
        parent: {
            id: p.excuse.parent.id,
            title: p.excuse.parent.title,
            active: p.excuse.parent.active,
            code: generateSlug(p.excuse.parent.title),
        },
        apostolic: p.excuse.apostolic,
        gospel: p.excuse.gospel,
        sort: p.excuse.sort,
        code: p.excuse.code,
    });

    const preachingWrapper = async (preachings: Preaching[]) => {
        const promises = preachings.map(async p => ({
            id: p.id,
            theme: p.theme,
            text: await new TextFormatter(p.text).format(),
            active: p.active,
            code: p.code,
            preacher: p.preacher,
            excuse: extractExcuse(p),
        }));

        return Promise.all(promises);
    };

    return {
        preaching: await preachingWrapper(preaching),
        excuse: preaching.length ? extractExcuse(preaching[0]) : null,
        gospel: gospelVerses.map(verse => {
            const { book, ...rest } = verse;
            return {
                ...rest,
                book_code: verse.book.code,
                short: getVerseShortCode(verse.book, verse),
            };
        }),
        apostolic: apostolicVerses.map(verse => {
            const { book, ...rest } = verse;
            return {
                ...rest,
                book_code: verse.book.code,
                short: getVerseShortCode(verse.book, verse),
            };
        }),
        pages: returnPaginationObject({
            ...validatedParams,
            totalCount,
        }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
