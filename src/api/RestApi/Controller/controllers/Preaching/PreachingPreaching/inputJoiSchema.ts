import * as Joi from '@hapi/joi';

export default Joi.object({
    preacher_id: Joi.string()
        .description('ID проповедника (опционально)')
        .optional(),
    excuse_id: Joi.string()
        .description('ID повода (опционально)')
        .allow('', null)
        .optional(),
    verse_id: Joi.number()
        .description('ID стиха, к которому относится проповедь (опционально)')
        .optional(),
    page: Joi.number()
        .description('Номер страницы (опционально, по умолчанию 1)')
        .default(1)
        .optional(),
    'per-page': Joi.number()
        .description('Количество элементов на странице (опционально, по умолчанию 20)')
        .default(20)
        .optional(),
}).required();
