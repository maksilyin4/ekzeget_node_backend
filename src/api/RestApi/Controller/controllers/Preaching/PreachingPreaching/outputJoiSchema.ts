import * as Joi from '@hapi/joi';
import { paginationObjectScheme } from '../../../../../../libs/helpers/pagination';

const excuseScheme = Joi.object({
    id: Joi.number(),
    title: Joi.string(),
    active: Joi.number(),
    parent: Joi.object({
        id: Joi.number(),
        title: Joi.string(),
        active: Joi.number(),
        code: Joi.string(),
    }),
    apostolic: Joi.string().allow(''),
    gospel: Joi.string().allow(''),
    sort: Joi.number(),
    code: Joi.string(),
})
    .allow(null)
    .description('Повод');

const versesScheme = Joi.array().items({
    id: Joi.number(),
    number: Joi.number(),
    book_id: Joi.number(),
    text: Joi.string(),
    chapter: Joi.number(),
    chapter_id: Joi.number(),
    book_code: Joi.string(),
    short: Joi.string(),
});

export default Joi.object({
    preaching: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            theme: Joi.string().description('Тема проповеди'),
            text: Joi.string().description('Текст проповеди'),
            active: Joi.number().description('Активность'),
            code: Joi.string().description('Код проповеди'),
            preacher: Joi.object({
                id: Joi.number(),
                name: Joi.string(),
                active: Joi.number(),
                code: Joi.string(),
            }).description('Проповедник'),
            excuse: excuseScheme,
        })
        .description('Список'),
    excuse: excuseScheme,
    gospel: versesScheme.description('Евангелие'),
    apostolic: versesScheme.description('Апостол'),
    pages: paginationObjectScheme,
}).required();
