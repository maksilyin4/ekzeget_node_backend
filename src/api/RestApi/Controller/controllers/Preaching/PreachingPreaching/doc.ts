/**
 * @api {get} /preaching/?preacher_id=:preacher_id&excuse_id=:excuse_id&verse_id=verse_id&page=:page&per-page=:per-page Список проповедей
 * @apiVersion 0.1.0
 * @apiName Preaching
 * @apiGroup Preaching
 *
 *
 * @apiDescription Список поводов
 * @apiPermission All
 *
 * @apiParam {Integer}  preacher_id        ID проповедника (опционально)
 * @apiParam {Integer}  excuse_id    ID повода (опционально)
 * @apiParam {Integer}  verse_id    ID стиха, к которому относится проповедь (опционально)
 * @apiParam {Integer}  page        Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page    Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} preaching                       Список
 * @apiSuccess {Integer}  preaching.id                    ID
 * @apiSuccess {String}   preaching.theme                 Тема проповеди
 * @apiSuccess {String}   preaching.text                  Текст проповеди
 * @apiSuccess {Object}   preaching.preacher              Проповедник
 * @apiSuccess {Object}   preaching.excuse                Повод
 * @apiSuccess {Object[]} gospel                          Евангилие
 * @apiSuccess {Object[]} apostolic                       Апостол
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
