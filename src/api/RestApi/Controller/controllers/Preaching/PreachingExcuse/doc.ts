/**
 * @api {get} /preaching/excuse/?page=:page&per-page=:per-page Список поводов
 * @apiVersion 0.1.0
 * @apiName Excuse
 * @apiGroup Preaching
 *
 *
 * @apiDescription Список поводов
 * @apiPermission All
 *
 * @apiParam {Integer}  page        Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page    Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} excuse                       Список
 * @apiSuccess {Integer}  excuse.id                    ID
 * @apiSuccess {Object[]}  excuse.parent               Массив групп
 * @apiSuccess {String}   excuse.title                 Название повода
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
