import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Excuse from '../../../../../../entities/Excuse';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const [excuses, totalCount] = await Excuse.findAndCount({
        take: validatedParams['per-page'],
        skip: validatedParams['per-page'] * validatedParams.page,
        relations: ['parent'],
        order: { sort: 'ASC' },
    });

    return { excuse: excuses, pages: returnPaginationObject({ ...validatedParams, totalCount }) };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
