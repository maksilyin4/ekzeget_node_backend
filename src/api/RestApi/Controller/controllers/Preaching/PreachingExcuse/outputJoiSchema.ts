import * as Joi from '@hapi/joi';

export default Joi.object({
    excuse: Joi.array()
        .items(
            Joi.object({
                id: Joi.number().description('ID'),
                parent: Joi.object({
                    id: Joi.number().description('ID повода'),
                    title: Joi.string().description('Название повода'),
                    active: Joi.number(),
                }).description('Массив групп'),
                title: Joi.string().description('Название повода'),
            }).unknown()
        )
        .description('Список'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    })
        .unknown()
        .description('Информация о возможной постраничной навигации'),
}).required();
