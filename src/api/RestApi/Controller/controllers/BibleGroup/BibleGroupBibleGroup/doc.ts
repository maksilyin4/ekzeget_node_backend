/**
 * @api {get} /bible-group/                             Список библейских групп
 * @apiVersion 0.1.0
 * @apiName bible-group
 * @apiGroup BibleGroup
 *
 *
 * @apiDescription Список библейских групп
 * @apiPermission All
 *
 * @apiSuccess {Object[]} group                          Список
 * @apiSuccess {Integer}  group.id                       ID
 * @apiSuccess {String}   group.data                     Дата добавления
 * @apiSuccess {String}   group.name                     Название группы
 * @apiSuccess {String}   group.foto                     URL фотографии
 * @apiSuccess {String}   group.history                  История
 * @apiSuccess {String}   group.location                 Местонахождение
 * @apiSuccess {String}   group.days                     Дни собраний
 * @apiSuccess {String}   group.times                    Время собраний
 * @apiSuccess {String}   group.vedet                    Ведущий
 * @apiSuccess {String}   group.priester
 * @apiSuccess {String}   group.phone                    Телефон
 * @apiSuccess {String}   group.email                    email
 * @apiSuccess {String}   group.web                      URL web-страницы
 * @apiSuccess {String}   group.we_reads                 Чтения
 * @apiSuccess {String}   group.kak_prohodit             Как проходит
 * @apiSuccess {String}   group.type                     Тип
 * @apiSuccess {String}   group.coords                   Координаты
 * @apiSuccess {String}   group.country                  Страна
 * @apiSuccess {String}   group.region                   Регион
 *
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
