import * as Joi from '@hapi/joi';

export default Joi.object({
    group: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            data: Joi.string().description('Дата добавления'),
            name: Joi.string().description('Название группы'),
            foto: Joi.string()
                .description('URL фотографии')
                .allow('', null),
            history: Joi.string()
                .description('История')
                .allow('', null),
            location: Joi.string().description('Местонахождение'),
            days: Joi.string().description('Дни собраний'),
            times: Joi.string().description('Время собраний'),
            vedet: Joi.string()
                .description('Ведущий')
                .allow('', null),
            priester: Joi.string().allow('', null),
            phone: Joi.string()
                .description('Телефон')
                .allow('', null),
            email: Joi.string().description('email'),
            web: Joi.string()
                .description('URL web-страницы')
                .allow('', null),
            we_reads: Joi.string()
                .description('Чтения')
                .allow('', null),
            kak_prohodit: Joi.string()
                .description('Как проходит')
                .allow('', null),
            type: Joi.array().items(Joi.any()),
            coords: Joi.string()
                .description('Координаты')
                .allow('', null),
            country: Joi.string().description('Страна'),
            region: Joi.string().description('Регион'),
            active: Joi.number().description('Активна ли группа'),
            code: Joi.string()
                .description('Код группы')
                .allow('', null),
        })
        .description('Список'),
}).required();
