import * as Joi from '@hapi/joi';

export default Joi.object({
    limit: Joi.number().optional(),
}).required();
