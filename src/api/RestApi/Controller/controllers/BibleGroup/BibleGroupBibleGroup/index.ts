import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import BibleGroup from '../../../../../../entities/BibleGroup';
import BibleGroupsTypes from '../../../../../../entities/BibleGroupsTypes';
import { groupBy } from '../../../../../../libs/helpers/groupBy';
import BibleGroupType from '../../../../../../entities/BibleGroupType';

const typePresenter = ({ type, typeId }: BibleGroupsTypes): Partial<BibleGroupType> => {
    if (!typeId) return null; // У некоторых групп не указан тип

    return {
        id: type.id,
        title: type.title,
        description: type.description,
    };
};

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const [groups, groupToTypeLink] = await Promise.all([
        BibleGroup.find({
            order: {
                id: 'DESC',
            },
        }),
        BibleGroupsTypes.find({
            loadEagerRelations: true,
            relations: ['type'],
        }),
    ]);

    const groupTypeIDsByGroupId = groupBy(groupToTypeLink, 'groupId');

    return {
        group: groups.map(group => {
            const types = groupTypeIDsByGroupId[group.id] || [];
            return {
                ...group,
                type: types.map(t => typePresenter(t)).filter(Boolean),
            };
        }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
