import * as Joi from '@hapi/joi';

export default Joi.object({
    note: Joi.object({
        id: Joi.number().description('ID'),
        data: Joi.string().description('Дата добавления'),
        title: Joi.string().description('Название группы'),
        text: Joi.string().description('Информация'),
        views: Joi.number().description('Количество просмотров'),
        type: Joi.number().description('Тип TYPE'),
        tags: Joi.string().allow(null, ''),
        active: Joi.number(),
        code: Joi.string(),
    }).description('Список'),
}).required();
