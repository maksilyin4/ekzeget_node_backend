/**
 * @api {get} /bible-group/note-detail/:id/              Детальная Информация/описания
 * @apiVersion 0.1.0
 * @apiName note-detail
 * @apiGroup BibleGroup
 *
 *
 * @apiDescription Информация/описания
 * @apiPermission All
 *
 * @apiParam {Integer}    type                          Тип записи (опционально)
 *
 * @apiSuccess {Object} note                          Список
 * @apiSuccess {Integer}  note.id                       ID
 * @apiSuccess {String}   note.data                     Дата добавления
 * @apiSuccess {String}   note.title                    Название группы
 * @apiSuccess {String}   note.text                     Информация
 * @apiSuccess {Integer}   note.views                   Количество просмотров
 * @apiSuccess {Integer}   note.type                    Тип TYPE = [
 * 1 => 'Для начинающих',
 * 2 => 'Материалы семинара для ведущих',
 * 3 => 'Опыт и вопросы',
 * 4 => 'Вопросы и ответы по ведению кружков',
 * ];
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
