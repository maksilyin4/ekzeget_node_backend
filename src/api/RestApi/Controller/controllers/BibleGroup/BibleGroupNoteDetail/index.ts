import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import BibleGroupNote from '../../../../../../entities/BibleGroupNote';
import returnCodeOrIdConditionObject from '../../../../../../libs/helpers/returnCodeOrIdConditionObject';
import { generateSlug } from '../../../../../../libs/Rus2Translit/generateSlug';
import { ServerError } from '../../../../../../libs/ErrorHandler';

const handler = async ({
    id,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const note = await BibleGroupNote.findOne({ where: returnCodeOrIdConditionObject(id) });
    if (!note) throw new ServerError('Группа не найдена', 404);

    note.views = (+note.views + 1).toString();
    await note.save();

    return {
        note: {
            ...note,
            type: +note.type,
            views: +note.views,
            code: generateSlug(note.title),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
