import * as Joi from '@hapi/joi';

export default Joi.object({
    id: Joi.string().description('ID записи или код записи (опционально)'),
}).required();
