import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import BibleGroupType from '../../../../../../entities/BibleGroupType';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const type = await BibleGroupType.find();
    return { type };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
