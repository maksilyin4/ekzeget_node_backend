import * as Joi from '@hapi/joi';

export default Joi.object({
    type: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            title: Joi.string().description('Название'),
            description: Joi.string()
                .description('Описание')
                .allow('', null),
            active: Joi.number(),
        })
        .description('Список'),
}).required();
