/**
 * @api {get} /bible-group/type/                       Типы
 * @apiVersion 0.1.0
 * @apiName type
 * @apiGroup BibleGroup
 *
 *
 * @apiDescription Информация/описания
 * @apiPermission All
 *
 *
 * @apiSuccess {Object[]} type                          Список
 * @apiSuccess {Integer}  type.id                       ID
 * @apiSuccess {String}   type.title                    Название
 * @apiSuccess {String}   type.description             Описание
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
