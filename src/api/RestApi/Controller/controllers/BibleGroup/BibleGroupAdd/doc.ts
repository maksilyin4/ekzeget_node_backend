/**
 * @api {post} /bible-group/add/                       Добавить группу
 * @apiVersion 0.1.0
 * @apiName add
 * @apiGroup BibleGroup
 *
 *
 * @apiDescription Добавить библейскую группу
 * @apiPermission All
 *
 * @apiParam {String}   name                     Название группы
 * @apiParam {File}     foto                     фотография
 * @apiParam {String}   history                  История
 * @apiParam {String}   location                 Местонахождение
 * @apiParam {String}   days                     Дни собраний
 * @apiParam {String}   times                    Время собраний
 * @apiParam {String}   vedet                    Ведущий
 * @apiParam {String}   priester
 * @apiParam {String}   phone                    Телефон
 * @apiParam {String}   email                    email
 * @apiParam {String}   web                      URL web-страницы
 * @apiParam {String}   we_reads                 Чтения
 * @apiParam {String}   kak_prohodit             Как проходит
 * @apiParam {Object[]} type                     Тип
 * @apiParam {String}   coords                   Координаты
 *
 * @apiSuccess {String}   status
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
