import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import BibleGroup from '../../../../../../entities/BibleGroup';
import BibleGroupsTypes from '../../../../../../entities/BibleGroupsTypes';
import { generateSlug } from '../../../../../../libs/Rus2Translit/generateSlug';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import BibleGroupType from '../../../../../../entities/BibleGroupType';
import getCurrentDateTimeString from '../../../../../../libs/helpers/getCurrentDateTimeString';
import EnvironmentManager from '../../../../../../managers/EnvironmentManager';

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { session }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { CHECK_CAPTCHA } = EnvironmentManager.envs;
    if (+CHECK_CAPTCHA && session.captcha !== +params.captcha)
        throw new ServerError(errorStrings.badCaptcha, 400);

    const typeIds = params.type.map(t => +t);
    const typesAsSingleString = await BibleGroupType.getGroupTypeAsString(typeIds);

    let status = 'ok';
    try {
        const bibleGroup = await BibleGroup.create({
            data: getCurrentDateTimeString(),
            name: params.name,
            code: generateSlug(params.name),
            location: params.location,
            country: params.country,
            region: params.region,
            coords: params.coords,
            days: params.days,
            times: params.times,
            vedet: params.vedet || '',
            priester: params.priester || '',
            phone: params.phone || '',
            email: params.email,
            web: params.web || '',
            we_reads: params.we_reads || '',
            kak_prohodit: params.kak_prohodit || '',
            history: params.history || '',
            type: typesAsSingleString,
            foto: params.foto || '',
            active: 0, // При добавлении группа неактивна и отправляется на модерацию
        }).save();

        await BibleGroupsTypes.createBibleGroupsTypesForBibleGroup({
            bibleGroupId: bibleGroup.id,
            typeIds,
        });
    } catch (e) {
        console.error(e.message);
        status = 'error';
    }

    return { status };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
