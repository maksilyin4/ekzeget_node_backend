import * as Joi from '@hapi/joi';

export default Joi.object({
    name: Joi.string()
        .description('Ваш храм или название группы')
        .required(),
    location: Joi.string()
        .description('Адрес')
        .required(),
    days: Joi.string()
        .description('Дни собраний')
        .required(),
    times: Joi.string()
        .description('Время начала')
        .required(),
    vedet: Joi.string()
        .description('Ведущий')
        .allow('')
        .optional(),
    priester: Joi.string()
        .description('Духовник группы (или кто благословил)')
        .allow('')
        .optional(),
    phone: Joi.string()
        .description('Телефон')
        .allow('')
        .optional(),
    email: Joi.string()
        .description('email')
        .required(),
    web: Joi.string()
        .description('URL web-страницы')
        .allow('')
        .optional(),
    we_reads: Joi.string()
        .description('Чтения / Что вы изучаете на занятиях')
        .allow('')
        .optional(),
    kak_prohodit: Joi.string()
        .description('Как проходит')
        .allow('')
        .optional(),
    history: Joi.string()
        .description('История')
        .allow('')
        .optional(),
    type: Joi.array()
        .items(Joi.string().required())
        .description('Типы группы')
        .required(),
    materials: Joi.string()
        .description('Какие материалы по теме работы библейских групп вам были бы интересны?')
        .allow('')
        .optional(),
    hardness: Joi.string()
        .description('С какими трудностями вы сталкивались в работе группы?')
        .allow('')
        .optional(),
    captcha: Joi.string()
        .description('Captcha')
        .allow('')
        .optional(),
    verifyCode: Joi.string()
        .description('Captcha 2?')
        .allow('')
        .optional(),

    coords: Joi.string()
        .description('Координаты')
        .example('56.29434329999999, 43.9511435')
        .required(),
    region: Joi.string()
        .description('Регион')
        .example("Nizhegorodskaya oblast'")
        .required(),
    country: Joi.string()
        .description('Страна')
        .example('Russia')
        .required(),
    foto: Joi.string()
        .description('Фотография')
        .allow('')
        .optional(),
}).required();
