import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import BibleGroup from '../../../../../../entities/BibleGroup';
import { ServerError } from '../../../../../../libs/ErrorHandler';

const handler = async (
    { id }: Joi.extractType<typeof inputJoiSchema>,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {

    let status = 'ok';
    try {
        const bibleGroup = await BibleGroup.findOne({ where: { id } });
        if (!bibleGroup) {
            throw new ServerError('Группа не найдена', 404);
        }
        await bibleGroup.remove();
    } catch (e) {
        console.error(e.message);
        status = 'error';
    }

    return { status };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
