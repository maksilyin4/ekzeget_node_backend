import * as Joi from '@hapi/joi';

export default Joi.object({
    id: Joi.number()
        .description('ID группы')
        .required(),
}).required();
