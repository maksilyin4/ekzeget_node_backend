import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import BibleGroup from '../../../../../../entities/BibleGroup';
import BibleGroupsTypes from '../../../../../../entities/BibleGroupsTypes';
import { generateSlug } from '../../../../../../libs/Rus2Translit/generateSlug';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import BibleGroupType from '../../../../../../entities/BibleGroupType';
import getCurrentDateTimeString from '../../../../../../libs/helpers/getCurrentDateTimeString';
import EnvironmentManager from '../../../../../../managers/EnvironmentManager';

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { session }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { CHECK_CAPTCHA } = EnvironmentManager.envs;
    if (+CHECK_CAPTCHA && session.captcha !== +params.captcha)
        throw new ServerError(errorStrings.badCaptcha, 400);

    const [bibleGroup, groupWithNewNameExists] = await Promise.all([
        BibleGroup.findOne({
            where: {
                id: params.id,
            },
        }),
        BibleGroup.findOne({
            where: {
                code: generateSlug(params.name),
            },
        }),
    ]);
    if (!bibleGroup) {
        throw new ServerError(`Библейская группа не найдена`, 404);
    }

    if (groupWithNewNameExists) {
        throw new ServerError(`Группа с таким названием уже существует`, 400);
    }

    const typeIds = params.type.map(t => +t);
    const typesAsSingleString = await BibleGroupType.getGroupTypeAsString(typeIds);

    let status = 'ok';
    try {
        bibleGroup.data = getCurrentDateTimeString();
        bibleGroup.name = params.name;
        bibleGroup.code = generateSlug(params.name);
        bibleGroup.location = params.location;
        bibleGroup.country = params.country;
        bibleGroup.region = params.region;
        bibleGroup.coords = params.coords;
        bibleGroup.days = params.days;
        bibleGroup.times = params.times;
        bibleGroup.vedet = params.vedet || '';
        bibleGroup.priester = params.priester || '';
        bibleGroup.phone = params.phone || '';
        bibleGroup.email = params.email;
        bibleGroup.web = params.web || '';
        bibleGroup.we_reads = params.we_reads || '';
        bibleGroup.kak_prohodit = params.kak_prohodit || '';
        bibleGroup.history = params.history || '';
        bibleGroup.type = typesAsSingleString;
        bibleGroup.foto = params.foto || '';
        bibleGroup.active = 0; // При редактировании группа становится неактивной для повторной модерации
        await bibleGroup.save();

        await BibleGroupsTypes.deleteByGroupId(bibleGroup.id);
        await BibleGroupsTypes.createBibleGroupsTypesForBibleGroup({
            bibleGroupId: bibleGroup.id,
            typeIds,
        });
    } catch (e) {
        console.error(e.message);
        status = 'error';
    }

    return { status };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
