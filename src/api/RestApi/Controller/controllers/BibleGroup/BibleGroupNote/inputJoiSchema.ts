import * as Joi from '@hapi/joi';

export default Joi.object({
    type: Joi.number()
        .description('Тип записи (опционально)')
        .optional(),
}).required();
