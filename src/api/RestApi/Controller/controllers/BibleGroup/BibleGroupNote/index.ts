import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import BibleGroupNote from '../../../../../../entities/BibleGroupNote';
import { generateSlug } from '../../../../../../libs/Rus2Translit/generateSlug';

const handler = async ({
    type,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const all = await BibleGroupNote.findByType(type);

    return {
        note: all.map(note => ({
            id: note.id,
            data: note.data,
            title: note.title,
            views: +note.views,
            type: +note.type,
            code: note.code,
        })),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
