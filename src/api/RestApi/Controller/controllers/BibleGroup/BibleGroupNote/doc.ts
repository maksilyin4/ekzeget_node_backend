/**
 * @api {get} /bible-group/note/:type/                        Информация/описания
 * @apiVersion 0.1.0
 * @apiName note
 * @apiGroup BibleGroup
 *
 *
 * @apiDescription Информация/описания
 * @apiPermission All
 *
 * @apiParam {Integer}     type                          Тип записи (опционально)
 *
 * @apiSuccess {Object[]}  note                          Список
 * @apiSuccess {Integer}   note.id                       ID
 * @apiSuccess {String}    note.data                     Дата добавления
 * @apiSuccess {String}    note.title                    Название группы
 * @apiSuccess {Integer}   note.views                   Количество просмотров
 * @apiSuccess {Integer}   note.type                    Тип
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
