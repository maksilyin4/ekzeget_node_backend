import * as Joi from '@hapi/joi';

export default Joi.object({
    note: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            data: Joi.string().description('Дата добавления'),
            title: Joi.string().description('Название группы'),
            views: Joi.number().description('Количество просмотров'),
            type: Joi.number().description('Тип'),
            code: Joi.string().description('Код'),
        })
        .description('Список'),
}).required();
