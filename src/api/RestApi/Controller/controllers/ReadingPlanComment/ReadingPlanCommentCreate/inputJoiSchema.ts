import * as Joi from '@hapi/joi';

export default Joi.object({
    day: Joi.number().integer().positive(),
    group_id: Joi.number().integer().positive(),
    content: Joi.string().required().min(5),
    reply_to_comment_id: Joi.number().positive().optional(),
}).required();
