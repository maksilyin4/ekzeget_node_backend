import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import ReadingPlanComment from '../../../../../../entities/ReadingPlanComment';
import { userPresenter } from '../../../presenters/User';

const handler = async (
    commentDto: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userId = typeof user === 'number'
        ? user
        : user?.id;
    if (!userId)
        throw new ServerError(errorStrings.error401, 401);

    const created = await ReadingPlanComment.create({
        ...commentDto,
        author_id: userId,
    }).save();

    if (created.id === created.reply_to_comment_id) {
        await created.remove();
        throw new ServerError("Comment can't reply to itself.", 400);
    }

    const comment = await ReadingPlanComment.findOne(created.id, {
        relations: ['author', 'author.userProfile'],
    });

    return { comment: { ...comment, author: userPresenter(comment.author) } };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
