import * as Joi from '@hapi/joi';

export default Joi.object({
    comment: Joi.object({
        id: Joi.number().integer().positive().required(),
        author_id: Joi.number().integer().positive().required(),
        author: Joi.object().unknown().required(),
        day: Joi.number().integer().positive().required(),
        group_id: Joi.number().integer().positive().allow(null),
        content: Joi.string().required().min(5),
        reply_to_comment_id: Joi.number().integer().positive().allow(null),
        created_at: Joi.date().required(),
        updated_at: Joi.date().required(),
    }),
}).required();
