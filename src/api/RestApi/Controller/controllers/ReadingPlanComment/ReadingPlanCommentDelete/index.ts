import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import ReadingPlanComment from '../../../../../../entities/ReadingPlanComment';

const handler = async (
    { id: commentId }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userId = typeof user === 'number'
        ? user
        : user?.id;
    if (!userId)
        throw new ServerError(errorStrings.error401, 401);

    const comment = await ReadingPlanComment.findOne(
        commentId,
        { relations: ['group'] },
    );
    if (!comment)
        throw new ServerError(`Comment with id=${commentId} not found.`, 404);
    if (comment.author_id !== userId && comment.group?.mentor_id !== userId)
        throw new ServerError(`You can't delete other people's comments.`, 403);

    await comment.remove();
    return { status: 'ok' };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
