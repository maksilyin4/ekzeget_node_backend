import * as Joi from '@hapi/joi';

const commentSchema = Joi.object({
    id: Joi.number().integer().positive().required(),
    author_id: Joi.number().integer().positive().required(),
    author: Joi.object().unknown().required(),
    day: Joi.number().integer().positive().required(),
    group_id: Joi.number().integer().positive().allow(null),
    content: Joi.string().required().min(5),
    reply_to_comment_id: Joi.number().integer().positive().allow(null),
    created_at: Joi.date().required(),
    updated_at: Joi.date().required(),
    replies: Joi.array().items(
        // @ts-ignore a wrong version of types for Joi
        Joi.link('#comment'),
    ).allow(null),
})
    // @ts-ignore a wrong version of types for Joi
    .id('comment');


export default Joi.object({
    comments: Joi.array().items(commentSchema),
}).required();
