import * as Joi from '@hapi/joi';

export default Joi.object({
    day: Joi.number().positive().required(),
    group_id: Joi.number().positive().optional(),
}).required();
