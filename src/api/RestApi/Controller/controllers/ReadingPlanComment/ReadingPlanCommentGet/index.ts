import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import ReadingPlanComment from '../../../../../../entities/ReadingPlanComment';
import { readingPlanCommentPresenter } from '../../../presenters/ReadingPlanComment';

type Params = Joi.extractType<typeof inputJoiSchema>;
type Return = Joi.extractType<typeof outputJoiSchema>;

const handler = async (params: Params): Promise<Return> => {
    const allComments = await ReadingPlanComment.find({
        where: { ...params },
        relations: ['author', 'author.userProfile'],
    });
    const topLevelComments = allComments.filter(comment => !comment.reply_to_comment_id);
    return {
        comments: topLevelComments.map(comment =>
            readingPlanCommentPresenter(comment, allComments),
        ),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
