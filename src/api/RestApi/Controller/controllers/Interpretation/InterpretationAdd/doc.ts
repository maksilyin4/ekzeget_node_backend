/**
 * @apiIgnore Not finished Method
 * @api {post} /interpretation/add/                      Добавить толкование
 * @apiVersion 0.1.0
 * @apiName add
 * @apiGroup Interpretation
 *
 *
 * @apiDescription Список толкований в которых упоминается стих
 * @apiPermission Authorized
 *
 * @apiParam {Array[]} verse                             Массив ID толкований
 * @apiParam {Integer}  ekzeget_id                        ID толкователя
 * @apiParam {String}   text                              ID толкователя
 *
 * @apiSuccess {String}   status                        Статус
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (422) {ApiException} error Ошибка валидации
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 *
 */
