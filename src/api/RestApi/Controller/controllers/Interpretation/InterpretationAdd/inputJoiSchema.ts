import * as Joi from '@hapi/joi';

export default Joi.object({
    verse: Joi.array()
        .items({})
        .description('Массив ID толкований'),
    ekzeget_id: Joi.number().description('ID толкователя'),
    text: Joi.string().description('ID толкователя'),
}).required();
