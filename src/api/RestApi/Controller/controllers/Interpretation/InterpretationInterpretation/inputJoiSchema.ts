import * as Joi from '@hapi/joi';
import pagination from '../../../../../../const/pagination';

export default Joi.object({
    verse_id: Joi.string()
        .description('ID стиха, по которому необходимо получить толкования (опционально)')
        .optional(),
    book_code: Joi.string()
        .description('Код книги (опционально)')
        .optional(),
    chapter: Joi.string()
        .description('Номер главы (опционально)')
        .optional(),
    number: Joi.string()
        .description('Номер стиха (опционально)')
        .optional(),
    interpretator_id: Joi.string()
        .description('ID толкователя (опционально)')
        .optional(),
    sort: Joi.string()
        .description(
            'Сортировка толкований по дате, используется только с параметрами page (опционально)'
        )
        .optional(),
    page: Joi.string()
        .default(pagination.defaultPage)
        .description('Номер страницы (опционально, по умолчанию 1)')
        .optional(),
    'per-page': Joi.string()
        .default(pagination.defaultPerPage)
        .description('Количество элементов на странице (опционально, по умолчанию 20)')
        .optional(),
}).required();
