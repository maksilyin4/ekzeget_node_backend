/**
 * @api {get} /interpretation/:book_code/?verse_id=:verse_id&chapter=:chapter&number=:number&interpretator_id=:interpretator_id&sort=:sort&page=:page&per-page=:per-page Список толкований
 * @apiVersion 0.1.0
 * @apiName interpretation
 * @apiGroup Interpretation
 *
 *
 * @apiDescription Список толкований
 * @apiPermission All
 *
 * @apiParam {Integer}  verse_id                        ID стиха, по которому необходимо получить толкования (опционально)
 * @apiParam {String}   book_code                       Код книги (опционально)
 * @apiParam {Integer}  chapter                         Номер главы (опционально)
 * @apiParam {Integer}  number                          Номер стиха (опционально)
 * @apiParam {Integer}  interpretator_id                ID толкователя (опционально)
 * @apiParam {Integer}  sort                            Сортировка толкований по дате, используется только с параметрами page (опционально)
 * @apiParam {Integer}  page                            Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page                        Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} interpretation                Список
 * @apiSuccess {Integer}  interpretation.id             ID
 * @apiSuccess {String}   interpretation.comment        Текст толкования
 * @apiSuccess {Object}   interpretation.added_by       Пользователь, добавивший толкование
 * @apiSuccess {Object}   interpretation.edited_by      Пользователь, изменивший толкование
 * @apiSuccess {Integer}  interpretation.added_at       Дата добавления (unixtimestamp)
 * @apiSuccess {Integer}  interpretation.edited_at      Дата редактирования (unixtimestamp)
 * @apiSuccess {Integer}  interpretation.investigated   Исследование (0/1)
 * @apiSuccess {Object[]}  interpretation.verse         Массив стихов, к которым относится толкование
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (422) {ApiException} error Ошибка валидации
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 *
 */
