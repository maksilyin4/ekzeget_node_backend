import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import pagination from '../../../../../../const/pagination';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import { extractNumber } from '../../../../../../libs/helpers/urlParsers';
import { interpretationInterpretationPresenter as presenter } from '../../../presenters/Interpretation/InterpretationInterpretation';
import Interpretation from '../../../../../../entities/Interpretation';
import EkzegetPerson from '../../../../../../entities/EkzegetPerson';
import CacheManager from '../../../../../../managers/CacheManager';
import { CacheManagerTTL } from '../../../../../../managers/CacheManager/enum';

const isNumeric = code => !isNaN(code);

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const perPage = +validatedParams['per-page'] || pagination.defaultPerPage;
    const page = +validatedParams['page'] || pagination.defaultPage;
    const sort = +validatedParams['sort'] || 0;

    const { verse_id, book_code, chapter, number } = validatedParams;

    const ekzeget_id = await EkzegetPerson.getInterpreterId(validatedParams.interpretator_id);

    const params = {
        book_code,
        chapter: extractNumber(chapter),
        number: extractNumber(number),
        ekzeget_id,
        verse_id: isNumeric(verse_id) ? Number(verse_id) : 0,

        perPage,
        page,
        sort,
    };
    const cacheKey = `getInterpretationVersesAndCount-${JSON.stringify(params)}`;
    const cache = await CacheManager.get(cacheKey);

    let interpretations: Interpretation[], presentedInterpretations, totalCount: number;
    if (cache) {
        [presentedInterpretations, totalCount] = cache;
    } else {
        [interpretations, totalCount] = await Interpretation.getInterpretationVersesAndCount(
            params
        );
        presentedInterpretations = await Promise.all(
            interpretations.map(async iv => presenter(iv))
        );
        await CacheManager.set(
            cacheKey,
            [presentedInterpretations, totalCount],
            CacheManagerTTL.day
        );
    }

    return {
        interpretations: presentedInterpretations,
        pages: returnPaginationObject({
            page,
            totalCount,
            'per-page': perPage,
        }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
