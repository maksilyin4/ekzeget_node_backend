import * as Joi from '@hapi/joi';

const verseScheme = Joi.object({
    id: Joi.number().required(),
    number: Joi.number().required(),
    book_id: Joi.number().required(),
    text: Joi.string().required(),
    chapter: Joi.number().required(),
    chapter_id: Joi.number().required(),
    book_code: Joi.string().required(),
    short: Joi.string().required(),
    book: Joi.object({
        id: Joi.number().required(),
        testament_id: Joi.number().required(),
        title: Joi.string().required(),
        short_title: Joi.string().required(),
        parts: Joi.number().required(),
        code: Joi.string().required(),
        legacy_code: Joi.string().required(),
        menu: Joi.string().required(),
        author: Joi.string().required(),
        year: Joi.string()
            .allow('')
            .required(),
        place: Joi.string()
            .required()
            .allow(''),
        ext_id: Joi.number().required(),
        gospel: Joi.number().required(),
        sort: Joi.number().required(),
    }).required(),
    dictionaries: Joi.array()
        .items(
            Joi.object({
                id: Joi.number().required(),
                type_id: Joi.number().required(),
                word: Joi.string().required(),
                letter: Joi.string().required(),
                description: Joi.string()
                    .required()
                    .allow(''),
                variant: Joi.string()
                    .required()
                    .allow(''),
                active: Joi.number().required(),
                code: Joi.string().required(),
            })
        )
        .required(),
    bible_maps_points: Joi.array()
        .items(Joi.any()) // TODO Прописать реальный тип
        .required(),
});

export default Joi.object({
    interpretations: Joi.array()
        .items(
            Joi.object({
                id: Joi.number().description('ID'),
                ekzeget: Joi.object().description('толкователь'),
                comment: Joi.string()
                    .allow(null, '')
                    .description('Текст толкования'),
                added_by: Joi.object()
                    .allow(null)
                    .description('Пользователь, добавивший толкование'),
                edited_by: Joi.object()
                    .allow(null)
                    .description('Пользователь, изменивший толкование'),
                added_at: Joi.number()
                    .allow(null)
                    .description('Дата добавления (unix timestamp)'),
                edited_at: Joi.number()
                    .allow(null)
                    .description('Дата редактирования (unix timestamp)'),
                investigated: Joi.number().description('Исследование (0/1)'),
                verse: Joi.array()
                    .items(verseScheme)
                    .description('Массив стихов, к которым относится толкование'),
            })
        )
        .description('Список'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    })
        .unknown()
        .description('Информация о возможной постраничной навигации'),
}).required();
