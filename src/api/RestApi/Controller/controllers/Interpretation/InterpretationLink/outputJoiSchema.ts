import * as Joi from '@hapi/joi';

export default Joi.object({
    interpretation: Joi.array()
        .items({
            id: Joi.number(),
            ekzeget: Joi.object({
                id: Joi.number()
                    .allow(null)
                    .description('ID толкователя'),
                name: Joi.string()
                    .allow(null)
                    .description('Имя толкователя'),
            }),
            verses: Joi.array()
                .items({
                    id: Joi.number().description('ID стиха'),
                    short: Joi.string().description('Аббривиатура'),
                    url: Joi.string().description(
                        'URL вида /:book_code/:verse_chapter/:verse_number/:interptetator_id/'
                    ),
                })
                .description('Стихи, на которое составлено толкование'),
        })
        .description('Список'),
}).required();
