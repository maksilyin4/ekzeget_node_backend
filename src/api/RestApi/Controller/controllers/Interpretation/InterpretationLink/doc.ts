/**
 * @api {get} /interpretation/link/:verse_id/          Упоминания
 * @apiVersion 0.1.0
 * @apiName link
 * @apiGroup Interpretation
 *
 *
 * @apiDescription Список толкований в которых упоминается стих
 * @apiPermission All
 *
 * @apiParam {Integer}  verse_id                        ID стиха
 *
 * @apiSuccess {Object[]} interpretation                Список
 * @apiSuccess {String}   interpretation.ekzeget        Имя толкователя
 * @apiSuccess {Object[]} interpretation.verses         Стихи, на которое составлено толкование
 * @apiSuccess {Integer}  interpretation.verses.id      ID стиха
 * @apiSuccess {String}   interpretation.verses.short   Аббривиатура
 * @apiSuccess {String}   interpretation.verses.url     URL вида /:book_code/:verse_chapter/:verse_number/:interptetator_id/
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (422) {ApiException} error Ошибка валидации
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 *
 */
