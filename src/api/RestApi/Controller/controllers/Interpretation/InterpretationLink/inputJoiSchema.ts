import * as Joi from '@hapi/joi';

export default Joi.object({
    verse_id: Joi.number().description('ID стиха'),
}).required();
