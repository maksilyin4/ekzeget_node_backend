import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import InterpretationVerseLink from '../../../../../../entities/InterpretationVerseLink';
import { groupBy } from '../../../../../../libs/helpers/groupBy';
import { getVerseShortCode } from '../../../presenters/Verse/getVerseShortCode';
import makeArrayUnique from '../../../../../../libs/helpers/makeArrayUnique';
import { sortByKeyASC } from '../../../../../../libs/helpers/sort';

interface RawDbResponse {
    interpretation_id: number;
    verse_id: number;
    ekzeget_id: number;
    ekzeget_name: string;
    ekzeget_code: string;
    verse_chapter: number;
    verse_number: number;
    book_short_title: string;
    book_code: string;
    sort: number;
}
const getInterpretationsData = async (verseId: number): Promise<RawDbResponse[]> =>
    InterpretationVerseLink.createQueryBuilder('ivl')
        .select([
            'ivl.interpretation_id as interpretation_id',
            'iv.verse_id as verse_id',
            'i.ekzeget_id as ekzeget_id',
            'ep.name as ekzeget_name',
            'ep.code as ekzeget_code',
            'v.chapter as verse_chapter',
            'v.number as verse_number',
            'b.short_title as book_short_title',
            'b.code as book_code',
            'b.sort as sort',
        ])
        .leftJoin('interpretation_verse', 'iv', 'ivl.interpretation_id = iv.interpretation_id')
        .leftJoin('interpretation', 'i', 'i.id = ivl.interpretation_id')
        .leftJoin('ekzeget_person', 'ep', 'i.ekzeget_id = ep.id')
        .leftJoin('verse', 'v', 'iv.verse_id = v.id')
        .leftJoin('book', 'b', 'v.book_id = b.id')
        .where('ivl.verse_id = :verseId', { verseId })
        .orderBy('b.sort', 'ASC')
        .getRawMany();

const generateInterpretationUrl = ({
    book_code,
    ekzeget_code,
    verse_chapter,
    verse_number,
}: RawDbResponse): string =>
    `/interpretation/${book_code}/glava-${verse_chapter}/stih-${verse_number}/${ekzeget_code}/`;

// TODO: Итоговая сортировка немного отличается от той что на prod,
//  т.к. не удалось понять по какому принципу сортирует prod
const presenter = (rawData: RawDbResponse[]): any => {
    const order = rawData.map(v => v.interpretation_id).filter(makeArrayUnique);
    const grouped = groupBy<RawDbResponse>(rawData, 'interpretation_id');

    const interpretation = order.map((iId: number) => {
        const data: RawDbResponse[] = grouped[iId];
        return {
            id: data[0].interpretation_id,
            ekzeget: {
                id: data[0].ekzeget_id,
                name: data[0].ekzeget_name,
            },
            verses: data
                .filter(i => Boolean(i.verse_id))
                .map(i => ({
                    id: i.verse_id,
                    short: getVerseShortCode(
                        { short_title: i.book_short_title },
                        { chapter: i.verse_chapter, number: i.verse_number }
                    ),
                    url: generateInterpretationUrl(i),
                }))
                .sort((a, b) => sortByKeyASC(a, b, 'id')),
        };
    });

    return {
        interpretation,
    };
};

const handler = async ({
    verse_id,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const rawData = await getInterpretationsData(verse_id);
    return presenter(rawData);
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
