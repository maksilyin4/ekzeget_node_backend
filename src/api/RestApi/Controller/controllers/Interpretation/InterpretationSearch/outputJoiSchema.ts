import * as Joi from '@hapi/joi';

export default Joi.object({
    interpretation: Joi.array()
        .items({})
        .description('Толкования'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    }).description('Информация о возможной постраничной навигации'),
}).required();
