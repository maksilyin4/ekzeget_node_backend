import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import TextBlock from '../../../../../../entities/TextBlock';
import CacheManager from '../../../../../../managers/CacheManager';
import { CacheManagerTTL } from '../../../../../../managers/CacheManager/enum';

type textBlock = TextBlock;

const handler = async ({
    code,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const cacheKey = `TextBlock-${code}`;
    const cache = await CacheManager.get(cacheKey);
    if (cache) return cache;

    const text: textBlock =
        (await TextBlock.findOne({
            select: ['id', 'title', 'text', 'code'],
            where: {
                code: code,
                active: true,
            },
        })) || null;

    if (text) {
        await CacheManager.set(cacheKey, { text }, CacheManagerTTL.day);
    }

    return { text };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
