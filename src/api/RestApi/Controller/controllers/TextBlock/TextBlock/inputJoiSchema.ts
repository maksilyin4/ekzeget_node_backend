import * as Joi from '@hapi/joi';

export default Joi.object({ code: Joi.string() })
    .options({ allowUnknown: true })
    .required();
