/**
 * @param string $code
 * @param string $section
 * @return array
 * @api {get} /meta/get-by-code/:code/?section=:section    Мета-тег по коду
 * @apiVersion 0.1.0
 * @apiName meta
 * @apiGroup Meta
 *
 *
 * @apiDescription Мета-тег по коду
 * @apiPermission All
 *
 * @apiSuccess {Object}   meta                    Мета-тег
 * @apiSuccess {Integer}  meta.id                 ID
 * @apiSuccess {String}   meta.category           Категория
 * @apiSuccess {String}   meta.code               Код
 * @apiSuccess {String}   meta.title              Title
 * @apiSuccess {String}   meta.description        Description
 * @apiSuccess {String}   meta.h_one              H1
 * @apiSuccess {Integer}  meta.created_at         Дата создания
 * @apiSuccess {Integer}  meta.updated_at         Дата обновления
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 *
 */
