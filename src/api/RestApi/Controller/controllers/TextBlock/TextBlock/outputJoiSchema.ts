import * as Joi from '@hapi/joi';

export default Joi.object({
    text: Joi.object({
        id: Joi.number().description('ID'),
        title: Joi.string()
            .allow('', null)
            .description('Title'),
        text: Joi.string()
            .allow('', null)
            .description('Text'),
        code: Joi.string()
            .allow('', null)
            .description('Code'),
    })
        .allow(null)
        .unknown(true)
        .description('Текстовый блок'),
}).required();
