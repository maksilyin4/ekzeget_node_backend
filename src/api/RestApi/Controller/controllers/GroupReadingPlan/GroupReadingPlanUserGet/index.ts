import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import { userPresenter } from '../../../presenters/User';
import { unserialize } from 'php-serialize';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import { StatisticsService } from '../../../../../../services/userServices/StatisticsService';

const handler = async (
    _: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userId = typeof user === 'number'
        ? user
        : user?.id;

    if (!userId)
        throw new ServerError(errorStrings.error401, 401);

    const userPlan = await UserReadingPlan.findOne({
        where: { user_id: userId},
        relations: [
            'group',
            'plan',
            'group.users',
            'group.users.userProfile',
            'group.users.userReadingPlans',
        ],
    });

    if (!userPlan?.group)
        return { group: {} };

    const group = userPlan.group;
    return {
        group: {
            ...group,
            schedule: unserialize(group.schedule),
            users: await Promise.all(
                group.users.map(async user => {
                    const dto: any = userPresenter(user);
                    dto.statistics = await StatisticsService.getStatistics(
                        user.userReadingPlans[0]
                    );
                    return dto;
                }),
            ),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
