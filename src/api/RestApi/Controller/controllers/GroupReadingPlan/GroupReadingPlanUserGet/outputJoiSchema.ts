import * as Joi from '@hapi/joi';

export default Joi.object({
    status: Joi.string().description('ok'),
    group: Joi.object({
        id: Joi.number(),
        mentor_id: Joi.number(),
        plan_id: Joi.number(),
        plan: Joi.object({}).unknown(),
        start_date: Joi.string(),
        stop_date: Joi.string(),
        subscribe: Joi.number(),
        schedule: Joi.object({
            Mon: Joi.number().valid(0, 1),
            Tue: Joi.number().valid(0, 1),
            Wed: Joi.number().valid(0, 1),
            Thu: Joi.number().valid(0, 1),
            Fri: Joi.number().valid(0, 1),
            Sat: Joi.number().valid(0, 1),
            Sun: Joi.number().valid(0, 1),
        }),
        check_date: Joi.string(),
        users: Joi.array().items(Joi.object().unknown()),
    })
        .unknown()
        .description('Группа, которой руководит пользователь'),
}).required();
