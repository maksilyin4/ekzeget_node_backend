import * as Joi from '@hapi/joi';

export default Joi.object({
    group_id: Joi.number()
        .positive()
        .required(),
    subscribe: Joi.boolean().allow(0, 1, '0', '1'),
}).required();
