import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import GroupReadingPlan from '../../../../../../entities/GroupReadingPlan';
import { userPresenter } from '../../../presenters/User';
import { unserialize } from 'php-serialize';

const handler = async (
    { group_id }: Joi.extractType<typeof inputJoiSchema>,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const group = await GroupReadingPlan.findOne(
        group_id,
        { relations: ['plan', 'users', 'users.userProfile'] },
    );
    return {
        group: {
            ...group,
            schedule: unserialize(group.schedule),
            users: group.users.map(userPresenter),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
