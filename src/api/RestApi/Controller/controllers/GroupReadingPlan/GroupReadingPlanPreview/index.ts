import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import GroupReadingPlan from '../../../../../../entities/GroupReadingPlan';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import { userPresenter } from '../../../presenters/User';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import { unserialize } from 'php-serialize';
import { StatisticsService } from '../../../../../../services/userServices/StatisticsService';

const handler = async (
    { group_id }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userId = typeof user === 'number'
        ? user
        : user?.id;
    if (!userId)
        throw new ServerError(errorStrings.error401, 401);

    const alreadyHasPlan = await UserReadingPlan.findOne({ where: { user_id: userId } });
    if (alreadyHasPlan)
        throw new ServerError('You already have a reading plan.', 400);

    const group = await GroupReadingPlan.findOne(
        group_id,
        { relations: ['plan', 'userPlans', 'userPlans.user', 'userPlans.user.userProfile'] },
    );
    if (!group)
        throw new ServerError('Group not found', 404);

    const userPlans = group.userPlans;
    delete group.userPlans;

    return {
        group: {
            ...group,
            schedule: unserialize(group.schedule),
            users: await Promise.all(
                userPlans.map(async userPlan => {
                    const dto: any = userPresenter(userPlan.user);
                    dto.statistics = await StatisticsService.getStatistics(userPlan);
                    return dto;
                }),
            ),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
