import * as Joi from '@hapi/joi';

export default Joi.object({
    group_id: Joi.number()
        .positive()
        .required(),
}).required();
