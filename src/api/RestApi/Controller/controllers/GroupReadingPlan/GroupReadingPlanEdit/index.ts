import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import GroupReadingPlan from '../../../../../../entities/GroupReadingPlan';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import { serialize, unserialize } from 'php-serialize';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';

const handler = async (
    data: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {

    if (!user)
        throw new ServerError(errorStrings.error401, 401);

    let toEdit: GroupReadingPlan;
    if (data.group_id) {
        toEdit = await GroupReadingPlan.findOne(data.group_id, { relations: ['userPlans'] });
    } else {
        const userPlan = await UserReadingPlan.findOne({
            where: { user_id: user.id },
            relations: ['group', 'group.userPlans'],
        });
        toEdit = userPlan.group;
    }
    if (!toEdit)
        throw new ServerError(`Group with id=${data.group_id} not found`, 404);
    if (toEdit.mentor_id !== user.id)
        throw new ServerError(`You're not a mentor of this group.`, 403);

    delete data['group_id'];
    // @ts-ignore
    data.schedule = serialize(data.schedule);
    Object.assign(toEdit, data);
    const edited = await toEdit.save();

    for (const userPlan of toEdit.userPlans) {
        Object.assign(userPlan, data);
        await userPlan.save();
    }

    return {
        group: {
            ...edited,
            schedule: unserialize(edited.schedule),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
