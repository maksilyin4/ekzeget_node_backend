import * as Joi from '@hapi/joi';

export default Joi.object({
    group_id: Joi.number().optional(),
    start_date: Joi.string().optional(),
    stop_date: Joi.string().optional(),
    check_date: Joi.string().optional(),
    schedule: Joi.object({
        Mon: Joi.number().valid(0, 1).required(),
        Tue: Joi.number().valid(0, 1).required(),
        Wed: Joi.number().valid(0, 1).required(),
        Thu: Joi.number().valid(0, 1).required(),
        Fri: Joi.number().valid(0, 1).required(),
        Sat: Joi.number().valid(0, 1).required(),
        Sun: Joi.number().valid(0, 1).required(),
    }).optional(),
}).required();
