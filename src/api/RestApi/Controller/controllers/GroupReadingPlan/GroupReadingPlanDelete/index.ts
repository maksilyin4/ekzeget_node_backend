import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import GroupReadingPlan from '../../../../../../entities/GroupReadingPlan';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import ReadingArchive from '../../../../../../entities/ReadingArchive';
import { userPresenter } from '../../../presenters/User';

const handler = async (
    _: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {

    if (!user)
        throw new ServerError(errorStrings.error401, 401);

    const group = await GroupReadingPlan.findOne({
        where: { mentor_id: user.id },
        relations: ['userPlans'],
    });
    if (!group)
        throw new ServerError(`You don't have any mentored groups.`, 404);

    let mentorUrp = null;
    for (const urp of group.userPlans) {
        if (urp.user_id === user.id) {
            // do not remove yet to not change initial member count
            // since mentor's user-plan is not archived, if it's removed
            // it won't be included in final member_count
            mentorUrp = urp;
        } else {
            // archive snapshot of current statistics
            await ReadingArchive.fromUserReadingPlan(urp);
            // let them finish their plans
            urp.group_id = null;
            await urp.save();
        }
    }

    const archived = await ReadingArchive.fromGroup(group);
    await mentorUrp?.remove();
    await group.remove();

    return {
        archived: {
            ...archived,
            user: userPresenter(archived.user),
            mentor: userPresenter(archived.mentor),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
