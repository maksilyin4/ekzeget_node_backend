import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import GroupReadingPlan from '../../../../../../entities/GroupReadingPlan';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import { unserialize } from 'php-serialize';
import { userPresenter } from '../../../presenters/User';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import ReadingArchive from '../../../../../../entities/ReadingArchive';

const handler = async (
    { member_id }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {

    if (!user)
        throw new ServerError(errorStrings.error401, 401);

    let memberToRemove: UserReadingPlan;
    let group = await GroupReadingPlan.findOne({
        where: { mentor_id: user.id },
        relations: ['userPlans'],
    });

    if (group) {
        if (!member_id)
            throw new ServerError('member_id query param not provided', 400);
        memberToRemove = group.userPlans.find(plan => plan.user_id === Number(member_id));
        if (!memberToRemove)
            throw new ServerError(`This group doesn't contain member with id=${member_id}`, 404);
    } else {
        memberToRemove = await UserReadingPlan.findOne({ where: { user_id: user.id } });
        if (!memberToRemove?.group_id)
            throw new ServerError(`You don't belong to any group.`, 400);
    }
    const groupId = memberToRemove.group_id;

    await ReadingArchive.fromUserReadingPlan(memberToRemove);

    memberToRemove.group_id = null;
    await memberToRemove.save();

    group = await GroupReadingPlan.findOne(groupId, { relations: ['users'] });
    return {
        status: 'ok',
        group: {
            ...group,
            schedule: unserialize(group.schedule),
            users: group.users.map(userPresenter),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
