import * as Joi from '@hapi/joi';

export default Joi.object({
    member_id: Joi.number().integer().positive().optional(),
}).required();
