import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import GroupReadingPlan from '../../../../../../entities/GroupReadingPlan';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import { serialize, unserialize } from 'php-serialize';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';

const handler = async (
    data: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {

    if (!user)
        throw new ServerError(errorStrings.error401, 401);

    const alreadyMentors = await GroupReadingPlan.findOne({
        where: { mentor_id: user.id },
    });
    if (alreadyMentors)
        throw new ServerError('You already have a group.', 400);

    const alreadyReads = await UserReadingPlan.findOne({
        where: { user_id: user.id },
    });
    if (alreadyReads)
        throw new ServerError('You already have a reading plan', 400);

    const created = await GroupReadingPlan.create({
        ...data,
        schedule: serialize(data.schedule),
        mentor_id: user.id,
    }).save();

    const userPlan = await UserReadingPlan.create({
        ...data,
        ...created,
        subscribe: data.subscribe ? Number(data.subscribe) : 0,
        user_id: user.id,
        group_id: created.id,
    }).save();

    return {
        group: {
            ...created,
            schedule: unserialize(created.schedule),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
