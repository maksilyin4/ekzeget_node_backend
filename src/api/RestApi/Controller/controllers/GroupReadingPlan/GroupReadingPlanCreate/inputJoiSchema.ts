import * as Joi from '@hapi/joi';

export default Joi.object({
    plan_id: Joi.number().required(),
    start_date: Joi.string().required(),
    stop_date: Joi.string().required(),
    check_date: Joi.string().required(),
    schedule: Joi.object({
        Mon: Joi.number().valid(0, 1).required(),
        Tue: Joi.number().valid(0, 1).required(),
        Wed: Joi.number().valid(0, 1).required(),
        Thu: Joi.number().valid(0, 1).required(),
        Fri: Joi.number().valid(0, 1).required(),
        Sat: Joi.number().valid(0, 1).required(),
        Sun: Joi.number().valid(0, 1).required(),
    }).required(),
    subscribe: Joi.number().valid(0, 1).allow(true, false).optional(),
    translate_id: Joi.number().min(0).optional(),
}).required();
