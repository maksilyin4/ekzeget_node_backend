import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import GroupReadingPlan from '../../../../../../entities/GroupReadingPlan';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import { userPresenter } from '../../../presenters/User';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import { serialize, unserialize } from 'php-serialize';

const handler = async (
    { group_id, subscribe = false }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userId = typeof user === 'number'
        ? user
        : user?.id;
    if (!userId)
        throw new ServerError(errorStrings.error401, 401);

    const alreadyHasPlan = await UserReadingPlan.findOne({ where: { user_id: userId } });
    if (alreadyHasPlan)
        throw new ServerError('You already have a reading plan.', 400);

    let group = await GroupReadingPlan.findOne(group_id);
    if (!group)
        throw new ServerError('Group not found', 404);

    const urp = UserReadingPlan.create({
        ...group,
        subscribe: Number(subscribe),
        user_id: userId,
        group_id: group.id,
    });
    delete urp.id;
    await urp.save();

    group = await GroupReadingPlan.findOne(
        group_id,
        { relations: ['plan', 'users', 'users.userProfile'] },
    );
    return {
        group: {
            ...group,
            schedule: unserialize(group.schedule),
            users: group.users.map(userPresenter),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
