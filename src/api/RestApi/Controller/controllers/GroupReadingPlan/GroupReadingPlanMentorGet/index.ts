import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import GroupReadingPlan from '../../../../../../entities/GroupReadingPlan';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import { unserialize } from 'php-serialize';
import { userPresenter } from '../../../presenters/User';
import { StatisticsService } from '../../../../../../services/userServices/StatisticsService';

const handler = async (
    _: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {

    if (!user)
        throw new ServerError(errorStrings.error401, 401);

    const group = await GroupReadingPlan.findOne({
        where: { mentor_id: user.id },
        order: { id: 'DESC' },
        relations: [
            'plan',
            'userPlans',
            'userPlans.user',
            'userPlans.user.userProfile',
        ],
    });

    if (!group) return { group: {} };

    const groupDto = {
        ...group,
        userPlans: undefined,
        schedule: unserialize(group.schedule),
        users: await Promise.all(
            group.userPlans.map(async userPlan => {
                const dto: any = userPresenter(userPlan.user);
                dto.statistics = await StatisticsService.getStatistics(userPlan);
                return dto;
            }),
        ),
    };
    delete groupDto.userPlans;
    return { group: groupDto };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
