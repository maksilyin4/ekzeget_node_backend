/**
     * @api {get} /motivator/:verse_id                     Список мотиваторов стиха
     * @apiVersion 0.1.0
     * @apiName motivator
     * @apiGroup Motivator
     *
     *
     * @apiDescription Список записей
     * @apiPermission All
     *
     * @apiSuccess {Object[]} motivators                    Список мотиваторов стиха
     * @apiSuccess {Integer}  motivators.id                 ID
     * @apiSuccess {Integer}  motivators.status             Статус 1 - активна, 0 - нет
     * @apiSuccess {Integer}  motivators.created_at         время создание
     * @apiSuccess {Integer}  motivators.updated_at         время обновленения
     *
     * @apiSuccess {Object[]} motivators.images[]            Изображения. Формат: Тип изображения => Данные изображения
     *
     * @apiSuccess {Integer}  motivators.images.id             ID
     * @apiSuccess {Integer}  motivators.images.motivator_id         ID Мотиватора
     * @apiSuccess {String}  motivators.images.path            Путь к картинке
     * @apiSuccess {Integer}  motivators.images.width          Ширина
     * @apiSuccess {Integer}  motivators.images.height         Высота
     * @apiSuccess {String}  motivators.images.type                Тип  [
                                                                        'original' => 'Оригинальная',
                                                                        'preview' => 'Превью',
                                                                        'small' => 'Маленькая',
                                                                        'big' => 'Большая',
                                                                 ]
     * @apiSuccess {String}  motivator.images.created_at         время создание
     * @apiSuccess {String}  motivator.images.updated_at         время обновленения
     *
     *
     * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
     * @apiSuccess {Integer}  pages.currentPage             Текущая страница
     * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
     * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
     *
     * @apiError (400) {ApiException} error Авторизация не прошла
     * @apiError (403) {ApiException} error Доступ запрещен
     * @apiError (500) {ApiException} error Внутренняя ошибка сервера
     */
