import * as Joi from '@hapi/joi';

export default Joi.object({
    motivators: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            status: Joi.number().description('Статус 1 - активна, 0 - нет'),
            created_at: Joi.number().description('время создание'),
            updated_at: Joi.number().description('время обновленения'),
            images: Joi.array()
                .items({
                    id: Joi.number().description('ID'),
                    motivator_id: Joi.number().description('ID Мотиватора'),
                    path: Joi.string().description('Путь к картинке'),
                    width: Joi.number().description('Ширина'),
                    height: Joi.number().description('Высота'),
                    type: Joi.string().description(
                        "Тип [ 'original' => 'Оригинальная', 'preview' => 'Превью', 'small' => 'Маленькая', 'big' => 'Большая', ]"
                    ),
                    created_at: Joi.string().description('время создание'),
                    updated_at: Joi.string().description('время обновленения'),
                })
                .description('Список мотиваторов стиха'),
        })
        .description('Изображения. Формат: Тип изображения => Данные изображения'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    }).description('Информация о возможной постраничной навигации'),
}).required();
