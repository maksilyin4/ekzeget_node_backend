import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Article from '../../../../../../entities/Article';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import { paginationParamsWrapper } from '../../../../../../libs/helpers/pagination';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { perPage } = paginationParamsWrapper(validatedParams);
    const [articles, totalCount] = await Article.findAndCount({
        where: { category_id: validatedParams.category_id },
        take: perPage,
        order: { created_at: 'DESC' },
        relations: ['image_file'],
    });

    return {
        article: articles,
        pages: returnPaginationObject({ ...validatedParams, totalCount }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
