import * as Joi from '@hapi/joi';

export default Joi.object({
    category_id: Joi.number().description('ID категории'),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
    img_sizes: Joi.array().items(
        Joi.string().description('строка с названием категории размера картинки')
    ),
}).required();
