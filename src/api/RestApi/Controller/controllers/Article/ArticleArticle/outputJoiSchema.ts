import * as Joi from '@hapi/joi';

export default Joi.object({
    article: Joi.array()
        .items(
            Joi.object({
                id: Joi.number().description('ID'),
                slug: Joi.string().description('Алиас записи'),
                title: Joi.string().description('Название записи'),
                body: Joi.string().description('Текст'),
                category_id: Joi.number().description('ID категории'),
                status: Joi.number().description('Статус 1 - активна, 0 - нет'),
                created_at: Joi.number().description('время создание'),
                updated_at: Joi.number().description('время обновления'),
                published_at: Joi.number().description('время публикации'),
                image: Joi.string()
                    .allow(null)
                    .description('url картинки'),
            }).unknown()
        )
        .description('Список'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    })
        .unknown()
        .description('Информация о возможной постраничной навигации'),
}).required();
