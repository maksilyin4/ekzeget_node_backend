/**
 * @api {get} /article/:category_id                     Список записей
 * @apiVersion 0.1.0
 * @apiName article
 * @apiGroup Article
 *
 *
 * @apiDescription Список записей
 * @apiPermission All
 *
 * @apiParam {Integer}  category_id                   ID категории
 * @apiParam {Integer}  page                            Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page                        Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} article                       Список
 * @apiSuccess {Integer}  article.id                    ID
 * @apiSuccess {String}   article.slug                  Алиас записи
 * @apiSuccess {String}   article.title                 Название записи
 * @apiSuccess {String}   article.body                  Текст
 * @apiSuccess {Integer}  article.category_id           ID категории
 * @apiSuccess {String}   article.status                Статус 1 - активна, 0 - нет
 * @apiSuccess {Integer}  article.created_at            время создание
 * @apiSuccess {Integer}  article.updated_at            время обновления
 * @apiSuccess {Integer}  article.published_at          время публикации
 * @apiSuccess {String}   article.image        url картинки
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
