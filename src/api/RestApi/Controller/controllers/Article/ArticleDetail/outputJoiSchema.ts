import * as Joi from '@hapi/joi';

export default Joi.object({
    article: Joi.object({
        id: Joi.number().description('ID'),
        slug: Joi.string().description('Алиас записи'),
        title: Joi.string().description('Название записи'),
        body: Joi.string().description('Текст'),
        category_id: Joi.number().description('ID категории'),
        status: Joi.number().description('Статус 1 - активна, 0 - нет'),
        created_at: Joi.number().description('время создание'),
        updated_at: Joi.number().description('время обновления'),
        published_at: Joi.number()
            .allow(null)
            .description('время публикации'),
        image: Joi.string()
            .allow(null, '')
            .description('url картинки'),
        optimized: Joi.array()
            .items({
                news_image: Joi.array()
                    .items({
                        original: Joi.string().description('url картинки'),
                        webp: Joi.string().description('url картинки'),
                    })
                    .description('Картинки'),
                news_image_mob: Joi.array()
                    .items({
                        original: Joi.string().description('url картинки'),
                        webp: Joi.string().description('url картинки'),
                    })
                    .description('Картинки мобильной версии'),
            })
            .description('Картинки Альбома'),
    })
        .unknown()
        .description('Список'),
    images: Joi.array()
        .items(
            Joi.object({
                id: Joi.number().description('ID'),
                article_id: Joi.number().description('ID статьи'),
                path: Joi.string().description('url картинки'),
                created_at: Joi.number().description('время создание'),
            }).unknown()
        )
        .description(
            'Список картинок (@todo это еще актуально? по коду это осталость только в Motivator)'
        ),
}).required();
