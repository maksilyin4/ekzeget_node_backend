import * as Joi from '@hapi/joi';

export default Joi.object({
    slug: Joi.string().description('slug статьи'),
}).required();
