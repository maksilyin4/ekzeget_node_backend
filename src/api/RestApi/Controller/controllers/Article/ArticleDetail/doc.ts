/**
 * @api {get} /article/detail/:id                       Запись
 * @apiVersion 0.1.0
 * @apiName detail
 * @apiGroup Article
 *
 *
 * @apiDescription Детали записи
 * @apiPermission All
 *
 * @apiParam {Integer}    id                           ID или алиас записи
 *
 * @apiSuccess {Object}   article                       Список
 * @apiSuccess {Integer}  article.id                    ID
 * @apiSuccess {String}   article.slug                  Алиас записи
 * @apiSuccess {String}   article.title                 Название записи
 * @apiSuccess {String}   article.body                  Текст
 * @apiSuccess {Integer}  article.category_id             ID категории
 * @apiSuccess {String}   article.status                Статус 1 - активна, 0 - нет
 * @apiSuccess {Integer}  article.created_at            время создание
 * @apiSuccess {Integer}  article.updated_at            время обновления
 * @apiSuccess {Integer}  article.published_at          время публикации
 * @apiSuccess {String}   article.image                 url картинки
 * @apiSuccess {Object[]}   article.optimized                           Картинки Альбома
 * @apiSuccess {Object[]}   article.optimized.news_image                Картинки
 * @apiSuccess {String}     article.optimized.news_image.original       url картинки
 * @apiSuccess {String}     article.optimized.news_image.webp           url картинки
 * @apiSuccess {Object[]}   article.optimized.news_image_mob            Картинки мобильной версии
 * @apiSuccess {String}     article.optimized.news_image_mob.original   url картинки
 * @apiSuccess {String}     article.optimized.news_image_mob.webp       url картинки
 *
 *
 * @apiSuccess {Object[]}   images                     Список картинок
 * @apiSuccess {Integer}   images.id                   ID
 * @apiSuccess {Integer}   images.article_id           ID статьи
 * @apiSuccess {String}   images.path                  url картинки
 * @apiSuccess {Integer}  images.created_at            время создание
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
