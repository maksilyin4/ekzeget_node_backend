import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Article from '../../../../../../entities/Article';
import ArticleAttachment from '../../../../../../entities/ArticleAttachment';

const handler = async ({
    slug,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    let article = await Article.findOne({ where: { slug } });

    if (!article) {
        article = await Article.findOne({ where: { id: slug } });
    }

    const images = await ArticleAttachment.find({ where: { article_id: article.id } });

    return { article, images };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
