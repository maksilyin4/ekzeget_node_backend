/**
 * @api {get} /article/category/    Список категорий
 * @apiVersion 0.1.0
 * @apiName category
 * @apiGroup Article
 *
 *
 * @apiDescription Список категорий
 * @apiPermission All
 *
 * @apiSuccess {Object[]} category                       Список
 * @apiSuccess {Integer}  category.id                    ID
 * @apiSuccess {String}   category.slug                  Алиас категории
 * @apiSuccess {String}   category.title                 Название категории
 * @apiSuccess {String}   category.body                  Описание категории
 * @apiSuccess {Integer}  category.parent_id             Родительская категория
 * @apiSuccess {String}   category.status                Статус 1 - активна, 0 - нет
 * @apiSuccess {Integer}  category.created_at            время создание
 * @apiSuccess {Integer}  category.updated_at            время обновления
 *
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
