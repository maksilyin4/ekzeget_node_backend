import * as Joi from '@hapi/joi';

export default Joi.object({
    category: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            slug: Joi.string().description('Алиас категории'),
            title: Joi.string().description('Название категории'),
            body: Joi.string().description('Описание категории'),
            parent_id: Joi.number().description('Родительская категория'),
            status: Joi.string().description('Статус 1 - активна, 0 - нет'),
            created_at: Joi.number().description('время создание'),
            updated_at: Joi.number().description('время обновления'),
        })
        .description('Список'),
}).required();
