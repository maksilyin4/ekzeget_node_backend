import * as Joi from '@hapi/joi';

export default Joi.object({
    path: Joi.string()
        .description('Путь'),
});
