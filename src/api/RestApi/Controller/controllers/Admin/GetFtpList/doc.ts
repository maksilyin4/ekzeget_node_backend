/**
 * @api {get} /admin/get-ftp-list/?path=:path
 * @apiVersion 0.1.0
 * @apiName admin
 * @apiGroup admin
 *
 *
 * @apiDescription Получить содержимое папки на  ftp
 * @apiPermission All
 *
 * @apiParam {Integer}  path      путь
 *
 *
 * @apiSuccess {Object[]} tree                        дерево каталогов
 * @apiSuccess {Integer}  words.title                 название
 * @apiSuccess {String}   words.key                   полный путь
 * @apiSuccess {String}   words.isLeaf                директория или файл

 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
