import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import FtpManager from '../../../../../../managers/FtpManager';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import RbacAuthAssignment from '../../../../../../entities/RbacAuthAssignment';
import User from '../../../../../../entities/User';
import { In } from 'typeorm/find-options/operator/In';

async function replaceContainerNamesWithUserNames(
    ftpList: { title: string; isLeaf: boolean; key: string }[]
) {
    const admins = await RbacAuthAssignment.createQueryBuilder('rbac')
        .select(['user.username as username', 'rbac.user_id as user_id'])
        .innerJoinAndSelect(User, 'user', 'user.id = rbac.user_id')
        .where({
            user_id: In(ftpList.map(ftpItem => ftpItem.title)),
            item_name: User.roles.administrator,
        })
        .getRawMany();
    for (const admin of admins) {
        for (const ftpItem of ftpList) {
            if (ftpItem.title === admin.user_id) {
                ftpItem.title = admin.username;
            }
        }
    }
}

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { path } = validatedParams;
    const directoryContents = await new FtpManager().getDirectoryContents(
        path === '#' ? '' : path,
        '/'
    );

    if (path === '#') {
        await replaceContainerNamesWithUserNames(directoryContents);
    }

    return { tree: directoryContents.filter(item => !item.title.startsWith('.')) };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
