import * as Joi from '@hapi/joi';

export default Joi.object({
    tree: Joi.array()
        .items({
            title: Joi.string(),
            key: Joi.string(),
            isLeaf: Joi.boolean(),
        })
        .allow(null),
}).required();
