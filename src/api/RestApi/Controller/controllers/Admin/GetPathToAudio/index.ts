import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import FtpManager from '../../../../../../managers/FtpManager';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Media from '../../../../../../entities/Media';

/**
 * Отложено до лучших времен. Роут нужен был для связи аудио и вопросов викторины.
 * @param validatedParams
 */

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { media_id } = validatedParams;
    const mediaProperties = await Media.findOne({
        select: ['properties'],
        where: {
            id: media_id,
        },
    });

    const mediaPropertiesJson: { audio_href: string } = await JSON.parse(
        mediaProperties.properties
    );
    const path = mediaPropertiesJson.audio_href ? mediaPropertiesJson.audio_href : null;

    return { path };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
