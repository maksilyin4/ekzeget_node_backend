import * as Joi from '@hapi/joi';

export default Joi.object({
    media_id: Joi.number(),
});
