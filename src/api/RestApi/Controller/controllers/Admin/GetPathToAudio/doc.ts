/**
 * @api {get} /admin/get-audio-path/?media_id=media_id
 * @apiVersion 0.1.0
 * @apiName admin
 * @apiGroup admin
 *
 *
 * @apiDescription Получить путь к аудиофайлу
 * @apiPermission All
 *
 * @apiParam {Integer}  media_id      id медиа
 *
 *
 * @apiSuccess {String}   path        путь к медиафайлу

 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
