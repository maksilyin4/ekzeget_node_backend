import * as Joi from '@hapi/joi';

export default Joi.object({
    codeOrId: Joi.alternatives()
        .try(Joi.string(), Joi.number())
        .description(
            'ID слова (старый проект реализован так что сюда могут передавать и id записи и ее код, круто да?!)'
        ),
}).required();
