import * as Joi from '@hapi/joi';

export default Joi.object({
    word: Joi.object({
        id: Joi.number().description('ID'),
        word: Joi.string().description('Слово'),
        letter: Joi.string().description('Первый символ слова'),
        variant: Joi.array().items(Joi.string().allow('', null)),
        description: Joi.string().allow('').description('Описание'),
        dictionary: Joi.object({
            id: Joi.number(),
            title: Joi.string(),
            alias: Joi.string().allow('', null),
            active: Joi.number(),
            code: Joi.string(),
        }).description('Информация о словаре'),
        code: Joi.string(),
        same_words: Joi.array()
            .items(Joi.object().description('базовые параметры те что,что в слове уровнем выше'))
            .description('Слова на туже букву в этом же словаре'),
        other_dictionaries: Joi.array()
            .items(
                Joi.object({
                    id: Joi.number().description('id слова'),
                    type_id: Joi.number().description('id словаря'),
                })
            )
            .description('Словари с этой же буквой'),
    }),
}).required();
