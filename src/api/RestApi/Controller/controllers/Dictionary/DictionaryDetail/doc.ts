/**
 * @api {get} /dictionary/detail/:id/          Описание слова
 * @apiVersion 0.1.0
 * @apiName detail
 * @apiGroup Dictionary
 *
 *
 * @apiDescription Детальное описание слова
 * @apiPermission All
 *
 * @apiParam {Integer}    id                            ID слова
 *
 *
 * @apiSuccess {Object}   word
 * @apiSuccess {Integer}  word.id                  ID
 * @apiSuccess {String}   word.word                Слово
 * @apiSuccess {String}   word.letter              Первый символ слова
 * @apiSuccess {String}   word.variant             Варианты
 * @apiSuccess {String}   word.description         Описание
 * @apiSuccess {Object}   word.dictionary          Информация о словаре
 * @apiSuccess {Array}    word.same_words[]        Слова на туже букву в этом же словаре
 * @apiSuccess {Array}    word.other_dictionaries[]  Словари с этой же буквой
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
