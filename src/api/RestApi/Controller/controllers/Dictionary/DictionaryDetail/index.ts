import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Dictionary from '../../../../../../entities/Dictionary';
import { Not } from 'typeorm';
import returnCodeOrIdConditionObject from '../../../../../../libs/helpers/returnCodeOrIdConditionObject';
import TextFormatter from '../../../../../../libs/TextFormatter';
import DictionaryType from '../../../../../../entities/DictionaryType';
import { ServerError } from '../../../../../../libs/ErrorHandler';

const handler = async ({
    codeOrId,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const dictionary = await Dictionary.findOne({
        where: returnCodeOrIdConditionObject(codeOrId),
    });

    if (!dictionary) {
        throw new ServerError(`Словарь не найден [${codeOrId}]`, 404);
    }

    const { id, word, letter, variant, description, type_id, code } = dictionary;

    const [
        otherWordsWithThisLetter,
        thisWordFromOtherDictionaries,
        dictionaryInfo,
    ] = await Promise.all([
        Dictionary.find({
            where: {
                type_id,
                letter,
            },
        }),
        Dictionary.find({
            where: {
                word,
                type_id: Not(type_id),
            },
        }),
        DictionaryType.findOne({ where: { id: type_id } }),
    ]);

    return {
        word: {
            id,
            word,
            code,
            letter,
            dictionary: dictionaryInfo,
            variant: [...variant.split(' ')],
            description: await new TextFormatter(description).format(),
            same_words: otherWordsWithThisLetter.map(
                ({ word, letter, variant, description, code }) => ({
                    word,
                    letter,
                    variant,
                    description,
                    code,
                })
            ),
            other_dictionaries: thisWordFromOtherDictionaries.map(({ id, type_id }) => ({
                id,
                type_id,
            })),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
