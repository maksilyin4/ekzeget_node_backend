import * as Joi from '@hapi/joi';

export default Joi.object({
    type_id: Joi.number().description('ID словаря'),
    letter: Joi.string().description('Буква'),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
}).required();
