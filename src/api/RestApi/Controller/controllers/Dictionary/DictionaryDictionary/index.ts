import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Dictionary from '../../../../../../entities/Dictionary';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { type_id, letter } = validatedParams;
    const [dictionaries, totalCount] = await Dictionary.findAndCount({
        where: { type_id, letter },
        order: { word: 'ASC' },
    });

    return {
        dictionary: dictionaries,
        pages: returnPaginationObject({ ...validatedParams, totalCount }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
