import * as Joi from '@hapi/joi';

export default Joi.object({
    dictionary: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            type_id: Joi.number().description('ID словаря'),
            word: Joi.string().description('Слово'),
            letter: Joi.string().description('Первый символ слова'),
            variant: Joi.string()
                .allow('')
                .description('Варианты'),
            code: Joi.string(),
            active: Joi.number(),
            description: Joi.string().allow('', null),
        })
        .description('Список слов'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    })
        .unknown()
        .description('Информация о возможной постраничной навигации'),
}).required();
