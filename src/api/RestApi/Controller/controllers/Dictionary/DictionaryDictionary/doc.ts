/**
 * @api {get} /dictionary/:type_id/:letter/?page=:page&per-page=:per-page  Список слов
 * @apiVersion 0.1.0
 * @apiName dictionary
 * @apiGroup Dictionary
 *
 *
 * @apiDescription Список слов на заданную букву в заданном словаре
 * @apiPermission All
 *
 * @apiParam {Integer}  type_id                         ID словаря
 * @apiParam {Integer}  letter                          Буква
 * @apiParam {Integer}  page                            Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page                        Количество элементов на странице (опционально, по умолчанию 20)
 *
 *
 * @apiSuccess {Object[]} dictionary                     Список слов
 * @apiSuccess {Integer}  dictionary.id                  ID
 * @apiSuccess {Integer}  dictionary.type_id             ID словаря
 * @apiSuccess {String}   dictionary.word                Слово
 * @apiSuccess {String}   dictionary.letter              Первый символ слова
 * @apiSuccess {String}   dictionary.variant             Варианты
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
