import * as Joi from '@hapi/joi';

export default Joi.object({
    dictionary: Joi.array().items(
        Joi.object({
            id: Joi.number().description('ID'),
            active: Joi.number(),
            title: Joi.string().description('Название'),
            alias: Joi.string()
                .description('Алиас')
                .allow('', null),
            code: Joi.string(),
            letters: Joi.array()
                .items(Joi.string())
                .description('Список букв, с которых начинаются слова в словаре'),
        })
    ),
}).required();
