/**
 * @api {get} /dictionary/list/                   Список словарей
 * @apiVersion 0.1.0
 * @apiName list
 * @apiGroup Dictionary
 *
 *
 * @apiDescription Детальное описание слова
 * @apiPermission All
 *
 *
 * @apiSuccess {Object[]} dictionary
 * @apiSuccess {Integer}  dictionary.id                  ID
 * @apiSuccess {String}   dictionary.title               Название
 * @apiSuccess {String}   dictionary.alias               Алиас
 * @apiSuccess {Object[]} dictionary.letters             Список букв, с которых начинаются слова в словаре
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
