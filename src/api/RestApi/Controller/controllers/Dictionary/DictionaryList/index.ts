import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import DictionaryType from '../../../../../../entities/DictionaryType';
import Dictionary from '../../../../../../entities/Dictionary';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const dictionaryTypes = await DictionaryType.find({ where: { active: 1 } });

    const clientDictionaryPromises = dictionaryTypes.map(async dt => ({
        ...dt,
        letters: (
            await Dictionary.createQueryBuilder('dictionary')
                .select(['letter'])
                .where('type_id = :type_id', { type_id: dt.id })
                .andWhere('active=1')
                .groupBy('letter')
                .orderBy('letter')
                .getRawMany()
        ).map(d => d.letter),
    }));

    const clientDictionary = await Promise.all(clientDictionaryPromises);

    return {
        dictionary: clientDictionary.map(({ id, active, title, alias, letters, code }) => ({
            id,
            active,
            title,
            alias: alias || null,
            letters,
            code,
        })),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
