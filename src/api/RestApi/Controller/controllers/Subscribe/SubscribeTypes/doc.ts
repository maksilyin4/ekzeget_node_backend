/**
 * @api {get} /subscribe/types                               Список
 * @apiVersion 0.1.0
 * @apiName  Types
 * @apiGroup Subscribe
 *
 * @apiDescription                                           Список типов подписок
 * @apiPermission All
 *
 * @apiSuccess {Object[]} subscribe_types                    Список типов подписок
 * @apiSuccess {Integer}  subscribe_types.id                 ID
 * @apiSuccess {String}   subscribe_types.name               Имя
 * @apiSuccess {String}   subscribe_types.slug               Слаг
 * @apiSuccess {Integer}  subscribe_types.created_at         Время создания
 * @apiSuccess {Integer}  subscribe_types.updated_at         Время обновления
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
