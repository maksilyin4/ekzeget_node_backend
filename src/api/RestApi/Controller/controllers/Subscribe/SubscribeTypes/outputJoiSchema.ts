import * as Joi from '@hapi/joi';

export default Joi.object({
    subscribe_types: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            name: Joi.string().description('Имя'),
            slug: Joi.string().description('Слаг'),
            created_at: Joi.number().description('Время создания'),
            updated_at: Joi.number().description('Время обновления'),
        })
        .description('Список типов подписок'),
}).required();
