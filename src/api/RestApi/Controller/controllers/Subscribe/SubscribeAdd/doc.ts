/**
 * @api {post} /subscribe/add                    Подписаться
 * @apiVersion 0.1.0
 * @apiName  Add
 * @apiGroup Subscribe
 *
 * @apiDescription                               Подписаться на рассылку
 * @apiPermission All
 *
 * @apiParam {String}   name                     Имя
 * @apiParam {String}   email                    E-mail
 * @apiParam {Integer}  is_confirm               Согласие на рассылку. 1 или 0
 * @apiParam {Array}    types[]                  Список ID типов рассылки
 *
 * @apiSuccess {String}   status
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
