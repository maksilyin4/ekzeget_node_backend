import * as Joi from '@hapi/joi';

export default Joi.object({
    name: Joi.string().description('Имя'),
    email: Joi.string().description('E-mail'),
    is_confirm: Joi.number().description('Согласие на рассылку. 1 или 0'),
    types: Joi.array()
        .items({})
        .description('Список ID типов рассылки'),
}).required();
