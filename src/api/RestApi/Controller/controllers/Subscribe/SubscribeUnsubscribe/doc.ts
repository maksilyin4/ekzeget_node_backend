/**
 * @api {get} /subscribe/unsubscribe?hash=:hash    Отписаться
 * @apiVersion 0.1.0
 * @apiName  Unsubscribe
 * @apiGroup Subscribe
 *
 * @apiDescription                                 Подписаться на рассылку
 * @apiPermission All
 *
 * @apiParam {String}   hash                       Hash из урла на отписку
 *
 * @apiSuccess {String}   status
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 *
 * @param string $hash
 */
