import * as Joi from '@hapi/joi';

export default Joi.object({
    hash: Joi.string().description('Hash из урла на отписку'),
}).required();
