import * as Joi from '@hapi/joi';

export default Joi.object({
    book: Joi.string().description('Код книги или его ID'),
    chapter: Joi.number().description('Номер главы (опционально)'),
    number: Joi.number().description('Номер стиха (опционально)'),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
}).required();
