import * as Joi from '@hapi/joi';

export default Joi.object({
    testament_id: Joi.number().description('Завет (опционально 1/2, 1 - старый, 2 - новый)'),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
}).required();
