import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import PHPUnserialize from 'php-unserialize';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import BibleMapsPoints from '../../../../../../entities/BibleMapsPoints';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import { paginationParamsWrapper } from '../../../../../../libs/helpers/pagination';
import Verse from '../../../../../../entities/Verse';
import { getBookShortCode } from '../../../presenters/Book/getBookShortCode';

type VerseExtended = Partial<Verse> & { book_code: string; short: string };

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { testament_id } = validatedParams;
    const { page, perPage } = paginationParamsWrapper(validatedParams);

    const query = BibleMapsPoints.createQueryBuilder('bmp')
        .leftJoinAndSelect('bmp.bible_maps_verses', 'bmv')
        .leftJoinAndSelect('bmv.verse', 'verse')
        .leftJoinAndSelect('verse.book', 'book')
        .where('bmp.active = 1')
        .take(perPage)
        .skip(perPage * page);

    if (testament_id) {
        query.andWhere('book.testament_id =: testament_id', { testament_id });
    }

    const [points, total] = await query.getManyAndCount();

    const verseExtender = (verse: Verse): VerseExtended => ({
        id: verse.id,
        number: verse.number,
        book_id: verse.book_id,
        text: verse.text,
        chapter: verse.chapter,
        chapter_id: verse.chapter_id,
        book_code: verse.book.code,
        short: getBookShortCode(verse.book, verse.chapter, verse.number),
    });

    const clientPoints = points.map((p: BibleMapsPoints) => {
        const bookHash = {};
        p.bible_maps_verses.forEach(bmv => {
            if (bookHash[bmv.verse.book.id]) {
                bookHash[bmv.verse.book.id].verse.push(verseExtender(bmv.verse));
                return;
            }
            bookHash[bmv.verse.book.id] = {
                book: bmv.verse.book,
                verse: [bmv.verse].map(v => verseExtender(v)),
            };
        });

        return {
            active: p.active,
            id: p.id,
            title: p.title,
            description: p.description,
            lon: p.lon,
            lat: p.lat,
            created_by: p.created_by,
            edited_by: p.edited_by,
            created_at: p.created_at,
            edited_at: p.edited_at,
            verses: Object.values(bookHash),
            image: PHPUnserialize.unserialize(p.image_id),
        };
    });

    return {
        points: clientPoints,
        pages: returnPaginationObject({ 'per-page': perPage, page, totalCount: total }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
