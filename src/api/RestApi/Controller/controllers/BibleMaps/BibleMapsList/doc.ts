/**
 * @api {get} /bible-maps/:testament_id/?page=:page&per-page=:per-page Список точек
 * @apiVersion 0.1.0
 * @apiName list
 * @apiGroup BibleMaps
 *
 *
 * @apiDescription Список точек
 * @apiPermission All
 *
 * @apiParam {Integer}  [testament_id]   Завет (опционально 1/2, 1 - старый, 2 - новый)
 * @apiParam {Integer}  page        Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page    Количество элементов на странице (опционально, по умолчанию 20)
 *
 *
 * @apiSuccess {Object[]} points                   Список точек
 * @apiSuccess {Integer}  points.id                ID
 * @apiSuccess {String}   points.title             Название точки
 * @apiSuccess {String}   points.description       Описание
 * @apiSuccess {Float}    points.lon               Координаты долгота
 * @apiSuccess {Float}    points.lat               Координаты широта
 *
 * @apiSuccess {Object[]} points.verses            Массив привязанных стихов
 *
 * @apiSuccess {Object}   points.verses.book       информация о книге
 * @apiSuccess {Object[]} points.verses.verse      информация о стихах
 *
 * @apiSuccess {Integer}  points.verses.verse.id         ID записи
 * @apiSuccess {Integer}  points.verses.verse.point_id   ID точки
 * @apiSuccess {Integer}  points.verses.verse.verse_id   ID стиха
 * @apiSuccess {Object}   points.verses.verse.verse      Стих
 * @apiSuccess {Integer}  points.verses.verse.verse.id   ID стиха
 * @apiSuccess {Integer}  points.verses.verse.verse.number  Номер стиха
 * @apiSuccess {Integer}  points.verses.verse.verse.book_id ID книги
 * @apiSuccess {String}   points.verses.verse.verse.text    текст стиха
 * @apiSuccess {Integer}  points.verses.verse.verse.chapter Номер главы
 * @apiSuccess {String}   points.verses.verse.verse.book_code  код книги
 * @apiSuccess {String}   points.verses.verse.verse.short      короткое обозначение стиха
 *
 * @apiSuccess {Object}   points.image            картинка
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (4xx) {ApiException} error
 */
