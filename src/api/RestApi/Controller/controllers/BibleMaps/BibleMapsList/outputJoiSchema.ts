import * as Joi from '@hapi/joi';

export default Joi.object({
    points: Joi.array()
        .items(
            Joi.object({
                id: Joi.number().description('ID'),
                title: Joi.string().description('Название точки'),
                description: Joi.string().description('Описание'),
                lon: Joi.number().description('Координаты долгота'),
                lat: Joi.number().description('Координаты широта'),
                verses: Joi.array()
                    .items({
                        book: Joi.object()
                            .unknown()
                            .description('информация о книге'),
                        verse: Joi.array()
                            .items(
                                Joi.object({
                                    id: Joi.number().description('ID стиха'),
                                    number: Joi.number().description('Номер стиха'),
                                    book_id: Joi.number().description('ID книги'),
                                    text: Joi.string().description('текст стиха'),
                                    chapter: Joi.number().description('Номер главы'),
                                    book_code: Joi.string().description('код книги'),
                                    short: Joi.string().description('короткое обозначение стиха'),
                                })
                                    .unknown()
                                    .description('Стих')
                            )
                            .description('информация о стихах'),
                    })
                    .description('Массив привязанных стихов'),
                image: Joi.alternatives(Joi.object().unknown(), Joi.string().allow('')).description(
                    'картинка'
                ),
            }).unknown()
        )
        .description('Список точек'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    })
        .unknown()
        .description('Информация о возможной постраничной навигации'),
}).required();
