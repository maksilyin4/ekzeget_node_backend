import * as Joi from '@hapi/joi';

export default Joi.object({
    points: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            title: Joi.string().description('Название точки'),
            description: Joi.string().description('Описание'),
            lon: Joi.number().description('Координаты долгота'),
            lat: Joi.number().description('Координаты широта'),
            verses: Joi.array()
                .items({
                    book: Joi.object({}).description('информация о книге'),
                    verse: Joi.array()
                        .items({
                            id: Joi.number().description('ID записи'),
                            point_id: Joi.number().description('ID точки'),
                            verse_id: Joi.number().description('ID стиха'),
                            verse: Joi.object({
                                id: Joi.number().description('ID стиха'),
                                number: Joi.number().description('Номер стиха'),
                                book_id: Joi.number().description('ID книги'),
                                text: Joi.string().description('текст стиха'),
                                chapter: Joi.number().description('Номер главы'),
                                book_code: Joi.string().description('код книги'),
                                short: Joi.string().description('короткое обозначение стиха'),
                            }).description('Стих'),
                        })
                        .description('информация о стихах'),
                })
                .description('Массив привязанных стихов'),
            image: Joi.object({}).description('картинка'),
        })
        .description('Список точек'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    }).description('Информация о возможной постраничной навигации'),
}).required();
