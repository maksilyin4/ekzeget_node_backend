import * as Joi from '@hapi/joi';

export default Joi.object({
    plan: Joi.object().pattern(
        /^/,
        Joi.array()
            .items(
                Joi.object({
                    plan_id: Joi.number().integer().positive().required(),
                    branch: Joi.number().positive().integer().allow(null),
                    day: Joi.number(),
                    book_code: Joi.string(),
                    chapter: Joi.number(),
                    verse: Joi.string().allow('', null),
                    short: Joi.string(),
                })
            )
            .description('Детальная информация о плане')
    ),
}).required();
