import * as Joi from '@hapi/joi';

export default Joi.object({
    plan_id: Joi.number(),
}).required();
