import ReadingPlan from '../../../../../../entities/ReadingPlan';
import readingPlanDetail from './index';
import { BaseEntity } from 'typeorm';
import Reading from '../../../../../../entities/Reading';
import { IReadingPlanDetails } from '../../../../../../core/App/interfaces/IReadingPlanDetails';
import readingPlanException from '../../../../../../const/strings/readingPlan/readingPlanException';

const readingPlanSpy = jest.spyOn(ReadingPlan, 'findOne');
const readingSpy = jest.spyOn(Reading, 'find');
const readingGetDetailsByReadingsAndDaySpy = jest.spyOn(Reading, 'getDetailsByReadingsAndDay');

const getDetailsByReadingsAndDayOutput = [
    [
        {
            day: 1,
            chapter: 1,
            book_code: 'evangelie-ot-matfea',
            verse: '1-17',
            short: 'Мф. 1:1-17',
        },
        {
            day: 1,
            chapter: 1,
            book_code: 'bytie',
            verse: '',
            short: 'Быт. 1',
        },
        {
            day: 1,
            chapter: 2,
            book_code: 'bytie',
            verse: '',
            short: 'Быт. 2',
        },
        {
            day: 1,
            chapter: 1,
            book_code: 'kniga-iova',
            verse: '',
            short: 'Иов. 1',
        },
    ],
    [
        {
            day: 2,
            chapter: 1,
            book_code: 'evangelie-ot-matfea',
            verse: '18-25',
            short: 'Мф. 1:18-25',
        },
        {
            day: 2,
            chapter: 3,
            book_code: 'bytie',
            verse: '',
            short: 'Быт. 3',
        },
        {
            day: 2,
            chapter: 4,
            book_code: 'bytie',
            verse: '',
            short: 'Быт. 4',
        },
        {
            day: 2,
            chapter: 2,
            book_code: 'kniga-iova',
            verse: '',
            short: 'Иов. 2',
        },
    ],
    [
        {
            day: 3,
            chapter: 2,
            book_code: 'evangelie-ot-matfea',
            verse: '1-12',
            short: 'Мф. 2:1-12',
        },
        {
            day: 3,
            chapter: 5,
            book_code: 'bytie',
            verse: '',
            short: 'Быт. 5',
        },
        {
            day: 3,
            chapter: 6,
            book_code: 'bytie',
            verse: '',
            short: 'Быт. 6',
        },
        {
            day: 3,
            chapter: 3,
            book_code: 'kniga-iova',
            verse: '',
            short: 'Иов. 3',
        },
    ],
];

const resultObject = {
    plan: {
        1: [
            {
                day: 1,
                chapter: 1,
                book_code: 'evangelie-ot-matfea',
                verse: '1-17',
                short: 'Мф. 1:1-17',
            },
            {
                day: 1,
                chapter: 1,
                book_code: 'bytie',
                verse: '',
                short: 'Быт. 1',
            },
            {
                day: 1,
                chapter: 2,
                book_code: 'bytie',
                verse: '',
                short: 'Быт. 2',
            },
            {
                day: 1,
                chapter: 1,
                book_code: 'kniga-iova',
                verse: '',
                short: 'Иов. 1',
            },
        ],
        2: [
            {
                day: 2,
                chapter: 1,
                book_code: 'evangelie-ot-matfea',
                verse: '18-25',
                short: 'Мф. 1:18-25',
            },
            {
                day: 2,
                chapter: 3,
                book_code: 'bytie',
                verse: '',
                short: 'Быт. 3',
            },
            {
                day: 2,
                chapter: 4,
                book_code: 'bytie',
                verse: '',
                short: 'Быт. 4',
            },
            {
                day: 2,
                chapter: 2,
                book_code: 'kniga-iova',
                verse: '',
                short: 'Иов. 2',
            },
        ],
        3: [
            {
                day: 3,
                chapter: 2,
                book_code: 'evangelie-ot-matfea',
                verse: '1-12',
                short: 'Мф. 2:1-12',
            },
            {
                day: 3,
                chapter: 5,
                book_code: 'bytie',
                verse: '',
                short: 'Быт. 5',
            },
            {
                day: 3,
                chapter: 6,
                book_code: 'bytie',
                verse: '',
                short: 'Быт. 6',
            },
            {
                day: 3,
                chapter: 3,
                book_code: 'kniga-iova',
                verse: '',
                short: 'Иов. 3',
            },
        ],
    },
};

readingGetDetailsByReadingsAndDaySpy.mockImplementation(async (plans, day) => {
    return (getDetailsByReadingsAndDayOutput[day - 1] as unknown) as IReadingPlanDetails[];
});

afterAll(() => {
    jest.resetAllMocks();
});

describe('get reading plan details', () => {
    test('normal behavior', async () => {
        readingPlanSpy.mockResolvedValueOnce(({
            lenght: 3,
            plan: 'vz1, vz2, uch2, nz',
        } as unknown) as BaseEntity);

        readingSpy.mockResolvedValueOnce(([
            { id: 7 },
            { id: 9 },
            { id: 10 },
            { id: 11 },
        ] as unknown) as BaseEntity[]);

        const result = await readingPlanDetail.handle({ plan_id: 2 });

        expect(result).toEqual(resultObject);
    });

    test('reading plan not found', async () => {
        readingPlanSpy.mockResolvedValueOnce((undefined as unknown) as BaseEntity);
        try {
            const result = await readingPlanDetail.handle({ plan_id: 22 });
        } catch (e) {
            expect(e.message).toBe(readingPlanException.READING_PLAN_NOT_FOUND_MESSAGE);
            expect(e.statusCode).toBe(422);
        }
    });
});
