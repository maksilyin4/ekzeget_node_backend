import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import ReadingPlan from '../../../../../../entities/ReadingPlan';
import Reading from '../../../../../../entities/Reading';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import readingPlanException from '../../../../../../const/strings/readingPlan/readingPlanException';
import { IReadingPlanDetails } from '../../../../../../core/App/interfaces/IReadingPlanDetails';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { plan_id } = validatedParams;
    try {
        const reading = await ReadingPlan.findOne({
            select: ['lenght', 'plan'],
            where: { id: plan_id },
        });

        if (!reading) {
            const error = new Error(readingPlanException.READING_PLAN_NOT_FOUND_MESSAGE);
            error.name = readingPlanException.READING_PLAN_NOT_FOUND_NAME;
            throw error;
        }

        const plan = await getDetails(reading.plan.split(', '), reading.lenght);
        return { plan };
    } catch (e) {
        const serverError = new ServerError(e.message, 422);
        serverError.name = e.name;
        throw serverError;
    }
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);

async function getDetails(plans: string[], length: number) {
    const plan = {};
    const days: number[] = [];
    for (let day = 1; day <= length; ++day) {
        days.push(day);
    }

    const resolved: IReadingPlanDetails[][] = await Promise.all(
        days.map(async d => Reading.getDetailsByReadingsAndDay(plans, d))
    );

    resolved.forEach((_, index) => {
        const d = days[index];
        plan[d] = resolved[index];
    });

    return plan;
}
