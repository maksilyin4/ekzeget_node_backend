/**
 * @api {get} /reading/plan-detail/:plan_id             Детальная информация о плане
 * @apiVersion 0.1.0
 * @apiName plan-detail
 * @apiGroup Reading
 *
 *
 * @apiDescription   Список чтений
 * @apiPermission    All
 *
 *
 * @apiSuccess {Object[]}   plan                        Список планов
 * @apiSuccess {Object[]}   plan.reading                Чтения на выбранный день
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
