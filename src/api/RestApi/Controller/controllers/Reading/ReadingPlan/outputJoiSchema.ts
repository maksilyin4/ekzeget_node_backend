import * as Joi from '@hapi/joi';

export default Joi.object({
    plan: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            title: Joi.string().description('Название'),
            description: Joi.string().description('Описание'),
            lenght: Joi.number().description('Длительность (дней)'),
            plan: Joi.string().description('План'),
            comment: Joi.string().description('Комментарий'),
            active: Joi.number().description('0/1'),
            sort: Joi.number().allow(null).description('Коэф. сортировки'),
            creator_id: Joi.number().allow(null).required(),
            created_at: Joi.date().required(),
        })
        .description('Список планов'),
}).required();
