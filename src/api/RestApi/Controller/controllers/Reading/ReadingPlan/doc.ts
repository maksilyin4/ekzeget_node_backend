/**
 * @api {get} /reading/plan/                           Список планов чтения
 * @apiVersion 0.1.0
 * @apiName plan
 * @apiGroup Reading
 *
 *
 * @apiDescription  Список чтений
 * @apiPermission All
 *
 *
 * @apiSuccess {Object[]}   plan                        Список планов
 * @apiSuccess {Integer}    plan.id                     ID
 * @apiSuccess {String}     plan.title                  Название
 * @apiSuccess {String}     plan.description            Описание
 * @apiSuccess {Integer}    plan.length                 Длительность (дней)
 * @apiSuccess {String}     plan.plan                   План
 * @apiSuccess {String}     plan.comment                Комментарий
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
