import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import ReadingPlan from '../../../../../../entities/ReadingPlan';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';

const handler = async (
    _,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userId = typeof user === 'number'
        ? user
        : user?.id;

    // const readingPlans = await ReadingPlan.find({ where: { active: 1 }, order: { sort: 'ASC' } });
    let query = ReadingPlan.createQueryBuilder('plan')
        .where('plan.active = 1')
        .orderBy('plan.created_at', 'DESC')
        .addOrderBy('-plan.sort', 'DESC')
    if (userId)
        query = query.andWhere(
            '(plan.creator_id IS NULL OR plan.creator_id = :userId)',
            { userId },
        );
    else
        query = query.andWhere('plan.creator_id IS NULL');

    const plans = await query.getMany();
    if (userId) {
        const userPlan = await UserReadingPlan.findOne({
            where: { user_id: userId},
            relations: [
                'group',
                'plan',
                'group.users',
                'group.users.userProfile',
                'group.users.userReadingPlans',
            ],
        });
        if (userPlan && !plans.map(it => it.id).includes(userPlan.plan_id))
            plans.unshift(userPlan.plan);
    }

    return { plan: plans };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
