import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { IReadingPlanDetails } from '../../../../../../core/App/interfaces/IReadingPlanDetails';
import Book from '../../../../../../entities/Book';
import { groupBy } from '../../../../../../libs/helpers/groupBy';

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {

    const bookIds = new Set<number>(params.plan.map(it => it.book_id));
    const books = await Book.findByIds(Array.from(bookIds));
    const idBook = new Map(books.map(book => [book.id, book]));

    const dayPlan = groupBy<Joi.extractType<typeof inputJoiSchema>['plan'][number]>(params.plan, 'day');

    const returnValue: Record<string, IReadingPlanDetails[]> = {};
    Object.entries(dayPlan).forEach(([day, plan]) => {
        returnValue[day] = plan.map(todayPlan => {
            const book = idBook.get(todayPlan.book_id);
            return <IReadingPlanDetails>{
                day: todayPlan.day,
                chapter: todayPlan.chapter,
                book_code: book.code,
                verse: todayPlan.verse ?? '',
                short: book.short_title,
                branch: todayPlan.branch,
            };
        });
    });
    return { plan: returnValue };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
