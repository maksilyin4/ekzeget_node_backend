import * as Joi from '@hapi/joi';

export default Joi.object({
    plan: Joi.object().pattern(
        /^/,
        Joi.array()
            .items(
                Joi.object({
                    day: Joi.number(),
                    book_code: Joi.string(),
                    chapter: Joi.number(),
                    verse: Joi.string().allow('', null),
                    short: Joi.string(),
                    branch: Joi.number(),
                }).unknown(),
            )
            .description('Детальная информация о плане'),
    ),
}).required();
