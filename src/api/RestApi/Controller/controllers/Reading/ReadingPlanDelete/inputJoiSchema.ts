import * as Joi from '@hapi/joi';

export default Joi.object({
    planId: Joi.number().positive().required(),
}).required();
