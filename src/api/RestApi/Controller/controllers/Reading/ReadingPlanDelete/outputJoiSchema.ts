import * as Joi from '@hapi/joi';

export default Joi.object({
    status: 'ok',
}).required();
