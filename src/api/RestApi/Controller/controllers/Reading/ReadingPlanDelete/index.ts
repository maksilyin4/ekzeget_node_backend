import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import ReadingPlan from '../../../../../../entities/ReadingPlan';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import Reading from '../../../../../../entities/Reading';
import { getManager, In } from 'typeorm';

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {

    if (!user)
        throw new ServerError(errorStrings.error401, 401);

    const planToDelete = await ReadingPlan.findOne(params.planId);
    if (!planToDelete)
        throw new ServerError(`Plan with id=${params.planId} does not exist.`, 404);
    if (planToDelete.creator_id !== user.id)
        throw new ServerError(`You don't own this plan.`, 403);

    const planTitles = planToDelete.plan.split(',').map(it => it.trim());

    try {
        await getManager().transaction(async transaction => {
            await transaction.delete(Reading, { title: In(planTitles) });
            await transaction.delete(ReadingPlan, { id: planToDelete.id });
        });
    } catch {
        throw new ServerError('A group may be using this plan. Deletion failed.', 400);
    }

    return { status: 'ok' };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
