import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import ReadingPlan from '../../../../../../entities/ReadingPlan';
import { getManager } from 'typeorm';
import Plan from '../../../../../../entities/Plan';
import Reading from '../../../../../../entities/Reading';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import createRandomString from '../../../../../../libs/helpers/createRandomString';

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {

    if (!user)
        throw new ServerError(errorStrings.error401, 401);

    return await getManager().transaction(async transaction => {
        const reading = transaction.create(Reading, {
            title: `custom-reading-draft-${createRandomString()}`,
            plan: '',
            comment: '',
        });
        await transaction.save(reading);
        reading.title = `custom-reading-${reading.id}`;
        await transaction.save(reading);

        const createdReadingPlan = transaction.create(ReadingPlan, {
            title: `custom-reading-plan-${createRandomString()}`,
            description: params.description,
            comment: params.comment,
            lenght: params.lenght,
            plan: reading.title,
            creator_id: user.id,
            active: 1,
        });
        await transaction.save(createdReadingPlan);
        createdReadingPlan.title = `custom-reading-plan-${createdReadingPlan.id}`;
        await transaction.save(createdReadingPlan);

        const plans = transaction.create(
            Plan,
            params.plan.map(plan => ({ ...plan, reading_id: reading.id })),
        );
        await transaction.save(Plan, plans);

        return { plan: createdReadingPlan };
    });
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
