import * as Joi from '@hapi/joi';

export default Joi.object({
    description: Joi.string().required(),
    comment: Joi.string().required(),
    lenght: Joi.number().positive().required(),
    plan: Joi.array()
        .items({
            branch: Joi.number().positive().optional(),
            day: Joi.number().positive().required(),
            book_id: Joi.number().positive().required(),
            chapter: Joi.number().positive().required(),
            verse: Joi.string().regex(/\d*-\d*/).optional(),
        })
        .required(),
}).required();
