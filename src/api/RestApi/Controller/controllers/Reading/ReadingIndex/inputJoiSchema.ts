import * as Joi from '@hapi/joi';
import pagination from '../../../../../../const/pagination';

export default Joi.object({
    date: Joi.string().description('Дата, в формате dd.mm.YYYY'),
    page: Joi.string()
        .default(pagination.defaultPage)
        .description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.string()
        .default(pagination.defaultPerPage)
        .description('Количество элементов на странице (опционально, по умолчанию 20)'),
}).required();
