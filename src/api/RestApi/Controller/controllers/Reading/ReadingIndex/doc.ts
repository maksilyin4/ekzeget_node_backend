/**
 * @api {get} /reading/:date/                           Список чтений
 * @apiVersion 0.1.0
 * @apiName index
 * @apiGroup Reading
 *
 *
 * @apiDescription  Список чтений
 * @apiPermission All
 *
 * @apiParam {String}   date                             Дата, в формате dd.mm.YYYY
 *
 * @apiSuccess {Object}   reading                        Чтения
 * @apiSuccess {Object[]} reading.title                  Список названий чтений
 * @apiSuccess {String}   reading.apostolic_reading      Апостольские чтения
 * @apiSuccess {String}   reading.gospel_reading         Евангелие
 * @apiSuccess {String}   reading.morning_reading        Утренние чтения
 * @apiSuccess {String}   reading.more_reading           Дополнительные чтения
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
