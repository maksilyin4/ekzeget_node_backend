import * as Joi from '@hapi/joi';

export default Joi.object({
    reading: Joi.object({
        title: Joi.array()
            .items(Joi.string())
            .description('Список названий чтений'),
        color: Joi.string().allow(''),
        apostolic_reading: Joi.string()
            .allow('')
            .description('Апостольские чтения'),
        gospel_reading: Joi.string()
            .allow('')
            .description('Евангелие'),
        morning_reading: Joi.string()
            .allow('')
            .description('Утренние чтения'),
        more_reading: Joi.string()
            .allow('')
            .description('Дополнительные чтения'),
    })
        .options({ presence: 'required' })
        .required()
        .description('Чтения'),
    readingIdx: Joi.object(),
}).required();
