import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import { getReadings, Readings } from '../../../../../../services/readings';

const fixResponseInCaseOfBadDBData = (old: Readings): Readings => {
    return {
        title:
            Array.isArray(old.title) && old.title.every(item => typeof item === 'string')
                ? old.title
                : [],
        apostolic_reading: typeof old.apostolic_reading === 'string' ? old.apostolic_reading : '',
        morning_reading: typeof old.morning_reading === 'string' ? old.morning_reading : '',
        gospel_reading: typeof old.gospel_reading === 'string' ? old.gospel_reading : '',
        more_reading: typeof old.more_reading === 'string' ? old.more_reading : '',
        color: typeof old.color === 'string' ? old.color : '',
    };
};

const handler = async ({
    date,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { readings, readingIdx } = await getReadings(date);

    return { reading: fixResponseInCaseOfBadDBData(readings), readingIdx };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
