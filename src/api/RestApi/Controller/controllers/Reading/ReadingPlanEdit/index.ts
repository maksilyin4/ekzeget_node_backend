import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import ReadingPlan from '../../../../../../entities/ReadingPlan';
import { getManager, In } from 'typeorm/index';
import Plan from '../../../../../../entities/Plan';
import Reading from '../../../../../../entities/Reading';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    if (!user) throw new ServerError(errorStrings.error401, 401);

    return await getManager().transaction(async transaction => {
        const planToUpdate = await transaction.findOne(ReadingPlan, {
            where: { id: params.planId },
            relations: ['userReadingPlans', 'groupReadingPlans'],
        });

        if (!planToUpdate)
            throw new ServerError(`No ReadingPlan found for id=${params.planId}`, 404);
        if (planToUpdate.creator_id !== user.id)
            throw new ServerError(`Can't edit ReadingPlan with id=${params.planId}.`, 403);

        if (params.description) planToUpdate.description = params.description;
        if (params.comment) planToUpdate.comment = params.comment;
        if (params.lenght) planToUpdate.lenght = params.lenght;
        await planToUpdate.save();

        if (params.plan?.length) {
            const planTitles = planToUpdate.plan.split(',').map(it => it.trim());
            const readings = await transaction.find(Reading, { where: { title: In(planTitles) } });
            const readingIds = readings.map(it => it.id);
            const existentPlans = await transaction.getRepository(Plan).find({
                where: { reading_id: In(readingIds) },
            });

            const toDelete: Plan[] = [];
            const toCreate: any[] = [];
            for (const newPlan of params.plan) {
                const exists = existentPlans.find(existent => existent.equals(newPlan));
                if (!exists)
                    toCreate.push(newPlan);
            }
            for (const existent of existentPlans) {
                const shouldExist = params.plan.find(newPlan => existent.equals(newPlan));
                if (!shouldExist)
                    toDelete.push(existent);
            }

            if (toDelete.length > 0)
                await transaction.getRepository(Plan).createQueryBuilder()
                    .delete()
                    .where('id IN (:...planIds)', { planIds: toDelete.map(it => it.id) })
                    .execute();
            if (toCreate.length > 0) {
                const plans = transaction.create(
                    Plan,
                    toCreate.map(plan => ({ ...plan, reading_id: readingIds[0] })),
                );
                await transaction.save(Plan, plans);
            }
        }

        delete planToUpdate['userReadingPlans'];
        delete planToUpdate['groupReadingPlans'];
        return { plan: planToUpdate };
    });
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
