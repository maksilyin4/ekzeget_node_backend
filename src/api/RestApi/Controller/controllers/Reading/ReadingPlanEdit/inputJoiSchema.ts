import * as Joi from '@hapi/joi';

export default Joi.object({
    planId: Joi.number().positive().required(),
    description: Joi.string().optional(),
    comment: Joi.string().optional(),
    lenght: Joi.number().positive().optional(),
    plan: Joi.array()
        .items({
            branch: Joi.number().positive().optional(),
            day: Joi.number().positive().required(),
            book_id: Joi.number().positive().required(),
            chapter: Joi.number().positive().required(),
            verse: Joi.string().regex(/\d*-\d*/).optional(),
        })
        .optional(),
}).required();
