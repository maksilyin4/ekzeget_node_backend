/**
 * @api {get} /book/description/:book_code/ Список описаний книги
 * @apiVersion 0.1.0
 * @apiName description
 * @apiGroup Book
 *
 *
 * @apiDescription Список описаний книги
 * @apiPermission All
 *
 * @apiParam {Integer}    book_code                   ID или код книги
 *
 * @apiSuccess {Object[]} descriptor                   Список стихов
 * @apiSuccess {Integer}  descriptor.id                ID
 * @apiSuccess {Integer}  descriptor.book_id           ID Книги
 * @apiSuccess {String}   descriptor.text               Текст описания
 * @apiSuccess {Object}   descriptor.descriptor         Автор описания
 *
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
