import * as Joi from '@hapi/joi';

export default Joi.object({
    book_code: Joi.number().description('ID или код книги'),
}).required();
