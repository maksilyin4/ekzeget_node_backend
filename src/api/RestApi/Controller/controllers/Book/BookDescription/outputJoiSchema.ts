import * as Joi from '@hapi/joi';

export default Joi.object({
    descriptor: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            book_id: Joi.number().description('ID Книги'),
            text: Joi.string().description('Текст описания'),
            descriptor: Joi.object({}).description('Автор описания'),
        })
        .description('Список стихов'),
}).required();
