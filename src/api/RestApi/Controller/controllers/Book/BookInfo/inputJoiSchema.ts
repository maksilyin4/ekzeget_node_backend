import * as Joi from '@hapi/joi';

export default Joi.object({
    book: Joi.string().description('Код или ID книги').required(),
    chapter: Joi.string().description('Номер глава (опционально)').optional(),
    verse: Joi.string().description('Номер стиха (опционально)').optional(),
}).required();
