import * as Joi from '@hapi/joi';

// TODO Валидация отключена на время разработки
export default Joi.object({
    book: Joi.any().optional(),
    verse: Joi.any().optional(),
});

const old = Joi.object({
    book: Joi.object({
        id: Joi.number().description('ID Книги'),
        testament_id: Joi.number().description('ID'),
        title: Joi.string().description('Название'),
        short_title: Joi.string().description('Короткое название'),
        parts: Joi.number().description('Количество глав в книге'),
        code: Joi.string().description('Код книги'),
        legacy_code: Joi.string().description('???'),
        menu: Joi.string().description('???'),
        author: Joi.string().description('Автор книги'),
        year: Joi.string().description('Годы написания книги'),
        place: Joi.string().description('Место написания книги'),
        ext_id: Joi.number().description('ID книги в сервисе codex-sinaiticus.net'),
        gospel: Joi.number().description('???'),
        sort: Joi.number().description('???'),
        testament: Joi.object({
            id: Joi.number().description('ID'),
            title: Joi.string().description('Название'),
        }).description('Завет'),
        chapters: Joi.array()
            .items({
                number: Joi.number().description('Номер главы'),
                title: Joi.string().description('Название главы'),
                verse_count: Joi.number().description('Количество стихов в главе'),
                media: Joi.object({
                    id: Joi.number().description('Аудио запись к главе'),
                    author: Joi.string().description('Автор / чтец'),
                    title: Joi.string().description('Название'),
                    description: Joi.string().description('Описание'),
                    type: Joi.number().description('Тип (1 - аудио / 2 - видео)'),
                    comment: Joi.string().description('Комментарий'),
                    path: Joi.string().description('Путь до файла'),
                }).description('Аудио запись к главе'),
            })
            .description('Главы'),
    }),
    verse: Joi.object({}).description(
        'Детальная информация о стихе, возвращается при наличии /:book/:chapter/:verse/, ответ идентичен <a href=#api-Verse-detail>/api/v1/verse/detail/:id</a>'
    ),
}).required();
