import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import Book from '../../../../../../entities/Book';
import { groupBy } from '../../../../../../libs/helpers/groupBy';
import { ChapterInterface } from './types/ChapterInterface';
import { ChaptersRawInterface } from './types/ChaptersRawInterface';
import { SingleChapterInterface } from './types/SingleChapterInterface';
import { AudioInterface } from './types/AudioInterface';
import { MediaInterface } from './types/MediaInterface';
import { bookInfoPresenter } from './presenters/bookInfoPresenter';
import { findSingleChapter } from './helpers/findSingleChapter';
import { findManyChapters } from './helpers/findManyChapters';
import { getVerseCountByChapter } from './helpers/getVerseCountByChapter';
import { singleChapterRawPresenter } from './presenters/singleChapterRawPresenter';
import { singleChapterAudioPresenter } from './presenters/singleChapterAudioPresenter';
import { singleChapterMediaPresenter } from './presenters/singleChapterMediaPresenter';
import { extractNumber, UrlPrefixes } from '../../../../../../libs/helpers/urlParsers';
import { ServerError } from '../../../../../../libs/ErrorHandler';

const findAllChapters = async (book: Book): Promise<ChapterInterface[]> => {
    const [chapters, verseCountersByChapterIds] = await Promise.all([
        findManyChapters(book),
        getVerseCountByChapter(book),
    ]);

    const chaptersById = groupBy<ChaptersRawInterface>(chapters, 'id');
    const chapterIds: string[] = Object.keys(chaptersById);

    return chapterIds.map(
        (chapterId: string): ChapterInterface => {
            const chapter: SingleChapterInterface = singleChapterRawPresenter(
                chaptersById[chapterId][0]
            );
            const audio: AudioInterface = singleChapterAudioPresenter(chaptersById[chapterId][0]);
            const media: MediaInterface[] = singleChapterMediaPresenter(chaptersById[chapterId]);

            return {
                ...chapter,
                verse_count: verseCountersByChapterIds[chapterId] || 0,
                title: `Глава ${chapter.number}`,
                audio,
                media,
            };
        }
    );
};

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { redirectFn, finishResponseFn }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { book, verse, chapter } = validatedParams;
    const chapterNum: number = extractNumber(chapter);
    const verseNum: number = extractNumber(verse);

    const bookFound = await Book.findBookByCodeOrId(book);
    if (!bookFound) {
        throw new ServerError('Книга не найдена', 404);
    }

    if (bookFound.legacy_code === book) {
        const route = '/api/v1/book/info';
        const newUrl = `${route}/${book}/${UrlPrefixes.chapter}-${chapterNum}/${UrlPrefixes.versePrefix1}-${verseNum}`;

        redirectFn(newUrl);
        finishResponseFn();
        return;
    }

    const chapters: ChapterInterface[] =
        chapterNum > 0
            ? await findSingleChapter(bookFound, chapterNum)
            : await findAllChapters(bookFound);

    return bookInfoPresenter(bookFound, chapters);
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
