import { ChaptersRawInterface } from '../types/ChaptersRawInterface';
import { AudioInterface } from '../types/AudioInterface';

export const singleChapterAudioPresenter = (chapter: ChaptersRawInterface): AudioInterface => ({
    id: chapter.audio_id,
    src: chapter.audio_src,
    chapter_id: chapter.audio_chapter_id,
    duration: chapter.audio_duration,
});
