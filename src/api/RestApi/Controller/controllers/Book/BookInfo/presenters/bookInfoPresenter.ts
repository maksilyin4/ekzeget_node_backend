import * as Joi from '@hapi/joi';
import outputJoiSchema from '../outputJoiSchema';
import Book from '../../../../../../../entities/Book';
import { ChapterInterface } from '../types/ChapterInterface';

export const bookInfoPresenter = (
    book: Book,
    chapters: ChapterInterface[]
): Joi.extractType<typeof outputJoiSchema> => {
    const {
        id,
        testament_id,
        title,
        short_title,
        parts,
        code,
        legacy_code,
        menu,
        author,
        year,
        place,
        ext_id,
        gospel,
        sort,
        testament,
    } = book;

    return {
        book: {
            id,
            testament_id,
            title,
            short_title,
            parts,
            code,
            legacy_code,
            menu,
            author,
            year,
            place,
            ext_id,
            gospel,
            sort,
            testament: {
                id: testament.id,
                title: testament.title,
            },
            chapters,
        },
    };
};
