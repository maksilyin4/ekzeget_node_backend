import { groupBy } from '../../../../../../../libs/helpers/groupBy';
import { SingleChapterVerseRawInterface } from '../types/SingleChapterVerseRawInterface';
import { VerseInterface } from '../types/VerseInterface';
import { ParallelsInterface } from '../types/ParallelsInterface';
import { VerseTranslatesInterface } from '../types/VerseTranslatesInterface';
import { VerseDictionariesRawInterface } from '../types/VerseDictionariesRawInterface';
import { BibleMapsPointsRaw } from '../helpers/getBibleMapPoints';
import { dictionariesPresenter } from './dictionariesPresenter';
import { bibleMapPointsPresenter } from './bibleMapPointsPresenter';

export const singleChapterVersePresenter = (
    verses: SingleChapterVerseRawInterface[],
    parallels: ParallelsInterface[],
    bibleMapsPointsRaw: BibleMapsPointsRaw[],
    verseTranslates: VerseTranslatesInterface[],
    dictionariesRaw: VerseDictionariesRawInterface[]
): VerseInterface[] => {
    const mapVersesByVerseId = groupBy(verses, 'id');
    const mapParallelsByVerseId = groupBy(parallels, 'parent_verse_id');
    const mapBibleMapsPointsByVerseId = groupBy(bibleMapsPointsRaw, 'bmv_verse_id');
    const mapVerseTranslatesByVerseId = groupBy(verseTranslates, 'verse_id');
    const mapDictionariesByVerseId = groupBy(dictionariesRaw, 'dictionary_verse_verse_id');

    const result: VerseInterface[] = [];
    for (const verseId in mapVersesByVerseId) {
        const {
            id,
            number,
            book_id,
            text,
            chapter,
            chapter_id,
            short,
            book_code,
        } = mapVersesByVerseId[verseId][0];

        const { verseDictionaries, dictionaries } = dictionariesPresenter(
            mapDictionariesByVerseId[verseId] || []
        );
        const { verseBibleMapsPoints, bibleMapsPoints } = bibleMapPointsPresenter(
            mapBibleMapsPointsByVerseId[verseId] || []
        );

        result.push({
            id,
            number,
            book_id,
            text,
            chapter,
            chapter_id,
            short,
            book_code,
            parallels: mapParallelsByVerseId[verseId] || [],
            verseBibleMapsPoints,
            bibleMapsPoints,
            verseTranslates: mapVerseTranslatesByVerseId[verseId] || [],
            verseDictionaries,
            dictionaries,
        });
    }

    return result;
};
