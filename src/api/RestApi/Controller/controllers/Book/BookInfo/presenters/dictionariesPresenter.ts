import { VerseDictionariesRawInterface } from '../types/VerseDictionariesRawInterface';
import { VerseDictionariesInterface } from '../types/VerseDictionariesInterface';
import { DictionariesInterface } from '../types/DictionariesInterface';

interface DictionariesPack {
    verseDictionaries: VerseDictionariesInterface[];
    dictionaries: DictionariesInterface[];
}
export const dictionariesPresenter = (
    dictionariesRaw: VerseDictionariesRawInterface[]
): DictionariesPack => {
    const verseDictionaries: VerseDictionariesInterface[] = [];
    const dictionaries: DictionariesInterface[] = [];

    dictionariesRaw.forEach(d => {
        verseDictionaries.push({
            id: d.dictionary_verse_id,
            dictionary_id: d.dictionary_verse_id,
            verse_id: d.dictionary_verse_verse_id,
        });
        dictionaries.push({
            id: d.dictionary_id,
            type_id: d.dictionary_type_id,
            word: d.dictionary_word,
            letter: d.dictionary_letter,
            description: d.dictionary_description,
            variant: d.dictionary_variant,
            active: d.dictionary_active,
            code: d.dictionary_code,
        });
    });

    return {
        verseDictionaries,
        dictionaries,
    };
};
