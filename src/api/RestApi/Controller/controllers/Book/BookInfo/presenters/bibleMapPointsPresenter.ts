import { BibleMapsPointsInterface } from '../types/BibleMapsPointsInterface';
import { VerseBibleMapsPointsInterface } from '../types/VerseBibleMapsPointsInterface';
import { BibleMapsPointsRaw } from '../helpers/getBibleMapPoints';

interface BibleMapPointsPack {
    verseBibleMapsPoints: VerseBibleMapsPointsInterface[];
    bibleMapsPoints: BibleMapsPointsInterface[];
}
export const bibleMapPointsPresenter = (pointsRaws: BibleMapsPointsRaw[]): BibleMapPointsPack => {
    const verseBibleMapsPoints: VerseBibleMapsPointsInterface[] = [];
    const bibleMapsPoints: BibleMapsPointsInterface[] = [];

    pointsRaws.forEach((p: BibleMapsPointsRaw) => {
        verseBibleMapsPoints.push({
            id: p.bmv_id,
            point_id: p.bmv_point_id,
            verse_id: p.bmv_verse_id,
        });
        bibleMapsPoints.push({
            id: p.bmp_id,
            title: p.bmp_title,
            description: p.bmp_description,
            image_id: p.bmp_image_id,
            lon: p.bmp_lon,
            lat: p.bmp_lat,
            created_by: p.bmp_created_by,
            edited_by: p.bmp_edited_by,
            created_at: p.bmp_created_at,
            edited_at: p.bmp_edited_at,
            active: p.bmp_active,
        });
    });

    return {
        verseBibleMapsPoints,
        bibleMapsPoints,
    };
};
