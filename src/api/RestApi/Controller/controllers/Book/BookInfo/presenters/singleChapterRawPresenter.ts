import { ChaptersRawInterface } from '../types/ChaptersRawInterface';
import { SingleChapterInterface } from '../types/SingleChapterInterface';

export const singleChapterRawPresenter = (c: ChaptersRawInterface): SingleChapterInterface => ({
    id: c.id,
    number: c.number,
    book_id: c.book_id,
});
