import { ChaptersRawInterface } from '../types/ChaptersRawInterface';
import { MediaInterface } from '../types/MediaInterface';

export const singleChapterMediaPresenter = (chapters: ChaptersRawInterface[]): MediaInterface[] =>
    chapters.map(c => ({
        id: c.media_lib_id,
        author: c.media_lib_author,
        title: c.media_lib_title,
        description: c.media_lib_description,
        type: c.media_lib_type,
        comment: c.media_lib_comment,
        path: c.media_lib_path,
        book_id: c.media_lib_book_id,
        chapter: c.media_lib_chapter,
        keywords: c.media_lib_keywords,
        active: c.media_lib_active,
        playlist: c.media_lib_playlist,
        created_at: c.media_lib_created_at,
        length: c.media_lib_length,
        chapter_id: c.media_lib_chapter_id,
        code: c.media_lib_code,
    }));
