export interface VerseDictionariesInterface {
    id: number;
    dictionary_id: number;
    verse_id: number;
}
