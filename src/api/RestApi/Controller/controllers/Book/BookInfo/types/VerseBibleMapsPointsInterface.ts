export interface VerseBibleMapsPointsInterface {
    id: number;
    point_id: number;
    verse_id: number;
}
