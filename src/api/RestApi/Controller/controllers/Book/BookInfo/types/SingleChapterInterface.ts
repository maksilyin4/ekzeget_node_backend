export interface SingleChapterInterface {
    id: number;
    book_id: number;
    number: number;
}
