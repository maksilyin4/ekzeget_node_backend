import { VerseInterface } from './VerseInterface';
import { AudioInterface } from './AudioInterface';
import { MediaInterface } from './MediaInterface';

export interface ChapterInterface {
    id: number;
    book_id: number;
    number: number;
    verse_count: number;
    title: string;
    verses?: VerseInterface[];
    audio: AudioInterface;
    media: MediaInterface[];
}
