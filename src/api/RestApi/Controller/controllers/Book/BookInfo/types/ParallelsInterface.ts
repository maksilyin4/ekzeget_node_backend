export interface ParallelsInterface {
    id: number;
    parent_verse_id: number;
    verse_id: number;
    number: number;
    book_id: number;
    text: string;
    chapter: number;
    chapter_id: number;
    translated: string;
    short: string;
    book_code: string;
}
