export interface MediaInterface {
    id: number;
    author: string;
    title: string;
    description: string;
    type: number;
    comment: string;
    path: string;
    book_id: number;
    chapter: number;
    keywords: string;
    active: number;
    playlist: number;
    created_at: number;
    length: string;
    chapter_id: number;
    code: string;
}
