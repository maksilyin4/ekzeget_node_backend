export interface AudioInterface {
    id: number;
    src: string;
    duration: string;
    chapter_id: number;
}
