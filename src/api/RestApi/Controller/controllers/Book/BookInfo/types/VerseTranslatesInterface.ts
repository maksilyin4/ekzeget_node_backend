interface TranslatesInterface {
    id: number;
    number: number;
    code: string;
    title: string;
    select: string;
    search: string;
}

export interface VerseTranslatesInterface {
    id: number;
    text: string;
    verse_id: number;
    code: TranslatesInterface;
}
