export interface DictionariesInterface {
    id: number;
    type_id: number;
    word: string;
    letter: string;
    description: string;
    variant: string;
    active: number;
    code: string;
}
