export interface SingleChapterVerseRawInterface {
    id: number;
    number: number;
    book_id: number;
    text: string;
    chapter: number;
    chapter_id: number;
    short: string;
    book_code: string;
}
