import { SingleChapterInterface } from './SingleChapterInterface';

export interface ChaptersRawInterface extends SingleChapterInterface {
    audio_id: number;
    audio_src: string;
    audio_duration: string;
    audio_chapter_id: number;

    media_lib_id: number;
    media_lib_author: string;
    media_lib_title: string;
    media_lib_description: string;
    media_lib_type: number;
    media_lib_comment: string;
    media_lib_path: string;
    media_lib_book_id: number;
    media_lib_chapter: number;
    media_lib_keywords: string;
    media_lib_active: number;
    media_lib_playlist: number;
    media_lib_created_at: number;
    media_lib_length: string;
    media_lib_chapter_id: number;
    media_lib_code: string;
}
