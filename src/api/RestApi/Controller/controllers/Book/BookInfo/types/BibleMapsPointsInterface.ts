export interface BibleMapsPointsInterface {
    id: number;
    title: string;
    description: string;
    image_id: string;
    lon: number;
    lat: number;
    created_by: number;
    edited_by: number;
    created_at: number;
    edited_at: number;
    active: number;
}
