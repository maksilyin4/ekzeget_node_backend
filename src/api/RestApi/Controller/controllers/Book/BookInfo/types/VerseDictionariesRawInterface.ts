export interface VerseDictionariesRawInterface {
    dictionary_verse_id: number;
    dictionary_verse_dictionary_id: number;
    dictionary_verse_verse_id: number;

    dictionary_id: number;
    dictionary_type_id: number;
    dictionary_word: string;
    dictionary_letter: string;
    dictionary_description: string;
    dictionary_variant: string;
    dictionary_active: number;
    dictionary_code: string;
}
