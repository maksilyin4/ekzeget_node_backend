import { ParallelsInterface } from './ParallelsInterface';
import { BibleMapsPointsInterface } from './BibleMapsPointsInterface';
import { VerseBibleMapsPointsInterface } from './VerseBibleMapsPointsInterface';
import { VerseTranslatesInterface } from './VerseTranslatesInterface';
import { VerseDictionariesInterface } from './VerseDictionariesInterface';
import { DictionariesInterface } from './DictionariesInterface';

export interface VerseInterface {
    id: number;
    number: number;
    book_id: number;
    text: string;
    chapter: number;
    chapter_id: number;

    short: string;
    book_code: string;
    parallels: ParallelsInterface[];
    verseBibleMapsPoints: VerseBibleMapsPointsInterface[];
    bibleMapsPoints: BibleMapsPointsInterface[];
    verseTranslates: VerseTranslatesInterface[];
    verseDictionaries: VerseDictionariesInterface[];
    dictionaries: DictionariesInterface[];
}
