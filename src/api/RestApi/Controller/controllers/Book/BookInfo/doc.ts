/**
 * @api {get} /book/info/:book/:chapter/:verse/  Детальная информация о книге
 * @apiVersion 0.1.0
 * @apiName info
 * @apiGroup Book
 *
 *
 * @apiDescription Детальная информация о книге
 * @apiPermission All
 *
 * @apiParam {String}   book        Код или ID книги
 * @apiParam {Integer}  chapter     Номер глава (опционально)
 * @apiParam {Integer}  verse       Номер стиха (опционально)
 *
 *
 * @apiSuccess {Object[]} book                   Список стихов
 * @apiSuccess {Integer}  book.id                ID Книги
 * @apiSuccess {String}   book.title             Название
 * @apiSuccess {String}   book.short_title       Короткое название
 * @apiSuccess {Integer}  book.parts             Количество глав в книге
 * @apiSuccess {String}   book.code              Код книги
 * @apiSuccess {Object}   book.testament         Завет
 * @apiSuccess {String}   book.author            Автор книги
 * @apiSuccess {String}   book.year              Годы написания книги
 * @apiSuccess {String}   book.place             Место написания книги
 * @apiSuccess {Integer}  books.ext_id            ID книги в сервисе codex-sinaiticus.net
 * @apiSuccess {Integer}  book.testament.id      ID
 * @apiSuccess {String}   book.testament.title   Название
 * @apiSuccess {Object[]} book.chapters           Главы
 * @apiSuccess {Integer}  book.chapters.number    Номер главы
 * @apiSuccess {String}   book.chapters.title     Название главы
 * @apiSuccess {Integer}  book.chapters.verse_count   Количество стихов в главе
 * @apiSuccess {Object}   book.chapters.media          Аудио запись к главе
 * @apiSuccess {Integer}  book.chapters.media.id          Аудио запись к главе
 * @apiSuccess {String}   book.chapters.media.author         Аатор / чтец
 * @apiSuccess {String}   book.chapters.media.title          Название
 * @apiSuccess {String}   book.chapters.media.description    Описание
 * @apiSuccess {Integer}  book.chapters.media.type           Тип (1 - аудио / 2 - видео)
 * @apiSuccess {String}   book.chapters.media.comment        Комментарий
 * @apiSuccess {String}   book.chapters.media.path           Путь до файла
 *
 * @apiSuccess {Object}   book.chapter                 Текущая глава
 * @apiSuccess {Integer}  book.chapter.number          Номер главы
 * @apiSuccess {String}   book.chapter.title           Название главы
 * @apiSuccess {Object[]} book.chapter.verse           Список стихов принадлежащих главе
 * @apiSuccess {Integer}  book.chapter.media.id          Аудио запись к главе
 * @apiSuccess {String}   book.chapter.media.author         Аатор / чтец
 * @apiSuccess {String}   book.chapter.media.title          Название
 * @apiSuccess {String}   book.chapter.media.description    Описание
 * @apiSuccess {Integer}  book.chapter.media.type           Тип (1 - аудио / 2 - видео)
 * @apiSuccess {String}   book.chapter.media.comment        Комментарий
 * @apiSuccess {String}   book.chapter.media.path           Путь до файла
 *
 * @apiSuccess {Object} verse   Детальная информация о стихе, возвращается при наличии /:book/:chapter/:verse/, ответ идентичен <a href=#api-Verse-detail>/api/v1/verse/detail/:id</a>
 *
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
