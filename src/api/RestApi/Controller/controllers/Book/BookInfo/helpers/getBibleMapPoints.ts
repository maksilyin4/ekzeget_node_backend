import BibleMapsVerse from '../../../../../../../entities/BibleMapsVerse';

export interface BibleMapsPointsRaw {
    bmv_id: number;
    bmv_point_id: number;
    bmv_verse_id: number;

    bmp_id: number;
    bmp_title: string;
    bmp_description: string;
    bmp_image_id: string;
    bmp_lon: number;
    bmp_lat: number;
    bmp_created_by: number;
    bmp_edited_by: number;
    bmp_created_at: number;
    bmp_edited_at: number;
    bmp_active: number;
}

export const getBibleMapPoints = async (versesIds: number[]): Promise<BibleMapsPointsRaw[]> =>
    BibleMapsVerse.createQueryBuilder('bmv')
        .select(['bmv', 'bmp'])
        .leftJoin('bible_maps_points', 'bmp', 'bmv.point_id = bmp.id')
        .where('bmv.verse_id IN (:...versesIds)', { versesIds })
        .getRawMany();
