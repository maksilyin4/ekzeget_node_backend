import Book from '../../../../../../../entities/Book';
import { ParallelsInterface } from '../types/ParallelsInterface';
import Parallel from '../../../../../../../entities/Parallel';

interface ParallelsRawInterface {
    parallel_id: number;
    parallel_parent_verse_id: number;
    parallel_verse_id: number;

    verse_id: number;
    verse_number: number;
    verse_book_id: number;
    verse_text: string;
    verse_chapter: number;
    verse_chapter_id: number;

    book_short_title: string;
    book_code: string;
}

export const getParallels = async (
    book: Book,
    versesIds: number[]
): Promise<ParallelsInterface[]> => {
    const parallels: ParallelsRawInterface[] = await Parallel.createQueryBuilder('parallel')
        .select(['parallel', 'verse', 'book.code', 'book.short_title'])
        .leftJoin('verse', 'verse', 'verse.id = parallel.verse_id')
        .leftJoin('book', 'book', 'book.id = verse.book_id')
        .where('parallel.parent_verse_id IN (:...versesIds)', { versesIds })
        .getRawMany();

    return parallels.map(p => ({
        id: p.parallel_id,
        parent_verse_id: p.parallel_parent_verse_id,
        verse_id: p.parallel_verse_id,
        number: p.verse_number,
        book_id: p.verse_book_id,
        text: p.verse_text,
        chapter: p.verse_chapter,
        chapter_id: p.verse_chapter_id,
        translated: null,
        short: p.book_short_title + ':' + p.verse_chapter + ':' + p.verse_number,
        book_code: p.book_code,
    }));
};
