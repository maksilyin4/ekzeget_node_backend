import { VerseTranslatesInterface } from '../types/VerseTranslatesInterface';
import VerseTranslate from '../../../../../../../entities/VerseTranslate';

interface VerseTranslatesRawInterface {
    verse_translate_id: number,
    verse_translate_code: string,
    verse_translate_text: string,
    verse_translate_verse_id: number,

    translate_id: number,
    translate_number: number,
    translate_code: string,
    translate_title: string,
    translate_select: string,
    translate_search: string,
}

export const getTranslates = async (versesIds: number[]): Promise<VerseTranslatesInterface[]> => {
    const translates: VerseTranslatesRawInterface[] = await VerseTranslate.createQueryBuilder('verse_translate')
      .select(['verse_translate','translate'])
      .leftJoin('translate', 'translate', 'verse_translate.code = translate.code')
      .where('verse_translate.verse_id IN (:...versesIds)', { versesIds })
      .getRawMany();

    return translates.map((t) => ({
        id: t.verse_translate_id,
        text: t.verse_translate_text,
        code: {
            id: t.translate_id,
            number: t.translate_number,
            code: t.translate_code,
            title: t.translate_title,
            select: t.translate_select,
            search: t.translate_search,
        },
        verse_id: t.verse_translate_verse_id,
    }));
};
