import Book from '../../../../../../../entities/Book';
import { ChaptersRawInterface } from '../types/ChaptersRawInterface';
import Chapters from '../../../../../../../entities/Chapters';

export const findManyChapters = async (book: Book): Promise<ChaptersRawInterface[]> =>
    Chapters.createQueryBuilder('chapters')
        .select([
            'chapters.id as id',
            'chapters.book_id as book_id',
            'chapters.number as number',
            'audio',
            'media_lib',
        ])
        .leftJoin('chapters.audio', 'audio')
        .leftJoin('media_lib', 'media_lib', 'media_lib.chapter_id = chapters.id')
        .where('chapters.book_id = :book_id', { book_id: book.id })
        .getRawMany();
