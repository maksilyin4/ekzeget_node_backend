import Book from '../../../../../../../entities/Book';
import Chapters from '../../../../../../../entities/Chapters';

interface VerseCounterByChapterMap {
    [id: number]: number;
}

interface VerseCounterByChapterId {
    chapters_id: number;
    counter: string;
}

export const getVerseCountByChapter = async (book: Book): Promise<VerseCounterByChapterMap> => {
     const verseCountersByChapterIds: VerseCounterByChapterId[] = await Chapters.createQueryBuilder('chapters')
      .select([
          'chapters.id',
          'count(verse.id) as counter'
      ])
      .leftJoin('verse', 'verse', 'verse.chapter_id = chapters.id')
      .where('chapters.book_id = :book_id', { book_id: book.id })
      .groupBy('chapters.id')
      .getRawMany();

    const map: VerseCounterByChapterMap = {};
     verseCountersByChapterIds.forEach((v) => {
         map[v.chapters_id] = parseInt(v.counter);
     });

     return map;
};
