import Book from '../../../../../../../entities/Book';
import Chapters from '../../../../../../../entities/Chapters';
import { ChapterInterface } from '../types/ChapterInterface';
import { ChaptersRawInterface } from '../types/ChaptersRawInterface';
import { SingleChapterInterface } from '../types/SingleChapterInterface';
import { AudioInterface } from '../types/AudioInterface';
import { MediaInterface } from '../types/MediaInterface';
import { SingleChapterVerseRawInterface } from '../types/SingleChapterVerseRawInterface';
import { singleChapterRawPresenter } from '../presenters/singleChapterRawPresenter';
import { singleChapterAudioPresenter } from '../presenters/singleChapterAudioPresenter';
import { singleChapterMediaPresenter } from '../presenters/singleChapterMediaPresenter';
import { singleChapterVersePresenter } from '../presenters/singleChapterVersePresenter';
import { getVerses } from './getVerses';
import { getParallels } from './getParallels';
import { getBibleMapPoints } from './getBibleMapPoints';
import { getTranslates } from './getTranslates';
import { getDictionaries } from './getDictionaries';

export const findSingleChapter = async (
    book: Book,
    chapterNum: number
): Promise<ChapterInterface[]> => {
    const chapters: ChaptersRawInterface[] = await Chapters.createQueryBuilder('chapters')
        .select([
            'chapters.id as id',
            'chapters.book_id as book_id',
            'chapters.number as number',
            'audio',
            'media_lib',
        ])
        .leftJoin('chapters.audio', 'audio')
        .leftJoin('media_lib', 'media_lib', 'media_lib.chapter_id = chapters.id')
        .where('chapters.book_id = :book_id', { book_id: book.id })
        .andWhere('chapters.number = :chapter_num', { chapter_num: chapterNum })
        .getRawMany();

    if (!chapters.length) {
        return [];
    }

    const chapter: SingleChapterInterface = singleChapterRawPresenter(chapters[0]);
    const audio: AudioInterface = singleChapterAudioPresenter(chapters[0]);
    const media: MediaInterface[] = singleChapterMediaPresenter(chapters);

    const versesRaw: SingleChapterVerseRawInterface[] = await getVerses(book, chapters[0].id);
    const versesIds: number[] = versesRaw.map(verse => verse.id);

    const [parallels, bibleMapsPointsRaw, verseTranslates, dictionaries] = await Promise.all([
        getParallels(book, versesIds),
        getBibleMapPoints(versesIds),
        getTranslates(versesIds),
        getDictionaries(versesIds),
    ]);

    const verses = singleChapterVersePresenter(
        versesRaw,
        parallels,
        bibleMapsPointsRaw,
        verseTranslates,
        dictionaries
    );

    return [
        {
            ...chapter,
            verse_count: verses.length,
            title: `Глава ${chapter.number}`,
            verses,
            audio,
            media,
        },
    ];
};
