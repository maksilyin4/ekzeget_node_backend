import DictionaryVerse from '../../../../../../../entities/DictionaryVerse';
import { VerseDictionariesRawInterface } from '../types/VerseDictionariesRawInterface';

export const getDictionaries = async (versesIds: number[]): Promise<VerseDictionariesRawInterface[]> =>
  DictionaryVerse.createQueryBuilder('dictionary_verse')
    .select(['dictionary_verse', 'dictionary'])
    .leftJoin('dictionary', 'dictionary', 'dictionary.id = dictionary_verse.dictionary_id')
    .where('dictionary_verse.verse_id IN (:...versesIds)', { versesIds })
    .getRawMany();
