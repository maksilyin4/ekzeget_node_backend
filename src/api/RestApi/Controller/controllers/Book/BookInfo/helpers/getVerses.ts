import Book from '../../../../../../../entities/Book';
import { SingleChapterVerseRawInterface } from '../types/SingleChapterVerseRawInterface';
import Verse from '../../../../../../../entities/Verse';

interface VerseRawInterface {
    id: number;
    number: number;
    book_id: number;
    text: string;
    chapter: number;
    chapter_id: number;
}

export const getVerses = async (
    book: Book,
    chapterId: number
): Promise<SingleChapterVerseRawInterface[]> => {
    const versesRaw: VerseRawInterface[] = await Verse.createQueryBuilder('verse')
        .select([
            'verse.id as id',
            'verse.number as number',
            'verse.book_id as book_id',
            'verse.text as text',
            'verse.chapter as chapter',
            'verse.chapter_id as chapter_id',
        ])
        .where('verse.book_id = :book_id', { book_id: book.id })
        .andWhere('verse.chapter_id = :chapter_id', { chapter_id: chapterId })
        .getRawMany();

    return versesRaw.map(verse => ({
        ...verse,
        short: `${book.short_title}:${verse.chapter}:${verse.number}`, // TODO Почему тут формат не как везде?
        book_code: book.code,
    }));
};
