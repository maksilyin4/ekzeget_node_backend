import * as Joi from '@hapi/joi';

export default Joi.object({
    testament: Joi.number().description('Завет (опционально 1/2, 1 - старый, 2 - новый)'),
    page: Joi.string().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.string().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
}).required();
