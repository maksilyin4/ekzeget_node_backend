/**
 * @api {get} /book/list/:testament/?page=:page&per-page=:per-page Книги
 * @apiVersion 0.1.0
 * @apiName list
 * @apiGroup Book
 *
 *
 * @apiDescription Список книг
 * @apiPermission All
 *
 * @apiParam {Integer}  testament   Завет (опционально 1/2, 1 - старый, 2 - новый)
 * @apiParam {Integer}  page        Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page    Количество элементов на странице (опционально, по умолчанию 20)
 *
 *
 * @apiSuccess {Object[]} books                   Список стихов
 * @apiSuccess {Integer}  books.id                ID Книги
 * @apiSuccess {String}   books.title             Название
 * @apiSuccess {String}   books.short_title       Короткое название
 * @apiSuccess {Integer}  books.parts             Количество глав в книге
 * @apiSuccess {String}   books.code              Код книги
 * @apiSuccess {Object}   books.testament         Завет
 * @apiSuccess {Integer}  books.testament.id      ID
 * @apiSuccess {String}   books.testament.title   Название
 * @apiSuccess {String}   books.author            Автор книги
 * @apiSuccess {String}   books.year              Годы написания книги
 * @apiSuccess {String}   books.place             Место написания книги
 * @apiSuccess {Integer}  books.ext_id            ID книги в сервисе codex-sinaiticus.net
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
