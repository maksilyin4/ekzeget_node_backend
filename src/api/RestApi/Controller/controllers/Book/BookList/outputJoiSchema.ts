import * as Joi from '@hapi/joi';

export default Joi.object({
    'testament-1': Joi.object({
        books: Joi.array()
            .items(
                Joi.object({
                    id: Joi.number().description('ID Книги'),
                    title: Joi.string().description('Название'),
                    short_title: Joi.string().description('Короткое название'),
                    parts: Joi.number().description('Количество глав в книге'),
                    code: Joi.string().description('Код книги'),
                    testament: Joi.object({
                        id: Joi.number().description('ID'),
                        title: Joi.string().description('Название'),
                    }).description('Завет'),
                    author: Joi.string().description('Автор книги'),
                    year: Joi.string()
                        .allow('')
                        .description('Годы написания книги'),
                    place: Joi.string()
                        .allow('')
                        .description('Место написания книги'),
                    ext_id: Joi.number().description('ID книги в сервисе codex-sinaiticus.net'),
                }).unknown()
            )
            .description('Список стихов'),
    }),
    'testament-2': Joi.object({
        books: Joi.array()
            .items(
                Joi.object({
                    id: Joi.number().description('ID Книги'),
                    title: Joi.string().description('Название'),
                    short_title: Joi.string().description('Короткое название'),
                    parts: Joi.number().description('Количество глав в книге'),
                    code: Joi.string().description('Код книги'),
                    testament: Joi.object({
                        id: Joi.number().description('ID'),
                        title: Joi.string().description('Название'),
                    }).description('Завет'),
                    author: Joi.string().description('Автор книги'),
                    year: Joi.string()
                        .allow('')
                        .description('Годы написания книги'),
                    place: Joi.string()
                        .allow('')
                        .description('Место написания книги'),
                    ext_id: Joi.number().description('ID книги в сервисе codex-sinaiticus.net'),
                }).unknown()
            )
            .description('Список стихов'),
    }),
}).required();
