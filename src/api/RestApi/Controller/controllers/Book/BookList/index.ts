import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Book from '../../../../../../entities/Book';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    return {
        'testament-1': {
            books: await Book.find({ where: { testament_id: 1 }, relations: ['testament'] }),
        },
        'testament-2': {
            books: await Book.find({ where: { testament_id: 2 }, relations: ['testament'] }),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
