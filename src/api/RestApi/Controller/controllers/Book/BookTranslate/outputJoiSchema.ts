import * as Joi from '@hapi/joi';

export default Joi.object({
    translates: Joi.array()
        .items({
            id: Joi.number().description('ID'),
            number: Joi.number().description('Номер перевода'),
            code: Joi.string().description('Код'),
            title: Joi.string().description('Название'),
            select: Joi.string().description('Название для списка'),
            search: Joi.string()
                .allow('')
                .description('это хз что такое'),
        })
        .description('Список стихов'),
}).required();
