/**
 * @api {get} /book/translate-list                  Список переводов
 * @apiVersion 0.1.0
 * @apiName translate
 * @apiGroup Book
 *
 *
 * @apiDescription Список переводов
 * @apiPermission All
 *
 * @apiSuccess {Object[]} translates                   Список стихов
 * @apiSuccess {Integer}  translates.id                ID
 * @apiSuccess {Integer}  translates.number            Номер перевода
 * @apiSuccess {String}   translates.code              Код
 * @apiSuccess {Object}   translates.title             Название
 * @apiSuccess {String}   translates.select            Название для списка
 * @apiSuccess {Object}   translates.search            это хз что такое
 *
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
