import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import UserService from '../../../../../../services/userServices/UserService';
import { userPresenter } from '../../../presenters/User';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { user }
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userWithProfile = await new UserService(user).getUserWithProfile();

    return { user: userPresenter(userWithProfile) };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
