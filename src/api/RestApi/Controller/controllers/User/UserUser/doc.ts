/**
 * @api {get} /user/ Данные пользователя
 * @apiVersion 0.1.0
 * @apiName user
 * @apiGroup User
 *
 *
 * @apiDescription Получить данные текущего пользователя
 * @apiPermission Authorized client
 *
 *
 * @apiSuccess {String}     status              Статус запроса
 * @apiSuccess {Object}     user                Пользователь
 * @apiSuccess {String}     user.username       Имя пользотеля
 * @apiSuccess {Integer}    user.id             Идентификатор пользователя
 * @apiSuccess {String}     user.email          email пользотеля (он же логин)
 * @apiSuccess {String}     user.phone          Телефон пользотеля
 * @apiSuccess {Integer}    user.status         Статус пользователя (1 - не активен, 2 - активен, 3 - заблокирован)
 * @apiSuccess {Integer}    user.created_at     Дата создания аккаунта
 * @apiSuccess {Integer}    user.logged_at      Дата поледнего посещения
 * @apiSuccess {String}     user.country        Страна
 * @apiSuccess {String}     user.city           Город
 * @apiSuccess {String}     user.firstname      Имя пользователя
 * @apiSuccess {String}     user.lastname       Фамилия пользователя
 * @apiSuccess {String}     user.middlename     Отчество пользоватея
 * @apiSuccess {Integer}    user.gender         Пол (1 - мужской, 2 - женский)
 * @apiSuccess {Date}       user.birthday       Дата рождения пользователя
 * @apiSuccess {String}     user.avatar         url фотографии пользователя
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
