import * as Joi from '@hapi/joi';

export default Joi.object({
    status: Joi.string().description('Статус запроса'),
    user: Joi.object({
        username: Joi.string().description('Имя пользотеля'),
        id: Joi.number().description('Идентификатор пользователя'),
        email: Joi.string().description('email пользотеля (он же логин)'),
        phone: Joi.string()
            .allow('')
            .description('Телефон пользотеля'),
        status: Joi.number().description(
            'Статус пользователя (1 - не активен, 2 - активен, 3 - заблокирован)'
        ),
        created_at: Joi.number().description('Дата создания аккаунта'),
        logged_at: Joi.number()
            .allow(null)
            .description('Дата поледнего посещения'),
        country: Joi.string()
            .allow('')
            .description('Страна'),
        city: Joi.string()
            .allow('')
            .description('Город'),
        firstname: Joi.string()
            .allow(null)
            .description('Имя пользователя'),
        lastname: Joi.string()
            .allow(null)
            .description('Фамилия пользователя'),
        middlename: Joi.string()
            .allow('', null)
            .description('Отчество пользоватея'),
        gender: Joi.number()
            .allow(null)
            .description('Пол (1 - мужской, 2 - женский)'),
        birthday: Joi.string()
            .allow(null)
            .description('Дата рождения пользователя'),
        avatar: Joi.string()
            .allow(null)
            .description('url фотографии пользователя'),
        about: Joi.string()
            .allow(null, '')
            .description('О себе'),
    }).description('Пользователь'),
}).required();
