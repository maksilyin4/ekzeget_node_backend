import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import RegisterService from '../../../../../../services/userServices/RegisterService';

const handler = async ({
    secret,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const registerService = new RegisterService();

    const { id, token } = await registerService.approveUser(secret);

    return {
        user: {
            id,
            token,
        },
        status: 'ok',
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
