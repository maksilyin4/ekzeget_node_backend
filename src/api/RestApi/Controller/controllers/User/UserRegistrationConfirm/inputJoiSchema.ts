import * as Joi from '@hapi/joi';

export default Joi.object({
    secret: Joi.string().description('ключ-строка'),
}).required();
