/**
 * @api {post} /user-profile/registration-confirm/                   Подтверждение регистрации
 * @apiVersion 0.1.0
 * @apiName  registration-confirm
 * @apiGroup User Profile
 *
 *
 * @apiDescription Подтверждение регистрации
 * @apiPermission  All
 *
 * @apiParam {String}       secret     ключ-строка
 *
 * @apiSuccess {Object}     user      Пользователь
 * @apiSuccess {Integer}    user.id   id
 * @apiSuccess {String}     user.token Токен
 * @apiSuccess {String}     status    ok
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
