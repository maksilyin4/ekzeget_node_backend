import * as Joi from '@hapi/joi';

export default Joi.object({
    user: Joi.object({
        id: Joi.number().description('id'),
        token: Joi.string().description('Токен'),
    }).description('Пользователь'),
    status: Joi.string().description('ok'),
}).required();
