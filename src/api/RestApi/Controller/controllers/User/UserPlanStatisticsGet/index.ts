import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import { StatisticsService } from '../../../../../../services/userServices/StatisticsService';

const handler = async (
    { plan_id }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    let userReadingPlan: UserReadingPlan;
    if (plan_id)
        userReadingPlan = await UserReadingPlan.findOne(plan_id, { relations: ['plan'] });
    else
        userReadingPlan = await UserReadingPlan.findOne({
            where: { user_id: user.id },
            relations: ['plan'],
        });

    if (!userReadingPlan) {
        return { statistics: {} };
    }

    return { statistics: await StatisticsService.getStatistics(userReadingPlan) };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
