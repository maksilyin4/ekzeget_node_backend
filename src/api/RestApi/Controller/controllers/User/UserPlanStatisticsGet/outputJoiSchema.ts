import * as Joi from '@hapi/joi';
import { UserPlanStatistics } from '../../../../../../services/userServices/StatisticsService/types';

export default Joi.object({
    statistics: UserPlanStatistics.allow({}),
}).required();
