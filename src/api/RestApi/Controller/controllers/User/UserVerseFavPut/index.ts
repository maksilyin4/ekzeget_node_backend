import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import UserVerseFav from '../../../../../../entities/UserVerseFav';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';

const handler = async (
    { id, tags, color }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    if (!user) {
        throw new ServerError(errorStrings.error401, 401);
    }

    const fav = await UserVerseFav.findOne({
        where: {
            id: id,
            userId: user.id,
        },
    });
    if (!fav) {
        throw new ServerError('Закладка не найдена', 400);
    }

    fav.color = color;
    fav.tags = tags;
    await fav.save();

    return {};
};
export default new Controller(handler, inputJoiSchema, outputJoiSchema);
