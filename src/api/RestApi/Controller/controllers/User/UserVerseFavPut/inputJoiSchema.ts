import * as Joi from '@hapi/joi';

export default Joi.object({
    color: Joi.string().description('| PUT, POST | Цвет в формате HEX'),
    tags: Joi.string().description('| PUT, POST | Теги через запятую'),
    id: Joi.number().description('| PUT, DELETE | ID закладки'),
}).required();
