/**
 * @api {get, post, put, delete} /user/verse-fav/?tag=:tag&page=:page&per-page=:per-page        Избранные стихи
 * @apiVersion 0.1.0
 * @apiName verse-fav
 * @apiGroup User
 *
 * @apiDescription Избранные стихи текущего пользователя POST - добавление нового, PUT - Редактирование
 * @apiPermission  Authorized client
 *
 * @apiParam {Integer}  page      | GET |       Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page  | GET |       Количество элементов на странице (опционально, по умолчанию 20)
 * @apiParam {String}   tag       | GET |       Выборка избранных стихов по тегу (опционально)
 * @apiParam {String}   color     | PUT, POST |      Цвет в формате HEX
 * @apiParam {Object[]} verse     | POST |      id стиха
 * @apiParam {String}   tags      | PUT, POST |      Теги через запятую
 * @apiParam {Integer}  id        | PUT, DELETE |    ID закладки
 *
 * @apiSuccess {Object[]}   verse                               Список
 * @apiSuccess {String}     verse.color               Цвет
 * @apiSuccess {String}     verse.tags                Теги
 * @apiSuccess {Object}     verse.verse               Информация о стихе
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
