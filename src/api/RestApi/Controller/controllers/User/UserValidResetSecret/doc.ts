/**
 * @api {get} /user-profile/valid-reset-secret/?secret=:secret Проверка ключа восстановления пароля
 * @apiVersion 0.1.0
 * @apiName  valid-reset-secret
 * @apiGroup User Profile
 *
 *
 * @apiDescription Проверка ключа на валидность
 * @apiPermission  All
 *
 * @apiParam {String}       secret     ключ-строка
 *
 * @apiSuccess {String}     status    ok
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
