import * as Joi from '@hapi/joi';

export default Joi.object({
    verse: Joi.string().description('| PUT | POST - Стих или глава в формате Быт. 1:1 / Быт. 1'),
    text: Joi.string().description('| PUT | POST - Текст заметки'),
    note_id: Joi.number().description('| DELETE | PUT - ID заметки для удаления'),
}).required();
