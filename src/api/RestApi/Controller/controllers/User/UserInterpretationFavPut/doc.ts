/**
 * @api {get, put, delete} /user/interpretation-fav/?page=:page&per-page=:per-page        Избранные толкования
 * @apiVersion 0.1.0
 * @apiName interpretation-fav
 * @apiGroup User
 *
 * @apiDescription Избранные толкования текущего пользователя
 * @apiPermission  Authorized client
 *
 * @apiParam {Integer}  page      | GET |   Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page  | GET |   Количество элементов на странице (опционально, по умолчанию 20)
 * @apiParam {Integer}  interpretation_id  | PUT |   id толкования
 * @apiParam {Integer}  id        | DELETE |   ID закладки
 *
 * @apiSuccess {Object[]}   interpretation                     Список
 * @apiSuccess {Object}     interpretation.interpretation      Толкование
 * @apiSuccess {Object}     interpretation.ekzeget             Информация о толкователе
 * @apiSuccess {Object[]}   interpretation.verse               Стих/стихи, к которым относится толкование
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
