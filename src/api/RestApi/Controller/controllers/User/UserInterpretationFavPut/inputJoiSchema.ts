import * as Joi from '@hapi/joi';

export default Joi.object({
    interpretation_id: Joi.number().description('| PUT | id толкования'),
}).required();
