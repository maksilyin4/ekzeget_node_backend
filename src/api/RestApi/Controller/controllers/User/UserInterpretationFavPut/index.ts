import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import UserInterpretationFav from '../../../../../../entities/UserInterpretationFav';
import Interpretation from '../../../../../../entities/Interpretation';

const handler = async (
    { interpretation_id }: Joi.extractType<typeof inputJoiSchema>,
    { user }
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    await Interpretation.findOneOrFail(interpretation_id);

    await UserInterpretationFav.create({
        user_id: user.id,
        interpretation_id,
    }).save();

    return { status: 'ok' };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
