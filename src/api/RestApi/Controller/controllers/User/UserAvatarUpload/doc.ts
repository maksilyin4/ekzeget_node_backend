/**
 * @api {post} /user/avatar-upload/ Обновить аватар пользователя
 * @apiVersion 0.1.0
 * @apiName avatar-upload
 * @apiGroup User
 *
 *
 * @apiDescription Обновить аватар текущего пользователя.
 * @apiPermission Authorized client
 *
 * @apiParam {File}  file          фаил картинки
 *
 *
 * @apiSuccess {String}     status              Статус запроса
 * @apiSuccess {String}     url                 url картинки
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
