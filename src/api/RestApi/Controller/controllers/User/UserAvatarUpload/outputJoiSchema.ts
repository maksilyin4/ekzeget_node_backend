import * as Joi from '@hapi/joi';

export default Joi.object({
    status: Joi.string().description('Статус запроса'),
    url: Joi.string().description('url картинки'),
}).required();
