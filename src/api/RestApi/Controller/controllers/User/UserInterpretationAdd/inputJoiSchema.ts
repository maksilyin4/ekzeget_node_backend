import * as Joi from '@hapi/joi';

export default Joi.object({
    interpretator: Joi.number()
        .optional()
        .description('ID толкователя (опционально)'),
    investigated: Joi.number()
        .optional()
        .default(0)
        .description('Исследование / толкование 1/0 (опционально, по умолчанию 0)'),
    text: Joi.string().description('Текст толкования'),
    verse: Joi.array()
        .items(Joi.number())
        .required()
        .description('Массив ID стихов, к которым относится толкование'),
}).required();
