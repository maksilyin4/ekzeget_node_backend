/**
 * @api {post} /user/interpretation-add/        Добавить толкование
 * @apiVersion 0.1.0
 * @apiName  interpretation-add
 * @apiGroup User
 *
 * @apiDescription Добавить толкование
 * @apiPermission  Authorized client
 *
 * @apiParam {Integer}   interpretator       ID толкователя (опционально)
 * @apiParam {Integer}   investigated        Иследование / толкование 1/0 (опционально, по умолчанию 0)
 * @apiParam {String}    text                Текст толкования
 * @apiParam {Object[]}  verse               Массив ID стихов, к которым относится толковане
 *
 * @apiSuccess {String}   status                        Статус
 *
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (422) {ApiException} error Ошибка валидации
 */
