import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Interpretation from '../../../../../../entities/Interpretation';
import InterpretationVerse from '../../../../../../entities/InterpretationVerse';
import TextFormatter from '../../../../../../libs/TextFormatter';
import Verse from '../../../../../../entities/Verse';
import InterpretationVerseLink from '../../../../../../entities/InterpretationVerseLink';
import TimelineEvent from '../../../../../../entities/TimelineEvent';
import {
    TimelineApplicationEnum,
    TimelineCategoriesEnum,
    TimelineEventsEnum,
} from '../../../../../../const/enums';
import getUnixSec from '../../../../../../libs/helpers/getUnixSec';

const handler = async (
    {
        text,
        investigated,
        interpretator: ekzeget_id,
        verse,
    }: Joi.extractType<typeof inputJoiSchema>,
    { user }
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const interpretation = await Interpretation.create({
        ekzeget_id,
        parsed_text: await new TextFormatter(text).format(),
        comment: text,
        investigated,
        added_by: user.id,
    }).save();

    const saveIntVersePromises: Promise<InterpretationVerse>[] = verse.map(verse_id =>
        InterpretationVerse.create({
            interpretation_id: interpretation.id,
            verse_id,
        }).save()
    );

    const saveMentionedVersesPromise = InterpretationVerseLink.newMentionedVerses(
        text,
        interpretation.id
    );

    const allPromises: Promise<InterpretationVerse | void | TimelineEvent>[] = [
        ...saveIntVersePromises,
        ...[saveMentionedVersesPromise],
    ];
    await Promise.all(allPromises);

    const timelineData = await TimelineEvent.getDataByInterpretation(interpretation, user);

    await TimelineEvent.create({
        event: TimelineEventsEnum.new,
        application: TimelineApplicationEnum.frontend,
        category: TimelineCategoriesEnum.interpretation,
        data: JSON.stringify(timelineData),
    }).save();

    return { status: 'ok' };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
