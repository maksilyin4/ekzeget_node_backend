import * as Joi from '@hapi/joi';

export default Joi.object({
    interpretation: Joi.array()
        .items(
            Joi.object({
                id: Joi.number().description('ID'),
                comment: Joi.string().description('Текст толкования'),
                added_by: Joi.object()
                    .unknown()
                    .description('Пользователь, добавивший толкование'),
                edited_by: Joi.object()
                    .unknown()
                    .allow(null)
                    .description('Пользователь, изменивший толкование'),
                added_at: Joi.number().description('Дата добавления (unixtimestamp)'),
                edited_at: Joi.number()
                    .allow(null)
                    .description('Дата редактирования (unixtimestamp)'),
                investigated: Joi.number().description('Исследование (0/1)'),
                verse: Joi.array()
                    .items(Joi.object().unknown())
                    .description('Массив стихов, к которым относится толкование'),
                editable: Joi.boolean().description('На модерации или уже нет'),
            }).unknown()
        )
        .description('Список'),
}).required();
