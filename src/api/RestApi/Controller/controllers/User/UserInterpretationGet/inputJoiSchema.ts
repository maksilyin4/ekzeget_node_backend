import * as Joi from '@hapi/joi';

export default Joi.object({
    investigated: Joi.number()
        .optional()
        .description('Иследование / толкование : 1/0 '),
}).required();
