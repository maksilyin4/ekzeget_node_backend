/**
 * @api {get, put} /user/interpretation/        Толкования
 * @apiVersion 0.1.0
 * @apiName  interpretation
 * @apiGroup User
 *
 * @apiDescription Получить/отредактировать толкования добавленые пользователем
 * @apiPermission  Authorized client
 *
 * @apiParam {Integer}   interpretator       ID толкователя (опционально)
 * @apiParam {Integer}   investigated        Иследование / толкование 1/0 (опционально, по умолчанию 0)
 * @apiParam {String}    text                Текст толкования
 * @apiParam {Object[]}  verse               Массив ID стихов, к которым относится толковане
 *
 * @apiSuccess {Object[]} interpretation                Список
 * @apiSuccess {Integer}  interpretation.id             ID
 * @apiSuccess {String}   interpretation.comment        Текст толкования
 * @apiSuccess {Object}   interpretation.added_by       Пользователь, добавивший толкование
 * @apiSuccess {Object}   interpretation.edited_by      Пользователь, изменивший толкование
 * @apiSuccess {Integer}  interpretation.added_at       Дата добавления (unixtimestamp)
 * @apiSuccess {Integer}  interpretation.edited_at      Дата редактирования (unixtimestamp)
 * @apiSuccess {Integer}  interpretation.investigated   Исследование (0/1)
 * @apiSuccess {Object[]} interpretation.verse         Массив стихов, к которым относится толкование
 * @apiSuccess {Bool}     interpretation.editable       Для стиха доступно редактирование
 *
 * @apiSuccess {String}   status              Статус
 *
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (422) {ApiException} error Ошибка валидации
 */
