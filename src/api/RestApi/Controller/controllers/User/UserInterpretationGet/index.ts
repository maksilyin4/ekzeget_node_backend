import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Interpretation from '../../../../../../entities/Interpretation';
import { interpretationInterpretationPresenter as presenter } from '../../../presenters/Interpretation/InterpretationInterpretation';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { user }
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const [interpretations]: [
        Interpretation[],
        number
    ] = await Interpretation.getInterpretationVersesAndCount({ added_by: user.id });

    return {
        interpretation: await Promise.all(
            interpretations.map(async iv => ({
                ...(await presenter(iv)),
                editable: !iv.active && !iv.edited_by,
            }))
        ),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
