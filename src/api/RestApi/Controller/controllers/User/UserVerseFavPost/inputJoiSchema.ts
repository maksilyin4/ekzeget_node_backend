import * as Joi from '@hapi/joi';

export default Joi.object({
    color: Joi.string()
        .required()
        .description('| PUT, POST | Цвет в формате HEX'),
    verse: Joi.array()
        .items(Joi.string().required())
        .required()
        .description('| POST | id стиха'),
    tags: Joi.string()
        .required()
        .allow('')
        .description('| PUT, POST | Теги через запятую'),
}).required();
