import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import UserVerseFav from '../../../../../../entities/UserVerseFav';
import Verse from '../../../../../../entities/Verse';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';

const handler = async (
    { verse, tags, color }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    if (!user) {
        throw new ServerError(errorStrings.error401, 401);
    }

    const verseIds: number[] = verse.map(v => +v);

    const checkPromises: Promise<void>[] = verseIds.map(async verseId => {
        const [isAlreadyExist, verseExist] = await Promise.all([
            UserVerseFav.getOneByUser({ userId: user.id, verseId }),
            Verse.findOne({
                where: {
                    id: verseId,
                },
            }),
        ]);

        if (isAlreadyExist) {
            throw new ServerError('Закладка уже существует', 422);
        }

        if (!verseExist) {
            throw new ServerError('Не корректные данные', 422);
        }
    });

    await Promise.all(checkPromises);

    const savePromises: Promise<UserVerseFav>[] = verseIds.map(verseId =>
        UserVerseFav.create({
            userId: user.id,
            verseId,
            tags,
            color,
        }).save()
    );

    await Promise.all(savePromises);

    return {};
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
