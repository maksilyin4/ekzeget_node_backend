import * as Joi from '@hapi/joi';

export default Joi.object({
    email: Joi.string().description('email пользотеля (он же логин)'),
    username: Joi.string().description('Имя пользователя'),
    city: Joi.string().description('Город'),
    country: Joi.string().description('Страна'),
    firstname: Joi.string().description('Имя'),
    phone: Joi.string().description('телефон'),
    lastname: Joi.string().description('Фамилия'),
    middlename: Joi.string().description('Отчество'),
    gender: Joi.number().description('Пол (1 - мужской, 2 - женский)'),
    birthday: Joi.string().description('Дата рождения пользователя (гггг-мм-дд)'),
    about: Joi.string().description('О себе'),
}).required();
