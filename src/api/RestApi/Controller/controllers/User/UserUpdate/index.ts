import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import UserService from '../../../../../../services/userServices/UserService';
import { userPresenter } from '../../../presenters/User';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { user }
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userService = new UserService(user);
    await userService.updateUser(validatedParams);
    const updatedUserWithProfile = await userService.getUserWithProfile();

    return { user: userPresenter(updatedUserWithProfile) };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
