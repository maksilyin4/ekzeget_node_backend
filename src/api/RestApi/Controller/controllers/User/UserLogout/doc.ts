/**
 * @api {post} /user/logout/ Выход из профиля
 * @apiVersion 0.1.0
 * @apiName logout
 * @apiGroup User
 *
 *
 * @apiDescription Выход из профиля
 * @apiPermission  Authorized client
 *
 *
 * @apiSuccess {String} status    200
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
