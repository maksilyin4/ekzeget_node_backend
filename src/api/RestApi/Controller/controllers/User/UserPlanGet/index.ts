import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';

const handler = async (
    { day = 1 }: Joi.extractType<typeof inputJoiSchema>,
    { user },
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userReadingPlan = await UserReadingPlan.findOne({
        where: { user_id: user.id },
        order: { id: 'DESC' },
        relations: ['plan'],
    });
    if (!userReadingPlan) {
        return {
            plan: {},
        };
    }

    await userReadingPlan.setFieldsForClient(day);
    return { plan: { [userReadingPlan.plan_id]: userReadingPlan } };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
