/**
 * @api {get, put, delete} /user/plan/:day/       Планы чтения
 * @apiVersion 0.1.0
 * @apiName plan
 * @apiGroup User
 *
 * @apiParam {Integer}  [day]       | GET       Номер для от начала чтения, чтения на который необходимо получить (Опционально, по умолчанию текущий день)
 * @apiParam {Integer}  plan_id     | PUT       ID плана на который подписывается пользователь
 * @apiParam {Integer}  subscribe   | PUT       Подписатьсять/отписаться на рассылку (0/1)
 * @apiParam {Integer}  [translate_id]| PUT       ID перевода, в котором необходимо делать рассылку
 * @apiParam {Integer}  id          | DELETE    ID пользовательского плана, который надо удалить
 *
 * @apiDescription Планы чтения текущего пользователя
 * @apiPermission  Authorized client
 *
 *
 * @apiSuccess {Object[]} plan                 Планы чтения
 * @apiSuccess {Date}     plan.start_date      Дата начала чтения
 * @apiSuccess {Date}     plan.stop_date       Дата конца чтения
 * @apiSuccess {Object}   plan.plan            Информация о плане
 * @apiSuccess {Object}   plan.closed_days     Выполненные дни
 * @apiSuccess {Object}   plan.remainder_days  Пропущенные / оставшиеся дни
 * @apiSuccess {Object[]} plan.reading         Чтения на выбранный день
 * @apiSuccess {Object[]} plan.subscribe       0 - не подписан, 1 - подписан
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
