import * as Joi from '@hapi/joi';

export default Joi.object({
    plan: Joi.object()
        .unknown()
        .description('Ассоциативный массив - планы чтения'),
}).required();
