import * as Joi from '@hapi/joi';

export default Joi.object({
    day: Joi.number().description(
        '| GET Номер для от начала чтения, чтения на который необходимо получить (Опционально, по умолчанию текущий день)'
    ),
}).required();
