import * as Joi from '@hapi/joi';
import { paginationObjectScheme } from '../../../../../../libs/helpers/pagination';

export default Joi.object({
    verse: Joi.array()
        .items({
            color: Joi.string()
                .description('Цвет')
                .required(),
            tags: Joi.string()
                .description('Теги')
                .allow(''),
            id: Joi.number().required(),
            verse: Joi.object({
                id: Joi.number().required(),
                book_code: Joi.string().required(),
                book_id: Joi.number().required(),
                chapter: Joi.number().required(),
                chapter_id: Joi.number().required(),
                number: Joi.number().required(),
                short: Joi.string().required(),
                text: Joi.string()
                    .required()
                    .allow(''),
            }).description('Информация о стихе'),
        })
        .description('Список'),
    pages: paginationObjectScheme,
}).required();
