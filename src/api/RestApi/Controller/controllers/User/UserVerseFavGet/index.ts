import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import UserVerseFav from '../../../../../../entities/UserVerseFav';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import pagination from '../../../../../../const/pagination';
import { UserVerseFavVersePresenter } from '../../../presenters/UserVerseFav';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    if (!user) {
        throw new ServerError(errorStrings.error401, 401);
    }

    const page: number = validatedParams.page ? +validatedParams.page : pagination.defaultPage;
    const perPage: number = validatedParams['per-page']
        ? +validatedParams['per-page']
        : pagination.defaultPerPage;

    const fav: UserVerseFav[] = await UserVerseFav.getByUser({
        userId: user.id,
        tag: validatedParams.tag ? validatedParams.tag : null,
        page,
        perPage,
    });

    return {
        verse: fav.map((f: UserVerseFav) => UserVerseFavVersePresenter(f)),
        pages: returnPaginationObject({
            totalCount: fav.length,
            'per-page': perPage,
            page,
        }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
