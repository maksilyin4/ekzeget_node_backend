import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import { serialize } from 'php-serialize';

const handler = async (
    data: Joi.extractType<typeof inputJoiSchema>,
    { user },
): Promise<Joi.extractType<typeof outputJoiSchema>> => {

    if (!user)
        throw new ServerError(errorStrings.error401, 401);

    const urp = await UserReadingPlan.findOne({ where: { user_id: user.id } });
    if (!urp)
        throw new ServerError('You don\'t have any reading plans.', 400);
    if (urp.group_id)
        throw new ServerError('You participate in a group reading plan. Can not edit user-plan.', 400);

    if (data.start_date)
        urp.start_date = data.start_date;
    if (data.stop_date)
        urp.stop_date = data.stop_date;
    if (data.schedule)
        urp.schedule = serialize(data.schedule);

    return { plan: await urp.save() };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
