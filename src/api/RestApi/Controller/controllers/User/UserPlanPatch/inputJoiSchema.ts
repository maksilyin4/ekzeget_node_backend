import * as Joi from '@hapi/joi';

export default Joi.object({
    start_date: Joi.string().isoDate(),
    stop_date: Joi.string().isoDate(),
    schedule: Joi.object({
        Mon: Joi.number().valid(0, 1).required(),
        Tue: Joi.number().valid(0, 1).required(),
        Wed: Joi.number().valid(0, 1).required(),
        Thu: Joi.number().valid(0, 1).required(),
        Fri: Joi.number().valid(0, 1).required(),
        Sat: Joi.number().valid(0, 1).required(),
        Sun: Joi.number().valid(0, 1).required(),
    }),
}).required();
