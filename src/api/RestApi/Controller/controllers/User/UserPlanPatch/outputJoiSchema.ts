import * as Joi from '@hapi/joi';

export default Joi.object({
    status: Joi.string().description('ok'),
    plan: Joi.object({
        id: Joi.number(),
        plan_id: Joi.number(),
        start_date: Joi.string().description('Дата начала чтения'),
        stop_date: Joi.string().description('Дата конца чтения'),
        plan: Joi.object()
            .unknown()
            .description('Информация о плане'),
        closed_days: Joi.array().description('Выполненные дни'),
        remainder_days: Joi.array().description('Пропущенные / оставшиеся дни'),
        reading: Joi.array().description('Чтения на выбранный день'),
        subscribe: Joi.number().description('0 - не подписан, 1 - подписан'),
        translate_id: Joi.number(),
        user_id: Joi.number(),
    })
        .unknown()
        .description('Планы чтения'),
}).required();
