import * as Joi from '@hapi/joi';
import { paginationObjectScheme } from '../../../../../../libs/helpers/pagination';

export default Joi.object({
    note: Joi.array()
        .items({
            book_code: Joi.string()
                .description('Id книги')
                .required(),
            book_id: Joi.number()
                .description('Id книги')
                .required(),
            chapter: Joi.number()
                .description('Глава')
                .required(),
            id: Joi.number()
                .description('ID заметки')
                .required(),
            number: Joi.number()
                .description('Номер стиха')
                .required(),
            short: Joi.string()
                .description('Короткий код книги')
                .required(),
            text: Joi.string()
                .description('Текст заметки')
                .required(),
            user_id: Joi.number()
                .description('ID пользователя')
                .required(),
        })
        .description('Список закладок'),
    pages: paginationObjectScheme,
}).required();
