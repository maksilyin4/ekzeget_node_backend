import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import UserNote from '../../../../../../entities/UserNote';
import { parsePaginationParams } from '../../../../../../libs/helpers/parsePaginationParams';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import { UserNotePresenter } from '../../../presenters/User/Note';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    if (!user) {
        throw new ServerError(errorStrings.error401, 401);
    }

    const { page, perPage } = parsePaginationParams(validatedParams);

    const notes = await UserNote.find({
        where: {
            userId: user.id,
        },
        relations: ['book'],
        take: perPage,
        skip: page * perPage,
    });

    return {
        note: notes.map(n => UserNotePresenter(n)),
        pages: returnPaginationObject({
            totalCount: notes.length,
            page,
            'per-page': perPage,
        }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
