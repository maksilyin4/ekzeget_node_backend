import * as Joi from '@hapi/joi';

export default Joi.object({
    status: Joi.string().description('ok'),
    user: Joi.object({
        id: Joi.number().description('id пользователя'),
        token: Joi.string().description('токен пользователя'),
    }).description('информация о пользователе'),
}).required();
