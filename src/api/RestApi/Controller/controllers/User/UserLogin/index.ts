import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import AuthService from '../../../../../../services/userServices/AuthService';
import { ServerError } from '../../../../../../libs/ErrorHandler';

const handler = async ({
    email,
    password,
    device_info,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const authService = new AuthService();

    try {
        const user = await authService.login({ email, password, device_info });
        return { user, status: 'ok' };
    } catch (err) {
        throw new ServerError('Неверный email или пароль', 401);
    }
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
