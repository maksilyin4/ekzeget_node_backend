import * as Joi from '@hapi/joi';

export default Joi.object({
    email: Joi.string().description('Имя пользователя'),
    password: Joi.string().description('Пароль'),
    device_info: Joi.string().description('json строка с информации об устройстве'),
}).required();
