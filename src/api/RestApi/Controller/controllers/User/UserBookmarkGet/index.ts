import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import UserMark from '../../../../../../entities/UserMark';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { user }
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const page = validatedParams.page || 0;
    const limit = validatedParams['per-page' || 1000];

    const [userBookmarks, totalCount] = await UserMark.findAndCount({
        where: { user_id: user.id },
    });

    const userBookmarksByVerseLink = await Promise.all(
        userBookmarks.map(UserMark.getUserBookmarksByVerseLink)
    );
    const bookmarkHash = {} as { [x: string]: typeof userBookmarksByVerseLink[number] };
    userBookmarksByVerseLink.forEach(ub => (bookmarkHash[ub.bookmark] = ub));

    return {
        bookmark: bookmarkHash,
        pages: returnPaginationObject({ page, 'per-page': limit, totalCount }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
