import * as Joi from '@hapi/joi';

export default Joi.object({
    page: Joi.number().description('| GET | Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        '| GET | Количество элементов на странице (опционально, по умолчанию 20)'
    ),
}).required();
