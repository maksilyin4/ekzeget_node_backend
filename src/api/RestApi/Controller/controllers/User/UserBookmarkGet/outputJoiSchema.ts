import * as Joi from '@hapi/joi';
import { paginationObjectScheme } from '../../../../../../libs/helpers/pagination';

export default Joi.object({
    bookmark: Joi.object()
        .pattern(
            /\w/,
            Joi.object({
                id: Joi.number().description('ID закладки'),
                color: Joi.string().description('цвет в формате HEX'),
                bookmark: Joi.string().description('Текст закладки'),
                created_at: Joi.number().description('Дата добавления'),
                user_id: Joi.number().description('ID юзера'),
                info: Joi.object({
                    book: Joi.object().unknown(),
                    chapter: Joi.number(),
                    number: Joi.number().allow(null),
                    verse: Joi.object()
                        .unknown()
                        .allow(null),
                }).required(),
            })
        )
        .description('Список закладок'),
    pages: paginationObjectScheme,
}).required();
