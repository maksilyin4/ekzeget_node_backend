import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import UserNote from '../../../../../../entities/UserNote';

const handler = async (
    { note_id }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    if (!user) {
        throw new ServerError(errorStrings.error401, 401);
    }

    const note = await UserNote.findOne({
        where: {
            id: note_id,
            userId: user.id,
        },
    });
    if (!note) {
        throw new ServerError('Некорректные данные', 400);
    }

    await note.remove();

    return {};
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
