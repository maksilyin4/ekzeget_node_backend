import * as Joi from '@hapi/joi';

export default Joi.object({
    note_id: Joi.number().description('| DELETE | PUT - ID заметки для удаления'),
}).required();
