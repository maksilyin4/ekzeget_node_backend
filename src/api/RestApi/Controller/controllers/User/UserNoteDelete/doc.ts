/**
 * @api {get, post, put, delete} /user/note/?page=:page&per-page=:per-page        Заметки
 * @apiVersion 0.1.0
 * @apiName  note
 * @apiGroup User
 *
 * @apiDescription Заметки текущего пользователя. POST добавление, PUT редактирование
 * @apiPermission  Authorized client
 *
 * @apiParam {Integer}   page       | GET            - Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}   per-page   | GET            - Количество элементов на странице (опционально, по умолчанию 20)
 * @apiParam {String}    verse      | PUT | POST     - Стих или глава в формате Быт. 1:1 / Быт. 1
 * @apiParam {String}    text       | PUT | POST     - Текст заметки
 * @apiParam {Integer}   note_id    | DELETE | PUT   - ID заметки для удаления
 *
 * @apiSuccess {Object[]}    note                               Список закладок
 * @apiSuccess {Integer}     note.book_id                       Id книги
 * @apiSuccess {Integer}     note.chapter                       Глава
 * @apiSuccess {Integer}     note.number                        Номер стиха
 * @apiSuccess {String}      note.text                          Текст заметки
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
