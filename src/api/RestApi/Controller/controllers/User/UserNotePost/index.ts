import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import UserNote from '../../../../../../entities/UserNote';
import { VerseLinkParser } from '../../../../../../libs/VerseLinkParser';
import Book from '../../../../../../entities/Book';

const handler = async (
    { verse, text }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    if (!user) {
        throw new ServerError(errorStrings.error401, 401);
    }

    const [{ bookShortTitle, chapters }] = new VerseLinkParser().parseString(verse);
    const book = await Book.findOne({
        where: {
            short_title: bookShortTitle,
        },
    });
    if (!book) {
        throw new ServerError('Book not found', 404);
    }
    const [chapter] = chapters;

    const note = UserNote.create({
        userId: user.id,
        bookId: book.id,
        chapter: chapter.chapterNumber.toString(),
        number: chapter.verses.length ? chapter.verses[0].equal.toString() : '',
        text,
    });

    await note.save();

    return {};
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
