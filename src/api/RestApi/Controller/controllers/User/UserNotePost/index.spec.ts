import Book from '../../../../../../entities/Book';
import UserNote from '../../../../../../entities/UserNote';
import UserNotePost from './index';
import User from '../../../../../../entities/User';
import errorStrings from '../../../../../../const/strings/logging/error';

describe('User add note', () => {
    afterAll(async () => {
        jest.restoreAllMocks();
    });

    const bookFindSpy = jest.spyOn(Book, 'findOne');
    const userNoteCreateSpy = jest.spyOn(UserNote, 'create');

    userNoteCreateSpy.mockImplementation(() => {
        return {
            save: async () => {
                return { id: 1 } as UserNote;
            },
        } as UserNote;
    });

    const dataWithoutVerse = { verse: 'Быт. 5', text: 'Test text' };
    const dataWithVerse = { verse: '1 Цар. 5:2', text: 'Test text' };
    const user = { user: { id: 123 } as User };

    test('Normal data without verse', async () => {
        bookFindSpy.mockResolvedValueOnce({ id: 321 } as Book);

        await UserNotePost.handle(dataWithoutVerse, user);

        expect(bookFindSpy.mock.calls[0][0]).toEqual({
            where: {
                short_title: 'Быт',
            },
        });

        expect(userNoteCreateSpy.mock.calls[0][0]).toEqual({
            userId: 123,
            bookId: 321,
            chapter: '5',
            number: '',
            text: 'Test text',
        });
    });

    test('Normal data with verse', async () => {
        bookFindSpy.mockResolvedValueOnce({ id: 321 } as Book);

        await UserNotePost.handle(dataWithVerse, user);

        expect(bookFindSpy.mock.calls[1][0]).toEqual({
            where: {
                short_title: '1 Цар',
            },
        });

        expect(userNoteCreateSpy.mock.calls[1][0]).toEqual({
            userId: 123,
            bookId: 321,
            chapter: '5',
            number: '2',
            text: 'Test text',
        });
    });

    test('No user', async () => {
        bookFindSpy.mockResolvedValueOnce({ id: 321 } as Book);

        try {
            await UserNotePost.handle(dataWithVerse, { user: {} as User });
        } catch (e) {
            expect(e.message).toBe(errorStrings.error401);
            expect(e.statusCode).toBe(401);
        }
    });

    test('No book', async () => {
        bookFindSpy.mockResolvedValueOnce(undefined);

        try {
            await UserNotePost.handle(dataWithVerse, user);
        } catch (e) {
            expect(e.message).toBe('Book not found');
            expect(e.statusCode).toBe(404);
        }
    });
});
