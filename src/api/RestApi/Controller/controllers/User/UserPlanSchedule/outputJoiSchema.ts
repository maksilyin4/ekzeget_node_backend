import * as Joi from '@hapi/joi';

export default Joi.object({
    schedule: Joi.object({}).description('Объект дней недели'),
    status: Joi.string().description('Статус'),
}).required();
