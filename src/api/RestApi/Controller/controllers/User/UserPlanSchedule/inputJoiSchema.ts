import * as Joi from '@hapi/joi';

export default Joi.object({
    plan_id: Joi.number().description('ID пользовательского плана чтения'),
    schedule: Joi.array()
        .items({})
        .description('Массив дней недели в формате schedule[Mon/Tue/Wed/Thu/Fri/Sat/Sun] = 0/1'),
}).required();
