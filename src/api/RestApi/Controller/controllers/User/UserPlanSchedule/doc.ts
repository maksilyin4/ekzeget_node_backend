/**
 * @api {post} /user/plan-schedule/      Подписка по дням недели
 * @apiVersion 0.1.0
 * @apiName  PlanSchedule
 * @apiGroup User
 *
 * @apiDescription Подписаться / отписаться от дня недели
 * @apiPermission  Authorized client
 *
 * @apiParam {Integer}   plan_id             ID пользовательского плана чтения
 * @apiParam {Object[]}  schedule            Массив дней недели в формате schedule[Mon/Tue/Wed/Thu/Fri/Sat/Sun] = 0/1
 *
 * @apiSuccess {Object} schedule             Объект дней недели
 *
 * @apiSuccess {String}   status             Статус
 *
 *
 * @apiError (4xx) {ApiException} error
 */
