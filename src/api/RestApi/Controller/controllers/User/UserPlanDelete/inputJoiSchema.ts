import * as Joi from '@hapi/joi';

export default Joi.object({
    id: Joi.number().description('| DELETE ID пользовательского плана, который надо удалить'),
}).required();
