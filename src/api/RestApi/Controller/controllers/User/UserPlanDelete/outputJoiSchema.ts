import { ReadingArchiveSchema } from '../../../../../../entities/ReadingArchive';
import * as Joi from '@hapi/joi';

export default Joi.object({
    archived: ReadingArchiveSchema,
});
