import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import ReadingArchive from '../../../../../../entities/ReadingArchive';
import { userPresenter } from '../../../presenters/User';

const handler = async (
    { id }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userPlan = await UserReadingPlan.findOne(id);
    if (!userPlan)
        throw new ServerError('Not found', 404);
    if (userPlan.user_id !== user.id)
        throw new ServerError(`You don't own this plan`, 403);

    const archived = await ReadingArchive.fromUserReadingPlan(userPlan);
    await userPlan.remove();
    return {
        archived: {
            ...archived,
            user: userPresenter(archived.user),
            mentor: userPresenter(archived.mentor),
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
