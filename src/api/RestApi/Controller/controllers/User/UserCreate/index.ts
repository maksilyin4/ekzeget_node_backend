import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import Controller, { HandlerOptionalParams } from '../../../../Controller';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';

import UserService from '../../../../../../services/userServices/RegisterService';
import EnvironmentManager from '../../../../../../managers/EnvironmentManager';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';

const handler = async (
    { captcha, ...registerData }: Joi.extractType<typeof inputJoiSchema>,
    { session, clientHost }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { CHECK_CAPTCHA: shouldCaptchaBeChecked } = EnvironmentManager.envs;

    if (+shouldCaptchaBeChecked && session.captcha !== +captcha)
        throw new ServerError(errorStrings.badCaptcha, 400);

    const userService = new UserService();
    await userService.register({ ...registerData, clientHost });

    return { status: 'ok' };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
