/**
 * @api {post} /user-profile/create/ Регистрация пользователя
 * @apiVersion 0.1.0
 * @apiName create
 * @apiGroup User Profile
 *
 *
 * @apiDescription          Регистрация пользователя
 * @apiPermission           All
 *
 * @apiParam {String}       email      Имя пользователя
 * @apiParam {String}       password   Пароль
 * @apiParam {String}       device_info   json строка с информации об устройстве
 * @apiParam {String}       username    Имя пользователя
 *
 *
 * @apiSuccess {String}     status    ok
 * @apiSuccess {Object}     user      информация о пользователе
 * @apiSuccess {Integer}    user.id  id пользователя
 * @apiSuccess {String}     user.token    токен пользователя
 *      *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
