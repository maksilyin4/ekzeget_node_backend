import * as Joi from '@hapi/joi';

export default Joi.object({
    username: Joi.string()
        .required()
        .description('Имя пользователя'),
    email: Joi.string()
        .required()
        .description('Почта пользователя'),
    password: Joi.string()
        .required()
        .description('Пароль'),
    captcha: Joi.string()
        .required()
        .description('Капча'),
    device_info: Joi.string()
        .required()
        .description('json-строка с объектом, описывающим устройство'),
});
