import * as Joi from '@hapi/joi';

export default Joi.object({
    color: Joi.string().description('| PUT | Цвет в формате HEX'),
    mark: Joi.string().description('| PUT | Стих или глава в формате Быт. 1:1'),
}).required();
