import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import UserMark from '../../../../../../entities/UserMark';
import getUnixSec from '../../../../../../libs/helpers/getUnixSec';

const handler = async (
    { mark, color }: Joi.extractType<typeof inputJoiSchema>,
    { user }
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    await UserMark.create({
        user_id: user.id,
        bookmark: mark,
        color,
        created_at: getUnixSec(),
    }).save();

    return { status: 'ok' };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
