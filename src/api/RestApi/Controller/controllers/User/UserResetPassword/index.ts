import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import bcrypt from 'bcryptjs';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import EnvironmentManager from '../../../../../../managers/EnvironmentManager';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import User from '../../../../../../entities/User';
import UserResetPassword from '../../../../../../entities/UserResetPassword';
import { MoreThan } from 'typeorm';
import createRandomString from '../../../../../../libs/helpers/createRandomString';
import secretsLength from '../../../../../../const/numbers/secretsLength';
import MailManager from '../../../../../../managers/MailManager';
import letters from '../../../../../../managers/MailManager/letters';
import getUnixSec from '../../../../../../libs/helpers/getUnixSec';

/**
 * Сброс пароля
 */
const resetPassword = async ({
    password,
    secret,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    if (!password || password.length < 6) {
        throw new ServerError('Пароль не может быть короче 6 символов', 422);
    }

    const reset = await UserResetPassword.findOne({
        where: {
            secret,
            createdAt: MoreThan(getUnixSec() - 60 * 60 * 24),
        },
    });
    if (!reset) {
        throw new ServerError('Not found', 422);
    }

    const user = await User.findOne({
        where: {
            id: reset.userId,
        },
    });
    user.password_hash = bcrypt.hashSync(password);
    await Promise.all([user.save(), reset.remove()]);

    return {};
};

/**
 * Запрос письма для сброса пароля
 */
const requestPasswordReset = async (
    { login, captcha }: Joi.extractType<typeof inputJoiSchema>,
    { session, clientHost }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { CHECK_CAPTCHA } = EnvironmentManager.envs;
    if (+CHECK_CAPTCHA && session.captcha !== +captcha) {
        throw new ServerError(errorStrings.badCaptcha, 400);
    }

    if (!login) {
        throw new ServerError('login is empty', 422);
    }

    const user = await User.findByLogin(login);
    if (!user) {
        throw new ServerError('user not found', 404);
    }

    if (!user.email) {
        throw new ServerError('user has not set their email address', 422);
    }

    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);

    const secretExist = await UserResetPassword.findOne({
        where: {
            createdAt: MoreThan(yesterday.getTime()),
        },
    });
    if (secretExist) {
        throw new ServerError('Повторите запрос позже', 403);
    }

    const secret = createRandomString(secretsLength.registerConfirmCode);

    try {
        await UserResetPassword.create({
            userId: user.id,
            secret,
            createdAt: getUnixSec(),
        }).save();
        await MailManager.send(letters.getPasswordResetConfirm(user.email, { secret, clientHost }));
    } catch (e) {
        throw new ServerError('Ошибка обновления пароля', 422);
    }

    return {};
};

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    optionalParams: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    return validatedParams.secret
        ? resetPassword(validatedParams)
        : requestPasswordReset(validatedParams, optionalParams);
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
