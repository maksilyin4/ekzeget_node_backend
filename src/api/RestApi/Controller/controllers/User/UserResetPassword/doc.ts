/**
 * @api {post} /user-profile/reset-password/ Восстановление пароля
 * @apiVersion 0.1.0
 * @apiName reset-password
 * @apiGroup User Profile
 *
 *
 * @apiDescription Восстановление пароля пользователя. После отправки письма на почту, ссылка действительна в течении суток
 * @apiPermission  All
 *
 * @apiParam {String}       login      Имя пользователя, при первичном запросе для отправки письма
 * @apiParam {String}       secret     ключ-строка, при вторичном запросе для смены пароля
 * @apiParam {String}       password   Пароль, при вторичном запросе для смены пароля
 *
 *
 * @apiSuccess {String}     status    ok
 * @apiSuccess {Object}     user          информация о пользователе, объект возвращается при успешной смене пароля
 * @apiSuccess {Integer}    user.id       id пользователя
 * @apiSuccess {String}     user.token    токен пользователя
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
