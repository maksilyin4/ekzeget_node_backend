import * as Joi from '@hapi/joi';

export default Joi.object({
    login: Joi.string()
        .description('Имя пользователя, при первичном запросе для отправки письма')
        .optional(),
    secret: Joi.string()
        .description('ключ-строка, при вторичном запросе для смены пароля')
        .optional(),
    password: Joi.string()
        .description('Пароль, при вторичном запросе для смены пароля')
        .optional(),
    captcha: Joi.string()
        .description('Captcha')
        .optional(),
}).required();
