import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import UserInterpretationFav from '../../../../../../entities/UserInterpretationFav';

const handler = async (
    { id }: Joi.extractType<typeof inputJoiSchema>,
    { user }
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    await UserInterpretationFav.delete({ id, user_id: user.id });

    return { status: 'ok' };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
