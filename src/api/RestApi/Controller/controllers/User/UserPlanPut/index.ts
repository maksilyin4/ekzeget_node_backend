import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import ReadingPlan from '../../../../../../entities/ReadingPlan';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import { DatesHelper } from '../../../../../../libs/helpers/DatesHelper';
import { secInDay } from '../../../../../../const/numbers/time';

const handler = async (
    { plan_id, subscribe = 0, translate_id = 1 }: Joi.extractType<typeof inputJoiSchema>,
    { user },
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const readingPlan = await ReadingPlan.findOneOrFail(plan_id);

    const sameUserReadingPlan = await UserReadingPlan.findOne({
        where: { user_id: user.id, plan_id: readingPlan.id },
    });

    if (sameUserReadingPlan) {
        sameUserReadingPlan.subscribe = subscribe;
        sameUserReadingPlan.translate_id = translate_id;
        return { plan: await sameUserReadingPlan.save() };
    }

    const anotherUserReadingPlan = await UserReadingPlan.findOne({ where: { user_id: user.id} });

    const newUserReadingPlan: UserReadingPlan = UserReadingPlan.create({
        ...(anotherUserReadingPlan || {}),
        subscribe,
        translate_id,
        plan: readingPlan,
        start_date: DatesHelper.getDateYYYYMMDD(),
        stop_date: DatesHelper.getDateYYYYMMDD(secInDay * readingPlan.lenght),
        user_id: +user.id,
        group_id: null,
    });

    await newUserReadingPlan.save();
    await newUserReadingPlan.setFieldsForClient();

    return { plan: newUserReadingPlan };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
