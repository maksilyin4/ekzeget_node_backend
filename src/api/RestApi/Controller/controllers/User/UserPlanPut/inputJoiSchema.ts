import * as Joi from '@hapi/joi';

export default Joi.object({
    plan_id: Joi.number().description('| PUT ID плана на который подписывается пользователь'),
    subscribe: Joi.number().description('| PUT Подписатьсять/отписаться на рассылку (0/1)'),
    translate_id: Joi.number().description('PUT ID перевода, в котором необходимо делать рассылку'),
}).required();
