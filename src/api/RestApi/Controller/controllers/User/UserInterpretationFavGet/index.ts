import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import UserInterpretationFav from '../../../../../../entities/UserInterpretationFav';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { user }
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const limit = validatedParams['per-page'] || 50;
    const page = validatedParams['page'] || 0;

    const [interpretations, totalCount] = await UserInterpretationFav.createQueryBuilder('uifav')
        .select(['uifav.id'])
        .leftJoinAndSelect('uifav.interpretation', 'i')
        .leftJoinAndSelect('i.ekzeget', 'e')
        .leftJoinAndSelect('i.interpretation_verses', 'iv')
        .leftJoinAndSelect('iv.verse', 'verse')
        .where('uifav.user_id = :user_id', { user_id: user.id })
        .limit(limit)
        .offset(page * limit)
        .getManyAndCount();

    const presentedInterpretations = interpretations.map(
        ({ interpretation: { ekzeget, interpretation_verses, ...interpretationRest }, id }) => ({
            id,
            interpretation: interpretationRest,
            verse: interpretation_verses.map(iv => iv.verse),
            ekzeget,
        })
    );

    return {
        interpretation: presentedInterpretations,
        pages: returnPaginationObject({
            page,
            totalCount,
            'per-page': limit,
        }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
