import * as Joi from '@hapi/joi';
import { paginationObjectScheme } from '../../../../../../libs/helpers/pagination';

export default Joi.object({
    interpretation: Joi.array()
        .items({
            id: Joi.number()
                .required()
                .description('ID закладки толкования'),
            interpretation: Joi.object()
                .unknown()
                .description('Толкование'),
            ekzeget: Joi.object()
                .unknown()
                .description('Информация о толкователе'),
            verse: Joi.array()
                .items(Joi.object().unknown())
                .description('Стих/стихи, к которым относится толкование'),
        })
        .description('Список'),
    pages: paginationObjectScheme,
}).required();
