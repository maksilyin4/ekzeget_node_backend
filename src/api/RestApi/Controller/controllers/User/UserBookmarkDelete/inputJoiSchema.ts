import * as Joi from '@hapi/joi';

export default Joi.object({
    mark_id: Joi.number().description('ID of mark'),
}).required();
