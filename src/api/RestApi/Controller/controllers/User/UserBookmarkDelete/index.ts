import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import UserMark from '../../../../../../entities/UserMark';

const handler = async (
    { mark_id }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    await UserMark.delete({ id: mark_id, user_id: user.id });

    return { status: 'ok' };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
