/**
 * @api {get, put, delete} /user/bookmark/?page=:page&per-page=:per-page        Закладки
 * @apiVersion 0.1.0
 * @apiName  bookmark
 * @apiGroup User
 *
 * @apiDescription Закладки текущего пользователя
 * @apiPermission  Authorized client
 *
 * @apiParam {Integer}  page      | GET |  Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page  | GET |   Количество элементов на странице (опционально, по умолчанию 20)
 * @apiParam {String}   color     | PUT |   Цвет в формате HEX
 * @apiParam {String}   mark      | PUT |   Стих или глава в формате Быт. 1:1
 * @apiParam {Integer}  mark_id   | DELETE |   ID закладки
 *
 * @apiSuccess {Object[]}   bookmark                               Список закладок
 * @apiSuccess {Integer}     bookmark.ID                            ID закладки
 * @apiSuccess {String}     bookmark.color                          цвет в формате HEX
 * @apiSuccess {String}     bookmark.bookmark                       Текст закладки
 * @apiSuccess {Integer}     bookmark.created_at                    Дата добавления
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
