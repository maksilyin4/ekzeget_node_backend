import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import { serialize, unserialize } from 'php-serialize';
import Plan from '../../../../../../entities/Plan';
import UserReadingPlanClosedPlans from '../../../../../../entities/UserReadingPlanClosedPlans';

const handler = async (
    { plan_id }: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userReadingPlan = await UserReadingPlan.findOne({
        where: { user_id: user.id},
        relations: ['plan', 'closedPlans'],
    });

    await UserReadingPlanClosedPlans.create({
        user_reading_plan_id: userReadingPlan.id,
        plan_id,
    }).save();

    return { status: 'Ok' };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
