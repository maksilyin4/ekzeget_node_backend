/**
 * @api {post} /user/plan/       Прочитать день
 * @apiVersion 0.1.0
 * @apiName plan_post
 * @apiGroup User
 *
 * @apiParam {Integer}  [day]      Номер дня от начала чтения, чтения который необходимо закрыть (Опционально, по умолчанию текущий день)
 *
 * @apiDescription Планы чтения текущего пользователя
 * @apiPermission  Authorized client
 *
 * @apiSuccess {String}   status               Статус
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (401) {ApiException} error Нет данных о пользователе
 * @apiError (403) {ApiException} error Доступ запрещен
 */
