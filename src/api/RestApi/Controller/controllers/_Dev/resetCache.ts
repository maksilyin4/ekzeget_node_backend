import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import Controller from '../..';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import CacheManager from '../../../../../managers/CacheManager';
import Book from '../../../../../entities/Book';
import { CacheManagerGlobalKeys } from '../../../../../managers/CacheManager/enum';

const handler = async (
    _: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    await CacheManager.clear();

    const booksFromDB: Book[] = await Book.find();
    await CacheManager.set(CacheManagerGlobalKeys.books, booksFromDB);

    return {};
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
