import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import Controller from '../..';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Media from '../../../../../entities/Media';
import { ServerError } from '../../../../../libs/ErrorHandler';

const handler = async ({
    media_id,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    if (!media_id) {
        throw new ServerError('Empty media_id', 500);
    }
    const media = await Media.findOne({ where: { id: media_id } });
    if (!media) {
        throw new ServerError(`Media element with ID ${media_id} not found`, 404);
    }

    console.log(media);

    return {};
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
