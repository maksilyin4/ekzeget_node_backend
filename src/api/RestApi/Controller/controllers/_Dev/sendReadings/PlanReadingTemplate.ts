import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import Verse from '../../../../../../entities/Verse';

export interface PlanReadingTemplateParamsData {
    plan: {
        book: {
            title: string;
        };
        chapter: number | string;
        verse: number | string;
    };
    verses: Verse[];
}

export interface PlanReadingTemplateParams {
    baseUrl: string;
    emailAdmin: string;
    userReadingPlan: UserReadingPlan;
    currentDay: number;
    data: PlanReadingTemplateParamsData[];
    translate?: {
        code: string;
    };
}

const generateTitle = (data: PlanReadingTemplateParamsData): string => {
    const verse = data.plan.verse ? `:${data.plan.verse}` : ``;
    return `${data.plan.book.title}, глава ${data.plan.chapter}${verse}`;
};

const generateReadings = (
    verse: Verse,
    { translate, baseUrl }: Partial<PlanReadingTemplateParams>
): string => {
    let verse_text = verse.text;
    // const SYNODAL_TRANSLATION_CODE = 'st_text';
    // const wrapper_class = translate ? translate.code : SYNODAL_TRANSLATION_CODE;

    if (translate) {
        verse.verse_translates.forEach(trans => {
            if (translate.code === trans.code && Boolean(trans.text)) {
                verse_text = trans.text;
            }
        });
    }

    return `<sup>${verse.number}</sup>
    <a href="${baseUrl}/bible/${verse.book.code}/${verse.chapter}/${verse.number}/" style="color:#2b2b2b !important;text-decoration:none;">
        <span style="color:#2b2b2b">${verse_text}</span>
    </a>`;
};

const generateMainContent = (
    data: PlanReadingTemplateParamsData,
    allParams: Partial<PlanReadingTemplateParams>
): string =>
    `<div style="text-align: justify;">
                <h2 style="font-size: 20px">${generateTitle(data)}</h2>
                <div style="padding: 0 10px;font-size: 16px;margin-bottom:20px;">
                    ${data.verses.map(verse => generateReadings(verse, allParams)).join('')}
                </div>
                <hr width="90%" size="2" align="right" color="#C7C7C7" style="width:90%;" />
            </div>`;

export const planReadingTemplate = (params: PlanReadingTemplateParams) =>
    `<div style="font-family:Helvetica Neue,Arial,sans-serif">
    <div style="width:100%;text-align: center;background:#243737; padding: 10px 0;line-height: 2;">
        <a href="${
            params.baseUrl
        }/bible" style="margin: 10px;color:#fff !important;text-decoration:none;">
            <span style="color:#fff;white-space:nowrap;">О проекте</span>
        </a>
        <a href="${
            params.baseUrl
        }/ekzegetes" style="margin: 10px;color:#fff !important;text-decoration:none;">
            <span style="color:#fff;white-space:nowrap;">Новости и обновления</span>
        </a>
        <a href="${
            params.baseUrl
        }/preaching" style="margin: 10px;color:#fff !important;text-decoration:none;">
            <span style="color:#fff;white-space:nowrap;">Пожертвования</span>
        </a>
        <a href="${
            params.baseUrl
        }/dictonaries" style="margin: 10px;color:#fff !important;text-decoration:none;">
            <span style="color:#fff;white-space:nowrap;">Контакты</span>
        </a>
    </div>
    <div style="max-width:640px;padding:0 20px;margin: 0 auto;">
        <p style="font-size: 20px;text-align:center;">Здравствуйте, ${
            params.userReadingPlan.user.username
        }, Вам доставлено Ваше чтение Библии!</p>
        <p>План чтения:
            <b>${params.userReadingPlan.plan.description}</b>
        </p>
        <a href="${
            params.baseUrl
        }/lk/reading-plan/" style="color:#2b2b2b !important;">Настроить или изменить</a> |
        <a href="${params.baseUrl}/lk/reading-plan/${params.userReadingPlan.plan_id}/?day=${
        params.currentDay
    }" style="color:#2b2b2b !important;">Читать на сайте</a>
    </div>
    <div style="max-width:640px;padding:0 20px;margin: 40px auto 0;">
        <h2 style="font-size: 20px;">Сегодня ${params.currentDay}-й день чтения</h2>
        <hr width="90%" size="2" align="right" color="#C7C7C7" style="width:90%;" />
        ${params.data.map(item => generateMainContent(item, params)).join('')}
    </div>
    <div style="max-width:640px;margin:30px auto 50px;padding:0 16px;">
        <i style="text-align:justify;">
            <p>Это письмо сформировано автоматически, просьба не отвечать на него. По всем вопросам обращайтесь к администрации
                сайта -
                <a href="mailto:${params.emailAdmin}">${params.emailAdmin}</a>
            </p>
        </i>
    </div>
    <div style="width:100%;text-align: center;background:#243737; padding: 10px 0;line-height: 2;">
        <a href="${
            params.baseUrl
        }/bible" style="margin: 10px;color:#fff !important;text-decoration:none;">
            <span style="color:#fff;white-space:nowrap;">Библия</span>
        </a>
        <a href="${
            params.baseUrl
        }/ekzegetes" style="margin: 10px;color:#fff !important;text-decoration:none;">
            <span style="color:#fff;white-space:nowrap;">Экзегеты</span>
        </a>
        <a href="${
            params.baseUrl
        }/preaching" style="margin: 10px;color:#fff !important;text-decoration:none;">
            <span style="color:#fff;white-space:nowrap;">Проповеди</span>
        </a>
        <a href="${
            params.baseUrl
        }/dictonaries" style="margin: 10px;color:#fff !important;text-decoration:none;">
            <span style="color:#fff;white-space:nowrap;">Словари</span>
        </a>
        <a href="${
            params.baseUrl
        }/bible-group" style="margin: 10px;color:#fff !important;text-decoration:none;">
            <span style="color:#fff;white-space:nowrap;">Библейские группы</span>
        </a>
        <a href="${
            params.baseUrl
        }/lecture" style="margin: 10px;color:#fff !important;text-decoration:none;">
            <span style="color:#fff;white-space:nowrap;">Лекторий</span>
        </a>
    </div>
</div>`;
