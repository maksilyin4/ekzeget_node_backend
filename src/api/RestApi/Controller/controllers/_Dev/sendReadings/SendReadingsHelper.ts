import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import { LessThanOrEqual, MoreThanOrEqual } from 'typeorm';
import { planReadingTemplate, PlanReadingTemplateParamsData } from './PlanReadingTemplate';
import EnvironmentChecker from '../../../../../../core/EnvironmentChecker';
import MailManager from '../../../../../../managers/MailManager';
import mailing from '../../../../../../const/strings/mailing';
import EnvironmentDTO from '../../../../../../core/EnvironmentChecker/EnvironmentDTO';
import Verse from '../../../../../../entities/Verse';

export class SendReadingsHelper {
    constructor() {
        const environmentChecker = new EnvironmentChecker();
        this.env = environmentChecker.getCheckedEnvironment();
    }

    private env: EnvironmentDTO;

    private log({ id, user_id }: UserReadingPlan, status: string): void {
        console.info(`SendReadings [UID=${user_id}|UPR=${id}]: ${status}`);
    }

    private getCurrentDay({ start_date }: UserReadingPlan): number {
        return +((Date.now() - Date.parse(start_date)) / 1000 / 86400).toFixed();
    }

    public async getActivePlans(): Promise<UserReadingPlan[]> {
        return UserReadingPlan.find({
            where: {
                subscribe: 1,
                start_date: LessThanOrEqual(new Date()),
                stop_date: MoreThanOrEqual(new Date()),
            },
            relations: ['plan', 'translate', 'user'],
        });
    }

    public async sendReadings(userReadingPlan: UserReadingPlan) {
        this.log(userReadingPlan, 'In progress');
        const currentDay = this.getCurrentDay(userReadingPlan);
        await userReadingPlan.setFieldsForClient(currentDay);

        if (this.skipWeekDay(userReadingPlan)) {
            this.log(userReadingPlan, 'Skipped by week day');
            return;
        }

        const emailContent = planReadingTemplate({
            baseUrl: `${this.env.PROTOCOL}://${this.env.REST_HOST}`,
            currentDay,
            emailAdmin: this.env.SMTP_ADMIN_MAIL,
            data: await this.prepareData(userReadingPlan),
            userReadingPlan: userReadingPlan,
        });

        await MailManager.send({
            from: `${mailing.senderName}<${mailing.senderMail}>`,
            to: userReadingPlan.user.email,
            subject: `${currentDay}-й день чтения Библии`,
            html: emailContent,
        });

        this.log(userReadingPlan, 'Done');
    }

    private async prepareData(
        userReadingPlan: UserReadingPlan
    ): Promise<PlanReadingTemplateParamsData[]> {
        const result: PlanReadingTemplateParamsData[] = [];

        for (const reading of userReadingPlan.reading) {
            result.push({
                plan: reading[0],
                verses: await Verse.getByShortLink(reading[0].short),
            });
        }

        return result;
    }

    private skipWeekDay({ schedule }: UserReadingPlan): boolean {
        const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        const todayNameShort = days[new Date().getDay()];

        return schedule ? schedule[todayNameShort] !== 1 : false;
    }
}
