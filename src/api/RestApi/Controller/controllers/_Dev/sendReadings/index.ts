import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import Controller from '../../../index';
import inputJoiSchema from '../inputJoiSchema';
import outputJoiSchema from '../outputJoiSchema';
import { SendReadingsHelper } from './SendReadingsHelper';
import UserReadingPlan from '../../../../../../entities/UserReadingPlan';
import CacheManager from '../../../../../../managers/CacheManager';
import { CacheManagerTTL } from '../../../../../../managers/CacheManager/enum';

const handler = async (
    _: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const cacheKey = `SendReadings-${new Date().toISOString().slice(0, 10)}`;
    const cache = await CacheManager.get(cacheKey);
    if (cache) {
        return {};
    }

    const helper = new SendReadingsHelper();

    const userReadingPlans: UserReadingPlan[] = await helper.getActivePlans();
    console.info(`SendReadings: Всего выбрано пользователей [${userReadingPlans.length}]`);

    if (!userReadingPlans || !userReadingPlans.length) {
        return {};
    }

    for (const urp of userReadingPlans) {
        if (urp.user.email) {
            await helper.sendReadings(urp);
        }
    }

    console.info(`SendReadings: All messages are sent!`);
    await CacheManager.set(cacheKey, true, CacheManagerTTL.day);

    return {};
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
