import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import Controller from '../..';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Article from '../../../../../entities/Article';
import EnvironmentManager from '../../../../../managers/EnvironmentManager';

const handler = async ({
    media_id,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    // Замена абсолютных ссылок на картинки
    if (media_id === 1) {
        await updateArticles();
    }

    return {};
};

async function updateArticles() {
    const articles = await Article.find();

    const updateArticleBodies = async (article: Article): Promise<Article> => {
        const regExp1: RegExp = /src="\/storage\/web\/source\/1/gi;
        const newSrc1 = `src="${EnvironmentManager.envs.STORAGE_ASSETS_CDN}/storage/web/source/1`;
        article.body = article.body.replace(regExp1, newSrc1);

        const regExp2: RegExp = /src="https:\/\/ekzeget.ru\/storage\/web\/source/gi;
        const newSrc2 = `src="${EnvironmentManager.envs.STORAGE_ASSETS_CDN}/storage/web/source`;
        article.body = article.body.replace(regExp2, newSrc2);

        return article.save();
    };

    const promises = articles.map(a => updateArticleBodies(a));
    await Promise.all(promises);
}

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
