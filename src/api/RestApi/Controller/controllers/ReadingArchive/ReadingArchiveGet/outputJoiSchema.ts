import * as Joi from '@hapi/joi';
import { ReadingArchiveSchema } from '../../../../../../entities/ReadingArchive';

export default Joi.object({
    archive: Joi.array().items(ReadingArchiveSchema),
}).required();
