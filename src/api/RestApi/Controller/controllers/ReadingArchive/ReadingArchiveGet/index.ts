import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import ReadingArchive from '../../../../../../entities/ReadingArchive';

const handler = async (
    _: Joi.extractType<typeof inputJoiSchema>,
    { user }: HandlerOptionalParams,
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const userId = typeof user === 'number'
        ? user
        : user?.id;

    const archive = await ReadingArchive.find({
        where: { user_id: userId },
        order: { id: 'DESC' },
    });
    return { archive };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
