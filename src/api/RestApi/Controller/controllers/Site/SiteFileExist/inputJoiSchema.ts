import * as Joi from '@hapi/joi';

export default Joi.object({
    file: Joi.string().description(
        'Строка содержащая путь до файла (например: st_text/mf/mf-1-st_text.mp3)'
    ),
}).required();
