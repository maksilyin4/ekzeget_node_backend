/**
 * @api {post} /site/file-exist/                          Проверка наличия аудиофайла
 * @apiVersion 0.1.0
 * @apiName  file-exist
 * @apiGroup Site
 *
 *
 * @apiDescription  Проверка наличия аудиофайла
 * @apiPermission All
 *
 * @apiParam {String}     file                           Строка содержащая путь до файла (например: st_text/mf/mf-1-st_text.mp3)
 *
 *
 * @apiSuccess {Bool}     file_exist                      Доступен или не доступен фаил
 *
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
