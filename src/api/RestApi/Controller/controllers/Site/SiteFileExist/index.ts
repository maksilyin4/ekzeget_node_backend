import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import axios from 'axios';
import EnvironmentManager from '../../../../../../managers/EnvironmentManager';

const handler = async ({
    file,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const url = EnvironmentManager.envs.OLD_BIBLE_CDN + file;
    let status: number;

    try {
        const response = await axios.head(url);
        status = response.status;
    } catch (e) {
        return { file_exist: false };
    }

    return { file_exist: status === 200 };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
