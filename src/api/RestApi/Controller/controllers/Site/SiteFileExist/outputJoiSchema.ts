import * as Joi from '@hapi/joi';

export default Joi.object({
    file_exist: Joi.boolean().description('Доступен или не доступен фаил'),
}).required();
