/**
 * @api {post} /site/contact/                            Обратная связь
 * @apiVersion 0.1.0
 * @apiName  contact
 * @apiGroup Site
 *
 *
 * @apiDescription  Обратная связь
 * @apiPermission All
 *
 * @apiParam {String}     body                           Текст сообщения
 * @apiParam {String}     email                          email отправителя
 * @apiParam {String}     name                           Имя отправителя
 * @apiParam {String}     verifyCode                     Каптча
 * @apiParam {String}     sendTo                         На какой адрес отправить email. Параметр должен соответствовать
 *                                                       второй части переменной mail.*
 *                                                       Например, что бы отправить форму на адрес/группу адресов sendTo=bgroup,
 *                                                       необходимо <a href="http://ekzeget1.itech-test.ru/admin/key-storage/index" target="_blank">создать переменную</a>
 *                                                       mail.bgroup содержащую адрес/группу адресов через запятую.
 *                                                       По умолчанию используется переменная <i>mail.admin</i>
 *
 * @apiSuccess {String}   status                         статус
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
