import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import EnvironmentManager from '../../../../../../managers/EnvironmentManager';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import errorStrings from '../../../../../../const/strings/logging/error';
import MailManager from '../../../../../../managers/MailManager';
import letters from '../../../../../../managers/MailManager/letters';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { session, clientHost }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { CHECK_CAPTCHA, SMTP_ADMIN_MAIL } = EnvironmentManager.envs;
    if (+CHECK_CAPTCHA && session.captcha !== +validatedParams.captcha)
        throw new ServerError(errorStrings.badCaptcha, 400);

    try {
        await MailManager.send(
            letters.contactsForm(SMTP_ADMIN_MAIL, {
                senderName: validatedParams.name,
                body: validatedParams.body,
                mail: validatedParams.email,
                clientHost,
            })
        );

        return { status: 'ok' };
    } catch (e) {
        throw new ServerError('Ошибка отправки письма', 500);
    }
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
