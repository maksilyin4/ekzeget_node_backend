import * as Joi from '@hapi/joi';

export default Joi.object({
    name: Joi.string().description('Имя отправителя'),
    email: Joi.string().description('email отправителя'),
    body: Joi.string().description('Текст сообщения'),
    captcha: Joi.string().description('Каптча'),
}).required();
