import * as Joi from '@hapi/joi';

export default Joi.object({
    url: Joi.string().required(),
    before_selection: Joi.string().required(),
    selection: Joi.string().required(),
    after_selection: Joi.string().required(),
    comment: Joi.string(),
}).required();
