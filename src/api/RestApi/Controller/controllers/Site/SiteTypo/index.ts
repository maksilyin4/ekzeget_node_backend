import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import Typo from '../../../../../../entities/Typo';
import getUnixSec from '../../../../../../libs/helpers/getUnixSec';

const handler = async (
    report: Joi.extractType<typeof inputJoiSchema>,
    { session }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    await Typo.insert({
        ...report,
        user_id: session.passport?.user,
        created_at: getUnixSec(),
        selection: `
            ${stripTags(report.before_selection)}
                <span class="selection" style="color: red; text-decoration: underline;">
                    ${stripTags(report.selection)}
                </span>
            ${stripTags(report.after_selection)}`,
    });

    return { status: 'ok' };
};

function stripTags(text) {
    return text.replace(/(<([^>]+)>)/g, '');
}

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
