import * as Joi from '@hapi/joi';

export default Joi.object({
    captcha: Joi.string()
        .required()
        .description('Pgn капчи  в формате base64'),
});
