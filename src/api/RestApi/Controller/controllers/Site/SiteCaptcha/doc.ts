/**
 * @api {get} /site/captcha/                             Каптча
 * @apiVersion 0.1.0
 * @apiName  captcha
 * @apiGroup Site
 *
 *
 * @apiDescription  Обратная связь
 * @apiPermission All
 *
 * @apiSuccess {Object}   captcha
 * @apiSuccess {String}   captcha.url                    ссылка на картинку содержащую код
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
