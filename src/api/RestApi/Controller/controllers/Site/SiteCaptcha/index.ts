import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import { HandlerOptionalParams } from '../../../index';
import Controller from '../../../../Controller';
import Captcha from '../../../../../../libs/Captcha';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { session }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { captchaImageBase64, captchaNumber } = new Captcha();

    session.captcha = captchaNumber;

    return { captcha: captchaImageBase64 };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
