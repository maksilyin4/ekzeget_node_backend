import * as Joi from '@hapi/joi';

export default Joi.object({
    celebrates: Joi.array()
        .items({
            title: Joi.string().description('Название праздника'),
            date: Joi.string().description('дата/срок'),
        })
        .description('Праздники'),
    calendar_type: Joi.string().description('Тип календаря'),
}).required();
