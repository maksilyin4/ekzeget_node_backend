/**
 * @api {post} /site/easter/                             Вечная пасхалия
 * @apiVersion 0.1.0
 * @apiName  easter
 * @apiGroup Site
 *
 *
 * @apiDescription  Обратная связь
 * @apiPermission All
 *
 * @apiParam {Integer}    year                           Год в формате YYYY
 *
 * @apiSuccess {Object[]}   celebrates                     Праздники
 * @apiSuccess {String}     celebrates.title               Название праздника
 * @apiSuccess {String}     celebrates.date                дата/срок
 * @apiSuccess {String}     calendar_type                  Тип календаря
 *
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
