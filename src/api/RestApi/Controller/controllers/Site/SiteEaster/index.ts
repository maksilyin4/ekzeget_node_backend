import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import months from '../../../../../../const/strings/months/months';
import CacheManager from '../../../../../../managers/CacheManager';

type TEternalEaster = Joi.extractType<typeof outputJoiSchema>;
type esterStyle = 'old' | 'new';
type events =
    | 'mitarAndFariseya'
    | 'greatePost'
    | 'vaiyWeek'
    | 'assentionsGod'
    | 'holyTrinity'
    | 'petrovPost';

const handler = async (params: Joi.extractType<typeof inputJoiSchema>): Promise<TEternalEaster> => {
    const { year } = params;
    const eternalEaster: TEternalEaster = {
        celebrates: [],
        calendar_type:
            year < 1583 ? 'Даты по юлианскому календарю' : 'Даты по григорианскому календарю',
    };

    let easter: Date;
    let easterCatholic: Date;

    if (year < 1583) {
        easter = getEasterDate(year, 'old');
        easterCatholic = easter;
    } else {
        easter = getEasterDate(year, 'new');
        easterCatholic = getEasterCatholicForNewStyle(year);
    }

    const petrovPostBegin: Date = getEvent(easter, 'petrovPost');
    eternalEaster.celebrates.push(
        {
            title: 'Неделя о Мытаре и фарисее',
            date: dateDecorator(getEvent(easter, 'mitarAndFariseya')),
        },
        {
            title: 'Начало Великого поста',
            date: dateDecorator(getEvent(easter, 'greatePost')),
        },
        {
            title: 'Неделя Ваий (вербное воскресение)',
            date: dateDecorator(getEvent(easter, 'vaiyWeek')),
        },
        {
            title: 'Пасха',
            date: dateDecorator(easter),
        },
        {
            title: 'Вознесение Господне',
            date: dateDecorator(getEvent(easter, 'assentionsGod')),
        },
        {
            title: 'Пятидесятница (День Святой Троицы)',
            date: dateDecorator(getEvent(easter, 'holyTrinity')),
        },
        {
            title: 'Продолжительность Петрова поста',
            date: `${getPetrovPostDuration(year, petrovPostBegin)} (с ${dateDecorator(
                petrovPostBegin
            )})`,
        },
        {
            title: 'Католическая Пасха',
            date: dateDecorator(easterCatholic),
        },
        {
            title: 'Иудейская Пасха',
            date: dateDecorator(getPesah(year)),
        }
    );

    return eternalEaster;
};

/**
 * По новому стилю Пасха в 20-21 веке сдвигается на 13 дней от даты старого стиля.
 * Коррекция для остальных веков высчитывается как -1 за каждый век младше 19 и +1 за каждый век
 * старше 20, но при этом, каждые 4 века коррекция не используется. 1583 год, переход стаорого стиял на новый,
 * считается за 16 столетие.
 * @param year
 * @return количество дней для коррекции даты Пасхи по новому стилю
 */
function easterCorrectionForNewStyle(year: number) {
    const referenceNumber: number = year < 1600 ? 16 : Math.trunc(year / 100);

    let correction: number = 13;

    if (referenceNumber < 19) {
        correction -= 19 - referenceNumber;
    } else if (referenceNumber > 20) {
        correction += referenceNumber - 20;

        for (let cycle: number = 24; cycle <= referenceNumber; cycle += 4) {
            correction--;
        }
    }

    return correction;
}

/**
 *Реализиует формулу Гаусса для рассчета Пасхи по старому стилю:
 * ((2 * b + 4 + c + 6 + d + 6) % 7) + d
 * По потребности, накладывает декоратор для Пасхи по новому стилю
 * @param year
 * @param style указывает по новому или старому стилю требуется получить дату Пасхи
 * @return дата Пасхи в указаном году
 */
function getEasterDate(year: number, style: esterStyle) {
    const a: number = year % 19;
    const b: number = year % 4;
    const c: number = year % 7;
    const d: number = (19 * a + 15) % 30;
    const e: number = (2 * b + 4 * c + 6 * d + 6) % 7;

    let day: number = d + e;
    let month: number;

    if (day <= 9) {
        day += 22;
        month = 2;
    } else {
        day -= 9;
        month = 3;
    }

    const easterDate: Date = new Date(new Date().setFullYear(year, month, day));
    if (style === 'new') {
        easterDate.setDate(easterDate.getDate() + easterCorrectionForNewStyle(year));
    }

    return easterDate;
}

/**
 * Используется усложненная формула Гаусса:
 * @param year
 * @return date
 */
function getEasterCatholicForNewStyle(year: number) {
    const a: number = year % 19;
    const b: number = year % 4;
    const c: number = year % 7;
    const k: number = Math.trunc(year / 100);
    const p: number = Math.trunc((13 + 8 * k) / 25);
    const q: number = Math.trunc(k / 4);
    const M: number = (15 - p + k - q) % 30;
    const N: number = (4 + k - q) % 7;
    const d: number = (19 * a + M) % 30;
    const e: number = (2 * b + 4 * c + 6 * d + N) % 7;

    let day: number = e + d;
    let month: number = 0;

    if (day <= 9) {
        day += 22;
        month = 2;
    } else {
        day -= 9;
        month = 3;
    }

    if (d === 29 && e === 6) {
        day = 19;
        month = 3;
    }

    if (d === 28 && e === 6 && (11 * M) % 30 < 19) {
        day = 18;
        month = 3;
    }

    return new Date(year, month, day);
}

function getEvent(easter: Date, event: events) {
    const correctDate: Date = new Date(
        new Date().setFullYear(easter.getFullYear(), easter.getMonth(), easter.getDate())
    );

    const year: number = easter.getFullYear();
    let correction: number;

    switch (event) {
        case 'mitarAndFariseya':
            correction = year > 1980 && year < 2012 ? 69 : 70;
            correctDate.setDate(correctDate.getDate() - correction);
            break;
        case 'greatePost':
            correction = year > 1980 && year < 2012 ? 47 : 48;
            correctDate.setDate(correctDate.getDate() - correction);
            break;
        case 'vaiyWeek':
            correctDate.setDate(correctDate.getDate() - 7);
            break;
        case 'assentionsGod':
            correctDate.setDate(correctDate.getDate() + 39);
            break;
        case 'holyTrinity':
            correctDate.setDate(correctDate.getDate() + 49);
            break;
        case 'petrovPost':
            correctDate.setDate(correctDate.getDate() + 57);
            break;
    }
    return correctDate;
}

function getPetrovPostDuration(year: number, begin: Date) {
    begin.setHours(0);
    begin.setMinutes(0);
    const end: Date =
        year < 1583 ? new Date(new Date().setFullYear(year, 5, 29)) : new Date(year, 6, 12);

    const correction: number = year < 1583 ? 0 : easterCorrectionForPetrovPost(year);
    const duration: number =
        Math.round((end.getTime() - begin.getTime()) / (60 * 60 * 24 * 1000)) + correction;

    return daysDecorator(duration);
}

function daysDecorator(duration: number) {
    const titles: Array<string> = ['день', 'дня', 'дней'];
    const collationTable: Array<number> = [2, 0, 1, 1, 1, 2];

    return `${duration} ${
        titles[
            duration % 100 > 4 && duration % 100 < 20
                ? 2
                : collationTable[Math.min(duration % 10, 5)]
        ]
    }`;
}

/**
 * Сферическая коррекция в вакууме для продолжительности Петрова поста,
 * цифры из старых исходников, каждые 4 столетия коррекция не применяется.
 * @param year
 */
function easterCorrectionForPetrovPost(year: number) {
    let correction: number = 0;
    const century = year < 1600 ? 16 : Math.trunc(year / 100);

    if (year >= 1500 && year < 1900) {
        correction = century - 19;
    } else if (year >= 2100) {
        correction = century - 20;

        for (let cycle: number = 24; cycle <= century; cycle += 4) {
            correction--;
        }
    }

    return correction;
}

/**
 * Песах- иудейская пасха.
 * Рассчитывается очень странными формулами, формулы, константы и имена переменных взяты из старых исходников,
 * не смог интерпритировать и придумать другие имена
 * @param year
 */
function getPesah(year: number) {
    const a: number = (12 * year + 12) % 19;
    const b: number = year % 4;

    const mmm: number = 20.0955877 + 1.5542418 * a + 0.25 * b - 0.003177794 * year;
    const mm: number = Math.trunc(mmm);
    const m: number = mmm - mm;

    const c: number = (mm + 3 * year + 5 * b + 1) % 7;

    let day: number = mm;
    let month: number = 2;

    if (c === 2 || c === 4 || c === 6) {
        day = mm + 1;
        month = 2;
    }

    if (c == 1 && a > 6 && m >= 0.63287037) {
        day = mm + 2;
        month = 2;
    }
    if (c == 0 && a > 11 && m >= 0.89772376) {
        day = mm + 1;
        month = 2;
    }

    if (day > 31) {
        day -= 31;
        month = 3;
    }

    const pesah = new Date(new Date().setFullYear(year, month, day));
    const correction: number = year < 1583 ? 0 : easterCorrectionForNewStyle(year);
    pesah.setDate(pesah.getDate() + correction);

    return pesah;
}

function dateDecorator(date: Date) {
    return `${date.getDate()} ${months[date.getMonth()]}`;
}

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
