import * as Joi from '@hapi/joi';

export default Joi.object({
    year: Joi.number()
        .min(1)
        .max(9999)
        .description('Год в формате YYYY'),
}).required();
