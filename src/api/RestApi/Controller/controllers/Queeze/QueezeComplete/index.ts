import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import QueezeApplications from '../../../../../../entities/QueezeApplications';
import { ICookieQuiz } from '../../../../../../core/App/interfaces/IQuiz';

type TQueezeComplete = {
    result: string;
    cookie: ICookieQuiz;
};

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { cookie, session }: HandlerOptionalParams & { cookie: ICookieQuiz }
): Promise<TQueezeComplete> => {
    const lastActiveQuiz = await QueezeApplications.getLastActiveQuiz(
        cookie.queezes_codes,
        session.passport?.user
    );
    if (lastActiveQuiz) {
        await lastActiveQuiz.remove();
    }
    if ('queezes_codes' in cookie && cookie.queezes_codes.length !== 0) {
        const indexOfActiveQuizCode = cookie.queezes_codes.indexOf(lastActiveQuiz.code);
        if (indexOfActiveQuizCode !== -1) {
            cookie.queezes_codes.splice(indexOfActiveQuizCode, 1);
        }
    }

    return {
        result: 'OK',
        cookie: {
            queezes_codes: cookie.queezes_codes || [],
        },
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
