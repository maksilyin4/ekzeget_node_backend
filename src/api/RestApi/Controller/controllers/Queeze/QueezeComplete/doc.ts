/**
 * @api {get} /api/v1/queeze/complete Завершить викторину
 *
 * @apiVersion 0.1.0
 * @apiName QueezeComplete
 * @apiGroup Queeze
 *
 * @apiDescription Завершить викторину
 * @apiPermission All
 *
 * @apiSuccess {String}   result        всегда OK

 *
 * @apiError (500) {ApiException}       error Ошибка базы данных

 */
