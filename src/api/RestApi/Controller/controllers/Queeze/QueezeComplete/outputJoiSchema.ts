import * as Joi from '@hapi/joi';

export default Joi.object({
    result: Joi.string().description('всегда OK'),
}).required();
