import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import exceptionConstants from '../../../../../../const/strings/quiz/quizException';
import { IQuiz, ICookieQuiz } from '../../../../../../core/App/interfaces/IQuiz';
import Quiz from '../Quiz';
import QueezeApplications from '../../../../../../entities/QueezeApplications';

type TQueezeCreate = {
    queeze: Partial<IQuiz>;
    cookie: ICookieQuiz;
};

const handler = async (
    questionsFilter: Joi.extractType<typeof inputJoiSchema>,
    { cookie, session }: HandlerOptionalParams & { cookie: ICookieQuiz }
): Promise<TQueezeCreate> => {
    const { book_ids } = questionsFilter;
    if (!book_ids || book_ids.length === 0) {
        throw new ServerError(exceptionConstants.MISSING_BOOK, 400);
    }

    try {
        await QueezeApplications.closeAllUserQuizzes(cookie.queezes_codes, session.passport?.user);

        questionsFilter.question_id = Number(questionsFilter.question_id);
        questionsFilter.chapter_id = Number(questionsFilter.chapter_id);
        questionsFilter.plan_id = Number(questionsFilter.plan_id) || undefined;
        const quiz = await Quiz.startQuiz(questionsFilter, session.passport?.user);

        if (!session.passport) {
            if (!('queezes_codes' in cookie) || cookie.queezes_codes.length === 0) {
                cookie.queezes_codes = [];
            }
            cookie.queezes_codes.push(quiz.getCode());
        }

        return {
            queeze: quiz.getProperties(),
            cookie: {
                queezes_codes: cookie.queezes_codes || [],
            },
        };
    } catch (err) {
        throw new ServerError(exceptionConstants.CANT_CREATE_NEW + ' ' + err.message, 500);
    }
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
