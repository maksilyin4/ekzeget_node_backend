/**
 * @api {get} /api/v1/queeze/create/ Создать викторину
 *
 * @apiVersion 0.1.0
 * @apiName QueezeCreate
 * @apiGroup Queeze
 *
 * @apiDescription Создать викторину
 * @apiPermission All
 *
 * @apiParam {number}           is_related_to_text                        Викторина по тексту
 * @apiParam {number}           is_related_to_interpretations              Викторина по толкованиям
 * @apiParam {number}           chapter_id?                     Глава книги по которой взять вопросы
 * @apiParam {number}           question_id?                    Вопрос, который должен быть первым
 * @apiParam {Array[number]}    book_ids                       Массив книг, по которым првоодить викторину
 *
 * @apiSuccess {String}   status                        Статус ответа R или error
 * @apiSuccess {number}   user_id                       id пользователя
 * @apiSuccess {number}   id                            id
 * @apiSuccess {String}   code                          Уникальный код викторины
 * @apiSuccess {number}   current_question              Активный вопрос
 * @apiSuccess {String}   current_question_code         Текстовый код вопроса
 * @apiSuccess {Object[]} questions                     Вопросы в текущей попытке
 * @apiSuccess {number} questions.num                   Номер вопроса
 * @apiSuccess {String} questions.status                Статус вопроса- NA | NO | OK
 * @apiSuccess {String} questions.description           Пояснение к вопросу
 * @apiSuccess {number} questions.id                    id вопроса
 * @apiSuccess {String} questions.title                 Текст вопроса
 * @apiSuccess {String} questions.interpretations       Толкования
 * @apiSuccess {String} questions.code                  Текстовый код вопроса
 * @apiSuccess {Object[]} questions.answers             Ответы
 * @apiSuccess {number} questions.answers.id            id ответа
 * @apiSuccess {number} questions.answers.question_id   id вопроса, к которому принадлежит ответ
 * @apiSuccess {String} questions.answers.text          Текст ответа
 * @apiSuccess {Boolean} questions.answers.is_correct   Правильный ли ответ
 *
 * @apiError (500) {ApiException}       error Ошибка базы данных

 */
