import * as Joi from '@hapi/joi';

export default Joi.object({
    plan_id: Joi.number()
        .integer()
        .positive()
        .optional(),
    is_related_to_text: Joi.string()
        .valid('1', '0')
        .description('Викторина по тексту')
        .required(),
    is_related_to_interpretations: Joi.string()
        .valid('1', '0')
        .description('Викторина по толкованиям')
        .required(),
    chapter_id: Joi.number()
        .integer()
        .description('Номер главы, из котороый брать вопросы (не обязательный)'),
    question_id: Joi.number()
        .integer()
        .description('Вопрос, который должен быть первым (не обязательный)'),
    book_ids: Joi.array()
        .items(Joi.string())
        .required(),
});
