import * as Joi from '@hapi/joi';

export default Joi.object({
    queeze: Joi.object({
        status: Joi.string()
            .valid('R', 'error')
            .description('Статус ответа'),
        user_id: Joi.number()
            .allow(null)
            .description('user_id'),
        plan_id: Joi.number()
            .allow(null)
            .description('plan_id'),
        id: Joi.number().description('id'),
        code: Joi.string().description('code'),
        current_question: Joi.number().description('Активный вопрос'),
        current_question_code: Joi.string().description('Текстовый код вопроса'),
        wrong: Joi.string()
            .allow('', null)
            .description('Путь к аудиофайлу неверного ответа'),
        introduction: Joi.string()
            .allow('', null)
            .description('Путь к аудиофайлу вступления'),
        questions: Joi.array()
            .items({
                num: Joi.number().description('Номер вопроса'),
                status: Joi.string()
                    .valid('NA', 'NO', 'OK')
                    .description('Статус вопроса- NA | NO | OK'),
                description: Joi.string().description('Пояснение к вопросу'),
                id: Joi.number().description('id вопроса'),
                verse: Joi.object().description('стих'),
                title: Joi.string().description('Текст вопроса'),
                interpretations: Joi.array().description('Толкования'),
                code: Joi.string().description('Текстовый код вопроса'),
                mediaQuestion: Joi.string()
                    .allow(null, '')
                    .description('Озвучка для вопроса'),
                mediaAnswer: Joi.string()
                    .allow(null, '')
                    .description('Озвучка для ответа'),
                answers: Joi.array()
                    .items({
                        id: Joi.number().description('id ответа'),
                        question_id: Joi.number().description(
                            'id вопроса, к которому принадлежит ответ'
                        ),
                        text: Joi.string().description('Текст ответа'),
                        is_correct: Joi.boolean().description('Правильный ли ответ'),
                    })
                    .description('Ответы'),
            })
            .description('Вопросы'),
    }),
}).required();
