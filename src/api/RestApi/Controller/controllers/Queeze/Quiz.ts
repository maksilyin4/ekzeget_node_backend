import {
    IQuiz,
    IClientQuestion,
    TQuestionStatus,
    TQuizStatus,
    IQuestionMediaAndAll, TQuizCriteria,
} from '../../../../../core/App/interfaces/IQuiz';
import QueezeApplications from '../../../../../entities/QueezeApplications';
import Verse from '../../../../../entities/Verse';
import QueezeQuestions from '../../../../../entities/QueezeQuestions';
import QuizSubmission from '../../../../../entities/QuizSubmission';
import QueezeAnswers from '../../../../../entities/QueezeAnswers';
import quizConstants from '../../../../../const/numbers/quiz/quizConstants';
import Book from '../../../../../entities/Book';
import Config from '../../../../../entities/Config';
import inputJoiSchema from './QueezeCreate/inputJoiSchema';
import * as Joi from "@hapi/joi";
import { ServerError } from '../../../../../libs/ErrorHandler';
import exceptionConstants from "../../../../../const/strings/quiz/quizException";

/**
 * user_id всегда null, пока не будет авторизации. Ни на что не влияет.
 */
export default class Quiz {
    private readonly currentQuiz: Partial<IQuiz>;

    private constructor(quiz: Partial<IQuiz>) {
        this.currentQuiz = quiz;
    }

    static async startQuiz(questionsFilter: Joi.extractType<typeof inputJoiSchema>, userId?: number): Promise<Quiz> {
        const { wrong, introduction } = await Config.getPathToAudioForQuiz();

        const newQuiz: IQuiz = {
            status: 'R',
            user_id: userId,
            plan_id: questionsFilter.plan_id,
            id: 0,
            code: '',
            current_question: 1,
            current_question_code: '',
            questions:
                questionsFilter.question_id && questionsFilter.chapter_id
                    ? await QueezeQuestions.getQuestionsWithAnswersByBookAndChapter(questionsFilter)
                    : await QueezeQuestions.getRandQuestions(questionsFilter),
            wrong,
            introduction,
        };
        const types: TQuizCriteria['types'] = [];
        if (questionsFilter.is_related_to_text === '1') {
            types.push('T');
        }
        if (questionsFilter.is_related_to_interpretations === '1') {
            types.push('I');
        }
        [newQuiz.id, newQuiz.code, newQuiz.plan_id] = await QueezeApplications.setNewQuiz(
            userId,
            {
                types,
                books: questionsFilter.book_ids.map(Number),
                chapter_id: questionsFilter.chapter_id,
            },
            questionsFilter.plan_id,
        );
        newQuiz.current_question_code = newQuiz.questions[0].code;

        await QuizSubmission.saveQuiz(newQuiz);

        return new Quiz(newQuiz);
    }

    static async continueQuiz(quizId: number): Promise<Quiz> {
        const { wrong, introduction } = await Config.getPathToAudioForQuiz();
        const currentQuiz: IQuiz = {
            status: 'R',
            user_id: null,
            id: quizId,
            plan_id: null,
            code: null,
            current_question: 1,
            current_question_code: null,
            created_at: null,
            updated_at: null,
            questions: null,
            wrong,
            introduction,
        };

        const oldQuiz = await QueezeApplications.getQuizById(quizId);

        currentQuiz.plan_id = oldQuiz.planId;
        currentQuiz.user_id = oldQuiz.userId;
        currentQuiz.code = oldQuiz.code;
        currentQuiz.created_at = oldQuiz.createdAt;
        currentQuiz.updated_at = oldQuiz.updatedAt;
        currentQuiz.questions = await this.createQuestionsListByQuizId(currentQuiz.id);

        for (const item of currentQuiz.questions) {
            if (item.status === 'NA') {
                currentQuiz.current_question = item.num;
                currentQuiz.current_question_code = item.code;
                break;
            }
        }

        return new Quiz(currentQuiz);
    }

    static async answerToQuestion(
        questionId: string,
        answerId: string,
        quizId: number
    ): Promise<Quiz> {
        const currentQuiz: Partial<IQuiz> = {
            status: 'R',
            id: quizId,
            questions: await QuizSubmission.getQuestionsByQuizId(quizId),
        };

        for (const item of currentQuiz.questions) {
            if (item.id === Number(questionId)) {
                const correctAnswer = await QueezeAnswers.isCorrect(Number(answerId));
                item.status = correctAnswer ? 'OK' : 'NO';
                await QuizSubmission.updateStatus(
                    currentQuiz.id,
                    Number(questionId),
                    item.status,
                    answerId
                );

                if (item.num === quizConstants.QUESTIONS_COUNT) {
                    await QueezeApplications.changeQuizStatus(currentQuiz.id, 'C');
                }
                break;
            }
        }

        return new Quiz(currentQuiz);
    }

    static async getStatisticsOrFail(
        quizCodes?: string[],
        userId?: number,
        planId?: number,
    ): Promise<Partial<IQuiz>[]> {
        const quizzes = await QueezeApplications.getUserQuizzes(quizCodes, userId, planId);
        const statisticsPromise = quizzes.map(
            async (anotherQuiz): Promise<Partial<Quiz>> => {
                const quiz = {
                    status: anotherQuiz.status as TQuizStatus,
                    id: anotherQuiz.id,
                    plan_id: anotherQuiz.planId,
                    created_at: anotherQuiz.createdAt,
                    updated_at: anotherQuiz.updatedAt,
                    description: `${anotherQuiz.description} / список книг:`,
                    current_question: null,
                    current_question_code: null,
                    questions: await QuizSubmission.getQuestionsByQuizId(anotherQuiz.id),
                    meta: {
                        total: 0,
                        answered: 0,
                        correct: 0,
                    },
                    books: [],
                };

                quiz.meta.total = quiz.questions.length;
                const questionsIds: number[] = [];

                for (const question of quiz.questions) {
                    question.code = await QueezeQuestions.getCodeById(question.id);
                    if (question.status == 'NA' && quiz.current_question === null) {
                        quiz.current_question = question.num;
                        quiz.current_question_code = question.code;
                    }

                    if (question.status != 'NA') {
                        quiz.meta.answered++;
                        if (question.status === 'OK') {
                            quiz.meta.correct++;
                        }
                    }
                    questionsIds.push(question['id']);
                    delete question['id'];
                }
                quiz.books = await Book.getRelationBooksByQuestIds(questionsIds);
                return new Quiz(quiz);
            }
        );

        const statistics = (await Promise.all(statisticsPromise))
            .filter(item => item !== undefined)
            .map(item => {
                return item.getProperties();
            });

        if (!statistics.length) {
            throw new ServerError(exceptionConstants.NO_QUIZ, 404);
        }
        return statistics;
    }

    static async getLastActiveQuizOrFail(
        codes: string[] = [],
        userId?: number
    ): Promise<QueezeApplications> {
        const lastQuiz = await QueezeApplications.getLastActiveQuiz(codes, userId);
        if (!lastQuiz) {
            throw new ServerError(exceptionConstants.NO_QUIZ, 404);
        }
        return lastQuiz;
    }

    static async getQuestionsForBibleChapter(
        bookId: number,
        chapterId: number,
        type: string
    ): Promise<IQuestionMediaAndAll[]> {
        const questionsChunk = await QueezeQuestions.getQuestionsByBookAndChapter(
            bookId,
            chapterId,
            type
        );

        return this.getVerseInfoForQuestionList(questionsChunk);
    }

    static async getQuestionForVerse(
        verseId: number,
        type: string
    ): Promise<Array<IQuestionMediaAndAll>> {
        const question = await QueezeQuestions.findOne({
            select: ['id', 'title', 'code'],
            where: { verseId, type },
        });
        if (!question) {
            return [];
        }
        return [{ ...question, verse: await Verse.getVerseWithBookInfo(verseId) }];
    }

    static async getAllQuestions(perPage: number, page: number): Promise<IQuestionMediaAndAll[]> {
        const questionsChunk = await QueezeQuestions.getAllQuestionsByPaginator(perPage, page);

        return await this.getVerseInfoForQuestionList(questionsChunk);
    }

    static async getOneQuestion(questionCode): Promise<IQuestionMediaAndAll> {
        const questionChunk = await QueezeQuestions.getOneQuestion(questionCode);

        return {
            id: questionChunk.id,
            verseId: questionChunk.verseId,
            type: questionChunk.type,
            code: questionCode,
            title: questionChunk.title,
            interpretations: questionChunk.interpretations,
            description: questionChunk.description,
            active: questionChunk.active,
            verse: await Verse.getVerseWithBookInfo(questionChunk.verseId),
            answers: questionChunk.answers,
        };
    }

    private static async getVerseInfoForQuestionList(questions: Partial<QueezeQuestions>[]) {
        const questionsWithVerses = [];
        for (const question of questions) {
            questionsWithVerses.push({
                id: question.id,
                title: question.title,
                code: question.code,
                verse: await Verse.getVerseWithBookInfo(question['verse_id']),
            });
        }
        return questionsWithVerses;
    }

    private static async createQuestionsListByQuizId(quizId: number): Promise<IClientQuestion[]> {
        const questionsList = await QuizSubmission.getQuestionsByQuizId(quizId);

        const promises = questionsList.map(
            async (item): Promise<IClientQuestion> => {
                const question: IClientQuestion = await QueezeQuestions.getQuestionById(item.id);
                question.num = item.num;
                question.status = item.status as TQuestionStatus;
                question.submittedAnswerId = item.submittedAnswerId;

                return question;
            }
        );

        return await Promise.all(promises);
    }

    public getProperties(): Partial<IQuiz> {
        return this.currentQuiz;
    }

    public getCode() {
        return this.currentQuiz.code;
    }
}
