import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import {
    ICookieQuiz,
    IQuestionMediaAndAll,
    IQuiz,
} from '../../../../../../core/App/interfaces/IQuiz';
import Quiz from '../Quiz';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import quizException from '../../../../../../const/strings/quiz/quizException';
import QueezeApplications from '../../../../../../entities/QueezeApplications';

type TQuestion = {
    question: IQuestionMediaAndAll;
    exists: false;
};

type TActiveQuiz = {
    current: Partial<IQuiz>;
    exists: true;
};

// For compatibility reasons, an active quiz is returned if the question is associated with it. This allows client to load game controls
const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { cookie, session }: HandlerOptionalParams & { cookie: ICookieQuiz }
): Promise<TQuestion | TActiveQuiz> => {
    try {
        const activeQuizSummary = await QueezeApplications.getLastActiveQuiz(
            cookie.queezes_codes,
            session.passport?.user
        );
        if (activeQuizSummary) {
            const activeQuiz = (await Quiz.continueQuiz(activeQuizSummary.id)).getProperties();

            const questionOfActiveQuiz = activeQuiz.questions.find(
                question => question.code === params.questionCode
            );
            if (questionOfActiveQuiz) {
                return {
                    current: {
                        ...activeQuiz,
                        current_question_code: params.questionCode,
                        current_question: questionOfActiveQuiz.num,
                    },
                    exists: true,
                };
            }
        }

        return {
            question: await Quiz.getOneQuestion(params.questionCode),
            exists: false,
        };
    } catch (err) {
        throw new ServerError(quizException.CANT_GET_QUESTION + ' ' + err.message, 400);
    }
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
