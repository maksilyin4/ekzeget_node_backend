import * as Joi from '@hapi/joi';

export default Joi.object({
    questionCode: Joi.string().description('Код вопроса'),
}).required();
