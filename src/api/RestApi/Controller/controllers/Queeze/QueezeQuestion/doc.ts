/**
 * @api {get} /api/v1/queeze/question/ Просмотр одного вопроса
 *
 * @apiVersion 0.1.0
 * @apiName QueezeQuestion
 * @apiGroup Queeze
 *
 * @apiDescription Просмотр одного вопроса
 * @apiPermission All
 *
 * @apiParam {number}       questionCode                    Код выбранного вопроса
 *
 * @apiSuccess {boolean}    exist                           ? всегда false
 * @apiSuccess {Object[]}   questions                       Вопросы для текущей главы
 * @apiSuccess {number}     question.id                     id
 * @apiSuccess {number}     question.verse_id               verse_id
 * @apiSuccess {String}     questions.type                  Тип вопроса (текст или толкование)
 * @apiSuccess {String}     questions.code                  Текстовый код вопроса
 * @apiSuccess {String}     questions.title                 Текст вопроса
 * @apiSuccess {String}     questions.interpretations       Толкования
 * @apiSuccess {String}     questions.description           Описание
 * @apiSuccess {String}     questions.active                Активный ли вопрос
 * @apiSuccess {Object}     questions.verse                 Стихи
 * @apiSuccess {number}     questions.verse.id              id
 * @apiSuccess {number}     questions.verse.number          номер стиха в главе
 * @apiSuccess {number}     questions.verse.book_id         id связанной книги
 * @apiSuccess {String}     questions.verse.text            текст стиха (может быть пустым)
 * @apiSuccess {number}     questions.verse.chapter         глава к которой относится стих (и вопрос)
 * @apiSuccess {number}     questions.verse.chapter_id      id главы к которой относится стих (и вопрос)
 * @apiSuccess {String}     questions.verse.book_code       Текстовый код связанной книги
 * @apiSuccess {String}     questions.verse.short           Сокращенное название связанной книги
 * @apiSuccess {Object[]} questions.answers             Ответы
 * @apiSuccess {number} questions.answers.id            id ответа
 * @apiSuccess {number} questions.answers.question_id   id вопроса, к которому принадлежит ответ
 * @apiSuccess {String} questions.answers.text          Текст ответа
 * @apiSuccess {Boolean} questions.answers.is_correct   Правильный ли ответ
 *
 * @apiError (500) {ApiException}       error Ошибка базы данных

 */
