/**
 * @api {get} /api/v1/queeze/current/ Получить текущую попытку
 *
 * @apiVersion 0.1.0
 * @apiName GetQueezeCurrent
 * @apiGroup Queeze
 *
 * @apiDescription Получить текущую попытку
 * @apiPermission All
 *
 * @apiSuccess {String}   status                        Статус ответа R или error
 * @apiSuccess {number}   id                            id Викторины
 * @apiSuccess {number}   user_id                       user id
 * @apiSuccess {String}   created_at"
 * @apiSuccess {String}   updated_at
 * @apiSuccess {String}   code                          Уникальный код викторины
 * @apiSuccess {number} current_question                Номер текущего вопроса
 * @apiSuccess {String} current_question_code           Код текущего вопроса
 * @apiSuccess {Object[]} questions                     Вопросы в текущей попытке
 * @apiSuccess {number} questions.num                   Номер вопроса
 * @apiSuccess {String} questions.status                Статус вопроса- NA | NO | OK
 * @apiSuccess {String} questions.description           Пояснение к вопросу
 * @apiSuccess {number} questions.id                    id вопроса
 * @apiSuccess {String} questions.title                 Текст вопроса
 * @apiSuccess {String} questions.interpretations       Толкования
 * @apiSuccess {String} questions.code                  Текстовый код вопроса
 * @apiSuccess {Object[]} questions.answers             Ответы
 * @apiSuccess {number} questions.answers.id            id ответа
 * @apiSuccess {number} questions.answers.question_id   id вопроса, к которому принадлежит ответ
 * @apiSuccess {String} questions.answers.text          Текст ответа
 * @apiSuccess {Boolean} questions.answers.is_correct   Правильный ли ответ
 *

 * @apiError (500) {ApiException}       error Ошибка базы данных

 */

