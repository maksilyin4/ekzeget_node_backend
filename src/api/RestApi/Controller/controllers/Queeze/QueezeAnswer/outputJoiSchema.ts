import * as Joi from '@hapi/joi';

export default Joi.object({
    queeze: Joi.object({
        status: Joi.string()
            .valid('R', 'error', 'C', 'P')
            .description('Статус ответа'),
        id: Joi.number().description('user викторины'),
        questions: Joi.array()
            .items({
                num: Joi.number().description('Номер вопроса'),
                status: Joi.string()
                    .valid('NA', 'NO', 'OK')
                    .description('Статус вопроса- NA | NO | OK'),
                submittedAnswerId: Joi.number().allow(null),
                id: Joi.number().description('id вопроса'),
            })
            .description('Вопросы'),
    }),
}).required();
