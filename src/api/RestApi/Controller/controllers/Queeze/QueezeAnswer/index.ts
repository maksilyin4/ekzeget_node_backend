import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { ICookieQuiz, IQuiz } from '../../../../../../core/App/interfaces/IQuiz';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import exceptionConstants from '../../../../../../const/strings/quiz/quizException';
import Quiz from '../Quiz';

type TQueezeCurrent = { queeze: Partial<IQuiz> };

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { cookie, session }: HandlerOptionalParams & { cookie: ICookieQuiz }
): Promise<TQueezeCurrent> => {
    const lastActiveQuiz = await Quiz.getLastActiveQuizOrFail(
        cookie.queezes_codes,
        session.passport?.user
    );

    try {
        const quizesAnswer = await Quiz.answerToQuestion(
            params.questionId,
            params.answerId,
            lastActiveQuiz.id
        );
        return { queeze: quizesAnswer.getProperties() };
    } catch (err) {
        throw new ServerError(exceptionConstants.CANT_PROCESS_ANSWER + ' ' + err.message, 404);
    }
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
