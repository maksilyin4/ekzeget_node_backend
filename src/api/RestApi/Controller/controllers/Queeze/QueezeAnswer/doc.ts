/**
 * @api {get} /queeze/answer/:questionId/:answerId Ответ на вопрос викторины
 *
 * @apiVersion 0.1.0
 * @apiName QueezeAnswer
 * @apiGroup Queeze
 *
 * @apiDescription Ответ на вопрос викторины
 * @apiPermission All
 *
 * @apiParam {number}           questionId
 * @apiParam {number}           answerId
 *
 * @apiSuccess {String}   status                        Статус викторины R | C | P
 * @apiSuccess {number}   id                            id викторины
 * @apiSuccess {Object[]} questions                     Вопросы в текущей попытке
 * @apiSuccess {number} questions.num                   Номер вопроса
 * @apiSuccess {String} questions.status                Статус вопроса- NA | NO | OK
 * @apiSuccess {number} questions.id                    id вопроса
 *
 * @apiError (500) {ApiException}       error Ошибка базы данных

 */
