import * as Joi from '@hapi/joi';

export default Joi.object({
    questionId: Joi.string()
        .regex(/^[0-9]+$/)
        .disallow(null, '')
        .description('Викторина по тексту'),
    answerId: Joi.string()
        .regex(/^[0-9]+$/)
        .disallow(null, '')
        .description('Ответ на вопрос'),
}).required();
