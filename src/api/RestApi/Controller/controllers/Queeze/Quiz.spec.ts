import QueezeQuestions from '../../../../../entities/QueezeQuestions';
import Verse from '../../../../../entities/Verse';
import Quiz from './Quiz';
import { testGetQuestionForVerse } from './helpers/ioDataForTests';

const findOneSpy = jest.spyOn(QueezeQuestions, 'findOne');
const getVerseWithBookInfoSpy = jest.spyOn(Verse, 'getVerseWithBookInfo');

afterAll(() => {
    jest.restoreAllMocks();
});

describe('getQuestionForVerse', () => {
    test('have a question for verse', async () => {
        findOneSpy.mockResolvedValueOnce(testGetQuestionForVerse.findOneResolve);

        getVerseWithBookInfoSpy.mockResolvedValueOnce(
            testGetQuestionForVerse.getVerseWithBookInfoResolve
        );

        const question = await Quiz.getQuestionForVerse(123);

        expect(question).toEqual(testGetQuestionForVerse.result);
    });
    test('dont have a question for verse', async () => {
        findOneSpy.mockResolvedValueOnce(undefined);

        const question = await Quiz.getQuestionForVerse(123);
        expect(question).toEqual([]);
    });
});
