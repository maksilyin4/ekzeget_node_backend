import QueezeQuestions from '../../../../../../entities/QueezeQuestions';
import { IVerseForMediaAndAll } from '../../../../../../core/App/interfaces/IQuiz';

export const testGetQuestionForVerse = {
    findOneResolve: {
        id: 1,
        title: 'test question',
        code: 'test code',
    } as QueezeQuestions,

    getVerseWithBookInfoResolve: {
        book_code: 'evangelie-ot-matfea',
        book_id: 55,
        chapter: 1,
        chapter_id: 1113,
        id: 24994,
        number: 6,
        short: 'Мф. 1:6',
        text: '',
    } as IVerseForMediaAndAll,

    result: [
        {
            id: 1,
            title: 'test question',
            code: 'test code',
            verse: {
                book_code: 'evangelie-ot-matfea',
                book_id: 55,
                chapter: 1,
                chapter_id: 1113,
                id: 24994,
                number: 6,
                short: 'Мф. 1:6',
                text: '',
            },
        },
    ],
};

export const testQueezeQuestionList = {
    resultQuestionForVerse: {
        questions: [
            {
                id: 1,
                title: 'test question',
                code: 'test code',
                verse: {
                    book_code: 'evangelie-ot-matfea',
                    book_id: 55,
                    chapter: 1,
                    chapter_id: 1113,
                    id: 24994,
                    number: 6,
                    short: 'Мф. 1:6',
                    text: '',
                },
            },
        ],
        totalCount: 1,
    },
};
