import * as Joi from '@hapi/joi';

export default Joi.object({
    pageNumber: Joi.string()
        .regex(/^[0-9]+$/)
        .allow(null, '')
        .description('Номер страницы'),
    plan_id: Joi.number()
        .min(0).integer()
        .allow(null),
}).required();
