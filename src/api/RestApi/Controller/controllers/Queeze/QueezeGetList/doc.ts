/**
 * @api {get} /api/v1/queeze/list/ Список вопросов к библи (с пагинацией)
 *
 * @apiVersion 0.1.0
 * @apiName GetQueezeList
 * @apiGroup Queeze
 *
 * @apiDescription Списк вопросов викторины (с пагинацией)
 * @apiPermission All
 *
 * @apiParam {Object[]}             queezes                         массив объектов викторин
 * @apiParam {String}               queezes.status                  Статус викторины (R, P, C)
 * @apiParam {Integer}              queezes.id                      id викторины
 * @apiParam {Date}                 queezes.created_at              когда создана
 * @apiParam {Date}                 queezes.updateted_at            когда модифицирована
 * @apiParam {Integer}              queezes.current_question        текущий вопрос для викторины
 * @apiParam {String}               queezes.current_question_code   код текущего вопроса
 * @apiParam {String}               queezes.description             описание викторины (по тексту/по толкованию список книг: (пока только Новый завет))

 * @apiParam {Object[]}             queezes.questions               вопросы к викторине
 * @apiParam {Integer}              queezes.questions.num           порядковый номер вопроса
 * @apiParam {String}               queezes.questions.status        статус вопроса NA OK NO
 * @apiParam {String}               queezes.questions.code          текстовый код вопроса

 * @apiParam {Object[]}             queezes.meta                    количесво вопросов и правильность ответов
 * @apiParam {Integer}              queezes.meta.total              вопросов всего
 * @apiParam {Integer}              queezes.meta.answered           всего ответов
 * @apiParam {Integer}              queezes.meta.correct            всего правильных ответов

 * @apiParam {Object[]}             queezes.books                   список книг, из которых взяты вопросы
 * @apiParam {Integer}              queezes.books.id                id книги
 * @apiParam {String}               queezes.books.title             названние книги
 * @apiParam {String}               queezes.books.code              текстовый код книги

 * @apiParam {Object[]}             pages                           настройки пагинатора'
 * @apiParam {String}               pages.pageParam                 ? (page)
 * @apiParam {String}               pages.pageSizeParam             ? (per-page)
 * @apiParam {Boolean}              pages.forcePageParam            ? (true)
 * @apiParam {?}                    pages.route                     ? (null)
 * @apiParam {?}                    pages.params                    ? (null)
 * @apiParam {?}                    pages.urlManager                ? (null)
 * @apiParam {Boolean}              pages.validatePage              ? (true)
 * @apiParam {Integer}              pages.totalCount                количество викторин
 * @apiParam {Integer}              pages.defaultPageSize           сколько максимум викторин на странице
 * @apiParam {Integer[]}            pages.pageSizeLimit             возможное количество викторин на странице
 * @apiParam {Integer}              pages.pageSizeLimit[0]          минимум
 * @apiParam {Integer}              pages.pageSizeLimit[1]          максимум
 *
 * @return Object
 */
