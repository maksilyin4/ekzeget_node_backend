import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, {HandlerOptionalParams} from '../../../../Controller';
import { ICookieQuiz } from '../../../../../../core/App/interfaces/IQuiz';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import exceptionConstants from '../../../../../../const/strings/quiz/quizException';
import Quiz from '../Quiz';
import quizConstants from '../../../../../../const/numbers/quiz/quizConstants';

type TFinalList = Joi.extractType<typeof outputJoiSchema>;

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { cookie, session }: HandlerOptionalParams & { cookie: ICookieQuiz }
): Promise<TFinalList> => {
    try {
        const quizStatistics = await Quiz.getStatisticsOrFail(
            paginator(cookie.queezes_codes, Number(params.pageNumber)),
            session.passport?.user,
            Number(params.plan_id),
        );

        return {
            queezes: quizStatistics,
            pages: {
                pageParam: 'page',
                pageSizeParam: 'per-page',
                forcePageParam: true,
                route: null,
                params: null,
                urlManager: null,
                validatePage: true,
                totalCount: quizStatistics.length,
                defaultPageSize: quizConstants.PAGINATOR_QUIZ_ON_PAGE,
                pageSizeLimit: [1, 50],
            },
        };
    } catch (err) {
        throw new ServerError(exceptionConstants.NO_QUIZ + ' ' + err.message, 500);
    }

    function paginator(quizCodes: string[] = [], pageNumber: number) {
        const quizOnPage = quizConstants.PAGINATOR_QUIZ_ON_PAGE;
        if (
            Math.ceil(quizCodes.length / quizOnPage) < pageNumber ||
            pageNumber <= 0 ||
            isNaN(pageNumber)
        ) {
            pageNumber = 1;
        }

        if (quizCodes.length <= quizOnPage) {
            return quizCodes;
        } else {
            let start = quizCodes.length - quizOnPage * pageNumber;
            let end = 0;
            if (start < 0) {
                end = quizOnPage - start;
                start = 0;
            } else {
                end = start + quizOnPage;
            }

            return quizCodes.slice(start, end);
        }
    }
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
