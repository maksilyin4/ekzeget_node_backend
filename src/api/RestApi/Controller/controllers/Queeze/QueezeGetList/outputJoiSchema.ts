import * as Joi from '@hapi/joi';
import quizConstants from '../../../../../../const/numbers/quiz/quizConstants';

export default Joi.object({
    queezes: Joi.array()
        .items({
            status: Joi.string()
                .valid('R', 'P', 'C')
                .description('Статус викторины'),
            id: Joi.number().description('id викторины'),
            plan_id: Joi.number().allow(null).description('id плана'),
            created_at: Joi.date(),
            updated_at: Joi.date()
                .allow(null)
                .description('updated_at'),
            current_question: Joi.number()
                .allow(null)
                .description('Номер текущего вопроса'),
            current_question_code: Joi.string()
                .allow(null, '')
                .description('Код текущего вопроса'),
            description: Joi.string().description('Описание викторины'),
            questions: Joi.array()
                .items({
                    num: Joi.number().description('Номер вопроса'),
                    status: Joi.string()
                        .valid('NA', 'NO', 'OK')
                        .description('Статус вопроса- NA | NO | OK'),
                    submittedAnswerId: Joi.number().allow(null),
                    code: Joi.string().description('Текстовый код вопроса'),
                })
                .description('Вопросы'),

            meta: Joi.object({
                total: Joi.number()
                    .min(1)
                    .description('Всего вопросов'),
                answered: Joi.number()
                    .min(0)
                    .description('Всего ответов'),
                correct: Joi.number()
                    .min(0)
                    .description('Всего правильных ответов'),
            }).description('Количество вопросов и правильность ответов'),

            books: Joi.array()
                .items({
                    id: Joi.number().description('id книги'),
                    title: Joi.string()
                        .allow(null, '')
                        .description('Название книги'),
                    code: Joi.string().description('Название книги'),
                })
                .description('Список книг, из которых взяты вопросы'),
        })
        .description('Викторины'),
    pages: Joi.object({
        pageParam: Joi.string().description('? (page)'),
        pageSizeParam: Joi.string().description('? (per-page)'),
        forcePageParam: Joi.boolean().description('? (true)'),
        route: Joi.string()
            .allow(null, '')
            .description('? (null)'),
        params: Joi.string()
            .allow(null, '')
            .description('? (null)'),
        urlManager: Joi.string()
            .allow(null, '')
            .description('? (null)'),
        validatePage: Joi.boolean().description('? (true)'),
        totalCount: Joi.number()
            .min(1)
            .description('количество викторин'),
        defaultPageSize: Joi.number()
            .min(1)
            .description('сколько викторин на странице'),
        pageSizeLimit: Joi.array()
            .items(Joi.number().min(1))
            .length(2)
            .description('возможное количество викторин на странице'),
    }).description('Настройки пагинатора'),
}).required();
