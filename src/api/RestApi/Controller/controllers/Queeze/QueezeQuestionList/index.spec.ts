import QueezeQuestions from '../../../../../../entities/QueezeQuestions';
import Verse from '../../../../../../entities/Verse';
import QueezeQuestionList from './index';
import { testGetQuestionForVerse, testQueezeQuestionList } from '../helpers/ioDataForTests';
import quizException from '../../../../../../const/strings/quiz/quizException';

const findOneSpy = jest.spyOn(QueezeQuestions, 'findOne');
const getVerseWithBookInfoSpy = jest.spyOn(Verse, 'getVerseWithBookInfo');

afterAll(() => {
    jest.restoreAllMocks();
});

describe('get one question fo verse', () => {
    test('have a question for verse', async () => {
        findOneSpy.mockResolvedValueOnce(testGetQuestionForVerse.findOneResolve);

        getVerseWithBookInfoSpy.mockResolvedValueOnce(
            testGetQuestionForVerse.getVerseWithBookInfoResolve
        );

        const response = await QueezeQuestionList.handle({ verse_id: 123, type: 'T' });

        expect(response).toEqual(testQueezeQuestionList.resultQuestionForVerse);
    });
    test('dont have a question for verse', async () => {
        findOneSpy.mockResolvedValueOnce(undefined);

        const response = await QueezeQuestionList.handle({ verse_id: 123, type: 'T' });

        expect(response).toEqual({ questions: [], totalCount: 0 });
    });

    test('error', async () => {
        findOneSpy.mockRejectedValueOnce(new Error('Test error'));

        try {
            const response = await QueezeQuestionList.handle({ verse_id: 123, type: 'T' });
        } catch (err) {
            expect(err.message).toBe(
                quizException.CANT_GET_QUESTION_FOR_BIBLE_CHAPTER + ' Test error'
            );
            expect(err.statusCode).toBe(500);
        }
    });
});
