import * as Joi from '@hapi/joi';

export default Joi.object({
    questions: Joi.array().items({
        id: Joi.number().description('id вопроса'),
        code: Joi.string().description('Текстовый код вопроса'),
        title: Joi.string().description('Текст вопроса'),
        verse: Joi.object({
            id: Joi.number().description('id стиха'),
            number: Joi.number().description('номер стиха в главе'),
            book_id: Joi.number().description('id связанной книги'),
            text: Joi.string()
                .allow(null, '')
                .description('текст стиха'),
            chapter: Joi.number().description('глава книги к которой относится стих'),
            chapter_id: Joi.number().description('id главы книги к которой относится стих'),
            book_code: Joi.string().description('текстовый код связанной книги'),
            short: Joi.string().description('сокращенное название связанной книги'),
        }),
    }),
    totalCount: Joi.number()
        .allow(null, 0)
        .description('количество связанных вопросов'),
}).required();
