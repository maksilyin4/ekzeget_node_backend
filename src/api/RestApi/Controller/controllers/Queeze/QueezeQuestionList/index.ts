import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import { IQuestionMediaAndAll } from '../../../../../../core/App/interfaces/IQuiz';
import Quiz from '../Quiz';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import quizException from '../../../../../../const/strings/quiz/quizException';
import QueezeQuestions from '../../../../../../entities/QueezeQuestions';

type TQuestionList = {
    questions: IQuestionMediaAndAll[];
    totalCount: number;
};

const handler = async (params: Joi.extractType<typeof inputJoiSchema>): Promise<TQuestionList> => {
    const perPage = Number(params['per-page']);
    const { book_id, chapter_id, page, verse_id } = params;

    const result: TQuestionList = {
        questions: [],
        totalCount: 0,
    };
    try {
        if (params.type) {
            result.questions = chapter_id
                ? await Quiz.getQuestionsForBibleChapter(book_id, chapter_id, params.type)
                : await Quiz.getQuestionForVerse(verse_id, params.type);
            result.totalCount = result.questions.length;
        } else {
            result.questions = await Quiz.getAllQuestions(perPage, page);
            result.totalCount = await QueezeQuestions.getCountActiveQuestions();
        }
        return result;
    } catch (err) {
        throw new ServerError(
            quizException.CANT_GET_QUESTION_FOR_BIBLE_CHAPTER + ' ' + err.message,
            500
        );
    }
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
