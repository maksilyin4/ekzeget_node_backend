import * as Joi from '@hapi/joi';

export default Joi.object({
    'per-page': Joi.number()
        .min(1)
        .description('? Всегда 20')
        .required(),
    page: Joi.number()
        .min(1)
        .description('? Всегда 1')
        .required(),
    type: Joi.string()
        .valid('T', 'I')
        .description('Тип вопросов Т- Текст, I- Толкование'),
    book_id: Joi.number().description('id книги'),
    chapter_id: Joi.number().description('id главы').optional(),
    verse_id: Joi.number().description('id стиха').optional(),
});
