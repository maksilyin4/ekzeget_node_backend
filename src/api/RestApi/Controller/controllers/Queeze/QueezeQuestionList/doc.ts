/**
 * @api {get} /api/v1/queeze/question-list/ получить вопросы по главе книги
 *
 * @apiVersion 0.1.0
 * @apiName QueezeQuestionList
 * @apiGroup Queeze
 *
 * @apiDescription Создать викторину
 * @apiPermission All
 *
 * @apiParam {number}           per-page                     Настройка пагинатора. Сколько на странице (всегда 20)
 * @apiParam {number}           page                         Настройка пагинатора. Номер страницы (всегда 1)
 * @apiParam {String}           type?                        Тип вопросов (Т- текст, I- толкование)
 * @apiParam {number}           book_id?                     id книги
 * @apiParam {number}           chapter_id?                  id главы
 *
 * @apiSuccess {Object[]}   questions                       Вопросы для текущей главы
 * @apiSuccess {number}     question.id                     id
 * @apiSuccess {String}     questions.code                  Текстовый код вопроса
 * @apiSuccess {String}     questions.title                 Текст вопроса
 * @apiSuccess {Object}     questions.verse                 Стихи
 * @apiSuccess {number}     questions.verse.id              id
 * @apiSuccess {number}     questions.verse.number          номер стиха в главе
 * @apiSuccess {number}     questions.verse.book_id         id связанной книги
 * @apiSuccess {String}     questions.verse.text            текст стиха (может быть пустым)
 * @apiSuccess {number}     questions.verse.chapter         глава к которой относится стих (и вопрос)
 * @apiSuccess {number}     questions.verse.chapter_id      id главы к которой относится стих (и вопрос)
 * @apiSuccess {String}     questions.verse.book_code       Текстовый код связанной книги
 * @apiSuccess {String}     questions.verse.short           Сокращенное название связанной книги
 * @apiSuccess {number}     totalCount                      Количесвто связанных вопросов
 *
 * @apiError (500) {ApiException}       error Ошибка базы данных

 */
