import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Config from '../../../../../../entities/Config';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const urlToTutorial = await Config.getTutorialForQuiz();

    return {
        url: urlToTutorial.value,
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
