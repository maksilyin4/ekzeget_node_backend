import * as Joi from '@hapi/joi';

export default Joi.object({
    url: Joi.string().description('Ссылка на видеоинструкцию'),
}).required();
