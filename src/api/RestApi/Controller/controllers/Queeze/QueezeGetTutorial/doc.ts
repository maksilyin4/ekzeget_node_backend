/**
 * @api {get} /api/v1/queeze/tutorial/ получить ссылку на видеоинструкцию
 *
 * @apiVersion 0.1.0
 * @apiName GetQueezeTutirial
 * @apiGroup Queeze
 *
 * @apiDescription Ссылка на видеоинструкцию
 * @apiPermission All
 *
 * @apiSuccess {String}   url                        Ссылка на видео
 * @apiError (500) {ApiException}       error Ошибка базы данных
 *
 * @return array
 */
