import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import EnviromentManager from '../../../../../../managers/EnvironmentManager/index';
import FtpManager from '../../../../../../managers/FtpManager/index';
import Media from '../../../../../../entities/Media';
import QueezeQuestions from '../../../../../../entities/QueezeQuestions';
import { generateSlug } from '../../../../../../libs/Rus2Translit/generateSlug';
import { getChapterAndBookId, getPropertiesFromBase, getSpreadName } from './helpers';

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const {
        questionContentId,
        answerContentId,
        questionDir,
        answerDir,
        mediaCount,
    } = await getPropertiesFromBase();

    if (mediaCount) {
        return { status: 'Медиа уже есть' };
    }

    const questionList = await new FtpManager().getDirectoryContents(questionDir);

    await Promise.all(
        questionList.map(async item => {
            if (item.title === 'introduction.mp3' || item.title === 'wrong.mp3') {
                return null;
            }
            const spreadName = getSpreadName(item.title);

            try {
                const { chapter_id, book_id } = await getChapterAndBookId(
                    spreadName.bookShort,
                    spreadName.chapter
                );

                const questionList = await QueezeQuestions.getQuestionsByBookAndChapter(
                    book_id,
                    chapter_id
                );

                const questionId = questionList[spreadName.numberQuestion - 1].id;
                const answerTitle = item.title.replace('1.mp3', '2.mp3');

                const relatedIds = await Media.insertManyAndGetId([
                    {
                        title: item.title,
                        active: 1,
                        code: generateSlug(item.title),
                        type: 'A',
                        properties: await JSON.stringify({
                            audio_href:
                                EnviromentManager.envs.STORAGE_MEDIA_CDN +
                                questionDir +
                                '/' +
                                item.title,
                        }),
                        main_category: questionContentId,
                    },
                    {
                        title: answerTitle,
                        active: 1,
                        code: generateSlug(answerTitle),
                        type: 'A',
                        properties: await JSON.stringify({
                            audio_href:
                                EnviromentManager.envs.STORAGE_MEDIA_CDN +
                                answerDir +
                                '/' +
                                answerTitle,
                        }),
                        main_category: answerContentId,
                    },
                ]);

                await QueezeQuestions.update(
                    { id: questionId },
                    {
                        media_question: relatedIds[0],
                        media_answer: relatedIds[1],
                    }
                );
            } catch (e) {
                console.log(spreadName);
            }
        })
    );

    return { status: 'Все медиа загружены' };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
