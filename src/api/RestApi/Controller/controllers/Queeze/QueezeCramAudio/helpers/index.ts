import MediaCategory from '../../../../../../../entities/MediaCategory';
import Config from '../../../../../../../entities/Config';
import Media from '../../../../../../../entities/Media';
import Chapters from '../../../../../../../entities/Chapters';

export async function getPropertiesFromBase() {
    const questionContentId = (
        await MediaCategory.findOne({
            select: ['id'],
            where: {
                title: 'Вопросы викторины',
            },
        })
    ).id;

    const answerContentId = (
        await MediaCategory.findOne({
            select: ['id'],
            where: {
                title: 'Ответы на вопросы викторины',
            },
        })
    ).id;

    const questionDir = (
        await Config.findOne({
            select: ['value'],
            where: {
                name: 'quiz_questions_audio_dir',
            },
        })
    ).value;

    const answerDir = (
        await Config.findOne({
            select: ['value'],
            where: {
                name: 'quiz_answers_audio_dir',
            },
        })
    ).value;

    const mediaCount = await Media.count({
        where: {
            main_category: questionContentId,
        },
    });

    return {
        questionContentId,
        answerContentId,
        questionDir,
        answerDir,
        mediaCount,
    };
}

export function getSpreadName(name: string) {
    let bookShort = name.slice(name.indexOf('_') + 1, name.indexOf('-'));
    const chapter = +name.slice(name.indexOf('-') + 1, name.indexOf('-') + 3);
    const numberQuestion = +name.slice(name.lastIndexOf('-') - 2, name.lastIndexOf('-'));

    if (+bookShort[0]) {
        bookShort = bookShort[0] + ' ' + bookShort.slice(1);
    }

    return {
        bookShort,
        chapter,
        numberQuestion,
    };
}

export async function getChapterAndBookId(
    bookShort,
    chapter
): Promise<{
    chapter_id: number;
    book_id: number;
}> {
    return await Chapters.createQueryBuilder('chapter')
        .select(['chapter.id', 'book.id'])
        .leftJoin('book', 'book', 'book.short_title = :short', {
            short: bookShort,
        })
        .where('chapter.number = :chapter', { chapter: chapter })
        .andWhere('chapter.book_id = book.id')
        .getRawOne();
}
