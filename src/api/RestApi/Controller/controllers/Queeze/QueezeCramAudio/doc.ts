/**
 * @api {get} /api/v1/queeze/cram-audio-into-base Добавить озвучку викторины с FTP в базу
 *
 * @apiVersion 0.1.0
 * @apiName QueezeCramAudio)
 * @apiGroup Queeze
 *
 * @apiDescription Добавить озвучку викторины с FTP в базу
 * @apiPermission All
 *
 * @apiSuccess {String}   status                        Статус ответа R или error
 *

 * @apiError (500) {ApiException}       error Ошибка базы данных

 */
