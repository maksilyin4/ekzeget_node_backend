import * as Joi from '@hapi/joi';

export default Joi.object({
    status: Joi.string().description('Статус ответа'),
}).required();
