import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import { ICookieQuiz, IQuiz } from '../../../../../../core/App/interfaces/IQuiz';
import exceptionConstants from '../../../../../../const/strings/quiz/quizException';
import Quiz from '../Quiz';
import QueezeApplications from '../../../../../../entities/QueezeApplications';

type TQueezeCurrent = { queeze: Partial<IQuiz> };

const handler = async (
    params: Joi.extractType<typeof inputJoiSchema>,
    { cookie, session }: HandlerOptionalParams & { cookie: ICookieQuiz }
): Promise<TQueezeCurrent> => {
    await QueezeApplications.closeAllUserQuizzes(cookie.queezes_codes, session.passport?.user);

    try {
        await QueezeApplications.changeQuizStatus(Number(params.quizId), 'R');

        const continuedQuiz = await Quiz.continueQuiz(Number(params.quizId));
        return { queeze: continuedQuiz.getProperties() };
    } catch (err) {
        throw new ServerError(exceptionConstants.CANT_CONTINUE, 404);
    }
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
