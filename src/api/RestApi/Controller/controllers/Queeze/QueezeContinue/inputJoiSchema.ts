import * as Joi from '@hapi/joi';

export default Joi.object({
    quizId: Joi.string()
        .regex(/^[0-9]+$/)
        .disallow(null, '')
        .description('id викторины'),
}).required();
