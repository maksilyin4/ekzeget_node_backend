import * as Joi from '@hapi/joi';

export default Joi.object({
    books: Joi.array()
        .items({
            id: Joi.number().description('id книги'),
            testament_id: Joi.number().description('id Завета'),
            title: Joi.string().description('Название книги'),
            short_title: Joi.string().description('Сокращенное название'),
            parts: Joi.number().description('Количество глав'),
            code: Joi.string().description('Код книге на транслите'),
            legacy_code: Joi.string().description('Сокращенный код книги на транслите'),
            menu: Joi.string().description('Название книги на кнопках в меню'),
            author: Joi.string().description('Автор'),
            year: Joi.string().description('Год написания'),
            place: Joi.string().allow(null, '').description('Место'),
            ext_id: Joi.number().description('ID книги в сервисе'),
            gospel: Joi.number().description('Евангелие'),
            sort: Joi.number().description('Сортировка'),
        })
        .description('Список книг для викторины'),
}).required();
