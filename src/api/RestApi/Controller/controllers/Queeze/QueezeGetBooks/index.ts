import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Book from '../../../../../../entities/Book';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const books = await Book.getBooksForQuiz();

    return { books };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
