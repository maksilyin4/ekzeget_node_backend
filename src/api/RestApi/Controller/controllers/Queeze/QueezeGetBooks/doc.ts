/**
 * @api {get} /api/v1/queeze/getBooks/ Получить список книг для лотери
 *
 * @apiVersion 0.1.0
 * @apiName GetQueezeBooks
 * @apiGroup Queeze
 *
 * @apiDescription Получить список книг для лотереи
 * @apiPermission All
 *
 * @apiSuccess {Object[]} books                     Список книг
 * @apiSuccess {number} books.id                    id книги
 * @apiSuccess {number} books.testament_id          id Завета
 * @apiSuccess {string} books.title                 Название книги
 * @apiSuccess {string} books.short_title           Сокращенное название
 * @apiSuccess {number} books.parts                 Количество глав
 * @apiSuccess {string} books.code                  Код книге на транслите
 * @apiSuccess {string} books.legacy_code           Сокращенный код книги на транслите
 * @apiSuccess {string} books.menu                  Название книги на кнопках в меню
 * @apiSuccess {string} books.author                Автор
 * @apiSuccess {string} books.year                  Год написания
 * @apiSuccess {string} books.place                 Место
 * @apiSuccess {number} books.ext_id                ID книги в сервисе
 * @apiSuccess {number} books.gospel                Евангелие
 * @apiSuccess {number} books.sort                  Сортировка
 *
 *
 *
 * @apiError (500) {ApiException}       error Ошибка базы данных
 *
 */
