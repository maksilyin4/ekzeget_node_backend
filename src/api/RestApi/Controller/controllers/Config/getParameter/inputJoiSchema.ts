import * as Joi from '@hapi/joi';

export default Joi.object({
    name: Joi.string().description('Название параметра'),
});
