import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Config from '../../../../../../entities/Config';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import configException from '../../../../../../const/strings/Config/configException';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { name } = validatedParams;

    const parameter = await Config.findOne({ where: { name: name } });
    if (parameter) {
        return {
            data: {
                value: parameter.value,
            },
        };
    }

    throw new ServerError(configException.NO_DATA, 404);
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
