import * as Joi from '@hapi/joi';

export default Joi.object({
    data: Joi.object({
        value: Joi.string(),
    }).allow(null),
}).required();
