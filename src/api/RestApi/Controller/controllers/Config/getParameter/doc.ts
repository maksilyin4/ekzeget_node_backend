/**
 * @api {get} /api/v1/config
 * @apiVersion 0.1.0
 * @apiName admin
 * @apiGroup admin
 *
 *
 * @apiDescription Получить параметр по имени
 * @apiPermission All
 *
 * @apiParam {Integer}  name      название параметра
 *
 *
 * @apiSuccess {Object}   data                        данные
 * @apiSuccess {String}   data.value                  значение
 * @apiSuccess {Object}   error                       объект ошибки
 * @apiSuccess {Number}   error.code                  код ошибки
 * @apiSuccess {String}   error.message               сообщение ошибки
 * @apiSuccess {String}   error.status                статус ошибки

 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
