import Config from '../../../../../../entities/Config';
import getParameter from './index';

describe('getParameters', () => {
    afterAll(async () => {
        jest.restoreAllMocks();
    });

    const configSpy = jest.spyOn(Config, 'findOne');

    test('Data is in the database', async () => {
        configSpy.mockResolvedValueOnce({ value: 'TestValue' } as Config);

        const result = await getParameter.handle({ name: 'Test query' });

        expect(configSpy.mock.calls[0][0]).toEqual({ where: { name: 'Test query' } });
        expect(result).toEqual({
            data: {
                value: 'TestValue',
            },
        });
    });

    test('No data in the database', async () => {
        configSpy.mockResolvedValueOnce(undefined);

        try {
            const result = await getParameter.handle({ name: 'Test query' });
        } catch (e) {
            expect(e.message).toBe('Данные не найдены');
            expect(e.statusCode).toBe(404);
        }
    });
});
