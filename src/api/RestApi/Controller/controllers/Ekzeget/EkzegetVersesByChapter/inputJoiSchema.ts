import * as Joi from '@hapi/joi';

export default Joi.object({
    interpreter_id: Joi.number().required(),
    chapter_id: Joi.number().required(),
}).required();
