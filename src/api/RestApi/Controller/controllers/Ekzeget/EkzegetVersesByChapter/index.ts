import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Interpretation from '../../../../../../entities/Interpretation';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { interpreter_id, chapter_id } = validatedParams;

    const query = Interpretation.createQueryBuilder('interpretation')
        .select(['verse.number', 'verse.text', 'interpretation.id', 'interpretation.parsed_text'])
        .innerJoin('interpretation.interpretation_verses', 'intpn_verses')
        .innerJoin('intpn_verses.verse', 'verse')
        .where({ ekzeget_id: interpreter_id })
        .andWhere( `verse.chapter_id = :chapter_id`, {chapter_id} )
        .orderBy('verse.number');

    return { verses: await query.getRawMany() };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
