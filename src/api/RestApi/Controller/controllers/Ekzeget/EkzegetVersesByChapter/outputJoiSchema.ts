import * as Joi from '@hapi/joi';

export default Joi.object({
    verses: Joi.array().items(
        Joi.object({
            verse_number: Joi.number().required(),
            verse_text: Joi.string().required(),
            interpretation_id: Joi.number().required(),
            interpretation_parsed_text: Joi.string().required(),
        })
    ),
}).required();
