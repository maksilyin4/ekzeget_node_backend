import * as Joi from '@hapi/joi';

export default Joi.object({
    ekzeget_type: Joi.object({
        id: Joi.number().description('ID Типа'),
        title: Joi.string().description('Название типа'),
    }).description('Информация о типе экзегета'),
}).required();
