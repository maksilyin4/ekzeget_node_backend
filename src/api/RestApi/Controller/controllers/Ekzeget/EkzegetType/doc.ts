/**
     * @api {get} /ekzeget/type/ Список типов экзегетов
     * @apiVersion 0.1.0
     * @apiName type
     * @apiGroup Ekzeget
     *
     *
     * @apiDescription Список типов экзегетов
     * @apiPermission All
     *

     * @apiSuccess {Object}   ekzeget_type                   Информация о типе экзегета
     * @apiSuccess {Integer}  ekzeget_type.id                ID Типа
     * @apiSuccess {String}   ekzeget_type.title             Название типа
     *
     * @apiError (400) {ApiException} error Авторизация не прошла
     * @apiError (403) {ApiException} error Доступ запрещен
     * @apiError (500) {ApiException} error Внутренняя ошибка сервера
     */
