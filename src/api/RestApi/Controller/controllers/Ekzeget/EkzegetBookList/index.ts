import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Interpretation from '../../../../../../entities/Interpretation';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { interpreter_id } = validatedParams;

    const query = Interpretation.createQueryBuilder('interpretation')
        .select(['book.id', 'book.title'])
        .innerJoin('interpretation.interpretation_verses', 'intpn_verses')
        .innerJoin('intpn_verses.verse', 'verse')
        .innerJoin('verse.book', 'book')
        .where({ ekzeget_id: interpreter_id })
        .groupBy('verse.book_id');

    return { books: await query.getRawMany() };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
