import * as Joi from '@hapi/joi';

export default Joi.object({
    books: Joi.array().items({
        book_id: Joi.number().required(),
        book_title: Joi.string().required(),
    }),
}).required();
