import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller, { HandlerOptionalParams } from '../../../../Controller';
import { extractNumber } from '../../../../../../libs/helpers/urlParsers';
import Book from '../../../../../../entities/Book';
import { getEkzegetsRelatedToBook } from './getEkzegetsRelatedToBook';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import returnCodeOrIdConditionObject from '../../../../../../libs/helpers/returnCodeOrIdConditionObject';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>,
    { session }: HandlerOptionalParams
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const {
        book: bookCodeOrId,
        chapter: chapterRaw,
        verse: verseNumber,
        type,
        century,
    } = validatedParams;
    const chapterNum = extractNumber(chapterRaw);

    const book: Book = await Book.findOne({
        where: returnCodeOrIdConditionObject(bookCodeOrId),
    });
    if (!book) {
        throw new ServerError(`Книга не найдена [${bookCodeOrId}]`, 404);
    }

    const [ekzegets, totalCount] = await getEkzegetsRelatedToBook({
        bookId: book.id,
        chapter: chapterNum,
        verseNumber: +verseNumber,
        type,
        century,
        userId: session.passport?.user,
    });

    return {
        ekzeget: ekzegets,
        pages: returnPaginationObject({
            ...validatedParams,
            totalCount,
        }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
