/**
 * @api {get} /ekzeget/list-for-chapter/:book/:chapter/:verse/?century=:century&type=:type&info=:info&page=:page&per-page=:per-page Список экзегетов по главе
 * @apiVersion 0.1.0
 * @apiName list-for-chapter
 * @apiGroup Ekzeget
 *
 *
 * @apiDescription  Список экзегетов по главе
 * @apiPermission All
 *
 * @apiParam {String}   book                            Код или ID книги
 * @apiParam {Integer}  chapter                         Номер глава
 * @apiParam {Integer}  verse                         Номер стиха (опционально)
 * @apiParam {Integer}  century                         Век (опционально)
 * @apiParam {Integer}  type                            Тип экзегета (опционально)
 * @apiParam {Integer}  info                            Вывести детальную информацию об экзегете 0/1 (опционально, по умолчанию 0)
 * @apiParam {Integer}  page                            Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page                        Количество элементов на странице (опционально, по умолчанию 20)
 *
 *
 * @apiSuccess {Object[]} ekzeget                        Экзегет
 * @apiSuccess {Integer}  ekzeget.id                     ID экзегета
 * @apiSuccess {Integer}  ekzeget.century                Век экзегета
 * @apiSuccess {String}   ekzeget.name                   Имя экзегета
 * @apiSuccess {String}   ekzeget.info                   Информация об экзегете (опционально)
 * @apiSuccess {Object}   ekzeget.ekzeget_type           Информация о типе экзегета
 * @apiSuccess {Integer}  ekzeget.ekzeget_type.id                ID Типа
 * @apiSuccess {String}   ekzeget.ekzeget_type.title             Название типа
 * @apiSuccess {Bool}     ekzeget.investigated           У толкователя есть исследования по выбранному стиху/главе
 * @apiSuccess {Bool}     ekzeget.interpretation         У толкователя есть толкования по выбранному стиху/главе
 * @apiSuccess {Bool}     ekzeget.in_fav                Есть ли у пользователя в избранном толкование стиха у толкователя
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
