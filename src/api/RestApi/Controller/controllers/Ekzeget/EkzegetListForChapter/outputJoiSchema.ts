import * as Joi from '@hapi/joi';
import { paginationObjectScheme } from '../../../../../../libs/helpers/pagination';

export default Joi.object({
    ekzeget: Joi.array()
        .items({
            id: Joi.number().description('ID экзегета'),
            name: Joi.string().description('Имя экзегета'),
            code: Joi.string()
                .allow('')
                .description('Код экзегета'),
            century: Joi.number()
                .allow(null)
                .description('Век экзегета'),
            active: Joi.number().description('Активен? (0/1)'),
            ekzeget_type: Joi.object({
                id: Joi.number().description('ID Типа'),
                title: Joi.string().description('Название типа'),
                active: Joi.number().description('Активен? (0/1)'),
                code: Joi.string().description('Код типа экзегета'),
            }).description('Информация о типе экзегета'),
            investigated: Joi.boolean().description(
                'У толкователя есть исследования по выбранному стиху/главе'
            ),
            interpretation: Joi.boolean().description(
                'У толкователя есть толкования по выбранному стиху/главе'
            ),
            in_fav: Joi.boolean().description(
                'Есть ли у пользователя в избранном толкование стиха у толкователя'
            ),
        })
        .description('Экзегет'),
    pages: paginationObjectScheme,
}).required();
