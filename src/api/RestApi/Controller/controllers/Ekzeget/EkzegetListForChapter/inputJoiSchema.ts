import * as Joi from '@hapi/joi';

export default Joi.object({
    book: Joi.string()
        .description('Код или ID книги')
        .required(),
    chapter: Joi.string()
        .description('Номер глава')
        .required(),
    verse: Joi.string()
        .description('Номер стиха (опционально)')
        .optional(),
    century: Joi.number()
        .description('Век (опционально)')
        .optional(),
    type: Joi.number()
        .description('Тип экзегета (опционально)')
        .optional(),
    info: Joi.number()
        .description('Вывести детальную информацию об экзегете 0/1 (опционально, по умолчанию 0)')
        .optional()
        .default(0),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number()
        .description('Количество элементов на странице (опционально, по умолчанию 20)')
        .default(20),
}).required();
