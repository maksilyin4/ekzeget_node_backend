import EkzegetPerson from '../../../../../../entities/EkzegetPerson';
import EkzegetType from '../../../../../../entities/EkzegetType';

interface InputParams {
    bookId: number;
    chapter: number;
    verseNumber?: number;
    offset?: number;
    limit?: number;
    type?: number;
    century?: number;
    userId?: number;
}

interface ExegeteWithStatsRaw extends EkzegetPerson {
    investigated: string;
    interpretation: string;
    count: string;
    in_fav?: number;
    ekzeget_type_id: number;
    ekzeget_type_title: string;
    ekzeget_type_active: number;
    ekzeget_type_code: string;
}
interface ExegeteWithStats extends Omit<EkzegetPerson, 'ekzeget_type'> {
    ekzeget_type: Partial<EkzegetType>;
    investigated: boolean;
    interpretation: boolean;
    in_fav?: boolean;
}

const presenter = (ekzegets: ExegeteWithStatsRaw[]): Partial<ExegeteWithStats>[] => {
    return ekzegets.map(ep => {
        const {
            info,
            count,
            ekzeget_type_id,
            ekzeget_type_title,
            ekzeget_type_code,
            ekzeget_type_active,
            ...rest
        } = ep;
        return {
            ...rest,
            ekzeget_type: {
                id: ekzeget_type_id,
                title: ekzeget_type_title,
                active: ekzeget_type_active,
                code: ekzeget_type_code,
            },
            investigated: Number(ep.investigated) > 0,
            interpretation: Number(ep.count) > Number(ep.investigated),
            in_fav: !!ep.in_fav,
        };
    });
};

export const getEkzegetsRelatedToBook = async ({
    bookId,
    chapter,
    verseNumber,
    offset,
    limit,
    type,
    century,
    userId,
}: InputParams): Promise<[Partial<ExegeteWithStats>[], number]> => {
    const query = EkzegetPerson.createQueryBuilder('ekzeget_person')
        .select(['ekzeget_person.*', 'SUM(i.investigated) as investigated', 'COUNT(*) as count'])
        .innerJoinAndSelect(
            'ekzeget_type',
            'ekzeget_type',
            'ekzeget_type.id = ekzeget_person.ekzeget_type_id'
        )
        .leftJoin('interpretation', 'i', 'ekzeget_person.id = i.ekzeget_id')
        .leftJoin('interpretation_verse', 'iv', 'iv.interpretation_id = i.id')
        .innerJoin(
            'verse',
            'v',
            'iv.verse_id = v.id AND v.chapter = :chapter AND v.book_id = :bookId',
            {
                chapter,
                bookId,
            }
        )
        .where('i.active = TRUE AND ekzeget_person.active = TRUE');

    if (verseNumber) {
        query.andWhere('v.number = :verseNumber', { verseNumber });
    }

    if (type) {
        query.andWhere('ekzeget_person.ekzeget_type_id = :type', { type });
    }

    if (century) {
        query.andWhere('ekzeget_person.century = :century', { century });
    }

    query.groupBy('ekzeget_person.id');

    if (userId) {
        query
            .addSelect(['IF(uif.id IS NOT NULL, true, false) AS `in_fav`'])
            .leftJoin(
                'user_interpretation_fav',
                'uif',
                'uif.interpretation_id = i.id AND uif.user_id = :user_id',
                { user_id: userId }
            );
    }

    if (offset) {
        query.offset(offset);
    }
    if (limit) {
        query.limit(limit);
    }

    const ekzegets: ExegeteWithStatsRaw[] = await query.getRawMany();
    const total = offset || limit ? await query.getCount() : ekzegets.length;

    return [presenter(ekzegets), total];
};
