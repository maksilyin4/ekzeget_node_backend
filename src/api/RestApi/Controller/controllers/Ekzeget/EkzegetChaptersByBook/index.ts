import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Interpretation from '../../../../../../entities/Interpretation';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { interpreter_id, book_id } = validatedParams;

    const query = Interpretation.createQueryBuilder('interpretation')
        .select(['verse.chapter', 'verse.chapter_id'])
        .distinct(true)
        .innerJoin('interpretation.interpretation_verses', 'intpn_verses')
        .innerJoin('intpn_verses.verse', 'verse')
        .where({ ekzeget_id: interpreter_id })
        .andWhere(`verse.book_id = :book_id`, { book_id })
        .addOrderBy('verse.chapter');

    return { chapters: await query.getRawMany() };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
