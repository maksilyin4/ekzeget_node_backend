import * as Joi from '@hapi/joi';

export default Joi.object({
    chapters: Joi.array().items(
        Joi.object({
            verse_chapter: Joi.number().required(),
            verse_chapter_id: Joi.number().required(),
        })
    ),
}).required();
