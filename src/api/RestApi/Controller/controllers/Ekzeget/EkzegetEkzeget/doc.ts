/**
 * @api {get} /ekzeget/?q=:q&century=:century&type=:type&info=:info&id=:id&page=:page&per-page=:per-page Список экзегетов
 * @apiVersion 0.1.0
 * @apiName ekzeget
 * @apiGroup Ekzeget
 *
 *
 * @apiDescription Список экзегетов
 * @apiPermission All
 *
 * @apiParam {String}   q                               Строка поиска по имени
 * @apiParam {Integer}  century                         Век (опционально)
 * @apiParam {Integer}  type                            Тип эекзегета (опционально)
 * @apiParam {Integer}  info                            Вывести детельную информаю об экзегете 0/1 (опционально, по умолчанию 0)
 * @apiParam {Integer}  id                              ID экзегета (опционально)
 * @apiParam {Integer}  page                            Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page                        Количество элементов на странице (опционально, по умолчанию 20)
 *
 *
 * @apiSuccess {Object[]} ekzeget                        Экзегет
 * @apiSuccess {Integer}  ekzeget.id                     ID эекзегета
 * @apiSuccess {Integer}  ekzeget.century                Век эекзегета
 * @apiSuccess {String}   ekzeget.name                   Имя эекзегета
 * @apiSuccess {String}   ekzeget.info                   Информация об эекзегете (опционально)
 * @apiSuccess {Object}   ekzeget.ekzeget_type           Информация о типе экзегета
 * @apiSuccess {Integer}  ekzeget.ekzeget_type.id                ID Типа
 * @apiSuccess {String}   ekzeget.ekzeget_type.title             Название типа
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
