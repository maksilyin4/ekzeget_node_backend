import * as Joi from '@hapi/joi';

export default Joi.object({
    q: Joi.string()
        .optional()
        .description('Строка поиска по имени'),
    century: Joi.number()
        .optional()
        .description('Век (опционально)'),
    type: Joi.number()
        .optional()
        .description('Тип экзегета (опционально)'),
    info: Joi.number()
        .optional()
        .description('Вывести детальную информацию об экзегете 0/1 (опционально, по умолчанию 0)'),
    id: Joi.string()
        .optional()
        .allow('')
        .description('ID экзегета (опционально)'),
    page: Joi.number()
        .optional()
        .description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number()
        .optional()
        .description('Количество элементов на странице (опционально, по умолчанию 20)'),

    // Параметры отсутствующие в исходной документации, но которые приходят с фронта на проде
    sa: Joi.string()
        .allow('')
        .optional(),
    ved: Joi.string()
        .allow('')
        .optional(),
    usg: Joi.string()
        .allow('')
        .optional(),
}).required();
