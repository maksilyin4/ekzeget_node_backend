import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import EkzegetPerson from '../../../../../../entities/EkzegetPerson';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import returnCodeOrIdConditionObject from '../../../../../../libs/helpers/returnCodeOrIdConditionObject';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { id } = validatedParams;

    if (id) {
        return {
            ekzeget: await EkzegetPerson.findOne({
                where: returnCodeOrIdConditionObject(id),
                relations: ['ekzeget_type'],
            }),
        };
    }

    const [ekzegets, totalCount] = await EkzegetPerson.findAndCount({
        take: validatedParams['per-page'],
        skip: validatedParams['per-page'] * validatedParams.page,
        relations: ['ekzeget_type'],
    });

    return {
        ekzeget: ekzegets.sort((a, b) => ('' + a.name).localeCompare(b.name)),
        pages: returnPaginationObject({ ...validatedParams, totalCount }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
