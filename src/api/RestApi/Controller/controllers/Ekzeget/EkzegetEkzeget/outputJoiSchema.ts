import * as Joi from '@hapi/joi';

export default Joi.object({
    ekzeget: Joi.alternatives([
        Joi.array().items(
            Joi.object({
                id: Joi.number().description('ID эекзегета'),
                century: Joi.number()
                    .allow(null)
                    .description('Век эекзегета'),
                name: Joi.string().description('Имя эекзегета'),
                info: Joi.string().description('Информация об эекзегете (опционально)'),
                ekzeget_type: Joi.object({
                    id: Joi.number().description('ID Типа'),
                    title: Joi.string().description('Название типа'),
                })
                    .unknown()
                    .description('Информация о типе экзегета'),
            })
                .unknown()
                .description('Экзегет')
        ),
        Joi.object({
            id: Joi.number().description('ID эекзегета'),
            century: Joi.number()
                .allow(null)
                .description('Век эекзегета'),
            name: Joi.string().description('Имя эекзегета'),
            info: Joi.string().description('Информация об эекзегете (опционально)'),
            ekzeget_type: Joi.object({
                id: Joi.number().description('ID Типа'),
                title: Joi.string().description('Название типа'),
            })
                .unknown()
                .description('Информация о типе экзегета'),
        })
            .unknown()
            .description('Экзегет'),
    ]),

    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    })
        .unknown()
        .description('Информация о возможной постраничной навигации'),
}).required();
