/**
 * @api {get} /ekzeget/verse-list/:interpretator_id/?q=:q&page=:page&per-page=:per-page Список стихов
 * @apiVersion 0.1.0
 * @apiName  verse-list
 * @apiGroup Ekzeget
 *
 *
 * @apiDescription  Список стихов, на которые писал толкования данный толкователь
 * @apiPermission All
 *
 * @apiParam {Integer}   interpretator_id                ID толкователя
 * @apiParam {String}    q                               Строка поиска (опционально)
 * @apiParam {Integer}  page                            Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page                        Количество элементов на странице (опционально, по умолчанию 20)
 *
 *
 * @apiSuccess {Object[]} verse                          Список стихов
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
