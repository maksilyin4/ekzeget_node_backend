import * as Joi from '@hapi/joi';

export default Joi.object({
    interpretator_id: Joi.string().description('ID толкователя'),
    q: Joi.string()
        .allow('', null)
        .description('Строка поиска (опционально)'),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
}).required();
