import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import EkzegetPerson from '../../../../../../entities/EkzegetPerson';
import Verse from '../../../../../../entities/Verse';
import Interpretation from '../../../../../../entities/Interpretation';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import returnCodeOrIdConditionObject from '../../../../../../libs/helpers/returnCodeOrIdConditionObject';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { interpretator_id, q } = validatedParams;

    // именно здесь екс-разрабы почему то решили пагинировать с 1, а не с 0, ак везде
    const page = validatedParams.page ? +validatedParams.page : 1;
    const perPage = +validatedParams['per-page'] || 20;

    const { id: ekzeget_id } = await EkzegetPerson.findOne(
        returnCodeOrIdConditionObject(interpretator_id)
    );

    const query = Verse.createQueryBuilder('verse')
        .limit(perPage)
        .offset(perPage * (page - 1))
        .leftJoin('verse.book', 'book')
        .innerJoin(
            Interpretation,
            'interpretation',
            'interpretation.ekzeget_id = :ekzeget_id and interpretation.active = 1',
            {
                ekzeget_id,
            }
        )
        .innerJoin('interpretation.interpretation_verses', 'interpretation_verse')
        .select([
            'verse.book_id',
            'verse.chapter_id',
            'verse.chapter',
            'verse.id',
            'verse.number',
            'verse.text',
            'book.short_title',
            'book.code',
        ])
        .where(
            'interpretation_verse.verse_id = verse.id and interpretation_verse.interpretation_id = interpretation.id'
        );

    if (q) query.andWhere(`book.short_title LIKE :like'`, { like: `%${q}%` });

    query
        .orderBy('book.sort', 'ASC')
        .addOrderBy('verse.chapter', 'ASC')
        .addOrderBy('verse.number', 'ASC');

    const [verses, total] = await query.getManyAndCount();

    return {
        verse: verses.map(v => ({
            ...v,
            book_code: v.book.code,
            short: `${v.book.short_title}. ${v.chapter}:${v.number}`,
        })),
        pages: returnPaginationObject({
            page,
            'per-page': perPage,
            totalCount: +total,
        }),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
