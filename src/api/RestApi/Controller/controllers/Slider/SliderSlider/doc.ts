/**
 * @api {get} /slider                     Список слайдеров
 * @apiVersion 0.1.0
 * @apiName slider
 * @apiGroup Slider
 *
 *
 * @apiDescription Список записей
 * @apiPermission All
 *
 * @apiSuccess {Object[]} sliders                    Список слайдеров
 * @apiSuccess {Integer}  slider_change_time   Время автоматического пролистывания слайдера в с. 0 - без пролистывания
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
