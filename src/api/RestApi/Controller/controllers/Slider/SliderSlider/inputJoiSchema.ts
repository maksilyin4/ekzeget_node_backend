import * as Joi from '@hapi/joi';

export default Joi.object({
    img_sizes: Joi.array().items(Joi.string()),
}).required();
