import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Slider from '../../../../../../entities/Slider';

const STATUS_PUBLISHED = 1;
const TYPE_BIG = 'big';

const sliderPresenter = (slider: Slider) => {
    return {
        id: slider.id,
        article_id: slider.article_id,
        video_url: slider.video_url,
        image: slider.image,
        title: slider.title,
        description: slider.description,
        type: slider.type,
        status: slider.status,
        created_at: slider.created_at,
        updated_at: slider.updated_at,
        url: slider.url,
        article_title: slider.article_id ? slider.article.title : null,
        article_image: slider.article_id ? slider.article.image : null,
        image_optimized: [],
        article_image_optimized: [],
    };
};

const handler = async ({
    img_sizes,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const sliders = await Slider.createQueryBuilder('slider')
        .select('slider')
        .leftJoinAndSelect('slider.article', 'article')
        .where('slider.status = :status', { status: STATUS_PUBLISHED })
        .orderBy('slider.created_at', 'DESC')
        .getMany();

    return {
        sliders,
        slider_change_time: '8',
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
