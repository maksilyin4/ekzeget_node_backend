import * as Joi from '@hapi/joi';

export default Joi.object({
    verse: Joi.array()
        .items(
            Joi.object({
                id: Joi.number().description('ID стиха'),
                number: Joi.number().description('Номер стиха'),
                text: Joi.string().description('Текст стиха'),
                chapter: Joi.number().description('Номер главы'),
                book: Joi.object({
                    id: Joi.number().description('ID Книги'),
                    testament_id: Joi.number().description('Завет, 1 - Ветхий, 2 - Новый'),
                    title: Joi.string().description('Название'),
                    short_title: Joi.string().description('Короткое название'),
                    parts: Joi.number().description('Количество глав в книге'),
                    code: Joi.string().description('Код книги'),
                })
                    .unknown(true)
                    .description('Информация о книге'),
            }).unknown(true)
        )
        .description('Стих'),
    pages: Joi.object(),
}).required();
