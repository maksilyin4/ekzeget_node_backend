/**
 * @api {get} /verse/info/:verse/?translate=:translate&parallel:parallel Информация по названию
 * @apiVersion 0.1.0
 * @apiName info
 * @apiGroup Verse
 *
 *
 * @apiDescription Информация о стихе по короткому названию
 * @apiPermission All
 *
 * @apiParam {String}   verse       Сокращенное название стиха в форматах:<br>
 *                                  - Книга [Книга], например info/Быт/ - возвращает все стихи книги<br>
 *                                  - Глава [Книга. Глава], например info/Быт 1/ - возвращает все стихи главы книги<br>
 *                                  - Стих [Книга. Глава:Стих], например info/Быт 1:1/ - возвращает конкретный стих<br>
 *                                  - Группа стихов [Книга. Глава:Стих-Стих], например info/Быт 1:1-10/ - возвращает стихи из заданного интервала
 * @apiParam {Integer}  chapter     Номер главы (опционально)
 * @apiParam {Integer}  translate   Показать переводы стиха (опционально 0/1)
 *
 * @apiSuccess {Object[]} verse                        Стих
 * @apiSuccess {Integer}  verse.id                     ID стиха
 * @apiSuccess {Integer}  verse.number                 Номер стиха
 * @apiSuccess {String}   verse.text                   Текст стиха
 * @apiSuccess {Integer}  verse.chapter                Номер главы
 * @apiSuccess {Object}   verse.book                   Информация о книге
 * @apiSuccess {Integer}  verse.book.id                ID Книги
 * @apiSuccess {Integer}  verse.book.testament_id      Завет, 1 - Ветхий, 2 - Новый
 * @apiSuccess {String}   verse.book.title             Название
 * @apiSuccess {String}   verse.book.short_title       Короткое название
 * @apiSuccess {Integer}  verse.book.parts             Количество глав в книге
 * @apiSuccess {String}   verse.book.code              Код книги
 * @apiSuccess {Object[]} verse.translate              Список переводов стиха (опционально)
 * @apiSuccess {Integer}  verse.translate.id           ID записи
 * @apiSuccess {String}   verse.translate.code         Код языка
 * @apiSuccess {String}   verse.translate.text         Текст
 * @apiSuccess {Integer}  verse.translate.verse_id     ID родительского стиха
 * @apiSuccess {Object[]} verse.parallel               Список паралельных стихов (опционально)
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
