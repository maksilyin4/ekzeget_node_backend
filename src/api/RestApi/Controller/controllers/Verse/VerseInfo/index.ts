import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Book from '../../../../../../entities/Book';
import Verse from '../../../../../../entities/Verse';
import returnPaginationObject from '../../../../../../libs/helpers/returnPaginationObject';
import pagination from '../../../../../../const/pagination';
import { VerseLinkParser } from '../../../../../../libs/VerseLinkParser';
import { valueOrRangeToFindOperator } from '../../../../../../libs/VerseLinkParser/valueOrRangeToFindOperator';
import { ParsedShortLink } from '../../../../../../libs/VerseLinkParser/types';
import CacheManager from '../../../../../../managers/CacheManager';

const VerseInfoCacheManager = {
    get: async (
        validatedParams: Joi.extractType<typeof inputJoiSchema>
    ): Promise<Joi.extractType<typeof outputJoiSchema>> =>
        CacheManager.get(`VerseInfo-${JSON.stringify(validatedParams)}`),

    set: async (
        validatedParams: Joi.extractType<typeof inputJoiSchema>,
        handlerResponse: Joi.extractType<typeof outputJoiSchema>
    ): Promise<boolean> =>
        CacheManager.set(`VerseInfo-${JSON.stringify(validatedParams)}`, handlerResponse),
};

const handler = async (validatedParams: Joi.extractType<typeof inputJoiSchema>) => {
    const cached = await VerseInfoCacheManager.get(validatedParams);
    if (cached) {
        return cached;
    }

    const parsedLinks: ParsedShortLink[] = new VerseLinkParser().parseString(validatedParams.verse);
    let allVerses: Verse[] = [];
    let allVersesCounter = 0;
    const limit = +validatedParams['per-page'] || pagination.defaultPerPage;

    for (const pl of parsedLinks) {
        const { bookShortTitle, chapters } = pl;
        const { id } = await Book.findOne({ where: { short_title: bookShortTitle } });

        for (const { verses, chapterNumber } of chapters) {
            if (!verses.length) {
                const [verses, count] = await Verse.findAndCount({
                    where: { book_id: id, chapter: chapterNumber },
                    relations: ['book', 'verse_translates', 'verse_translates.code2'],
                });
                allVerses = verses;
                allVersesCounter = count;
            }

            for (const verseNumbers of verses) {
                if (allVersesCounter >= limit) {
                    break;
                }
                const [verses, count] = await Verse.findAndCount({
                    where: {
                        book_id: id,
                        chapter: chapterNumber,
                        number: valueOrRangeToFindOperator(verseNumbers),
                    },
                    relations: ['book', 'verse_translates', 'verse_translates.code2'],
                });
                allVerses = [...allVerses, ...verses];
                allVersesCounter += count;
            }
        }
    }

    // Обрезаем лишнее если оно есть
    if (allVersesCounter > limit) {
        allVerses.length = limit;
        allVersesCounter = limit;
    }

    const result: Joi.extractType<typeof outputJoiSchema> = {
        verse: allVerses.map(v => ({
            ...v,
            translate: v.verse_translates.map(vt => ({ ...vt, code: vt.code2, code2: undefined })),
            verse_translates: undefined,
        })),
        pages: returnPaginationObject({
            'per-page': limit,
            page: +validatedParams.page,
            totalCount: allVersesCounter,
        }),
    };
    await VerseInfoCacheManager.set(validatedParams, result);

    return result;
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
