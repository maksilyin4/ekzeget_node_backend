import * as Joi from '@hapi/joi';
import pagination from '../../../../../../const/pagination';

export default Joi.object({
    verse: Joi.string().description('Сокращенное название стиха в форматах:<br>'),
    chapter: Joi.number().description('Номер главы (опционально)'),
    translate: Joi.number().description('Показать переводы стиха (опционально 0/1)'),
    page: Joi.string()
        .default(pagination.defaultPage)
        .description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.string()
        .default(pagination.defaultPerPage)
        .description('Количество элементов на странице (опционально, по умолчанию 20)'),
}).required();
