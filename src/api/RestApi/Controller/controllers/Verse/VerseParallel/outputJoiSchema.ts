import * as Joi from '@hapi/joi';

export default Joi.object({
    parallels: Joi.array()
        .items({
            id: Joi.number().description('ID стиха'),
            short: Joi.string().description('Короткое название'),
            book_code: Joi.string().description('Короткое название книги'),
            chapter: Joi.number().description('Номер главы'),
            number: Joi.number().description('Номер стиха'),
            text: Joi.string().description('Текст стиха в синадальном переводе'),
            translates: Joi.array()
                .items({
                    id: Joi.number().required(),
                    code: Joi.object({
                        id: Joi.number().required(),
                        number: Joi.number().required(),
                        code: Joi.string().required(),
                        title: Joi.string()
                            .required()
                            .allow(''),
                        select: Joi.string()
                            .required()
                            .allow(''),
                        search: Joi.string()
                            .required()
                            .allow(''),
                    }).description('Информация о переводе'),
                    text: Joi.string()
                        .description('Текст стиха в текущем переводе')
                        .allow(''),
                    verse_id: Joi.number().required(),
                })
                .description('Переводы стиха'),
        })
        .description('Стих'),
}).required();
