import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Parallel from '../../../../../../entities/Parallel';
import Verse from '../../../../../../entities/Verse';
import { getVerseShortCode } from '../../../presenters/Verse/getVerseShortCode';

const handler = async ({
    verse_id,
}: Joi.extractType<typeof inputJoiSchema>): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const parallels = await Parallel.createQueryBuilder('pd')
        .select('pd')
        .where('pd.verse_id = :verse_id', { verse_id })
        .getMany();

    if (!parallels.length) {
        return {
            parallels: [],
        };
    }

    const versesIds: number[] = parallels.map(p => p.parentVerseId);

    const verses: Verse[] = await Verse.createQueryBuilder('v')
        .select('v')
        .leftJoinAndSelect('v.book', 'b')
        .leftJoinAndSelect('v.verse_translates', 't')
        .leftJoinAndSelect('t.code2', 'c')
        .where('v.id IN (:...versesIds)', { versesIds })
        .getMany();

    return {
        parallels: verses.map((v: Verse) => ({
            id: v.id,
            short: getVerseShortCode(v.book, v),
            book_code: v.book.code,
            chapter: v.chapter,
            number: v.number,
            text: v.text,
            translates: v.verse_translates.map(t => ({
                id: t.id,
                code: {
                    id: t.code2.id,
                    number: t.code2.number,
                    code: t.code2.code,
                    title: t.code2.title,
                    select: t.code2.select,
                    search: t.code2.search,
                },
                text: t.text,
                verse_id: t.verse_id,
            })),
        })),
    };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
