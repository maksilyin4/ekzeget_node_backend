/**
 * @api {get} /verse/parallel/:verse_id/              Параллельные стихи
 * @apiVersion 0.1.0
 * @apiName parallel
 * @apiGroup Verse
 *
 *
 * @apiDescription Параллельные стихи с переводами
 * @apiPermission All
 *
 * @apiParam {Integer}    verse_id        ID стиха
 *
 * @apiSuccess {Object[]} verse                        Стих
 * @apiSuccess {Integer}  verse.id                     ID стиха
 * @apiSuccess {Integer}  verse.number                 Номер стиха
 * @apiSuccess {String}   verse.text                   Текст стиха в синадальном переводе
 * @apiSuccess {Integer}  verse.chapter                Номер главы
 * @apiSuccess {String}   verse.short                  Короткое название
 * @apiSuccess {String}   verse.book_code              Короткое название книги
 * @apiSuccess {Object[]} verse.translates             Переводы стиха
 * @apiSuccess {Object}   verse.translates.code        Информация о переводе
 * @apiSuccess {String}   verse.translates.text        Текст стиха в текущем переводе
 *
 * @apiError (4xx, 5xx) {ApiException} error
 */
