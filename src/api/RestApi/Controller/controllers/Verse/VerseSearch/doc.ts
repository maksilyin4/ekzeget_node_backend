/**
 * @api {get} /verse/search/?q=:q&page=:page&per-page=:per-page  Поиск по стихам
 * @apiVersion 0.1.0
 * @apiName search
 * @apiGroup Verse
 *
 *
 * @apiDescription Глобальный поиск по стихам
 * @apiPermission All
 *
 * @apiParam {String}   q           Строка поиска
 * @apiParam {Integer}  page        Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page    Количество элементов на странице (опционально, по умолчанию 20)
 *
 * @apiSuccess {Object[]} verse                        Стих
 * @apiSuccess {Integer}  verse.id                     ID стиха
 * @apiSuccess {Integer}  verse.number                 Номер стиха
 * @apiSuccess {String}   verse.text                   Текст стиха
 * @apiSuccess {Integer}  verse.chapter                Номер главы
 * @apiSuccess {Object}   verse.book                   Информация о книге
 * @apiSuccess {Integer}  verse.book.id                ID Книги
 * @apiSuccess {Integer}  verse.book.testament_id      Завет, 1 - Ветхий, 2 - Новый
 * @apiSuccess {String}   verse.book.title             Название
 * @apiSuccess {String}   verse.book.short_title       Короткое название
 * @apiSuccess {Integer}  verse.book.parts             Количество глав в книге
 * @apiSuccess {String}   verse.book.code              Код книги
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 *
 * @apiError (4xx, 5xx) {ApiException} error
 */
