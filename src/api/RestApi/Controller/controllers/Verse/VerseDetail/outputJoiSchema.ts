import * as Joi from '@hapi/joi';

export default Joi.object({
    verse: Joi.array()
        .items({
            id: Joi.number().description('ID стиха'),
            number: Joi.number().description('Номер стиха'),
            text: Joi.string().description('Текст стиха'),
            chapter: Joi.number().description('Номер главы'),
            book: Joi.object({
                id: Joi.number().description('ID Книги'),
                testament_id: Joi.number().description('Завет, 1 - Ветхий, 2 - Новый'),
                title: Joi.string().description('Название'),
                short_title: Joi.string().description('Короткое название'),
                parts: Joi.number().description('Количество глав в книге'),
                code: Joi.string().description('Код книги'),
            }).description('Информация о книге'),
            translate: Joi.array()
                .items({
                    id: Joi.number().description('ID записи'),
                    code: Joi.string().description('Код языка'),
                    text: Joi.string().description('Текст'),
                    verse_id: Joi.number().description('ID родительского стиха'),
                })
                .description('Список переводов стиха'),
            parallel: Joi.array()
                .items({
                    id: Joi.number().description('ID записи'),
                    verse_id: Joi.number().description('ID паралельного стихаё'),
                })
                .description('Список паралельных стихов'),
            interpretation: Joi.array()
                .items({
                    comment: Joi.string().description('Текст толкования'),
                    added_by: Joi.object({}).description('Пользователь, добавивший толкование'),
                    edited_by: Joi.object({}).description('Пользователь, изменивший толкование'),
                    added_at: Joi.number().description('Дата добавления (unixtimestamp)'),
                    edited_at: Joi.number().description('Дата редактирования (unixtimestamp)'),
                    investigated: Joi.number().description('Исследование (0/1)'),
                })
                .description('Толкования стиха'),
            dictionaries: Joi.object({
                id: Joi.number().description('ID статьи'),
                type_id: Joi.number().description('ID словаря'),
                word: Joi.string().description('Слово'),
                letter: Joi.string().description('Первая буква'),
                description: Joi.string().description('Описание'),
            }).description('Словарные статьи'),
            bible_maps_points: Joi.object({
                id: Joi.number().description('ID карты'),
                title: Joi.string().description('Название'),
                description: Joi.string().description('Описание'),
                image_id: Joi.string().description('Изображение'),
                lon: Joi.string().description('Долгота'),
                lat: Joi.string().description('Широта'),
            }).description('Библейские карты'),
        })
        .description('Стих'),
}).required();
