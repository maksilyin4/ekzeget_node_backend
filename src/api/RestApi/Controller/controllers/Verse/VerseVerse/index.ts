import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Verse from '../../../../../../entities/Verse';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { book_id, chapter } = validatedParams;
    const verses = {
        verses: await Verse.find({
            select: ['id', 'number', 'text'],
            where: {
                book_id: book_id,
                chapter: chapter,
            },
        }),
    };
    return verses;
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
