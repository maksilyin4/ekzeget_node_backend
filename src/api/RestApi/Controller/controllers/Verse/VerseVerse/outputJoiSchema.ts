import * as Joi from '@hapi/joi';

/*
export default Joi.object({
    verses: Joi.array()
        .items({
            id: Joi.number().description('ID стиха'),
            number: Joi.number().description('Номер стиха'),
            text: Joi.string().description('Текст стиха'),
            chapter: Joi.number().description('Номер главы'),
            book: Joi.object({
                id: Joi.number().description('ID Книги'),
                testament_id: Joi.number().description('Завет, 1 - Ветхий, 2 - Новый'),
                title: Joi.string().description('Название'),
                short_title: Joi.string().description('Короткое название'),
                parts: Joi.number().description('Количество глав в книге'),
                code: Joi.string().description('Код книги'),
            }).description('Информация о книге'),
            translate: Joi.array()
                .items({
                    id: Joi.number().description('ID записи'),
                    code: Joi.string().description('Код языка'),
                    text: Joi.string().description('Текст'),
                    verse_id: Joi.number().description('ID родительского стиха'),
                })
                .description('Список переводов стиха (опционально)'),
            parallel: Joi.array()
                .items({
                    id: Joi.number().description('ID записи'),
                    verse_id: Joi.number().description('ID паралельного стиха'),
                })
                .description('Список паралельных стихов (опционально)'),
            dictionaries: Joi.object({
                id: Joi.number().description('ID статьи'),
                type_id: Joi.number().description('ID словаря'),
                word: Joi.string().description('Слово'),
                letter: Joi.string().description('Первая буква'),
                description: Joi.string().description('Описание'),
            }).description('Словарные статьи'),
            bible_maps_points: Joi.object({
                id: Joi.number().description('ID карты'),
                title: Joi.string().description('Название'),
                description: Joi.string().description('Описание'),
                image_id: Joi.string().description('Изображение'),
                lon: Joi.string().description('Долгота'),
                lat: Joi.string().description('Широта'),
            }).description('Библейские карты'),
        })
        .description('Список стихов'),
    pages: Joi.object({
        currentPage: Joi.number().description('Текущая страница'),
        totalCount: Joi.number().description('Количество умолчанию'),
        defaultPageSize: Joi.number().description('Количество элементов на странице по умолчанию'),
    }).description('Информация о возможной постраничной навигации'),
}).required();*/

/**
 * Использую часть функционала в админке во время редактирования вопросов викторины, по быстрому накидал
 функционал необходимый мне. То что сделал, совпадает с ответом оригинального прода, можно делать
 нормальную версию функционала не боясь ничего сломать в админке.
 */
export default Joi.object({
    verses: Joi.array().items({
        id: Joi.number().description('ID стиха'),
        number: Joi.number().description('Номер стиха'),
        text: Joi.string().description('Текст стиха'),
    }),
});
