import * as Joi from '@hapi/joi';

export default Joi.object({
    book_id: Joi.number()
        .description('ID Книги (опционально)')
        .required(),
    chapter: Joi.number()
        .description('Номер главы (опционально)')
        .required(),
    translate: Joi.number().description('Показать переводы стиха (опционально 0/1)'),
    parallel: Joi.number().description('Показать паралельные стихи (опционально 0/1)'),
    page: Joi.number().description('Номер страницы (опционально, по умолчанию 1)'),
    'per-page': Joi.number().description(
        'Количество элементов на странице (опционально, по умолчанию 20)'
    ),
});
