/**
 * @api {get} /verse/:book_id/:chapter/?translate=:translate&parallel:parallel&page=:page&per-page=:per-page Стихи
 * @apiVersion 0.1.0
 * @apiName verse
 * @apiGroup Verse
 *
 *
 * @apiDescription Списки стихов
 * @apiPermission All
 *
 * @apiParam {Integer}  book_id     ID Книги (опционально)
 * @apiParam {Integer}  chapter     Номер главы (опционально)
 * @apiParam {Integer}  translate   Показать переводы стиха (опционально 0/1)
 * @apiParam {Integer}  parallel    Показать паралельные стихи (опционально 0/1)
 * @apiParam {Integer}  page        Номер страницы (опционально, по умолчанию 1)
 * @apiParam {Integer}  per-page    Количество элементов на странице (опционально, по умолчанию 20)
 *
 *
 * @apiSuccess {Object[]} verses                        Список стихов
 * @apiSuccess {Integer}  verses.id                     ID стиха
 * @apiSuccess {Integer}  verses.number                 Номер стиха
 * @apiSuccess {String}   verses.text                   Текст стиха
 * @apiSuccess {Integer}  verses.chapter                Номер главы
 *
 * @apiSuccess {Object}   verses.book                   Информация о книге
 * @apiSuccess {Integer}  verses.book.id                ID Книги
 * @apiSuccess {Integer}  verses.book.testament_id      Завет, 1 - Ветхий, 2 - Новый
 * @apiSuccess {String}   verses.book.title             Название
 * @apiSuccess {String}   verses.book.short_title       Короткое название
 * @apiSuccess {Integer}  verses.book.parts             Количество глав в книге
 * @apiSuccess {String}   verses.book.code              Код книги
 *
 * @apiSuccess {Object[]} verses.translate              Список переводов стиха (опционально)
 * @apiSuccess {Integer}  verses.translate.id           ID записи
 * @apiSuccess {String}   verses.translate.code         Код языка
 * @apiSuccess {String}   verses.translate.text         Текст
 * @apiSuccess {Integer}  verses.translate.verse_id     ID родительского стиха
 *
 * @apiSuccess {Object[]} verses.parallel               Список паралельных стихов (опционально)
 * @apiSuccess {Integer}  verses.parallel.id            ID записи
 * @apiSuccess {Integer}  verses.parallel.verse_id      ID паралельного стиха
 *
 * @apiSuccess {Object}   verses.dictionaries             Словарные статьи
 * @apiSuccess {Integer}  verses.dictionaries.id          ID статьи
 * @apiSuccess {Integer}  verses.dictionaries.type_id     ID словаря
 * @apiSuccess {String}   verses.dictionaries.word        Слово
 * @apiSuccess {String}   verses.dictionaries.letter      Первая буква
 * @apiSuccess {String}   verses.dictionaries.description Описание
 *
 * @apiSuccess {Object}   verses.bible_maps_points             Библейские карты
 * @apiSuccess {Integer}  verses.bible_maps_points.id          ID карты
 * @apiSuccess {String}   verses.bible_maps_points.title       Название
 * @apiSuccess {String}   verses.bible_maps_points.description Описание
 * @apiSuccess {String}   verses.bible_maps_points.image_id    Изображение
 * @apiSuccess {String}   verses.bible_maps_points.lon         Долгота
 * @apiSuccess {String}   verses.bible_maps_points.lat         Широта
 *
 * @apiSuccess {Object}   pages                         Информация о возможной постраничной навигации
 * @apiSuccess {Integer}  pages.currentPage             Текущая страница
 * @apiSuccess {Integer}  pages.totalCount              Количество умолчанию
 * @apiSuccess {Integer}  pages.defaultPageSize         Количество элементов на странице по умолчанию
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
