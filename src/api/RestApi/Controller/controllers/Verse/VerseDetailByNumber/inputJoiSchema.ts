import * as Joi from '@hapi/joi';

export default Joi.object({
    book_code: Joi.string()
        .description('Код книги')
        .required(),
    chapter: Joi.string()
        .description('Номер главы')
        .required(),
    number: Joi.string()
        .description('Номер стиха')
        .required(),
}).required();
