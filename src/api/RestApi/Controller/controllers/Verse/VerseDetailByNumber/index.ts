import * as Joi from '@hapi/joi';
import 'joi-extract-type';
import { FindOneOptions, In } from 'typeorm';
import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import { extractNumber } from '../../../../../../libs/helpers/urlParsers';
import { verseDetailByNumberPresenter } from './presenter';
import Book from '../../../../../../entities/Book';
import Verse from '../../../../../../entities/Verse';
import Parallel from '../../../../../../entities/Parallel';
import DictionaryVerse from '../../../../../../entities/DictionaryVerse';
import Dictionary from '../../../../../../entities/Dictionary';
import VerseTranslate from '../../../../../../entities/VerseTranslate';
import BibleMapsVerse from '../../../../../../entities/BibleMapsVerse';
import BibleMapsPoints from '../../../../../../entities/BibleMapsPoints';
import { ServerError } from '../../../../../../libs/ErrorHandler';
import returnCodeOrIdConditionObject from '../../../../../../libs/helpers/returnCodeOrIdConditionObject';

const SYNODAL_TRANSLATION_CODE = 'st_text';

const getParallels = async (parentVerseId: number): Promise<any[]> =>
    Parallel.find({
        where: {
            parentVerseId,
        },
        loadEagerRelations: true,
        relations: ['verse', 'verse.book'],
    });

const getDictionaryVerses = async (dictionaryVerses: DictionaryVerse[]): Promise<Dictionary[]> => {
    if (!dictionaryVerses.length) return [];

    return Dictionary.find({
        where: {
            id: In(dictionaryVerses.map(dv => dv.dictionaryId)),
        },
    });
};

const getBibleMapsPoints = async (
    bibleMapsVerses: BibleMapsVerse[]
): Promise<BibleMapsPoints[]> => {
    const pointIDs = bibleMapsVerses.map(({ point_id }) => point_id);
    if (!pointIDs.length) return [];

    return BibleMapsPoints.find({
        where: {
            id: In(pointIDs),
        },
    });
};

type ConditionType = (vt: VerseTranslate) => boolean;
const moveSynodalToIndex0 = (array: VerseTranslate[], condition: ConditionType) => {
    const index = array.findIndex(condition);
    if (index > -1) {
        const [element] = array.splice(index, 1);
        return [element, ...array];
    }
    return array;
};
const conditionFn: ConditionType = vt => vt.code === SYNODAL_TRANSLATION_CODE;

const getMainVerse = async (
    book: Book,
    chapterNumber: number,
    verseNumber: number
): Promise<Verse> => {
    const where: FindOneOptions<Verse> = {
        where: {
            book_id: book.id,
            chapter: chapterNumber,
            number: verseNumber,
        },
        relations: [
            'book',
            'verse_translates',
            'verse_translates.code2',
            'parallels2',
            'dictionary_verses',
            'bible_maps_verses',
        ],
        loadEagerRelations: true,
    };

    let mainVerse = await Verse.findOne(where);
    if (!mainVerse) {
        throw new ServerError(`Стих не найден [${book.id}|${chapterNumber}|${verseNumber}]`, 404);
    }

    if (!mainVerse.verse_translates) {
        return mainVerse;
    }

    const synodal = mainVerse.verse_translates.find(conditionFn);
    if (synodal) {
        mainVerse.verse_translates = moveSynodalToIndex0(mainVerse.verse_translates, conditionFn);
        return mainVerse;
    }

    await VerseTranslate.create({
        code: SYNODAL_TRANSLATION_CODE,
        text: mainVerse.text,
        verse_id: mainVerse.id,
    }).save();

    mainVerse = await Verse.findOne(where);
    mainVerse.verse_translates = moveSynodalToIndex0(mainVerse.verse_translates, conditionFn);

    return mainVerse;
};

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { book_code, chapter, number } = validatedParams;

    const book = await Book.findOne({
        where: returnCodeOrIdConditionObject(book_code),
    });
    if (!book) {
        throw new ServerError(`Книга не найдена [${book_code}]`, 404);
    }

    const chapterNumber = extractNumber(chapter);
    const verseNumber = extractNumber(number);

    const mainVerse = await getMainVerse(book, chapterNumber, verseNumber);

    const [parallels, dictionaries, bible_maps_points] = await Promise.all([
        getParallels(mainVerse.id),
        getDictionaryVerses(mainVerse.dictionary_verses),
        getBibleMapsPoints(mainVerse.bible_maps_verses),
    ]);

    return verseDetailByNumberPresenter({
        mainVerse,
        parallels,
        dictionaries,
        bible_maps_points,
    });
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
