/**
 * @api {get} /verse/detail-by-number/:book_code/:chapter/:number   Детальный стих по номеру
 * @apiVersion 0.1.0
 * @apiName detailByNumber
 * @apiGroup Verse
 *
 *
 * @apiDescription Списки стихов
 * @apiPermission All
 *
 * @apiParam {String}   book_code                      Код книги
 * @apiParam {Integer}  chapter                        Номер главы
 * @apiParam {Integer}  number                         Номер стиха
 *
 *
 * @apiSuccess {Object[]} verse                         Стих
 * @apiSuccess {Integer}  verse.id                     ID стиха
 * @apiSuccess {Integer}  verse.number                 Номер стиха
 * @apiSuccess {String}   verse.text                   Текст стиха
 * @apiSuccess {Integer}  verse.chapter                Номер главы
 * @apiSuccess {Object}   verse.book                   Информация о книге
 * @apiSuccess {Integer}  verse.book.id                ID Книги
 * @apiSuccess {Integer}  verse.book.testament_id      Завет, 1 - Ветхий, 2 - Новый
 * @apiSuccess {String}   verse.book.title             Название
 * @apiSuccess {String}   verse.book.short_title       Короткое название
 * @apiSuccess {Integer}  verse.book.parts             Количество глав в книге
 * @apiSuccess {String}   verse.book.code              Код книги
 * @apiSuccess {Object[]} verse.translate              Список переводов стиха
 * @apiSuccess {Integer}  verse.translate.id           ID записи
 * @apiSuccess {String}   verse.translate.code         Код языка
 * @apiSuccess {String}   verse.translate.text         Текст
 * @apiSuccess {Integer}  verse.translate.verse_id     ID родительского стиха
 * @apiSuccess {Object[]} verse.parallel               Список паралельных стихов
 * @apiSuccess {Integer}  verse.parallel.id            ID записи
 * @apiSuccess {Integer}  verse.parallel.verse_id      ID паралельного стихаё
 * @apiSuccess {Object[]} verse.interpretation         Толкования стиха
 * @apiSuccess {String}   verse.interpretation.comment       Текст толкования
 * @apiSuccess {Object}   verse.interpretation.added_by       Пользователь, добавивший толкование
 * @apiSuccess {Object}   verse.interpretation.edited_by       Пользователь, изменивший толкование
 * @apiSuccess {Integer}  verse.interpretation.added_at      Дата добавления (unixtimestamp)
 * @apiSuccess {Integer}  verse.interpretation.edited_at      Дата редактирования (unixtimestamp)
 * @apiSuccess {Integer}  verse.interpretation.investigated      Исследование (0/1)
 *
 * @apiError (400) {ApiException} error Авторизация не прошла
 * @apiError (403) {ApiException} error Доступ запрещен
 * @apiError (500) {ApiException} error Внутренняя ошибка сервера
 */
