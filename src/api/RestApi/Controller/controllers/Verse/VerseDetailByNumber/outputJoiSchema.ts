import * as Joi from '@hapi/joi';

export default Joi.object({
    verse: Joi.object({
        id: Joi.number().description('ID стиха'),
        number: Joi.number().description('Номер стиха'),
        book_id: Joi.number().description('ID книги'),
        text: Joi.string().description('Текст стиха'),
        chapter: Joi.number().description('Номер главы'),
        chapter_id: Joi.number().description('ID главы'),
        book_code: Joi.string().description('Код книги'),
        short: Joi.string().description('Код стиха'),

        book: Joi.object({
            id: Joi.number().description('ID Книги'),
            testament_id: Joi.number().description('Завет, 1 - Ветхий, 2 - Новый'),
            title: Joi.string().description('Название'),
            short_title: Joi.string().description('Короткое название'),
            parts: Joi.number().description('Количество глав в книге'),
            code: Joi.string().description('Код книги'),
            legacy_code: Joi.string(),
            menu: Joi.string(),
            author: Joi.string(),
            year: Joi.string().allow(''),
            place: Joi.string().allow(''),
            ext_id: Joi.number(),
            gospel: Joi.number(),
            sort: Joi.number(),
        }).description('Информация о книге'),

        translate: Joi.array()
            .items({
                id: Joi.number()
                    .description('ID записи')
                    .allow(null), // null у синодального перевода
                code: Joi.object({
                    id: Joi.number(),
                    number: Joi.number(),
                    code: Joi.string(),
                    title: Joi.string(),
                    select: Joi.string(),
                    search: Joi.string().allow(''),
                }),
                text: Joi.string()
                    .description('Текст')
                    .allow(''),
                verse_id: Joi.number().description('ID родительского стиха'),
            })
            .description('Список переводов стиха'),

        parallel: Joi.array()
            .items({
                verse_id: Joi.number(),
                short: Joi.string(),
                book_code: Joi.string(),
                chapter: Joi.number(),
                number: Joi.number(),
                text: Joi.string(),
            })
            .description('Список параллельных стихов'),

        dictionaries: Joi.array().items(
            Joi.object({
                id: Joi.number().description('ID'),
                type_id: Joi.number().description('ID словаря'),
                word: Joi.string().description('Слово'),
                letter: Joi.string().description('Первый символ слова'),
                description: Joi.string().allow('', null),
                variant: Joi.string()
                    .allow('')
                    .description('Варианты'),
                active: Joi.number(),
                code: Joi.string(),
            })
        ),

        bible_maps_points: Joi.array().items(
            Joi.object({
                active: Joi.number(),
                id: Joi.number(),
                title: Joi.string(),
                description: Joi.string(),
                image_id: Joi.string(),
                lon: Joi.number().description('Координаты долгота'),
                lat: Joi.number().description('Координаты широта'),
                created_by: Joi.number().allow(null),
                edited_by: Joi.number().allow(null),
                created_at: Joi.number().allow(null),
                edited_at: Joi.number().allow(null),
            }),
        ),
    }),
}).required();
