import * as Joi from '@hapi/joi';
import outputJoiSchema from './outputJoiSchema';
import Verse from '../../../../../../entities/Verse';
import VerseTranslate from '../../../../../../entities/VerseTranslate';
import Parallel from '../../../../../../entities/Parallel';
import Dictionary from '../../../../../../entities/Dictionary';
import { getVerseShortCode } from '../../../presenters/Verse/getVerseShortCode';
import { parallelsPresenter } from '../../../presenters/Parallel';

const verseTranslatesPresenter = (verseTranslate: VerseTranslate) => {
    const { code2, ...restT } = verseTranslate;
    return {
        ...restT,
        code: verseTranslate.code2,
    };
};

interface VerseDetailByNumberPresenterParams {
    /**
     * Стих, включающий в себя так же информацию
     * о переводах и их кодах (Verse.verse_translates),
     * о параллелях (Verse.parallels2),
     * о словарях стихов (Verse.dictionary_verses),
     */
    mainVerse: Verse;
    parallels: Parallel[];
    dictionaries: Dictionary[];
    bible_maps_points: any[];
}
export const verseDetailByNumberPresenter = (
    params: VerseDetailByNumberPresenterParams
): Joi.extractType<typeof outputJoiSchema> => {
    const {
        verse_translates,
        parallels2,
        dictionary_verses,
        book,
        bible_maps_verses,
        ...restV
    } = params.mainVerse;

    return {
        verse: {
            ...restV,
            book_code: params.mainVerse.book.code,
            short: getVerseShortCode(params.mainVerse.book, params.mainVerse),
            book,
            translate: verse_translates.map(vt => verseTranslatesPresenter(vt)),
            parallel: params.parallels.map(p => parallelsPresenter(p)),
            dictionaries: params.dictionaries,
            bible_maps_points: params.bible_maps_points,
        },
    };
};
