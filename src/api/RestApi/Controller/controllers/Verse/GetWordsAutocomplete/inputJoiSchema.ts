import * as Joi from '@hapi/joi';

export default Joi.object({
    id: Joi.number().description('ID словаря'),
    search: Joi.string()
        .description('Строка поиска')
        .allow(null, ''),
});
