import * as Joi from '@hapi/joi';
import 'joi-extract-type';

import inputJoiSchema from './inputJoiSchema';
import outputJoiSchema from './outputJoiSchema';
import Controller from '../../../../Controller';
import Dictionary from '../../../../../../entities/Dictionary';
import { Like } from 'typeorm';

const handler = async (
    validatedParams: Joi.extractType<typeof inputJoiSchema>
): Promise<Joi.extractType<typeof outputJoiSchema>> => {
    const { id, search } = validatedParams;
    const words: Partial<Dictionary>[] = await Dictionary.find({
        select: ['id', 'word'],
        where: {
            type_id: id,
            word: Like(`%${search}%`),
        },
    });

    return { words: words };
};

export default new Controller(handler, inputJoiSchema, outputJoiSchema);
