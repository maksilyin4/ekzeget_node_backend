import * as Joi from '@hapi/joi';

export default Joi.object({
    words: Joi.array().items({
        id: Joi.number(),
        word: Joi.string(),
    }),
});
