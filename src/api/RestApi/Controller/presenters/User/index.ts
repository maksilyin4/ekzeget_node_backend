import User from '../../../../../entities/User';

export const userPresenter = (user: User) => {
    if (!user) return null;

    const {
        id,
        username,
        email,
        phone,
        status,
        created_at,
        logged_at,
    } = user;
    const {
        firstname,
        lastname,
        middlename,
        gender,
        birthday,
        avatar_base_url,
        avatar_path,
        country,
        city,
        about,
    } = user.userProfile ?? {};
    return {
        id,
        username,
        email,
        phone,
        status,
        created_at,
        logged_at,
        firstname,
        lastname,
        middlename,
        gender,
        birthday,
        avatar: avatar_base_url && avatar_path ? `${avatar_base_url}/${avatar_path}` : null,
        country,
        city,
        about,
    };
};
