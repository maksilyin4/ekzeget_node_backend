import UserNote from '../../../../../../entities/UserNote';
import { getBookShortCode } from '../../Book/getBookShortCode';

export const UserNotePresenter = (
    note: UserNote
): {
    book_code: string;
    book_id: number;
    chapter: number;
    id: number;
    number: number;
    short: string;
    text: string;
    user_id: number;
} => ({
    book_code: note.book.code,
    book_id: note.book.id,
    chapter: +note.chapter,
    id: note.id,
    number: +note.number,
    short: getBookShortCode(note.book, +note.chapter),
    text: note.text,
    user_id: note.userId,
});
