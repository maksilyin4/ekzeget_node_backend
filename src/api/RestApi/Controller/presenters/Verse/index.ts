import { getVerseShortCode } from './getVerseShortCode';
import { sortByKeyASC } from '../../../../../libs/helpers/sort';
import Verse from '../../../../../entities/Verse';
import BibleMapsPoints from '../../../../../entities/BibleMapsPoints';
import { In } from 'typeorm';
import { dictionaryPresenter } from '../Dictionary/dictionaryPresenter';

export const versePresenter = async (verse: Verse) => {
    const bible_maps_points = verse.bible_maps_verses.length
        ? await BibleMapsPoints.find({
            where: {
                id: In(verse.bible_maps_verses.map(({ point_id }) => point_id))
            }
        })
        : [];

    return {
        id: verse.id,
        number: verse.number,
        book_id: verse.book_id,
        text: verse.text,
        chapter: verse.chapter,
        chapter_id: verse.chapter_id,
        book_code: verse.book.code,
        short: getVerseShortCode(verse.book, verse),
        book: {
            id: verse.book.id,
            testament_id: verse.book.testament_id,
            title: verse.book.title,
            short_title: verse.book.short_title,
            parts: verse.book.parts,
            code: verse.book.code,
            legacy_code: verse.book.legacy_code,
            menu: verse.book.menu,
            author: verse.book.author,
            year: verse.book.year,
            place: verse.book.place,
            ext_id: verse.book.ext_id,
            gospel: verse.book.gospel,
            sort: verse.book.sort,
        },
        dictionaries: verse.dictionary_verses
            .map(dv => dictionaryPresenter(dv.dictionary))
            .sort((a, b) => sortByKeyASC(a, b, 'id')),
        bible_maps_points,
    };
};
