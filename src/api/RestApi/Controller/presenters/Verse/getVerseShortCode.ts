interface BookInfo {
    short_title: string;
}
interface VerseInfo {
    chapter: number;
    number: number;
}

export const getVerseShortCode = (
    { short_title }: BookInfo,
    { chapter, number }: VerseInfo
): string => {
    return `${short_title}. ${chapter}:${number}`;
};
