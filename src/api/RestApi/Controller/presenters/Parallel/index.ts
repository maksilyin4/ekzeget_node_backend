import Parallel from '../../../../../entities/Parallel';
import { getVerseShortCode } from '../Verse/getVerseShortCode';

export const parallelsPresenter = (parallel: Parallel) => {
    const { verse } = parallel;
    const { id, book, chapter, number, text } = verse;
    return {
        verse_id: id,
        short: getVerseShortCode(book, verse),
        book_code: book.code,
        chapter,
        number,
        text,
    };
};
