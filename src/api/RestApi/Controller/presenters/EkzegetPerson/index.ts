import EkzegetPerson from '../../../../../entities/EkzegetPerson';

export const ekzegetPersonPresenter = (ekzeget: EkzegetPerson) => ({
    id: ekzeget.id,
    name: ekzeget.name,
    code: ekzeget.code,
    century: ekzeget.century,
    active: ekzeget.active,
    ekzeget_type: {
        id: ekzeget.ekzeget_type.id,
        title: ekzeget.ekzeget_type.title,
        active: ekzeget.ekzeget_type.active,
        code: ekzeget.ekzeget_type.code,
    },
    investigated: false,
    interpretation: false,
    in_fav: false,
});
