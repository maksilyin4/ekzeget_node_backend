import UserVerseFav from '../../../../../entities/UserVerseFav';
import { getVerseShortCode } from '../Verse/getVerseShortCode';

export const UserVerseFavVersePresenter = (favoriteVerse: UserVerseFav) => {
    return {
        ...favoriteVerse,
        verse: {
            book_code: favoriteVerse.verse.book.code,
            book_id: favoriteVerse.verse.book.id,
            chapter: favoriteVerse.verse.chapter,
            chapter_id: favoriteVerse.verse.chapter_id,
            id: favoriteVerse.verse.id,
            number: favoriteVerse.verse.number,
            short: getVerseShortCode(favoriteVerse.verse.book, favoriteVerse.verse),
            text: favoriteVerse.verse.text,
        },
    };
};
