import { getBookShortCode } from './getBookShortCode';

describe('getBookShortCode', () => {
    const book = { short_title: 'ST' };
    const chapter = 11;
    const number = 22;

    test('should return short code with verse chapter', () => {
        const result = getBookShortCode(book, chapter);
        expect(result).toEqual('ST. 11');
    });

    test('should return short code with verse chapter and number', () => {
        const result = getBookShortCode(book, chapter, number);
        expect(result).toEqual('ST. 11:22');
    });
});
