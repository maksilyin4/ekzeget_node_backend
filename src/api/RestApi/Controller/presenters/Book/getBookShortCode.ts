interface BookInfo {
    short_title: string;
}

export const getBookShortCode = (
    { short_title }: BookInfo,
    chapter: number,
    number?: number
): string => (number ? `${short_title}. ${chapter}:${number}` : `${short_title}. ${chapter}`);
