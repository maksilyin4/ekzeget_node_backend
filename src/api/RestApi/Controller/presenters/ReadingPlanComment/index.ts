import { userPresenter } from '../User';
import ReadingPlanComment from '../../../../../entities/ReadingPlanComment';

export const readingPlanCommentPresenter = (
    comment: ReadingPlanComment,
    allComments: ReadingPlanComment[] = [],
) => {
    return {
        ...comment,
        author: userPresenter(comment.author),
        replies: allComments
            .filter(reply => reply.reply_to_comment_id === comment.id)
            .map(reply => readingPlanCommentPresenter(reply, allComments)),
    };
};
