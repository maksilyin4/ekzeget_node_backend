import Dictionary from '../../../../../entities/Dictionary';

export const dictionaryPresenter = ({ id, type_id, word, letter, description, variant, active, code }: Dictionary) => ({
    id, type_id, word, letter, description, variant, active, code,
})
