import Interpretation from '../../../../../entities/Interpretation';
import { ekzegetPersonPresenter } from '../EkzegetPerson';
import TextFormatter from '../../../../../libs/TextFormatter';
import { userPresenter } from '../User';
import { versePresenter } from '../Verse';

export const interpretationInterpretationPresenter = async ({
    added_at,
    added_by_user,
    comment,
    edited_at,
    edited_by_user,
    ekzeget,
    id,
    interpretation_verses,
    investigated,
}: Interpretation) => {
    const verse = await Promise.all(
        interpretation_verses.map(async ({ verse }) => versePresenter(verse))
    );

    return {
        id: id,
        ekzeget: ekzegetPersonPresenter(ekzeget),
        comment: await new TextFormatter(comment).format(),
        added_by: userPresenter(added_by_user),
        edited_by: userPresenter(edited_by_user),
        added_at: added_at,
        edited_at: edited_at,
        investigated: investigated,
        verse,
    };
};
