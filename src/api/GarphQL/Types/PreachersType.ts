import { GraphQLID, GraphQLObjectType, GraphQLString } from 'graphql';

const PreachersType = new GraphQLObjectType({
    name: 'Preachers',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        code: { type: GraphQLString },
    }),
});

export default PreachersType;
