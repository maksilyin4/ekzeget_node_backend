import { GraphQLBoolean, GraphQLID, GraphQLObjectType, GraphQLString } from 'graphql';
import PHPUnserialize from 'php-unserialize';
import CelebrationExcuseType from './CelebrationExcuseType';
import CelebrationExcuse from '../../../entities/CelebrationExcuse';
import { In } from 'typeorm/index';

const ExcuseType = new GraphQLObjectType({
    name: 'Excuse',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        gospel: { type: GraphQLString },
        apostolic: { type: GraphQLString },
        code: { type: GraphQLString },
        parent: {
            type: CelebrationExcuseType,
            resolve: async source => {
                const unserializeParent = PHPUnserialize.unserialize(source.parent_str);
                const data = unserializeParent ? Object.values(unserializeParent) : [];
                return await CelebrationExcuse.findOne({ where: { id: In(data) } });
            },
        },
        active: { type: GraphQLBoolean },
    }),
});

export default ExcuseType;
