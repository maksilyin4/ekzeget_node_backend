import { GraphQLID, GraphQLObjectType, GraphQLString } from 'graphql';

export default new GraphQLObjectType({
    name: 'celebration',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
    }),
});
