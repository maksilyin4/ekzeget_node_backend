import { GraphQLList, GraphQLObjectType, GraphQLID, GraphQLString, GraphQLNonNull } from 'graphql';

const ImageType = new GraphQLObjectType({
    name: 'Image',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        url: { type: GraphQLString },
        alt: { type: GraphQLString },
        optimized: {
            args: {
                sizes: { type: GraphQLNonNull(GraphQLList(GraphQLString)) },
            },
            type: GraphQLList(
                new GraphQLObjectType({
                    name: 'OptimizedImage',
                    fields: {
                        size_code: {
                            type: GraphQLString,
                            resolve: async image => image['size_code'],
                        },
                        original: {
                            type: GraphQLString,
                            resolve: async image => image['original'],
                        },
                        webp: {
                            type: GraphQLString,
                            resolve: async image => image['webp'],
                        },
                    },
                })
            ),
        },
    }),
});

export default ImageType;
