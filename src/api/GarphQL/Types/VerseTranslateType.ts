import { GraphQLObjectType, GraphQLID, GraphQLString } from 'graphql';

const VersesTranslateType = new GraphQLObjectType({
    name: 'VersesTranslate',
    fields: () => ({
        id: { type: GraphQLID },
        code: { type: GraphQLString },
        text: { type: GraphQLString },
    }),
});

export default VersesTranslateType;
