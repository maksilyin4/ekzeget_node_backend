import { GraphQLObjectType, GraphQLID, GraphQLInt, GraphQLList } from 'graphql';
import BookType from './BookType';
import VerseType from './VerseType';
import Book from '../../../entities/Book';

const ChapterType = new GraphQLObjectType({
    name: 'Chapter',
    fields: () => ({
        id: { type: GraphQLID },
        book_id: { type: GraphQLInt },
        number: { type: GraphQLInt },
        book: {
            type: BookType,
            resolve: async source => {
                return await Book.findOne({ where: { id: source.book_id } });
            },
        },
        verses: { type: GraphQLList(VerseType) },
    }),
});

export default ChapterType;
