import { GraphQLID, GraphQLObjectType, GraphQLString } from 'graphql';
import { generateSlug } from '../../../libs/Rus2Translit/generateSlug';

const CelebrationExcuseType = new GraphQLObjectType({
    name: 'CelebrationExcuse',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        code: {
            type: GraphQLString,
            resolve: source => generateSlug(source.title),
        },
    }),
});

export default CelebrationExcuseType;
