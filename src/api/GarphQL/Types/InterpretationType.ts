import { GraphQLBoolean, GraphQLID, GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';
import UserType from './UserType';
import User from '../../../entities/User';
import VerseType from './VerseType';
import Interpretation from '../../../entities/Interpretation';

const InterpretationType = new GraphQLObjectType({
    name: 'Interpretation',
    fields: () => ({
        id: { type: GraphQLID },
        comment: { type: GraphQLString },
        added_at: { type: GraphQLString },
        edited_at: { type: GraphQLString },
        added: {
            type: UserType,
            resolve: async source => {
                return await User.findOne({ where: { id: source.added_by } });
            },
        },
        edited: {
            type: UserType,
            resolve: async source => {
                return await User.findOne({ where: { id: source.edited_by } });
            },
        },
        verses: {
            type: GraphQLList(VerseType),
            resolve: async source => {
                const { interpretation_verses } = await Interpretation.findOne({
                    where: { id: source.id, active: 1 },
                    relations: ['interpretation_verses', 'interpretation_verses.verse'],
                });

                return interpretation_verses.map(iv => iv.verse);
            },
        },
        active: { type: GraphQLBoolean },
    }),
});

export default InterpretationType;
