import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLInputObjectType,
} from 'graphql';
import AuthorType from './AuthorType';
import Media from '../../../entities/Media';
import MediaAuthor from '../../../entities/MediaAuthor';
import { In } from 'typeorm';
import ChapterType from './ChapterType';
import Chapters from '../../../entities/Chapters';

const BookType = new GraphQLObjectType({
    name: 'Book',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        short_title: { type: GraphQLString },
        code: { type: GraphQLString },
        author: { type: GraphQLString },
        menu: { type: GraphQLString },
        testament_id: { type: GraphQLInt },
        chapters: {
            type: GraphQLList(ChapterType),
            resolve: async ({ id }) => await Chapters.find({ where: { book_id: id } }),
        },
        media_authors: {
            type: GraphQLList(AuthorType),
            args: {
                medias_filter: {
                    type: new GraphQLInputObjectType({
                        name: 'medias_filter',
                        fields: {
                            type: { type: GraphQLString },
                        },
                    }),
                },
            },
            resolve: async ({ id: book_id }, { medias_filter }) => {
                const filterColumn = Object.keys(medias_filter)[0];
                const filterValue = Object.values(medias_filter)[0];

                /* Пардон за голый запрос. Если в тайпорм для Mysql появится способ
                  делать distinctOn запросы с createQueryBuilder или Find-оператором
                  маякните мне на почту: al.kowalsky7@gmail.com
                  Сейчас в доке предоставляется такая возможность только для postgres
                 **/
                const neededMedias = await Media.query(`
                    SELECT DISTINCT media.author_id FROM media
                    JOIN media_bible ON media_bible.media_id = media.id
                    WHERE ${filterColumn} = '${filterValue}'
                    AND media_bible.book_id = ${book_id}
                `);

                const neededAuthorIds = neededMedias.map(a => a.author_id);

                if (!neededAuthorIds.length) return [];
                return MediaAuthor.find({ where: { id: In(neededAuthorIds) } });
            },
        },
    }),
});

export default BookType;
