import { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLBoolean } from 'graphql';

const DictionaryTypeType = new GraphQLObjectType({
    name: 'DictionaryType',
    fields: () => ({
        id: { type: GraphQLID },
        code: { type: GraphQLString },
        title: { type: GraphQLString },
        alias: { type: GraphQLString },
        active: { type: GraphQLBoolean },
    }),
});

export default DictionaryTypeType;
