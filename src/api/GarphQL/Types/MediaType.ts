import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql';
import BookType from './BookType';
import AuthorType from './AuthorType';
import CategoryType from './CategoryType';
import GenreType from './GenreType';
import TagType from './TagType';
import ImageType from './ImageType';
import MediaBibleType from './MediaBibleType';
import CelebrationType from './CelebrationType';
import MediaBible from '../../../entities/MediaBible';
import Media from '../../../entities/Media';
import FileStorageItem from '../../../entities/FileStorageItem';
import MediaStatistics from '../../../entities/MediaStatistics';
import MediaComment from '../../../entities/MediaComment';
import MetaType from './MetaType';
import MediaCategory from '../../../entities/MediaCategory';
import MediaRelated from '../../../entities/MediaRelated';
import MediaTags from '../../../entities/MediaTags';
import MediaTag from '../../../entities/MediaTag';
import Config from '../../../entities/Config';

const MediaType = new GraphQLObjectType({
    name: 'media',
    fields: () => ({
        id: { type: GraphQLID },
        book_id: { type: GraphQLInt },
        number: { type: GraphQLInt },
        active: { type: GraphQLInt },
        title: { type: GraphQLString },
        grouping_name: { type: GraphQLString },
        text: { type: GraphQLString },
        allow_comment: { type: GraphQLBoolean },
        is_primary: { type: GraphQLBoolean },
        code: { type: GraphQLString },
        type: { type: GraphQLString },
        author: { type: AuthorType },
        categories: {
            type: GraphQLList(CategoryType),
            resolve: async ({ id }) => {
                const media = await Media.findOne({
                    where: { id },
                    relations: ['mediaCategories'],
                });
                return media.mediaCategories;
            },
        },
        main_category: {
            type: CategoryType,
            resolve: ({ main_category }) => MediaCategory.findOne(main_category),
        },
        genre: { type: GenreType },
        tags: {
            type: GraphQLList(TagType),
            resolve: async ({ id }) => {
                const media = await Media.findOne({
                    where: { id },
                    relations: ['media_tags'],
                });
                const tag_ids = media.media_tags.map(t => t.tag_id);
                if (!tag_ids.length) return [];

                const mediaTags: MediaTags[] = await MediaTags.createQueryBuilder('mt')
                    .leftJoinAndSelect('mt.tag', 'tag')
                    .where('mt.tag_id IN (:...tag_ids)', { tag_ids })
                    .getMany();

                const distinctMediaTags: MediaTag[] = [];
                mediaTags.forEach(mts => {
                    if (!distinctMediaTags.find(mt => mt.id === mts.tag.id)) {
                        distinctMediaTags.push(mts.tag);
                    }
                });

                return distinctMediaTags;
            },
        },
        era: { type: GraphQLString },
        location: { type: GraphQLString },
        art_title: { type: GraphQLString, resolve: async ({ art_title }) => art_title || '' },
        art_created_date: {
            type: GraphQLString,
            resolve: async ({ art_created_date }) => art_created_date || '',
        },
        length: { type: GraphQLString },
        art_director: {
            type: GraphQLString,
            resolve: async ({ art_director }) => art_director || '',
        },
        template: { type: GraphQLString },
        audio_href: {
            type: GraphQLString,
            resolve: async ({ properties }) => {
                const propertiesObj = JSON.parse(properties);
                return (propertiesObj && propertiesObj.audio_href) || '';
            },
        },
        fb2: {
            type: GraphQLString,
            resolve: async ({ properties }) => {
                const propertiesObj = JSON.parse(properties);
                return (propertiesObj && propertiesObj.fb2) || '';
            },
        },
        epub: {
            type: GraphQLString,
            resolve: async ({ properties }) => {
                const propertiesObj = JSON.parse(properties);
                return (propertiesObj && propertiesObj.epub) || '';
            },
        },
        pdf: {
            type: GraphQLString,
            resolve: async ({ properties }) => {
                const propertiesObj = JSON.parse(properties);
                return (propertiesObj && propertiesObj.pdf) || '';
            },
        },
        download_link: {
            type: GraphQLString,
            resolve: async ({ properties }) => {
                const propertiesObj = JSON.parse(properties);
                return (propertiesObj && propertiesObj.download_link) || '';
            },
        },
        video_href: {
            type: GraphQLString,
            resolve: async ({ properties }) => {
                const rankedVideoSources = await Config.getRankedVideoSources();
                const propertiesObj = JSON.parse(properties);
                if (propertiesObj) {
                    for (const videoSource of rankedVideoSources) {
                        if (propertiesObj[videoSource]) {
                            return propertiesObj[videoSource];
                        }
                    }
                }

                return '';
            },
        },
        year: { type: GraphQLString },
        image: {
            type: ImageType,
            resolve: async ({ image_id }) => {
                if (!image_id) return null;
                const { base_url, path } = await FileStorageItem.findOne(image_id);
                const url = base_url + '/' + path;
                return { url, id: image_id };
            },
        },
        sort: {
            type: GraphQLInt,
            resolve: async ({ id }) => {
                return (
                    await MediaRelated.findOne({
                        select: ['sort'],
                        where: { related_id: id },
                    })
                ).sort;
            },
        },
        related_media_types: {
            type: GraphQLList(GraphQLString),
            resolve: async ({ id }) => {
                const mediasRelated = await MediaRelated.find({
                    where: { media_id: id },
                    relations: ['related'],
                });

                const related = mediasRelated.map(mr => mr.related);

                return related.reduce((acc, media) => {
                    if (acc.includes(media.type)) return acc;
                    acc.push(media.type);
                    return acc;
                }, []);
            },
        },
        statistics_views_count: {
            type: GraphQLInt,
            resolve: async ({ id }) => {
                const stat = await MediaStatistics.findOne({ where: { media_id: id } });
                return stat ? stat.view_cnt : null;
            },
        },
        statistics_downloads_read_count: {
            type: GraphQLInt,
            resolve: async ({ id: media_id }) => {
                const stat = await MediaStatistics.findOne({
                    where: { media_id },
                    select: ['download_read_cnt'],
                });
                return (stat && stat.download_read_cnt) || 0;
            },
        },
        statistics_downloads_listen_count: {
            type: GraphQLInt,
            resolve: async ({ id: media_id }) => {
                const stat = await MediaStatistics.findOne({
                    where: { media_id },
                    select: ['download_listen_cnt'],
                });
                return (stat && stat.download_listen_cnt) || 0;
            },
        },
        comments_count: {
            type: GraphQLInt,
            resolve: async ({ id: media_id }) => {
                return await MediaComment.count({
                    where: { media_id, active: 1 },
                });
            },
        },
        media_bible: {
            type: GraphQLList(MediaBibleType),
            resolve: async ({ id: media_id }) => {
                return await MediaBible.find({
                    where: { media_id },
                    relations: ['verse'],
                });
            },
        },
        book: {
            type: BookType,
            resolve: async ({ id: media_id }) => {
                const mediaBible = await MediaBible.findOne({
                    where: { media_id },
                    relations: ['book'],
                });
                return mediaBible ? mediaBible.book : null;
            },
        },
        meta: {
            type: MetaType,
            resolve: async ({ title, social_description }) => ({
                title,
                description: social_description,
                h_one: title,
                keywords: '',
            }),
        },
        daily_media: { type: GraphQLString },
        celebration: { type: CelebrationType },
    }),
});

export default MediaType;
