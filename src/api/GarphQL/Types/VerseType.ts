import { GraphQLID, GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';
import ChapterType from './ChapterType';
import BookType from './BookType';
import VerseTranslateType from './VerseTranslateType';
import TranslateEnumType from './TranslateEnumType';
import Chapters from '../../../entities/Chapters';
import Book from '../../../entities/Book';
import VerseTranslate from '../../../entities/VerseTranslate';

const VerseType = new GraphQLObjectType({
    name: 'Verses',
    fields: () => ({
        id: { type: GraphQLID },
        number: { type: GraphQLInt },
        text: { type: GraphQLString },
        chapter_id: { type: GraphQLInt },
        chapter: {
            type: ChapterType,
            resolve: async source => {
                return await Chapters.findOne({ where: { id: source.chapter_id } });
            },
        },
        book_id: { type: GraphQLInt },
        book: {
            type: BookType,
            resolve: async source => {
                return await Book.findOne({ where: { id: source.book_id } });
            },
        },
        verse_translate: {
            type: GraphQLList(VerseTranslateType),
            args: { code: { type: new GraphQLList(TranslateEnumType) } },
            resolve: async (verse, { code }) => {
                return await VerseTranslate.find({
                    where: { verse_id: verse.id, code: code[0] },
                });
            },
        },
    }),
});

export default VerseType;
