import { GraphQLBoolean, GraphQLID, GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';
import VerseType from './VerseType';
import ExcuseType from './ExcuseType';
import PreacherType from './PreacherType';

const PreachingType = new GraphQLObjectType({
    name: 'Preaching',
    fields: () => ({
        id: { type: GraphQLID },
        theme: { type: GraphQLString },
        text: { type: GraphQLString },
        preacher: { type: PreacherType },
        excuse: { type: ExcuseType },
        verses: {
            type: GraphQLList(VerseType),
            resolve: async source => source.verses,
        },
        active: { type: GraphQLBoolean },
    }),
});

export default PreachingType;
