import { GraphQLScalarType } from 'graphql';

const TranslateEnumType = new GraphQLScalarType({
    name: 'TranslateEnumType',
    description: 'TranslateEnumType (в старой версии бека сюда тянутся code переводов)',
    serialize: value => typeof value === 'string',
});

export default TranslateEnumType;
