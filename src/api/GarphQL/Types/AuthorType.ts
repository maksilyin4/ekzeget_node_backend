import {
    GraphQLList,
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
    GraphQLInt,
    GraphQLNonNull,
} from 'graphql';
import MediaType from './MediaType';
import FileStorageItem from '../../../entities/FileStorageItem';
import Media from '../../../entities/Media';

const AuthorType = new GraphQLObjectType({
    name: 'Author',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        description: { type: GraphQLString },
        image: {
            type: GraphQLString,
            resolve: async ({ image: image_id }) => {
                if (!image_id) {
                    return '';
                }
                const imageObj = await FileStorageItem.findOne(image_id);
                if (!imageObj) return '';
                const { base_url, path } = imageObj;
                return base_url + '/' + path;
            },
        },
        book_media: {
            type: GraphQLList(MediaType),
            args: {
                book_id: { type: GraphQLNonNull(GraphQLInt) },
                limit: { type: GraphQLInt },
                afterId: { type: GraphQLInt },
            },
            resolve: async ({ id: author_id }, { book_id, limit = 20 }) => {
                const query = Media.createQueryBuilder('media');

                query.innerJoin('media.media_bibles', 'media_bible');

                query.where('media.author_id = :author_id', { author_id });
                query.andWhere('media_bible.book_id = :book_id', { book_id });

                query.orderBy('sort', 'ASC');
                query.limit(limit);

                return await query.getMany();
            },
        },
    }),
});

export default AuthorType;
