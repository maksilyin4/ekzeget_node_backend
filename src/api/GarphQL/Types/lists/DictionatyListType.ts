import { GraphQLInt, GraphQLList, GraphQLObjectType } from 'graphql';
import DictionaryType from '../DictionaryType';

export default new GraphQLObjectType({
    name: 'DictionaryList',
    fields: {
        items: {
            type: GraphQLList(DictionaryType),
        },
        total: {
            type: GraphQLInt,
        },
    },
});
