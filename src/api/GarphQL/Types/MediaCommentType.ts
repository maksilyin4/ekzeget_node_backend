import { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLInt } from 'graphql';
import MediaType from './MediaType';
import Media from '../../../entities/Media';

const MediaCommentType = new GraphQLObjectType({
    name: 'MediaComment',
    fields: () => ({
        id: { type: GraphQLID },
        comment: { type: GraphQLString },
        username: { type: GraphQLString },
        user_email: { type: GraphQLString },
        date: { type: GraphQLString },
        media_id: { type: GraphQLInt },
        media: { type: MediaType, resolve: ({ media_id }) => Media.findOne(media_id) },
    }),
});

export default MediaCommentType;
