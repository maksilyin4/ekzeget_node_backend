import { GraphQLList, GraphQLObjectType, GraphQLID, GraphQLString, GraphQLInt } from 'graphql';
import MediaType from './MediaType';
import MediaTags from '../../../entities/MediaTags';

const TagType = new GraphQLObjectType({
    name: 'Tag',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        code: { type: GraphQLString },
        created_at: { type: GraphQLInt },
        media: {
            type: GraphQLList(MediaType),
            args: {
                limit: { type: GraphQLInt },
                orderBy: { type: GraphQLString },
                order: { type: GraphQLInt },
            },
            resolve: async ({ id: tag_id }) => {
                const mediasOfTheTag = await MediaTags.find({
                    where: { tag_id },
                    relations: ['media'],
                    select: ['media'],
                });

                return mediasOfTheTag.map(i => i.media);
            },
        },
    }),
});

export default TagType;
