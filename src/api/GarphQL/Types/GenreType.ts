import { GraphQLList, GraphQLObjectType, GraphQLID, GraphQLString } from 'graphql';
import MediaType from './MediaType';

const GenreType = new GraphQLObjectType({
    name: 'Genre',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        description: { type: GraphQLString },
        book_media: {
            type: GraphQLList(MediaType),
            args: {},
        },
    }),
});

export default GenreType;
