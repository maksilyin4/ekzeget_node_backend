import { GraphQLID, GraphQLObjectType, GraphQLString } from 'graphql';

const EkzegetPersonType = new GraphQLObjectType({
    name: 'EkzegetPerson',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        code: { type: GraphQLString },
    }),
});

export default EkzegetPersonType;
