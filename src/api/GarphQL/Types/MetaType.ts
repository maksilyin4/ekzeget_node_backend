import { GraphQLObjectType, GraphQLID, GraphQLString } from 'graphql';

const MetaType = new GraphQLObjectType({
    name: 'Meta',
    fields: () => ({
        title: { type: GraphQLID },
        description: { type: GraphQLString },
        h_one: { type: GraphQLString },
        keywords: { type: GraphQLString },
    }),
});

export default MetaType;
