import { GraphQLObjectType, GraphQLID, GraphQLInt } from 'graphql';
import MediaType from './MediaType';
import BookType from './BookType';
import ChapterType from './ChapterType';
import VerseType from './VerseType';
import Book from '../../../entities/Book';
import Chapters from '../../../entities/Chapters';

const MediaBibleType = new GraphQLObjectType({
    name: 'media_bible',
    fields: () => ({
        id: { type: GraphQLID },
        media_id: { type: GraphQLInt },
        media: { type: MediaType },
        book_id: { type: GraphQLInt },
        book: {
            type: BookType,
            resolve: async ({ book_id }) => {
                return await Book.findOne(book_id);
            },
        },
        chapter_id: { type: GraphQLInt },
        chapter: {
            type: ChapterType,
            resolve: async ({ chapter_id }) => {
                return await Chapters.findOne(chapter_id);
            },
        },
        verse_id: { type: GraphQLInt },
        verse: { type: VerseType },
    }),
});

export default MediaBibleType;
