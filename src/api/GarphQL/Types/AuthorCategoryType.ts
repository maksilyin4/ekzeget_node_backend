import { GraphQLList, GraphQLObjectType, GraphQLID, GraphQLString, GraphQLInt } from 'graphql';
import AuthorType from './AuthorType';
import MediaAuthor from '../../../entities/MediaAuthor';

const AuthorCategoryType = new GraphQLObjectType({
    name: 'AuthorCategory',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        code: { type: GraphQLString },
        description: { type: GraphQLString },
        authors: {
            type: GraphQLList(AuthorType),
            args: {
                limit: { type: GraphQLInt },
                offset: { type: GraphQLInt },
                order: { type: GraphQLInt },
                orderBy: { type: GraphQLString },
            },
            resolve: async ({ id: category_id }, { limit, offset }) => {
                return await MediaAuthor.find({
                    where: { category_id },
                    take: limit || 20,
                    skip: offset || 0,
                });
            },
        },
    }),
});

export default AuthorCategoryType;
