import { GraphQLID, GraphQLObjectType, GraphQLString } from 'graphql';

const PreacherType = new GraphQLObjectType({
    name: 'Preacher',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        code: { type: GraphQLString },
    }),
});

export default PreacherType;
