import { GraphQLID, GraphQLObjectType, GraphQLString } from 'graphql';

const QuizQuestionsType = new GraphQLObjectType({
    name: 'QuizQuestions',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        description: { type: GraphQLString },
        comment: { type: GraphQLString },
        code: { type: GraphQLString },
    }),
});

export default QuizQuestionsType;
