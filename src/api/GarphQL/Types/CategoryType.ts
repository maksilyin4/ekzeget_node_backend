import { GraphQLList, GraphQLObjectType, GraphQLID, GraphQLString, GraphQLInt } from 'graphql';
import MetaType from './MetaType';
import MediaCategory from '../../../entities/MediaCategory';
import Meta from '../../../entities/Meta';

const CategoryType = new GraphQLObjectType({
    name: 'Category',
    fields: () => ({
        id: { type: GraphQLID },
        title: { type: GraphQLString },
        code: { type: GraphQLString },
        single_template: { type: GraphQLInt },
        template_code: { type: GraphQLString },
        description: { type: GraphQLString },
        parent: {
            type: CategoryType,
            resolve: ({ parent_id }) => MediaCategory.findOne(parent_id),
        },
        children: {
            type: GraphQLList(CategoryType),
            resolve: ({ id: parent_id }) => MediaCategory.find({ where: { parent_id } }),
        },
        meta: {
            type: MetaType,
            resolve: ({ code }) => Meta.findOne({ where: { code } }),
        },
    }),
});

export default CategoryType;
