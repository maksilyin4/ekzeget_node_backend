import { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLBoolean } from 'graphql';
import DictionaryTypeType from './DictionaryTypeType';
import TextFormatter from '../../../libs/TextFormatter';
import DictionaryTypeEntity from '../../../entities/DictionaryType';

const DictionaryType = new GraphQLObjectType({
    name: 'Dictionary',
    fields: () => ({
        id: { type: GraphQLID },
        code: { type: GraphQLString },
        word: { type: GraphQLString },
        type: {
            type: DictionaryTypeType,
            resolve: ({ type_id }) => DictionaryTypeEntity.findOne(type_id),
        },
        description: {
            type: GraphQLString,
            resolve: async ({ description }) => await new TextFormatter(description).format(),
        },
        active: { type: GraphQLBoolean },
    }),
});

export default DictionaryType;
