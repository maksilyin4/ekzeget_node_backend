import { GraphQLSchema } from 'graphql';
import Query from './query';

export default class GraphQL {
    private _schema;

    init() {
        this._schema = new GraphQLSchema({ query: Query });
    }

    get schema() {
        return this._schema;
    }
}
