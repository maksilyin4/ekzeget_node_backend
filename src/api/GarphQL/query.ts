import { GraphQLObjectType } from 'graphql';

import CategoryQuery from './Queries/CategoryQuery';
import BookQuery from './Queries/BookQuery';
import VerseListQuery from './Queries/VerseListQuery';
import ChapterQuery from './Queries/ChapterQuery';
import VerseQuery from './Queries/VerseQuery';
import MediaQuery from './Queries/MediaQuery';
import MediaListQuery from './Queries/MediaListQuery';
import TagQuery from './Queries/TagQuery';
import AuthorCategoryQuery from './Queries/AuthorCategoryQuery';
import MediaCommentQuery from './Queries/MediaCommentQuery';
import DictionaryListQuery from './Queries/DictionaryListQuery';
import InterpretationListQuery from './Queries/InterpretationListQuery';
import PreachingListQuery from './Queries/PreachingListQuery';
import QuizQuestionsListQuery from './Queries/QuizQuestionsListQuery';
import RelatedMediaQuery from './Queries/RelatedMediaQuery';
import EkzegetPersonQuery from './Queries/EkzegetPersonQuery';
import PreachersQuery from './Queries/PreachersQuery';
import MediasDailyQuery from './Queries/MediasDailyQuery';

const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {
        category: CategoryQuery,
        book: BookQuery,
        dictionaries: DictionaryListQuery,
        verses: VerseListQuery,
        verse: VerseQuery,
        chapter: ChapterQuery,
        interpretations: InterpretationListQuery,
        medias: MediaListQuery,
        media: MediaQuery,
        medias_daily: MediasDailyQuery,
        tag: TagQuery,
        author_category: AuthorCategoryQuery,
        comment: MediaCommentQuery,
        preaching_list: PreachingListQuery,
        queeze_questions_list: QuizQuestionsListQuery,
        related_media: RelatedMediaQuery,
        ekzeget_person: EkzegetPersonQuery,
        preachers: PreachersQuery,
    },
});

export default Query;
