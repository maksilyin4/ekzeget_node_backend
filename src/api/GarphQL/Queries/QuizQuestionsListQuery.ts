import { GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';
import QuizQuestionsType from '../Types/QuizQuestionsType';
import QueezeQuestions from '../../../entities/QueezeQuestions';
import { Brackets } from 'typeorm';
import SphinxSearch from '../../../database/SphinxSearch';

export default {
    type: new GraphQLObjectType({
        name: 'QuizQuestionsList',
        fields: {
            items: {
                type: GraphQLList(QuizQuestionsType),
            },
            total: {
                type: GraphQLInt,
            },
        },
    }),
    args: {
        limit: { type: GraphQLInt },
        offset: { type: GraphQLInt },
        connected_to_book_id: { type: GraphQLInt },
        connected_to_chapter_id: { type: GraphQLInt },
        connected_to_verse_id: { type: GraphQLList(GraphQLInt) },
        order: { type: GraphQLInt },
        orderBy: { type: GraphQLString },
        search_query: { type: GraphQLString },
        preacher_id: { type: GraphQLInt },
    },
    resolve: async function(root, args) {
        const {
            limit = 1,
            offset = 0,
            orderBy,
            order,
            search_query,
            connected_to_book_id,
            connected_to_chapter_id,
            connected_to_verse_id,
        } = args;

        const query = QueezeQuestions.createQueryBuilder('qq');

        query.leftJoin('qq.verse', 'v');
        query.leftJoin('v.book', 'b');
        switch (orderBy) {
            case 'sequence':
                query.orderBy('b.sort', 'ASC');
                query.addOrderBy('v.chapter', 'ASC');
                query.addOrderBy('v.number', 'ASC');
                break;
            case 'title':
                query.orderBy('qq.title', +order ? 'DESC' : 'ASC');
                break;
        }

        if (search_query) {
            try {
                const ids = await SphinxSearch.query({ match: search_query, against: 'quiz_question' });
                query
                    .andWhere('qq.id IN (:...ids)')
                    .addOrderBy('FIELD(qq.id, :...ids)')
                    .setParameter('ids', ids);
            } catch (e) {
                console.error('Sphinx error: ' + e.message);
                query.andWhere(
                    new Brackets(qb =>
                        qb
                            .where('qq.title LIKE :like', { like: `%${search_query}%` })
                            .orWhere('qq.description LIKE :like', { like: `%${search_query}%` })
                    )
                );
            }
        }

        if (connected_to_book_id) {
            query.andWhere('v.book_id = :connected_to_book_id', {
                connected_to_book_id,
            });
        }

        if (connected_to_chapter_id) {
            query.andWhere('v.chapter_id = :connected_to_chapter_id', {
                connected_to_chapter_id,
            });
        }

        if (connected_to_verse_id && connected_to_verse_id.length) {
            query.andWhere('v.id IN (:...connected_to_verse_id)', {
                connected_to_verse_id,
            });
        }

        query.limit(limit);
        query.offset(offset);

        const [items, total] = await query.getManyAndCount();

        return {
            items,
            total,
        };
    },
};
