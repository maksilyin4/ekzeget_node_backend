import { GraphQLInt, GraphQLList } from 'graphql';
import TagType from '../Types/TagType';
import MediaTag from '../../../entities/MediaTag';

export default {
    type: GraphQLList(TagType),
    args: {
        connected_to_chapter_id: { type: GraphQLInt },
    },
    resolve: async function() {
        return await MediaTag.find({ take: 100 });
    },
};
