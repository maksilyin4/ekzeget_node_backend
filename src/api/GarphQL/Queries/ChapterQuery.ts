import { GraphQLInt, GraphQLList } from 'graphql';
import ChapterType from '../Types/ChapterType';
import Chapters from '../../../entities/Chapters';

export default {
    type: GraphQLList(ChapterType),
    args: {
        book_id: { type: GraphQLInt },
        limit: { type: GraphQLInt },
    },
    resolve: async function(source, { book_id, limit }) {
        return await Chapters.find({
            where: { book_id },
            select: ['id', 'book_id', 'number'],
            take: limit,
        });
    },
};
