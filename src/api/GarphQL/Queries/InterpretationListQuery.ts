import { GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';
import InterpretationType from '../Types/InterpretationType';
import Interpretation from '../../../entities/Interpretation';
import SphinxSearch from '../../../database/SphinxSearch';

export default {
    type: new GraphQLObjectType({
        name: 'InterpretationList',
        fields: {
            items: {
                type: GraphQLList(InterpretationType),
            },
            total: {
                type: GraphQLInt,
            },
        },
    }),
    args: {
        limit: { type: GraphQLInt },
        offset: { type: GraphQLInt },
        connected_to_book_id: { type: GraphQLInt },
        connected_to_chapter_id: { type: GraphQLInt },
        connected_to_verse_id: { type: GraphQLList(GraphQLInt) },
        order: { type: GraphQLInt },
        orderBy: { type: GraphQLString },
        search_query: { type: GraphQLString },
    },
    resolve: async function(root, args) {
        const {
            limit = 1,
            offset = 0,
            orderBy,
            order,
            search_query,
            connected_to_book_id,
            connected_to_chapter_id,
            connected_to_verse_id,
        } = args;

        const query = Interpretation.createQueryBuilder('i');

        query.leftJoin('i.interpretation_verses', 'iv');
        query.leftJoin('iv.verse', 'v');
        query.leftJoin('v.book', 'b');

        if (connected_to_book_id) {
            query.andWhere('b.id = :connected_to_book_id', {
                connected_to_book_id,
            });
        }

        if (connected_to_chapter_id) {
            query.andWhere('v.chapter = :connected_to_chapter_id', {
                connected_to_chapter_id,
            });
        }

        if (connected_to_verse_id && connected_to_verse_id.length) {
            query.andWhere('v.id IN (:...connected_to_verse_id)', {
                connected_to_verse_id,
            });
        }

        switch (orderBy) {
            case 'sequence':
                query.orderBy('b.sort', 'ASC');
                query.addOrderBy('v.chapter', 'ASC');
                query.addOrderBy('v.number', 'ASC');
                break;
            case 'title':
                query.orderBy('i.comment', +order ? 'DESC' : 'ASC');
                break;
        }

        if (search_query) {
            try {
                const ids = await SphinxSearch.query({ match: search_query, against: 'interpretation' });
                query
                    .andWhere('i.id IN (:...ids)')
                    .addOrderBy('FIELD(i.id, :...ids)')
                    .setParameter('ids', ids);
            } catch (e) {
                console.error('Sphinx error: ' + e.message);
                query.andWhere(`i.comment LIKE :like`, { like: `%${search_query}%` });
            }
        }

        query.limit(limit);
        query.offset(offset);

        const [items, total] = await query.getManyAndCount();

        return {
            items,
            total,
        };
    },
};
