import CategoryType from '../Types/CategoryType';
import { GraphQLID, GraphQLInt, GraphQLList, GraphQLString } from 'graphql';
import MediaCategory from '../../../entities/MediaCategory';

export default {
    type: GraphQLList(CategoryType),
    args: {
        id: { type: GraphQLID },
        parent_id: { type: GraphQLInt },
        code: { type: GraphQLString },
        template_code: { type: GraphQLString },
        single_template: { type: GraphQLInt },
    },
    resolve: async function(root, { code }) {
        if (code) return await MediaCategory.find({ where: { code, active: 1 } });
        return await MediaCategory.find({ where: { parent_id: 0, active: 1 } });
    },
};
