import { GraphQLBoolean, GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';
import Media from '../../../entities/Media';
import MediaType from '../Types/MediaType';
import { Brackets } from 'typeorm';
import SphinxSearch from '../../../database/SphinxSearch';

const MEDIA_SELECTION_FIELDS = [
    'media.id',
    'media.title',
    'media.title_for_sort',
    'media.social_description',
    'media.author_id',
    'media.genre_id',
    'media.image_id',
    'media.created_by',
    'media.edited_by',
    'media.created_at',
    'media.edited_at',
    'media.allow_comment',
    'media.active',
    'media.year',
    'media.code',
    'media.type',
    // 'media.text', // text selection drastically affects performance
    'media.location',
    'media.era',
    'media.properties',
    'media.grouping_name',
    'media.template',
    'media.sort',
    'media.is_primary',
    'media.art_title',
    'media.art_created_date',
    'media.art_director',
    'media.length',
    'media.main_category',
    'media.daily_media',
    'media.celebration_id',
    'media.reading_apostolic_id',
    'media.reading_gospel_id',
    'media.reading_post_id',
    'media.reading_morning_id',
    'media.mineja_id',
];

export default {
    type: new GraphQLObjectType({
        name: 'MediasList',
        fields: {
            items: {
                type: GraphQLList(MediaType),
            },
            total: {
                type: GraphQLInt,
            },
        },
    }),
    args: {
        limit: { type: GraphQLInt },
        offset: { type: GraphQLInt },
        order: { type: GraphQLInt },
        orderBy: { type: GraphQLString },
        id: { type: GraphQLInt },
        code: { type: GraphQLString },
        search_query: { type: GraphQLString },
        type: { type: GraphQLString },
        category_code: { type: GraphQLString },
        author_id: { type: GraphQLInt },
        tag_ids: { type: GraphQLList(GraphQLInt) },
        category_ids: { type: GraphQLList(GraphQLInt) },
        category_codes: { type: GraphQLList(GraphQLString) },
        template: { type: GraphQLString },
        connected_to_book_id: { type: GraphQLInt },
        connected_to_chapter_id: { type: GraphQLInt },
        connected_to_chapter_only: { type: GraphQLBoolean },
        connected_to_verse_id: { type: GraphQLList(GraphQLInt) },
        connected_to_verse_only: { type: GraphQLBoolean },
        statistics_views_count: { type: GraphQLInt },
        statistics_downloads_read_count: { type: GraphQLInt },
        statistics_downloads_listen_count: { type: GraphQLInt },
        current_item_id: { type: GraphQLInt },
    },
    resolve: async function(root, args) {
        const {
            limit = 1,
            offset = 0,
            category_code,
            connected_to_book_id,
            connected_to_chapter_id,
            connected_to_verse_id,
            author_id,
            orderBy,
            order,
            tag_ids,
            search_query,
            connected_to_verse_only,
            connected_to_chapter_only,
            code,
        } = args;

        const query = Media.createQueryBuilder('media').select(MEDIA_SELECTION_FIELDS);

        query.leftJoinAndSelect('media.author', 'author');
        query.leftJoinAndSelect('media.genre', 'genre');

        if (code) {
            query.where('media.code = :code', { code });
        } else if (search_query) {
            query.where('media.is_primary = 1');
        }

        // TODO refactor building connection to Bible query
        if (connected_to_book_id) {
            query.innerJoin('media.media_bibles', 'media_bible');
            query.andWhere('media_bible.book_id = :connected_to_book_id', {
                connected_to_book_id,
            });
            if (connected_to_verse_only) {
                query.andWhere('media_bible.verse_id is not NULL');
            }
            if (connected_to_chapter_only) {
                query.andWhere('media_bible.verse_id is NULL');
                query.andWhere('media_bible.chapter_id is not NULL');
            }
        }

        if (connected_to_chapter_id) {
            query.andWhere('media_bible.chapter_id = :connected_to_chapter_id', {
                connected_to_chapter_id,
            });
            if (connected_to_verse_only) {
                query.andWhere('media_bible.verse_id is not NULL');
            }
            if (connected_to_chapter_only) {
                query.andWhere('media_bible.verse_id is NULL');
            }
        }

        if (connected_to_verse_id && connected_to_verse_id.length) {
            query.andWhere('media_bible.verse_id IN (:...connected_to_verse_id)', {
                connected_to_verse_id,
            });
        }

        if (author_id) {
            query.andWhere('media.author_id = :author_id', { author_id });
        }

        switch (orderBy) {
            case 'sequence':
                if (!connected_to_book_id) {
                    query.innerJoin('media.media_bibles', 'media_bible');
                }
                query.leftJoin('media_bible.verse', 'verse');
                query.leftJoin('media_bible.chapter', 'chapter');
                query.leftJoin('media_bible.book', 'book');
                query.orderBy('book.sort', 'ASC');
                query.addOrderBy('chapter.number', 'ASC');
                query.addOrderBy('verse.number', 'ASC');
                break;
            case 'created_at':
                query.andWhere('media.created_at is not NULL');
                query.orderBy('media.created_at', +order ? 'ASC' : 'DESC');
                break;
            case 'title':
                query.orderBy('media.title_for_sort', +order ? 'DESC' : 'ASC');
                break;
            case 'views':
                query.leftJoin('media.media_statistics', 'stat');
                query.orderBy('stat.view_cnt', +order ? 'DESC' : 'ASC');
                break;
            default:
                query.orderBy('media.sort', +order ? 'DESC' : 'ASC');
        }

        if (tag_ids && tag_ids.length) {
            query.innerJoin('media.media_tags', 'media_tags');
            query.andWhere('media_tags.tag_id IN (:...tag_ids)', {
                tag_ids,
            });
        }

        if (search_query) {
            try {
                let where = "type NOT IN ('A', 'C')";
                const params = [];
                if (author_id) {
                    where += ' AND author_id = ?';
                    params.push(author_id);
                }

                const ids = await SphinxSearch.query({
                    match: search_query,
                    against: 'media',
                    where,
                    params,
                });
                query
                    .andWhere('media.id IN (:...ids)')
                    .addOrderBy('FIELD(media.id, :...ids)')
                    .setParameter('ids', ids);
            } catch (e) {
                console.error('Sphinx error: ' + e.message);
                query.andWhere(
                    new Brackets(qb =>
                        qb
                            .where(`media.title LIKE :like`, { like: `%${search_query}%` })
                            .orWhere(`media.text LIKE :like`, { like: `%${search_query}%` })
                    )
                );
            }
        }

        if (category_code) {
            query.innerJoin('media_categories', 'mcats', 'mcats.media_id = media.id');
            query.leftJoin('media_category', 'mcat', 'mcats.category_id = mcat.id');
            query.andWhere('mcat.code = :category_code', { category_code });
        }

        query.andWhere('media.type NOT IN (:...excludedTypes)', { excludedTypes: ['C', 'A'] });
        query.groupBy('media.id');
        query.limit(limit);
        query.offset(offset);

        const [items, total] = await query.getManyAndCount();

        return {
            items,
            total,
        };
    },
};
