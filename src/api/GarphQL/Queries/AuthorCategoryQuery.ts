import { GraphQLID, GraphQLList } from 'graphql';
import AuthorCategoryType from '../Types/AuthorCategoryType';
import MediaAuthorCategory from '../../../entities/MediaAuthorCategory';

export default {
    type: GraphQLList(AuthorCategoryType),
    args: { id: { type: GraphQLID } },
    resolve: async function(root, { id }) {
        if (id) return await MediaAuthorCategory.findOne(id);
        return await MediaAuthorCategory.find();
    },
};
