import { GraphQLInt, GraphQLList, GraphQLString } from 'graphql';
import MediaType from '../Types/MediaType';

export default {
    type: GraphQLList(MediaType),
    args: {
        connected_to_chapter_id: { type: GraphQLInt },
    },
};
