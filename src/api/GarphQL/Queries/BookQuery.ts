import { GraphQLInt, GraphQLList } from 'graphql';
import BookType from '../Types/BookType';
import Book from '../../../entities/Book';

export default {
    type: GraphQLList(BookType),
    args: {
        id: { type: GraphQLInt },
        parent_id: { type: GraphQLInt },
    },
    resolve: async function(root, { id }) {
        if (id) return [await Book.findOne(id, { select: ['id', 'testament_id', 'menu'] })];
        return await Book.find({ select: ['id', 'testament_id', 'menu'] });
    },
};
