import { GraphQLList, GraphQLString } from 'graphql';
import MediaType from '../Types/MediaType';
import Media from '../../../entities/Media';
import Celebration from '../../../entities/Celebration';
import { getReadingsWithDate } from '../../../services/readings';
import { In } from 'typeorm/index';

export default {
    type: GraphQLList(MediaType),
    args: {
        daily_media: { type: GraphQLString },
    },
    resolve: async function(root, { daily_media }) {
        const query = Media.createQueryBuilder('media');

        query.leftJoinAndSelect('media.genre', 'genre');
        query.leftJoinAndSelect('media.celebration', 'celebration');
        query.andWhere('media.daily_media = :daily_media', { daily_media });
        query.andWhere('media.active = :active', { active: 1 });

        const mediasByDate = await query.getMany();
        const { readings, readingIdx } = await getReadingsWithDate(new Date(daily_media));
        const celebrationTitles = readings.title.map(t => t.trim());

        if (!celebrationTitles.length) {
            return mediasByDate;
        }

        const celebrationQuery = Celebration.createQueryBuilder('celebration');

        for (const i in celebrationTitles) {
            celebrationQuery.orWhere(`celebration.title like :title${i}`, {[`title${i}`]: celebrationTitles[i]})
        }

        const celebrationsIds = (await celebrationQuery.getMany()).map(({ id }) => id);

        let mediasByCelebrations = [];

        if (celebrationsIds.length) {
            mediasByCelebrations = await Media.find({
                where: { celebration_id: In(celebrationsIds) },
                relations: ['genre', 'celebration'],
            });
        }

        let mediasByReadings = [];
        const { apostolic, gospel, post, morning, mineja } = readingIdx;

        if (apostolic.length || gospel.length || post.length || morning.length || mineja.length) {
            const rQb = Media.getRepository().createQueryBuilder('media');

            apostolic.length && rQb.orWhere('media.reading_apostolic_id IN (:...apostolic)', { apostolic });
            gospel.length && rQb.orWhere('media.reading_gospel_id IN (:...gospel)', { gospel });
            post.length && rQb.orWhere('media.reading_post_id IN (:...post)', { post });
            morning.length && rQb.orWhere('media.reading_morning_id IN (:...morning)', { morning });
            mineja.length && rQb.orWhere('media.mineja_id IN (:...mineja)', { mineja });

            mediasByReadings = await rQb.getMany();
        }

        return Object.values(
            [...mediasByDate, ...mediasByCelebrations, ...mediasByReadings].reduce((res, media) => ({[media.id]: media, ...res}), {})
        );
    },
};
