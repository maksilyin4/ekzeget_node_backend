import { GraphQLInt, GraphQLList } from 'graphql';
import PreachersType from '../Types/PreachersType';
import Preacher from '../../../entities/Preacher';

export default {
    type: GraphQLList(PreachersType),
    args: {
        limit: { type: GraphQLInt },
    },
    resolve: async function(root, args) {
        const { limit = 1 } = args;

        return await Preacher.find({ select: ['id', 'name', 'code'], take: limit });
    },
};
