import DictionaryListType from '../Types/lists/DictionatyListType';
import { GraphQLInt, GraphQLString } from 'graphql';
import Dictionary from '../../../entities/Dictionary';
import SphinxSearch from '../../../database/SphinxSearch';

export default {
    type: DictionaryListType,
    args: {
        limit: { type: GraphQLInt },
        offset: { type: GraphQLInt },
        order: { type: GraphQLInt },
        orderBy: { type: GraphQLString },
        search_query: { type: GraphQLString },
    },
    resolve: async function(root, args) {
        const { limit = 1, offset = 0, orderBy, order, search_query } = args;

        const query = Dictionary.createQueryBuilder('d');

        switch (orderBy) {
            case 'sequence':
                query.leftJoin(
                    'dictionary_verse.verse_id',
                    'v',
                    'd.id = dictionary_verse.dictionary_id'
                );
                query.leftJoin('v.book', 'b');

                query.orderBy('b.sort', 'ASC');
                query.addOrderBy('v.chapter', 'ASC');
                query.addOrderBy('v.number', 'ASC');
                break;
            case 'title':
                query.orderBy('d.word', +order ? 'DESC' : 'ASC');
                break;
        }

        if (search_query) {
            try {
                const ids = await SphinxSearch.query({ match: search_query, against: 'dictionary' });
                query
                    .andWhere('d.id IN (:...ids)')
                    .addOrderBy('FIELD(d.id, :...ids)')
                    .setParameter('ids', ids);
            } catch (e) {
                console.error('Sphinx error: ' + e.message);
                query.andWhere(`d.description LIKE :like`, { like: `%${search_query}%` });
            }
        }

        query.limit(limit);
        query.offset(offset);

        const [items, total] = await query.getManyAndCount();

        return {
            items,
            total,
        };
    },
};
