import { GraphQLInt, GraphQLList } from 'graphql';
import Verse from '../../../entities/Verse';
import VerseType from '../Types/VerseType';

export default {
    type: GraphQLList(VerseType),
    args: {
        connected_to_chapter_id: { type: GraphQLInt },
    },
    resolve: async function(source, { connected_to_chapter_id }) {
        return await Verse.find({
            where: { chapter_id: connected_to_chapter_id },
            select: ['id', 'number'],
        });
    },
};
