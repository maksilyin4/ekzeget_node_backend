import { GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';
import VerseType from '../Types/VerseType';
import TranslateEnumType from '../Types/TranslateEnumType';
import Verse from '../../../entities/Verse';
import Chapters from '../../../entities/Chapters';
import SphinxSearch from '../../../database/SphinxSearch';

export default {
    type: new GraphQLObjectType({
        name: 'VersesList',
        fields: {
            items: {
                type: GraphQLList(VerseType),
            },
            total: {
                type: GraphQLInt,
            },
        },
    }),
    args: {
        limit: { type: GraphQLInt },
        offset: { type: GraphQLInt },
        order: { type: GraphQLInt },
        orderBy: { type: GraphQLString },
        connected_to_book_id: { type: GraphQLInt },
        connected_to_chapter_id: { type: GraphQLInt },
        connected_to_verse_id: { type: GraphQLList(GraphQLInt) },
        search_query: { type: GraphQLString },
        code: { type: GraphQLList(TranslateEnumType) },
    },
    resolve: async function(root, args) {
        const {
            limit,
            offset,
            search_query,
            connected_to_book_id,
            connected_to_chapter_id,
            connected_to_verse_id,
            orderBy,
            order,
        } = args;

        function createNotSearchingQuery() {
            const sqlQuery = Verse.createQueryBuilder('verse');

            sqlQuery.limit(limit || 20);
            sqlQuery.offset(offset || 0);

            sqlQuery.where('verse.text != :string', { string: '' });

            connected_to_book_id &&
                sqlQuery.andWhere('verse.book_id = :connected_to_book_id', {
                    connected_to_book_id,
                });

            connected_to_chapter_id &&
                sqlQuery.andWhere('verse.chapter_id = :connected_to_chapter_id', {
                    connected_to_chapter_id,
                });

            connected_to_verse_id &&
                sqlQuery.andWhere('verse.id = :connected_to_verse_id', {
                    connected_to_verse_id,
                });

            switch (orderBy) {
                case 'title':
                    sqlQuery.orderBy('verse.text', order ? 'DESC' : 'ASC');
                    break;
                case 'sequence':
                    sqlQuery.leftJoin(Chapters, 'chapter', 'chapter.id = verse.chapter_id');
                    sqlQuery.leftJoin('verse.book', 'book');
                    sqlQuery.orderBy('book.sort', 'ASC');
                    sqlQuery.addOrderBy('chapter.number', 'ASC');
                    sqlQuery.addOrderBy('verse.number', 'ASC');
                    break;
            }

            return sqlQuery;
        }

        if (!search_query) {
            const response = await createNotSearchingQuery().getManyAndCount();
            return {
                items: response[0],
                total: response[1],
            };
        }

        const sqlQueryByVersesWithSearch = createNotSearchingQuery();
        try {
            const ids = await SphinxSearch.query({ match: search_query, against: 'verse' });
            sqlQueryByVersesWithSearch
                .andWhere('verse.id IN (:...ids)')
                .addOrderBy('FIELD(verse.id, :...ids)')
                .setParameter('ids', ids);
        } catch (e) {
            console.error('Sphinx error: ' + e.message);
            sqlQueryByVersesWithSearch.andWhere(
                `match(verse.text) against(:like IN BOOLEAN MODE)`,
                {
                    like: `"${search_query}*"`,
                }
            );
        }

        const matchedVerses = await sqlQueryByVersesWithSearch.getManyAndCount();
        if (matchedVerses.length)
            return {
                items: matchedVerses[0],
                total: matchedVerses[1],
            };

        const sqlQueryInTranslatesWIthSearch = createNotSearchingQuery();
        sqlQueryInTranslatesWIthSearch.innerJoin('verse.verse_translates', 'vt');
        sqlQueryInTranslatesWIthSearch.groupBy('verse.id');
        sqlQueryInTranslatesWIthSearch.andWhere(`match(vt.text) against(:like IN BOOLEAN MODE)`, {
            like: `"${search_query}*"`,
        });

        const [items, total] = await sqlQueryInTranslatesWIthSearch.getManyAndCount();

        return {
            items,
            total,
        };
    },
};
