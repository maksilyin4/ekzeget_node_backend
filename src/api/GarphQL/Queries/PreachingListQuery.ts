import { GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLString } from 'graphql';
import PreachingType from '../Types/PreachingType';
import Preaching from '../../../entities/Preaching';
import SphinxSearch from '../../../database/SphinxSearch';

export default {
    type: new GraphQLObjectType({
        name: 'PreachingList',
        fields: {
            items: {
                type: GraphQLList(PreachingType),
            },
            total: {
                type: GraphQLInt,
            },
        },
    }),
    args: {
        preacher_id: { type: GraphQLInt },
        limit: { type: GraphQLInt },
        offset: { type: GraphQLInt },
        order: { type: GraphQLInt },
        orderBy: { type: GraphQLString },
        search_query: { type: GraphQLString },
        connected_to_book_id: { type: GraphQLInt },
        connected_to_chapter_id: { type: GraphQLInt },
        connected_to_verse_id: { type: GraphQLList(GraphQLInt) },
    },
    resolve: async function(root, args) {
        const {
            limit = 1,
            offset = 0,
            orderBy,
            order,
            search_query,
            connected_to_book_id,
            connected_to_chapter_id,
            connected_to_verse_id,
            preacher_id,
        } = args;

        const query = Preaching.createQueryBuilder('preaching');

        query.leftJoinAndSelect('preaching.preacher', 'preacher');
        query.leftJoinAndSelect('preaching.excuse', 'excuse');

        query.leftJoin('preaching.preaching_verses', 'pv');
        query.leftJoin('pv.verse', 'v');
        query.leftJoin('v.book', 'b');

        if (connected_to_book_id) {
            query.andWhere('v.book_id = :connected_to_book_id', {
                connected_to_book_id,
            });
        }

        if (connected_to_chapter_id) {
            query.andWhere('v.chapter = :connected_to_chapter_id', {
                connected_to_chapter_id,
            });
        }

        if (connected_to_verse_id) {
            query.andWhere('v.id = :connected_to_verse_id', {
                connected_to_verse_id,
            });
        }

        if (preacher_id) {
            query.andWhere('preaching.preacher_id = :preacher_id', {
                preacher_id,
            });
        }

        switch (orderBy) {
            case 'sequence':
                query.orderBy('b.sort', 'ASC');
                query.addOrderBy('v.chapter', 'ASC');
                query.addOrderBy('v.number', 'ASC');
                break;
            case 'title':
                query.orderBy('preaching.text', +order ? 'DESC' : 'ASC');
                break;
        }

        if (search_query) {
            try {
                const ids = await SphinxSearch.query({ match: search_query, against: 'preaching' });
                query
                    .andWhere('preaching.id IN (:...ids)')
                    .addOrderBy('FIELD(preaching.id, :...ids)')
                    .setParameter('ids', ids);
            } catch (e) {
                console.error('Sphinx error: ' + e.message);
                query.andWhere(`preaching.text LIKE :like`, { like: `%${search_query}%` });
            }
        }

        query.limit(limit);
        query.offset(offset);

        const [items, total] = await query.getManyAndCount();

        return {
            items,
            total,
        };
    },
};
