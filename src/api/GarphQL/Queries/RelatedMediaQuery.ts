import { GraphQLInt, GraphQLList, GraphQLString } from 'graphql';
import MediaType from '../Types/MediaType';
import MediaRelated from '../../../entities/MediaRelated';
import Media from '../../../entities/Media';

export default {
    type: GraphQLList(MediaType),
    args: {
        media_id: { type: GraphQLInt },
        type: { type: GraphQLString },
        code: { type: GraphQLString },
        limit: { type: GraphQLInt },
        offset: { type: GraphQLInt },
        order: { type: GraphQLInt },
        orderBy: { type: GraphQLString },
    },
    resolve: async (root, { media_id, type, limit = 100, offset = 0 }) => {
        const mediaRelated = await MediaRelated.find({
            where: { media_id },
            select: ['related_id', 'sort'],
            order: { sort: 'DESC' },
        });

        const relatedIds = mediaRelated.map(mr => mr.related_id);

        const query = Media.createQueryBuilder('media');

        if (!relatedIds.length) return [];
        query
            .where('media.id IN (:...relatedIds)', { relatedIds })
            .leftJoin(MediaRelated, 'mediaRelated', 'mediaRelated.related_id = media.id')
            .orderBy('mediaRelated.sort');

        if (type) {
            query.andWhere('media.type = :type', { type });
        }

        return await query.getMany();
    },
};
