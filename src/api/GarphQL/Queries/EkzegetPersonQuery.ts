import { GraphQLInt, GraphQLList } from 'graphql';
import EkzegetPersonType from '../Types/EkzegetPersonType';
import EkzegetPerson from '../../../entities/EkzegetPerson';

export default {
    type: GraphQLList(EkzegetPersonType),
    args: {
        limit: { type: GraphQLInt },
    },
    resolve: async function(root, args) {
        const { limit = 1 } = args;

        return await EkzegetPerson.find({ select: ['id', 'name', 'code'], take: limit });
    },
};
