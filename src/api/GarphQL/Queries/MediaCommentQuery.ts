import { GraphQLInt, GraphQLList } from 'graphql';
import MediaCommentType from '../Types/MediaCommentType';
import MediaComment from '../../../entities/MediaComment';

export default {
    type: GraphQLList(MediaCommentType),
    args: {
        offset: { type: GraphQLInt },
        limit: { type: GraphQLInt },
        media_id: { type: GraphQLInt },
        excludeIds: { type: GraphQLList(GraphQLInt) },
    },
    resolve: async function(source, { media_id, excludeIds, limit, offset }) {
        const query = MediaComment.createQueryBuilder('media_comment');

        query.limit(limit || 20);
        query.offset(offset || 0);

        query.where('media_comment.media_id = media_id', { media_id });
        if (excludeIds && excludeIds.length) {
            query.andWhere('media_comment.id not in (:...excludeIds)', { excludeIds });
        }

        return query.getMany();
    },
};
