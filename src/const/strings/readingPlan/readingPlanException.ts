export default {
    READING_PLAN_NOT_FOUND_MESSAGE: 'Неверные параметры',
    READING_PLAN_NOT_FOUND_NAME: 'План чтения не найден',
};
