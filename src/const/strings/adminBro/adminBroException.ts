export default {
    SAVE_CUSTOM_FIELD_ERROR: 'Внутрення ошибка сервера при сохранении изменений',
    DELETE_CUSTOM_FIELD_ERROR: 'Внутрення ошибка сервера при удалении связанных полей',
};
