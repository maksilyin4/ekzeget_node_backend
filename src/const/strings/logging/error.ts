export default {
    unsetEnvs: 'Unset envs ',
    // register
    badCaptcha: 'Введен неправильный код',
    missedDataForRegistration: 'Для регистрации введите имя, адрес почты и придумайте пароль',
    registerError: 'Ошибка регистрации',
    error401: 'Ошибка авторизации',
    // image manager
    storageConnectionError: 'Ошибка подключения к файловому хранилищу',
};
