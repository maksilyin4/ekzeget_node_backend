export enum WeekDays {
    // Воскресенье
    Sunday = 0,

    // Понедельник
    Monday = 1,

    // Вторник
    Tuesday = 2,

    // Среда
    Wednesday = 3,

    // Четверг
    Thursday = 4,

    // Пятница
    Friday = 5,

    // Суббота
    Saturday = 6,
}

/* Значения взяты из базы */

export enum TimelineEventsEnum {
    signup = 'signup',
    new = 'new',
    edit = 'edit',
}

export enum TimelineApplicationEnum {
    frontend = 'frontend',
    console = 'console',
    backend = 'backend',
}

export enum TimelineCategoriesEnum {
    user = 'user',
    interpretation = 'interpretation',
}
