import Database from './database';
import EnvironmentChecker from './core/EnvironmentChecker';
import { AppConstructor } from './core/App/BaseApp';
import SphinxSearch from './database/SphinxSearch';

export default class Server {
    constructor(
        private _App: AppConstructor,
        private _Database: typeof Database,
        private environmentChecker: EnvironmentChecker
    ) {}

    public async run() {
        const checkedEnvironment = this.environmentChecker.getCheckedEnvironment();
        const database = new this._Database(checkedEnvironment);
        await database.init();
        SphinxSearch.init();
        new this._App(checkedEnvironment, database.connection).init();
    }
}
