import Server from './Server';
import Database from './database';
import EnvironmentChecker from './core/EnvironmentChecker';
import AdminApp from './core/App/AdminApp';

const server = new Server(AdminApp, Database, new EnvironmentChecker());
server.run().catch(e => {
    console.error(e);
    process.exit(1);
});
