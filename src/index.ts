import Server from './Server';
import App from './core/App';
import Database from './database';
import EnvironmentChecker from './core/EnvironmentChecker';

const server = new Server(App, Database, new EnvironmentChecker());
server.run().catch(e => {
    console.error(e);
    process.exit(1);
});
