import nodemailer from 'nodemailer';
import EnvironmentManager from '../../managers/EnvironmentManager';
import { ServerError } from '../../libs/ErrorHandler';
import { EnvList } from '../../core/EnvironmentChecker/EnvironmentDTO';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import Mail from 'nodemailer/lib/mailer';

type EmailNotifierConfig = {
    emailServiceSourceAddress: string;
    emailServicePort: string;
    emailServiceHost: string;
    emailServiceLogin: string;
    emailServicePassword: string;
};

class EmailNotifier {
    private config;

    private service: nodemailer.Transporter;

    constructor(params: EmailNotifierConfig) {
        const { emailServiceHost, emailServicePort, emailServiceSourceAddress } = params;
        this.config = { emailServiceHost, emailServicePort, emailServiceSourceAddress };

        const options: string | SMTPTransport | SMTPTransport.Options = {
            host: emailServiceHost,
            port: +emailServicePort,
            secure: false, // true for port 465, false for other ports
        };

        if (EnvironmentManager.envs.ENV === EnvList.prod) {
            options.auth = {
                user: params.emailServiceLogin,
                pass: params.emailServicePassword,
            };
        }
        this.service = nodemailer.createTransport(options);
    }

    async send(params: Mail.Options) {
        const { from: source, to, text, subject, html } = params;
        const from = `"${source}" <${this.config.emailServiceSourceAddress}>`;

        return this.service
            .sendMail({
                from,
                to,
                text,
                subject,
                html,
            })
            .catch(err => {
                console.error('EmailNotifier: Ошибка отправки письма');
                throw new ServerError(err);
            });
    }
}

const { SMTP_HOST, SMTP_PORT, SMTP_SOURCE_MAIL, SMTP_LOGIN, SMTP_PASS } = EnvironmentManager.envs;

export default new EmailNotifier({
    emailServiceHost: SMTP_HOST,
    emailServicePort: SMTP_PORT,
    emailServiceSourceAddress: SMTP_SOURCE_MAIL,
    emailServiceLogin: SMTP_LOGIN,
    emailServicePassword: SMTP_PASS,
});
