import Mail from 'nodemailer/lib/mailer';
import mailing from '../../../const/strings/mailing';

const letters: {
    [x: string]: (mail: string, params: { [x: string]: any }) => Mail.Options;
} = {
    getRequestRegisterConfirm: (mail, { secret, clientHost }): Mail.Options => ({
        from: `${mailing.senderName}<${mailing.senderMail}>`,
        to: mail,
        text: `${mailing.register_text_confirm} ${clientHost}/registration-confirm/${secret}`,
        subject: mailing.resister_confirm_topic,
    }),
    getPasswordResetConfirm: (mail, { secret, clientHost }): Mail.Options => ({
        from: `${mailing.senderName}<${mailing.senderMail}>`,
        to: mail,
        text: `${mailing.password_reset_body} ${clientHost}/forgot-password/${secret}`,
        subject: mailing.password_reset_topic,
    }),
    contactsForm: (adminEmail, { senderName, body, mail, clientHost }): Mail.Options => ({
        from: `${mailing.senderName}<${mailing.senderMail}>`,
        to: adminEmail,
        text: `<p>${body}</p> <p>Отправитель: ${senderName}, ${mail}</p>`,
        subject: `Сообщение от ${clientHost}`,
    }),
};

export default letters;
