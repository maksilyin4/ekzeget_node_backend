import EnvironmentDTO from '../../core/EnvironmentChecker/EnvironmentDTO';
import EnvironmentChecker from '../../core/EnvironmentChecker';

class EnvironmentManager {
    constructor(private _envs: EnvironmentDTO) {}

    get envs() {
        return this._envs;
    }

    get baseUrl() {
        return `${envs.PROTOCOL}://${envs.HOST}`;
    }

    get ftpStorageAccessConfig() {
        return {
            host: envs.STORAGE_HOST,
            user: envs.STORAGE_FTP_USER,
            password: envs.STORAGE_FTP_PASS.replace(/"/g, ''),
        };
    }
}

const envs = new EnvironmentChecker().getCheckedEnvironment();

export default new EnvironmentManager(envs);
