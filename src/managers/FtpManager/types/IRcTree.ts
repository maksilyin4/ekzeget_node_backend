export default interface IRcTree {
    title: string;
    key: string;
    isLeaf: boolean;
    children?: IRcTree[];
}
