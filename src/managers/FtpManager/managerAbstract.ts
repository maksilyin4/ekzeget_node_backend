import EnvironmentManager from '../EnvironmentManager';
import { ServerError } from '../../libs/ErrorHandler';
import error from '../../const/strings/logging/error';
import * as ftp from 'basic-ftp';
import IRcTree from './types/IRcTree';

type FtpManagerConfig = {
    host: string;
    user: string;
    password: string;
};

export abstract class FtpManagerAbstract {
    protected client: ftp.Client;
    protected config: FtpManagerConfig;

    public constructor() {
        this.config = EnvironmentManager.ftpStorageAccessConfig;
        this.client = new ftp.Client();
    }

    protected async connect(): Promise<void> {
        try {
            if (this.client.closed) {
                await this.client.access(this.config);
            }
        } catch (e) {
            throw new ServerError(error.storageConnectionError, 500);
        }
    }

    protected async disconnect(): Promise<void> {
        await this.client.close();
    }

    public async getDirectoryContents(
        path: string,
        root = EnvironmentManager.envs.STORAGE_MEDIA_DIR
    ) {
        await this.connect();

        const contents = await this.client.list(root + path);

        const directoryTree: IRcTree[] = contents.map(fileInfo => ({
            title: fileInfo.name,
            key: path + '/' + fileInfo.name,
            isLeaf: fileInfo.type === 1,
        }));

        await this.disconnect();

        return directoryTree;
    }
}
