import IFtpFile from './types/IFtpFile';
import EnvironmentManager from '../../managers/EnvironmentManager';
import { FtpManagerAbstract } from './managerAbstract';
import { StringWriter } from 'basic-ftp/dist/StringWriter';

export default class FtpManager extends FtpManagerAbstract {
    public async uploadImageToStore(image: IFtpFile, pathTo: string) {
        await this.connect();
        const { path, name } = image;
        await this.client.ensureDir(EnvironmentManager.envs.STORAGE_ASSETS_DIR + pathTo);
        await this.client.uploadFrom(path, name);

        await this.disconnect();
    }

    public async uploadMediaFilesToStore(files: IFtpFile[], pathTo: string) {
        await this.connect();
        await this.client.ensureDir(EnvironmentManager.envs.STORAGE_MEDIA_DIR + pathTo);

        for (const { path, name } of files) {
            await this.client.uploadFrom(path, name);
        }

        await this.disconnect();
    }

    public async removeDir(pathTo: string) {
        await this.connect();

        await this.client.removeDir(EnvironmentManager.envs.STORAGE_MEDIA_DIR + pathTo);

        await this.disconnect();
    }

    public async getFileContents(path: string): Promise<string> {
        await this.connect();

        const buffer = new StringWriter();
        await this.client.downloadTo(buffer, path);

        await this.disconnect();

        return buffer.getText('utf8');
    }
}
