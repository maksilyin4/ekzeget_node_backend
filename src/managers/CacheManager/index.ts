import NodeCache from 'node-cache';

const nodeCache = new NodeCache();

class CacheManager {
    public async set(keyName: string, value: any, TTL?: number): Promise<boolean> {
        if (TTL) nodeCache.ttl(keyName, TTL);
        return nodeCache.set(keyName, value);
    }

    public async has(keyName: string): Promise<any> {
        return nodeCache.has(keyName);
    }

    public async get(keyName: string): Promise<any> {
        return nodeCache.get(keyName);
    }

    public async take(keyName: string): Promise<any> {
        return nodeCache.take(keyName);
    }

    public async clear(): Promise<void> {
        return nodeCache.flushAll();
    }
}

export default new CacheManager();
