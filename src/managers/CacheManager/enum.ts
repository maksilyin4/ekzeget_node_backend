export enum CacheManagerTTL {
    day = 86400,
    infinity = 0,
}

export enum CacheManagerGlobalKeys {
    books = 'books',
}
