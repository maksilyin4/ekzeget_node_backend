import bcrypt from 'bcryptjs';

import User from '../../../entities/User';
import { IAuthParams } from './types';
import UserToken from '../../../entities/UserToken';
import createMd5Hash from '../../../libs/helpers/createMd5Hash';
import RegisterService from '../RegisterService';
import { getManager } from 'typeorm/index';
import createRandomString from '../../../libs/helpers/createRandomString';
import secretsLength from '../../../const/numbers/secretsLength';
import getUnixSec from '../../../libs/helpers/getUnixSec';
import { ServerError } from '../../../libs/ErrorHandler';
import authStrings from '../../../const/strings/auth';

export default class AuthService {
    private _user: User;

    public async login({ email, password, device_info }: IAuthParams) {
        this._user = await User.findOne({
            where: [
                { email, status: User.statuses.active },
                { username: email, status: User.statuses.active },
            ],
        });

        if (!this._user) throw new ServerError(authStrings.unknownAuthData, 401);

        const isPassValid = await this.validatePassword(password, this._user.password_hash);
        if (!isPassValid) throw new ServerError(authStrings.unknownAuthData, 401);

        const { token } = await this.processUserTokenRecord(device_info);

        return { id: this._user.id, token };
    }

    public async processUserTokenRecord(device_info: string, user_id: number = this._user.id) {
        let userToken = await UserToken.findOne({
            where: { user_id, device_hash: createMd5Hash(device_info) },
        });

        if (userToken) {
            userToken = await this.updateUserTokenRecord(userToken.id, device_info);
        } else {
            const registerService = new RegisterService();
            userToken = await registerService.createUserTokenRecord(
                getManager(),
                device_info,
                user_id
            );
        }

        return userToken;
    }

    private async updateUserTokenRecord(token_id: number, device_info: string) {
        return await UserToken.create({
            id: token_id,
            type: UserToken.types.web,

            device_hash: createMd5Hash(device_info),
            token: createRandomString(secretsLength.accessToken),

            updated_at: getUnixSec(),
        }).save();
    }

    private async validatePassword(password: string, passwordHash: string) {
        return await bcrypt.compare(password, passwordHash);
    }
}
