export interface IAuthParams {
    email: string;
    password: string;
    device_info: string;
}
