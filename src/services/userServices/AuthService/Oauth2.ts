import OAuth2Strategy from 'passport-oauth2';
import passport, { Profile } from 'passport';
import { VerifyCallback } from 'passport-google-oauth20';
import express from 'express';
import { RequestHandler } from 'express-serve-static-core';
import AuthService from '../../../services/userServices/AuthService';

export default class Oauth2 {
    constructor(
        private origin: string,
        private basePath: string,
        private strategies: SimplifiedOAuth2StrategyOptions[],
        private verifier: (
            accessToken: string,
            refreshToken: string,
            profile: Profile,
            done: VerifyCallback
        ) => void
    ) {}

    useStrategies() {
        for (const strategy of this.strategies) {
            const Strategy: typeof SimplifiedOAuth2Strategy = require(strategy.package).Strategy;
            passport.use(
                new Strategy(
                    {
                        callbackURL: `${this.origin}${this.basePath}/redirect/${strategy.name}`,
                        ...strategy,
                    },
                    this.verifier
                )
            );
        }
    }

    public bindRoutes(
        app: express.Application,
        authenticator: RequestHandler = defaultAuthenticator
    ) {
        for (const strategy of this.strategies) {
            app.get(`${this.basePath}/${strategy.name}`, passport.authenticate(strategy.name));

            app.get(
                `${this.basePath}/redirect/${strategy.name}`,
                passport.authenticate(strategy.name, {
                    failureRedirect: '/login',
                    failureMessage: true,
                }),
                authenticator
            );
        }

        app.get(`${this.basePath}/list`, (req, res) => {
            res.json(this.strategies.map(strategy => strategy.name));
        });
    }
}

async function defaultAuthenticator(req, res) {
    const authService = new AuthService();
    const { token } = await authService.processUserTokenRecord(
        req.headers['user-agent'],
        req.session['passport']?.user
    );

    res.end(`
                <html>
                    <head><script>
                        localStorage.setItem('token', '${token}');
                        const oauthReturnUrl = localStorage.getItem('oauthReturnUrl');
                        if (oauthReturnUrl) {
                            localStorage.removeItem('oauthReturnUrl');
                            window.location.href = oauthReturnUrl;
                        } else {
                            window.location.href = '/';
                        }
                    </script></head>
                    <body></body>
                </html>
                `);
}

type SimplifiedOAuth2StrategyOptions = Omit<
    OAuth2Strategy.StrategyOptions,
    'tokenURL' | 'authorizationURL'
> & { package: string; name: string };

declare class SimplifiedOAuth2Strategy extends OAuth2Strategy {
    constructor(options: SimplifiedOAuth2StrategyOptions, verify: OAuth2Strategy.VerifyFunction);
}
