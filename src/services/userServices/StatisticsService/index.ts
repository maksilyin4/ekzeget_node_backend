import UserReadingPlan from '../../../entities/UserReadingPlan';
import ReadingPlan from '../../../entities/ReadingPlan';
import ReadingPlanComment from '../../../entities/ReadingPlanComment';
import { ICommentsStatistics, IPlanCompletionStatistics, IQuizStatistics, IUserPlanStatistics } from './types';
import { In } from 'typeorm';
import Reading from '../../../entities/Reading';
import QueezeApplications from '../../../entities/QueezeApplications';
import UserReadingPlanClosedPlans from '../../../entities/UserReadingPlanClosedPlans';

export class StatisticsService {
    private constructor() {
    }

    static async getStatistics(userPlan: UserReadingPlan): Promise<IUserPlanStatistics> {
        return {
            planCompletion: await this.getPlanCompletionStatistics(userPlan),
            comments: await this.getCommentsStatistics(userPlan),
            quizzes: await this.getQuizStatistics(userPlan),
        };
    }

    static async getPlanCompletionStatistics(userPlan: UserReadingPlan): Promise<IPlanCompletionStatistics> {
        if (!userPlan.plan)
            userPlan.plan = await ReadingPlan.findOneOrFail(userPlan.plan_id);
        const currentPlanDay = Math.ceil(
            Number(
                (Date.now() - Date.parse(userPlan.start_date)),
            ) / 1000 / 86400,
        );

        const planTitles = userPlan.plan.plan.split(',').map(it => it.trim());
        const readingsForPlan = await Reading.find({
            where: { title: In(planTitles) },
            relations: ['plans'],
        });

        const allPlans = readingsForPlan.flatMap(reading => reading.plans).filter(plan => plan.day < currentPlanDay);
        const closedPlans = (
            await UserReadingPlanClosedPlans.find({
                where: { user_reading_plan_id: userPlan.id },
                relations: ['plan'],
            })
        ).map(it => it.plan).filter(plan => plan.day <= currentPlanDay);
        const remainingPlans = allPlans.filter(plan => !closedPlans.some(closedPlan => closedPlan.id === plan.id));

        return {
            daysBehindSchedule: new Set(remainingPlans.map(it => it.day)).size,
            percentBehindSchedule: 1 - remainingPlans.length / allPlans.length,
        };
    }

    static async getCommentsStatistics(userPlan: UserReadingPlan): Promise<ICommentsStatistics> {
        const weekAgo = getWeekAgo();
        const comments = await ReadingPlanComment.find({
            where: {
                author_id: userPlan.user_id,
                group_id: userPlan.group_id,
            },
        });
        const commentsLastWeek = comments.filter(comment => comment.created_at > weekAgo);
        return {
            total: comments.length,
            lastWeek: commentsLastWeek.length,
        };
    }

    static async getQuizStatistics(userPlan: UserReadingPlan): Promise<IQuizStatistics> {
        if (!userPlan.plan)
            userPlan.plan = await ReadingPlan.findOne(userPlan.plan_id);
        const currentPlanDay = Math.ceil(
            Number(
                (Date.now() - Date.parse(userPlan.start_date)),
            ) / 1000 / 86400,
        );

        const planTitles = userPlan.plan.plan.split(',').map(it => it.trim());
        const readingsForPlan = await Reading.find({
            where: { title: In(planTitles) },
            relations: ['plans'],
        });

        const plans = readingsForPlan.flatMap(reading => reading.plans);
        const plansThisWeek = plans.filter(it =>
            it.day >= currentPlanDay - 7 && it.day <= currentPlanDay,
        );
        const plansThisWeekIds = plansThisWeek.map(it => it.id);
        const plansShouldBeCompleted = plans;
        const plansShouldBeCompletedThisWeek = plansShouldBeCompleted.filter(it =>
            it.day >= currentPlanDay - 7 && it.day <= currentPlanDay,
        );

        const applications = selectLatestApplications(
            await QueezeApplications.find({
                where: {
                    userId: userPlan.user_id,
                    status: 'C',
                    planId: In(plansShouldBeCompleted.map(it => it.id)),
                },
                relations: ['submissions'],
            }),
        );
        const applicationsLastWeek = applications.filter(it => plansThisWeekIds.includes(it.planId));

        let allTime = null;
        if (plansShouldBeCompleted.length > 0)
            allTime = {
                completedPercent: applications.length / plansShouldBeCompleted.length,
                accuracy: getAccuracy(applications),
            };

        let lastWeek = null;
        if (plansShouldBeCompletedThisWeek.length > 0)
            lastWeek = {
                completedPercent: applicationsLastWeek.length / plansShouldBeCompletedThisWeek.length,
                accuracy: getAccuracy(applicationsLastWeek),
            };

        return { allTime, lastWeek };
    }
}

function selectLatestApplications(applications: QueezeApplications[]): QueezeApplications[] {
    const planIdToApplication = new Map<number, QueezeApplications>();
    for (const application of applications) {
        const exists = planIdToApplication.get(application.planId);
        if (!exists || exists.createdAt < application.createdAt)
            planIdToApplication.set(application.planId, application);
    }
    return Array.from(planIdToApplication.values());
}

function getWeekAgo(): Date {
    const date = new Date();
    date.setDate(date.getDate() - 7);
    return date;
}

function getAccuracy(applications: QueezeApplications[]): number {
    let correct = 0;
    let incorrect = 0;
    for (const application of applications) {
        for (const submission of application.submissions) {
            if (submission.status.toUpperCase() === 'OK')
                correct += 1;
            else
                incorrect += 1;
        }
    }

    return correct > 0
        ? correct / (correct + incorrect)
        : 0;
}
