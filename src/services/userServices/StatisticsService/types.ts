import * as Joi from '@hapi/joi';
import 'joi-extract-type';

export type IPlanCompletionStatistics = Joi.extractType<typeof PlanCompletionStatistics>
export const PlanCompletionStatistics = Joi.object({
    daysBehindSchedule: Joi.number()
        .integer().max(0)
        .required().description('Отставание от плана в днях.'),
    percentBehindSchedule: Joi.number()
        .min(0).max(1)
        .required().description('Доля отставания от плана.'),
}).description('Статистика по прочтению плана.');

export type ICommentsStatistics = Joi.extractType<typeof CommentsStatistics>
export const CommentsStatistics = Joi.object({
    total: Joi.number()
        .integer().min(0)
        .required().description('Количество комментариев за все время.'),
    lastWeek: Joi.number()
        .integer().min(0)
        .required().description('Количество комментариев за последнюю неделю.'),
}).description('Статистика по комментариям.');

export type IQuizStatistics = Joi.extractType<typeof QuizStatistics>
export const QuizStatistics = Joi.object({
    allTime: Joi.object({
        completedPercent: Joi.number().required()
            .min(0).max(1)
            .description('Доля пройденных викторин относительно текущего дня чтений.'),
        accuracy: Joi.number().required()
            .min(0).max(1)
            .description('Доля правильных ответов.'),
    }).allow(null).description('За все время.'),
    lastWeek: Joi.object({
        completedPercent: Joi.number().required()
            .min(0).max(1)
            .description('Доля пройденных викторин относительно текущего дня чтений.'),
        accuracy: Joi.number().required()
            .min(0).max(1)
            .description('Доля правильных ответов.'),
    }).allow(null).description('За последнюю неделю.'),
}).description('Статистика по викторинам.');

export type IUserPlanStatistics = Joi.extractType<typeof UserPlanStatistics>
export const UserPlanStatistics = Joi.object({
    planCompletion: PlanCompletionStatistics.required(),
    comments: CommentsStatistics.required(),
    quizzes: QuizStatistics.required(),
});
