export interface ICreateUserParams {
    username: string;
    email: string;
    password: string;
    device_info: string;
    phone?: string;
    clientHost?: string;
}

export interface IUniqCreateUserParams {
    username?: string;
    email?: string;
}

export interface IVerifiedUserParams {
    username: string;
    email: string;
    name: { givenName: string; familyName: string };
}
