import bcrypt from 'bcryptjs';
import { EntityManager, getManager } from 'typeorm';

import User from '../../../entities/User';
import createRandomString from '../../../libs/helpers/createRandomString';
import getUnixSec from '../../../libs/helpers/getUnixSec';

import { ICreateUserParams, IVerifiedUserParams } from './types';
import UserConfirmEmail from '../../../entities/UserConfirmEmail';
import secretsLength from '../../../const/numbers/secretsLength';
import MailManager from '../../../managers/MailManager';
import letters from '../../../managers/MailManager/letters';
import UserToken from '../../../entities/UserToken';
import createMd5Hash from '../../../libs/helpers/createMd5Hash';
import UserProfile from '../../../entities/UserProfile';
import TimelineEvent from '../../../entities/TimelineEvent';
import { TimelineApplicationEnum, TimelineCategoriesEnum, TimelineEventsEnum } from '../../../const/enums';

export default class RegisterService {
    private _user: User;

    public async register(params: ICreateUserParams) {
        await getManager().transaction(async transaction => {
            const { email, password, username, device_info, clientHost } = params;

            await this.createInactiveUser(transaction, { email, password, username });
            await this.createUserProfile(transaction);
            await this.createUserTokenRecord(transaction, device_info);
            const { secret } = await this.createRegisterConfirmation(transaction);

            await MailManager.send(
                letters.getRequestRegisterConfirm(params.email, { secret, clientHost })
            );
        });
    }

    public async registerVerifiedUser(params: IVerifiedUserParams): Promise<User> {
        return getManager().transaction(async transaction => {
            const user = await this.createActiveUser(
                transaction,
                params.email,
                await User.generateUniqueName(params.username)
            );
            await this.createUserProfile(transaction, user.id, params.name);
            return user;
        });
    }

    private async createInactiveUser(
        transaction: EntityManager,
        { username, email, password }: { username: string; email: string; password: string }
    ) {
        const passwordHash = bcrypt.hashSync(password);

        this._user = await transaction
            .create(User, {
                username,
                email,
                phone: '',
                password_hash: passwordHash,
                status: User.statuses.not_active,
                created_at: getUnixSec(),
                updated_at: getUnixSec(),
            })
            .save();

        return this._user;
    }

    private async createActiveUser(
        transaction: EntityManager,
        email: string,
        username: string
    ): Promise<User> {
        return transaction
            .create(User, {
                username,
                email,
                phone: '',
                status: User.statuses.active,
                created_at: getUnixSec(),
                updated_at: getUnixSec(),
            })
            .save();
    }

    public async createUserTokenRecord(
        transaction: EntityManager,
        device_info: string,
        user_id = this._user.id
    ) {
        return await transaction
            .create(UserToken, {
                user_id,
                type: UserToken.types.reg,

                device_info: '{}',
                device_hash: createMd5Hash(device_info),
                token: createRandomString(secretsLength.accessToken),

                created_at: getUnixSec(),
                updated_at: getUnixSec(),
            })
            .save();
    }

    private async createRegisterConfirmation(transaction: EntityManager, user_id = this._user.id) {
        const registerConfirmCode = createRandomString(secretsLength.registerConfirmCode);

        return await transaction
            .create(UserConfirmEmail, {
                user_id,
                secret: registerConfirmCode,
                created_at: getUnixSec(),
            })
            .save();
    }

    public async approveUser(secret: string) {
        const { user } = await UserConfirmEmail.findOneOrFail({
            where: { secret },
            relations: ['user'],
        });

        user.status = User.statuses.active;
        user.updated_at = getUnixSec();

        await user.save();

        await TimelineEvent.insert({
            application: TimelineApplicationEnum.frontend,
            category: TimelineCategoriesEnum.user,
            event: TimelineEventsEnum.signup,
            data: JSON.stringify({
                public_identity: user.username,
                user_id: user.id,
                created_at: user.created_at,
            }),
        });

        const { token } = await UserToken.findOne({ where: { user_id: user.id } });

        return { id: user.id, token };
    }

    private async createUserProfile(
        transaction: EntityManager,
        user_id = this._user.id,
        name?: { givenName: string; familyName: string }
    ) {
        return await transaction
            .create(UserProfile, {
                user_id,
                firstname: name?.givenName,
                lastname: name?.familyName,
                locale: 'ru-RU',
            })
            .save();
    }

    get user() {
        return this._user;
    }
}
