import User from '../../../entities/User';
import getUnixSec from '../../../libs/helpers/getUnixSec';
import UserProfile from '../../../entities/UserProfile';
import { EntityManager, getManager } from 'typeorm/index';

export default class UserService {
    constructor(private _user?: User) {}

    public async getUser(findCondition?: { id: number }) {
        if (!this._user) return (this._user = await User.findOneOrFail({ where: findCondition }));
        return this._user;
    }

    public async updateUser(updatingProps: Partial<User & UserProfile>) {
        const { email, username, ...profile } = updatingProps;

        await getManager().transaction(async transaction => {
            this._user = await transaction
                .create(User, {
                    ...this._user,
                    email: email || this._user.email,
                    username: username || this._user.username,
                    updated_at: getUnixSec(),
                })
                .save();

            await this.updateUserProfile(transaction, profile);
        });
    }

    public async updateUserProfile(
        transaction: EntityManager,
        updatingProps: Partial<UserProfile>
    ) {
        await transaction.update(UserProfile, { user_id: this._user.id }, updatingProps);
    }

    public async getUserWithProfile() {
        const profile = await UserProfile.findOneOrFail({ where: { user_id: this._user.id } });
        return Object.assign(this._user, { userProfile: profile });
    }
}
