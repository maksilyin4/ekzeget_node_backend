import { easterDatesCache } from './cache';
import { WeekDays } from '../../const/enums';
import { DatesHelper } from '../../libs/helpers/DatesHelper';

interface EasterRanges {
    // Дата последней Пасхи
    lastDate: Date;

    // Дней с последней Пасхи
    sinceLast: number;

    // Дата следующей Пасхи
    nextDate: Date;

    // Дней до следующей Пасхи
    untilNext: number;
}
interface PrestupkaValues {
    prestupkaDate: Date;
    prestupkaDays: number;
}

export class ReadingsHelper {
    constructor(today: Date) {
        this.today = today;
        this.currentYear = today.getFullYear();
    }

    private readonly today: Date;
    private readonly currentYear: number;

    public getDate(): Date {
        return this.today;
    }

    /**
     * Возвращает значения, связанные с Преступкой
     */
    public getPrestupkaValues(): PrestupkaValues {
        const { lastDate: lastEaster } = this.getCurrentYearEasterRanges();
        const yearOfLastEaster: number = lastEaster.getFullYear();

        const week: Date[] = [
            new Date(`09/28/${yearOfLastEaster}`),
            new Date(`09/29/${yearOfLastEaster}`),
            new Date(`09/30/${yearOfLastEaster}`),
            new Date(`10/01/${yearOfLastEaster}`),
            new Date(`10/02/${yearOfLastEaster}`),
            new Date(`10/03/${yearOfLastEaster}`),
            new Date(`10/04/${yearOfLastEaster}`),
        ];
        const sunday = week.find((date: Date) => date.getDay() === WeekDays.Sunday);
        sunday.setHours(0, 0, 0, 0);

        const monday: Date = DatesHelper.addDays(sunday, 1);

        // В инструкции - $dayCount
        const fromLastEasterToMondayDiff: number = DatesHelper.calcDifferenceInFullDays(
            monday,
            lastEaster
        );

        // Может быть отрицательным
        const prestupkaDays = 169 - fromLastEasterToMondayDiff;

        const newDate: Date = DatesHelper.addDays(monday, prestupkaDays);

        const prestupkaDate = monday > newDate ? newDate : monday;

        return {
            prestupkaDate,
            prestupkaDays,
        };
    }

    /**
     * Даты и промежутки между сегодняшним днем, прошлой Пасхой и следующей Пасхой
     */
    public getCurrentYearEasterRanges(): EasterRanges {
        const currentYearEasterDate: Date = this.getEasterDateByYear(this.currentYear);
        let sinceLast: number, lastDate: Date, untilNext: number, nextDate: Date;

        // Была ли Пасха в этом году?
        const isCurrentYearEasterPassed = this.today > currentYearEasterDate;
        if (isCurrentYearEasterPassed) {
            lastDate = currentYearEasterDate;
            sinceLast = DatesHelper.calcDifferenceInFullDays(this.today, currentYearEasterDate);
            nextDate = this.getEasterDateByYear(this.currentYear + 1);
            untilNext = DatesHelper.calcDifferenceInFullDays(nextDate, this.today);
        } else {
            lastDate = this.getEasterDateByYear(this.currentYear - 1);
            sinceLast = DatesHelper.calcDifferenceInFullDays(this.today, lastDate);
            nextDate = currentYearEasterDate;
            untilNext = DatesHelper.calcDifferenceInFullDays(
                currentYearEasterDate,
                this.today,
                Math.ceil
            );
        }

        return {
            lastDate,
            sinceLast,
            nextDate,
            untilNext,
        };
    }

    public evangelieDaysEqual245 = (): number => {
        const weekInDecember: Date[] = [
            new Date(`12/24/${this.currentYear}`),
            new Date(`12/25/${this.currentYear}`),
            new Date(`12/26/${this.currentYear}`),
            new Date(`12/27/${this.currentYear}`),
            new Date(`12/28/${this.currentYear}`),
            new Date(`12/29/${this.currentYear}`),
            new Date(`12/30/${this.currentYear}`),
        ];
        const weekWithOffset = weekInDecember.map(date => this.addCenturyOffset(date));
        const sunday = weekWithOffset.find(date => date.getDay() === WeekDays.Sunday);
        const diff: number = DatesHelper.calcDifferenceInFullDays(sunday, this.today);

        return 245 + diff;
    };

    /**
     * Получение значения переменной, которая упоминается в инструкции как "дата из пункта 8.3" (а так же 8.1, или 8.2);
     */
    public getDateString83 = (): string => {
        // Текущая дата в календарном стиле 20-21 веков
        const todayWithOffset = this.addCenturyOffset(this.today);

        let resultDate: Date;

        // Високосный ли год сейчас?
        const isLeapYear = DatesHelper.isLeapYear(this.currentYear);
        if (isLeapYear) {
            const february29: Date = DatesHelper.parseDateStringDDMMYYYYToDate(
                `29.02.${this.currentYear}`
            );
            const march13: Date = DatesHelper.parseDateStringDDMMYYYYToDate(
                `13.03.${this.currentYear}`
            );

            if (february29 < todayWithOffset && todayWithOffset < march13) {
                resultDate = DatesHelper.addDays(todayWithOffset, 1);
            } else {
                resultDate = todayWithOffset;
            }
        } else {
            resultDate = todayWithOffset;
        }

        return DatesHelper.getDateStringDDMM(resultDate);
    };

    /**
     * Рассчитывает значение дней для запросов к чтениям в случае,
     * когда количество дней до Пасхи больше 70 и evangelieDays больше 279
     * TODO Что если num_ned = 5+ ?
     */
    public getNewDays(untilNextEaster: number, evangelieDays: number): number {
        const numNed: number = (evangelieDays - 279 + (untilNextEaster - 70) - 1) / 7;

        let newDays;
        switch (numNed) {
            case 0:
            case 1:
                newDays = evangelieDays - 7;
                break;
            case 2:
                newDays = evangelieDays - 14;
                break;
            case 3:
                newDays = evangelieDays - 21;
                break;
            case 4:
                newDays = evangelieDays - 28;
                break;
            case 5:
                if (evangelieDays >= 281 && evangelieDays <= 294) newDays = evangelieDays - 28;
                if (evangelieDays >= 295 && evangelieDays <= 301) newDays = evangelieDays - 133;
                if (evangelieDays >= 302 && evangelieDays <= 314) newDays = evangelieDays - 35;
                break;
        }

        return newDays;
    }

    /**
     * Сегодня воскресенье?
     */
    public isItSundayToday(): boolean {
        return this.today.getDay() === WeekDays.Sunday;
    }

    /**
     * Получаем номер недели (понедельник = 1, ..., воскресенье = 7)
     */
    public getWeekDayNumber(): number {
        return this.today.getDay() === WeekDays.Sunday ? 7 : this.today.getDay();
    }

    /**
     * Вычисляет дату Пасхи для 20 и 21 веков.
     * Для других веков надо дополнительно вычислять offset.
     */
    private easterModernDate(year: number): Date {
        let day: number, month: number;

        const c = ((year % 19) * 19 + 15) % 30;
        const a = year % 4;
        const b = year % 7;
        const d = (2 * a + 4 * b + 6 * c + 6) % 7;
        day = 4 + c + d;

        if (day > 30) {
            day = day - 30;
            month = 5; // Май
        } else {
            month = 4; // Апрель
        }

        const resultDate = new Date();
        resultDate.setHours(0, 0, 0, 0);
        resultDate.setFullYear(year, month - 1, day);

        return resultDate;
    }

    /**
     * Прибавляет (или вычитает) поправку в зависимости от века
     */
    public addCenturyOffset(date: Date): Date {
        let result: Date;
        const century = this.getCenturyByYear(date.getFullYear());
        const offset = this.getOffsetByCentury(century);

        if (century > 21) {
            result = DatesHelper.addDays(date, offset);
        } else if (century < 20) {
            result = DatesHelper.subtractDays(date, offset);
        } else {
            result = date;
        }

        return result;
    }

    /**
     * Возвращает дату Пасхи для любого года
     */
    private getEasterDateByYear = (year: number): Date => {
        if (easterDatesCache[year]) {
            return easterDatesCache[year];
        }

        const modernEasterDate = this.easterModernDate(year);
        const dateWithOffset = this.addCenturyOffset(modernEasterDate);
        easterDatesCache[year] = dateWithOffset;

        return dateWithOffset;
    };

    /**
     * Вычислить век по году
     */
    private getCenturyByYear = (year: number) => Math.ceil(year / 100);

    /**
     *  Вычислить поправку в днях по веку, значение может быть отрицательным
     */
    private getOffsetByCentury = (century: number): number => {
        const a = 20 - century;
        const b = Math.floor(a / 4);
        return a - b;
    };
}
