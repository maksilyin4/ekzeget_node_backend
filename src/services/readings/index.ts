import { DatesHelper } from '../../libs/helpers/DatesHelper';
import { ReadingsHelper } from './ReadingsHelper';
import GospelReading from '../../entities/GospelReading';
import ApostolicReading from '../../entities/ApostolicReading';
import PostReading from '../../entities/PostReading';
import MorningReading from '../../entities/MorningReading';
import FloatingReadingRules from '../../entities/FloatingReadingRules';
import FloatingReading from '../../entities/FloatingReading';
import {In} from 'typeorm';
import Mineja from '../../entities/Mineja';

type MinejaZnak = '12' | 'bden';

interface WrapLocationCommonReadingParams {
    basicReading: string;
    floatingReading: string;
    //-1 – предыдущие чтения перед чтениями из таблицы floating_chten, не обозначаются сокращением "ряд.:"
    //0 – предыдущие чтения перед чтениями из таблицы floating_chten
    //1 – предыдущие чтения после чтений из таблицы floating_chten
    locationCommonReading: number;
}
const wrapLocationCommonReading = ({
    basicReading,
    floatingReading,
    locationCommonReading,
}: WrapLocationCommonReadingParams): string => {
    if (locationCommonReading === -1) {
        return `${basicReading}; ${floatingReading}`;
    }
    return locationCommonReading
        ? `${floatingReading}; ряд.: ${basicReading}`
        : `ряд.: ${basicReading}; ${floatingReading}`;
};

const wrapMinejaReadings = (reading: string, prefix: string): string =>
    prefix && reading ? `${prefix}: ${reading}` : reading;

const makeTitlesUnique = (titles: string[]): string[] => {
    const clearTitles = titles.map(title =>
        title
            .replace(/[\n]/gi, ' ')
            .replace(/[\r]/gi, ' ')
            .replace(/\s\s/gi, ' ')
    );

    return [...new Set(clearTitles)].filter(Boolean);
};

interface Readings {
    title: string[];
    apostolic_reading: string;
    morning_reading: string;
    gospel_reading: string;
    more_reading: string;
    color: string;
}

interface ReadigsIdx {
    apostolic: number[];
    morning: number[];
    gospel: number[];
    post: number[];
    mineja: number[];
}

interface ReadingsWithIdx {
    readings: Readings;
    readingIdx: ReadigsIdx;
}

const getReadings = async (date: string): Promise<ReadingsWithIdx> => {
    const today: Date = DatesHelper.parseDateStringDDMMYYYYToDate(date);
    const readingsHelper: ReadingsHelper = new ReadingsHelper(today);

    const { sinceLast, untilNext } = readingsHelper.getCurrentYearEasterRanges();
    const { prestupkaDate, prestupkaDays } = readingsHelper.getPrestupkaValues();

    let specialComment = '';
    let apostolicDays: number;
    let evangelieDays: number;
    let readings: Readings = {
        title: [],
        apostolic_reading: '',
        morning_reading: '',
        gospel_reading: '',
        more_reading: '',
        color: '',
    };
    let readingIdx: ReadigsIdx = { apostolic: [], morning: [], gospel: [], post: [], mineja: [] };

    //region Вычисление суточных чтений и праздников, зависящих от Пасхи.
    if (untilNext > 70) {
        // --- Апостольское чтение и чтение на Утрене ---
        if (sinceLast > 279 && prestupkaDays < 0) {
            apostolicDays = sinceLast + prestupkaDays;
        } else {
            apostolicDays = sinceLast;
        }

        // --- Евангельское чтение ---
        evangelieDays = sinceLast;
        if (prestupkaDays < 0) {
            const prestupkaDaysAbs = Math.abs(prestupkaDays);
            const date = DatesHelper.addDays(prestupkaDate, prestupkaDaysAbs);

            if (today >= prestupkaDate) {
                if (today < date) {
                    evangelieDays = sinceLast - (42 + prestupkaDaysAbs);
                } else {
                    evangelieDays = sinceLast - prestupkaDaysAbs;
                }
            }
        } else if (today >= prestupkaDate) {
            evangelieDays = sinceLast + prestupkaDays;
        } else {
            evangelieDays = sinceLast;
        }

        // ИСКЛЮЧЕНИЕ: если evangelieDays = 245 дней
        if (evangelieDays === 245) {
            evangelieDays = readingsHelper.evangelieDaysEqual245();
        }

        const [gospelReading, apostolicReading] = await Promise.all([
            GospelReading.findOne({ where: { day: evangelieDays } }),
            ApostolicReading.findOne({ where: { day: sinceLast } }),
        ]);

        if (sinceLast === 252) {
            //Если 29 неделя, то ее апостол читается на неделю праотцев, а с праотцев переносится на сегодня!
            const weeksUntilForefathersSunday = ~~((30 - today.getDate()) / 7);
            const forefathersSundayApostolicReading = await ApostolicReading.findOne({
                where: { day: sinceLast + weeksUntilForefathersSunday * 7 },
            });
            apostolicReading.reading = forefathersSundayApostolicReading.reading;
        }

        if (apostolicDays !== sinceLast) {
            apostolicReading.reading = (
                await ApostolicReading.findOne({ where: { day: apostolicDays } })
            )?.reading;
        }
        readings = {
            title: [apostolicReading ? apostolicReading.title : ''],
            apostolic_reading: apostolicReading ? apostolicReading.reading : '',
            morning_reading: apostolicReading ? apostolicReading.morning : '',
            gospel_reading: gospelReading ? gospelReading.reading : '',
            more_reading: apostolicReading ? apostolicReading.more : '',
            color: '',
        };
        readingIdx.apostolic = [apostolicReading.id];
        readingIdx.gospel = [apostolicReading.id];
        // ------------------------------------------------------------------
    } else {
        const postReading = await PostReading.findOne({ where: { day: untilNext } });

        if (postReading) {
            readings = {
                title: [postReading.title],
                apostolic_reading: postReading.apostle,
                morning_reading: postReading.morning,
                gospel_reading: postReading.gospel,
                more_reading: postReading.vz,
                color: '',
            };
            readingIdx.post = [postReading.id];
        }

        // ИСКЛЮЧЕНИЕ: Воскресенье - обязательно должно быть утреннее чтение
        if (readingsHelper.isItSundayToday() && readings.morning_reading === '') {
            const dni = Math.floor((sinceLast - 49) / 7);
            const morningReading = await MorningReading.findOne({ where: { dni } });
            readings.morning_reading = morningReading.utr;
            readingIdx.morning = [morningReading.id];
        }
    }

    // ИСКЛЮЧЕНИЕ: если количество дней до Пасхи больше 70 и evangelieDays больше 279
    if (untilNext > 70 && evangelieDays > 279) {
        const newDays = readingsHelper.getNewDays(untilNext, evangelieDays);

        const [{ title: newTitle, id }, { reading: ar, id: arId }, { reading: gr, id: grId }] = await Promise.all([
            ApostolicReading.findOne({ where: { day: sinceLast } }),
            ApostolicReading.findOne({ where: { day: newDays } }),
            GospelReading.findOne({ where: { day: newDays } }),
        ]);

        if (newTitle && ar && gr) {
            readings.title = [newTitle];
            readings.apostolic_reading = ar;
            readings.gospel_reading = gr;
            readingIdx.apostolic.push(id);
            readingIdx.apostolic.push(arId);
            readingIdx.gospel = [grId];
        }
    }
    //endregion Вычисление суточных чтений и праздников, зависящих от Пасхи.

    //region Вычисление календарных праздников и чтений, не зависящих от Пасхи.

    // Переменная, которая упоминается в инструкции как "дата из пункта 8.3" (а так же 8.1, или 8.2);
    let dateString83: string = readingsHelper.getDateString83();

    const weekDayNumber: number = readingsHelper.getWeekDayNumber();

    //region Вычисление «плавающих» праздников

    //region Исключения из «плавающих» праздников
    let existMineja: boolean = true;

    if (
        (untilNext < 49 || untilNext === 51 || untilNext === 53) &&
        (![6, 7].includes(weekDayNumber) || readings.title[0]?.startsWith('Страстная')) &&
        !['09.03', '21.03', '22.03', '07.04'].includes(dateString83)
    ) {
        existMineja = false;
    }

    if (
        sinceLast < 38 &&
        weekDayNumber === 7 &&
        !['06.05', '21.05', '22.05', '03.06'].includes(dateString83)
    ) {
        existMineja = false;
    }

    if (untilNext === 58 && dateString83 == '08.03') {
        dateString83 = '09.03';
    }

    if (untilNext === 57 && dateString83 == '09.03') {
        existMineja = false;
    }

    if (dateString83 === '21.03' && [6, 7].includes(weekDayNumber)) {
        existMineja = false;
    }

    if (dateString83 === '22.03' && untilNext === 25) {
        existMineja = false;
        specialComment = 'переносится на 21 марта';
    }

    if (dateString83 === '21.03' && untilNext === 26) {
        existMineja = true;
        dateString83 = '22.03';
        specialComment = 'с 22 марта';
    }

    if (dateString83 === '20.03' && untilNext === 27) {
        existMineja = true;
        dateString83 = '21.03';
    }
    //endregion Исключения из «плавающих» праздников

    const floatingReadingRules = await FloatingReadingRules.findOne({
        where: {
            date: `${dateString83}.`,
            day_of_week: weekDayNumber,
        },
    });
    let floatingReadings: FloatingReading;
    if (floatingReadingRules) {
        floatingReadings = await FloatingReading.findOne({
            where: {
                id: floatingReadingRules.floating_chten_id,
            },
        });

        if (floatingReadings.name.includes('праотец')) {
            floatingReadings.utr_chten = readings.morning_reading;
            floatingReadings.apostol_chten = (
                await ApostolicReading.findOne({ where: { day: 252 } })
            ).reading;
        } else if (dateString83 === '18.01') {
            const additionalFloatingReadingId = (
                await FloatingReadingRules.findOne({
                    where: { date: '18.01.', day_of_week: In([6, 7]) },
                    order: { id: 'DESC' },
                })
            ).floating_chten_id;
            const additionalFloatingReadings = await FloatingReading.findOne(
                additionalFloatingReadingId
            );
            floatingReadings.name += '; ' + additionalFloatingReadings.name;
            readings.more_reading = additionalFloatingReadings.dop_chten;
        } else if (floatingReadingRules.floating_chten_id === 21 && untilNext === 70) {
            floatingReadings = await FloatingReading.findOne({
                where: {
                    id: 22,
                },
            });
        }
    }

    if (sinceLast === 61) {
        floatingReadings = await FloatingReading.findOne({
            where: {
                id: 34,
            },
        });
    }

    if (floatingReadings) {
        if (floatingReadings.name) {
            readings.title.push(...floatingReadings.name.split('; '));
        }
        readings.color = floatingReadings.color;

        readings.more_reading = floatingReadings.dop_chten || readings.more_reading;
        readings.morning_reading = floatingReadings.utr_chten || readings.morning_reading;

        if (!floatingReadings.exist_common_chten) {
            readings.apostolic_reading = floatingReadings.apostol_chten ?? '';
            readings.gospel_reading = floatingReadings.evang_chten ?? '';
        } else {
            if (dateString83 === '13.01' && weekDayNumber === 6) {
                readings.apostolic_reading += ' (под зачало)';
                readings.gospel_reading += ' (под зачало)';
            }

            readings.apostolic_reading = wrapLocationCommonReading({
                basicReading: readings.apostolic_reading,
                floatingReading: floatingReadings.apostol_chten,
                locationCommonReading: floatingReadings.location_common_chten,
            });
            readings.gospel_reading = wrapLocationCommonReading({
                basicReading: readings.gospel_reading,
                floatingReading: floatingReadings.evang_chten,
                locationCommonReading: floatingReadings.location_common_chten,
            });
        }
    }

    //endregion Вычисление «плавающих» праздников

    //region Вычисление праздников Минеи

    if (floatingReadings && !floatingReadings.exist_mineja) {
        existMineja = false;
    }
    const minejaReadings = await Mineja.findOne({
        where: {
            den: `${dateString83}.`,
        },
    });

    if (minejaReadings) {
        readings.title.push(...minejaReadings.title.split('\n'));


        if (existMineja) {
            readingIdx.mineja.push(minejaReadings.id);
            const minejaZnak = minejaReadings.znak as MinejaZnak;

            const minejaReadingsCondition =
                (minejaZnak === '12' || minejaZnak === 'bden') &&
                !readingsHelper.isItSundayToday() &&
                ![1, 2, 3].includes(sinceLast);

            // Апостольские чтения
            const minejaApostolReadings = wrapMinejaReadings(
                minejaReadings.apostolChten,
                minejaReadings.abbr
            );
            readings.apostolic_reading = minejaReadingsCondition
                ? minejaApostolReadings
                : readings.apostolic_reading || minejaApostolReadings
                    ? `${readings.apostolic_reading}; ${minejaApostolReadings}`
                    : '';

            // Евангельские чтения
            const minejaEvangReadings = wrapMinejaReadings(
                minejaReadings.evangChten,
                minejaReadings.abbr
            );
            readings.gospel_reading = minejaReadingsCondition
                ? minejaEvangReadings
                : readings.gospel_reading || minejaEvangReadings
                    ? `${readings.gospel_reading}; ${minejaEvangReadings}`
                    : '';

            if (minejaReadingsCondition) {
                readingIdx.apostolic = [];
                readingIdx.gospel = [];
                readingIdx.post = [];
            }

            // Утренние чтения
            const minejaMorningReadingsCondition1 =
                !readingsHelper.isItSundayToday() && !readings.morning_reading;
            const minejaMorningReadingsCondition2 =
                readingsHelper.isItSundayToday() && minejaZnak === '12';

            if (minejaMorningReadingsCondition1 || minejaMorningReadingsCondition2) {
                readings.morning_reading = minejaReadings.utrChten;
                readingIdx.morning = [];
            }

            // Дополнительные чтения
            if (readings.more_reading) {
                readings.more_reading = `${readings.more_reading}; ${minejaReadings.dopChten}`;
            } else {
                readings.more_reading = minejaReadings.dopChten;
            }

            readings.color = minejaReadings.color;
        }
    }
    //endregion Вычисление праздников Минеи
    //endregion Вычисление календарных праздников и чтений, не зависящих от Пасхи.

    // Удаление одинаковых названий из разных чтений
    readings.title = makeTitlesUnique(readings.title);

    //region Если сегодня Пасха - все прошлые вычисления игнорируются
    if (untilNext === 0) {
        readings = {
            title: ['Пасха'],
            apostolic_reading: 'Деян. 1:1-8',
            morning_reading: '',
            gospel_reading: 'Ин. 1:1-17',
            more_reading: 'В тот же день на вечерне: Ин. 20:19-25',
            color: 'red',
        };

        // Если текущая дата – Пасха, и при этом дата, вычисленная в п. 8.3. = 07.04,
        // то также отменяются все данные, вычисленные ранее, и заменяются на следующие:
        if (dateString83 === '07.04') {
            readings = {
                title: ['Пасха, Благовещение Пресвятой Богородицы'],
                apostolic_reading: 'Деян. 1:1-8, Богородице: Евр. 2:11–18',
                morning_reading: 'Лк. 1:39-49, 56',
                gospel_reading: 'Ин. 1:1-17, Богородице: Лк. 1:24–38',
                more_reading: 'В тот же день на вечерне: Ин. 20:19-25',
                color: 'red',
            };
        }

        readingIdx = { apostolic: [], morning: [], gospel: [], post: [], mineja: [] };
    }
    //endregion Если сегодня Пасха - все прошлые вычисления игнорируются

    // Если дата из п. 8.3. = 13.03,
    // то к названию праздника добавляется: Преподобного Иоанна Кассиана Римлянина
    if (
        dateString83 === '13.03' &&
        DatesHelper.getDateStringDDMM(readingsHelper.getDate()) === dateString83
    ) {
        readings.title.push('Преподобного Иоанна Кассиана Римлянина');
    }
    if (
        untilNext === 7 ||
        readingsHelper.isItSundayToday() ||
        /Светл|Вход|Вознесение/.test(readings.title[0])
    ) {
        readings.color = 'red';
    }

    if (untilNext === 77 && sinceLast !== 273) {
        readings.apostolic_reading = readings.apostolic_reading.replace(
            '1 Тим. 4:9–15;',
            '1 Тим. 4:9–15; (32-й недели);'
        );
        readings.gospel_reading = readings.gospel_reading.replace(
            'Лк. 19:1–10;',
            'Лк. 19:1–10; (32-й недели, о Закхее);'
        );
    }

    if (readings.title[0] === 'Пятидесятница' || readings.title[0] === 'Понедельник Святого Духа') {
        readings.color = 'green';
    }

    if (specialComment) {
        readings.title.push(`(${specialComment})`);
    }

    return { readings, readingIdx };
};

const getReadingsWithDate = (date: Date): Promise<ReadingsWithIdx> => {
    return getReadings(
        `${String(date.getDate()).padStart(2, '0')}.${String(date.getMonth() + 1).padStart(2, '0')}.${date.getFullYear()}`
    );
}

export { getReadings, getReadingsWithDate, Readings };
