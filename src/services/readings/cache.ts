interface DatesByYearCacheInterface {
    [year: number]: Date;
}

/**
 * Кэшируем даты Пасхи
 */
export const easterDatesCache: DatesByYearCacheInterface = {};
