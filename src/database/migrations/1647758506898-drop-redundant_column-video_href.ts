import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class dropRedundantColumnVideoHref1647758506898 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('media', 'video_href');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'media',
            new TableColumn({
                name: 'video_href',
                type: 'varchar',
                length: '255',
                default: null,
                isNullable: true,
            })
        );
    }
}
