UPDATE ekzeget_person
set
  code = 'hromatijj_akvileyskijj_sviatitel'
where
  name = 'Хроматий Аквилейский святитель';

# Revert:
# UPDATE ekzeget_person
# set
#   code = ''
# where
#   name = 'Хроматий Аквилейский святитель';
