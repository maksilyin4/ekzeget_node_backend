import { MigrationInterface, QueryRunner, Table, TableColumn } from 'typeorm';

export class createTableTypo1674168282086 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'typo',
                columns: [
                    new TableColumn({
                        name: 'id',
                        type: 'int',
                        unsigned: true,
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'comment',
                        type: 'varchar',
                        isNullable: true,
                        length: '255',
                    }),
                    new TableColumn({ name: 'url', type: 'varchar', length: '1024' }),
                    new TableColumn({ name: 'selection', type: 'varchar', length: '1024' }),
                    new TableColumn({
                        name: 'is_report_helpful',
                        type: 'boolean',
                        isNullable: true,
                    }),
                    new TableColumn({ name: 'processed', type: 'boolean', default: false }),
                    new TableColumn({
                        name: 'created_at',
                        type: 'int',
                    }),
                    new TableColumn({
                        name: 'user_id',
                        type: 'int',
                        isNullable: true,
                    }),
                ],
                foreignKeys: [
                    {
                        columnNames: ['user_id'],
                        referencedTableName: 'user',
                        referencedColumnNames: ['id'],
                    },
                ],
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('typo');
    }
}
