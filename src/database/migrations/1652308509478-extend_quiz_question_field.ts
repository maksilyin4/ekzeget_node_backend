import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class extendQuizQuestionField1652308509478 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.changeColumn(
            'queeze_questions',
            'title',
            new TableColumn({ name: 'title', type: 'text' })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.changeColumn(
            'queeze_questions',
            'title',
            new TableColumn({ name: 'title', type: 'varchar', length: '256' })
        );
    }
}
