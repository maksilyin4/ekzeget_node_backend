alter table meta_template
  drop key meta_template_title_uindex;

# Revert:
# alter table meta_template
#   add constraint meta_template_title_uindex
#     unique (title);
