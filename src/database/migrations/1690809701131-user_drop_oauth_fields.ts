import { Column, MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class userDropOauthFields1690809701131 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('user', 'auth_key');
        await queryRunner.dropColumn('user', 'access_token');
        await queryRunner.dropColumn('user', 'oauth_client');
        await queryRunner.dropColumn('user', 'oauth_client_user_id');
        await queryRunner.changeColumn(
            'user',
            'password_hash',
            new TableColumn({
                name: 'password_hash',
                type: 'varchar',
                length: '255',
                isNullable: true,
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'user',
            new TableColumn({
                name: 'auth_key',
                type: 'varchar',
                length: '32',
            })
        );

        await queryRunner.addColumn(
            'user',
            new TableColumn({
                name: 'access_token',
                type: 'varchar',
                length: '40',
            })
        );

        await queryRunner.addColumn(
            'user',
            new TableColumn({
                name: 'oauth_client',
                type: 'varchar',
                length: '255',
                isNullable: true,
            })
        );

        await queryRunner.addColumn(
            'user',
            new TableColumn({
                name: 'oauth_client_user_id',
                type: 'varchar',
                length: '255',
                isNullable: true,
            })
        );

        await queryRunner.query('UPDATE user SET password_hash = "" WHERE  password_hash IS NULL');

        await queryRunner.changeColumn(
            'user',
            'password_hash',
            new TableColumn({
                name: 'password_hash',
                type: 'varchar',
                length: '255',
                isNullable: false,
            })
        );
    }
}
