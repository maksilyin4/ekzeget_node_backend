import { MigrationInterface, Not, QueryRunner, TableColumn } from 'typeorm';

export class mediaChangeDescription_to_socialDescription1673806252753 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`UPDATE \`media\`
                                 SET text = description
                                 WHERE (text IS NULL OR TRIM(text) = '')
                                   AND description IS NOT NULL
                                   AND TRIM(description) != '' AND description != title`);
        await queryRunner.changeColumn(
            'media',
            'description',
            new TableColumn({
                name: 'social_description',
                type: 'varchar',
                length: '200',
                isNullable: true,
            })
        );
        await queryRunner.query(`UPDATE \`media\`
                                 SET social_description = SUBSTRING(REGEXP_REPLACE(text, '(<([^>]+)>)', ''), 1, 200)
                                 WHERE type != 'C'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.changeColumn(
            'media',
            'social_description',
            new TableColumn({
                name: 'description',
                type: 'text',
                default: null,
                isNullable: true,
            })
        );
    }
}
