import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class queezeApplicationsAddGameCriteria1680652923572 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'queeze_applications',
            new TableColumn({ name: 'criteria', type: 'json' })
        );
        await queryRunner.query(`UPDATE queeze_applications qa, (SELECT application_id,
                                                                        JSON_OBJECT('types',
                                                                                    JSON_ARRAYAGG(DISTINCT type),
                                                                                    'books',
                                                                                    JSON_ARRAYAGG(DISTINCT book_id)) AS criteria
                                                                 FROM quiz_submission
                                                                          JOIN queeze_questions qq ON question_id = qq.id
                                                                          JOIN verse ON qq.verse_id = verse.id
                                                                          JOIN book ON book.id = verse.book_id
                                                                 GROUP BY application_id) derrived
                                 SET qa.criteria = derrived.criteria
                                 WHERE derrived.application_id = qa.id`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('queeze_applications', 'criteria');
    }
}
