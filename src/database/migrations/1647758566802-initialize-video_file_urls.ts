import { MigrationInterface, QueryRunner } from 'typeorm';
import Media from '../../entities/Media';

export class initializeVideoFileUrls1647758566802 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        const media = await Media.find({
            type: 'V',
        });
        for (const media_item of media) {
            const media_urls = JSON.parse(media_item.properties);
            media_urls.video_file_url = '';
            media_item.properties = JSON.stringify(media_urls);
        }
        await Media.save(media);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const media = await Media.find({
            type: 'V',
        });
        for (const media_item of media) {
            const media_urls = JSON.parse(media_item.properties);
            delete media_urls.video_file_url;
            media_item.properties = JSON.stringify(media_urls);
        }
        await Media.save(media);
    }
}
