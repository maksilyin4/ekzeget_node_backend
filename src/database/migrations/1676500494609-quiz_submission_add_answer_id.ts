import { MigrationInterface, QueryRunner, TableColumn, TableForeignKey } from 'typeorm';

export class quizSubmissionAddAnswerId1676500494609 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'quiz_submission',
            new TableColumn({ name: 'answer_id', type: 'int', isNullable: true, unsigned: true })
        );
        await queryRunner.createForeignKey(
            'quiz_submission',
            new TableForeignKey({
                name: 'FK_quiz_submission_queeze_answers',
                columnNames: ['answer_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'queeze_answers',
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey('quiz_submission', 'FK_quiz_submission_queeze_answers');
        await queryRunner.dropColumn('quiz_submission', 'answer_id');
    }
}
