import { MigrationInterface, QueryRunner} from "typeorm";

export class renameTable_queezeApplicationsAnswers_to_quizSubmission1676500033827 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.renameTable('queeze_applications_answers', 'quiz_submission');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.renameTable('quiz_submission', 'queeze_applications_answers');
    }
}
