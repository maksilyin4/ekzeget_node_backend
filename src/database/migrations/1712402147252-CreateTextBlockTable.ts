import { MigrationInterface, QueryRunner, Table, TableColumn, TableIndex } from 'typeorm';

export class CreateTextBlockTable1712402147252 implements MigrationInterface {
    name = 'CreateTextBlockTable1712402147252'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'text_block',
                engine: 'innoDB',
                columns: [
                    new TableColumn({
                        name: 'id',
                        type: 'int',
                        unsigned: true,
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'title',
                        type: 'varchar',
                        length: '255',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'code',
                        type: 'varchar',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'text',
                        type: 'text',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'active',
                        type: 'smallint',
                        default: 1,
                    }),
                ],
            }),
            true
        );

        await queryRunner.createIndex(
            'text_block',
            new TableIndex({
                name: 'IDX_TEXT_BLOCK_CODE',
                columnNames: ['code'],
                isUnique: true,
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('text_block');
    }
}
