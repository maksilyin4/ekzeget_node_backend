ALTER TABLE timeline_event
  MODIFY created_at INT NULL;

# Revert:
# ALTER TABLE timeline_event
#   MODIFY created_at INT NOT NULL;
