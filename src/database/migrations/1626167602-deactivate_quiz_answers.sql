UPDATE media_category SET active = 0 WHERE id = 59;
UPDATE media_category SET active = 0 WHERE id = 60;

# Revert:
# UPDATE media_category SET active = 1 WHERE id = 59;
# UPDATE media_category SET active = 1 WHERE id = 60;
