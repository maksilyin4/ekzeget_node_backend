import { MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey } from 'typeorm';

export class userReadingPlanClosedPlans1698919968367 implements MigrationInterface {
    name = 'userReadingPlanClosedPlans1698919968367';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'user_reading_plan_closed_plans',
                columns: [
                    new TableColumn({
                        name: 'user_reading_plan_id',
                        type: 'int',
                        isNullable: false,
                        isPrimary: true,
                    }),
                    new TableColumn({
                        name: 'plan_id',
                        type: 'int',
                        isNullable: false,
                        isPrimary: true,
                    }),
                ],
                foreignKeys: [
                    new TableForeignKey({
                        name: 'user_reading_plan_closed_plans__user_reading_plan_id',
                        columnNames: ['user_reading_plan_id'],
                        referencedTableName: 'user_reading_plan',
                        referencedColumnNames: ['id'],
                    }),
                    new TableForeignKey({
                        name: 'user_reading_plan_closed_plans__plan_id',
                        columnNames: ['plan_id'],
                        referencedTableName: 'plan',
                        referencedColumnNames: ['id'],
                    }),
                ],
                engine: 'innoDB',
            }),
            true,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('user_reading_plan_closed_plans');
    }

}
