alter table meta_template modify h_one varchar(512) null;

# Revert:
# alter table meta_template modify h_one varchar(128) null;
