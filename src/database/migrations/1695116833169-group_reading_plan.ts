import { IsNull, MigrationInterface, Not, QueryRunner, Table, TableColumn, TableForeignKey, TableIndex } from 'typeorm';
import UserReadingPlan from '../../entities/UserReadingPlan';

export class groupReadingPlan1695116833169 implements MigrationInterface {
    name = 'groupReadingPlan1695116833169';

    public async up(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.createTable(
            new Table({
                name: 'group_reading_plan',
                columns: [
                    new TableColumn({
                        name: 'id',
                        type: 'int',
                        unsigned: true,
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'mentor_id',
                        type: 'int',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'plan_id',
                        type: 'int',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'start_date',
                        type: 'date',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'stop_date',
                        type: 'date',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'translate_id',
                        type: 'int',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'schedule',
                        type: 'text',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'check_date',
                        type: 'date',
                        isNullable: true,
                    }),
                ],
                foreignKeys: [
                    new TableForeignKey({
                        name: 'FK_group_reading_plan_mentor_user_id',
                        columnNames: ['mentor_id'],
                        referencedColumnNames: ['id'],
                        referencedTableName: 'user',
                    }),
                    new TableForeignKey({
                        name: 'FK_group_reading_plan_plan_id',
                        columnNames: ['plan_id'],
                        referencedColumnNames: ['id'],
                        referencedTableName: 'reading_plan',
                    }),
                    new TableForeignKey({
                        name: 'FK_group_reading_plan_translate_id',
                        columnNames: ['translate_id'],
                        referencedColumnNames: ['id'],
                        referencedTableName: 'translate',
                    }),
                ],
                engine: 'innoDB',
            }),
            true,
        );

        await queryRunner.addColumn(
            'user_reading_plan',
            new TableColumn({
                name: 'group_id',
                type: 'int',
                unsigned: true,
                isNullable: true,
                default: null,
            }),
        );
        await queryRunner.createForeignKey(
            'user_reading_plan',
            new TableForeignKey({
                name: 'FK_user_reading_plan_group',
                columnNames: ['group_id'],
                referencedTableName: 'group_reading_plan',
                referencedColumnNames: ['id'],
            }),
        );
        await queryRunner.createIndex(
            'user_reading_plan',
            new TableIndex({
                name: 'IDX_user_reading_plan_group',
                columnNames: ['group_id'],
            }),
        );

    }

    public async down(
        queryRunner: QueryRunner,
        deleteUserReadingPlansAssociatedWithGroup = true,
    ): Promise<void> {
        if (deleteUserReadingPlansAssociatedWithGroup)
            await UserReadingPlan.delete({ group_id: Not(IsNull()) });
        await queryRunner.dropIndex('user_reading_plan', 'IDX_user_reading_plan_group');
        await queryRunner.dropForeignKey('user_reading_plan', 'FK_user_reading_plan_group');
        await queryRunner.dropColumn('user_reading_plan', 'group_id');
        await queryRunner.dropTable(
            'group_reading_plan',
            false,
            true,
            true,
        );
    }

}
