import { MigrationInterface, QueryRunner, TableColumn, TableForeignKey } from 'typeorm';
import FileStorageItem from '../../entities/FileStorageItem';
import getUnixSec from '../../libs/helpers/getUnixSec';
import Article from '../../entities/Article';

export class relateArticleImageToFileStorageItems1640166919576 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('article', new TableColumn({ name: 'image_id', type: 'int', isNullable: true, }));

        const allArticles = <Article[]>await queryRunner.connection.query('SELECT * FROM article');

        for (const article of allArticles) {
            if (article.image) {
                let image_id;
                if (isNaN(parseInt(article.image))) {
                    if (!article.image.includes('/')) {
                        continue;
                    }
                    const path = article.image
                        .split('/')
                        .slice(-2)
                        .join('/');
                    const fileStorageItem = FileStorageItem.create({
                        name: path.split('/')[1],
                        base_url: '/storage/web/source',
                        path,
                        component: 'fileStorage',
                        created_at: getUnixSec(),
                    });

                    await fileStorageItem.save();
                    image_id = fileStorageItem.id;
                } else {
                    image_id = parseInt(article.image);
                }

                await queryRunner.query('UPDATE article SET image_id = ? WHERE id = ?', [image_id, article.id]);
            }
        }

        await queryRunner.createForeignKey(
            'article',
            new TableForeignKey({
                name: 'FK_article_file_storage_item',
                columnNames: ['image_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'file_storage_item',
            })
        );

        await queryRunner.dropColumn('article', 'image');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'article',
            new TableColumn({ name: 'image', type: 'varchar', isNullable: true, length: '256' })
        );

        await queryRunner.query('UPDATE article SET image = image_id');

        await queryRunner.dropForeignKey('article', 'FK_article_file_storage_item');
        await queryRunner.dropColumn('article', 'image_id');
    }
}
