import { MigrationInterface, QueryRunner } from 'typeorm';
import Config from '../../entities/Config';

export class addRankedVideoSourcesCofig1648038282655 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await Config.insert({
            name: 'ranked_video_sources',
            value: 'youtube_url, video_file_url',
        });
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await Config.delete({
            name: 'ranked_video_sources',
        });
    }
}
