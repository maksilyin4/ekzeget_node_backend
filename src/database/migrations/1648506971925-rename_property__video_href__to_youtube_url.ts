import { MigrationInterface, QueryRunner } from 'typeorm';

export class renameProperty_videoHref_toYoutubeUrl1648506971925 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'UPDATE media SET properties = REPLACE(properties, "video_href", "youtube_url")'
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            'UPDATE media SET properties = REPLACE(properties, "youtube_url", "video_href")'
        );
    }

}
