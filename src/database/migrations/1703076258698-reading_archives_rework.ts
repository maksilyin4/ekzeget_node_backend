import {
    Column,
    MigrationInterface,
    PrimaryGeneratedColumn,
    QueryRunner,
    Table,
    TableColumn,
    TableForeignKey,
} from 'typeorm';
import { readingArchives1702539861288 } from './1702539861288-reading_archives';
import User from '../../entities/User';

export class readingArchivesRework1703076258698 implements MigrationInterface {
    name = 'readingArchivesRework1703076258698';
    private readingArchivesMigration = new readingArchives1702539861288();

    public async up(queryRunner: QueryRunner): Promise<void> {
        await this.readingArchivesMigration.down(queryRunner);
        await queryRunner.createTable(
            new Table({
                name: 'reading_archive',
                engine: 'innoDB',
                columns: [
                    new TableColumn({
                        name: 'id',
                        type: 'int',
                        unsigned: true,
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'user_id',
                        type: 'int',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'mentor_id',
                        type: 'int',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'start_date',
                        type: 'date',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'stop_date',
                        type: 'date',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'members_count',
                        type: 'int',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'answers_percent',
                        type: 'float',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'correct_answers_percent',
                        type: 'float',
                        isNullable: false,
                    }),
                ],
                foreignKeys: [
                    new TableForeignKey({
                        name: 'FK_reading_archive__user_id',
                        columnNames: ['user_id'],
                        referencedTableName: 'user',
                        referencedColumnNames: ['id'],
                    }),
                    new TableForeignKey({
                        name: 'FK_reading_archive__mentor_id',
                        columnNames: ['mentor_id'],
                        referencedTableName: 'user',
                        referencedColumnNames: ['id'],
                    }),
                ],
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('reading_archive');
        await this.readingArchivesMigration.up(queryRunner);
    }

}
