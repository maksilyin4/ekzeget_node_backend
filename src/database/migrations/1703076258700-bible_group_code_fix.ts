import { IsNull, MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
import BibleGroup from '../../entities/BibleGroup';
import { generateSlug } from '../../libs/Rus2Translit/generateSlug';

export class bibleGroupCodeFix1703076258700 implements MigrationInterface {
    name = 'bibleGroupCodeFix1703076258700';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.changeColumn(
            'bible_group',
            'code',
            new TableColumn({
                name: 'code',
                type: 'varchar',
                length: '255',
                isNullable: false,
                isUnique: true,
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.changeColumn(
            'bible_group',
            'code',
            new TableColumn({
                name: 'code',
                type: 'varchar',
                length: '255',
                isNullable: true,
                isUnique: true,
            }),
        );
    }
}
