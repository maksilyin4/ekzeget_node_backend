import { MigrationInterface, QueryRunner, TableColumn, TableForeignKey } from 'typeorm';

export class planAddBranchColumn1698153395495 implements MigrationInterface {
    name = 'planAddBranchColumn1698153395495';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'plan',
            new TableColumn({
                name: 'branch',
                type: 'int',
                unsigned: true,
                isNullable: true,
            }),
        );

        await queryRunner.dropForeignKey('group_reading_plan', 'FK_group_reading_plan_translate_id');
        await queryRunner.dropColumn('group_reading_plan', 'translate_id');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('plan', 'branch');

        await queryRunner.addColumn(
            'group_reading_plan',
            new TableColumn({
                name: 'translate_id',
                type: 'int',
                isNullable: true,
            }),
        );
        await queryRunner.createForeignKey(
            'group_reading_plan',
            new TableForeignKey({
                name: 'FK_group_reading_plan_translate_id',
                columnNames: ['translate_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'translate',
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE',
            }),
        );
    }

}
