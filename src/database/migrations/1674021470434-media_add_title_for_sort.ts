import { Not, Equal, MigrationInterface, QueryRunner, TableColumn, TableIndex } from 'typeorm';
import Media from '../../entities/Media';

export class mediaAddTitleForSort1674021470434 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'media',
            new TableColumn({
                name: 'title_for_sort',
                type: 'varchar',
                isNullable: true,
                length: '255',
            })
        );

        const media = await Media.find({ type: Not(Equal('C')) });
        for (const mediaItem of media) {
            mediaItem.makeSortable();
            await mediaItem.save({ listeners: false });
        }

        await queryRunner.createIndex(
            'media',
            new TableIndex({
                name: 'media_title_for_sort_idx',
                columnNames: ['title_for_sort'],
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('media', 'title_for_sort');
    }
}
