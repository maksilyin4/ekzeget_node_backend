import { MigrationInterface, QueryRunner, Table, TableColumn } from 'typeorm';

export class replaceQuizCommentWithJunction1660987667428 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('queeze_questions', 'comment');

        await queryRunner.createTable(
            new Table({
                name: 'quiz_question_interpretation',
                columns: [
                    new TableColumn({
                        name: 'quiz_question_id',
                        type: 'int',
                        unsigned: true,
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'interpretation_id',
                        type: 'int',
                        isNullable: false,
                    }),
                ],
                foreignKeys: [
                    {
                        columnNames: ['quiz_question_id'],
                        referencedTableName: 'queeze_questions',
                        referencedColumnNames: ['id'],
                    },
                    {
                        columnNames: ['interpretation_id'],
                        referencedTableName: 'interpretation',
                        referencedColumnNames: ['id'],
                    },
                ],
                indices: [
                    {
                        columnNames: ['quiz_question_id', 'interpretation_id'],
                        isUnique: true,
                    },
                ],
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'queeze_questions',
            new TableColumn({ name: 'comment', type: 'text', default: null, isNullable: true })
        );
        await queryRunner.dropTable('quiz_question_interpretation');
    }
}
