import {MigrationInterface, QueryRunner} from "typeorm";

export class mediaAddDailyMedia1655735406716 implements MigrationInterface {
    name = 'mediaAddDailyMedia1655735406716'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE `media` ADD `daily_media` date NULL');
        await queryRunner.query('CREATE UNIQUE INDEX `media_daily_media_idx` ON `media` (`daily_media`)');
        await queryRunner.query('ALTER TABLE `media` ADD `celebration_id` int NULL');
        await queryRunner.query('CREATE UNIQUE INDEX `REL_746f701da1482a62d0216fd9c3` ON `media` (`celebration_id`)');
        queryRunner.query('ALTER TABLE `media` ADD CONSTRAINT `FK_746f701da1482a62d0216fd9c34` FOREIGN KEY (`celebration_id`) REFERENCES `celebration`(`id`) ON DELETE SET NULL ON UPDATE CASCADE');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('DROP INDEX `media_daily_media_idx` ON `media`');
        await queryRunner.query('ALTER TABLE `media` DROP COLUMN `daily_media`');
        await queryRunner.query('ALTER TABLE `media` DROP FOREIGN KEY `FK_746f701da1482a62d0216fd9c34`');
        await queryRunner.query('DROP INDEX `REL_746f701da1482a62d0216fd9c3` ON `media`');
        await queryRunner.query('ALTER TABLE `media` DROP COLUMN `celebration_id`');
    }
}
