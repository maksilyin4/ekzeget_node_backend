alter table queeze_questions
  add media_question int default null null;

alter table queeze_questions
  add media_answer int default null null;

alter table queeze_questions
  add constraint queeze_questions_media_answer_fk
    foreign key (media_answer) references media (id)
      on delete set null;

alter table queeze_questions
  add constraint queeze_questions_media_question_fk
    foreign key (media_question) references media (id)
      on delete set null;

INSERT INTO config (name, value)
VALUES
  ('quiz_questions_audio_dir', '/quiz/audio/questions'),
  ('quiz_answers_audio_dir', '/quiz/audio/answers'),
  ('quiz_introduction_audio', 'introduction.mp3'),
  ('quiz_wrong_audio', 'wrong.mp3');

INSERT INTO media_category
(
  title, description, image, created_by, edited_by, created_at, edited_at, active, parent_id, code, single_template,
  template_code
)
VALUES
  (
    'Вопросы викторины', '<p>Вопросы викторины</p>', NULL, 6498, NULL, 1621940613, NULL, 1, 0, 'voprosy-viktoriny',
    NULL, NULL
  ),
  (
    'Ответы на вопросы викторины', '<p>Ответы на вопросы викторины</p>', NULL, 6498, NULL, 1621940633, NULL, 1, 0,
    'otvety-na-voprosy-viktoriny', NULL, NULL
  );

# Revert:
# alter table queeze_questions drop foreign key queeze_questions_media_answer_fk;
# alter table queeze_questions drop column media_answer;
# alter table queeze_questions drop foreign key queeze_questions_media_question_fk;
# alter table queeze_questions drop column media_question;
#
# DELETE FROM config WHERE name = 'quiz_questions_audio_dir' OR name = 'quiz_answers_audio_dir' OR name = 'quiz_introduction_audio' OR name = 'quiz_wrong_audio';
# DELETE FROM media_category WHERE title = 'Вопросы викторины' OR title = 'Ответы на вопросы викторины';
