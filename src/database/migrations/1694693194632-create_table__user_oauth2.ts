import {MigrationInterface, QueryRunner, Table, TableColumn, TableIndex} from 'typeorm';

export class createTable_userOauth21694693194632 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'user_oauth2',
                columns: [
                    new TableColumn({
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    }),
                    new TableColumn({
                        name: 'user_id',
                        type: 'int',
                        isNullable: true,
                    }),

                    new TableColumn({
                        name: 'external_user_id',
                        type: 'varchar',
                        length: '255',
                    }),

                    new TableColumn({ name: 'provider', type: 'varchar', length: '32' }),

                    new TableColumn({
                        name: 'created_at',
                        type: 'int',
                    }),
                ],
                foreignKeys: [
                    {
                        columnNames: ['user_id'],
                        referencedTableName: 'user',
                        referencedColumnNames: ['id'],
                    },
                ],
            })
        );

        await queryRunner.createIndex(
            'user_oauth2',
            new TableIndex({
                name: 'idx_user_oauth2_external_user_id',
                columnNames: ['external_user_id'],
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('user_oauth2');
    }
}
