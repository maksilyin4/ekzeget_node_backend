import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
import MediaStatistics from '../../entities/MediaStatistics';

export class defaultMediaStatisticsCountersToZero1640349120428 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await MediaStatistics.update({ view_cnt: null }, { view_cnt: 0 });
        await MediaStatistics.update({ download_read_cnt: null }, { download_read_cnt: 0 });
        await MediaStatistics.update({ download_listen_cnt: null }, { download_listen_cnt: 0 });

        await queryRunner.changeColumns('media_statistics', [
            {
                oldColumn: new TableColumn({ name: 'view_cnt', type: 'bigint' }),
                newColumn: new TableColumn({ name: 'view_cnt', type: 'int', default: 0 }),
            },
            {
                oldColumn: new TableColumn({ name: 'download_read_cnt', type: 'bigint' }),
                newColumn: new TableColumn({ name: 'download_read_cnt', type: 'int', default: 0 }),
            },
            {
                oldColumn: new TableColumn({ name: 'download_listen_cnt', type: 'bigint' }),
                newColumn: new TableColumn({
                    name: 'download_listen_cnt',
                    type: 'int',
                    default: 0,
                }),
            },
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.changeColumns('media_statistics', [
            {
                oldColumn: new TableColumn({ name: 'view_cnt', type: 'int' }),
                newColumn: new TableColumn({ name: 'view_cnt', type: 'bigint', isNullable: true }),
            },
            {
                oldColumn: new TableColumn({ name: 'download_read_cnt', type: 'int' }),
                newColumn: new TableColumn({ name: 'download_read_cnt', type: 'bigint', isNullable: true }),
            },
            {
                oldColumn: new TableColumn({ name: 'download_listen_cnt', type: 'int' }),
                newColumn: new TableColumn({
                    name: 'download_listen_cnt',
                    type: 'bigint',
                    isNullable: true,
                }),
            },
        ]);

        await MediaStatistics.update({ view_cnt: 0 }, { view_cnt: null });
        await MediaStatistics.update({ download_read_cnt: 0 }, { download_read_cnt: null });
        await MediaStatistics.update({ download_listen_cnt: 0 }, { download_listen_cnt: null });
    }
}
