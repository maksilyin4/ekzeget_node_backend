create table IF NOT EXISTS meta_template
(
  id          int auto_increment,
  title       varchar(512) not null,
  url         varchar(255) not null,
  description text         null,
  h_one       varchar(128) null,
  keywords    text         null,
  created_at  int          null,
  updated_at  int          null,
  constraint meta_template_id_uindex
    unique (id),
  constraint meta_template_title_uindex
    unique (title),
  constraint meta_template_url_uindex
    unique (url),
  constraint meta_template_pk
    primary key (id)
);

# Revert:
# DROP TABLE meta_template;
