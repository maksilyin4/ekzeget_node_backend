create table session
(
  id         varchar(255),
  json       text,
  expired_at bigint,
  constraint id_pk primary key (id)
);

create index idx_session_expired_at
  on session (expired_at);

# Revert:
# DROP INDEX idx_session_expired_at ON session;
# DROP TABLE session;
