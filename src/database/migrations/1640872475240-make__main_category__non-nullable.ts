import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

const CATEGORY_FOR_RELATED_MEDIA_NAME = 'related media should be moved to their own table';

export class make_mainCategory_nonNullable1640872475240 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `INSERT INTO media_category SET title = '${CATEGORY_FOR_RELATED_MEDIA_NAME}', description = '', active = 0`
        );

        await queryRunner.query(
            `UPDATE media
                    SET main_category = (
                        SELECT id FROM media_category WHERE title = '${CATEGORY_FOR_RELATED_MEDIA_NAME}'
                    )
                    WHERE main_category IS NULL AND type IN ('A', 'C')`
        );

        await queryRunner.query(
            `UPDATE media
                    SET main_category = 
                        CASE 
                            WHEN type = 'V' THEN 5
                            WHEN type = 'I' THEN 6
                            WHEN type = 'B' THEN 13
                        END
                    WHERE main_category IS NULL AND type NOT IN ('A', 'C') AND created_at >=  1631606086`
        );

        await queryRunner.changeColumn(
            'media',
            new TableColumn({ name: 'main_category', type: 'int', length: '11', isNullable: true }),
            new TableColumn({ name: 'main_category', type: 'int', length: '11', isNullable: false })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.changeColumn(
            'media',
            new TableColumn({
                name: 'main_category',
                type: 'int',
                length: '11',
                isNullable: false,
            }),
            new TableColumn({ name: 'main_category', type: 'int', length: '11', isNullable: true })
        );
        await queryRunner.query(
            `UPDATE media
                   SET main_category = NULL
                   WHERE 
                          main_category IN (5, 6, 13) 
                      AND type NOT IN ('A', 'C')
                      AND created_at BETWEEN 1631606086 AND 1640872475`
        );

        await queryRunner.query(
            `UPDATE media
                    SET main_category = NULL
                    WHERE 
                          main_category = (
                              SELECT id FROM media_category WHERE title = '${CATEGORY_FOR_RELATED_MEDIA_NAME}'
                          )
                      AND type IN ('A', 'C')`
        );

        await queryRunner.query(
            `DELETE FROM media_category WHERE title = '${CATEGORY_FOR_RELATED_MEDIA_NAME}'`
        );
    }
}
