import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
import ReadingArchive from '../../entities/ReadingArchive';

export class readingArchiveMoreStats1703817215852 implements MigrationInterface {
    name = 'readingArchiveMoreStats1703817215852';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumns('reading_archive', [
            new TableColumn({ type: 'varchar', name: 'plan_description', isNullable: true, length: '255' }),
            new TableColumn({ type: 'text', name: 'plan_comment', isNullable: true }),
            new TableColumn({ type: 'int', name: 'plan_length', isNullable: true }),
            new TableColumn({ type: 'int', name: 'comments_count', isNullable: true }),
        ]);

        await queryRunner.manager.update(
            ReadingArchive,
            {
                plan_description: null,
                plan_comment: null,
                plan_length: null,
                comments_count: null,
            },
            {
                plan_description: `this plan added via migration`,
                plan_comment: 'this comment added via migration',
                plan_length: 0,
                comments_count: 0,
            },
        );

        await queryRunner.changeColumn(
            'reading_archive',
            'plan_description',
            new TableColumn({ type: 'varchar', name: 'plan_description', isNullable: false, length: '255' }),
        );
        await queryRunner.changeColumn(
            'reading_archive',
            'plan_comment',
            new TableColumn({ type: 'text', name: 'plan_comment', isNullable: false }),
        );
        await queryRunner.changeColumn(
            'reading_archive',
            'plan_length',
            new TableColumn({ type: 'int', name: 'plan_length', isNullable: false }),
        );
        await queryRunner.changeColumn(
            'reading_archive',
            'comments_count',
            new TableColumn({ type: 'int', name: 'comments_count', isNullable: false }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumns('reading_archive', [
            'plan_description',
            'plan_comment',
            'plan_length',
            'comments_count',
        ]);
    }
}
