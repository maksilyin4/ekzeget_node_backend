import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class userReadingPlan_dropDataColumn1702996672244 implements MigrationInterface {
    name = 'userReadingPlan_dropDataColumn1702996672244';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('user_reading_plan', 'data');
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'user_reading_plan',
            new TableColumn({
                name: 'data',
                type: 'text',
                isNullable: true,
            }),
        );
    }

}
