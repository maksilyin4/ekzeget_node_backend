import { MigrationInterface, QueryRunner, Table, TableColumn, TableForeignKey } from 'typeorm';

export class readingDayComment1697719306536 implements MigrationInterface {
    name = 'readingDayComment1697719306536';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'reading_plan_comment',
                columns: [
                    new TableColumn({
                        name: 'id',
                        type: 'int',
                        unsigned: true,
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'author_id',
                        type: 'int',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'plan_id',
                        type: 'int',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'group_id',
                        type: 'int',
                        unsigned: true,
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'content',
                        type: 'text',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'reply_to_comment_id',
                        type: 'int',
                        unsigned: true,
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'created_at',
                        type: 'timestamp',
                        default: 'CURRENT_TIMESTAMP',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'updated_at',
                        type: 'timestamp',
                        default: 'CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
                        isNullable: false,
                        onUpdate: 'CURRENT_TIMESTAMP',
                    }),
                ],

                foreignKeys: [
                    new TableForeignKey({
                        name: 'FK_reading_plan_comment__author_id',
                        columnNames: ['author_id'],
                        referencedColumnNames: ['id'],
                        referencedTableName: 'user',
                        onDelete: 'CASCADE',
                    }),
                    new TableForeignKey({
                        name: 'FK_reading_plan_comment__plan_id',
                        columnNames: ['plan_id'],
                        referencedColumnNames: ['id'],
                        referencedTableName: 'plan',
                        onDelete: 'CASCADE',
                    }),
                    new TableForeignKey({
                        name: 'FK_reading_plan_comment__group_id',
                        columnNames: ['group_id'],
                        referencedColumnNames: ['id'],
                        referencedTableName: 'group_reading_plan',
                        onDelete: 'CASCADE',
                    }),
                    new TableForeignKey({
                        name: 'FK_reading_plan_comment__reply_to_comment_id',
                        columnNames: ['reply_to_comment_id'],
                        referencedColumnNames: ['id'],
                        referencedTableName: 'reading_plan_comment',
                        onDelete: 'CASCADE',
                    }),
                ],

                engine: 'innoDB',
            }),
            true,
        );

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey(
            'reading_plan_comment',
            'FK_reading_plan_comment__author_id',
        );
        await queryRunner.dropForeignKey(
            'reading_plan_comment',
            'FK_reading_plan_comment__plan_id',
        );
        await queryRunner.dropForeignKey(
            'reading_plan_comment',
            'FK_reading_plan_comment__reply_to_comment_id',
        );
        await queryRunner.dropTable('reading_plan_comment');
    }

}
