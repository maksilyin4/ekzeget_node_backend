create fulltext index idx_search on verse(text);
create fulltext index idx_search on verse_translate(text);

# Revert:
# DROP INDEX idx_search ON verse;
# DROP INDEX idx_search ON verse_translate;
