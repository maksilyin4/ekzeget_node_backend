import { MigrationInterface, QueryRunner, TableColumn, TableForeignKey } from 'typeorm';

export class queezeForReadingPlans1698744020327 implements MigrationInterface {
    name = 'queezeForReadingPlans1698744020327';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'queeze_applications',
            new TableColumn({
                name: 'plan_id',
                type: 'int',
                isNullable: true,
            }),
        );
        await queryRunner.createForeignKey(
            'queeze_applications',
            new TableForeignKey({
                name: 'FK_queeze_applications__plan_id',
                columnNames: ['plan_id'],
                referencedTableName: 'plan',
                referencedColumnNames: ['id'],
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey(
            'queeze_applications',
            'FK_queeze_applications__plan_id',
        );
        await queryRunner.dropColumn(
            'queeze_applications',
            'plan_id',
        );
    }

}
