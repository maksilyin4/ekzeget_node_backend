# В базе прода эти данные уже есть, миграция сделана для локальных баз
TRUNCATE config;

INSERT INTO config (name, value) VALUES ('image_sizes', '[{"code":"news_image","width":210,"height":210},{"code":"big_slider","width":640,"height":420},{"code":"small_slider","width":250,"height":250},{"code":"small_slider_mob","width":180,"height":180},{"code":"news_image_mob","width":240,"height":220},{"code":"big_slider_mob","width":420,"height":420}]');
INSERT INTO config (name, value) VALUES ('queeze_tutorial_video_url', 'https://www.youtube.com/embed/NRtb2WTGbPs');
INSERT INTO config (name, value) VALUES ('mediateka_tutorial_video_url', 'https://www.youtube.com/embed/XtTaE5RuByo');
INSERT INTO config (name, value) VALUES ('bible_tutorial_video_url', 'https://www.youtube.com/embed/XtTaE5RuByo');
INSERT INTO config (name, value) VALUES ('quiz_questions_audio_dir', '/quiz/audio/questions');
INSERT INTO config (name, value) VALUES ('quiz_answers_audio_dir', '/quiz/audio/answers');
INSERT INTO config (name, value) VALUES ('quiz_introduction_audio', 'introduction.mp3');
INSERT INTO config (name, value) VALUES ('quiz_wrong_audio', 'wrong.mp3');
