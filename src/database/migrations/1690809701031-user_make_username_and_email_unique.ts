import { MigrationInterface, QueryRunner, TableColumn, TableIndex } from 'typeorm';

export class userMakeUsernameAndEmailUnique1690809701031 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        // create temporary indices to speedup subsequent duplicates deletion
        await queryRunner.createIndex(
            'user',
            new TableIndex({
                name: 'temp1',
                columnNames: ['username'],
            })
        );
        await queryRunner.createIndex(
            'user',
            new TableIndex({
                name: 'temp2',
                columnNames: ['email'],
            })
        );
        // delete rows with non-unique email and username values
        await queryRunner.query(
            'DELETE user1 FROM user user1 JOIN user user2 WHERE user1.id < user2.id AND (user1.email = user2.email OR user1.username = user2.username)'
        );

        await queryRunner.dropIndex('user', 'temp1');
        await queryRunner.dropIndex('user', 'temp2');

        await queryRunner.changeColumn(
            'user',
            'username',
            new TableColumn({
                name: 'username',
                type: 'varchar',
                length: '32',
                isUnique: true,
                isNullable: false,
            })
        );

        await queryRunner.changeColumn(
            'user',
            'email',
            new TableColumn({
                name: 'email',
                type: 'varchar',
                length: '255',
                isUnique: true,
                isNullable: true,
            })
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.changeColumn(
            'user',
            'username',
            new TableColumn({
                name: 'username',
                type: 'varchar',
                length: '32',
                isUnique: false,
                isNullable: true,
            })
        );

        await queryRunner.changeColumn(
            'user',
            'email',
            new TableColumn({
                name: 'email',
                type: 'varchar',
                length: '255',
                isUnique: false,
                isNullable: false,
            })
        );
    }
}
