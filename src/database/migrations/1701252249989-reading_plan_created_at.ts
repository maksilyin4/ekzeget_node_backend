import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class readingPlanCreatedAt1701252249989 implements MigrationInterface {
    name = 'readingPlanCreatedAt1701252249989';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'reading_plan',
            new TableColumn({
                name: 'created_at',
                type: 'datetime',
                isNullable: false,
                default: 'CURRENT_TIMESTAMP',
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('reading_plan', 'created_at');
    }

}
