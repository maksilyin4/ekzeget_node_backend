import { MigrationInterface, QueryRunner, Table, TableColumn, TableIndex } from 'typeorm';

export class childBible1711527094193 implements MigrationInterface {
    name = 'childBible1711527094193';

    public async up(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.createTable(
            new Table({
                name: 'child_book',
                engine: 'innoDB',
                columns: [
                    new TableColumn({
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'title',
                        type: 'varchar',
                        length: '255',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'short_title',
                        type: 'varchar',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'parts',
                        type: 'int',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'code',
                        type: 'varchar',
                        length: '50',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'legacy_code',
                        type: 'varchar',
                        length: '12',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'menu',
                        type: 'varchar',
                        length: '64',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'author',
                        type: 'varchar',
                        length: '255',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'sort',
                        type: 'int',
                        isNullable: false,
                        default: 0,
                    }),
                ],
                indices: [
                    new TableIndex({
                        name: 'idx_book_code',
                        columnNames: ['code'],
                    }),
                    new TableIndex({
                        name: 'idx-book-legacy_code',
                        columnNames: ['legacy_code'],
                    }),
                    new TableIndex({
                        name: 'idx_book_sort',
                        columnNames: ['sort'],
                    }),
                ],
            }),
            true,
        );

        await queryRunner.createTable(
            new Table({
                name: 'child_book_descriptor',
                engine: 'innoDB',
                columns: [
                    new TableColumn({
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'title',
                        type: 'varchar',
                        length: '255',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'active',
                        type: 'int',
                        isNullable: true,
                    }),
                ],
                indices: [
                    new TableIndex({
                        name: 'idx_book_descriptor_active',
                        columnNames: ['active'],
                    }),
                ],
            }),
            true,
        );

        await queryRunner.createTable(
            new Table({
                name: 'chapters_child_book',
                engine: 'innoDB',
                columns: [
                    new TableColumn({
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'title',
                        type: 'varchar',
                        length: '32',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'text',
                        type: 'text',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'bookId',
                        type: 'int',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'number',
                        type: 'int',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'image_id',
                        type: 'varchar',
                        length: '255',
                        isNullable: true,
                    }),
                    new TableColumn({
                        name: 'testament_id',
                        type: 'int',
                        isNullable: false,
                    }),
                    new TableColumn({
                        name: 'code',
                        type: 'varchar',
                        length: '50', isNullable: false,
                    }),
                ],
            }),
            true,
        );

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('chapters_child_book');
        await queryRunner.dropTable('child_book_descriptor');
        await queryRunner.dropTable('child_book');
    }
}
