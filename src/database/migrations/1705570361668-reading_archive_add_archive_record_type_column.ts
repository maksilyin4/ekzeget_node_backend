import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
import ReadingArchive, { ReadingArchiveRecordType } from '../../entities/ReadingArchive';

export class readingArchiveAddArchiveRecordTypeColumn1705570361668 implements MigrationInterface {
    name = 'readingArchiveAddArchiveRecordTypeColumn1705570361668';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'reading_archive',
            new TableColumn({
                name: 'record_type',
                type: 'varchar',
                length: '31',
                isNullable: false,
            }),
        );
        await queryRunner.query(`
            UPDATE reading_archive
            SET record_type = CASE
                                  WHEN group_id IS NOT NULL AND user_id = mentor_id
                                      THEN '${ReadingArchiveRecordType.GROUP_MENTOR}'
                                  WHEN group_id IS NOT NULL
                                      THEN '${ReadingArchiveRecordType.GROUP_USER}'
                                  WHEN group_id IS NULL
                                      THEN '${ReadingArchiveRecordType.SOLO_USER}'
                END;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('reading_archive', 'record_type');
    }

}
