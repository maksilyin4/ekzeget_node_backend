import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class readingArchiveGroupRelation1703592163978 implements MigrationInterface {
    name = 'readingArchiveGroupRelation1703592163978';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'reading_archive',
            new TableColumn({
                name: 'group_id',
                type: 'int',
                isNullable: true,
                unsigned: true,
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('reading_archive', 'group_id');
    }

}
