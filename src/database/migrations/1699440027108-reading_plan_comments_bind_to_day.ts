import { MigrationInterface, QueryRunner, TableColumn, TableForeignKey } from 'typeorm';

export class readingPlanCommentsBindToDay1699440027108 implements MigrationInterface {
    name = 'readingPlanCommentsBindToDay1699440027108';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey('reading_plan_comment', 'FK_reading_plan_comment__plan_id');
        await queryRunner.dropColumn('reading_plan_comment', 'plan_id');

        await queryRunner.addColumn(
            'reading_plan_comment',
            new TableColumn({
                name: 'day',
                type: 'int',
                unsigned: true,
                isNullable: false,
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('reading_plan_comment', 'day');

        await queryRunner.addColumn(
            'reading_plan_comment',
            new TableColumn({
                name: 'plan_id',
                type: 'int',
                isNullable: false,
            }),
        );
        await queryRunner.createForeignKey(
            'reading_plan_comment',
            new TableForeignKey({
                name: 'FK_reading_plan_comment__plan_id',
                columnNames: ['plan_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'plan',
                onDelete: 'CASCADE',
            }),
        );
    }

}
