import { IsNull, MigrationInterface, Not, QueryRunner, TableColumn, TableForeignKey } from 'typeorm';
import ReadingPlan from '../../entities/ReadingPlan';

export class customReadingPlan1696232487348 implements MigrationInterface {
    name = 'customReadingPlan1696232487348';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'reading_plan',
            new TableColumn({
                name: 'creator_id',
                type: 'int',
                isNullable: true,
            }),
        );

        // Adding the foreign key
        await queryRunner.createForeignKey(
            'reading_plan',
            new TableForeignKey({
                name: 'FK_reading_plan_creator_id',
                columnNames: ['creator_id'],
                referencedColumnNames: ['id'],
                referencedTableName: 'user',
                onDelete: 'CASCADE',
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const table = await queryRunner.getTable('reading_plan');
        const foreignKey = table.foreignKeys.find(fk => fk.columnNames.includes('creator_id'));

        await ReadingPlan.delete({ creator_id: Not(IsNull()) });
        await queryRunner.dropForeignKey(table, foreignKey);
        await queryRunner.dropColumn(table, 'creator_id');
    }

}
