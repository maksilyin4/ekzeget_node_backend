import { Connection, In, MigrationInterface, QueryRunner, Repository, TableColumn } from 'typeorm';
import UserReadingPlan from '../../entities/UserReadingPlan';
import { groupBy } from '../../libs/helpers/groupBy';

export class readingArchives1702539861288 implements MigrationInterface {
    name = 'readingArchives1702539861288';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'user_reading_plan',
            new TableColumn({
                name: 'archived',
                type: 'boolean',
                default: false,
            }),
        );

        await queryRunner.addColumn(
            'group_reading_plan',
            new TableColumn({
                name: 'archived',
                type: 'boolean',
                default: false,
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropColumn('user_reading_plan', 'archived');
        await queryRunner.dropColumn('group_reading_plan', 'archived');
    }

}
