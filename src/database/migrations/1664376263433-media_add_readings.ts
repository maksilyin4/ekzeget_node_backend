import {MigrationInterface, QueryRunner} from "typeorm";

export class mediaAddReadings1664376263433 implements MigrationInterface {
    name = 'mediaAddReadings1664376263433'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`media\` ADD \`reading_apostolic_id\` int NULL`);
        await queryRunner.query(`ALTER TABLE \`media\` ADD \`reading_gospel_id\` int NULL`);
        await queryRunner.query(`ALTER TABLE \`media\` ADD \`reading_post_id\` int NULL`);
        await queryRunner.query(`ALTER TABLE \`media\` ADD \`reading_morning_id\` int NULL`);
        await queryRunner.query(`ALTER TABLE \`media\` ADD \`mineja_id\` int NULL`);
        await queryRunner.query(`ALTER TABLE \`media\` ADD CONSTRAINT \`FK_0b52c6315171d7986e9d31610b9\` FOREIGN KEY (\`reading_apostolic_id\`) REFERENCES \`apostolic_reading\`(\`id\`) ON DELETE SET NULL ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`media\` ADD CONSTRAINT \`FK_e28767d15d9d7313f3ed44578e6\` FOREIGN KEY (\`reading_gospel_id\`) REFERENCES \`gospel_reading\`(\`id\`) ON DELETE SET NULL ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`media\` ADD CONSTRAINT \`FK_b382f6a5b50b48b7cdecdf6e239\` FOREIGN KEY (\`reading_post_id\`) REFERENCES \`post_reading\`(\`id\`) ON DELETE SET NULL ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`media\` ADD CONSTRAINT \`FK_a469dbc801869ed17ae2ebee4a8\` FOREIGN KEY (\`reading_morning_id\`) REFERENCES \`morning_reading\`(\`id\`) ON DELETE SET NULL ON UPDATE CASCADE`);
        //await queryRunner.query(`ALTER TABLE \`media\` ADD CONSTRAINT \`FK_d3fe6a80c9783d969b102914daa\` FOREIGN KEY (\`mineja_id\`) REFERENCES \`mineja\`(\`id\`) ON DELETE SET NULL ON UPDATE CASCADE`);

        await queryRunner.query(`CREATE INDEX \`media_mineja_id_idx\` ON \`media\` (\`mineja_id\`)`);
        await queryRunner.query(`CREATE INDEX \`media_reading_morning_id_idx\` ON \`media\` (\`reading_morning_id\`)`);
        await queryRunner.query(`CREATE INDEX \`media_reading_post_id_idx\` ON \`media\` (\`reading_post_id\`)`);
        await queryRunner.query(`CREATE INDEX \`media_reading_gospel_id_idx\` ON \`media\` (\`reading_gospel_id\`)`);
        await queryRunner.query(`CREATE INDEX \`media_reading_apostolic_id_idx\` ON \`media\` (\`reading_apostolic_id\`)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX \`media_reading_apostolic_id_idx\` ON \`media\``);
        await queryRunner.query(`DROP INDEX \`media_reading_gospel_id_idx\` ON \`media\``);
        await queryRunner.query(`DROP INDEX \`media_reading_post_id_idx\` ON \`media\``);
        await queryRunner.query(`DROP INDEX \`media_reading_morning_id_idx\` ON \`media\``);
        await queryRunner.query(`DROP INDEX \`media_mineja_id_idx\` ON \`media\``);

        //await queryRunner.query(`ALTER TABLE \`media\` DROP FOREIGN KEY \`FK_d3fe6a80c9783d969b102914daa\``);
        await queryRunner.query(`ALTER TABLE \`media\` DROP FOREIGN KEY \`FK_a469dbc801869ed17ae2ebee4a8\``);
        await queryRunner.query(`ALTER TABLE \`media\` DROP FOREIGN KEY \`FK_b382f6a5b50b48b7cdecdf6e239\``);
        await queryRunner.query(`ALTER TABLE \`media\` DROP FOREIGN KEY \`FK_e28767d15d9d7313f3ed44578e6\``);
        await queryRunner.query(`ALTER TABLE \`media\` DROP FOREIGN KEY \`FK_0b52c6315171d7986e9d31610b9\``);
        await queryRunner.query(`ALTER TABLE \`media\` DROP COLUMN \`mineja_id\``);
        await queryRunner.query(`ALTER TABLE \`media\` DROP COLUMN \`reading_morning_id\``);
        await queryRunner.query(`ALTER TABLE \`media\` DROP COLUMN \`reading_post_id\``);
        await queryRunner.query(`ALTER TABLE \`media\` DROP COLUMN \`reading_gospel_id\``);
        await queryRunner.query(`ALTER TABLE \`media\` DROP COLUMN \`reading_apostolic_id\``);
    }

}
