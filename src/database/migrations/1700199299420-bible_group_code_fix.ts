import { IsNull, MigrationInterface, QueryRunner, TableColumn } from 'typeorm';
import BibleGroup from '../../entities/BibleGroup';
import { generateSlug } from '../../libs/Rus2Translit/generateSlug';

export class bibleGroupCodeFix1700199299420 implements MigrationInterface {
    name = 'bibleGroupCodeFix1700199299420';

    async generateCodesForAllGroups() {
        const groups = await BibleGroup.find({ where: { code: IsNull() } });
        console.log(`Generating codes for group ids: ${groups.map(it => it.id)}`);
        for (const group of groups) {
            console.log(`Group: ${group.id} - ${group.name}`);
            group.code = generateSlug(group.name).toLowerCase();
            console.log(`Generated code: ${group.code}`);
            try {
                await group.save();
            } catch {
                const newCode = group.code + `-${group.id}`;
                console.warn(`Duplicate code: ${group.code}. Adding id: ${newCode}`);
                group.code = newCode;
                await group.save();
            }
        }
    }

    public async up(queryRunner: QueryRunner): Promise<void> {
        await this.generateCodesForAllGroups();
        await queryRunner.changeColumn(
            'bible_group',
            'code',
            new TableColumn({
                name: 'code',
                type: 'varchar',
                length: '255',
                isNullable: false,
            }),
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.changeColumn(
            'bible_group',
            'code',
            new TableColumn({
                name: 'code',
                type: 'varchar',
                length: '255',
                isNullable: true,
            }),
        );
    }
}
