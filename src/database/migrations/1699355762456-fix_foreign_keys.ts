import { MigrationInterface, QueryRunner, TableForeignKey } from 'typeorm';

export class fixForeignKeys1699355762456 implements MigrationInterface {
    name = 'fixForeignKeys1699355762456';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey(
            'queeze_applications',
            'FK_queeze_applications__plan_id',
        );
        await queryRunner.createForeignKey(
            'queeze_applications',
            new TableForeignKey({
                name: 'FK_queeze_applications__plan_id',
                columnNames: ['plan_id'],
                referencedTableName: 'plan',
                referencedColumnNames: ['id'],
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE',
            }),
        );

        await queryRunner.dropForeignKey(
            'user_reading_plan_closed_plans',
            'user_reading_plan_closed_plans__user_reading_plan_id',
        );
        await queryRunner.dropForeignKey(
            'user_reading_plan_closed_plans',
            'user_reading_plan_closed_plans__plan_id',
        );
        await queryRunner.createForeignKeys('user_reading_plan_closed_plans', [
            new TableForeignKey({
                name: 'FK_user_reading_plan_closed_plans__user_reading_plan_id',
                columnNames: ['user_reading_plan_id'],
                referencedTableName: 'user_reading_plan',
                referencedColumnNames: ['id'],
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE',
            }),
            new TableForeignKey({
                name: 'FK_user_reading_plan_closed_plans__plan_id',
                columnNames: ['plan_id'],
                referencedTableName: 'plan',
                referencedColumnNames: ['id'],
                onUpdate: 'CASCADE',
                onDelete: 'CASCADE',
            }),
        ]);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey(
            'queeze_applications',
            'FK_queeze_applications__plan_id',
        );
        await queryRunner.createForeignKey(
            'queeze_applications',
            new TableForeignKey({
                name: 'FK_queeze_applications__plan_id',
                columnNames: ['plan_id'],
                referencedTableName: 'plan',
                referencedColumnNames: ['id'],
            }),
        );

        await queryRunner.dropForeignKey(
            'user_reading_plan_closed_plans',
            'user_reading_plan_closed_plans__user_reading_plan_id',
        );
        await queryRunner.dropForeignKey(
            'user_reading_plan_closed_plans',
            'user_reading_plan_closed_plans__plan_id',
        );
        await queryRunner.createForeignKeys('user_reading_plan_closed_plans', [
            new TableForeignKey({
                name: 'user_reading_plan_closed_plans__user_reading_plan_id',
                columnNames: ['user_reading_plan_id'],
                referencedTableName: 'user_reading_plan',
                referencedColumnNames: ['id'],
            }),
            new TableForeignKey({
                name: 'user_reading_plan_closed_plans__plan_id',
                columnNames: ['plan_id'],
                referencedTableName: 'plan',
                referencedColumnNames: ['id'],
            }),
        ]);
    }

}
