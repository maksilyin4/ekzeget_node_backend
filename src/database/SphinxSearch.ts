import mysql from 'mysql';

export default class SphinxSearch {
    private static connection;

    static init(host = '127.0.0.1', port = 9306) {
        this.connection = mysql.createPool({
            host,
            port,
        });
    }

    static async query(options: {
        match: string;
        against: string;
        where?: string;
        params?: (string | null | number | boolean)[];
    }): Promise<number[]> {
        if (!options.params) {
            options.params = [];
        }
        let query = `SELECT id FROM ${this.connection.escapeId(options.against)} WHERE MATCH(?)`;
        if (options.where) {
            query += ` AND ${options.where}`;
        }
        query += ' LIMIT 1000';

        return new Promise((accept, reject) => {
            this.connection.query(query, [options.match, ...options.params], function(error, results, fields) {
                if (error) reject(error);
                else accept(results.map(res => res.id));
            });
        });
    }
}
