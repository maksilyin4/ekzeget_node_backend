import EnvironmentChecker from '../core/EnvironmentChecker';
import Entities from '../entities';
import { ConnectionOptions } from 'typeorm/connection/ConnectionOptions';

const envs = new EnvironmentChecker().getCheckedEnvironment();

export = {
    type: 'mysql',

    host: envs.MYSQL_HOST,
    port: +envs.MYSQL_PORT,
    username: envs.MYSQL_USER,
    password: envs.MYSQL_PASSWORD,
    database: envs.MYSQL_DATABASE,
    logging: ['dev'].includes(envs.ENV),
    migrationsRun: ['local', 'dev'].includes(envs.ENV),
    migrations: ['./src/database/migrations/*{.ts,.js}'],

    entities: Entities,
    synchronize: false,
    cache: false,

    cli: {
        entitiesDir: 'src/entities',
        migrationsDir: 'src/database/migrations',
    },
} as ConnectionOptions;
