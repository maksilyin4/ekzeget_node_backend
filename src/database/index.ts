import { Connection, createConnection } from 'typeorm';
import Entities from '../entities';
import EnvironmentDTO from '../core/EnvironmentChecker/EnvironmentDTO';

export default class Database {
    private _connection: Connection;

    constructor(private envs: EnvironmentDTO) {}

    public async init() {
        this._connection = await createConnection({
            type: 'mysql',

            host: this.envs.MYSQL_HOST,
            port: +this.envs.MYSQL_PORT,
            username: this.envs.MYSQL_USER,
            password: this.envs.MYSQL_PASSWORD,
            database: this.envs.MYSQL_DATABASE,
            logging: ['dev'].includes(this.envs.ENV),
            migrations: ['src/database/migrations/*{.ts,.js}'],

            entities: Entities,
            synchronize: false,
            cache: false,

            subscribers: ['src/database/subscribers/*{.ts,.js}'],

            cli: {
                entitiesDir: 'src/entities',
            },
        });

        if (['local', 'dev'].includes(this.envs.ENV)) {
            await this._connection.runMigrations({ transaction: 'each' });
        }
    }

    get connection() {
        return this._connection;
    }
}
