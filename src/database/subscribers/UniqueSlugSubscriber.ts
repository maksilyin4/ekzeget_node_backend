import { EventSubscriber, EntitySubscriberInterface, InsertEvent } from 'typeorm';
import Media from '../../entities/Media';

@EventSubscriber()
export default class UniqueSlugSubscriber implements EntitySubscriberInterface<Media> {
    listenTo() {
        return Media;
    }

    async beforeInsert({ entity }: InsertEvent<Media>) {
        let slug = entity.code,
            attempt = 0;
        while (await Media.findOne({ code: slug })) {
            attempt++;
            slug = `${entity.code}-${attempt}`;
        }
        entity.code = slug;
    }
}
