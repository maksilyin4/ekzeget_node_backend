import { EventSubscriber, EntitySubscriberInterface, UpdateEvent, InsertEvent } from 'typeorm';
import Media from '../../entities/Media';
import { saveFb2Content } from '../../admin-panel/content/resources/processing/helpers/Mediateka/Media/saveFb2Content';

@EventSubscriber()
export class FB2Subscriber implements EntitySubscriberInterface<Media> {
    listenTo() {
        return Media;
    }

    async afterInsert(event: InsertEvent<Media>) {
        const { entity } = event;
        if (entity && entity.type === 'B' && entity.properties) {
            saveFb2Content(entity);
        }
    }

    async afterUpdate(event: UpdateEvent<Media>) {
        const { entity, databaseEntity } = event;
        if (entity && databaseEntity) {
            if (entity.type === 'B' && entity.properties !== databaseEntity.properties) {
                saveFb2Content(<Media>entity);
            }
        }
    }
}
