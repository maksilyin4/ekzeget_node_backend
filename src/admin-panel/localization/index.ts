import _ from 'lodash';
import BookLocale from '../content/resources/all/Bible/Book/localization';
import { Locale } from 'admin-bro';
import BookDescriptionLocale from '../content/resources/all/Bible/BookDescription/localization';
import BookDescriptorLocale from '../content/resources/all/Bible/BookDescriptor/localization';
import ChapterLocale from '../content/resources/all/Bible/Chapter/localization';
import VerseLocale from '../content/resources/all/Bible/Verse/localization';
import MediaLocale from '../content/resources/all/Mediateka/Media/localization';
import MediaAuthorLocale from '../content/resources/all/Mediateka/MediaAuthor/localization';
import MediaCategoryLocale from '../content/resources/all/Mediateka/MediaCategory/localization';
import MediaGenreLocale from '../content/resources/all/Mediateka/MediaGenre/localization';
import MediaAuthorCategoryLocale from '../content/resources/all/Mediateka/MediaAuthorCategory/localization';
import LectureLocate from '../content/resources/all/EverythingAboutBible/Lecture/localization';
import LectureTypeLocate from '../content/resources/all/EverythingAboutBible/LectureType/localization';
import EkzegetPersonLocate from '../content/resources/all/EverythingAboutBible/EkzegetPerson/localization';
import EkzegetTypeLocate from '../content/resources/all/EverythingAboutBible/EkzegetType/localization';
import InterpretationsLocate from '../content/resources/all/EverythingAboutBible/Interpretations/localization';
import PreacherLocate from '../content/resources/all/EverythingAboutBible/Preacher/localization';
import PreachingLocate from '../content/resources/all/EverythingAboutBible/Preaching/localization';
import DictionaryLocate from '../content/resources/all/EverythingAboutBible/Dictionary/localization';
import DictionaryTypeLocate from '../content/resources/all/EverythingAboutBible/DictionaryType/localization';
import OcassionForPreaching from '../content/resources/all/EverythingAboutBible/OcassionForPreaching/localization';
import ReasonForHoliday from '../content/resources/all/EverythingAboutBible/ReasonForHoliday/localization';
import Celebrate from '../content/resources/all/EverythingAboutBible/Celebrate/localization';
import BibleMapsPoints from '../content/resources/all/EverythingAboutBible/BibleMapsPoints/localization';
import BibleGroupLocate from '../content/resources/all/BibleGroup/BibleGroup/localization';
import BibleGroupType from '../content/resources/all/BibleGroup/Type/localization';
import BibleGroupContent from '../content/resources/all/BibleGroup/Content/localization';
import ArticleLocate from '../content/resources/all/News/Article/localization';
import ArticleCategoryLocate from '../content/resources/all/News/ArticleCategory/localization';
import UserLocate from '../content/resources/all/User/User/localization';
import QueezeQuestions from '../content/resources/all/Quiz/QueezeQuestions/localization';
import Meta from '../content/resources/all/Metatags/Meta/localization';
import MetaSection from '../content/resources/all/Metatags/MetaSection/localization';
import Plans from '../content/resources/all/Reading/Plans/localization';
import GospelReading from '../content/resources/all/Reading/Gospel/localization';
import ApostolicReading from '../content/resources/all/Reading/Apostolic/localization';
import PostReading from '../content/resources/all/Reading/Post/localization';
import MorningReading from '../content/resources/all/Reading/Morning/localization';
import Mineja from '../content/resources/all/Reading/Mineja/localization';
import ContentOfPlans from '../content/resources/all/Reading/ContentOfPlans/localization';
import Reading from '../content/resources/all/Reading/Reading/localization';
import Slider from '../content/resources/all/Settings/Slider/localization';
import Config from '../content/resources/all/Settings/Config/localization';
import MetaTemplate from '../content/resources/all/Metatags/MetaTemplate/localization';
import MediaTag from '../content/resources/all/Mediateka/MediaTag/localization';
import TimeLine from '../content/resources/all/TimeLine/localization';
import Typo from '../content/resources/all/Typo/localization';
import TextBlock from '../content/resources/all/TextBlock/localization';

const CommonLocale: Locale = {
    language: 'ru',
    translations: {
        properties: {
            id: 'ID',
            code: 'Код',
            legacy_code: 'Старый код',
            added_by: 'Добавил',
            created_by: 'Создал',
            edited_by: 'Отредактировал',
            active: 'Активен',
            added_at: 'Дата добавления',
            edited_at: 'Дата редактирования',
            updated_at: 'Дата редактирования',
            sort: 'Индекс сорт.',
            title: 'Заголовок',
            short_title: 'Сокращенное название',
            text: 'Текст',
            testament_id: 'Завет',
            book_id: 'Книга (ID)',
            chapter_id: 'Глава (ID)',
            chapter: 'Глава',
            number: 'Номер',
            descriptor_id: 'Автор',
            description: 'Описание',
            genre_id: 'Жанр',
            author_id: 'Автор',
            image_id: 'Изображение',
            image: 'Изображение',
            type: 'Тип',
            type_id: 'Тип',
            category_id: 'Категория',
            parent_id: 'ID родителя',
            status: 'Статус',
            body: 'Содержимое',
            century: 'Век',
            info: 'Инфорация',
            name: 'Имя',
            foto: 'Фото',
            days: 'Дни',
            timese: 'Время',
            phone: 'Телефон',
            email: 'Почта',
            web: 'Сайт',
            country: 'Страна',
            region: 'Регион',
        },
    },
};

const localizations = [
    BookLocale,
    BookDescriptionLocale,
    BookDescriptorLocale,
    ChapterLocale,
    VerseLocale,

    MediaLocale,
    MediaAuthorLocale,
    MediaCategoryLocale,
    MediaGenreLocale,
    MediaAuthorCategoryLocale,
    MediaTag,

    LectureLocate,
    LectureTypeLocate,
    EkzegetPersonLocate,
    EkzegetTypeLocate,
    PreacherLocate,
    PreachingLocate,
    DictionaryLocate,
    DictionaryTypeLocate,
    OcassionForPreaching,
    ReasonForHoliday,
    Celebrate,
    BibleMapsPoints,
    InterpretationsLocate,

    BibleGroupLocate,
    BibleGroupType,
    BibleGroupContent,

    ArticleLocate,
    ArticleCategoryLocate,

    UserLocate,

    QueezeQuestions,

    Meta,
    MetaSection,
    MetaTemplate,

    Plans,
    GospelReading,
    ApostolicReading,
    PostReading,
    MorningReading,
    Mineja,
    ContentOfPlans,
    Reading,

    Slider,
    Config,

    TimeLine,

    Typo,
    TextBlock,
];

export default localizations.reduce((acc, locale) => _.merge(acc, locale), CommonLocale);
