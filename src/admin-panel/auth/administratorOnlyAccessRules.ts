import { RoleList } from './RoleList';
import { ResourceOptions } from 'admin-bro';

export default {
    show: {
        isVisible,
        isAccessible,
    },
    list: {
        isVisible,
        isAccessible,
    },
    search: {
        isVisible,
        isAccessible,
    },
    new: {
        isVisible,
        isAccessible,
    },
    edit: {
        isVisible,
        isAccessible,
    },
} as ResourceOptions['actions'];

function isVisible({ currentAdmin }) {
    return currentAdmin.role === RoleList.administrator;
}

function isAccessible({ currentAdmin }) {
    return currentAdmin.role === RoleList.administrator;
}
