import _ from 'lodash';
import bcrypt from 'bcryptjs';
import { In } from 'typeorm';

import User from '../../entities/User';
import RbacAuthAssignment from '../../entities/RbacAuthAssignment';
import { RoleList } from './RoleList';

export default async (email, password) => {
    try {
        const { id: user_id, username, password_hash } = await User.findOneOrFail({
            where: { email },
        });

        const { item_name: role } = await RbacAuthAssignment.findOneOrFail({
            where: { user_id, item_name: In(_.values(RoleList)) },
        });

        if (await bcrypt.compare(password, password_hash)) {
            return { role, username, id: user_id };
        }
    } catch (e) {
        return false;
    }
};
