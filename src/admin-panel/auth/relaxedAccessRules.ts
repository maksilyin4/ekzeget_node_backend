import { ResourceOptions } from 'admin-bro';

export default {
    show: {
        isVisible: true,
        isAccessible: true,
    },
    list: {
        isVisible: true,
        isAccessible: true,
    },
    search: {
        isVisible: true,
        isAccessible: true,
    },
    new: {
        isVisible: true,
        isAccessible: true,
    },
    edit: {
        isVisible: true,
        isAccessible: true,
    },
} as ResourceOptions['actions'];
