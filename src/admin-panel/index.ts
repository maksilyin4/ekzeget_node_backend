import AdminBroModule from 'admin-bro';
import AdminBroExpress from '@admin-bro/express';
import { Database, Resource } from '@admin-bro/typeorm';
import IAdminPanel from '../core/App/interfaces/IAdminPanel';
import localization from './localization';
import content from './content';
import auth from './auth';

export default class AdminBro implements IAdminPanel {
    private adminBro: AdminBroModule;
    private adminBroRouter;

    constructor() {
        this.equipAdminBro();
        this.createAdminBroRouter();
    }

    private equipAdminBro() {
        AdminBroModule.registerAdapter({ Database, Resource });

        this.adminBro = new AdminBroModule({
            assets: { styles: ['/public/css/index.css'] },
            resources: content,
            locale: localization,
            rootPath: '/admin',
            branding: {
                companyName: 'Ekzeget Admin Panel',
                softwareBrothers: false,
                logo: false,
            },
        });
    }

    private createAdminBroRouter() {
        this.adminBroRouter = AdminBroExpress.buildAuthenticatedRouter(this.adminBro, {
                authenticate: auth,
                cookiePassword: 'gikjlefrucnk wrkjfeh uierhf8 9ureht oiw4u5irhtgfio d',
            },
            undefined,
            undefined,
            { maxFileSize: 1024 * 1024 * 1200 }
        );
    }

    get plugin() {
        return this.adminBroRouter;
    }

    get pluginPath() {
        return this.adminBro.options.rootPath;
    }
}
