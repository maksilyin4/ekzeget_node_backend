import { PropertyType } from 'admin-bro/src/backend/adapters/property/base-property';
import AdminBro from 'admin-bro';
import path from 'path';

export default () => ({
    type: 'richtext' as PropertyType,
    components: {
        edit: AdminBro.bundle(path.resolve(__dirname, '../components/RichText/index.tsx')),
    },
    props: {
        tinyMCE: {

        },
    },
});
