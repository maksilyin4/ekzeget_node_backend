import { Action, RecordActionResponse } from 'admin-bro';
import { ServerError } from '../../../../../libs/ErrorHandler';
import exceptionMessage from '../../../../../const/strings/adminBro/adminBroException';
import deleteRelatedFields from '../helpers/deleteRelatedFields';

const del: Partial<Action<RecordActionResponse>> = {
    before: async request => {
        try {
            await deleteRelatedFields(request);
        } catch (err) {
            throw new ServerError(
                exceptionMessage.DELETE_CUSTOM_FIELD_ERROR + ' ' + err.message,
                500
            );
        }
        return request;
    },
};

export default del;
