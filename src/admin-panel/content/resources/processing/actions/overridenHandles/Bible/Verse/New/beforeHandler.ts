import Chapters from '../../../../../../../../../entities/Chapters';

const verseNewBeforeHandler = async payload => {
    if (payload.book_id && payload.number) {
        const chapterId = await Chapters.findOne({
            where: {
                book_id: payload.book_id,
                number: payload.number,
            },
        });
        return { chapter_id: chapterId.id };
    }
    return undefined;
};

export default verseNewBeforeHandler;
