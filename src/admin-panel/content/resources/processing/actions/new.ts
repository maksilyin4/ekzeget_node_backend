import { Action, RecordActionResponse, ValidationError } from 'admin-bro';

import { ILoadedContext } from './recordProseccing/AdminImageManager/types/ILoadedContext';
import adminImageManager from './recordProseccing/AdminImageManager';
import adminDateManager from './recordProseccing/AdminDateManager';
import adminActorManager from './recordProseccing/AdminActorManager';
import recordWriter from '../../processing/actions/recordProseccing/RecordWriter';
import codeGenerator from './recordProseccing/CodeGenerator';
import saveCustomFields from '../helpers/saveCustomFields';
import exceptionMessage from '../../../../../const/strings/adminBro/adminBroException';
import verseNewBeforeHandler from './overridenHandles/Bible/Verse/New/beforeHandler';
import UrlProcessor from './recordProseccing/UrlProcessor';
import importQuestionToQuiz from './recordProseccing/importQuestionToQuiz';
import TextFormatter from '../../../../../libs/TextFormatter';

const add: Partial<Action<RecordActionResponse>> = {
    before: async (request, context) => {
        const { payload } = request;

        context.images = adminImageManager.getImagesBeforeAction(
            payload,
            adminImageManager.findKeyForImageContent(context.resource._decorated.options)
        );

        request.payload = {
            ...payload,
            ...recordWriter.setDefaultValuesToPayload(payload),
            ...adminDateManager.getCreateDateObject(),
            ...adminActorManager.getCreateActorObject(context.currentAdmin),
            ...codeGenerator.generate(payload),
            ...UrlProcessor.getValidUrl(request),
            ...(await verseNewBeforeHandler(payload)),
        };

        if (request.params.resourceId === 'Interpretation') {
            request.payload['parsed_text'] = await new TextFormatter(
                request.payload['comment']
            ).format();
        }
        return request;
    },

    after: async (response, request, context) => {
        const { resource, record, images, h, currentAdmin } = context as ILoadedContext;

        if (request.payload.hasOwnProperty('fileForImportQuizQuestions')) {
            record.errors = {};
            const result = await importQuestionToQuiz(
                request.payload['fileForImportQuizQuestions'].path,
                context
            );

            return {
                redirectUrl: h.resourceUrl({
                    resourceId: resource._decorated?.id() || resource.id(),
                }),
                notice: result,
                record: record.toJSON(currentAdmin),
            };
        }

        if (record.isValid()) {
            try {
                await saveCustomFields(assignRecordIdToRequest(request, record.id()));
            } catch (err) {
                // TODO implement centralized custom fields validation
                // TODO validate in BEFORE hook prior record creation
                if (err instanceof ValidationError) {
                    await resource.delete(record.id());
                    err.message = err.baseError.message;
                    response.redirectUrl = h.resourceActionUrl({
                        resourceId: resource.id(),
                        actionName: 'new',
                    });
                } else {
                    response.redirectUrl = h.recordActionUrl({
                        resourceId: resource.id(),
                        recordId: record.id(),
                        actionName: 'edit',
                    });
                }
                response.notice.type = 'error';
                response.notice.message =
                    exceptionMessage.SAVE_CUSTOM_FIELD_ERROR + ': ' + err.message;

                return response;
            }
        } else {
            response.notice.message += ' ' + Object.values(record.errors)[0].message;
            return response;
        }

        await adminImageManager.storeImagesAfterAction(resource, +response.record.id, images);

        response.redirectUrl = h.recordActionUrl({
            recordId: record.id(),
            actionName: 'show',
            resourceId: resource.id(),
        });

        return response;
    },
};

export default add;

function assignRecordIdToRequest(request, recordId) {
    const reqClone = { ...request };
    reqClone.params.recordId = recordId;
    reqClone.payload.id = recordId;

    return reqClone;
}
