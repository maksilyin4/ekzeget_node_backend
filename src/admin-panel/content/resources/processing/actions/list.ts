import { Action, ActionContext, ActionRequest, RecordActionResponse } from 'admin-bro';
import recordPresenter from './recordProseccing/RecordPresenter';
import customFieldsFilter from './recordProseccing/customFieldsFilter';

const list: Partial<Action<RecordActionResponse>> = {
    before: async (request: ActionRequest, context: ActionContext): Promise<ActionRequest> => {
        request.query.perPage = request.query.perPage || 20;
        return customFieldsFilter.excludeFromRequest(request);
    },

    after: async (
        response: RecordActionResponse,
        req: ActionRequest,
        context: ActionContext
    ): Promise<RecordActionResponse> => {
        const modifyResponse = await customFieldsFilter.filter(response, req);

        const modifyRecordFunc = async (record, listProperties) => ({
            ...record,
            params: await recordPresenter.main(record.params, listProperties),
        });

        const modifiedRecords = await Promise.all(
            modifyResponse.records.map(record => {
                return modifyRecordFunc(record, context.resource._decorated.options.listProperties);
            })
        );

        return { ...modifyResponse, records: modifiedRecords };
    },
};

export default list;
