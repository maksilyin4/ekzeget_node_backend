import { Action, RecordActionResponse } from 'admin-bro';
import recordPresenter from './recordProseccing/RecordPresenter';

const show: Partial<Action<RecordActionResponse>> = {
    after: async (response: RecordActionResponse, req, context): Promise<RecordActionResponse> => {
        const modifiedRecord = {
            ...response.record,
            params: await recordPresenter.main(
                response.record.params,
                context.resource._decorated.options.showProperties
            ),
        };

        return { ...response, record: modifiedRecord };
    },
};

export default show;
