import { Action, ActionRequest, RecordActionResponse, ValidationError } from 'admin-bro';
import adminImageManager from './recordProseccing/AdminImageManager';
import { ILoadedContext } from './recordProseccing/AdminImageManager/types/ILoadedContext';
import recordPresenter from './recordProseccing/RecordPresenter';
import saveCustomFields from '../helpers/saveCustomFields';
import exceptionMessage from '../../../../../const/strings/adminBro/adminBroException';
import getModifiedPayload from './recordProseccing/modifyPayload';

const edit: Partial<Action<RecordActionResponse>> = {
    /**
     * При выборе Edit происходит GET запрос, параметры из листа передаются в
     * context.record.params
     * После редактирования полей и нажатии кнопки Edit срабатывает POST запрос
     * (в исходниках adminBro хэндлер edit проверяет какой тип запроса пришел и
     * понимает что ему надо делать- вывести на экран форму редактирования или занести данные в базу)
     * Новые данные приходят в
     * request.payload
     * @param request
     * @param context
     */
    before: async (request: ActionRequest, context) => {
        const { payload } = request;

        context.images = adminImageManager.getImagesBeforeAction(
            payload,
            adminImageManager.findKeyForImageContent(context.resource._decorated.options)
        );

        if (request.method === 'post') {
            request.payload = {
                ...(await getModifiedPayload(request, context)),
            };
        }

        return request;
    },

    /**
     * Параметры, приходящие после хэндлера response.record.params
     * @param response
     * @param request
     * @param context
     */
    after: async (response, request, context) => {
        const { resource, record, images, h } = context as ILoadedContext;

        if (request.method === 'post') {
            if (record.isValid()) {
                try {
                    await saveCustomFields(request);
                } catch (err) {
                    // TODO implement centralized custom fields validation
                    if (err instanceof ValidationError) {
                        err.message = err.baseError.message;
                    }
                    response.notice.type = 'error';
                    response.notice.message =
                        exceptionMessage.SAVE_CUSTOM_FIELD_ERROR + ': ' + err.message;
                    response.redirectUrl = h.recordActionUrl({
                        resourceId: resource.id(),
                        recordId: record.id(),
                        actionName: 'edit',
                    });
                    return response;
                }
            } else {
                response.notice.message += ' ' + Object.values(record.errors)[0].message;
                response.redirectUrl = h.recordActionUrl({
                    resourceId: resource.id(),
                    recordId: record.id(),
                    actionName: 'edit',
                });
                return response;
            }
            await adminImageManager.storeImagesAfterAction(resource, record.params.id, images);
        }

        response.record.params = await recordPresenter.main(
            response.record.params,
            context.resource._decorated.options.editProperties
        );

        response.redirectUrl = h.recordActionUrl({
            recordId: record.id(),
            actionName: 'show',
            resourceId: resource.id(),
        });

        return response;
    },
};

export default edit;
