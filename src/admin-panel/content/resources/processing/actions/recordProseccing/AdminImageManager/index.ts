import IContextImage from './types/IContextImage';
import storingWithPhpSerialize from './func/storingWithPhpSerialize';
import storingWithFileItemStorage from './func/storingWithFileItemStorage';
import {ResourceOptions} from "admin-bro";

class AdminImageManager {
    public getImagesBeforeAction(actionPayload, fieldName) {
        if (!actionPayload.hasOwnProperty('imageUploader')) return [];

        const images: IContextImage[] = [
            {
                fieldName,
                file: actionPayload['imageUploader'],
            },
        ];

        return images;
    }

    public async storeImagesAfterAction(
        entityForStoring,
        entityRecordId: number,
        images: IContextImage[]
    ) {
        if (!images || !images.length) return;

        const tableName = entityForStoring.model.getRepository().metadata.tableName;

        const storingFn = (() => {
            switch (tableName) {
                case 'bible_maps_points':
                    return storingWithPhpSerialize(entityForStoring, entityRecordId);
                default:
                    return storingWithFileItemStorage(entityForStoring, entityRecordId);
            }
        })();

        await Promise.all(images.map(storingFn));
    }

    public findKeyForImageContent(resourceOptions: ResourceOptions) {
        return resourceOptions.properties.image_field?.props.value;
    }

    public clearAxillaryFields = payload => {
        delete payload['imageUploader'];
        delete payload['image_id'];
        delete payload['image'];
    };
}

export default new AdminImageManager();
