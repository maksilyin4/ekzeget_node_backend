import IContextImage from '../types/IContextImage';
import FtpManager from '../../../../../../../../managers/FtpManager';
import FileStorageItem from '../../../../../../../../entities/FileStorageItem';
import getUnixSec from '../../../../../../../../libs/helpers/getUnixSec';

export default (entityForStoring, entityRecordId: number) => async ({
    file,
    fieldName,
}: IContextImage) => {
    await new FtpManager().uploadImageToStore(file, `/storage/web/source/777/`);

    const fileStorageItem = await FileStorageItem.create({
        name: file.name,
        size: file.size,
        base_url: '/storage/web/source',
        path: '777/' + file.name,
        component: 'fileStorage',
        created_at: getUnixSec(),
    });

    await fileStorageItem.save();

    await entityForStoring.update(entityRecordId, {
        [fieldName]: fileStorageItem.id,
        updated_at: getUnixSec(),
    });
};
