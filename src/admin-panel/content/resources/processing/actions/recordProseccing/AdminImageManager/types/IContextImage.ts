import IFtpFile from '../../../../../../../../managers/FtpManager/types/IFtpFile';

export default interface IContextImage {
    fieldName: string;
    file: IFtpFile;
}
