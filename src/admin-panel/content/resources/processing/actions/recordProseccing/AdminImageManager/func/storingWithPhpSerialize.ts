import IContextImage from '../types/IContextImage';
import FtpManager from '../../../../../../../../managers/FtpManager';
import getUnixSec from '../../../../../../../../libs/helpers/getUnixSec';
import { serialize as PHPSerialize } from 'php-serialize';

export default (entityForStoring, entityRecordId: number) => async ({
    file,
    fieldName,
}: IContextImage) => {
    await new FtpManager().uploadImageToStore(file, `/storage/web/source/777/`);
    await entityForStoring.update(entityRecordId, {
        [fieldName]: PHPSerialize({
            name: file.name,
            size: file.size,
            type: 'image/jpeg',
            base_url: '/storage/web/source',
            path: '777/' + file.name,
            order: '',
        }),
        updated_at: getUnixSec(),
    });
};
