import { ActionContext } from 'admin-bro';
import IContextImage from './IContextImage';

export type ILoadedContext = ActionContext & { images?: IContextImage[] };
