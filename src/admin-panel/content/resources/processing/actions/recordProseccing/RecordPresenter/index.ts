import unixSecTimeToIso from '../../../../../../../libs/helpers/unixSecTimeToIso';
import FileStorageItem from '../../../../../../../entities/FileStorageItem';
import Book from '../../../../../../../entities/Book';
import Verse from '../../../../../../../entities/Verse';
import QueezeAnswers from '../../../../../../../entities/QueezeAnswers';
import { IResourceEditQuizQuestion } from '../../../../../types/IResourceEditQuizQuestion';
import getResourceForEdit from '../../../helpers/Quiz/getResourceForEdit';
import { IRelationVerseForCustomComponents } from '../../../../../types/IRelationVerseForCustomComponents';
import getVerseForBibleMapsPoints from '../../../helpers/CustomRelationVerseComponent/getVerseForBibleMapsPoints';
import getVerseForBibleVerse from '../../../helpers/CustomRelationVerseComponent/getVerseForBibleVerse';
import VerseTranslate from '../../../../../../../entities/VerseTranslate';
import DictionaryVerse from '../../../../../../../entities/DictionaryVerse';
import EnvironmentManager from '../../../../../../../managers/EnvironmentManager';
import PHPUnserialize from 'php-unserialize';
import MediaBible from '../../../../../../../entities/MediaBible';
import convertRelationForMediaToStandartView from '../convertRelationForMediaToStandartView';
import { IRelationForMedia } from '../../../../../types/IRelationForMedia';
import MediaRelated from '../../../../../../../entities/MediaRelated';
import MediaTag from '../../../../../../../entities/MediaTag';
import { IRelationRecordInfo } from '../../../../../types/IRelatedRecordInfo';
import { IClientAnswer } from '../../../../../../../core/App/interfaces/IQuiz';
import getVerseForInterpretations from '../../../helpers/CustomRelationVerseComponent/getVerseForInterpretations';
import MediaCategory from '../../../../../../../entities/MediaCategory';
import Media from '../../../../../../../entities/Media';
import QueezeQuestions from "../../../../../../../entities/QueezeQuestions";

export class RecordPresenter {
    async main(recordParams, resourceProperties?: string[]) {
        const {
            id,
            published_at,
            updated_at,
            created_at,
            createdAt,
            added_at,
            logged_at,
            image,
            image_id,
            edited_at,
            verseId,
            verse_id,
        } = recordParams;

        return {
            ...recordParams,
            published_at: this.defineDate(published_at),
            created_at: this.defineDate(created_at),
            createdAt: this.defineDate(createdAt),
            added_at: this.defineDate(added_at),
            logged_at: this.defineDate(logged_at),
            edited_at: this.defineDate(edited_at),
            updated_at: this.defineDate(updated_at),
            image: await RecordPresenter.getImageUrl(image),
            image_id: await RecordPresenter.getImageUrl(image_id),

            /**
             * Поля для раздела "Викторина"
             */
            bookForQuestionList: await RecordPresenter.defineBookForQuestionList(
                verseId || verse_id,
                resourceProperties
            ),
            verseForQuestionList: await RecordPresenter.defineVerseForQuestionList(
                verseId || verse_id,
                resourceProperties
            ),
            answersForQuestionShowAndEdit: await RecordPresenter.defineAnswersForQuestionShowAndEdit(
                id,
                resourceProperties
            ),
            bookWithChapterAndVerse: await RecordPresenter.defineBookWithChapterAndVerse(
                verseId || verse_id,
                resourceProperties
            ),
            questionInterpretations: await RecordPresenter.defineQuestionInterpretations(
                id,
                resourceProperties
            ),

            /**
             * Поле для кастомного компонента "Связанные стихи"
             * В админке есть несколько мест в которых используется кастомный компонент с выбором связанных стихов.
             * Компонент унифицирован и использует одно поле, которое заполняется в зависимости от ресурса, в котором
             * компонент был вызван.
             */
            relationVerseForCustomComponents: await RecordPresenter.defineRelationVerseForCustomComponents(
                id,
                resourceProperties
            ),

            /**
             * Поля для раздела "Библия -> Стихи"
             */
            customRelationTranslations: await RecordPresenter.defineRelationTranslations(id, resourceProperties),
            customRelationDictionaryArticles: await RecordPresenter.defineCustomRelationDictionaryArticles(id),

            /**
             * Поля для раздела "Медиатека -> Медиа"
             */
            customRelationForMedia: await RecordPresenter.defineRelationForMedia(id, resourceProperties),
            informationAboutRelatedRecords: await RecordPresenter.defineInformationAboutRelatedRecords(
                id,
                resourceProperties
            ),
            customRelatedTags: await RecordPresenter.defineCustomRelatedTags(id, resourceProperties),

            // Custom categories
            customCat: await this.defineCustomCategories(id, resourceProperties),
        };
    }

    private static async defineCustomRelatedTags(
        id: number | undefined,
        resourceProperties: string[]
    ): Promise<Pick<MediaTag, 'id' | 'title'>[] | undefined> {
        if (resourceProperties.includes('customRelatedTags') && id) {
            return MediaTag.getRelationTagsByMediaId(id);
        }
        return undefined;
    }

    private static async defineInformationAboutRelatedRecords(
        id: number | undefined,
        resourceProperties: string[]
    ): Promise<IRelationRecordInfo[] | undefined> {
        if (id && resourceProperties.includes('customLinkToOtherRecords')) {
            return MediaRelated.getInfoAboutRelatedRecords(id);
        }
        return undefined;
    }

    private static async defineRelationForMedia(
        id: number | undefined,
        resourceProperties: string[]
    ): Promise<IRelationForMedia[] | undefined> {
        if (id && resourceProperties.includes('customRelationForMedia')) {
            const relationsIds = await MediaBible.find({
                where: { media_id: id },
            });
            return convertRelationForMediaToStandartView(relationsIds);
        }
        return undefined;
    }

    private defineDate = date => (date ? unixSecTimeToIso(date) : 'no date');

    private static async defineCustomRelationDictionaryArticles(id: number) {
        return DictionaryVerse.getRelationDictionaryArticlesForAdminBroCustomComponents(id);
    }

    private static async defineRelationTranslations(
        id: number,
        resourceProperties: string[]
    ): Promise<VerseTranslate[] | null> {
        if (resourceProperties.includes('customRelationVerseForBibleVerse')) {
            const relationTranslations = await VerseTranslate.find({
                select: ['code', 'text'],
                where: { verse_id: id },
            });
            return relationTranslations || [];
        }
        return null;
    }

    private static async defineRelationVerseForCustomComponents(
        id: number,
        resourceProperties: string[]
    ): Promise<IRelationVerseForCustomComponents[]> | undefined {
        if (resourceProperties === undefined) {
            return undefined;
        }

        if (resourceProperties.includes('customRelationVerseForBibleMapsPoint')) {
            return getVerseForBibleMapsPoints(id);
        }

        if (resourceProperties.includes('customRelationVerseForBibleVerse')) {
            return getVerseForBibleVerse(id);
        }

        if (resourceProperties.includes('customRelationVerseForInterpretation')) {
            return getVerseForInterpretations(id);
        }

        return undefined;
    }

    static createImageLink(base_url: string, path: string) {
        return `${EnvironmentManager.envs.STORAGE_ASSETS_CDN}${EnvironmentManager.envs.STORAGE_ASSETS_DIR}${base_url}/${path}`;
    }

    static async getImageUrl(id: number | string): Promise<string | void> {
        if (!id) return;

        const fileStorageItem = await FileStorageItem.findOne(id);
        if (fileStorageItem) {
            const { base_url, path } = fileStorageItem;
            return RecordPresenter.createImageLink(base_url, path);
        }

        let unSerializedImage;
        // Не доверяем данным в базе, хз как из сериализовывали
        try {
            unSerializedImage = PHPUnserialize.unserialize(id);
        } catch (e) {
            return;
        }

        if (unSerializedImage) {
            const { base_url, path } = unSerializedImage;
            return RecordPresenter.createImageLink(base_url, path);
        }
    }

    private static async defineBookForQuestionList(
        verseId,
        resourceProperties: string[]
    ): Promise<string | undefined> {
        if (resourceProperties === undefined || !resourceProperties.includes('bookForQuestionList')) {
            return undefined;
        }
        const book: Book = await Book.getBookByVerseId(verseId);
        return book.title;
    }

    private static async defineVerseForQuestionList(
        verseId,
        resourceProperties: string[]
    ): Promise<number | undefined> {
        if (resourceProperties === undefined || !resourceProperties.includes('verseForQuestionList')) {
            return undefined;
        }
        const verse = await Verse.findOne(verseId);
        return verse.chapter;
    }

    private static async defineAnswersForQuestionShowAndEdit(
        questionId,
        resourceProperties: string[]
    ): Promise<IClientAnswer[] | undefined> {
        if (
            resourceProperties === undefined ||
            !resourceProperties.includes('answersForQuestionShowAndEdit')
        ) {
            return undefined;
        }

        return QueezeAnswers.getAnswers(questionId);
    }

    private static async defineBookWithChapterAndVerse(
        verseId: number,
        resourceProperties: string[]
    ): Promise<IResourceEditQuizQuestion | undefined> {
        if (!resourceProperties) return;
        if (
            resourceProperties.includes('bookWithChapterAndVerse') ||
            resourceProperties.includes('answersForQuestionShowAndEdit')
        ) {
            const resourceForQuizEdit = await getResourceForEdit(verseId);
            return {
                relationBook: resourceForQuizEdit.relationBook,
                relationVerse: resourceForQuizEdit.relationVerse,
                allVerseForChapter: resourceForQuizEdit.allVerseForChapter,
            };
        }
        return undefined;
    }

    private static async defineQuestionInterpretations(
        questionId: number,
        resourceProperties: string[]
    ) {
        if (resourceProperties === undefined || !resourceProperties.includes('interpretations')) {
            return undefined;
        }

        return QueezeQuestions.getInterpretationsById(questionId);
    }

    private async defineCustomCategories(
        id: number | undefined,
        resourceProperties: string[] = []
    ): Promise<(Pick<MediaCategory, 'id' | 'title'> & { selected: boolean })[] | undefined> {
        if (!resourceProperties.includes('customCat')) {
            return;
        }

        const categories = await MediaCategory.find();
        const presentedCategories = categories.map(({ id, title }) => ({
            id,
            title,
            selected: false,
        }));

        const { mediaCategories } = await Media.findOne({
            where: { id },
            loadEagerRelations: true,
            relations: ['mediaCategories'],
        });

        const selectedCategories = mediaCategories.map(({ id }: MediaCategory) => id);

        return presentedCategories.map(category => ({
            ...category,
            selected: selectedCategories.includes(category.id),
        }));
    }
}

export default new RecordPresenter();
