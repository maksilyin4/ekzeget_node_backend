import QueezeAnswers from '../../../../../../../entities/QueezeAnswers';

/**
 * Преобразует массив вопросов в HTML код для вывода в компоненте "Как есть"
 * @param answers
 */
export default function answersToHtml(answers: QueezeAnswers[]) {
    const htmlForAnswers = answers.map(item => {
        return item['is_correct']
            ? `<li><b style="color: green; font-weight: bold">${item.text}</b></li>`
            : `<li>${item.text}</li>`;
    });

    htmlForAnswers.unshift(
        '<h1 style="font-size: 26px; margin-bottom: 20px"> Ответы</h1><ul style="list-style-type: circle; margin-left: 20px">'
    );
    htmlForAnswers.push('</ul>');
    return htmlForAnswers.join('');
}
