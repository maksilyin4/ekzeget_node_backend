export const context = {
    resource: {
        id: () => {
            return 1;
        },
    },
    translateMessage: (key, id) => {
        return key;
    },
};

export const manager = {
    save: async () => {
        return { id: 1 };
    },
};

export const createQueryRunner = {
    connect() {},
    startTransaction() {},
    manager: manager,
    async commitTransaction() {},

    async rollbackTransaction() {},
};

export const insertedParams = [
    [
        'QueezeQuestions',
        {
            type: 'T',
            verseId: 1,
            title: 'Иоанн Предтеча говорил людям о Христе: «…я крестил вас водою, а Он будет...».',
            description:
                '«...я крестил вас водою, а Он будет крестить вас Духом Святым». Мк. 1:8 (Синодальный перевод).',
            comment: '-',
            active: 'y',
            code: 'ioann-predtecha-govoril-lyudyam-o-hriste-ya-krestil-vas-vodoyu,-a-on-budet',
        },
    ],
    [
        'QueezeAnswers',
        [
            {
                questionId: 1,
                isCorrect: false,
                text: '«...судить вас судом праведным».',
            },
            {
                questionId: 1,
                isCorrect: true,
                text: '«...крестить вас Духом Святым».',
            },
            {
                questionId: 1,
                isCorrect: false,
                text: '«...учить Израиль закону и правде».',
            },
            {
                questionId: 1,
                isCorrect: false,
                text: '«...проповедовать слово Божие».',
            },
        ],
    ],
    [
        'QueezeQuestions',
        {
            type: 'T',
            verseId: 1,
            title: 'Где Господь наш Иисус Христос принял крещение от Иоанна Предтечи?',
            description:
                '«И было в те дни, пришел Иисус из Назарета Галилейского и крестился от Иоанна в Иордане». Мк. 1:9 (Синодальный перевод).',
            comment: '-',
            active: 'y',
            code: 'gde-gospod-nash-iisus-hristos-prinyal-kreschenie-ot-ioanna-predtechi',
        },
    ],
    [
        'QueezeAnswers',
        [
            { questionId: 1, isCorrect: true, text: 'В реке Иордан.' },
            { questionId: 1, isCorrect: false, text: 'В Галилейском море.' },
            { questionId: 1, isCorrect: false, text: 'В Красном море.' },
            { questionId: 1, isCorrect: false, text: 'В потоке Кедрон.' },
        ],
    ],
    [
        'QueezeQuestions',
        {
            type: 'T',
            verseId: 1,
            title: 'После какого события Христос вышел на проповедь?',
            description:
                '«После же того, как предан был Иоанн, пришел Иисус в Галилею, проповедуя Евангелие Царствия Божия». Мк. 1:14 (Синодальный перевод).',
            comment: '-',
            active: 'y',
            code: 'posle-kakogo-sobytiya-hristos-vyshel-na-propoved',
        },
    ],
    [
        'QueezeAnswers',
        [
            {
                questionId: 1,
                isCorrect: false,
                text: 'После Его беседы в Иерусалимском храме с учителями.',
            },
            {
                questionId: 1,
                isCorrect: true,
                text: 'После того, как Иоанн Креститель был брошен в тюрьму.',
            },
            {
                questionId: 1,
                isCorrect: false,
                text: 'После того, как Иоанн Креститель был казнен по приказу Ирода.',
            },
            {
                questionId: 1,
                isCorrect: false,
                text: 'После чуда на браке в Кане Галилейской.',
            },
        ],
    ],
    [
        'QueezeQuestions',
        {
            type: 'T',
            verseId: 1,
            title:
                'Выйдя на проповедь после Своего крещения, Иисус Христос говорил: «...приблизилось Царствие Божие…».',
            description:
                '«…и говоря, что исполнилось время и приблизилось Царствие Божие: покайтесь и веруйте в Евангелие». Мк. 1:15 (Синодальный перевод).',
            comment: '-',
            active: 'y',
            code:
                'vyydya-na-propoved-posle-svoego-krescheniya,-iisus-hristos-govoril-priblizilos-carstvie-bozhie',
        },
    ],
    [
        'QueezeAnswers',
        [
            { questionId: 1, isCorrect: false, text: '«...внемлите закону».' },
            {
                questionId: 1,
                isCorrect: true,
                text: '«...покайтесь и веруйте в Евангелие».',
            },
            {
                questionId: 1,
                isCorrect: false,
                text: '«...очистите себя крещением Иоанновым».',
            },
            {
                questionId: 1,
                isCorrect: false,
                text: '«...покайтесь и обратитесь, чтобы загладились грехи ваши».',
            },
        ],
    ],
    [
        'QueezeQuestions',
        {
            type: 'T',
            verseId: 1,
            title: 'После Андрея и Симона Иисус Христос призвал еще двух братьев. Это были:',
            description:
                '«И, пройдя оттуда немного, Он увидел Иакова Зеведеева и Иоанна, брата его, также в лодке починивающих сети». Мк. 1:19-20 (Синодальный перевод).',
            comment: '-',
            active: 'y',
            code: 'posle-andreya-i-simona-iisus-hristos-prizval-esche-dvuh-bratev-eto-byli',
        },
    ],
    [
        'QueezeAnswers',
        [
            { questionId: 1, isCorrect: false, text: 'Иаков и Марк.' },
            { questionId: 1, isCorrect: false, text: 'Марк и Лука.' },
            { questionId: 1, isCorrect: true, text: 'Иаков и Иоанн.' },
            { questionId: 1, isCorrect: false, text: 'Иоанн и Марк.' },
        ],
    ],
];
