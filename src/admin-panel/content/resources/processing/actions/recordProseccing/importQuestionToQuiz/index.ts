import { NoticeMessage, ActionContext } from 'admin-bro';
import { ILoadedContext } from '../AdminImageManager/types/ILoadedContext';
import XLSX from 'xlsx';
import Verse from '../../../../../../../entities/Verse';
import QueezeQuestions from '../../../../../../../entities/QueezeQuestions';
import { generateSlug } from '../../../../../../../libs/Rus2Translit/generateSlug';
import QueezeAnswers from '../../../../../../../entities/QueezeAnswers';
import { getConnection } from 'typeorm';

type parsedQuestion = {
    verse: string;
    answer: string;
    type: number;
    question: string;
    comment: string;
};

const importQuestionToQuiz = async (pathToFile, context: ActionContext): Promise<NoticeMessage> => {
    const { resource, translateMessage } = context as ILoadedContext;
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
        const parsedXML = XLSX.readFile(pathToFile);
        const parsedJson: parsedQuestion[] = XLSX.utils.sheet_to_json(parsedXML.Sheets[parsedXML.SheetNames[0]]);
        for (const question of parsedJson) {
            const verses: Verse[] = await Verse.getByShortLink(question['verse']);
            if (!verses.length) {
                throw new Error(`Unable to find verses by link: ${question['verse']}`);
            }

            const questionId = (
                await queryRunner.manager.save(
                    QueezeQuestions,
                    generateQueezeQuestionsFields(question, verses[0].id)
                )
            ).id;

            const OptionQuery = answerInsertBuilder(question, questionId);
            await queryRunner.manager.save(QueezeAnswers, [
                new OptionQuery(question['option_a'], 1),
                new OptionQuery(question['option_b'], 2),
                new OptionQuery(question['option_c'], 3),
                new OptionQuery(question['option_d'], 4),
            ]);
        }
    } catch (e) {
        await queryRunner.rollbackTransaction();

        return {
            message: translateMessage('Ошибка импорта', resource.id()),
            type: 'error',
        };
    }
    await queryRunner.commitTransaction();

    return {
        message: translateMessage('Вопросы импортированы', resource.id()),
        type: 'success',
    };
};

export default importQuestionToQuiz;

function answerInsertBuilder(question: parsedQuestion, questionId: number) {
    return function(text: string, optionNum: number) {
        this.question_id = questionId;
        this.is_correct = Number(question['answer']) === optionNum;
        this.text = text;
    };
}
function generateQueezeQuestionsFields(question: parsedQuestion, verseId: number) {
    return {
        type: question['type'] === 1 ? 'T' : 'I',
        verseId: verseId,
        title: question['question'],
        description: question['comment'],
        active: 'y',
        code: generateSlug(question['question'])
            .replace(/[…»«.:?']/gi, '')
            .slice(0, 254),
    };
}
