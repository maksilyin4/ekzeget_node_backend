import typeorm, { Connection } from 'typeorm';
import Verse from '../../../../../../../entities/Verse';
import importQuestionToQuiz from './index';
import { ActionContext } from 'admin-bro';
import { context, createQueryRunner, insertedParams, manager } from './testHelper';

const getByShortLinkSpy = jest.spyOn(Verse, 'getByShortLink');
const commitTransactionSpy = jest.spyOn(createQueryRunner, 'commitTransaction');
const rollbackTransactionSpy = jest.spyOn(createQueryRunner, 'rollbackTransaction');
const saveSpy = jest.spyOn(manager, 'save');

jest.mock('../../../../../../../entities/QueezeQuestions', () => {
    return 'QueezeQuestions';
});
jest.mock('../../../../../../../entities/QueezeAnswers', () => {
    return 'QueezeAnswers';
});

jest.mock('typeorm', () => ({
    getConnection: jest.fn().mockReturnValue(({
        createQueryRunner: () => {
            return createQueryRunner;
        },
    } as unknown) as Connection),
    BaseEntity: null,
    PrimaryGeneratedColumn: () => {},
    Column: () => {},
    Entity: () => {},
    OneToMany: () => {},
    ManyToOne: () => {},
    JoinColumn: () => {},
    Index: () => {},
    ManyToMany: () => {},
    JoinTable: () => {},
    OneToOne: () => {},
    PrimaryColumn: () => {},
}));

afterAll(() => {
    jest.restoreAllMocks();
});

describe('import', () => {
    test('xlsx without mistake', async () => {
        getByShortLinkSpy.mockResolvedValue([
            {
                id: 1,
            },
        ] as Verse[]);

        const response = await importQuestionToQuiz(
            'tests/files/test_import_good.xlsx',
            (context as unknown) as ActionContext
        );

        expect(response.type).toBe('success');
        expect(response.message).toBe('Вопросы импортированы');
        expect(saveSpy.mock.calls).toEqual(insertedParams);
        expect(commitTransactionSpy).toBeCalled();
        expect(rollbackTransactionSpy).not.toBeCalled();
    });

    test('xlsx with mistake', async () => {
        getByShortLinkSpy.mockImplementation(parseString => {
            throw new Error(`Unable to parse ${parseString}`);
        });

        commitTransactionSpy.mockClear();

        const response = await importQuestionToQuiz(
            'tests/files/test_import_bad.xlsx',
            (context as unknown) as ActionContext
        );

        expect(response.type).toBe('error');
        expect(response.message).toBe('Ошибка импорта');
        expect(commitTransactionSpy).not.toBeCalled();
        expect(rollbackTransactionSpy).toBeCalled();
    });
});
