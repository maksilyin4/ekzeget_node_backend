import { ActionRequest, RecordActionResponse } from 'admin-bro';
import cacheManager from '../../../../../../../managers/CacheManager';
import { unflatten } from 'flat';
import { IFilters } from '../../../../../types/IFilters';
import {
    getEntity,
    getFilterQuery,
    getPopulated,
    getSelectedPage,
    presenceOfCustomFields,
} from './helpers';
import QueezeQuestions from '../../../../../../../entities/QueezeQuestions';
import { ServerError } from '../../../../../../../libs/ErrorHandler';

class CustomFieldsFilter {
    private resourcesWithCustomFields = ['QueezeQuestions', 'Interpretation'];
    private filterableCustomFields = [
        //QueezeQuestions
        'bookForQuestionList',
        'verseForQuestionList',
        //Interpretation
        'interpretationVerseBook',
        'interpretationVerseChapter',
        'interpretationVerseNumber',
    ];

    public async excludeFromRequest(request: ActionRequest) {
        if (this.resourcesWithCustomFields.includes(request.params.resourceId)) {
            const filterParams: IFilters = unflatten(request.query);
            if (presenceOfCustomFields(filterParams, this.filterableCustomFields)) {
                await cacheManager.set(`${request.params.resourceId}Filters`, filterParams);
                return { ...request, query: {} };
            }
        }
        return request;
    }

    public async filter(response: RecordActionResponse, request: ActionRequest) {
        try {
            const hasInCache = await cacheManager.has(`${request.params.resourceId}Filters`);
            if (hasInCache) {
                const filters: IFilters = await cacheManager.take(
                    `${request.params.resourceId}Filters`
                );
                const recordsAndMeta = await this.getRecordsWithCustomFilters(
                    filters,
                    request,
                    response
                );
                return { ...response, meta: recordsAndMeta.meta, records: recordsAndMeta.params };
            }

            return response;
        } catch (err) {
            throw new ServerError('error when trying to custom filter: ' + err.message, 500);
        }
    }

    public getFilterableCustomFields() {
        return this.filterableCustomFields;
    }

    private async getRecordsWithCustomFilters(
        filters: IFilters,
        request: ActionRequest,
        response: RecordActionResponse
    ) {
        const entity = await getEntity(request.params.resourceId);
        const filterQuery = await getFilterQuery(entity, filters, response);
        const count = await filterQuery.getCount();
        const currentPage = getSelectedPage(request['url']);

        filterQuery.skip(10 * (+currentPage - 1));
        filterQuery.take(10);

        const fields = await filterQuery.getRawMany();
        const recordActions = response.records[0].recordActions;
        const bulkActions = response.records[0].bulkActions;

        const meta = {
            total: count,
            perPage: response.meta.perPage,
            page: currentPage,
            direction: response.meta.direction,
            sortBy: response.meta.sortBy,
        };

        const customPopulated = await getPopulated(fields);

        const params = fields.map((field, index) => ({
            params: field,
            populated: customPopulated[index],
            errors: {},
            id: field.id,
            title: field.title,
            recordActions: recordActions,
            bulkActions: bulkActions,
        }));
        return { meta, params };
    }
}

export default new CustomFieldsFilter();
