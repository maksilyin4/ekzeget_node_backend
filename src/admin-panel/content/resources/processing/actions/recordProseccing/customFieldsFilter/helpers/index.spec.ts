import {
    getEntity,
    getFilterQuery,
    getPopulated,
    getSelectedPage,
    presenceOfCustomFields,
} from './index';
import QueezeQuestions from '../../../../../../../../entities/QueezeQuestions';
import { IFilters } from '../../../../../../types/IFilters';
import customFieldsFilter from '../../customFieldsFilter';
import Verse from '../../../../../../../../entities/Verse';
import { BaseEntity, SelectQueryBuilder } from 'typeorm';
import { RecordActionResponse } from 'admin-bro';
// eslint-disable-next-line import/extensions
import { createQueryBuilderMock, createQueryBuilderMockResult } from './createQueryBuilder.mock';
import EkzegetPerson from '../../../../../../../../entities/EkzegetPerson';
import User from '../../../../../../../../entities/User';
import { getPopulatedResponseThisAllFields } from './resourceForTest';

const createQueryBuilderSpy = jest.spyOn(QueezeQuestions, 'createQueryBuilder');
const getVerseByBookIdSpy = jest.spyOn(Verse, 'getVersesByBookId');
const createQueryBuilderEkzegetPersonSpy = jest.spyOn(EkzegetPerson, 'createQueryBuilder');
const createQueryBuilderUserSpy = jest.spyOn(User, 'createQueryBuilder');

afterAll(() => {
    jest.restoreAllMocks();
});

describe('getEntity', () => {
    createQueryBuilderSpy.mockReturnValueOnce(null);
    test('returns the entity for the QueezeQuestion', async () => {
        const entity = await getEntity('QueezeQuestions');
        expect(createQueryBuilderSpy).toBeCalledWith('entity');
    });
});

describe('presenceOfCustomFields', () => {
    const scripts = [
        [
            'filtering required',
            {
                filters: {
                    id: 1,
                    bookForQuestionList: 2,
                },
                page: '1',
            },
            true,
        ],
        [
            'filtering not required',
            {
                filters: {
                    id: 1,
                },
                page: '1',
            },
            false,
        ],
    ];

    test.each(scripts)('if %s', (name, filters, result) => {
        const customFilterFields = customFieldsFilter.getFilterableCustomFields();
        const isFilter = presenceOfCustomFields(filters as IFilters, customFilterFields);
        expect(isFilter).toBe(result);
    });
});

describe('getSelectedPage', () => {
    const scripts = [
        [
            'only one filters',
            'http://localhost:4000/admin/resources/QueezeQuestions?filters.bookForQuestionList=61&page=1',
            '1',
        ],
        [
            'filters with any parameters',
            'http://localhost:4000/admin/resources/QueezeQuestions?filters.bookForQuestionList=61&page=13&direction=desc&sortBy=id',
            '13',
        ],
    ];
    test.each(scripts)('if %s', (name, url, result) => {
        const page = getSelectedPage(url);
        expect(page).toBe(result);
    });

    test.each(scripts)('if %s [another way]', (name, url, result) => {
        const urlObj = new URL(url);
        expect(urlObj.searchParams.get('page')).toBe(result);
    });
});

describe('getFilterQuery', () => {
    const entity = createQueryBuilderMock;

    const filters = {
        filters: {
            id: 1,
            bookForQuestionList: 2,
        },
        page: '1',
    };

    const response = {
        records: [{ params: { id: 1, verseId: 2 } }],
    };

    getVerseByBookIdSpy.mockResolvedValueOnce(([
        { id: 1 },
        { id: 2 },
        { id: 3 },
    ] as unknown) as Verse[]);

    test('bookForQuestionList filtering', async () => {
        const filterQuery = await getFilterQuery(
            (entity as unknown) as SelectQueryBuilder<any>,
            filters,
            (response as unknown) as RecordActionResponse
        );

        const result = filterQuery.getParameters() as createQueryBuilderMockResult;

        expect(result.selectedFields).toBe('id,verse_id');
        expect(result.whereQuery).toBeUndefined();
        expect(result.andWhereQuery).toEqual([
            { queryString: 'id = :valueid', queryParameters: { valueid: 1 } },
            {
                queryString: 'verse_id IN (:...value_bookForQuestionList)',
                queryParameters: { value_bookForQuestionList: [1, 2, 3] },
            },
        ]);
    });
});

describe('getPopulated', () => {
    createQueryBuilderEkzegetPersonSpy.mockReturnValue(
        (createQueryBuilderMock as unknown) as SelectQueryBuilder<BaseEntity>
    );

    createQueryBuilderUserSpy.mockReturnValue(
        (createQueryBuilderMock as unknown) as SelectQueryBuilder<BaseEntity>
    );

    test('interpretationVerse fields: ekzeget_id, edited_by, added_by', async () => {
        const result = await getPopulated([
            { id: 1, title: '1', ekzeget_id: 1, edited_by: 1, added_by: 1 },
            { id: 2, title: '2', ekzeget_id: 2, edited_by: 2, added_by: 2 },
            { id: 3, title: '3', ekzeget_id: 3, edited_by: 3, added_by: 3 },
        ]);

        expect(result).toEqual(getPopulatedResponseThisAllFields);
    });

    test('without field for populated', async () => {
        const result = await getPopulated([
            { id: 1, title: '1' },
            { id: 2, title: '2' },
            { id: 3, title: '3' },
        ]);

        expect(result).toEqual([{}, {}, {}]);
    });
});
