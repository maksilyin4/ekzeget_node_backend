import QueezeQuestions from '../../../../../../../../entities/QueezeQuestions';
import { IFilters } from '../../../../../../types/IFilters';
import { SelectQueryBuilder } from 'typeorm';
import { RecordActionResponse } from 'admin-bro';
import customFieldsFilter from '../index';
import Verse from '../../../../../../../../entities/Verse';
import Interpretation from '../../../../../../../../entities/Interpretation';
import InterpretationVerse from '../../../../../../../../entities/InterpretationVerse';
import EkzegetPerson from '../../../../../../../../entities/EkzegetPerson';
import User from '../../../../../../../../entities/User';

export async function getEntity(
    resource
): Promise<SelectQueryBuilder<QueezeQuestions | Interpretation> | null> {
    switch (resource) {
        case 'QueezeQuestions':
            return QueezeQuestions.createQueryBuilder('entity');
        case 'Interpretation':
            return Interpretation.createQueryBuilder('entity');
        default:
            return null;
    }
}

export function presenceOfCustomFields(params: IFilters, filterableCustomFields: string[]) {
    for (const key in params.filters) {
        if (filterableCustomFields.includes(key)) {
            return true;
        }
    }
    return false;
}

export async function getFilterQuery(
    entity: SelectQueryBuilder<any>,
    filters: IFilters,
    response: RecordActionResponse
) {
    const customFields = customFieldsFilter.getFilterableCustomFields();
    const params = incorrectNameProtection(Object.keys(response.records[0].params));
    const query = entity.select(params.join(','));

    for (const key in filters.filters) {
        const queryString = customFields.includes(key)
            ? getQueryStringForFilter(key)
            : `${key} = :value${key}`;
        const queryParameters = customFields.includes(key)
            ? await getQueryParametersForFilter(key, filters.filters[key])
            : { [`value${key}`]: filters.filters[key] };

        query.andWhere(queryString, queryParameters);
    }
    return query;
}

export function getSelectedPage(url: string) {
    const urlObj = new URL('http://geturl.ru' + url);
    return urlObj.searchParams.get('page');
}

export async function getPopulated(fields: any[]): Promise<any[]> {
    const buffer: Map<string, Partial<User | EkzegetPerson>> = new Map();
    const populated = await Promise.all(
        fields.map(async field => {
            const populate = {};

            const getParamsFromBuffer = async (
                queryBuilder: SelectQueryBuilder<EkzegetPerson | User>,
                param: string
            ) => {
                if (!buffer.has(`${param}_${field[param]}`)) {
                    const fieldValue = await queryBuilder
                        .where('id = :id', {
                            id: field[param],
                        })
                        .getOne();
                    buffer.set(`${param}_${field[param]}`, fieldValue);
                }
                return buffer.get(`${param}_${field[param]}`);
            };

            if (field.hasOwnProperty('ekzeget_id')) {
                const params = await getParamsFromBuffer(
                    EkzegetPerson.createQueryBuilder('entity'),
                    'ekzeget_id'
                );
                populate['ekzeget_id'] = getPopulate(params, 'name');
            }

            if (field.hasOwnProperty('edited_by')) {
                const params = await getParamsFromBuffer(
                    User.createQueryBuilder('entity'),
                    'edited_by'
                );
                populate['edited_by'] = getPopulate(params, 'username');
            }

            if (field.hasOwnProperty('added_by')) {
                const params = await getParamsFromBuffer(
                    User.createQueryBuilder('entity'),
                    'added_by'
                );
                populate['added_by'] = getPopulate(params, 'username');
            }
            return populate;
        })
    );

    buffer.clear();
    return populated;
}

function getPopulate(params: Partial<User | EkzegetPerson>, fieldName: string) {
    return {
        params,
        populated: {},
        errors: {},
        id: params?.id || null,
        title: params ? params[fieldName] : null,
        recordActions: getRecordAction('User'),
        bulkActions: [],
    };
}

function getRecordAction(entityName: string) {
    return [
        {
            name: 'show',
            actionType: 'record',
            icon: 'Screen',
            label: 'Show',
            resourceId: entityName,
            guard: '',
            showFilter: false,
            component: undefined,
            showInDrawer: false,
            hideActionHeader: false,
            containerWidth: 1,
            layout: null,
            variant: 'default',
            parent: null,
            hasHandler: true,
            custom: {},
        },
    ];
}

function getQueryStringForFilter(filter: string): string | null {
    switch (filter) {
        case 'bookForQuestionList':
            return `verse_id IN (:...value_bookForQuestionList)`;
        default:
            return `id IN (:...value_${filter})`;
    }
}

async function getQueryParametersForFilter(
    filter: string,
    value: number
): Promise<{ [key: string]: any[] } | null> {
    switch (filter) {
        case 'bookForQuestionList':
            const verseIds = await Verse.getVersesByBookId(value);
            return { value_bookForQuestionList: verseIds.map(verse => verse.id) };
        case 'interpretationVerseBook':
            return {
                value_interpretationVerseBook: await InterpretationVerse.getInterpretationIdsByBookParameters(
                    {
                        bookId: value,
                    }
                ),
            };
        case 'interpretationVerseChapter':
            return {
                value_interpretationVerseChapter: await InterpretationVerse.getInterpretationIdsByBookParameters(
                    {
                        chapter: value,
                    }
                ),
            };
        case 'interpretationVerseNumber':
            return {
                value_interpretationVerseNumber: await InterpretationVerse.getInterpretationIdsByBookParameters(
                    {
                        number: value,
                    }
                ),
            };
        default:
            return null;
    }
}

function incorrectNameProtection(keys: string[]): string[] {
    return keys.map(key => {
        switch (key) {
            case 'verseId':
                return 'verse_id';
            default:
                return key;
        }
    });
}
