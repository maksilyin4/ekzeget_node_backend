import customFieldsFilter from './index';
import cacheManager from '../../../../../../../managers/CacheManager';
import { ActionRequest, RecordActionResponse } from 'admin-bro';
import QueezeQuestions from '../../../../../../../entities/QueezeQuestions';
// eslint-disable-next-line import/extensions
import { createQueryBuilderMock } from './helpers/createQueryBuilder.mock';
import { BaseEntity, SelectQueryBuilder } from 'typeorm';
import Verse from '../../../../../../../entities/Verse';
import Interpretation from '../../../../../../../entities/Interpretation';
import InterpretationVerse from '../../../../../../../entities/InterpretationVerse';
import EkzegetPerson from '../../../../../../../entities/EkzegetPerson';
import {
    modifyResponseInterpretation,
    modifyResponseQueezeQuestions,
    requestInterpretation,
    requestQueezeQuestions,
    response,
    withCustomFields,
    withoutCustomFields,
} from './helpers/resourceForTest';

const createQueryBuilderQueezeQuestionsSpy = jest.spyOn(QueezeQuestions, 'createQueryBuilder');
const createQueryBuilderInterpretationSpy = jest.spyOn(Interpretation, 'createQueryBuilder');
const createQueryBuilderEkzegetPersonSpy = jest.spyOn(EkzegetPerson, 'createQueryBuilder');
const cacheManagerHasSpy = jest.spyOn(cacheManager, 'has');

const getInterpretationSpy = jest.spyOn(
    InterpretationVerse,
    'getInterpretationIdsByBookParameters'
);

const getVerseByBookIdSpy = jest.spyOn(Verse, 'getVersesByBookId');

afterAll(() => {
    jest.restoreAllMocks();
});

describe('excludeFromRequest', () => {
    test('with custom fields', async () => {
        const request = await customFieldsFilter.excludeFromRequest(
            (withCustomFields as unknown) as ActionRequest
        );

        const filterParam = await cacheManager.take('QueezeQuestionsFilters');

        expect(request).toEqual({
            params: {
                resourceId: 'QueezeQuestions',
            },
            query: {},
        });
        expect(filterParam).toEqual({
            filters: {
                bookForQuestionList: '61',
            },
            page: '1',
        });
    });

    test('without custom fields', async () => {
        const request = await customFieldsFilter.excludeFromRequest(
            (withoutCustomFields as unknown) as ActionRequest
        );
        expect(request).toEqual(withoutCustomFields);
    });
});

describe('filter', () => {
    createQueryBuilderQueezeQuestionsSpy.mockReturnValueOnce(
        (createQueryBuilderMock as unknown) as SelectQueryBuilder<BaseEntity>
    );

    createQueryBuilderInterpretationSpy.mockReturnValueOnce(
        (createQueryBuilderMock as unknown) as SelectQueryBuilder<BaseEntity>
    );

    createQueryBuilderEkzegetPersonSpy.mockReturnValue(
        (createQueryBuilderMock as unknown) as SelectQueryBuilder<BaseEntity>
    );

    getInterpretationSpy.mockResolvedValueOnce([1, 2, 3]);

    getVerseByBookIdSpy.mockResolvedValueOnce(([
        { id: 1 },
        { id: 2 },
        { id: 3 },
    ] as unknown) as Verse[]);

    test('with custom filter without populated', async () => {
        await cacheManager.set('QueezeQuestionsFilters', {
            filters: {
                bookForQuestionList: '61',
            },
            page: '1',
        });

        const result = await customFieldsFilter.filter(
            (response as unknown) as RecordActionResponse,
            (requestQueezeQuestions as unknown) as ActionRequest
        );

        expect(result).toEqual(modifyResponseQueezeQuestions);
    });

    test('with custom filter and populated (ekzeget_id)', async () => {
        await cacheManager.set('InterpretationFilters', {
            filters: {
                interpretationVerseBook: '61',
            },
            page: '1',
        });

        const result = await customFieldsFilter.filter(
            (response as unknown) as RecordActionResponse,
            (requestInterpretation as unknown) as ActionRequest
        );

        expect(result).toEqual(modifyResponseInterpretation);
    });

    test('without custom filter', async () => {
        const result = await customFieldsFilter.filter(
            ({ res: true } as unknown) as RecordActionResponse,
            ({
                params: {
                    resourceId: 'QueezeQuestions',
                },
            } as unknown) as ActionRequest
        );
        expect(result).toEqual({ res: true });
    });

    test('Error', async () => {
        cacheManagerHasSpy.mockRejectedValueOnce('TestError');
        try {
            const result = await customFieldsFilter.filter(
                (response as unknown) as RecordActionResponse,
                (requestInterpretation as unknown) as ActionRequest
            );
        } catch (err) {
            expect(err.message).toContain('error when trying to custom filter:');
            expect(err.statusCode).toBe(500);
        }
    });
});
