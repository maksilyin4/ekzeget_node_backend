type whereType = { queryParameters: {}; queryString: string };

export type createQueryBuilderMockResult = {
    selectedFields: string;
    whereQuery: whereType;
    andWhereQuery: whereType[];
};

export class createQueryBuilderMock {
    private readonly selectedFields: string = '';
    private readonly whereQuery: whereType;
    private andWhereQuery: whereType[] = [];
    private skipNumber: number = 0;
    private takeNumber: number = 0;

    constructor(fields: string, queryParameters?) {
        this.selectedFields = fields;

        if (queryParameters) {
            this.whereQuery = {
                queryParameters,
                queryString: fields,
            };
        }
    }

    public static select(fields: string) {
        return new createQueryBuilderMock(fields);
    }

    public static where(queryString, queryParameters) {
        return new createQueryBuilderMock(queryString, queryParameters);
    }

    public andWhere(queryString, queryParameters) {
        this.andWhereQuery.push({ queryString, queryParameters });
    }

    public getCount() {
        return 25;
    }

    public skip(skip: number) {
        this.skipNumber = skip;
    }

    public take(take: number) {
        this.takeNumber = take;
    }

    public getRawMany() {
        if (this.andWhereQuery[0].queryString.includes('interpretationVerse')) {
            return [
                { id: 1, title: '1', ekzeget_id: 1 },
                { id: 2, title: '2', ekzeget_id: 2 },
                { id: 3, title: '3', ekzeget_id: 3 },
            ];
        }

        return [
            { id: 1, title: '1', any: 1 },
            { id: 2, title: '2', any: 2 },
            { id: 3, title: '3', any: 3 },
        ];
    }

    public getParameters() {
        return {
            selectedFields: this.selectedFields,
            whereQuery: this.whereQuery,
            andWhereQuery: this.andWhereQuery,
        };
    }

    public getMany() {
        return [{ interpretation_id: 1 }, { interpretation_id: 1 }, { interpretation_id: 1 }];
    }

    public async getOne() {
        const idInQuery = this.whereQuery.queryParameters['id'];
        return {
            id: idInQuery,
            name: 'name' + idInQuery,
            code: idInQuery + 'code',
            username: 'username' + idInQuery,
        };
    }
}
