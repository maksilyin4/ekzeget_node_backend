import { ActionRequest } from 'admin-bro';
class UrlProcessor {
    getValidUrl = (request: ActionRequest) => {
        if (request.payload.hasOwnProperty('url')) {
            let url: string = request.payload.url;
            if (request.params.resourceId === 'MetaTemplate') {
                url = String.prototype.concat(
                    url.startsWith('/') ? '' : '/',
                    url,
                    url.endsWith('/') ? '' : '/'
                );
            }
            return {
                url: url,
            };
        }
        return null;
    };
}

export default new UrlProcessor();
