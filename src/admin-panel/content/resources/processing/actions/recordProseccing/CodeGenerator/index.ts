import { generateSlug } from '../../../../../../../libs/Rus2Translit/generateSlug';
import * as uuid from 'uuid';

class CodeGenerator {
    generate(recordCreatingParams): { code: string; legacy_code?: string } {
        const codePattern = recordCreatingParams['name'] || recordCreatingParams['title'];

        if (!codePattern) return { code: uuid.v4() };

        return {
            code: generateSlug(codePattern),
            legacy_code: `old_${uuid.v4()}`.slice(0, 12),
        };
    }
}

export default new CodeGenerator();
