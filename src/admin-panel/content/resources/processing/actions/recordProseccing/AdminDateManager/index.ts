import getUnixSec from '../../../../../../../libs/helpers/getUnixSec';
import _ from 'lodash';

class AdminDateManager {
    getCreateDateObject = () => ({
        created_at: getUnixSec(),
        added_at: getUnixSec(),
        published_at: getUnixSec(),
        data: getUnixSec(),
    });
    getUpdateDateObject = () => ({
        edited_at: getUnixSec(),
        updated_at: getUnixSec(),
    });
    toUnix(payload) {
        const datesObject = _.pick(payload, [
            'created_at',
            'added_at',
            'published_at',
            'data',
        ]);

        for (const key in datesObject) {
            const date = datesObject[key];
            if (!isNaN(date)) continue;
            datesObject[key] = Date.parse(date) / 1000 || null;
        }

        return datesObject;
    }
}

export default new AdminDateManager();
