import MediaBible from '../../../../../../../entities/MediaBible';
import Book from '../../../../../../../entities/Book';
import Chapters from '../../../../../../../entities/Chapters';
import Verse from '../../../../../../../entities/Verse';
import { IRelationForMedia } from '../../../../../types/IRelationForMedia';

const convertRelationForMediaToStandartView = async (
    relationIds: MediaBible[]
): Promise<Array<IRelationForMedia>> | undefined => {
    const standartViewForMediaReverse = await Promise.all(
        relationIds.map(
            async (item): Promise<IRelationForMedia> => {
                const { short_title } = await Book.findOne({
                    select: ['short_title'],
                    where: { id: item.book_id },
                });

                const chapterNumber = await Chapters.findOne({
                    select: ['number'],
                    where: { id: item.chapter_id },
                });

                const verseNumber = await Verse.findOne({
                    select: ['number'],
                    where: { id: item.verse_id },
                });

                return {
                    id: item.id,
                    relationBookShortRecord: short_title ? short_title : null,
                    relationVerseId: item.verse_id,
                    relationEntityId: item.media_id,
                    relationChapterNumber: chapterNumber ? chapterNumber.number : null,
                    relationVerseNumber: verseNumber ? verseNumber.number : null,
                };
            }
        )
    );
    return standartViewForMediaReverse;
};

export default convertRelationForMediaToStandartView;
