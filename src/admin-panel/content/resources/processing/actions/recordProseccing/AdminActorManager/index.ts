class AdminActorManager {
    getCreateActorObject = admin => {
        return {
            created_by: admin.id,
            added_by: admin.id,
        };
    };
    getUpdateActorObject = admin => {
        return {
            updated_by: admin.id,
            edited_by: admin.id,
        };
    };
}

export default new AdminActorManager();
