import adminDateManager from '../AdminDateManager';
import adminActorManager from '../AdminActorManager';
import UrlProcessor from '../UrlProcessor';
import TextFormatter from '../../../../../../../libs/TextFormatter';
import adminImageManager from '../AdminImageManager';
import { ActionContext, ActionRequest } from 'admin-bro';

const getModifiedPayload = async (
    request: ActionRequest,
    context: ActionContext
): Promise<Record<string, any>> => {
    const { payload } = request;

    const modifyPayload = {
        ...payload,
        ...adminDateManager.getUpdateDateObject(),
        ...adminActorManager.getUpdateActorObject(context.currentAdmin),
        ...UrlProcessor.getValidUrl(request),
    };

    if (request.params.resourceId === 'Interpretation') {
        modifyPayload['parsed_text'] = await new TextFormatter(modifyPayload['comment']).format();
    }

    adminImageManager.clearAxillaryFields(modifyPayload);
    Object.assign(modifyPayload, adminDateManager.toUnix(modifyPayload));

    return modifyPayload;
};

export default getModifiedPayload;
