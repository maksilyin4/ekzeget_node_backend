import getModifiedPayload from './index';
import { ActionContext, ActionRequest } from 'admin-bro';
import Book from '../../../../../../../entities/Book';
import { BaseEntity } from 'typeorm';

const testTimestamp = 1625742831;

const dateNowSpy = jest.spyOn(Date, 'now').mockReturnValue(testTimestamp * 1000);
const bookFindSpy = jest
    .spyOn(Book, 'find')
    .mockResolvedValue(([
        { short: '1 Кор', code: '1oe-poslanie-k-korinfanam-ap-pavla' },
    ] as unknown) as BaseEntity[]);

afterAll(() => {
    jest.restoreAllMocks();
});

const inputPayload = {
    created_at: '2021-07-08T11:13:51.000Z',
    added_at: '2021-07-08T11:13:51.000Z',
    published_at: undefined,
    data: undefined,
    comment: '{{{1 Кор. 3:7}}} и {{{1 Кор. 4:8}}}. [[[Logoj]]]',
    imageUploader: 'test',
    image_id: 123,
    image: 'test',
    url: 'testUrl/testPage',
};

const outputPayload = {
    created_at: testTimestamp,
    added_at: testTimestamp,
    published_at: null,
    data: null,
    comment: '{{{1 Кор. 3:7}}} и {{{1 Кор. 4:8}}}. [[[Logoj]]]',
    url: 'testUrl/testPage',
    edited_at: testTimestamp,
    updated_at: testTimestamp,
    updated_by: 456,
    edited_by: 456,
};

const context = ({
    currentAdmin: {
        id: 456,
    },
} as unknown) as ActionContext;

function getRequest(resource: string) {
    return ({
        params: {
            resourceId: resource,
        },
        payload: inputPayload,
    } as unknown) as ActionRequest;
}

describe('normal payload', () => {
    test('resourceId= Media', async () => {
        const result = await getModifiedPayload(getRequest('Media'), context);
        expect(result).toEqual(outputPayload);
    });

    test('resource = Interpretation', async () => {
        const result = await getModifiedPayload(getRequest('Interpretation'), context);
        expect(result['parsed_text']).toBe(
            '<a class="verse-link link_text" href="/">1 Кор. 3:7</a> и <a class="verse-link link_text" href="/">1 Кор. 4:8</a>. <span class="greek">Logoj</span>'
        );
    });

    test('resource = MetaTemplate', async () => {
        const result = await getModifiedPayload(getRequest('MetaTemplate'), context);
        expect(result['url']).toBe('/testUrl/testPage/');
    });
});
