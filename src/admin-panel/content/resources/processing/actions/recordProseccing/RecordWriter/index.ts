class RecordWriter {
    private defaultValuesObj = {
        place: '',
        year: '',
    };

    public setDefaultValuesToPayload(payload) {
        return { ...this.defaultValuesObj, ...payload };
    }
}

export default new RecordWriter();
