import React, { useEffect, useLayoutEffect, useState } from 'react';
import { BasePropertyProps } from 'admin-bro';
import { IResourceEditQuizQuestion } from '../../../../types/IResourceEditQuizQuestion';
import { Box, Label } from '@admin-bro/design-system';
import resourceEmptyTemplate from '../../../../templates/resourseEditQuizQuestionEmptyTemplate';
import Book from '../../../../../../entities/Book';
import { IRelationVerseForCustomComponents } from '../../../../types/IRelationVerseForCustomComponents';

const EditBookWithChapterAndVerse: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;

    const resource: IResourceEditQuizQuestion = resourceEmptyTemplate;
    const actualLink = props.property.props.actualLinkForInquiries;

    /**
     * Так как форма создания новой записи берется из формы редактирования, но с отсуствующим бэкграундом
     * из хэндлеров редактирования- получение списка книг выполняется на фронте
     */
    const [allBooks, setAllBooks] = useState([]);
    const [relationVerse, setRelationVerse] = useState<IRelationVerseForCustomComponents[]>(
        record.params.relationVerseForCustomComponents || []
    );
    const [selectedVerse, setSelectedVerse] = useState([]); //объект с id и номером стиха выбранных стихов в селекторе
    const [chapters, setChapters] = useState(resource.relationBook.parts);
    const [verses, setVerse] = useState(resource.allVerseForChapter);
    const [selectedBookId, setSelectedBookId] = useState(resource.relationBook.id);

    const [shortBookTitle, setShortBookTitle] = useState('');
    const [chapterNumber, setChapterNumber] = useState(0);

    /**
     * Начальная инициализация newRelationVerse
     */
    if (!record.params.hasOwnProperty('newRelationVerse')) {
        onChange({
            ...record,
            params: {
                ...record.params,
                newRelationVerse: [...relationVerse],
            },
        });
    }

    useEffect(() => {
        getAllBooks().then(res => setAllBooks(res));
    }, []);

    /**
     * Все связанные стихи хранятся в переменной relationVerse, любые удаления и добавления отражаются на содержимом
     * переменной. И при любом изменении переменной новые данные записываются в поле глобальног объекта params
     * newRelationVerse. После, на бэкэ сравнивается содержимое того что пришло изначально в relationVerseForCustomComponents
     * и того, что пришло после изменений в newRelationVerse
     */
    useEffect(() => {
        onChange({
            ...record,
            params: {
                ...record.params,
                newRelationVerse: [...relationVerse],
            },
        });
    }, [relationVerse]);

    const selectedVerseHandler = event => {
        setSelectedVerse(
            [...event.target.selectedOptions].map(item => {
                return { id: item.value, number: item.title };
            })
        );
    };

    const paintSelectedVerse = () => {
        const newRelationVerse = selectedVerse.map(item => {
            return {
                id: chapterNumber * item.number * Math.floor(Math.random() * 1000),
                relationBookShortRecord: shortBookTitle + '. ' + chapterNumber + ':' + item.number,
                relationVerseId: item.id,
                relationEntityId: null,
            };
        });
        setRelationVerse([...relationVerse, ...newRelationVerse]);
    };

    const removeVerse = value => {
        setRelationVerse(relationVerse.filter(item => item.id !== value));
    };

    async function getAllBooks() {
        const allBooksExtraFields = await fetch(actualLink + '/book/list', {
            method: 'GET',
        });

        const allBooksExtraFieldsJson: Book[] = await allBooksExtraFields.json();
        const allBooks = [];
        for (const key in allBooksExtraFieldsJson) {
            if (Array.isArray(allBooksExtraFieldsJson[key]['books'])) {
                allBooks.push(...allBooksExtraFieldsJson[key]['books']);
            }
        }

        return allBooks.map(({ id, code, parts, title, short_title }) => ({
            id,
            code,
            parts,
            title,
            short_title,
        }));
    }

    async function getChapter(event) {
        const data = { code: event.target.value };
        const result = await fetch(actualLink + '/book/info/' + data.code, {
            method: 'GET',
        });
        const toJsonResult = await result.json();
        setSelectedBookId(toJsonResult.book.id);
        setChapters(toJsonResult.book.parts);

        allBooks.forEach(item => {
            if (item.code === data.code) {
                setShortBookTitle(item.short_title);
            }
        });
    }

    async function getVerse(chapter) {
        const chapterNumber = chapter.target.value;
        const result = await fetch(actualLink + '/verse/' + selectedBookId + '/' + chapterNumber, {
            method: 'GET',
        });
        const toJsonResult = await result.json();
        setVerse(toJsonResult.verses);
        setChapterNumber(chapterNumber);
    }

    const optionListBooks = allBooks.map(book => (
        <option value={book.code} title={book.short_title} key={book.short_title}>
            {book.title}
        </option>
    ));

    const optionsListChapter = Array.from(Array(chapters).keys()).map(i => (
        <option value={i + 1} key={i + 1}>
            {i + 1}
        </option>
    ));

    const optionListVerse = verses.map(item => (
        <option value={item.id} title={item.number.toString()} key={item.number}>
            {item.number + ' - ' + item.text}
        </option>
    ));

    return (
        <Box>
            <Label>{'Связанные стихи'}</Label>
            <Label>{'Книга'}</Label>
            <select onChange={value => getChapter(value)}>{optionListBooks}</select>

            <Label>{'Глава'}</Label>
            <select onChange={value => getVerse(value)}>{optionsListChapter}</select>

            <Label>{'Стих'}</Label>
            <select multiple={true} size={10} onChange={selectedVerseHandler}>
                {optionListVerse}
            </select>
            <button type={'button'} onClick={paintSelectedVerse}>
                Добавить
            </button>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {relationVerse.map(item => {
                    return (
                        <button
                            type={'button'}
                            style={{ margin: '10px' }}
                            key={item.id}
                            onClick={() => removeVerse(item.id)}
                        >
                            {item.relationBookShortRecord}
                        </button>
                    );
                })}
            </div>
        </Box>
    );
};

export default EditBookWithChapterAndVerse;
