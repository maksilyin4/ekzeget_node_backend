import React from 'react';
import { Box, Label } from '@admin-bro/design-system';
import { BasePropertyProps } from 'admin-bro';

const Edit: React.FC<BasePropertyProps> = props => {
    const { record, property } = props;

    const srcImg = record.params['image'] || record.params['image_id'];

    return (
        <Box>
            <Label>{property.label}</Label>
            {srcImg ? <img src={srcImg} width="100px" /> : 'no image'}
            <div style={{ height: 40 }} />
        </Box>
    );
};

export default Edit;
