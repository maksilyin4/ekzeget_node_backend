import React from 'react';
import { BasePropertyProps } from 'admin-bro';
import { Box, Text, Label } from '@admin-bro/design-system';

const DateField: React.FC<BasePropertyProps> = props => {
    const val = props.record.params[props.property.name] || '-';

    return (
        <Box>
            {props.where ==='show' ? <Label variant={'light'} required={props.property.isRequired}>{props.property.label}</Label> : ''}
            <Text>{val}</Text>
        </Box>
    );
};

export default DateField;
