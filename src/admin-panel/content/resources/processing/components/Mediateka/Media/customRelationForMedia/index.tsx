import React, { useEffect, useState } from 'react';
import { BasePropertyProps } from 'admin-bro';
import { IResourceEditQuizQuestion } from '../../../../../../types/IResourceEditQuizQuestion';
import { Box, Label } from '@admin-bro/design-system';
import resourceEmptyTemplate from '../../../../../../templates/resourseEditQuizQuestionEmptyTemplate';
import Book from '../../../../../../../../entities/Book';
import { IRelationForMedia } from '../../../../../../types/IRelationForMedia';

const CustomRelationForMedia: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;

    const resource: IResourceEditQuizQuestion = resourceEmptyTemplate;
    const actualLink = props.property.props.actualLinkForInquiries;

    /**
     * Так как форма создания новой записи берется из формы редактирования, но с отсуствующим бэкграундом
     * из хэндлеров редактирования- получение списка книг выполняется на фронте
     */
    const [allBooks, setAllBooks] = useState([]);
    const [relations, setRelations] = useState<IRelationForMedia[]>(
        record.params.customRelationForMedia || []
    );

    const [chapters, setChapters] = useState(resource.relationBook.parts);
    const [verses, setVerse] = useState(resource.allVerseForChapter);
    const [selectedBookId, setSelectedBookId] = useState(resource.relationBook.id);

    const [shortBookTitle, setShortBookTitle] = useState('');
    const [chapterNumber, setChapterNumber] = useState(0);
    const [selectedVerses, setSelectedVerses] = useState([]);

    /**
     * Начальная инициализация newRelationsForMedia
     */
    useEffect(() => {
        if (!record.params.hasOwnProperty('newRelationsForMedia')) {
            record.params.newRelationsForMedia = relations;

            onChange({
                ...record,
                params: {
                    ...record.params,
                    newRelationsForMedia: [...relations],
                },
            });
        }
    }, [record.params.customRelationForMedia]);

    useEffect(() => {
        getAllBooks().then(res => setAllBooks(res));
    }, []);

    /**
     * Все связанные сущности хранятся в переменной relations, любые удаления и добавления отражаются на содержимом
     * переменной. И при любом изменении переменной новые данные записываются в поле глобальног объекта params
     * newRelationsForMedia. После, на бэкэ сравнивается содержимое того что пришло изначально в customRelationForMedia
     * и того, что пришло после изменений в newRelationsForMedia
     */
    useEffect(() => {
        onChange({
            ...record,
            params: {
                ...record.params,
                newRelationsForMedia: [...relations],
            },
        });
    }, [relations]);

    const selectedRelationHandler = event => {
        setSelectedVerses(
            [...event.target.selectedOptions].map(option => {
                return {
                    id: option.value,
                    number: option.title,
                };
            })
        );
    };

    const paintSelectedRelation = () => {
        if (shortBookTitle) {
            const currentRelationVersesIds = relations.map(relation => relation.relationVerseId);
            const newRelationsForMedia = selectedVerses
                .filter(selected => !currentRelationVersesIds.includes(selected.id))
                .map(item => {
                    return {
                        id:
                            (chapterNumber | 1) *
                            (item.number | 1) *
                            Math.floor(Math.random() * 1000),
                        relationBookShortRecord: shortBookTitle,
                        relationVerseId: item.id,
                        relationEntityId: null,
                        relationChapterNumber: chapterNumber,
                        relationVerseNumber: item.number,
                    };
                });

            if (newRelationsForMedia.length > 0) {
                setRelations([...relations, ...newRelationsForMedia]);
            }
        }
    };

    const removeVerse = value => {
        setRelations(relations.filter(item => item.id !== value));
    };

    async function getAllBooks() {
        const allBooksExtraFields = await fetch(actualLink + '/book/list', {
            method: 'GET',
        });

        const allBooksExtraFieldsJson: Book[] = await allBooksExtraFields.json();
        const allBooks = [];
        for (const key in allBooksExtraFieldsJson) {
            if (Array.isArray(allBooksExtraFieldsJson[key]['books'])) {
                allBooks.push(...allBooksExtraFieldsJson[key]['books']);
            }
        }

        return allBooks.map(({ id, code, parts, title, short_title }) => ({
            id,
            code,
            parts,
            title,
            short_title,
        }));
    }

    async function getChapter(event) {
        const data = { code: event.target.value };
        if (data.code != 'empty') {
            const result = await fetch(actualLink + '/book/info/' + data.code, {
                method: 'GET',
            });
            const toJsonResult = await result.json();
            setSelectedBookId(toJsonResult.book.id);
            setChapters(toJsonResult.book.parts);

            allBooks.forEach(item => {
                if (item.code === data.code) {
                    setShortBookTitle(item.short_title);
                }
            });
        } else {
            setSelectedBookId(null);
            setChapters(0);
            setShortBookTitle(null);
        }
    }

    async function getVerse(chapter) {
        const chapterNumber = chapter.target.value | 0;
        const result = await fetch(actualLink + '/verse/' + selectedBookId + '/' + chapterNumber, {
            method: 'GET',
        });
        const toJsonResult = await result.json();

        setVerse(toJsonResult.verses);
        setChapterNumber(chapterNumber);
    }

    const optionListBooks = allBooks.map(book => (
        <option value={book.code} title={book.short_title} key={book.short_title}>
            {book.title}
        </option>
    ));
    optionListBooks.unshift(
        <option value={'empty'} title={'empty'} key={'empty'}>
            {'Книга не выбрана'}
        </option>
    );

    const optionsListChapter = Array.from(Array(chapters).keys()).map(i => (
        <option value={i + 1} key={i + 1}>
            {i + 1}
        </option>
    ));
    optionsListChapter.unshift(
        <option value={null} key={null}>
            {'Не выбрана'}
        </option>
    );

    const optionListVerse = verses.map(item => (
        <option value={item.id} title={item.number.toString()} key={item.number}>
            {item.number + ' - ' + item.text}
        </option>
    ));
    optionListVerse.unshift(
        <option value={''} title={null} key={null}>
            {'Не выбран'}
        </option>
    );

    return (
        <Box>
            <Label>{'Связанные источники'}</Label>
            <Label>{'Книга'}</Label>
            <select onChange={value => getChapter(value)}>{optionListBooks}</select>

            <Label>{'Глава'}</Label>
            <select onChange={value => getVerse(value)}>{optionsListChapter}</select>

            <Label>{'Стих'}</Label>
            <select size={10} multiple={true} onChange={selectedRelationHandler}>
                {optionListVerse}
            </select>
            <button type={'button'} onClick={paintSelectedRelation}>
                Добавить
            </button>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {relations.map(item => {
                    let buttonLabel = item.relationBookShortRecord;
                    if (item.relationChapterNumber) {
                        buttonLabel += ' ' + item.relationChapterNumber;
                        if (item.relationVerseNumber) {
                            buttonLabel += ':' + item.relationVerseNumber;
                        }
                    }
                    return (
                        <button
                            type={'button'}
                            style={{ margin: '10px' }}
                            key={item.id}
                            onClick={() => removeVerse(item.id)}
                        >
                            {buttonLabel}
                        </button>
                    );
                })}
            </div>
        </Box>
    );
};

export default CustomRelationForMedia;
