import { BasePropertyProps } from 'admin-bro';
import { Box, Label } from '@admin-bro/design-system';
import React, { useState, useEffect } from 'react';

const defaultCategories = [
    { id: 5, title: 'Видео', selected: false },
    { id: 6, title: 'Мотиваторы', selected: false },
    { id: 7, title: 'Библия в искусстве', selected: false },
    { id: 8, title: 'Иконография', selected: false },
    { id: 12, title: 'Живопись', selected: false },
    { id: 13, title: 'Книги ', selected: false },
    { id: 14, title: 'Музыка', selected: false },
    { id: 20, title: 'Архитектура', selected: false },
    { id: 21, title: 'Библия - переводы', selected: false },
    { id: 22, title: 'Скульптура', selected: false },
    { id: 24, title: 'Толкования к главам', selected: false },
    { id: 25, title: 'Толкования к стихам', selected: false },
    { id: 26, title: 'Библия отвечает', selected: false },
    { id: 27, title: 'Праздники и события', selected: false },
    { id: 28, title: 'Православный ответ', selected: false },
    { id: 31, title: 'Искусство в Библии', selected: false },
    { id: 32, title: 'Экзегет фильм', selected: false },
    { id: 33, title: 'К  Библии', selected: false },
    { id: 34, title: 'Цитаты отцов', selected: false },
    { id: 39, title: 'Библейские темы', selected: false },
    { id: 40, title: 'Библия - толкования\t', selected: false },
    { id: 48, title: 'Святые отцы', selected: false },
    { id: 49, title: 'Священники и богословы', selected: false },
    { id: 50, title: 'Художественная литература ', selected: false },
    { id: 53, title: 'Литургика', selected: false },
    { id: 55, title: 'Иисусова молитва', selected: false },
    { id: 56, title: 'Толкования святых', selected: false },
    { id: 57, title: 'Молитвослов', selected: false },
    { id: 58, title: 'Молитва Господня', selected: false },
    { id: 59, title: 'Вопросы викторины', selected: false },
    { id: 60, title: 'Ответы на вопросы викторины', selected: false },
];

const CustomCat: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;

    const [selectedCategories, setSelectedCategories] = useState([]); //массив выбранных категорий
    const [categories, setCategories] = useState(record.params.customCat || defaultCategories);

    /**
     * Начальная инициализация newCategories
     */
    useEffect(() => {
        if (!record.params.hasOwnProperty('newCustomCat')) {

            record.params.newCustomCat = categories;
            onChange({
                ...record,
                params: {
                    ...record.params,
                    newCustomCat: [...categories],
                },
            });
        }
    }, [record.params.customCat]);

    useEffect(() => {
        onChange({
            ...record,
            params: {
                ...record.params,
                newCustomCat: [...categories],
            },
        });
    }, [categories]);

    const optionCategoriesList = categories.map(category => {
        return (
            <option value={category.id} title={category.title} key={category.id}>
                {category.title}
            </option>
        );
    });
    optionCategoriesList.unshift(
        <option key={null} title={''} id={null}>
            {'Категория не выбрана'}
        </option>
    );

    const selectCategoryHandler = event => {
        setSelectedCategories(
            [...event.target.selectedOptions].map(option => {
                return { id: Number(option.value), title: option.title, selected: true };
            })
        );
    };

    const removeCategory = value => {
        setCategories(
            categories.map(item => (item.id === value ? { ...item, selected: false } : item))
        );
    };

    const paintSelectedCategory = () => {
        const selectedIds = selectedCategories.map(category => category.id);

        setCategories(
            categories.map(item =>
                selectedIds.includes(item.id) ? { ...item, selected: true } : item
            )
        );
    };

    return (
        <div>
            <Box>
                <Label>{'Категория'}</Label>
                <select
                    style={{ width: '100%', marginBottom: '20px', height: '125px' }}
                    multiple={true}
                    onChange={selectCategoryHandler}
                >
                    {optionCategoriesList}
                </select>
                <div>
                    <button
                        style={{ marginBottom: '20px' }}
                        type={'button'}
                        onClick={paintSelectedCategory}
                    >
                        Добавить категорию
                    </button>
                </div>
                <div>
                    {categories
                        .filter(category => category.selected)
                        .map(item => {
                            return (
                                <button
                                    style={{
                                        marginTop: '10px',
                                        marginBottom: '20px',
                                        marginRight: '20px',
                                    }}
                                    key={item.id}
                                    onClick={() => removeCategory(item.id)}
                                >
                                    {item.title}
                                </button>
                            );
                        })}
                </div>
            </Box>
        </div>
    );
};

export default CustomCat;
