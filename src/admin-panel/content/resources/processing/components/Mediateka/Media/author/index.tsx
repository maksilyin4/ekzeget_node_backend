import React, { useState } from 'react';
import { ApiClient, BasePropertyProps, useResource } from 'admin-bro';
import { Box, Button, Icon, ModalInline, Overlay } from '@admin-bro/design-system';
import PropertyType from 'admin-bro/src/frontend/components/property-type/index';
import { RecordJSON } from 'admin-bro/src/frontend/interfaces/index';
import useRecord from 'admin-bro/src/frontend/hooks/use-record/use-record';

const author: React.FC<BasePropertyProps> = props => {
    const { record, onChange } = props;

    const [isCreatingAuthor, setIsCreatingAuthor] = useState(false);

    const [isSaving, setIsSaving] = useState(false);

    const authorResource = useResource('MediaAuthor');

    const { record: authorRecord, handleChange: authorHandleChange } = useRecord(
        undefined,
        'MediaAuthor'
    );

    const AuthorComponent = (
        <Box>
            {authorResource.editProperties.map(property => (
                <PropertyType
                    key={property.propertyPath}
                    where="edit"
                    onChange={authorHandleChange}
                    property={property}
                    resource={authorResource}
                    record={authorRecord as RecordJSON}
                />
            ))}
            <Button
                variant="primary"
                onClick={e => {
                    e.preventDefault();
                    saveAuthor();
                }}
            >
                {isSaving && <Icon icon="Fade" spin />}
                Сохранить
            </Button>
        </Box>
    );

    async function openAuthorModal(e) {
        e.preventDefault();
        setIsCreatingAuthor(true);
    }

    function closeAuthorModal() {
        setIsCreatingAuthor(false);
    }

    function discardChanges() {
        authorHandleChange({
            ...authorRecord,
            params: {},
        });
    }

    function saveAuthor() {
        setIsSaving(true);
        new ApiClient()
            .resourceAction({
                actionName: 'new',
                resourceId: 'MediaAuthor',
                data: { ...authorRecord.params },
                method: 'post',
            })
            .then(({ data }: any) => {
                onChange({
                    ...record,
                    params: {
                        ...record.params,
                        author_id: data.record.id,
                    },
                    populated: {
                        ...record.populated,
                        author_id: data.record,
                    },
                });
                closeAuthorModal();
                discardChanges();
            })
            .catch(e => {
                alert('Произошла ошибка: ' + e.message);
            })
            .finally(() => {
                setIsSaving(false);
            });
    }

    return (
        <Box>
            <Button mt={-50} onClick={openAuthorModal}>
                Создать автора
            </Button>

            {isCreatingAuthor && (
                <Box
                    style={{ inset: 0 }}
                    flex
                    position="fixed"
                    justifyContent="center"
                    alignItems="center"
                >
                    <Overlay onClick={closeAuthorModal} />
                    <ModalInline style={{ height: '90%', overflowY: 'scroll', zIndex: 1001 }}>
                        {AuthorComponent}
                    </ModalInline>
                </Box>
            )}
        </Box>
    );
};
export default author;
