import React, { useEffect, useState } from 'react';
import { BasePropertyProps } from 'admin-bro';
import { Box, Label } from '@admin-bro/design-system';
import Select from 'react-select/async';
import MediaTag from '../../../../../../../../entities/MediaTag';

const customRelatedTags: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;

    const actualLink = props.property.props.actualLinkForInquiries;

    const relatedTags: Array<Pick<MediaTag, 'id' | 'title'>> =
        record.params.customRelatedTags || [];

    const [selected, setSelected] = useState(
        relatedTags.map(tag => {
            return {
                value: tag.id,
                label: tag.title,
            };
        })
    );

    const getOptionsFromRecords = records => {
        return records.map(r => ({ value: r.id, label: r.title }));
    };

    /**
     * Начальная инициализация newCustomRelatedTags
     */
    useEffect(() => {
        if (!record.params.hasOwnProperty('newCustomRelatedTags')) {
            record.params.newCustomRelatedTags = getOptionsFromRecords(relatedTags);

            onChange({
                ...record,
                params: {
                    ...record.params,
                    newCustomRelatedTags: getOptionsFromRecords(relatedTags),
                },
            });
        }
    }, [record.params.customRelatedTags]);

    const loadOptions = async inputValue => {
        if (inputValue.length > 2) {
            const records = await fetch(actualLink + '/media/get-tags-autocomplete/' + inputValue, {
                method: 'GET',
            });

            const recordsJson = await records.json();

            return getOptionsFromRecords(recordsJson.tags);
        }
    };

    const handleChange = selectedOptions => {
        setSelected(selectedOptions);
        onChange({
            ...record,
            params: {
                ...record.params,
                newCustomRelatedTags: [...selectedOptions],
            },
        });
    };

    return (
        <Box>
            <Label>{'Связанные теги'}</Label>
            <Select
                style={{ marginTop: '20px' }}
                isMulti
                defaultOptions
                loadOptions={loadOptions}
                value={selected}
                onChange={handleChange}
            />
        </Box>
    );
};

export default customRelatedTags;
