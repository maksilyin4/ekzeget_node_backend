import IRcTree from '../../../../../../../../../managers/FtpManager/types/IRcTree';

export function treeTraversal(newBranch: IRcTree[], key: string) {
    return function addDateToTree(tree: IRcTree[]) {
        return tree.map(node => {
            if (node?.children?.length) {
                node.children = addDateToTree([...node.children]);
            } else {
                if (node.key === key) {
                    node.children = [...newBranch];
                }
            }

            return node;
        });
    };
}

export function pathProcessor(path: string) {
    const fileTypes = ['.mp3', '.pdf', '.fb2', '.epub'];
    const dotIndex = path.lastIndexOf('.');
    if (dotIndex != -1) {
        if (fileTypes.includes(path.slice(dotIndex))) {
            return path.slice(0, path.lastIndexOf('/'));
        }
    }
    return path;
}

export async function getContentFolder(path, actualLink) {
    const content = await fetch(actualLink + '/admin/get-ftp-list/?path=' + pathDecorator(path), {
        method: 'GET',
    });
    const contentJson = await content.json();
    return contentJson.tree;
}

function pathDecorator(path) {
    return path === '/' ? '%23' : path;
}
