import React, { useState } from 'react';
import { BasePropertyProps } from 'admin-bro';
import { Box, Text } from '@admin-bro/design-system';
import { getContentFolder } from './helpers';
import CollapsableTree from './helpers/CollapsableTree';

function setExpandedFileTreeKeys(expandedFileTreeKeys) {
    localStorage.setItem('expandedFileTreeKeys', JSON.stringify(expandedFileTreeKeys));
}

const FilesChooser: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;

    const actualLink = props.property.props.actualLinkForInquiries;

    const [ftpPath] = useState('/');
    const [isChangingRemoteFilesList, setIsChangingRemoteFilesList] = useState(false);
    const [chosenFiles, setChosenFiles] = useState({});
    const [chosenFilesQty, setChosenFilesQty] = useState(0);
    const [expandedFileTreeKeys] = useState(JSON.parse(localStorage.getItem('expandedFileTreeKeys')) || []);

    async function changeRemoteFilesChoice(keys, info, isEntry = true) {
        if (info.node.isLeaf) {
            if (info.checked) {
                chosenFiles[info.node.key] = null;
            } else {
                delete chosenFiles[info.node.key];
            }
        } else {
            if (isEntry) {
                setIsChangingRemoteFilesList(true);
            }
            const contentDir = await getContentFolder(info.node.key, actualLink);
            for (const node of contentDir) {
                await changeRemoteFilesChoice(keys, { ...info, node }, false);
            }
        }

        if (isEntry) {
            onChange({
                ...record,
                params: {
                    ...record.params,
                    remoteFilesToRelatedMedia: Object.keys(chosenFiles),
                },
            });
            setChosenFilesQty(Object.keys(chosenFiles).length);
            setIsChangingRemoteFilesList(false);
        }
    }

    return (
        <Box borderTopWidth={2} borderColor="grey" borderStyle="solid" pt={10}>
            <Text fontSize={20} textAlign="center">
                Выбрано файлов: {chosenFilesQty}
            </Text>

            <CollapsableTree
                checkable={true}
                onCheck={changeRemoteFilesChoice}
                height={400}
                defaultExpandParent={false}
                disabled={isChangingRemoteFilesList}
                actualLink={actualLink}
                ftpPath={ftpPath}
                collapsedMsg="Выбрать в ftp-хранилище"
                defaultExpandedKeys={expandedFileTreeKeys}
                onExpand={setExpandedFileTreeKeys}
            />
        </Box>
    );
};

export default FilesChooser;
