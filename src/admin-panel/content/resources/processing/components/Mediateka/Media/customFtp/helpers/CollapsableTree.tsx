import React, { useState } from 'react';
import { Box, Loader } from '@admin-bro/design-system';
import Tree from 'rc-tree';
import IRcTree from '../../../../../../../../../managers/FtpManager/types/IRcTree';
import { getContentFolder, treeTraversal } from '.';
import Button from '@admin-bro/design-system/src/atoms/button/button';
import { TreeProps } from 'rc-tree/lib/Tree';

export default (
    props: Partial<TreeProps> & {
        actualLink: string;
        ftpPath: string;
        openedMsg?: string;
        collapsedMsg?: string;
    }
) => {
    const [treeData, setTreeData] = useState<IRcTree[]>([]);
    const {
        actualLink,
        ftpPath,
        openedMsg = 'Скрыть ftp папку',
        collapsedMsg = 'Показать ftp папку',
    } = props;
    const [isFtpTreeLoading, setIsFtpTreeLoading] = useState(false);

    const treeLoadData = async treeNode => {
        const contentDir = await getContentFolder(treeNode.key, actualLink);
        if (contentDir) {
            const directoryTreeTraversal = treeTraversal(contentDir, treeNode.key);
            const newTree = directoryTreeTraversal([...treeData]);
            setTreeData(newTree);
        }
    };

    function toggleFtpTree(e) {
        e.preventDefault();
        if (treeData.length) {
            setTreeData([]);
        } else {
            setIsFtpTreeLoading(true);
            getContentFolder(ftpPath, actualLink).then(contentDir => {
                setTreeData(contentDir);
                setIsFtpTreeLoading(false);
            });
        }
    }

    return (
        <Box>
            <Button
                onClick={toggleFtpTree}
                my="10px"
                mx="auto"
                style={{ display: 'block' }}
                disabled={props.disabled || isFtpTreeLoading}
            >
                {treeData.length ? openedMsg : collapsedMsg}
            </Button>
            <div>
                <Tree
                    {...props}
                    disabled={props.disabled || isFtpTreeLoading}
                    loadData={treeLoadData}
                    treeData={treeData}
                />
            </div>

            {props.disabled || isFtpTreeLoading ? <Loader /> : ''}
        </Box>
    );
};
