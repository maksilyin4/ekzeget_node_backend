import React, { useState } from 'react';
import { BasePropertyProps } from 'admin-bro';
import { Box, Label, Input } from '@admin-bro/design-system';

interface IPropertiesForMedia {
    fb2?: string;
    epub?: string;
    pdf?: string;
    audio_href?: string;
}

const videoSources = { youtube_url: 'Youtube', vk_url: 'видео vk.com', video_file_url: 'видеофайл' };

const linkTypeTitles = new Map([
    ...Object.entries(videoSources),
    ['fb2', 'fb2'],
    ['epub', 'epub'],
    ['pdf', 'pdf'],
    ['audio_href', 'аудио'],
]);

const customRelatedTags: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;

    const propertiesJson = record.params.properties;

    const linksToMediaResources: IPropertiesForMedia &
        { [P in keyof typeof videoSources]?: string } = propertiesJson
        ? JSON.parse(propertiesJson)
        : {};

    const [newProperties, setNewProperties] = useState(
        linksToMediaResources !== '"null"' ? JSON.stringify(linksToMediaResources) : null
    );

    const [currentType, setCurrentType] = useState(null);

    /**
     * При добавлении новой записи- рисует поля "сылка на видео, ссылка на удио и.т." в зависимости от выбранной
     * категории
     */
    if (!Object.keys(linksToMediaResources).length) {
        if (record.params.hasOwnProperty('type') && currentType != record.params.type) {
            setCurrentType(record.params.type);
            switch (record.params.type) {
                case 'V':
                    for (const source of Object.keys(videoSources)) {
                        linksToMediaResources[source] = '';
                    }
                    setNewProperties(JSON.stringify(linksToMediaResources));
                    break;
                case 'A':
                    setNewProperties(JSON.stringify({ audio_href: '' }));
                    break;
                case 'B':
                    setNewProperties(JSON.stringify({ fb2: '', pdf: '', epub: '' }));
                    break;
            }
        }
    }

    const handleChange = params => {
        const value = params.target.value;
        const key = params.target.title;
        const changeProperties = JSON.parse(newProperties);
        changeProperties[key] = value;
        setNewProperties(JSON.stringify(changeProperties));
        onChange({
            ...record,
            params: {
                ...record.params,
                properties: JSON.stringify(changeProperties),
            },
        });
    };

    const content = [];
    if (newProperties) {
        for (const key in JSON.parse(newProperties)) {
            content.push(
                <Box>
                    <Label htmlFor={key}>Ссылка на {linkTypeTitles.get(key)}</Label>
                    <Input
                        id={key}
                        title={key}
                        width={1}
                        defaultValue={JSON.parse(newProperties)[key]}
                        onInput={handleChange}
                    />
                </Box>
            );
        }
    }

    return <Box>{content}</Box>;
};

export default customRelatedTags;
