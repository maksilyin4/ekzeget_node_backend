import { getContentFolder, pathProcessor, treeTraversal } from './index';

afterAll(() => {
    jest.restoreAllMocks();
});

describe('AdminBro: customFtp helpers', () => {
    describe('function pathProcessor', () => {
        const suite = [
            { input: '/Test', expected: '/Test' },
            { input: '/Test/music.mp3', expected: '/Test' },
            { input: '/Test/secondFolder', expected: '/Test/secondFolder' },
            { input: '/Test/secondFolder/book.fb2', expected: '/Test/secondFolder' },
            { input: '/Test/secondFolder/thirdFolder', expected: '/Test/secondFolder/thirdFolder' },
            {
                input: '/Test/secondFolder/thirdFolder/book.pdf',
                expected: '/Test/secondFolder/thirdFolder',
            },
        ];


        test.each([suite])('files', ({ expected, input }) => {
            const result = pathProcessor(input);

            expect(result).toContain(expected);
        });
    });

    describe('function getContentFolder', () => {
        const testTree = [
            { title: 'branch1', key: '/branch1', isLeaf: false },
            { title: 'branch2', key: '/branch2', isLeaf: false },
            { title: 'branch3', key: '/branch3', isLeaf: false },
        ];

        const testResponse = {
            tree: testTree,
        };

        global.fetch = jest.fn(
            () =>
                Promise.resolve({ json: () => Promise.resolve(testResponse) }) as Promise<Response>
        );

        test('request /', async () => {
            const response = await getContentFolder('/', 'testLink');
            expect(response).toEqual(testTree);
            expect(global.fetch).toHaveBeenCalledWith('testLink/admin/get-ftp-list/?path=%23', {
                method: 'GET',
            });
        });

        test('request /test', async () => {
            const response = await getContentFolder('/test', 'testLink');
            expect(response).toEqual(testTree);
            expect(global.fetch).toHaveBeenCalledWith('testLink/admin/get-ftp-list/?path=/test', {
                method: 'GET',
            });
        });
    });

    describe('function treeTraversal', () => {
        const testTree = [
            { title: 'first', key: '/first', isLeaf: false },
            { title: 'second', key: '/second', isLeaf: false },
            { title: 'third', key: '/third', isLeaf: false },
        ];

        const testBranch1 = [
            { title: 'firstBranch.mp3', key: '/first/firstBranch.mp3', isLeaf: true },
            { title: 'secondBranch.mp3', key: '/first/secondBranch.mp3', isLeaf: true },
            { title: 'thirdBranch.mp3', key: '/first/thirdBranch.mp3', isLeaf: true },
        ];

        const testBranch2 = [
            { title: 'firstBranch.mp3', key: '/second/firstBranch.mp3', isLeaf: true },
            { title: 'secondBranch.mp3', key: '/second/secondBranch.mp3', isLeaf: true },
            { title: 'thirdBranch.mp3', key: '/second/thirdBranch.mp3', isLeaf: true },
            { title: 'fourBranch', key: '/second/fourBranch', isLeaf: false },
        ];

        const testBranch3 = [
            { title: 'firstBranch.mp3', key: '/second/fourBranch/firstBranch.mp3', isLeaf: true },
            { title: 'secondBranch.mp3', key: '/second/fourBranch/secondBranch.mp3', isLeaf: true },
            { title: 'thirdBranch.mp3', key: '/second/fourBranch/thirdBranch.mp3', isLeaf: true },
        ];

        const resultTree = [
            {
                title: 'first',
                key: '/first',
                isLeaf: false,
                children: [
                    { title: 'firstBranch.mp3', key: '/first/firstBranch.mp3', isLeaf: true },
                    { title: 'secondBranch.mp3', key: '/first/secondBranch.mp3', isLeaf: true },
                    { title: 'thirdBranch.mp3', key: '/first/thirdBranch.mp3', isLeaf: true },
                ],
            },
            {
                title: 'second',
                key: '/second',
                isLeaf: false,
                children: [
                    { title: 'firstBranch.mp3', key: '/second/firstBranch.mp3', isLeaf: true },
                    { title: 'secondBranch.mp3', key: '/second/secondBranch.mp3', isLeaf: true },
                    { title: 'thirdBranch.mp3', key: '/second/thirdBranch.mp3', isLeaf: true },
                    {
                        title: 'fourBranch',
                        key: '/second/fourBranch',
                        isLeaf: false,
                        children: [
                            {
                                title: 'firstBranch.mp3',
                                key: '/second/fourBranch/firstBranch.mp3',
                                isLeaf: true,
                            },
                            {
                                title: 'secondBranch.mp3',
                                key: '/second/fourBranch/secondBranch.mp3',
                                isLeaf: true,
                            },
                            {
                                title: 'thirdBranch.mp3',
                                key: '/second/fourBranch/thirdBranch.mp3',
                                isLeaf: true,
                            },
                        ],
                    },
                ],
            },
            { title: 'third', key: '/third', isLeaf: false },
        ];
        test('treeTraversal', () => {
            let treeRec = treeTraversal(testBranch1, '/first');
            const firstTree = treeRec(testTree);

            treeRec = treeTraversal(testBranch2, '/second');
            const secondTree = treeRec(firstTree);

            treeRec = treeTraversal(testBranch3, '/second/fourBranch');
            const thirdTree = treeRec(secondTree);

            expect(thirdTree).toEqual(resultTree);
        });
    });
});
