import React, { useState } from 'react';
import { BasePropertyProps } from 'admin-bro';
import { Box, DropZone, Input, Label } from '@admin-bro/design-system';
import { pathProcessor } from './helpers';
import CollapsableTree from './helpers/CollapsableTree';

const BASE_PATH = '/uploads';
const Uploader: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;

    const actualLink = props.property.props.actualLinkForInquiries;

    const mimeTypes = [
        'audio/mpeg',
        'application/pdf',
        'application/epub+zip',
        'application/x-fictionbook+xml',
    ];

    const [ftpPath, setFtpPath] = useState('');

    const onUpload = (files: File[]) => {
        onChange({
            ...record,
            params: {
                ...record.params,
                filesToFtp: files,
                filesToFtpCounts: files.length,
            },
        });
    };

    const changeFtpPath = path => {
        setFtpPath(path.target.value.replace(BASE_PATH, ''));
        onChange({
            ...record,
            params: {
                ...record.params,
                ftpPath: path.target.value.replace(BASE_PATH, ''),
            },
        });
    };

    const treeOnSelect = info => {
        const path = pathProcessor(info[0]);
        changeFtpPath({ target: { value: path } });
    };

    return (
        <Box>
            <Label>{'Загрузка на FTP'}</Label>
            <Label htmlFor="pathInput">Папка для загрузки:</Label>
            <Input id="pathInput" value={ftpPath} width={1} onChange={changeFtpPath} />
            <DropZone multiple onChange={onUpload} validate={{ mimeTypes }} />
            <CollapsableTree
                onSelect={treeOnSelect}
                height={400}
                defaultExpandParent={false}
                actualLink={actualLink}
                ftpPath={BASE_PATH + ftpPath}
            />
        </Box>
    );
};

export default Uploader;
