import React, {useEffect, useState} from 'react';
import {BasePropertyProps} from 'admin-bro';
import {Box, Label} from '@admin-bro/design-system';
import Select from 'react-select/async';
import {IRelationRecordInfo} from '../../../../../../types/IRelatedRecordInfo';

const CustomLinkToOtherRecords: React.FC<BasePropertyProps> = props => {
    const { onChange, record, resource } = props;

    const actualLink = props.property.props.actualLinkForInquiries;

    const informationAboutRelatedRecords: IRelationRecordInfo[] =
        record.params.informationAboutRelatedRecords || [];

    const [selected, setSelected] = useState(
        informationAboutRelatedRecords.map((record, index) => {
            return {
                value: record.related_id,
                label: record.title,
                sort: index,
            };
        })
    );

    const getOptionsFromRecords = records => {
        return records.map(r => ({ value: r.id, label: r.title }));
    };

    /**
     * Начальная инициализация newRelatedRecords
     */
    useEffect(() => {
        if (!record.params.hasOwnProperty('newRelatedRecords')) {
            record.params.newRelatedRecords = informationAboutRelatedRecords;

            onChange({
                ...record,
                params: {
                    ...record.params,
                    newRelatedRecords: informationAboutRelatedRecords,
                },
            });
        }
    }, [record.params.informationAboutRelatedRecords]);

    const loadOptions = async inputValue => {
        if (inputValue.length > 2) {
            const records = await fetch(
                actualLink + '/media/get-media-autocomplete/' + inputValue,
                {
                    method: 'GET',
                }
            );
            const recordsJson = await records.json();
            return getOptionsFromRecords(recordsJson.words);
        }
    };

    const handleAddRecord = selectedOption => {
        const newItem = {
            ...selectedOption,
            sort: selected.length,
        };
        setSelected([...selected, newItem]);
        handleChangeNewRelatedRecords([...selected, newItem]);
    };

    function handleChangeNewRelatedRecords(options: typeof selected) {
        onChange({
            ...record,
            params: {
                ...record.params,
                newRelatedRecords: [...options],
            },
        });
    }

    const moveUp = sort => {
        if (sort) {
            setSelected(changeNeighboring('up', sort));
        }
        handleChangeNewRelatedRecords(selected);
    };

    const moveDown = sort => {
        if (sort < selected.length - 1) {
            setSelected(changeNeighboring('down', sort));
        }
        handleChangeNewRelatedRecords(selected);
    };

    const removeMedia = sort => {
        const recordArrayWithoutDeleted = Array.prototype.concat(
            selected.slice(0, sort),
            selected.slice(sort + 1).map(item => {
                item.sort--;
                return item;
            })
        );
        setSelected(recordArrayWithoutDeleted);
        handleChangeNewRelatedRecords(recordArrayWithoutDeleted);
    };

    function changeNeighboring(direction: 'up' | 'down', numberElement: number) {
        const correction = direction === 'up' ? -1 : 1;
        return selected
            .map((item, index) => {
                if (index === numberElement) {
                    item.sort += correction;
                }

                if (index === numberElement + 1 * correction) {
                    item.sort -= correction;
                }
                return item;
            })
            .sort((a, b) => a.sort - b.sort);
    }

    return (
        <Box>
            <Label>{'Связанные записи'}</Label>

            <Select
                style={{ marginTop: '20px' }}
                defaultOptions
                loadOptions={loadOptions}
                onChange={handleAddRecord}
            />

            <div>
                {selected.map(item => (
                    <div
                        style={{
                            border: '1px solid gray',
                            padding: '5px',
                            display: 'flex',
                            justifyContent: 'space-between',
                        }}
                        key={item.value}
                    >
                        <div>
                            <button
                                type={'button'}
                                key={item.value + 'up'}
                                style={{ padding: '5px' }}
                                onClick={() => moveUp(item.sort)}
                            >
                                {'↑'}
                            </button>
                            <button
                                type={'button'}
                                key={item.value + 'down'}
                                style={{ padding: '5px' }}
                                onClick={() => moveDown(item.sort)}
                            >
                                {'↓'}
                            </button>

                            <a href={`${resource.href}/records/${item.value}/edit`}>
                                {item.label}
                            </a>
                        </div>
                        <button
                            type={'button'}
                            style={{ padding: '5px', marginLeft: 'auto' }}
                            onClick={() => removeMedia(item.sort)}
                        >
                            {'X'}
                        </button>
                    </div>
                ))}
            </div>
        </Box>
    );
};

export default CustomLinkToOtherRecords;
