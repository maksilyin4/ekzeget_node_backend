import React from 'react';
import { BasePropertyProps } from 'admin-bro';
import Label from '@admin-bro/design-system/src/atoms/label';

const EmptyTemplate: React.FC<BasePropertyProps> = props => {
    return <Label></Label>;
};

export default EmptyTemplate;
