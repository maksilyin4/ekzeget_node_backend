import React from 'react';
import { BasePropertyProps } from 'admin-bro';
import { Box, Text } from '@admin-bro/design-system';
import Button from '@admin-bro/design-system/src/atoms/button/button';

const customRelatedTags: React.FC<BasePropertyProps> = props => {
    const data = JSON.parse(props.record.params.data);

    const text = data.hasOwnProperty('interpretation_id') ? (
        <Text>
            Пользователем:&nbsp;{data.public_identity}
            <br />
            Толкователя:&nbsp;
            <a
                className="verse-link link_text"
                href={`/admin/resources/EkzegetPerson/records/${data.ekzeget.id}/show`}
            >
                {data.ekzeget.name}
            </a>
            <br />
            Стихи:&nbsp;
            {data.verses.map(item => (
                <a key={item.id} href={`/admin/resources/Verse/records/${item.id}/show`}>
                    {item.short};&nbsp;
                </a>
            ))}
            <br />
            <a
                className="verse-link link_text"
                href={`/admin/resources/Interpretation/records/${data.interpretation_id}/show`}
            >
                <Button>Посмотреть толкование</Button>
            </a>
        </Text>
    ) : (
        <Text>
            Пользователь:&nbsp;
            <a
                className="verse-link link_text"
                href={`/admin/resources/User/records/${data.user_id}/show`}
            >
                <Button> {data.public_identity}</Button>
            </a>
        </Text>
    );

    return <Box>{text}</Box>;
};

export default customRelatedTags;
