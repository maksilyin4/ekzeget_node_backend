import React, { useState } from 'react';
import { BasePropertyProps } from 'admin-bro';
import { Box, Button, Icon, Label, Loader, ModalInline, Overlay } from '@admin-bro/design-system';
import Select from 'react-select';
import * as Joi from '@hapi/joi';
import booksOutputJoiSchema from '../../../../../../../api/RestApi/Controller/controllers/Ekzeget/EkzegetBookList/outputJoiSchema';
import chaptersOutputJoiSchema from '../../../../../../../api/RestApi/Controller/controllers/Ekzeget/EkzegetChaptersByBook/outputJoiSchema';
import versesOutputJoiSchema from '../../../../../../../api/RestApi/Controller/controllers/Ekzeget/EkzegetVersesByChapter/outputJoiSchema';

type BooksType = Joi.extractType<typeof booksOutputJoiSchema>['books'];
type ChaptersType = Joi.extractType<typeof chaptersOutputJoiSchema>['chapters'];
type VersesType = Joi.extractType<typeof versesOutputJoiSchema>['verses'];

const interpretations: React.FC<BasePropertyProps> = props => {
    const { record, onChange } = props;

    const [isAddingInterpretation, setIsAddingInterpretation] = useState(false);

    const [exegetes, setExegetes] = useState([]);
    const [exegete, setExegete] = useState<{ id: string; name: string }>();
    const [books, setBooks] = useState<BooksType>([]);
    const [book, setBook] = useState<BooksType[0]>();
    const [chapters, setChapters] = useState<ChaptersType>([]);
    const [chapter, setChapter] = useState<ChaptersType[0]>();
    const [verses, setVerses] = useState<VersesType>([]);
    const [verse, setVerse] = useState<VersesType[0]>();

    function resetChapters() {
        setChapter(undefined);
        setChapters([]);
        setVerse(undefined);
        setVerses([]);
    }

    function resetBook() {
        setBook(undefined);
        setBooks([]);
        resetChapters();
    }

    function reset() {
        setExegete(undefined);
        setExegetes([]);
        resetBook();
    }

    const apiUrl = props.property.props.apiUrl;

    async function selectExegete({ value: exegete }) {
        setExegete(exegete);

        const booksRequest = await fetch(apiUrl + `/ekzeget/${exegete.id}/book-list`, {
            method: 'GET',
        });

        resetBook();
        setBooks((await booksRequest.json()).books);
    }

    async function selectBook({ value: book }) {
        setBook(book);

        const chaptersRequest = await fetch(
            apiUrl + `/ekzeget/${exegete.id}/chapters-by-book/${book.book_id}`,
            {
                method: 'GET',
            }
        );

        resetChapters();
        setChapters((await chaptersRequest.json()).chapters);
    }

    async function selectChapter({ value: chapter }) {
        setChapter(chapter);

        const versesRequest = await fetch(
            apiUrl + `/ekzeget/${exegete.id}/verses-by-chapter/${chapter.verse_chapter_id}`,
            {
                method: 'GET',
            }
        );

        setVerse(undefined);
        setVerses((await versesRequest.json()).verses);
    }

    async function selectVerse({ value: verse }) {
        setVerse(verse);
    }

    const InterpretationComponent = (
        <Box>
            <Box mb={20}>
                {exegetes.length === 0 && <Loader />}
                <Select
                    isDisabled={exegetes.length === 0}
                    options={exegetes.map(exegete => ({ value: exegete, label: exegete.name }))}
                    onChange={selectExegete}
                    placeholder="Выберите экзегета"
                />
            </Box>
            <Box mb={20}>
                <Select
                    isDisabled={books.length === 0}
                    key={exegete?.id}
                    options={books.map(book => {
                        return { value: book, label: book.book_title };
                    })}
                    onChange={selectBook}
                    placeholder="Выберите книгу"
                />
            </Box>

            <Box mb={20}>
                <Select
                    isDisabled={chapters.length === 0}
                    key={book?.book_id + '' + exegete?.id}
                    options={chapters.map(chapter => {
                        return { value: chapter, label: chapter.verse_chapter };
                    })}
                    onChange={selectChapter}
                    placeholder="Выберите главу"
                />
            </Box>

            <Box mb={20}>
                <Select
                    isDisabled={verses.length === 0}
                    key={chapter?.verse_chapter_id + '' + book?.book_id + '' + exegete?.id}
                    options={verses.map(verse => {
                        return {
                            value: verse,
                            label: `${verse.verse_number} — ${verse.verse_text}`,
                        };
                    })}
                    onChange={selectVerse}
                    placeholder="Выберите стих"
                />
            </Box>

            {verse && (
                <Box mb={20}>
                    <Label dangerouslySetInnerHTML={{ __html: verse.interpretation_parsed_text }} />
                    <Button
                        variant="primary"
                        onClick={e => {
                            e.preventDefault();
                            saveInterpretation();
                        }}
                    >
                        Добавить
                    </Button>
                </Box>
            )}
        </Box>
    );

    async function openInterpretationModal(e) {
        e.preventDefault();
        setIsAddingInterpretation(true);

        if (!exegetes.length) {
            const exegetesRequest = await fetch(apiUrl + '/ekzeget', {
                method: 'GET',
            });

            const { ekzeget: exegetes } = await exegetesRequest.json();
            setExegetes(exegetes);
        }
    }

    function deleteInterpretation(e, id) {
        e.preventDefault();

        record.params.questionInterpretations.splice(
            record.params.questionInterpretations.findIndex(
                interpretation => interpretation.id === id
            ),
            1
        );

        onChange({
            ...record,
            params: {
                ...record.params,
                questionInterpretations: [...record.params.questionInterpretations],
            },
        });
    }

    function closeInterpretationModal() {
        setIsAddingInterpretation(false);
        reset();
    }

    function saveInterpretation() {
        if (!record.params.questionInterpretations) {
            record.params.questionInterpretations = [];
        }
        record.params.questionInterpretations.push({
            ekzeget: exegete,
            verse: { text: verse.verse_text },
            id: verse.interpretation_id,
        });
        onChange({
            ...record,
            params: {
                ...record.params,
                questionInterpretations: [...record.params.questionInterpretations],
            },
        });

        setIsAddingInterpretation(false);
        reset();
    }

    return (
        <Box mt={30}>
            <h1>Толкования</h1>
            {record.params.questionInterpretations &&
                record.params.questionInterpretations.map(interpretation => (
                    <Box mt={20} mb={10} key={interpretation.id}>
                        <Label size="lg">{interpretation.ekzeget.name}</Label>
                        <Label>{interpretation.verse.short}</Label>
                        <Label>{interpretation.verse.text}</Label>
                        <Button onClick={e => deleteInterpretation(e, interpretation.id)}>
                            <Icon icon="Delete" />
                        </Button>
                    </Box>
                ))}
            <Button onClick={openInterpretationModal} mt={20}>
                <Icon icon="Add" />
            </Button>

            {isAddingInterpretation && (
                <Box
                    style={{ inset: 0 }}
                    flex
                    position="fixed"
                    justifyContent="center"
                    alignItems="center"
                >
                    <Overlay onClick={closeInterpretationModal} />
                    <ModalInline style={{ height: '90%', overflowY: 'scroll', zIndex: 1001 }}>
                        {InterpretationComponent}
                    </ModalInline>
                </Box>
            )}
        </Box>
    );
};
export default interpretations;
