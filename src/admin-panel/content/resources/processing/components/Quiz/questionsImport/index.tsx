import React from 'react';
import { Box, DrawerFooter } from '@admin-bro/design-system';
import Button from '@admin-bro/design-system/src/atoms/button/button';
import { useTranslation, useRecord, BasePropertyComponent } from 'admin-bro';
import { useHistory } from 'react-router';

const questionsImport = props => {
    const { record: initialRecord, resource, action } = props;

    const { record, handleChange, submit } = useRecord(initialRecord, resource.id);
    const history = useHistory();
    const { translateButton } = useTranslation();
    const handleSubmit = event => {
        history.push('/admin');
        submit().then(response => {});
        return true;
    };

    return (
        <Box as="form" onSubmit={handleSubmit}>
            <BasePropertyComponent
                where="edit"
                onChange={handleChange}
                property={resource.properties.questionsImport}
                resource={resource}
                record={record}
            />
            <div style={{ height: 40 }} />
            <DrawerFooter>
                <Button variant="primary" size="lg">
                    {translateButton('save', resource.id)}
                </Button>
            </DrawerFooter>
        </Box>
    );
};

export default questionsImport;
