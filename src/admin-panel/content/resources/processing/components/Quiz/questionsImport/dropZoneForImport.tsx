import React from 'react';
import { Box, DropZone, DropZoneProps,  Label } from '@admin-bro/design-system';

const dropZoneForImport = props => {
    const { onChange, record } = props;

    const handleDropZoneChange: DropZoneProps['onChange'] = files => {
        onChange({
            ...record,
            params: {
                ...record.params,
                fileForImportQuizQuestions: files[0],
            },
        });
    };

    const mimeTypes = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
    return (
        <Box flex flexGrow={1} flexDirection="column">
            <Label>Импорт</Label>
            <DropZone onChange={handleDropZoneChange} validate={{ mimeTypes }} />
        </Box>
    );
};

export default dropZoneForImport;
