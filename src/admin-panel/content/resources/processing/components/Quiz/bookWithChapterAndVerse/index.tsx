import React, { useEffect, useLayoutEffect, useState } from 'react';
import { BasePropertyProps } from 'admin-bro';
import { IResourceEditQuizQuestion } from '../../../../../types/IResourceEditQuizQuestion';
import { Box, Label } from '@admin-bro/design-system';
import Book from '../../../../../../../entities/Book';
import resourceEmptyTemplate from '../../../../../templates/resourseEditQuizQuestionEmptyTemplate';

const EditBookWithChapterAndVerse: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;

    const resource: IResourceEditQuizQuestion =
        record.params.bookWithChapterAndVerse || resourceEmptyTemplate;

    const actualLink = props.property.props.actualLinkForInquiries;

    /**
     * Так как форма создания новой записи берется из формы редактирования, но с отсуствующим бэкграундом
     * из хэндлеров редактирования- получение списка книг выполняется на фронте и вместо остальных данных ставиться
     * пустая заглушка.
     */
    const [allBooks, setAllBooks] = useState([]);
    const [chapters, setChapters] = useState(resource.relationBook.parts);
    const [selectedChapter, setSelectedChapter] = useState(resource.relationVerse.chapter);
    const [verses, setVerses] = useState(resource.allVerseForChapter);
    const [selectedBookId, setSelectedBookId] = useState(resource.relationBook.id);

    async function getAllBooks() {
        const allBooksExtraFields = await fetch(actualLink + '/book/list', {
            method: 'GET',
        });

        const allBooksExtraFieldsJson: Book[] = await allBooksExtraFields.json();
        const allBooks = [];
        for (const key in allBooksExtraFieldsJson) {
            if (Array.isArray(allBooksExtraFieldsJson[key]['books'])) {
                allBooks.push(...allBooksExtraFieldsJson[key]['books']);
            }
        }

        return allBooks.map(({ id, code, parts, title }) => ({ id, code, parts, title }));
    }

    useEffect(() => {
        getAllBooks().then(res => setAllBooks(res));
    }, []);

    async function getChapters(event) {
        const data = { code: event.target.value };
        const result = await fetch(actualLink + '/book/info/' + data.code, {
            method: 'GET',
        });
        const toJsonResult = await result.json();
        setSelectedBookId(toJsonResult.book.id);
        setChapters(toJsonResult.book.parts);
        setSelectedChapter(toJsonResult.book.chapters[0].number);
        const verses = await getVerses(toJsonResult.book.id, toJsonResult.book.chapters[0].number);
        saveReference(+verses[0].id);
    }
    /**
     * Способ из доккументации для передачи параметров из кастомных компонентов на сохранение
     * Внутри переданного параметра. @param.target можно найти все необходимые поля из элемента. Хэндлер
     * принимает только один параметр.
     * @param id
     */
    const saveReference = (id: number): void => {
        onChange({
            ...record,
            params: {
                ...record.params,
                verseId: id,
            },
        });
    };

    async function getVerses(bookId, chapter) {
        const result = await fetch(
            actualLink + '/verse/' + bookId + '/' + chapter,
            {
                method: 'GET',
            }
        );
        const toJsonResult = await result.json();
        setVerses(toJsonResult.verses);
        return toJsonResult.verses;
    }

    const optionListBooks = allBooks.map(book => (
        <option
            value={book.code}
            key={'book' + book.id}
            selected={resource.relationBook.id === book.id}
        >
            {book.title}
        </option>
    ));

    const optionsListChapter = Array.from(Array(chapters).keys()).map(i => (
        <option
            value={i + 1}
            key={'chapter' + i}
            selected={selectedChapter === i + 1}
        >
            {i + 1}
        </option>
    ));

    const optionListVerse = verses.map(item => (
        <option
            value={item.id}
            key={'verse' + item.id}
            selected={resource.relationVerse.id === item.id}
        >
            {item.number + ' - ' + item.text}
        </option>
    ));

    return (
        <Box>
            <Label>{'Книга'}</Label>
            <select onChange={value => getChapters(value)}>{optionListBooks}</select>

            <Label>{'Глава'}</Label>
            <select
                onChange={({ target }) =>
                    getVerses(selectedBookId, target.value).then(verses => saveReference(+verses[0].id))
                }
            >
                {optionsListChapter}
            </select>

            <Label>{'Стих'}</Label>
            <select onChange={({ target }) => saveReference(+target.value)}>
                {optionListVerse}
            </select>
        </Box>
    );
};

export default EditBookWithChapterAndVerse;
