import React from 'react';
import { BasePropertyProps } from 'admin-bro';
import { Box, Label } from '@admin-bro/design-system';
import QueezeAnswers from '../../../../../../../entities/QueezeAnswers';
import Input from '@admin-bro/design-system/src/atoms/input';
import answersEmptyTemplate from '../../../../../templates/editQuizAnswersEmptyTemplate';

const EditAnswers: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;

    const radioClick = (fieldParams): void => {
        onChange({
            ...record,
            params: {
                ...record.params,
                correctAnswerId: +fieldParams.target.value,
            },
        });
    };
    const textChange = (fieldParams): void => {
        onChange({
            ...record,
            params: {
                ...record.params,
                newAnswers: {
                    ...record.params.newAnswers,
                    [fieldParams.target.name]: fieldParams.target.value,
                },
            },
        });
    };

    const answers: Partial<QueezeAnswers>[] =
        record.params.answersForQuestionShowAndEdit || answersEmptyTemplate;

    const formAnswer = answers.map(answer => {
        return (
            <Box key={answer.id}>
                <Label>{'Текст ответа'}</Label>
                <Input
                    type={'text'}
                    key={'answer' + answer.id}
                    name={answer.id}
                    defaultValue={answer.text}
                    onChange={fieldParams => textChange(fieldParams)}
                />
                <Label>{'Ответ правильный?'}</Label>
                <Input
                    key={'radioAnswer' + answer.id}
                    type={'radio'}
                    name={'answer'}
                    value={answer.id}
                    defaultChecked={answer['is_correct']}
                    onChange={fieldParams => radioClick(fieldParams)}
                />
            </Box>
        );
    });
    return (
        <Box>
            <Label>{'Ответы'}</Label>
            {formAnswer}
        </Box>
    );
};

export default EditAnswers;
