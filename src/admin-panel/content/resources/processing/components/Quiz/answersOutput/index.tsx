import React from 'react';
import { BasePropertyProps } from 'admin-bro';
import AnswersHTMLConverter from '../../../actions/recordProseccing/AnswersHTMLconverter';

const Show: React.FC<BasePropertyProps> = props => {
    const { record } = props;
    const answers = record.params['answersForQuestionShowAndEdit'];
    const answersHtml = AnswersHTMLConverter(answers);
    return <div dangerouslySetInnerHTML={{ __html: answersHtml }} />;
};

export default Show;
