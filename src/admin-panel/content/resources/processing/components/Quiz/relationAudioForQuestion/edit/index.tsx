import React, { useEffect, useState } from 'react';
import { Box, DropZone, DropZoneItem, Label, DropZoneProps, Input } from '@admin-bro/design-system';
import { BasePropertyProps } from 'admin-bro';
import fetch from "nodemailer/lib/fetch";

/**
 * Отложено до лучших времен. Компонент для связи аудио и вопросов викторины
 * @param props
 * @constructor
 */

const Edit: React.FC<BasePropertyProps> = props => {
    const { property, onChange, record } = props;

    const mimeTypes = ['audio/mpeg'];
    const actualLink = props.property.props.actualLinkForInquiries;

    const [uploadedAudio, setUploadedAudio] = useState('');

    useEffect(() => {
        getMediaPath(record.params.media_id).then(res => setUploadedAudio(res));
    });

    async function getMediaPath(media_id) {
        if (!media_id) {
            return 'Нет аудио';
        }

        const mediaPath = await fetch(actualLink + '')

    }

    const handleDropZoneChange: DropZoneProps['onChange'] = files => {
        onChange({
            ...record,
            params: {
                ...record.params,
                relationAudioForQuestion: files[0],
            },
        });
    };

    const audioToUpload = record.params[property.name];

    return (
        <Box>
            <Label>{'Ссылка на аудио'}</Label>
            {uploadedAudio ? <Input value={uploadedAudio} width={1} /> : 'no image'}
            <div style={{ height: 40 }} />
            <div style={{ height: 40 }} />
            <Label>{'Загрузить аудио'}</Label>
            <DropZone onChange={handleDropZoneChange} validate={{ mimeTypes }} />
            {uploadedAudio && !audioToUpload && <DropZoneItem src={uploadedAudio} />}
            <div style={{ height: 40 }} />
        </Box>
    );
};

export default Edit;
