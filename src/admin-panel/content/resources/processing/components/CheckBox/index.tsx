import React, {useEffect, useState} from 'react';
import { BasePropertyProps } from 'admin-bro';
import Label from '@admin-bro/design-system/src/atoms/label';
import CheckBox from '@admin-bro/design-system/src/atoms/check-box';
import {Box} from "@admin-bro/design-system";

const EmptyTemplate: React.FC<BasePropertyProps> = props => {
    const { onChange, property, record } = props;

    const [isChecked, setChecked] = useState(
        record.params[property.name] ?? !!property.props.defaultValue
    );

    function handleChange(event) {
        setChecked(!event.target.checked);
    }

    useEffect(() => {
        onChange({
            ...record,
            params: {
                ...record.params,
                [property.name]: isChecked,
            },
        });
    }, [isChecked]);

    return (
        <div>
            <Box>
                <CheckBox id={property.name} onChange={handleChange} checked={isChecked} />
                <Label inline htmlFor={property.name} ml="default">
                    {property.label}
                </Label>
            </Box>
        </div>
    );
};

export default EmptyTemplate;
