import React, { FC, memo, useRef, useState } from 'react';
import { FormGroup, FormMessage } from '@admin-bro/design-system';

import { Editor, IAllProps } from '@tinymce/tinymce-react';

import { EditPropertyProps } from 'admin-bro/src/frontend/components/property-type/base-property-props';
import { recordPropertyIsEqual } from 'admin-bro/src/frontend/components/property-type/record-property-is-equal';
import { PropertyLabel } from 'admin-bro/src/frontend/components/property-type/utils/property-label';

type CustomType = {
    tinyMCE?: IAllProps;
};

const Edit: FC<EditPropertyProps> = props => {
    const { property, record, onChange } = props;
    const [value] = useState(record.params?.[property.path] ?? '');

    const error = record.errors && record.errors[property.path];

    const { props: propertyProps } = property;

    const { tinyMCE = {} } = propertyProps as CustomType;
    const editorRef = useRef(null);

    return (
        <FormGroup error={Boolean(error)}>
            <PropertyLabel property={property} />
            <Editor
                {...tinyMCE}
                tinymceScriptSrc="/public/tinymce/tinymce.min.js"
                onInit={(evt, editor) => (editorRef.current = editor)}
                initialValue={value ?? ''}
                onChange={(ev, editor) => onChange(property.path, editor.getContent())}
                init={{
                    height: 500,
                    menubar: false,
                    contextmenu: false,
                    plugins:
                        'advlist autolink lists link image charmap preview ' +
                        'searchreplace visualblocks code fullscreen ' +
                        'insertdatetime media table code help wordcount',
                    toolbar:
                        'undo redo | formatselect | ' +
                        'bold italic backcolor | fontfamily fontsize blocks | alignleft aligncenter  | link | ' +
                        'alignright alignjustify | bullist numlist outdent indent | ' +
                        'removeformat | help',
                    paste_preprocess: function(plugin, args) {
                        if (/yt-formatted-string/g.test(args.content)) {
                            args.content = '<pre>' + args.content + '</pre>';
                        }
                    },
                    content_style:
                        'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
                }}
            />
            <FormMessage>{error?.message}</FormMessage>
        </FormGroup>
    );
};

export default memo(Edit, recordPropertyIsEqual);
