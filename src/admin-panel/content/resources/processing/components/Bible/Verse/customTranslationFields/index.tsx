import React, { useEffect, useState } from 'react';
import { BasePropertyProps } from 'admin-bro';
import { Box, Label, TextArea } from '@admin-bro/design-system';
import VerseTranslate from '../../../../../../../../entities/VerseTranslate';
import Translate from '../../../../../../../../entities/Translate';

interface ICustomTranslationField extends Pick<Translate, 'title' | 'code'> {
    text: string | null;
}

const EditBookWithChapterAndVerse: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;
    const actualLink = props.property.props.actualLinkForInquiries;

    const [allTranslate, setAllTranslate] = useState<ICustomTranslationField[]>([]);
    const [newRelationTranslate, setNewRelationTranslate] = useState<Map<String, String>>(
        new Map()
    );

    useEffect(() => {
        getAllTranslate().then(res => setAllTranslate(res));
    }, []);

    async function getAllTranslate() {
        const allTranslateExtraFields = await fetch(actualLink + '/book/translate-list', {
            method: 'GET',
        });

        const allTranslateExtraFieldsJson = await allTranslateExtraFields.json();
        const relationTranslationsArray: Array<Pick<VerseTranslate, 'code' | 'text'>> =
            record.params.customRelationTranslations || [];
        const relationTranslationsMap = new Map();

        relationTranslationsArray.forEach(item => {
            relationTranslationsMap.set(item.code, item.text);
        });

        onChange({
            ...record,
            params: {
                ...record.params,
                oldRelationTranslate: Object.fromEntries(relationTranslationsMap),
            },
        });

        return allTranslateExtraFieldsJson['translates'].map(item => {
            return {
                code: item.code,
                title: item.title,
                text: relationTranslationsMap.has(item.code)
                    ? relationTranslationsMap.get(item.code)
                    : '',
            };
        });
    }

    function changeTextArea(params) {
        setNewRelationTranslate(newRelationTranslate.set(params.target.title, params.target.value));

        onChange({
            ...record,
            params: {
                ...record.params,
                newRelationTranslate: Object.fromEntries(newRelationTranslate),
            },
        });
    }

    return (
        <Box>
            {allTranslate.map(item => {
                return (
                    <Box>
                        <Label htmlFor={item.code}>{item.title}</Label>
                        <TextArea
                            onChange={params => changeTextArea(params)}
                            id={item.code}
                            title={item.code}
                            defaultValue={item.text}
                            width={1}
                        />
                    </Box>
                );
            })}
        </Box>
    );
};

export default EditBookWithChapterAndVerse;
