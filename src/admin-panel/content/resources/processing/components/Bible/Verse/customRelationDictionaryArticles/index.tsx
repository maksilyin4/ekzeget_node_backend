import React, { useEffect, useState } from 'react';
import { BasePropertyProps } from 'admin-bro';
import { Box, Label } from '@admin-bro/design-system';
import DictionaryType from '../../../../../../../../entities/DictionaryType';
import Dictionary from '../../../../../../../../entities/Dictionary';
import Select from 'react-select/async';

const EditBookWithChapterAndVerse: React.FC<BasePropertyProps> = props => {
    const { onChange, record } = props;
    const actualLink = props.property.props.actualLinkForInquiries;

    const [allDictionary, setAllDictionary] = useState<Array<Pick<DictionaryType, 'id' | 'title'>>>(
        []
    );

    const customRelationDictionaryArticles: Array<Pick<Dictionary, 'id' | 'word'>> =
        record.params.customRelationDictionaryArticles || [];

    const [selectDictionaryId, setSelectDictionaryId] = useState(1);
    const [selected, setSelected] = useState(
        customRelationDictionaryArticles.map(article => {
            return {
                value: article.id,
                label: article.word,
            };
        })
    );

    useEffect(() => {
        getAllDictionary().then(res => setAllDictionary(res));
    }, []);

    async function getAllDictionary() {
        const allDictionaryExtraFields = await fetch(actualLink + '/dictionary/list', {
            method: 'GET',
        });

        const allDictionaryExtraFieldsJson = await allDictionaryExtraFields.json();

        return allDictionaryExtraFieldsJson['dictionary'].map(item => {
            return {
                id: item.id,
                title: item.title,
            };
        });
    }

    const getOptionsFromRecords = records => {
        return records.map(r => ({ value: r.id, label: r.word }));
    };

    /**
     * Начальная инициализация newRelationDictionaryArticle
     */
    if (!record.params.hasOwnProperty('newRelationDictionaryArticle')) {
        onChange({
            ...record,
            params: {
                ...record.params,
                newRelationDictionaryArticle: [
                    ...getOptionsFromRecords(customRelationDictionaryArticles),
                ],
            },
        });
    }

    async function changeDictionary(event) {
        const id = event.target.value;
        setSelectDictionaryId(id);
    }

    const loadOptions = async inputValue => {
        const select = inputValue.length > 2 ? inputValue : '0';
        const records = await fetch(
            actualLink + '/verse/get-words-autocomlete/' + selectDictionaryId + '/' + select,
            {
                method: 'GET',
            }
        );

        const recordsJson = await records.json();

        const options = getOptionsFromRecords(recordsJson.words);
        return options;
    };

    const optionListDictionary = allDictionary.map(dictionary => (
        <option value={dictionary.id} title={dictionary.title} key={dictionary.id}>
            {dictionary.title}
        </option>
    ));

    const handleChange = selectedOptions => {
        setSelected(selectedOptions);
        onChange({
            ...record,
            params: {
                ...record.params,
                newRelationDictionaryArticle: [...selectedOptions],
            },
        });
    };

    return (
        <Box>
            <Label>{'Связанные словарные статьи'}</Label>
            <select onChange={value => changeDictionary(value)}>{optionListDictionary}</select>
            <Select
                style={{ marginTop: '20px' }}
                isMulti
                defaultOptions
                loadOptions={loadOptions}
                value={selected}
                onChange={handleChange}
            />
        </Box>
    );
};

export default EditBookWithChapterAndVerse;
