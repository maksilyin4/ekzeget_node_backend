import React from 'react';
import { Box, DropZone, DropZoneItem, Label, DropZoneProps } from '@admin-bro/design-system';
import { BasePropertyProps } from 'admin-bro';

const Edit: React.FC<BasePropertyProps> = props => {
    const { property, onChange, record } = props;

    const handleDropZoneChange: DropZoneProps['onChange'] = files => {
        onChange({
            ...record,
            params: {
                ...record.params,
                imageUploader: files[0],
            },
        });
    };

    const uploadedPhoto = record.params['image'] || record.params['image_id'];
    const photoToUpload = record.params[property.name];

    return (
        <Box>
            <Label>{'Старое изображение'}</Label>
            {uploadedPhoto ? <img src={uploadedPhoto} width="100px" /> : 'no image'}
            <div style={{ height: 40 }} />
            <div style={{ height: 40 }} />
            <Label>{'Загрузить изображение'}</Label>
            <DropZone onChange={handleDropZoneChange} />
            {uploadedPhoto && !photoToUpload && <DropZoneItem src={uploadedPhoto} />}
            <div style={{ height: 40 }} />
        </Box>
    );
};

export default Edit;
