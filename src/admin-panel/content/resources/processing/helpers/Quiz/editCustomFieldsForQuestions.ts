import QueezeQuestions from '../../../../../../entities/QueezeQuestions';
import QueezeAnswers from '../../../../../../entities/QueezeAnswers';
import { saveInterpretations } from './saveCustomFieldsForQuestions';

const editCustomFieldsForQuestions = async customFieldsData => {
    await QueezeQuestions.updateVerseId(customFieldsData.id, customFieldsData.verseId);

    if ('correctAnswerId' in customFieldsData) {
        await QueezeAnswers.setIsCorrectAnswersFromGroup(
            customFieldsData.id,
            customFieldsData.correctAnswerId
        );
    }

    for (const key in customFieldsData) {
        if (key.startsWith('newAnswers')) {
            const id = key.slice(key.lastIndexOf('.') + 1);
            await QueezeAnswers.updateAnswerTextById(id, customFieldsData[key]);
        }
    }

    await saveInterpretations(customFieldsData.id, customFieldsData);
};

export default editCustomFieldsForQuestions;
