import QueezeQuestions from '../../../../../../entities/QueezeQuestions';
import QueezeAnswers from '../../../../../../entities/QueezeAnswers';
import Interpretation from '../../../../../../entities/Interpretation';

export async function saveInterpretations(questionId, customFieldsData) {
    const question = await QueezeQuestions.findOne(questionId);
    question.interpretations = [];

    for (let i = 0; ; i++) {
        const interpretationId = customFieldsData[`questionInterpretations.${i}.id`];
        if (!interpretationId) {
            break;
        }
        question.interpretations.push(await Interpretation.findOne(interpretationId));
    }

    await question.save();
}

const saveCustomFieldsForQuestions = async customFieldsData => {
    const answersArray: Partial<QueezeAnswers>[] = [];

    for (const key in customFieldsData) {
        if (key.startsWith('newAnswers')) {
            const number = key.slice(key.lastIndexOf('.') + 1);
            answersArray.push({
                is_correct: customFieldsData.correctAnswerId === number,
                text: customFieldsData[key],
            });
        }
    }

    while (answersArray.length < 4) {
        answersArray.push({ is_correct: false, text: '' });
    }

    const relatedQuestion = await QueezeQuestions.getOneQuestion(customFieldsData.code);

    await Promise.all(
        answersArray.map(async item => {
            item.question_id = relatedQuestion.id;
            return QueezeAnswers.insert(item);
        })
    );

    await saveInterpretations(customFieldsData.id, customFieldsData);
};

export default saveCustomFieldsForQuestions;
