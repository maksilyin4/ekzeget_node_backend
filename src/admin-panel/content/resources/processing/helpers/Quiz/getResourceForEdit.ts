import { IResourceEditQuizQuestion } from '../../../../types/IResourceEditQuizQuestion';
import Book from '../../../../../../entities/Book';
import Verse from '../../../../../../entities/Verse';

export default async function getResourceForEdit(
    verseId: number
): Promise<Partial<IResourceEditQuizQuestion>> {
    const relationBook = await Book.getBookByVerseId(verseId);
    const relationVerse = await Verse.findOne({ where: { id: verseId } });
    const resourseForQuizEdit: Partial<IResourceEditQuizQuestion> = {
        relationBook: relationBook,
        relationVerse: relationVerse,
        allVerseForChapter: await Verse.find({
            select: ['id', 'number', 'text'],
            where: {
                book_id: relationBook.id,
                chapter: relationVerse.chapter,
            },
        }),
    };
    return resourseForQuizEdit;
}
