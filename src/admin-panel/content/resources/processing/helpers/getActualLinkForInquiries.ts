import EnvironmentChecker from '../../../../../core/EnvironmentChecker';

export default function getActualLinkForInquiries() {
    const environmentChecker = new EnvironmentChecker();
    const env = environmentChecker.getCheckedEnvironment();
    return env.PROTOCOL + '://' + env.REST_HOST + env.API_PREFIX + env.API_VERSION;
}
