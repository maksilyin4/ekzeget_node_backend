import InterpretationVerseLink from '../../../../../../entities/InterpretationVerseLink';
import Interpretation from '../../../../../../entities/Interpretation';

const saveMentionedVerses = async (customFieldsData: Record<string, any>) => {
    if (customFieldsData.hasOwnProperty('id')) {
        await InterpretationVerseLink.editMentionedVerses(
            customFieldsData['comment'],
            customFieldsData['id']
        );
    } else {
        const { id } = await Interpretation.findOne({
            order: {
                id: 'DESC',
            },
        });
        await InterpretationVerseLink.newMentionedVerses(customFieldsData['comment'], id);
    }
};

export default saveMentionedVerses;
