import {
    TimelineApplicationEnum,
    TimelineCategoriesEnum,
    TimelineEventsEnum,
} from '../../../../../../const/enums';
import Interpretation from '../../../../../../entities/Interpretation';
import User from '../../../../../../entities/User';
import TimelineEvent from '../../../../../../entities/TimelineEvent';

const addEventToTimeLine = async (customFieldsData: Record<string, any>) => {
    const { interpretationId, event } = await getInterpretationIdAndEvent(
        customFieldsData.hasOwnProperty('id'),
        customFieldsData
    );

    const { username } = await User.findOne({
        select: ['username'],
        where: { id: customFieldsData['added_by'] },
    });

    const interpretation = {
        id: interpretationId,
        ekzeget_id: customFieldsData['ekzeget_id'],
        added_at: customFieldsData['added_at'],
    };

    const timelineData = await TimelineEvent.getDataByInterpretation(interpretation, {
        id: customFieldsData['added_by'],
        username,
    });

    await TimelineEvent.create({
        event,
        application: TimelineApplicationEnum.backend,
        category: TimelineCategoriesEnum.interpretation,
        data: JSON.stringify(timelineData),
    }).save();
};

export default addEventToTimeLine;

async function getInterpretationIdAndEvent(isEdit: boolean, customFieldsData: Record<string, any>) {
    let interpretationId;
    let event;
    if (isEdit) {
        interpretationId = customFieldsData.id;
        event = TimelineEventsEnum.edit;
    } else {
        interpretationId = (
            await Interpretation.findOne({
                order: {
                    id: 'DESC',
                },
            })
        ).id;
        event = TimelineEventsEnum.new;
    }

    return { interpretationId, event };
}
