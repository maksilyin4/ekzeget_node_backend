import addEventToTimeLine from './saveRelationsSources';
import User from '../../../../../../entities/User';
import { BaseEntity } from 'typeorm';
import Interpretation from '../../../../../../entities/Interpretation';
import EkzegetPerson from '../../../../../../entities/EkzegetPerson';
import InterpretationVerse from '../../../../../../entities/InterpretationVerse';
import Verse from '../../../../../../entities/Verse';
import { IVerseForMediaAndAll } from '../../../../../../core/App/interfaces/IQuiz';
import TimelineEvent from '../../../../../../entities/TimelineEvent';

const userFindSpy = jest
    .spyOn(User, 'findOne')
    .mockResolvedValue(({ username: 'testUser' } as unknown) as BaseEntity);

const interpretationFindOneSpy = jest
    .spyOn(Interpretation, 'findOne')
    .mockResolvedValue(({ id: 555 } as unknown) as BaseEntity);

const ekzegetPersonFindOneSpy = jest
    .spyOn(EkzegetPerson, 'findOne')
    .mockResolvedValue(({ name: 'testEkzeget' } as unknown) as BaseEntity);

const interpretationVerseFindSpy = jest
    .spyOn(InterpretationVerse, 'find')
    .mockResolvedValue(([
        { verse_id: 1 },
        { verse_id: 2 },
        { verse_id: 3 },
    ] as unknown) as BaseEntity[]);

const verseGetVerseWithBookInfoSpy = jest
    .spyOn(Verse, 'getVerseWithBookInfo')
    .mockImplementation((verse_id: number) => {
        return new Promise(resolve => {
            resolve({ id: verse_id, short: 'short_' + verse_id } as IVerseForMediaAndAll);
        });
    });

const timelineEventCreateSpy = jest.spyOn(TimelineEvent, 'create').mockReturnValue({
    save: () => {
        return new Promise(resolve => {
            resolve((true as unknown) as BaseEntity);
        });
    },
} as BaseEntity);

afterAll(() => {
    jest.restoreAllMocks();
});

describe('addEventToTimeLine', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    test('event Edit', async () => {
        await addEventToTimeLine({
            id: 123,
            added_by: 333,
            ekzeget_id: 321,
            added_at: 123456789,
        });

        expect(interpretationFindOneSpy).not.toBeCalled();

        expect(userFindSpy.mock.calls[0][0]).toEqual({
            select: ['username'],
            where: { id: 333 },
        });

        expect(timelineEventCreateSpy.mock.calls[0][0]).toEqual({
            event: 'edit',
            application: 'backend',
            category: 'interpretation',
            data:
                '{"public_identity":"testUser","user_id":333,"created_at":123456789,"interpretation_id":123,"verses":[{"id":1,"short":"short_1"},{"id":2,"short":"short_2"},{"id":3,"short":"short_3"}],"ekzeget":{"id":321,"name":"testEkzeget"}}',
        });
    });

    test('event New', async () => {
        await addEventToTimeLine({
            added_by: 333,
            ekzeget_id: 321,
            added_at: 123456789,
        });

        expect(interpretationFindOneSpy.mock.calls[0][0]).toEqual({
            order: {
                id: 'DESC',
            },
        });

        expect(userFindSpy.mock.calls[0][0]).toEqual({
            select: ['username'],
            where: { id: 333 },
        });

        expect(timelineEventCreateSpy.mock.calls[0][0]).toEqual({
            event: 'new',
            application: 'backend',
            category: 'interpretation',
            data:
                '{"public_identity":"testUser","user_id":333,"created_at":123456789,"interpretation_id":555,"verses":[{"id":1,"short":"short_1"},{"id":2,"short":"short_2"},{"id":3,"short":"short_3"}],"ekzeget":{"id":321,"name":"testEkzeget"}}',
        });
    });
});
