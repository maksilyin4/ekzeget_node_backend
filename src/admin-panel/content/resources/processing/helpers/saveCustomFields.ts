import { ActionRequest } from 'admin-bro';
import editCustomFieldsForQuestions from './Quiz/editCustomFieldsForQuestions';
import saveCustomFieldsForQuestions from './Quiz/saveCustomFieldsForQuestions';
import saveRelationVerse from './CustomRelationVerseComponent/saveRelationVerse';
import saveRelationDictionaryArticle from './Bible/Verse/saveRelationDictionaryArticle';
import saveRelationTranslates from './Bible/Verse/saveRelationTranslates';
import saveRelationsSources from './Mediateka/Media/saveRelationsSources';
import saveRelationsTags from './Mediateka/Media/saveRelationsTags';
import saveRelationsMedia from './Mediateka/Media/saveRelationsMedia';
import saveNewMediaToFtpAndRelate from './Mediateka/Media/saveNewMediaToFtpAndRelate';
import addEventToTimeLine from './Interpretation/saveRelationsSources';
import saveMentionedVerses from './Interpretation/saveMentionedVerses';
import { resourceWithRelationVerseComponent } from '../../../types/resourceWithRelationVerseComponent';
import { saveMediaCategories } from './Mediateka/Media/saveMediaCategories';
import relateMediaFromFtp from './Mediateka/Media/relateMediaFromFtp';

//TODO упросить процесс сохранения можно при помощи пакета https://www.npmjs.com/package/flat

const saveCustomFields = async (request: ActionRequest): Promise<void> => {
    const currentResourceName = request.params.resourceId;
    const action = request.params.action;

    if (resourceWithRelationVerseComponent.includes(currentResourceName)) {
        await saveRelationVerse(request.payload, currentResourceName);
    }

    switch (currentResourceName) {
        case 'QueezeQuestions':
            if (action === 'edit') {
                await editCustomFieldsForQuestions(request.payload);
            } else if (action === 'new') {
                request.payload.id = request.params.recordId;
                await saveCustomFieldsForQuestions(request.payload);
            }
            break;
        case 'Verse':
            await saveRelationDictionaryArticle(request.payload);
            await saveRelationTranslates(request.payload);
            break;
        case 'Media':
            await saveRelationsSources(request.payload);
            await saveRelationsTags(request.payload);
            await saveRelationsMedia(request.payload);
            await saveNewMediaToFtpAndRelate(request.payload);
            await relateMediaFromFtp(request.payload);
            await saveMediaCategories(request.payload);
            break;
        case 'Interpretation':
            await addEventToTimeLine(request.payload);
            await saveMentionedVerses(request.payload);
            break;
    }
};

export default saveCustomFields;
