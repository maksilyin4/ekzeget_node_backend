import InterpretationVerse from '../../../../../../entities/InterpretationVerse';
import saveRelationVerse from './saveRelationVerse';
import { resourcesForInterpretation } from './resourcesForTest';

const interpretationVerseDelete = jest
    .spyOn(InterpretationVerse, 'deleteByInterpretationAndVerseArray')
    .mockResolvedValue();

const interpretationVerseInsert = jest
    .spyOn(InterpretationVerse, 'insertManyByInterpretationAndVerseArray')
    .mockResolvedValue();

afterAll(() => {
    jest.restoreAllMocks();
});

describe('resource Implementation', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    test('with delete and new', async () => {
        await saveRelationVerse(
            resourcesForInterpretation.deleteAndNew as Record<string, any>,
            'Interpretation'
        );

        expect(interpretationVerseDelete.mock.calls[0][0]).toBe(123);
        expect(interpretationVerseDelete.mock.calls[0][1]).toEqual(['21247', '21248']);
        expect(interpretationVerseInsert.mock.calls[0][0]).toBe(123);
        expect(interpretationVerseInsert.mock.calls[0][1]).toEqual(['36767', '36768']);
    });

    test('only delete', async () => {
        await saveRelationVerse(
            resourcesForInterpretation.onlyDelete as Record<string, any>,
            'Interpretation'
        );

        expect(interpretationVerseDelete.mock.calls[0][0]).toBe(123);
        expect(interpretationVerseDelete.mock.calls[0][1]).toEqual(['36767', '36768']);
        expect(interpretationVerseInsert).not.toBeCalled();
    });

    test('only new', async () => {
        await saveRelationVerse(
            resourcesForInterpretation.onlyNew as Record<string, any>,
            'Interpretation'
        );

        expect(interpretationVerseInsert.mock.calls[0][0]).toBe(123);
        expect(interpretationVerseInsert.mock.calls[0][1]).toEqual(['36228', '36229', '36230']);
        expect(interpretationVerseDelete).not.toBeCalled();
    });

    test('without changes', async () => {
        await saveRelationVerse(
            resourcesForInterpretation.withoutChanges as Record<string, any>,
            'Interpretation'
        );

        expect(interpretationVerseDelete).not.toBeCalled();
        expect(interpretationVerseInsert).not.toBeCalled();
    });
});
