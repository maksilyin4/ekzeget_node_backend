import { IRelationVerseForCustomComponents } from '../../../../types/IRelationVerseForCustomComponents';
import Verse from '../../../../../../entities/Verse';
import Parallel from '../../../../../../entities/Parallel';

export default async function getVerseForBibleVerse(
    id: number
): Promise<IRelationVerseForCustomComponents[]> {
    const bibleMapsPointVerseIds: Parallel[] = await Parallel.find({
        where: { parentVerseId: id },
    });

    const relationVerseForCustomComponents: IRelationVerseForCustomComponents[] = await Promise.all(
        bibleMapsPointVerseIds.map(
            async (item): Promise<IRelationVerseForCustomComponents> => {
                const { id, parentVerseId, verseId } = item;
                const { short } = await Verse.getVerseWithBookInfo(verseId);

                return {
                    id: id,
                    relationBookShortRecord: short,
                    relationVerseId: verseId,
                    relationEntityId: parentVerseId,
                };
            }
        )
    );

    return relationVerseForCustomComponents;
}
