import BibleMapsVerse from '../../../../../../entities/BibleMapsVerse';
import { IRelationVerseForCustomComponents } from '../../../../types/IRelationVerseForCustomComponents';
import Verse from '../../../../../../entities/Verse';

export default async function getVerseForBibleMapsPoints(
    id: number
): Promise<IRelationVerseForCustomComponents[]> {
    const bibleMapsPointVerseIds: BibleMapsVerse[] = await BibleMapsVerse.find({
        where: { point_id: id },
    });

    const relationVerseForCustomComponents: IRelationVerseForCustomComponents[] = await Promise.all(
        bibleMapsPointVerseIds.map(
            async (item): Promise<IRelationVerseForCustomComponents> => {
                const { id, point_id, verse_id } = item;
                const { short } = await Verse.getVerseWithBookInfo(verse_id);

                return {
                    id: id,
                    relationBookShortRecord: short,
                    relationVerseId: verse_id,
                    relationEntityId: point_id,
                };
            }
        )
    );

    return relationVerseForCustomComponents;
}
