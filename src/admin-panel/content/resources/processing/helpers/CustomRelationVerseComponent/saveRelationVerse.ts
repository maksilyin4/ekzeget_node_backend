import BibleMapsVerse from '../../../../../../entities/BibleMapsVerse';
import Parallel from '../../../../../../entities/Parallel';
import InterpretationVerse from '../../../../../../entities/InterpretationVerse';
import {ValidationError} from "admin-bro";

const saveRelationVerse = async (customFieldsData: Record<string, any>, resourceName) => {
    const entityId = customFieldsData.id;
    const oldRelationVerse: Set<number> = new Set();
    const newRelationVerse: Set<number> = new Set();
    const deletedOldRelationVerse: Set<number> = new Set();

    for (const key in customFieldsData) {
        if (key.startsWith('relationVerseForCustomComponents') && key.endsWith('relationVerseId')) {
            oldRelationVerse.add(customFieldsData[key]);
        } else if (key.startsWith('newRelationVerse') && key.endsWith('relationVerseId')) {
            newRelationVerse.add(customFieldsData[key]);
        }
    }

    oldRelationVerse.forEach(id => {
        if (!newRelationVerse.delete(id)) {
            deletedOldRelationVerse.add(id);
        }
    });

    switch (resourceName) {
        case 'BibleMapsPoints':
            if (deletedOldRelationVerse.size) {
                await BibleMapsVerse.deleteByPointAndVerseArray(entityId, [
                    ...deletedOldRelationVerse,
                ]);
            }
            await BibleMapsVerse.insertManyByPointAndVerseArray(entityId, [...newRelationVerse]);
            break;
        case 'Verse':
            if (deletedOldRelationVerse.size) {
                await Parallel.deleteByParentAndVerseArray(entityId, [...deletedOldRelationVerse]);
            }
            if (newRelationVerse.size) {
                await Parallel.insertManyByParentAndVerseArray(entityId, [...newRelationVerse]);
            }
            break;
        case 'Interpretation':
            // TODO implement centralized custom fields validation
            if (
                !newRelationVerse.size &&
                (!oldRelationVerse.size || deletedOldRelationVerse.size === oldRelationVerse.size)
            ) {
                throw new ValidationError(
                    {
                        customRelationVerseForInterpretation: {
                            message: 'Не выбран ни один стих.',
                        },
                    },
                    { message: 'Не выбран ни один стих.' }
                );
            }

            if (deletedOldRelationVerse.size) {
                await InterpretationVerse.deleteByInterpretationAndVerseArray(entityId, [
                    ...deletedOldRelationVerse,
                ]);
            }
            if (newRelationVerse.size) {
                await InterpretationVerse.insertManyByInterpretationAndVerseArray(entityId, [
                    ...newRelationVerse,
                ]);
            }
            break;
        default:
            break;
    }
};

export default saveRelationVerse;
