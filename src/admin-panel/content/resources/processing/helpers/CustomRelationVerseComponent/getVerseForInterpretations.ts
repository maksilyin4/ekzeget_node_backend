import { IRelationVerseForCustomComponents } from '../../../../types/IRelationVerseForCustomComponents';
import Verse from '../../../../../../entities/Verse';
import InterpretationVerse from '../../../../../../entities/InterpretationVerse';

export default async function getVerseForInterpretations(
    id: number
): Promise<IRelationVerseForCustomComponents[]> {
    const verseIds: InterpretationVerse[] = await InterpretationVerse.find({
        where: { interpretation_id: id },
    });

    const relationVerseForCustomComponents: IRelationVerseForCustomComponents[] = await Promise.all(
        verseIds.map(
            async (item): Promise<IRelationVerseForCustomComponents> => {
                const { id, interpretation_id, verse_id } = item;
                const { short } = await Verse.getVerseWithBookInfo(verse_id);

                return {
                    id: id,
                    relationBookShortRecord: short,
                    relationVerseId: verse_id,
                    relationEntityId: interpretation_id,
                };
            }
        )
    );

    return relationVerseForCustomComponents;
}
