import InterpretationVerse from '../../../../../../entities/InterpretationVerse';
import Verse from '../../../../../../entities/Verse';
import { IVerseForMediaAndAll } from '../../../../../../core/App/interfaces/IQuiz';
import getVerseForInterpretations from './getVerseForInterpretations';

const interpretationVerseFind = jest.spyOn(InterpretationVerse, 'find');
const verseGetVerseWithBookInfo = jest.spyOn(Verse, 'getVerseWithBookInfo');

afterAll(() => {
    jest.restoreAllMocks();
});

describe('function getVerseForInterpretations', () => {
    test('normal behavior with mock entity`s methods', async () => {
        interpretationVerseFind.mockResolvedValueOnce([
            { id: 1, interpretation_id: 1, verse_id: 1 },
            { id: 2, interpretation_id: 1, verse_id: 2 },
            { id: 3, interpretation_id: 1, verse_id: 3 },
        ] as InterpretationVerse[]);

        verseGetVerseWithBookInfo.mockImplementation((verse_id: number) => {
            return new Promise(resolve =>
                resolve({ short: 'short_' + verse_id } as IVerseForMediaAndAll)
            );
        });

        const result = await getVerseForInterpretations(1);

        expect(result).toEqual([
            {
                id: 1,
                relationBookShortRecord: 'short_1',
                relationVerseId: 1,
                relationEntityId: 1,
            },
            {
                id: 2,
                relationBookShortRecord: 'short_2',
                relationVerseId: 2,
                relationEntityId: 1,
            },
            {
                id: 3,
                relationBookShortRecord: 'short_3',
                relationVerseId: 3,
                relationEntityId: 1,
            },
        ]);
    });
});
