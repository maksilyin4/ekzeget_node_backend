import DictionaryVerse from '../../../../../../../entities/DictionaryVerse';

const saveRelationDictionaryArticle = async (customFieldsData: Record<string, any>) => {
    const verseId = customFieldsData.id;
    const oldRelationArticle: Set<number> = new Set();
    const newRelationArticle: Set<number> = new Set();
    const deletedOldRelationArticle: Set<number> = new Set();

    for (const key in customFieldsData) {
        if (key.startsWith('customRelationDictionaryArticles') && key.endsWith('id')) {
            oldRelationArticle.add(customFieldsData[key]);
        } else if (key.startsWith('newRelationDictionaryArticle') && key.endsWith('value')) {
            newRelationArticle.add(customFieldsData[key]);
        }
    }

    oldRelationArticle.forEach(id => {
        if (!newRelationArticle.delete(id)) {
            deletedOldRelationArticle.add(id);
        }
    });

    if (deletedOldRelationArticle.size) {
        await DictionaryVerse.deleteByVerseIdAndDictionaryIdArray(verseId, [
            ...deletedOldRelationArticle,
        ]);
    }

    if (newRelationArticle.size) {
        await DictionaryVerse.insertManyByVerseIdAndDictionaryIdArray(verseId, [
            ...newRelationArticle,
        ]);
    }
};

export default saveRelationDictionaryArticle;
