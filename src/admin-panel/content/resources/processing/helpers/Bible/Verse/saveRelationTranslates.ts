import DictionaryVerse from '../../../../../../../entities/DictionaryVerse';
import VerseTranslate from '../../../../../../../entities/VerseTranslate';

const saveRelationTranslates = async (customFieldsData: Record<string, any>) => {
    const verseId = customFieldsData.id;
    const oldRelationTranslates: Map<string, string> = new Map();
    const newRelationTranslates: Map<string, string> = new Map();
    const oldTranslatesToUpdate: Map<string, string> = new Map();

    for (const key in customFieldsData) {
        if (key.startsWith('oldRelationTranslate.')) {
            oldRelationTranslates.set(key.slice(key.indexOf('.') + 1), customFieldsData[key]);
        } else if (key.startsWith('newRelationTranslate')) {
            newRelationTranslates.set(key.slice(key.indexOf('.') + 1), customFieldsData[key]);
        }
    }

    oldRelationTranslates.forEach((value, key) => {
        if (newRelationTranslates.has(key)) {
            oldTranslatesToUpdate.set(key, newRelationTranslates.get(key));
            newRelationTranslates.delete(key);
        }
    });

    if (oldTranslatesToUpdate.size) {
        await VerseTranslate.updateManyByVerseIdAndCodes(verseId, oldTranslatesToUpdate);
    }

    if (newRelationTranslates.size) {
        await VerseTranslate.insertManyByVerseIdAndParams(verseId, newRelationTranslates);
    }
};

export default saveRelationTranslates;
