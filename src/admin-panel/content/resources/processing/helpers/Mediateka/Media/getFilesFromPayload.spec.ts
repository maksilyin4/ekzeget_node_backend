import getFilesFromPayload from "./getFilesFromPayload";

describe('get files info from payload', () => {
    const testPayloads: Record<string, any> = {
        'filesToFtp.0': { title: '0' },
        'filesTo.0': { title: '01' },
        'filesToFtp.1': { title: '1' },
        'fiToFtp.0': { title: '11' },
        'filesToFtp.2': { title: '2' },
        'filesToF.3': { title: '3' },
        'filesToFtp.3': { title: '4' },
    };

    test('parse Payload', async () => {
        const filesFromPayloads = await getFilesFromPayload(testPayloads, 4);
        expect(filesFromPayloads).toEqual([
            {
                title: '0',
            },
            {
                title: '1',
            },
            {
                title: '2',
            },
            {
                title: '4',
            },
        ]);
    });
})