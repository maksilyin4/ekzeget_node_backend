import IFtpFile from '../../../../../../../managers/FtpManager/types/IFtpFile';

export default async function getFilesFromPayload(
    payload: Record<string, any>,
    countFiles: number
) {
    const newFiles: IFtpFile[] = [];
    for (let index = 0; index < countFiles; index++) {
        newFiles.push(payload[`filesToFtp.${index}`]);
    }
    return newFiles;
}
