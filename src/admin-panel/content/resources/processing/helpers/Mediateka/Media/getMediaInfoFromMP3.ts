import IFtpFile from '../../../../../../../managers/FtpManager/types/IFtpFile';
import Media from '../../../../../../../entities/Media';
import EnvironmentManager from '../../../../../../../managers/EnvironmentManager/index';
import { getAudioDurationInSeconds } from 'get-audio-duration';
import * as path from 'path';

async function getMediaInfoFromMP3(
    files: IFtpFile[],
    prefixPath: string
): Promise<Array<Partial<Media>>> {
    const media = [];
    for (const file of files) {
        media.push({
            title: path.basename(file.name, '.mp3'),
            active: false,
            properties: Media.mediaFilePathAsJson(
                file.type,
                EnvironmentManager.envs.STORAGE_MEDIA_CDN + prefixPath + '/' + file.name
            ),
            type: 'A',
            created_at: Math.trunc(Date.now() / 1000),
            length: await getDuration(file.path),
            code: prefixPath + '/' + file.name,
            main_category: 14, // TODO: related media should be moved to a separate table
        });
    }
    return media;
}

export async function getDuration(path) {
    try {
        const duration = await getAudioDurationInSeconds(path);
        return getStringFromDuration(Math.trunc(duration));
    } catch (e) {
        return null;
    }
}

function getStringFromDuration(timestamp) {
    const minutes = Math.floor(timestamp / 60);
    const seconds = timestamp % 60;

    return [minutes.toString().padStart(2, '0'), seconds.toString().padStart(2, '0')].join(':');
}

export default getMediaInfoFromMP3;
