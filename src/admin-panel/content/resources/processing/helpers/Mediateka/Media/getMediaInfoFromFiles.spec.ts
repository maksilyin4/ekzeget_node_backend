import getMediaInfoFromMP3 from './getMediaInfoFromMP3';
import IFtpFile from '../../../../../../../managers/FtpManager/types/IFtpFile';
import Media from '../../../../../../../entities/Media';

describe('adminBro helper getMediaInfoFromMP3', () => {
    const dateNowSpy = jest.spyOn(Date, 'now').mockImplementation(() => 1580774400000);

    afterAll(() => {
        dateNowSpy.mockReset();
    });

    test('good mp3 and fb2 file', async () => {
        const input: Array<Partial<IFtpFile>> = [
            {
                name: 'testMp3.mp3',
                type: 'audio/mpeg',
                path: 'tests/files/Test.mp3',
            },
            {
                name: 'testFb2.fb2',
                type: 'application/x-fictionbook+xml',
                path: 'tests/files/Test.fb2',
            },
        ];

        const expected: Array<Partial<Media>> = [
            {
                active: 0,
                code: '/Test/path/testMp3.mp3',
                created_at: 1580774400,
                length: '02:04',
                main_category: 14,
                properties:
                    '{"audio_href":"https://165104.selcdn.ru/ekzeget/dev/uploads/Test/path/testMp3.mp3"}',
                title: 'testMp3.mp3',
                type: 'A',
            },
            {
                active: 1,
                code: '/Test/path/testFb2.fb2',
                created_at: 1580774400,
                length: null,
                main_category: 13,
                properties:
                    '{"fb2":"https://165104.selcdn.ru/ekzeget/dev/uploads/Test/path/testFb2.fb2","pdf":"","epub":""}',
                title: 'testFb2.fb2',
                type: 'B',
            },
        ];

        const result = await getMediaInfoFromMP3(input as IFtpFile[], '/Test/path');
        expect(result).toEqual(expected);
    });

    test('bad mp3', async () => {
        const input = [
            {
                name: 'testMp3.mp3',
                type: 'audio/mpeg',
                path: 'test/files/bad.mp3',
            },
        ] as IFtpFile[];

        const expected = [
            {
                active: 0,
                code: '/Test/path/testMp3.mp3',
                created_at: 1580774400,
                length: null,
                main_category: 14,
                properties:
                    '{"audio_href":"https://165104.selcdn.ru/ekzeget/dev/uploads/Test/path/testMp3.mp3"}',
                title: 'testMp3.mp3',
                type: 'A',
            },
        ];

        const result = await getMediaInfoFromMP3(input, '/Test/path');
        expect(result).toEqual(expected);
    });
});
