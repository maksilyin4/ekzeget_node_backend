import MediaRelated from '../../../../../../../entities/MediaRelated';
import getArrayRelationRecords from '../../getArrayRelationRecords';
import MediaAttachments from '../../MediaAttachments';

type MediaRelatedMainFields = Pick<MediaRelated, 'related_id' | 'media_id' | 'sort'>;

const saveRelationsMedia = async (customFieldsData: Record<string, any>) => {
    const mainMediaId = customFieldsData.id;

    const oldRelations: MediaRelatedMainFields[] = getArrayRelationRecords(
        { prefix: 'informationAboutRelatedRecords', postfix: 'related_id' },
        customFieldsData,
        getOneRecord
    );
    const newRelations: MediaRelatedMainFields[] = getArrayRelationRecords(
        { prefix: 'newRelatedRecords', postfix: 'sort' },
        customFieldsData,
        getOneRecord
    );

    const { addedAttachments, deletedAttachmentsIds } = new RelatedMedia(
        oldRelations
    ).getDifference(newRelations);

    if (deletedAttachmentsIds.length) {
        await MediaRelated.deleteGroupByIds(deletedAttachmentsIds, mainMediaId);
    }
    if (addedAttachments.length) {
        await MediaRelated.insertMany(mainMediaId, addedAttachments);
    }
};

export default saveRelationsMedia;

function getOneRecord(prefix, customFieldsData: Record<string, any>): MediaRelatedMainFields {
    return {
        related_id: customFieldsData[`${prefix}.related_id`] || customFieldsData[`${prefix}.value`],
        sort: customFieldsData[`${prefix}.sort`] || null,
        media_id: customFieldsData.id || null,
    };
}

class RelatedMedia extends MediaAttachments<MediaRelatedMainFields> {
    idProp = 'related_id';

    getKey(attachment): string {
        return attachment.related_id + attachment.sort;
    }
}
