import IFtpFile from '../../../../../../../managers/FtpManager/types/IFtpFile';
import FtpManager from '../../../../../../../managers/FtpManager';
import Media from '../../../../../../../entities/Media';
import getMediaInfoFromMP3 from './getMediaInfoFromMP3';
import MediaRelated from '../../../../../../../entities/MediaRelated';
import getFilesFromPayload from './getFilesFromPayload';

const saveNewMediaToFtpAndRelate = async (customFieldsData: Record<string, any>) => {
    if (!checkingFtpProperties(customFieldsData)) {
        return;
    }
    const mainMediaId = customFieldsData.id;

    const lastRelatedMediaSort = await MediaRelated.getLastSort(mainMediaId);
    const ftpPath = customFieldsData.ftpPath;
    const countFiles = customFieldsData.filesToFtpCounts;

    const newFiles: IFtpFile[] = await getFilesFromPayload(customFieldsData, countFiles);

    new FtpManager().uploadMediaFilesToStore(newFiles, ftpPath)
        .then(async () => {
            const mediaInfo = await getMediaInfoFromMP3(
                newFiles.filter(file => file.type === 'audio/mpeg'),
                ftpPath
            );
            const insertedIds = await Media.insertManyAndGetId(mediaInfo);
            await MediaRelated.insertMany(
                mainMediaId,
                insertedIds.map(
                    (id, index): Partial<MediaRelated> => ({
                        media_id: mainMediaId,
                        related_id: id,
                        sort: lastRelatedMediaSort + index,
                    })
                )
            );
        })
        .catch(async e => {
            console.error(`Ошибка сохранения файлов в папку ${ftpPath}: ` + e.message);
        });
};

export default saveNewMediaToFtpAndRelate;

function checkingFtpProperties(customFieldsData) {
    return (
        customFieldsData.hasOwnProperty('ftpPath') &&
        customFieldsData.hasOwnProperty('filesToFtpCounts')
    );
}
