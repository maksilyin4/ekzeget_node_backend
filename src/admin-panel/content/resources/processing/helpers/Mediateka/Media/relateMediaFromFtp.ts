import Media from '../../../../../../../entities/Media';
import { getDuration } from './getMediaInfoFromMP3';
import MediaRelated from '../../../../../../../entities/MediaRelated';
import path from 'path';
import ftpManager from '../../../../../../../managers/FtpManager';

export default async function(customFieldsData: Record<string, any>) {
    if (!customFieldsData['remoteFilesToRelatedMedia' + '.0']) {
        return;
    }
    const mainMediaId = customFieldsData.id;

    const lastRelatedMediaSort = await MediaRelated.getLastSort(mainMediaId);

    const mediaInfo: Partial<Media>[] = [];
    const foldersToCdnUrlsMap = new Map();
    for (let i = 0; ; i++) {
        const ftpPath = customFieldsData[`remoteFilesToRelatedMedia.${i}`];
        if (!ftpPath) {
            break;
        }
        const [_, container, ...filePath] = ftpPath.split('/');

        if (!foldersToCdnUrlsMap.has(container)) {
            foldersToCdnUrlsMap.set(
                container,
                await new ftpManager().getFileContents(path.join(container, '.url'))
            );
        }
        const cdnUrl = foldersToCdnUrlsMap.get(container);
        const fileWebUrl = cdnUrl + '/' + path.join(...filePath);

        mediaInfo.push({
            title: path.basename(ftpPath, '.mp3'),
            active: 0,
            properties: Media.mediaFilePathAsJson('audio/mpeg', fileWebUrl),
            type: 'A',
            created_at: Math.trunc(Date.now() / 1000),
            length: await getDuration(fileWebUrl),
            code: ftpPath,
            main_category: 14,
        });
    }

    const insertedIds = await Media.insertManyAndGetId(mediaInfo);
    await MediaRelated.insertMany(
        mainMediaId,
        insertedIds.map(
            (id, index): Partial<MediaRelated> => ({
                media_id: mainMediaId,
                related_id: id,
                sort: lastRelatedMediaSort + index,
            })
        )
    );
}
