import { IRelationForMedia } from '../../../../../types/IRelationForMedia';
import MediaBible from '../../../../../../../entities/MediaBible';
import getArrayRelationRecords from '../../getArrayRelationRecords';
import MediaAttachments from '../../MediaAttachments';

const saveRelationsSources = async (customFieldsData: Record<string, any>) => {
    const entityId = customFieldsData.id;

    const oldRelations: IRelationForMedia[] = getArrayRelationRecords(
        { prefix: 'customRelationForMedia', postfix: 'id' },
        customFieldsData,
        getOneRecord
    );

    const newRelations: IRelationForMedia[] = getArrayRelationRecords(
        { prefix: 'newRelationsForMedia', postfix: 'id' },
        customFieldsData,
        getOneRecord
    );

    const { addedAttachments, deletedAttachmentsIds } = new BibleFragments(
        oldRelations
    ).getDifference(newRelations);

    if (deletedAttachmentsIds.length) {
        await MediaBible.deleteGroupByIds(deletedAttachmentsIds);
    }
    if (addedAttachments.length) {
        await MediaBible.insertMany(entityId, addedAttachments);
    }
};

export default saveRelationsSources;

function compareRelationship(oldRelation: IRelationForMedia, newRelation: IRelationForMedia) {
    return (
        oldRelation.relationBookShortRecord === newRelation.relationBookShortRecord &&
        oldRelation.relationVerseId === newRelation.relationVerseId &&
        oldRelation.relationChapterNumber === newRelation.relationChapterNumber &&
        oldRelation.relationVerseNumber === newRelation.relationVerseNumber
    );
}

function getOneRecord(prefix, customFieldsData: Record<string, any>) {
    return {
        id: customFieldsData[`${prefix}.id`],
        relationChapterNumber: customFieldsData[`${prefix}.relationChapterNumber`] || null,
        relationBookShortRecord: customFieldsData[`${prefix}.relationBookShortRecord`],
        relationVerseNumber: customFieldsData[`${prefix}.relationVerseNumber`] || null,
        relationEntityId: customFieldsData[`${prefix}.relationEntityId`] || null,
        relationVerseId: customFieldsData[`${prefix}.relationVerseId`] || null,
    };
}

class BibleFragments extends MediaAttachments<IRelationForMedia> {
    getKey(attachment): string {
        return (
            attachment.relationBookShortRecord +
            attachment.relationVerseId +
            attachment.relationChapterNumber +
            attachment.relationVerseNumber
        );
    }
}
