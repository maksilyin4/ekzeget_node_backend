import MediaTags from '../../../../../../../entities/MediaTags';
import getArrayRelationRecords from '../../getArrayRelationRecords';

const saveRelationsTags = async (customFieldsData: Record<string, any>) => {
    const mediaId = customFieldsData.id;

    const oldRelationTags: Set<number> = new Set(
        getArrayRelationRecords(
            { prefix: 'customRelatedTags', postfix: 'id' },
            customFieldsData,
            getOneRecord
        )
    );

    const newRelationTags: Set<number> = new Set(
        getArrayRelationRecords(
            { prefix: 'newCustomRelatedTags', postfix: 'value' },
            customFieldsData,
            getOneRecord
        )
    );

    const deletedOldRelationTags = getRelationsTagsToDelete(newRelationTags, oldRelationTags);

    if (deletedOldRelationTags.size) {
        await MediaTags.deleteByMediaIdAndTagsIdArray(mediaId, [...deletedOldRelationTags]);
    }

    if (newRelationTags.size) {
        await MediaTags.insertManyByMediaIdAndTagsIdArray(mediaId, [...newRelationTags]);
    }
};

export default saveRelationsTags;

function getOneRecord(prefix, customFieldsData: Record<string, any>): number {
    return customFieldsData[`${prefix}.id`] || customFieldsData[`${prefix}.value`];
}

function getRelationsTagsToDelete(newRelationTags: Set<number>, oldRelationTags: Set<number>) {
    const deletedOldRelationTags: Set<number> = new Set();

    oldRelationTags.forEach(id => {
        if (!newRelationTags.delete(id)) {
            deletedOldRelationTags.add(id);
        }
    });

    return deletedOldRelationTags;
}
