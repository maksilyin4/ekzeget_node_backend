import MediaCategories from '../../../../../../../entities/MediaCategories';

interface GetCategoriesArrayParams {
    prefix: string;
    postfixes: string[];
    payload: Object;
}

interface GetCategoriesArrayResult {
    id: string;
    title: string;
    selected: string;
}

const getCategoriesArray = ({
    prefix,
    postfixes,
    payload,
}: GetCategoriesArrayParams): GetCategoriesArrayResult[] => {
    const matched: GetCategoriesArrayResult[] = [];
    for (const key in payload) {
        const match = key.startsWith(prefix) && postfixes.some(postfix => key.endsWith(postfix));
        if (match) {
            const [prefix, index, prop] = key.split('.');
            if (!matched[index]) matched[index] = {};
            matched[index][prop] = payload[key];
        }
    }

    return matched;
};

export const saveMediaCategories = async (payload: any): Promise<void> => {
    const { id } = payload;
    const converted = getCategoriesArray({
        prefix: 'newCustomCat',
        postfixes: ['id', 'title', 'selected'],
        payload,
    });

    await MediaCategories.delete({ media_id: id });

    const promises: Promise<MediaCategories>[] = converted
        .filter(item => item.selected === 'true')
        .map(async category =>
            MediaCategories.create({
                media_id: id,
                category_id: +category.id,
            }).save()
        );

    await Promise.all(promises);
};
