import Media from '../../../../../../../entities/Media';
import { FB2Chapter, FB2Parser } from '../../../../../../../libs/FB2Parser';
import { In } from 'typeorm';
import MediaRelated from '../../../../../../../entities/MediaRelated';
import axios from 'axios';

export const saveFb2Content = async (media: Media): Promise<void> => {
    try {
        const filepath = getFilepath(media);
        if (!filepath) {
            return;
        }

        const res = await axios.get(filepath);
        await updateFb2Chapters(media, getBookChapters(res.data));
    } catch (e) {
        console.error(`SaveFb2Content: ${e.message}`);
    }
};

const getBookChapters = (content: string) => {
    const parser = new FB2Parser(content).parse();
    return parser.getChapters();
};

function getFilepath(payload: any): string {
    if (!payload.properties) {
        return;
    }

    let pathToFb2 = null;
    try {
        const parsedProps = JSON.parse(payload.properties);
        pathToFb2 = parsedProps.fb2;
    } catch (e) {
        console.error('SaveFB2Content: unable to parse properties');
    }

    return pathToFb2;
}

const deleteOldFb2Chapters = async (media: Media): Promise<void> => {
    const oldFb2Chapters = await MediaRelated.createQueryBuilder('mr')
        .leftJoinAndSelect('media', 'm', 'mr.related_id = m.id')
        .where('m.type = :type', { type: 'C' })
        .andWhere('mr.media_id = :id', { id: media.id })
        .getMany();

    const idsToDelete = oldFb2Chapters.map(({ related_id }) => related_id);

    await Media.delete({ id: In(idsToDelete) });
};

const saveNewFb2Chapters = async (media: Media, chapters: FB2Chapter[]): Promise<void> => {
    const chapterCodes = [];
    for (let i = 0; i < chapters.length; i++) {
        const mediaChapter = Media.create({
            title: chapters[i].title,
            code: `${media.code}-chapter-${i + 1}`,
            text: chapters[i].html,
            type: 'C',
            main_category: 62, // TODO: related media should be moved to a separate table
            sort: i,
        });
        await mediaChapter.save();
        chapterCodes.push(mediaChapter.code);
    }

    const freshChapters = await Media.find({
        where: { code: In(chapterCodes) },
        order: { id: 'ASC' },
    });

    for (const { id: related_id, sort } of freshChapters) {
        await MediaRelated.insert({
            media_id: media.id,
            related_id,
            sort,
        });
    }
};

const updateFb2Chapters = async (media: Media, chapters: FB2Chapter[]): Promise<void> => {
    await deleteOldFb2Chapters(media);
    await saveNewFb2Chapters(media, chapters);
};
