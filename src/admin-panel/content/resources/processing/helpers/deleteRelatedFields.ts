import { ActionRequest } from 'admin-bro';
import QueezeAnswers from '../../../../../entities/QueezeAnswers';
import VerseTranslate from '../../../../../entities/VerseTranslate';
import DictionaryVerse from '../../../../../entities/DictionaryVerse';
import Parallel from '../../../../../entities/Parallel';
import BibleMapsVerse from "../../../../../entities/BibleMapsVerse";

const deleteRelatedFields = async (request: ActionRequest): Promise<void> => {
    const recordId = request.params.recordId;
    switch (request.params.resourceId) {
        case 'QueezeQuestions':
            await QueezeAnswers.deleteGroupByQuestionId(recordId);
            break;
        case 'Verse':
            const relationDelete: Array<Promise<void>> = [
                VerseTranslate.deleteGroupByVerseId(recordId),
                DictionaryVerse.deleteGroupByVerseId(recordId),
                Parallel.deleteGroupByVerseId(recordId),
            ];
            await Promise.all(relationDelete);
            break;
        case 'BibleMapsPoints':
            await BibleMapsVerse.deleteGroupByPointId(recordId);
            break;
        default:
            break;
    }
};

export default deleteRelatedFields;
