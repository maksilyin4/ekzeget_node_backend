export default class MediaAttachments<T> {
    attachments: T[] = [];
    idProp = 'id';

    constructor(attachments = []) {
        this.attachments = attachments;
    }

    getKey(attachment: T): string {
        throw new Error('the method must be implemented');
    }

    getDifference(newAttachments: T[]): { addedAttachments: T[]; deletedAttachmentsIds: number[] } {
        const deletedAttachmentsIds = [];
        const newAttachmentsMap = {};

        for (const newAttachment of newAttachments) {
            newAttachmentsMap[this.getKey(newAttachment)] = newAttachment;
        }

        for (const attachment of this.attachments) {
            if (newAttachmentsMap[this.getKey(attachment)]) {
                delete newAttachmentsMap[this.getKey(attachment)];
            } else {
                deletedAttachmentsIds.push(attachment[this.idProp]);
            }
        }

        return {
            addedAttachments: Object.values(newAttachmentsMap),
            deletedAttachmentsIds: deletedAttachmentsIds,
        };
    }
}
