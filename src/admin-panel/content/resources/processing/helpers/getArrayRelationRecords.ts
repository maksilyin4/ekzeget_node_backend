export default function getArrayRelationRecords(
    searchingString: { prefix: string; postfix: string },
    customFieldsData: Record<string, any>,
    getOneRecord: (prefix: string, customFieldsData: Record<string, any>) => any
) {
    const relations = [];
    let continueSearchingRelations = true;
    for (let index = 0; continueSearchingRelations; index++) {
        continueSearchingRelations = false;
        if (
            customFieldsData.hasOwnProperty(
                `${searchingString.prefix}.${index}.${searchingString.postfix}`
            )
        ) {
            relations.push(getOneRecord(`${searchingString.prefix}.${index}`, customFieldsData));
            continueSearchingRelations = true;
        }
    }
    return relations;
}
