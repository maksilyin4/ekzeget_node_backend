import _ from 'lodash';
import AdminBroModule, { ResourceOptions } from 'admin-bro';
import { PropertyType } from 'admin-bro/src/backend/adapters/property/base-property';

import lodashConcatArraysCustomizer from '../../../libs/helpers/lodashConcatArraysCustomizer';

import createRichTextConf from './processing/propertyProcessing/createRichTextConf';
import list from './processing/actions/list';
import show from './processing/actions/show';
import add from './processing/actions/new';
import edit from './processing/actions/edit';

import { EntityType } from '../types/EntityType';
import del from './processing/actions/delete';
import administratorOnlyAccessRules from '../../auth/administratorOnlyAccessRules';
import path from 'path';

export default class ResourceBase {
    constructor(public resource: EntityType, mergedOptions: ResourceOptions) {
        this.options = _.mergeWith(
            _.merge({}, this.options),
            mergedOptions,
            lodashConcatArraysCustomizer
        );
    }

    public options: ResourceOptions = {
        listProperties: ['id'],
        showProperties: ['id'],
        filterProperties: ['id'],
        editProperties: [],

        properties: {
            title: { isRequired: true },
            name: { isRequired: true },
            sort: { isRequired: true },

            status: { type: 'boolean' as PropertyType },
            active: { type: 'boolean' as PropertyType },

            text: createRichTextConf(),
            //description: createRichTextConf(),
            content: createRichTextConf(),
            body: createRichTextConf(),
            info: createRichTextConf(),
            comment: createRichTextConf(),
            image_id: {
                components: {
                    list: AdminBroModule.bundle(path.resolve(__dirname, './processing/components/ImageArea/index.tsx')),
                    show: AdminBroModule.bundle(path.resolve(__dirname, './processing/components/ImageArea/index.tsx')),
                    edit: AdminBroModule.bundle(path.resolve(__dirname, './processing/components/ImageLoader/index.tsx')),
                },
            },
            image: {
                components: {
                    list: AdminBroModule.bundle(path.resolve(__dirname,'./processing/components/ImageArea/index.tsx')),
                    show: AdminBroModule.bundle(path.resolve(__dirname,'./processing/components/ImageArea/index.tsx')),
                    edit: AdminBroModule.bundle(path.resolve(__dirname, './processing/components/ImageLoader/index.tsx')),
                },
            },
            foto: {
                components: {
                    list: AdminBroModule.bundle(path.resolve(__dirname,'./processing/components/ImageArea/index.tsx')),
                    show: AdminBroModule.bundle(path.resolve(__dirname,'./processing/components/ImageArea/index.tsx')),
                    edit: AdminBroModule.bundle(path.resolve(__dirname, './processing/components/ImageLoader/index.tsx')),
                },
            },
        },

        actions: _.merge(
            {
                list,
                show,
                edit,
                new: add,
                delete: del,
            },
            administratorOnlyAccessRules
        ),
    };
}
