import MediaCategory from '../../../../../../entities/MediaCategory';
import ResourceBase from '../../../ResourceBase';
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';

export default new ResourceBase(MediaCategory, {
    listProperties: ['title', 'description', 'image', 'created_by', 'active'],
    showProperties: [
        'title',
        'description',
        'image',
        'created_by',
        'edited_by',
        'created_at',
        'edited_at',
        'parent_id',
        'code',
        'active',
    ],
    filterProperties: ['title', 'created_by', 'active'],
    editProperties: ['title', 'description', 'image', 'parent_id', 'active'],
    properties: {
        parent_id: {
            reference: 'MediaCategory',
        },
        image_field: {
            isVisible: false,
            props: {
                value: 'image',
            },
        },
    },

    actions: relaxedAccessRules,
});
