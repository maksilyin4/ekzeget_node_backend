import MediaAuthor from '../../../../../../entities/MediaAuthor';
import ResourceBase from '../../../ResourceBase';
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';

export default new ResourceBase(MediaAuthor, {
    listProperties: ['title', 'created_by', 'created_at', 'category_id', 'active'],
    showProperties: [
        'title',
        'description',
        'image',
        'created_by',
        'edited_by',
        'created_by',
        'created_at',
        'edited_at',
        'category_id',
        'active',
    ],
    filterProperties: ['title', 'created_by', 'edited_by', 'category_id', 'active'],
    editProperties: ['title', 'description', 'image', 'category_id', 'active'],
    properties: {
        image_field: {
            isVisible: false,
            props: {
                value: 'image',
            },
        },
    },
    actions: relaxedAccessRules,
});
