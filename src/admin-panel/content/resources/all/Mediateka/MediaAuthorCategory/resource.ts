import MediaAuthorCategory from '../../../../../../entities/MediaAuthorCategory';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(MediaAuthorCategory, {
    listProperties: ['title', 'created_at', 'status'],
    showProperties: ['title', 'slug', 'body', 'created_at', 'updated_at', 'status'],
    filterProperties: ['title', 'slug', 'status'],
    editProperties: ['title', 'slug', 'body', 'status'],
});
