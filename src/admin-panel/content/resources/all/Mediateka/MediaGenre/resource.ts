import MediaGenre from '../../../../../../entities/MediaGenre';
import ResourceBase from '../../../ResourceBase';
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';

export default new ResourceBase(MediaGenre, {
    listProperties: ['title', 'created_by', 'created_at', 'active'],
    showProperties: [
        'title',
        'description',
        'image',
        'created_by',
        'edited_by',
        'created_at',
        'edited_at',
        'code',
        'active',
    ],
    filterProperties: ['title', 'created_by', 'edited_by', 'active'],
    editProperties: ['title', 'description', 'image', 'active'],
    properties: {
        image_field: {
            isVisible: false,
            props: {
                value: 'image',
            },
        },
    },

    actions: relaxedAccessRules,
});
