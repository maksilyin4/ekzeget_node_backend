import ResourceBase from '../../../ResourceBase';
import MediaTag from '../../../../../../entities/MediaTag';

export default new ResourceBase(MediaTag, {
    listProperties: ['id', 'title', 'active'],
    showProperties: ['id', 'title', 'created_at', 'editedAt', 'code', 'active'],
    filterProperties: ['id', 'title', 'active'],
    editProperties: ['title', 'active'],
});
