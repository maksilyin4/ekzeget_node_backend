export default {
    translations: {
        labels: {
            Media: 'Медиа',
        },
        properties: {
            social_description: 'Описание для соцсетей',
            allow_comment: 'Комментирование',
            length: 'Продолжительность',
            main_category: 'Основная категория',
            location: 'Локация',
            era: 'Эра',
            properties: 'Свойства',
            template: 'Шаблон',
            created_at: 'Дата создания',
            grouping_name: 'Имя для группировки',
            daily_media: 'Дата видео на каждый день',
            celebration_id: 'Праздник на каждый день',
            reading_apostolic_id: 'Апостольское чтение',
            reading_gospel_id: 'Евангелие',
            reading_post_id: 'Постное чтение',
            reading_morning_id: 'Утреннее чтение (день)',
            mineja_id: 'Минея',
        },
    },
};
