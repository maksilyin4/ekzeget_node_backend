import OrigMedia from '../../../../../../entities/Media';
import { PropertyType } from 'admin-bro/src/backend/adapters/property/base-property';
import ResourceBase from '../../../ResourceBase';
import getActualLinkForInquiries from '../../../processing/helpers/getActualLinkForInquiries';
import AdminBro from 'admin-bro';
import { FindManyOptions, In, Not } from 'typeorm';
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';
import path from 'path';

// it is a hack class that filters related media out of admin list
// TODO dedicate a separate table/entity to related media
// @ts-ignore
class Media extends OrigMedia {
    static find(options: FindManyOptions = {}) {
        if (!options.where || Object.keys(options.where).length === 0) {
            options.where = { type: Not(In(['A', 'C'])) };
        }

        return OrigMedia.find(options);
    }

    static getRepository() {
        return OrigMedia.getRepository();
    }
}

// @ts-ignore
export default new ResourceBase(Media, {
    listProperties: ['title', 'author_id', 'genre_id', 'created_at', 'active', 'allow_comment', 'daily_media', 'celebration_id'],
    showProperties: [
        'title',
        'text',
        'social_description',
        'image_id',
        'type',
        'is_primary',
        'template',
        'author_id',
        'year',
        'location',
        'era',
        'art_title',
        'art_created_date',
        'art_director',
        'main_category',
        'customCat',
        'customLinksToMediaResource',
        'length',
        'sort',
        'genre_id',
        'grouping_name',
        'customRelationForMedia',
        'customRelatedTags',
        'allow_comment',
        'active',
        'customLinkToOtherRecords',
        'daily_media',
        'celebration_id',
        'reading_apostolic_id',
        'reading_gospel_id',
        'reading_post_id',
        'reading_morning_id',
        'mineja_id',
    ],
    filterProperties: [
        'title',
        'author_id',
        'genre_id',
        'created_by',
        'edited_by',
        'active',
        'allow_comment',
        'year',
        'type',
        'location',
        'era',
        'daily_media',
        'celebration_id',
    ],
    editProperties: [
        'title',
        'text',
        'social_description',
        'image_id',
        'type',
        'is_primary',
        'template',
        'author_id',
        'new_author_modal',
        'year',
        'location',
        'era',
        'art_title',
        'art_created_date',
        'art_director',
        'main_category',
        'customCat',
        'customLinksToMediaResource',
        'length',
        'sort',
        'genre_id',
        'grouping_name',
        'customRelationForMedia',
        'customRelatedTags',
        'allow_comment',
        'active',
        'customLinkToOtherRecords',
        'customFtp',
        'customFtpFilesChooser',
        'daily_media',
        'celebration_id',
        'reading_apostolic_id',
        'reading_gospel_id',
        'reading_post_id',
        'reading_morning_id',
        'mineja_id',
    ],
    properties: {
        social_description: {
            type: 'textarea',
        },
        allow_comment: {
            type: 'boolean' as PropertyType,
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/CheckBox/index.tsx')),
            },
            props: {
                defaultValue: true,
            },
        },
        active: {
            type: 'boolean' as PropertyType,
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/CheckBox/index.tsx')),
            },
            props: {
                defaultValue: true,
            },
        },
        is_primary: {
            type: 'boolean' as PropertyType,
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/CheckBox/index.tsx')),
            },
            props: {
                defaultValue: true,
            },
        },
        type: {
            availableValues: [
                { value: 'A', label: 'Аудио' },
                { value: 'B', label: 'Книга' },
                { value: 'V', label: 'Видео' },
                { value: 'C', label: 'Глава книги' },
                { value: 'I', label: 'Изображение' },
            ],
        },
        template: {
            availableValues: [
                { value: 'I1', label: 'Мотиватор' },
                { value: 'I2', label: 'Изображение (горизонтальный)' },
                { value: 'M', label: 'Основной шаблон (вертикальный)' },
                { value: 'V1', label: 'Видео' },
            ],
        },
        new_author_modal: {
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Mediateka/Media/author/index.tsx')),
            },
        },
        sort: {
            isRequired: false,
            props: {
                placeholder: '200',
            },
        },
        title: {
            isRequired: true,
        },
        main_category: {
            isRequired: true,
        },
        customCat: {
            components: {
                show: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/EmptyTemplate/index.tsx')),
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Mediateka/Media/customCat/index.tsx')),
            },
        },
        customRelationForMedia: {
            components: {
                show: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/EmptyTemplate/index.tsx')),
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Mediateka/Media/customRelationForMedia/index.tsx')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },
        customFtp: {
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Mediateka/Media/customFtp/Uploader.tsx')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },
        customFtpFilesChooser: {
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Mediateka/Media/customFtp/FilesChooser.tsx')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },
        customLinkToOtherRecords: {
            components: {
                show: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/EmptyTemplate/index.tsx')),
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Mediateka/Media/customLinkToOtherRecords/index.tsx')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },
        customRelatedTags: {
            components: {
                show: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/EmptyTemplate/index.tsx')),
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Mediateka/Media/customRelatedTags/index.tsx')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },
        customLinksToMediaResource: {
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Mediateka/Media/customLinksToMediaResource/index.tsx')),
            },
        },
        image_field: {
            isVisible: false,
            props: {
                value: 'image_id',
            },
        },
        daily_media: {
            props: {propertyType: 'date'},
            components: {
                list: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/DateField/index.tsx')),
                show: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/DateField/index.tsx')),
            },
            isSortable: false,
        },
        celebration_id: {
            reference: 'Celebration',
        },
        reading_apostolic_id: {
            reference: 'ApostolicReading',
        },
        reading_gospel_id: {
            reference: 'GospelReading',
        },
        reading_post_id: {
            reference: 'PostReading',
        },
        reading_morning_id: {
            reference: 'MorningReading',
        },
        mineja_id: {
            reference: 'Mineja',
        },
    },

    actions: relaxedAccessRules,
});
