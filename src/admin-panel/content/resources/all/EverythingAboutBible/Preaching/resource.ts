import Preaching from '../../../../../../entities/Preaching';
import ResourceBase from '../../../ResourceBase';
import Excuse from '../OcassionForPreaching/resource';
import Preacher from '../Preacher/resource';

export default new ResourceBase(Preaching, {
    listProperties: ['preacher_id', 'excuse_id', 'theme', 'active'],
    showProperties: ['preacher_id', 'excuse_id', 'theme', 'text', 'code', 'active'],
    filterProperties: ['preacher_id', 'excuse_id', 'theme', 'active'],
    editProperties: ['preacher_id', 'excuse_id', 'theme', 'text', 'active'],
    properties: {
        excuse_id: {
            reference: 'Excuse',
        },
        preacher_id: {
            reference: 'Preacher',
        },
    },
});
