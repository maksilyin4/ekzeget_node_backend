export default {
    translations: {
        labels: {
            Preaching: 'Проповеди',
        },
        properties: {
            preacher_id: 'Проповедник',
            excuse_id: 'Повод',
            theme: 'Тема',
        },
    },
};
