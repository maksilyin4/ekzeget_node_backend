import EkzegetType from '../../../../../../entities/EkzegetType';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(EkzegetType, {
    listProperties: ['title', 'active'],
    showProperties: ['code', 'title', 'active'],
    filterProperties: ['title', 'active'],
    editProperties: ['title', 'active'],
});
