import ResourceBase from '../../../ResourceBase';
import Celebration from '../../../../../../entities/Celebration';

export default new ResourceBase(Celebration, {
    listProperties: ['title', 'excuse_id', 'active'],
    showProperties: ['title', 'excuse_id', 'active'],
    filterProperties: ['title', 'excuse_id', 'active'],
    editProperties: ['title', 'excuse_id', 'active'],
    properties: {
        excuse_id: {
            isSortable: true,
        },
    },
});
