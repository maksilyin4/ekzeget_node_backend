import Lecture from '../../../../../../entities/Lecture';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(Lecture, {
    listProperties: ['title', 'type_id', 'added_at', 'active'],
    showProperties: [
        'title',
        'description',
        'text',
        'type_id',
        'added_at',
        'edited_at',
        'code',
        'active',
    ],
    filterProperties: ['title', 'type_id', 'active'],
    editProperties: ['title', 'description', 'text', 'type_id', 'active'],
});
