import Preacher from '../../../../../../entities/Preacher';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(Preacher, {
    listProperties: ['name', 'active'],
    showProperties: ['code', 'name', 'active'],
    filterProperties: ['name', 'active'],
    editProperties: ['name', 'active'],
});
