export default {
    translations: {
        labels: {
            EkzegetPerson: 'Толкователи',
        },
        properties: {
            ekzeget_type_id: 'Тип толкователя',
        },
    },
};
