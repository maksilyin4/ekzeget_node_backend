import EkzegetPerson from '../../../../../../entities/EkzegetPerson';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(EkzegetPerson, {
    listProperties: ['name', 'century', 'ekzeget_type_id', 'active'],
    showProperties: ['name', 'century', 'info', 'ekzeget_type_id', 'code', 'active'],
    filterProperties: ['name', 'century', 'ekzeget_type_id', 'active'],
    editProperties: ['name', 'century', 'info', 'ekzeget_type_id', 'active'],
    properties: {
        info: {
            isRequired: true,
        },
    },
});
