import DictionaryType from '../../../../../../entities/DictionaryType';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(DictionaryType, {
    listProperties: ['title', 'active'],
    showProperties: ['title', 'alias', 'code', 'active'],
    filterProperties: ['title', 'alias', 'active'],
    editProperties: ['title', 'alias', 'active'],
});
