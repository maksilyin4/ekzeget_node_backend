export default {
    translations: {
        labels: {
            DictionaryType: 'Словари',
        },
        properties: {
            preacher_id: 'Проповедник',
            excuse_id: 'Повод',
            theme: 'Тема',
        },
    },
};
