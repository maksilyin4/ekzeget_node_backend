import ResourceBase from '../../../ResourceBase';
import Excuse from '../../../../../../entities/Excuse';

export default new ResourceBase(Excuse, {
    listProperties: ['title', 'active', 'sort'],
    showProperties: ['title', 'gospel', 'apostolic', 'sort', 'active'],
    filterProperties: ['title', 'active', 'sort'],
    editProperties: ['title', 'gospel', 'apostolic', 'parent_id', 'sort', 'active'],
    properties: {
        sort: {
            isSortable: true,
        },
        parent_id: {
            reference: 'CelebrationExcuse',
        },
    },
});
