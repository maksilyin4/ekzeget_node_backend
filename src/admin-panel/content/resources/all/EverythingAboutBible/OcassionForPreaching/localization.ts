export default {
    translations: {
        labels: {
            Excuse: 'Поводы для проповедей',
        },
        properties: {
            apostolic: 'Апостол',
        },
    },
};
