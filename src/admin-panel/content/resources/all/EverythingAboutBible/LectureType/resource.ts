import LectureType from '../../../../../../entities/LectureType';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(LectureType, {
    listProperties: ['title', 'active'],
    showProperties: ['title', 'code', 'active'],
    filterProperties: ['title', 'active'],
    editProperties: ['title', 'active'],
});
