import ResourceBase from '../../../ResourceBase';
import CelebrationExcuse from '../../../../../../entities/CelebrationExcuse';

export default new ResourceBase(CelebrationExcuse, {
    listProperties: ['title', 'active'],
    showProperties: ['title', 'active'],
    filterProperties: ['title', 'active'],
    editProperties: ['title', 'active'],
});
