import ResourceBase from '../../../ResourceBase';
import BibleMapsPoints from '../../../../../../entities/BibleMapsPoints';
import AdminBro from 'admin-bro';
import getActualLinkForInquiries from '../../../processing/helpers/getActualLinkForInquiries';
import path from 'path';

export default new ResourceBase(BibleMapsPoints, {
    listProperties: ['title', 'description', 'active'],
    showProperties: [
        'title',
        'description',
        'image_id',
        'lon',
        'lat',
        'created_by',
        'edited_by',
        'created_at',
        'edited_at',
        'customRelationVerseForBibleMapsPoint',
        'active',
    ],
    filterProperties: ['title', 'description', 'active'],
    editProperties: [
        'title',
        'description',
        'imageUploader',
        'lon',
        'lat',
        'customRelationVerseForBibleMapsPoint',
        'active',
    ],
    properties: {
        imageUploader: {
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/ImageLoader/index.tsx')),
            },
        },
        customRelationVerseForBibleMapsPoint: {
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/RelationVerse/index')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },
    },
});
