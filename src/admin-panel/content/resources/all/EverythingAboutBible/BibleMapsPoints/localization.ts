export default {
    translations: {
        labels: {
            BibleMapsPoints: 'Точки на библейской карте',
        },
        properties: {
            customRelationVerseForBibleMapsPoint: 'Связанные стихи',
        },
    },
};
