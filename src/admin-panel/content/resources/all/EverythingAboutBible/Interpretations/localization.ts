export default {
    translations: {
        labels: {
            Interpretation: 'Толкования',
        },
        properties: {
            ekzeget_id: 'Экзегет',
            investigated: 'Исследование',
            interpretationVerseBook: 'Книга',
            interpretationVerseChapter: 'Глава',
            interpretationVerseNumber: 'Стих',
        },
    },
};
