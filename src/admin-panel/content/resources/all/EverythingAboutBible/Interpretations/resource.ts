import ResourceBase from '../../../ResourceBase';
import Interpretation from '../../../../../../entities/Interpretation';
import AdminBro from 'admin-bro';
import getActualLinkForInquiries from '../../../processing/helpers/getActualLinkForInquiries';
import path from 'path';

export default new ResourceBase(Interpretation, {
    listProperties: ['ekzeget_id', 'added_by', 'edited_by', 'investigated', 'active'],
    showProperties: [
        'ekzeget_id',
        'comment',
        'added_by',
        'edited_by',
        'added_at',
        'edited_at',
        'investigated',
        'active',
        'customRelationVerseForInterpretation',
    ],
    filterProperties: [
        'ekzeget_id',
        'added_by',
        'edited_by',
        'investigated',
        'active',
        'interpretationVerseBook',
        'interpretationVerseChapter',
        'interpretationVerseNumber',
    ],
    editProperties: [
        'ekzeget_id',
        'customRelationVerseForInterpretation',
        'comment',
        'investigated',
        'active',
    ],
    properties: {
        customRelationVerseForInterpretation: {
            components: {
                show: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/EmptyTemplate/index.tsx')),
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/RelationVerse/index')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
            isRequired: true,
        },
        investigated: {
            type: 'boolean',
        },

        interpretationVerseBook: {
            reference: 'Book',
        },
        interpretationVerseChapter: {
            type: 'number',
        },
        interpretationVerseNumber: {
            type: 'number',
        },
    },
});
