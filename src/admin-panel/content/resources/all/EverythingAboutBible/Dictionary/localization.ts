export default {
    translations: {
        labels: {
            Dictionary: 'Словарные слова',
        },
        properties: {
            word: 'Слово',
            letter: 'Буква',
            variant: 'Вариант',
        },
    },
};
