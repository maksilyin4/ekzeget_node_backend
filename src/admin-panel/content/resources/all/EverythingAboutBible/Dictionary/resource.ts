import Dictionary from '../../../../../../entities/Dictionary';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(Dictionary, {
    listProperties: ['type_id', 'word', 'active'],
    showProperties: ['type_id', 'word', 'letter', 'description', 'variant', 'code', 'active'],
    filterProperties: ['type_id', 'word', 'letter', 'variant', 'active'],
    editProperties: ['type_id', 'word', 'letter', 'description', 'variant', 'active'],
});
