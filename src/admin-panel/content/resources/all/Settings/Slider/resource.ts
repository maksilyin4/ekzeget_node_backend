import ResourceBase from '../../../ResourceBase';
import Slider from '../../../../../../entities/Slider';

export default new ResourceBase(Slider, {
    listProperties: ['title', 'article_id', 'type', 'status'],
    showProperties: [
        'article_id',
        'video_url',
        'url',
        'image',
        'title',
        'description',
        'type',
        'status',
        'created_at',
        'updated_at',
    ],
    filterProperties: ['title', 'type', 'status'],
    editProperties: [
        'article_id',
        'video_url',
        'url',
        'image',
        'title',
        'description',
        'type',
        'status',
    ],
    properties: {
        type: {
            availableValues: [
                { value: 'left', label: 'Видео слева' },
                { value: 'right', label: 'Видео справа' },
            ],
        },
        status: {
            availableValues: [
                { value: '1', label: 'Да' },
                { value: '0', label: 'Нет' },
            ],
        },
    },
});
