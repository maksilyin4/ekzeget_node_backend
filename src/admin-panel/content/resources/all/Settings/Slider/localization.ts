export default {
    translations: {
        labels: {
            Slider: 'Слайдер',
        },
        properties: {
            article_id: 'Новость',
            video_url: 'Ссылка на видео'
        },
    },
};
