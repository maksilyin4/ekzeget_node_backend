export default {
    translations: {
        labels: {
            Config: 'Конфигурации',
        },
        properties: {
            value: 'Значение',
        },
    },
};
