import ResourceBase from '../../../ResourceBase';
import Config from '../../../../../../entities/Config';

export default new ResourceBase(Config, {
    listProperties: ['name', 'value'],
    showProperties: [],
    filterProperties: [],
    editProperties: ['name', 'value'],
});
