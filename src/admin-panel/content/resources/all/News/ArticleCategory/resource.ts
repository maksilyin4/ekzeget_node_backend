import ArticleCategory from '../../../../../../entities/ArticleCategory';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(ArticleCategory, {
    listProperties: ['title', 'status'],
    showProperties: ['slug', 'title', 'body', 'parent_id', 'created_at', 'updated_at', 'status'],
    filterProperties: ['slug', 'title', 'parent_id', 'status'],
    editProperties: ['slug', 'title', 'body', 'parent_id', 'status'],
});
