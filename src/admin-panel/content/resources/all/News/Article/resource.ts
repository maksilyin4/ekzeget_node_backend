import Article from '../../../../../../entities/Article';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(Article, {
    listProperties: ['title', 'category_id', 'published_at', 'status'],
    showProperties: [
        'slug',
        'title',
        'body',
        'view',
        'category_id',
        'thumbnail_base_url',
        'thumbnail_path',
        'created_by',
        'updated_by',
        'published_at',
        'created_at',
        'updated_at',
        'image',
        'status',
    ],
    filterProperties: ['title', 'category_id', 'published_at', 'status'],
    editProperties: [
        'slug',
        'title',
        'body',
        'view',
        'category_id',
        'thumbnail_base_url',
        'thumbnail_path',
        'image',
        'status',
    ],
    properties: {
        image_field: {
            isVisible: false,
            props: {
                value: 'image_id',
            },
        },
        slug: { isRequired: true },
    },
});
