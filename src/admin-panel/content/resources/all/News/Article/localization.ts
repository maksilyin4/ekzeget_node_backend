export default {
    translations: {
        labels: {
            Article: 'Статьи',
        },
        properties: {
            thumbnail_base_url: 'Миниатюра(ссылка)',
            thumbnail_path: 'Миниатюра(путь)',
            view: 'Вид',
            slug: 'Название для человеко-читаемой ссылки',
            published_at: 'Дата публикации',
        },
    },
};
