import ResourceBase from '../../../ResourceBase';
import QueezeQuestions from '../../../../../../entities/QueezeQuestions';
import AdminBro, { PropertyType } from 'admin-bro';
import Book from '../../Bible/Book/resource';
import getActualLinkForInquiries from '../../../processing/helpers/getActualLinkForInquiries';
import path from 'path';

export default new ResourceBase(QueezeQuestions, {
    listProperties: [
        'bookForQuestionList',
        'verseForQuestionList',
        'title',
        'description',
        'active',
    ],
    showProperties: [
        'title',
        'active',
        'type',
        'description',
        'answersForQuestionShowAndEdit',
    ],
    filterProperties: ['title', 'bookForQuestionList', 'description', 'active'],
    editProperties: [
        'bookWithChapterAndVerse',
        'type',
        'active',
        'title',
        'description',
        'answersForQuestionShowAndEdit',
        'interpretations',
    ],
    actions: {
        questionsImport: {
            actionType: 'resource',
            name: 'questionsImport',
            icon: 'DocumentImport',
            isVisible: true,
            variant: 'primary',
            component: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Quiz/questionsImport/index')),
        },
    },
    properties: {
        questionsImport: {
            type: 'mixed',
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Quiz/questionsImport/dropZoneForImport')),
            },
        },
        type: {
            isRequired: true,
            type: 'string' as PropertyType,
            availableValues: [
                { value: 'T', label: 'По тексту' },
                { value: 'I', label: 'По интерпретации' },
            ],
        },
        active: {
            isRequired: true,
            type: 'string' as PropertyType,
            availableValues: [
                { value: 'y', label: 'Да' },
                { value: 'n', label: 'Нет' },
            ],
        },
        bookForQuestionList: {
            type: 'reference',
            isTitle: true,
            isVisible: {
                list: true,
                filter: true,
            },
            reference: 'Book',
            isSortable: true,
        },
        verseForQuestionList: {
            isVisible: {
                list: true,
                filter: true,
            },
            type: 'string' as PropertyType,
            isSortable: true,
        },
        answersForQuestionShowAndEdit: {
            isRequired: true,
            components: {
                show: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Quiz/answersOutput/index')),
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Quiz/editAnswers/index.tsx')),
            },
        },
        /*
       Отложено до лучших времен. Компонент для загрузки аудифайлов к вопросу
       relationAudioForQuestion: {
            components: {
                //show: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Quiz/answersOutput/index')),
                edit: AdminBro.bundle(
                    '../../../processing/components/Quiz/relationAudioForQuestion/edit/index'
                ),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },*/
        bookWithChapterAndVerse: {
            isRequired: true,
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Quiz/bookWithChapterAndVerse/index')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },
        interpretations: {
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Quiz/interpretations/index')),
            },
            props: {
                apiUrl: getActualLinkForInquiries(),
            },
        },
    },
});
