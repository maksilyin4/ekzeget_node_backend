export default {
    translations: {
        labels: {
            QueezeQuestions: 'Вопросы',
        },
        properties: {
            description: 'Пояснение',
            type: 'Тип',
            bookForQuestionList: 'Книга',
            verseForQuestionList: 'Глава',
        },
    },
};
