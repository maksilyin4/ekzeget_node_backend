import TimelineEvent from '../../../../../entities/TimelineEvent';
import ResourceBase from '../../ResourceBase';
import AdminBro from 'admin-bro';
import path from 'path';

export default new ResourceBase(TimelineEvent, {
    listProperties: ['category', 'event', 'timeLineText', 'createdAt'],
    showProperties: ['timeLineText'],
    filterProperties: ['category', 'event', 'createdAt'],
    editProperties: ['timeLineText'],
    properties: {
        createdAt: {
            type: 'datetime',
        },
        category: {
            availableValues: [
                { value: 'user', label: 'Пользователь' },
                { value: 'interpretation', label: 'Интерпретация' },
            ],
        },
        event: {
            availableValues: [
                { value: 'signup', label: 'Присоединился' },
                { value: 'new', label: 'Добавлена' },
                { value: 'edit', label: 'Отредактирована' },
            ],
        },
        timeLineText: {
            components: {
                list: AdminBro.bundle(path.resolve(__dirname, '../../processing/components/TimeLine/timeLineText/index.tsx')),
                edit: AdminBro.bundle(path.resolve(__dirname, '../../processing/components/TimeLine/timeLineText/index.tsx')),
                show: AdminBro.bundle(path.resolve(__dirname, '../../processing/components/TimeLine/timeLineText/index.tsx')),
            },
        },
    },
});
