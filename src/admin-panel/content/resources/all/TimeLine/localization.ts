export default {
    translations: {
        labels: {
            TimelineEvent: 'TimeLine',
        },
        properties: {
            category: 'Категория',
            event: 'Событие',
            timeLineText: 'Описание',
        },
    },
};
