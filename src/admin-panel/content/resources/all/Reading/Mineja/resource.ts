import ResourceBase from '../../../ResourceBase';
import Mineja from '../../../../../../entities/Mineja';

export default new ResourceBase(Mineja, {
    listProperties: ['den', 'title', 'znak', 'utrChten', 'active'],
    showProperties: [
        'den',
        'title',
        'znak',
        'utrChten',
        'apostolChten',
        'evangChten',
        'dopChten',
        'color',
        'abbr',
        'active',
    ],
    filterProperties: ['den', 'title', 'znak', 'utrChten', 'active'],
    editProperties: [
        'den',
        'title',
        'znak',
        'utrChten',
        'apostolChten',
        'evangChten',
        'dopChten',
        'color',
        'abbr',
        'active',
    ],
});
