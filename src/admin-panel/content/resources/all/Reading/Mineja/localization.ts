export default {
    translations: {
        labels: {
            Mineja: 'Минея',
        },
        properties: {
            den: 'День',
            znak: 'Знак',
            utrChten: 'Утренние чтения',
            apostolChten: 'Апостольские чтения',
            evangChten: 'Евангиле',
            dopChten: 'Дополнительные чтения',
            color: 'Цвет',
            abbr: 'Аббривиатура',
        },
    },
};
