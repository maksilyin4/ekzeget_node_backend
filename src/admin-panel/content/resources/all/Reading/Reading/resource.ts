import ResourceBase from '../../../ResourceBase';
import Reading from '../../../../../../entities/Reading';

export default new ResourceBase(Reading, {
    listProperties: ['title', 'plan', 'comment'],
    showProperties: ['title', 'plan', 'comment'],
    filterProperties: ['title', 'plan', 'comment'],
    editProperties: ['title', 'plan', 'comment'],
});
