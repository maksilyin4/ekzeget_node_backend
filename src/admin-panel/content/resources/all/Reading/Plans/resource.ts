import ResourceBase from '../../../ResourceBase';
import ReadingPlan from '../../../../../../entities/ReadingPlan';

export default new ResourceBase(ReadingPlan, {
    listProperties: ['title', 'description', 'lenght', 'plan', 'active'],
    showProperties: ['title', 'description', 'lenght', 'plan', 'comment', 'active'],
    filterProperties: ['title', 'description', 'lenght', 'plan', 'active'],
    editProperties: ['title', 'description', 'lenght', 'sort', 'plan', 'comment', 'active'],
});
