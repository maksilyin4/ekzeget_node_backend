import ResourceBase from '../../../ResourceBase';
import ApostolicReading from '../../../../../../entities/ApostolicReading';

export default new ResourceBase(ApostolicReading, {
    listProperties: ['day', 'title', 'reading', 'morning', 'active'],
    showProperties: ['day', 'title', 'reading', 'morning', 'more', 'active'],
    filterProperties: ['day', 'title', 'reading', 'morning', 'active'],
    editProperties: ['day', 'title', 'reading', 'morning', 'more', 'active'],
});
