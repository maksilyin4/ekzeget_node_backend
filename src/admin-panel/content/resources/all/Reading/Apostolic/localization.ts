export default {
    translations: {
        labels: {
            ApostolicReading: 'Апостольские чтения',
        },
        properties: {
            morning: 'Утренние',
            more: 'Дополнительные',
        },
    },
};
