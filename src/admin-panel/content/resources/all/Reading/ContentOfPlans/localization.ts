export default {
    translations: {
        labels: {
            Plan: 'Содержание планов чтения',
        },
        properties: {
            readingId: 'Чтение',
            bookId: 'Книга',
            verse: 'Стихи',
        },
    },
};
