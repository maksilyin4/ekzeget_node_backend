import ResourceBase from '../../../ResourceBase';
import Plan from '../../../../../../entities/Plan';

export default new ResourceBase(Plan, {
    listProperties: ['reading_id', 'day', 'book_id', 'chapter', 'verse'],
    showProperties: ['reading_id', 'day', 'book_id', 'chapter', 'verse'],
    filterProperties: ['reading_id', 'day', 'book_id', 'chapter', 'verse'],
    editProperties: ['reading_id', 'day', 'book_id', 'chapter', 'verse'],
});
