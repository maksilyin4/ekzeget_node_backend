import ResourceBase from '../../../ResourceBase';
import PostReading from '../../../../../../entities/PostReading';

export default new ResourceBase(PostReading, {
    listProperties: ['day', 'title', 'apostle', 'gospel', 'active'],
    showProperties: ['day', 'title', 'apostle', 'gospel', 'vz', 'morning', 'active'],
    filterProperties: ['day', 'title', 'apostle', 'gospel', 'active'],
    editProperties: ['day', 'title', 'apostle', 'gospel', 'vz', 'morning', 'active'],
});
