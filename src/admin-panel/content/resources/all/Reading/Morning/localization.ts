export default {
    translations: {
        labels: {
            MorningReading: 'Утренние чтения',
        },
        properties: {
            dni: 'Дни',
            utr: 'Чтения',
        },
    },
};
