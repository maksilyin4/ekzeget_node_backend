import ResourceBase from '../../../ResourceBase';
import MorningReading from '../../../../../../entities/MorningReading';

export default new ResourceBase(MorningReading, {
    listProperties: ['dni', 'utr'],
    showProperties: ['dni', 'utr'],
    filterProperties: ['dni', 'utr'],
    editProperties: ['dni', 'utr'],
    properties: {
        dni: {
            isTitle: true,
        },
    },
});
