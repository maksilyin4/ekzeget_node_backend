import ResourceBase from '../../../ResourceBase';
import GospelReading from '../../../../../../entities/GospelReading';

export default new ResourceBase(GospelReading, {
    listProperties: ['day', 'title', 'reading', 'active'],
    showProperties: ['day', 'title', 'reading', 'active'],
    filterProperties: ['day', 'title', 'reading', 'active'],
    editProperties: ['day', 'title', 'reading', 'active'],
});
