export default {
    translations: {
        labels: {
            GospelReading: 'Евангелие',
        },
        properties: {
            day: 'День',
            reading: 'Чтение',
        },
    },
};
