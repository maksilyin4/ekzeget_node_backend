import Book from '../../../../../../entities/Book';
import { PropertyType } from 'admin-bro/src/backend/adapters/property/base-property';
import ResourceBase from '../../../ResourceBase';

export default new ResourceBase(Book, {
    listProperties: ['testament_id', 'title', 'parts', 'author', 'year', 'place', 'sort'],
    showProperties: [
        'testament_id',
        'title',
        'short_title',
        'parts',
        'code',
        'menu',
        'author',
        'year',
        'place',
        'sort',
        'gospel',
    ],
    filterProperties: [
        'testament_id',
        'title',
        'short_title',
        'parts',
        'menu',
        'author',
        'year',
        'place',
        'gospel',
    ],
    editProperties: [
        'testament_id',
        'title',
        'short_title',
        'parts',
        'menu',
        'author',
        'year',
        'place',
        'sort',
        'gospel',
    ],
    properties: {
        gospel: { type: 'boolean' as PropertyType },
        sort: { type: 'number' as PropertyType, isRequired: true },
        testament_id: {
            isRequired: true,
            isSortable: true,
        },
        short_title: { isRequired: true },
        parts: { isRequired: true },
        menu: { isRequired: true },
        author: { isRequired: true },
    },
});
