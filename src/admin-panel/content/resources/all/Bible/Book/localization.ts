export default {
    language: 'ru',
    translations: {
        labels: {
            Book: 'Книги',
        },
        properties: {
            parsed_text: 'Толкование',
            parts: 'Количество глав',
            menu: 'Название в меню',
            gospel: 'Евангелие',
            author: 'Автор',
            year: 'Время происх.',
            place: 'Место происх.',
        },
    },
};
