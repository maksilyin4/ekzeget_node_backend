import BookDescriptor from '../../../../../../entities/BookDescriptor';
import ResourceBase from '../../../ResourceBase';
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';

export default new ResourceBase(BookDescriptor, {
    listProperties: ['title', 'active'],
    showProperties: ['title', 'active'],
    filterProperties: ['title', 'active'],
    editProperties: ['title', 'active'],
    properties: {
        active: {
            isSortable: true,
        },
    },
    actions: relaxedAccessRules,
});
