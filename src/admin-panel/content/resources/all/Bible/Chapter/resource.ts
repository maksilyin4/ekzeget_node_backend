import Chapter from '../../../../../../entities/Chapters';
import ResourceBase from '../../../ResourceBase';
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';

export default new ResourceBase(Chapter, {
    listProperties: ['book_id', 'number'],
    showProperties: ['book_id', 'number'],
    filterProperties: ['book_id', 'number'],
    editProperties: ['book_id', 'number'],

    actions: relaxedAccessRules,
});
