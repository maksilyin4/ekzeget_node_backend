import BookDescription from '../../../../../../entities/BookDescription';
import ResourceBase from '../../../ResourceBase';
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';

export default new ResourceBase(BookDescription, {
    listProperties: ['book_id', 'descriptor_id', 'active'],
    showProperties: ['book_id', 'descriptor_id', 'text', 'active'],
    filterProperties: ['book_id', 'descriptor_id', 'active'],
    editProperties: ['book_id', 'descriptor_id', 'text', 'active'],
    actions: relaxedAccessRules,
});
