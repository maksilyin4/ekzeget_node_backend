import Verse from '../../../../../../entities/Verse';
import ResourceBase from '../../../ResourceBase';
import AdminBro from 'admin-bro';
import getActualLinkForInquiries from '../../../processing/helpers/getActualLinkForInquiries';
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';
import path from 'path';

export default new ResourceBase(Verse, {
    listProperties: ['book_id', 'chapter', 'number', 'text'],
    showProperties: [
        'book_id',
        'chapter_id',
        'chapter',
        'number',
        'text',
        'customRelationVerseForBibleVerse',
    ],
    filterProperties: ['book_id', 'chapter', 'number'],
    editProperties: [
        'book_id',
        'chapter',
        'number',
        'text',
        'customRelationVerseForBibleVerse',
        'customRelationDictionaryArticles',
        'customRelationTranslations',
    ],
    properties: {
        customRelationVerseForBibleVerse: {
            components: {
                show: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/EmptyTemplate/index.tsx')),
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/RelationVerse/index')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },
        customRelationTranslations: {
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Bible/Verse/customTranslationFields/index.tsx')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },
        customRelationDictionaryArticles: {
            components: {
                edit: AdminBro.bundle(path.resolve(__dirname, '../../../processing/components/Bible/Verse/customRelationDictionaryArticles/index.tsx')),
            },
            props: {
                actualLinkForInquiries: getActualLinkForInquiries(),
            },
        },
    },

    actions: relaxedAccessRules,
});
