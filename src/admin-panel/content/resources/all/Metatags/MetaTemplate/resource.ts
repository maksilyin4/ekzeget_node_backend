import ResourceBase from '../../../ResourceBase';
import MetaTemplate from '../../../../../../entities/MetaTemplate';
import { PropertyType } from 'admin-bro';
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';

export default new ResourceBase(MetaTemplate, {
    listProperties: ['id', 'title', 'url'],
    filterProperties: ['id', 'title', 'url'],
    showProperties: [
        'id',
        'url',
        'title',
        'description',
        'h_one',
        'keywords',
        'created_at',
        'updated_at',
        'context_example',
    ],
    editProperties: ['url', 'title', 'description', 'h_one', 'keywords', 'context_example'],
    properties: {
        description: {
            type: 'string' as PropertyType,
        },
        context_example: {
            type: 'richtext'
        }
    },
    actions: relaxedAccessRules,
});
