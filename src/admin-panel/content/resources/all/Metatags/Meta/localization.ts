export default {
    translations: {
        labels: {
            Meta: 'Список',
        },
        properties: {
            description: 'Пояснение',
            type: 'Тип',
            comment: 'Детальное описание',
            section_id: 'Раздел',
            category: 'Категория',
            h_one: 'H1',
            keywords: 'Ключевые слова',
        },
    },
};
