import ResourceBase from '../../../ResourceBase';
import Meta from '../../../../../../entities/Meta';
import {PropertyType} from "admin-bro";
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';

export default new ResourceBase(Meta, {
    listProperties: ['section_id', 'category', 'code', 'title', 'h_one', 'description'],
    filterProperties: ['section_id', 'category', 'code', 'title', 'h_one', 'description'],
    showProperties: [
        'section_id',
        'category',
        'code',
        'title',
        'description',
        'h_one',
        'created_at',
        'updated_at',
    ],
    editProperties: ['section_id', 'category', 'code', 'title', 'h_one', 'description', 'keywords'],
    properties: {
        description: {
            type: 'string' as PropertyType,
        },
    },
    actions: relaxedAccessRules,
});
