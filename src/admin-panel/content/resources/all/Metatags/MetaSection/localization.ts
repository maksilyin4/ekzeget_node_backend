export default {
    translations: {
        labels: {
            MetaSection: 'Разделы',
        },
        properties: {
            description: 'Пояснение',
            entity: 'Сущность',
            createdAt: 'Дата создания',
            updatedAt: 'Дата изменения',
            hOne: 'H1',
        },
    },
};
