import ResourceBase from '../../../ResourceBase';
import MetaSection from '../../../../../../entities/MetaSection';
import {PropertyType} from "admin-bro";
import relaxedAccessRules from '../../../../../auth/relaxedAccessRules';

export default new ResourceBase(MetaSection, {
    listProperties: ['entity', 'name', 'title', 'keywords'],
    showProperties: [
        'entity',
        'name',
        'createdAt',
        'updatedAt',
        'title',
        'hOne',
        'description',
        'keywords',
    ],
    filterProperties: ['entity', 'name', 'title'],
    editProperties: ['entity', 'name', 'title', 'hOne', 'description', 'keywords'],
    properties: {
        description: {
            type: 'string' as PropertyType,
        },
    },
    actions: relaxedAccessRules,
});
