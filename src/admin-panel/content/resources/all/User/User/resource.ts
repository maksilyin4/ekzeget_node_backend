import ResourceBase from '../../../ResourceBase';
import User from '../../../../../../entities/User';
import { PropertyType } from 'admin-bro';

export default new ResourceBase(User, {
    listProperties: ['username', 'email', 'created_at', 'logged_at', 'show_email', 'status'],
    showProperties: [
        'username',
        'email',
        'created_at',
        'updated_at',
        'logged_at',
        'phone',
        'show_email',
        'status',
    ],
    filterProperties: ['username', 'email', 'phone', 'show_email', 'status'],
    editProperties: ['username', 'email', 'phone', 'show_email', 'status'],
    properties: {
        username: { isTitle: true },
        show_email: { type: 'boolean' as PropertyType },
    },
});
