export default {
    translations: {
        labels: {
            BibleGroup: 'Группы',
        },
        properties: {
            history: 'История',
            vedet: 'Ведущий',
            priester: 'Священник',
            we_reads: 'Чтение',
            kak_prohodit: 'Формат',
            coords: 'Координаты',
            times: 'Время провдения',
            // это честно не я
            data: 'Дата',
        },
    },
};
