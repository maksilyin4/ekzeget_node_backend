import ResourceBase from '../../../ResourceBase';
import BibleGroupType from '../../../../../../entities/BibleGroupType';

export default new ResourceBase(BibleGroupType, {
    listProperties: ['title', 'description', 'active'],
    showProperties: ['title', 'description', 'active'],
    filterProperties: ['title', 'description', 'active'],
    editProperties: ['title', 'description', 'active'],
});
