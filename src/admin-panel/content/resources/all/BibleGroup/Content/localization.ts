export default {
    translations: {
        labels: {
            BibleGroupNote: 'Контент',
        },
        properties: {
            views: 'Просмотры',
            tags: 'Теги',
        },
    },
};
