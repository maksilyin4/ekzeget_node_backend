import ResourceBase from '../../../ResourceBase';
import BibleGroupNote from '../../../../../../entities/BibleGroupNote';

export default new ResourceBase(BibleGroupNote, {
    listProperties: ['data', 'title', 'views', 'type'],
    showProperties: ['data', 'title', 'code', 'text', 'views', 'type', 'tags'],
    filterProperties: ['data', 'title', 'views', 'type'],
    editProperties: ['data', 'title', 'code', 'text', 'views', 'type', 'tags'],
    properties: {
        /*type: {
            reference: 'BibleGroupType',
        },*/
    },
});
