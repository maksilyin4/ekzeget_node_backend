export default {
    translations: {
        labels: {
            Typo: 'Опечатки',
        },
        properties: {
            url: 'Ссылка на страницу',
            selection: 'Текст с опечаткой',
            comment: 'Комментарий пользователя',
            is_report_helpful: 'Полезно ли сообщение об ошибке?',
            processed: 'Обработано',
            user_id: 'Пользователь',
            created_at: 'Дата',
        },
    },
};
