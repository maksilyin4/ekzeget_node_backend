import Typo from '../../../../../entities/Typo';
import ResourceBase from '../../ResourceBase';
import { PropertyType } from 'admin-bro/src/backend/adapters/property/base-property';

export default new ResourceBase(Typo, {
    listProperties: ['url', 'processed', 'user_id', 'created_at'],
    showProperties: [
        'url',
        'selection',
        'comment',
        'is_report_helpful',
        'processed',
        'user_id',
        'created_at',
    ],
    filterProperties: [
        'url',
        'selection',
        'comment',
        'is_report_helpful',
        'processed',
        'user_id',
        'created_at',
    ],
    editProperties: ['is_report_helpful', 'processed'],

    properties: {
        selection: {
            type: 'richtext' as PropertyType,
        },
        created_at: {
            type: 'date' as PropertyType,
        },
        user_id: {
            reference: 'User',
        },
    },

    navigation: {
        name: 'Обратная связь',
        icon: 'Chat',
    },
});
