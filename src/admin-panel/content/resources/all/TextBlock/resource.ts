import ResourceBase from '../../ResourceBase';
import TextBlock from '../../../../../entities/TextBlock';
import { PropertyType } from 'admin-bro';
import relaxedAccessRules from '../../../../auth/relaxedAccessRules';

export default new ResourceBase(TextBlock, {
    listProperties: ['id', 'code', 'title', 'text', 'active'],
    filterProperties: ['id', 'code', 'title', 'text', 'active'],
    showProperties: ['id', 'code', 'title', 'text', 'active'],
    editProperties: ['code', 'title', 'text', 'active'],
    actions: relaxedAccessRules,
});
