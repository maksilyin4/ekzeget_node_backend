export default {
    translations: {
        labels: {
            TextBlock: 'Текстовые блоки',
        },
        properties: {
            code: 'Символьный код',
            title: 'Название',
            text: 'Контент',
            active: 'Активен',
        },
    },
};
