import { IResourceWithOption } from '../types/IResource';

export enum NavigationGroupList {
    'bible' = 'Библия',
    'mediateka' = 'Медиатека',
    'everythingAboutBible' = 'Все о библии',
    'bibleGroup' = 'Библейские группы',
    'news' = 'Новости',
    'user' = 'Пользователи',
    'quiz' = 'Викторина',
    'metaTags' = 'Мета-теги',
    'reading' = 'Чтения',
    'settings' = 'Настройки',
    'timeline' = 'TimeLine',
    'textBlock' = 'Текстовые блоки',
}

// админ бро использует под капотом carbondesignsystem: https://www.carbondesignsystem.com/guidelines/icons/library
// название иконок оттуда
const navigationSet = {
    [NavigationGroupList.bible]: {
        name: NavigationGroupList.bible,
        icon: 'WorshipChristian',
    },
    [NavigationGroupList.mediateka]: {
        name: NavigationGroupList.mediateka,
        icon: 'Laptop',
    },
    [NavigationGroupList.everythingAboutBible]: {
        name: NavigationGroupList.everythingAboutBible,
        icon: 'Workspace',
    },
    [NavigationGroupList.bibleGroup]: {
        name: NavigationGroupList.bibleGroup,
        icon: 'Collaborate',
    },
    [NavigationGroupList.news]: {
        name: NavigationGroupList.news,
        icon: 'Bullhorn',
    },
    [NavigationGroupList.user]: {
        name: NavigationGroupList.user,
        icon: 'User',
    },
    [NavigationGroupList.quiz]: {
        name: NavigationGroupList.quiz,
        icon: 'Help',
    },
    [NavigationGroupList.metaTags]: {
        name: NavigationGroupList.metaTags,
        icon: 'Tag',
    },
    [NavigationGroupList.reading]: {
        name: NavigationGroupList.reading,
        icon: 'Notebook',
    },
    [NavigationGroupList.settings]: {
        name: NavigationGroupList.settings,
        icon: 'Settings',
    },
    [NavigationGroupList.timeline]: {
        name: NavigationGroupList.timeline,
        icon: 'Time',
    },
    [NavigationGroupList.textBlock]: {
        name: NavigationGroupList.textBlock,
        icon: 'Bullhorn',
    },
};

export default (resources: IResourceWithOption[], groupName: NavigationGroupList) => {
    return resources.map(r => {
        r.options.navigation = navigationSet[groupName];
        return r;
    });
};
