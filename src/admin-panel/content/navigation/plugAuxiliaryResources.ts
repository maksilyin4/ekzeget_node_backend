import Entities from '../../../entities';

export default (entities: typeof Entities[number][]) => {
    return entities.map(ent => ({
        resource: ent,
        options: {
            actions: {
                list: { isVisible: false },
                edit: { isVisible: false },
                delete: { isVisible: false },
                filter: { isVisible: false },
            },
        },
    }));
};
