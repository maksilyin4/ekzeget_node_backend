const emptyAnswers = [];
for (let i = 0; i < 4; ++i) {
    emptyAnswers.push({
        id: i,
        text: 'Ответ',
        is_correct: false,
    });
}

export default emptyAnswers;
