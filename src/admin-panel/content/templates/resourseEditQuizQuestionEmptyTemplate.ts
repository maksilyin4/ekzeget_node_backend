import {IResourceEditQuizQuestion} from "../types/IResourceEditQuizQuestion";

export default {
    relationBook: {
        parts: 1,
        id: 0,
    },
    allVerseForChapter: [],
    relationVerse: {
        chapter: null,
    },
} as IResourceEditQuizQuestion;
