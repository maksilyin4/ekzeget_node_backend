import createResourceNavigationGroup, {
    NavigationGroupList,
} from './navigation/createResourceNavigationGroup';
import plugAuxiliaryResources from './navigation/plugAuxiliaryResources';

import Testament from '../../entities/Testament';
import FileStorageItem from '../../entities/FileStorageItem';
import User from '../../entities/User';

import BookResource from './resources/all/Bible/Book/resource';
import BookDescriptionResource from './resources/all/Bible/BookDescription/resource';
import BookDescriptorResource from './resources/all/Bible/BookDescriptor/resource';
import ChapterResource from './resources/all/Bible/Chapter/resource';
import VerseResource from './resources/all/Bible/Verse/resource';
import MediaResource from './resources/all/Mediateka/Media/resource';
import MediaAuthorResource from './resources/all/Mediateka/MediaAuthor/resource';
import MediaCategoryResource from './resources/all/Mediateka/MediaCategory/resource';
import MediaGenreResource from './resources/all/Mediateka/MediaGenre/resource';
import MediaAuthorCategoryResource from './resources/all/Mediateka/MediaAuthorCategory/resource';
import LectureResource from './resources/all/EverythingAboutBible/Lecture/resource';
import LectureTypeResource from './resources/all/EverythingAboutBible/LectureType/resource';
import EkzegetPersonResource from './resources/all/EverythingAboutBible/EkzegetPerson/resource';
import EkzegetTypeResource from './resources/all/EverythingAboutBible/EkzegetType/resource';
import InterpretationsResource from './resources/all/EverythingAboutBible/Interpretations/resource';
import PreacherResource from './resources/all/EverythingAboutBible/Preacher/resource';
import PreachingResource from './resources/all/EverythingAboutBible/Preaching/resource';
import DictionaryResource from './resources/all/EverythingAboutBible/Dictionary/resource';
import OcassionForPreaching from './resources/all/EverythingAboutBible/OcassionForPreaching/resource';
import DictionaryTypeResource from './resources/all/EverythingAboutBible/DictionaryType/resource';
import ReasonForHoliday from './resources/all/EverythingAboutBible/ReasonForHoliday/resource';
import Celebrate from './resources/all/EverythingAboutBible/Celebrate/resource';
import BibleMapsPoints from './resources/all/EverythingAboutBible/BibleMapsPoints/resource';
import BibleGroupResource from './resources/all/BibleGroup/BibleGroup/resource';
import BibleGroupType from './resources/all/BibleGroup/Type/resource';
import BibleGroupContent from './resources/all/BibleGroup/Content/resource';
import ArticleResource from './resources/all/News/Article/resource';
import ArticleCategoryResource from './resources/all/News/ArticleCategory/resource';
import UserResource from './resources/all/User/User/resource';
import QueezeQuestions from './resources/all/Quiz/QueezeQuestions/resource';
import Meta from './resources/all/Metatags/Meta/resource';
import MetaSection from './resources/all/Metatags/MetaSection/resource';
import Plans from './resources/all/Reading/Plans/resource';
import GospelReading from './resources/all/Reading/Gospel/resource';
import ApostolicReading from './resources/all/Reading/Apostolic/resource';
import PostReading from './resources/all/Reading/Post/resource';
import MorningReading from './resources/all/Reading/Morning/resource';
import Mineja from './resources/all/Reading/Mineja/resource';
import ContentOfPlans from './resources/all/Reading/ContentOfPlans/resource';
import Reading from './resources/all/Reading/Reading/resource';
import Slider from './resources/all/Settings/Slider/resource';
import Config from './resources/all/Settings/Config/resource';
import MetaTemplate from './resources/all/Metatags/MetaTemplate/resource';
import MediaTag from './resources/all/Mediateka/MediaTag/resource';
import TimeLine from './resources/all/TimeLine/resource';
import TypoResource from './resources/all/Typo/resource';
import TextBlock from './resources/all/TextBlock/resource';

export default [
    ...createResourceNavigationGroup(
        [
            BookDescriptorResource,
            BookResource,
            BookDescriptionResource,
            ChapterResource,
            VerseResource,
        ],
        NavigationGroupList.bible
    ),
    ...createResourceNavigationGroup(
        [
            MediaResource,
            MediaAuthorResource,
            MediaCategoryResource,
            MediaGenreResource,
            MediaAuthorCategoryResource,
            MediaTag,
        ],
        NavigationGroupList.mediateka
    ),
    ...createResourceNavigationGroup(
        [
            LectureResource,
            LectureTypeResource,
            EkzegetPersonResource,
            EkzegetTypeResource,
            InterpretationsResource,
            DictionaryResource,
            DictionaryTypeResource,
            PreacherResource,
            PreachingResource,
            OcassionForPreaching,
            ReasonForHoliday,
            Celebrate,
            BibleMapsPoints,
        ],
        NavigationGroupList.everythingAboutBible
    ),
    ...createResourceNavigationGroup(
        [BibleGroupResource, BibleGroupType, BibleGroupContent],
        NavigationGroupList.bibleGroup
    ),
    ...createResourceNavigationGroup(
        [ArticleResource, ArticleCategoryResource],
        NavigationGroupList.news
    ),
    ...createResourceNavigationGroup([UserResource], NavigationGroupList.user),
    ...createResourceNavigationGroup([QueezeQuestions], NavigationGroupList.quiz),
    ...createResourceNavigationGroup(
        [Meta, MetaSection, MetaTemplate],
        NavigationGroupList.metaTags
    ),
    ...createResourceNavigationGroup(
        [
            Plans,
            GospelReading,
            ApostolicReading,
            PostReading,
            MorningReading,
            Mineja,
            ContentOfPlans,
            Reading,
        ],
        NavigationGroupList.reading
    ),
    TypoResource,
    ...createResourceNavigationGroup([Slider, Config, TimeLine], NavigationGroupList.settings),
    ...plugAuxiliaryResources([Testament, FileStorageItem, User]),
    ...createResourceNavigationGroup([TextBlock], NavigationGroupList.textBlock),
];
