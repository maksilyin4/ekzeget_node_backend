export interface IBookParameters {
    bookId?: number;
    chapter?: number;
    number?: number;
}
