export interface IRelationVerseForCustomComponents {
    id: number;
    relationBookShortRecord: string;
    relationVerseId: number;
    relationEntityId: number;
}
