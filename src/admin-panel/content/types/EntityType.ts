import Entities from '../../../entities';

export type EntityType = typeof Entities[number];
