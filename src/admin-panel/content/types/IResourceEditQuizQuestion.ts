import Book from '../../../entities/Book';
import Verse from '../../../entities/Verse';

export interface IResourceEditQuizQuestion {
    relationBook: Book;
    relationVerse: Verse;
    allVerseForChapter: Partial<Verse>[];
}
