import MediaRelated from '../../../entities/MediaRelated';

export interface IRelationRecordInfo extends Pick<MediaRelated, 'related_id' | 'sort'> {
    title: string;
}
