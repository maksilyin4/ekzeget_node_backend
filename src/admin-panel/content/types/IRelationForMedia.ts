import { IRelationVerseForCustomComponents } from './IRelationVerseForCustomComponents';

export interface IRelationForMedia extends IRelationVerseForCustomComponents {
    relationChapterNumber: number;
    relationVerseNumber: number;
}
