import { ResourceOptions } from 'admin-bro';
import { EntityType } from './EntityType';

export interface IResourceWithOption {
    resource: EntityType;
    options: ResourceOptions;
}
