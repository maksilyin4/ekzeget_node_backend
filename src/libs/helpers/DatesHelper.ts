import { MILLISECONDS_IN_DAY } from '../../const/numbers/millisecondsInDay';
import getUnixSec from './getUnixSec';

export class DatesHelper {
    /**
     * Прибавить дни
     */
    public static addDays = (date: Date, days: number): Date =>
        new Date(date.getTime() + MILLISECONDS_IN_DAY * days);

    /**
     * Отнять дни
     */
    public static subtractDays = (date: Date, days: number): Date =>
        new Date(date.getTime() - MILLISECONDS_IN_DAY * days);

    /**
     *  Возвращает разницу между датами в количестве полных дней
     */
    public static calcDifferenceInFullDays = (
        date1: Date,
        date2: Date,
        roundingFunction = Math.floor
    ): number => roundingFunction((date1.getTime() - date2.getTime()) / MILLISECONDS_IN_DAY);

    /**
     * Парсит дату в формате DD.MM.YYYY
     */
    public static parseDateStringDDMMYYYYToDate(dateString: string): Date {
        const split = dateString.split('.');

        const date = new Date();
        date.setHours(0, 0, 0, 0);
        date.setFullYear(parseInt(split[2]), parseInt(split[1]) - 1, parseInt(split[0]));
        return date;
    }

    public static getDateYYYYMMDD(offsetSec = 0): string {
        const today = new Date((getUnixSec() + offsetSec) * 1000);
        return today.toISOString().substring(0, 10);
    }

    /**
     * Проверка на високосный год
     */
    public static isLeapYear(yr: number): boolean {
        return !(yr % 4 || (!(yr % 100) && yr % 400));
    }

    /**
     * Возвращает дату в формате DD.MM
     */
    public static getDateStringDDMM(date: Date): string {
        const day = date
            .getDate()
            .toString()
            .padStart(2, '0');
        const month = (date.getMonth() + 1).toString().padStart(2, '0');
        return `${day}.${month}`;
    }
}
