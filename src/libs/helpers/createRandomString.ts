import nanoid from 'nanoid';

export default (
    length: number = 12,
    symbolString: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'
) => nanoid.customAlphabet(symbolString, length)();
