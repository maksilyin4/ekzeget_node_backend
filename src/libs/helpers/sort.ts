export const sortASC = (a: number | string, b: number | string) => (a < b ? -1 : a > b ? 1 : 0);

export const sortDESC = (a: number | string, b: number | string) => (a < b ? 1 : a > b ? -1 : 0);

export const sortByKeyASC = <T, K>(a: T, b: T, key: keyof T) =>
    a[key] < b[key] ? -1 : a[key] > b[key] ? 1 : 0;

export const sortByKeyDESC = <T, K>(a: T, b: T, key: keyof T) =>
    a[key] < b[key] ? 1 : a[key] > b[key] ? -1 : 0;
