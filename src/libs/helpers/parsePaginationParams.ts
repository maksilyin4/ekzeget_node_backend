import pagination from '../../const/pagination';

interface PaginationParams {
    page?: number;
    'per-page'?: number;
}

export const parsePaginationParams = (
    params: PaginationParams
): { page: number; perPage: number } => ({
    page: params.page ? +params.page : pagination.defaultPage,
    perPage: params['per-page'] ? +params['per-page'] : pagination.defaultPerPage,
});
