/**
 * Возвращает дату и время в формате DD.MM.YYYY HH:MM
 */
export default () => {
    const d = new Date();
    const days = d
        .getDate()
        .toString()
        .padStart(2, '0');
    const months = (d.getMonth() + 1).toString().padStart(2, '0');
    const hours = d
        .getHours()
        .toString()
        .padStart(2, '0');
    const minutes = d
        .getMinutes()
        .toString()
        .padStart(2, '0');
    return `${days}.${months}.${d.getFullYear()} ${hours}:${minutes}`;
};
