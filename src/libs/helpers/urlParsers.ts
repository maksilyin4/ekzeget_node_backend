export enum UrlPrefixes {
    chapter = 'glava-',
    versePrefix1 = 'stih-',
    versePrefix2 = 'tolkovanie-',
}

export const extractNumber = (str: string): number => {
    if (!str) return 0;
    const clear = str.replace(/(\D+)/g, '');
    return clear ? Number(clear) : 0;
};
