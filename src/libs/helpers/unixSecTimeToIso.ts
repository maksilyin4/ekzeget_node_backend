export default unixSec => new Date(unixSec * 1000);
