export default async func => {
    const startControllerTime = Date.now();
    const result = await func();
    const endControllerTime = Date.now();
    const timing = endControllerTime - startControllerTime + 'ms';
    return { result, timing };
};
