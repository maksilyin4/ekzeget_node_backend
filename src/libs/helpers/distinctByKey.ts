export function distinctByKey<T>(entities: T[], key: keyof T): T[] {
    const uniqBy = (arr, predicate) => [
        ...arr
            .reduce((map, item) => {
                const key = predicate(item);
                if (!map.has(key)) {
                    map.set(key, item);
                }
                return map;
            }, new Map())
            .values(),
    ];
    return uniqBy(entities, item => item[key]);
}
