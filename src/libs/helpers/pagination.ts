import * as Joi from '@hapi/joi';
import pagination from '../../const/pagination';

export const paginationObjectScheme = Joi.object({
    currentPage: Joi.number(),
    pageParam: Joi.string(),
    pageSizeParam: Joi.string(),
    params: Joi.any().allow(null),
    validatePage: Joi.boolean(),
    totalCount: Joi.number(),
    defaultPageSize: Joi.number(),
    pageSizeLimit: Joi.array().items(Joi.number()),
}).description('Информация о возможной постраничной навигации');

export const paginationParamsWrapper = (validatedParams: {
    'per-page'?: string | number;
    page?: number | string;
}): {
    perPage: number;
    page: number;
} => {
    const page = +validatedParams.page ? +validatedParams.page : pagination.defaultPage;
    const perPage = +validatedParams['per-page']
        ? +validatedParams['per-page']
        : pagination.defaultPerPage;

    return { page, perPage };
};
