const isArrayString = str => str.endsWith('[]');
const isAssociateArrayString = str => !!str.match(/\[(.*?)]/);
const getArrayLiteralPosition = str => str.indexOf('[');

export default (params: Object): Object => {
    for (const k of Object.keys(params)) {
        if (isArrayString(k)) {
            params[k.slice(0, k.length - 2)] = params[k].split(',');
            delete params[k];
        }
        if (isAssociateArrayString(k)) {
            const arrayFieldName = k.slice(0, getArrayLiteralPosition(k));
            const currentValue = params[k];
            params[arrayFieldName]
                ? params[arrayFieldName].push(currentValue)
                : (params[arrayFieldName] = [currentValue]);

            delete params[k];
        }
    }

    return params;
};
