export const groupBy = <TItem>(xs: TItem[], key: string): {[key: string]: TItem[]} => xs.reduce((rv, x) => {
  (rv[x[key]] = rv[x[key]] || []).push(x);
  return rv;
}, {});
