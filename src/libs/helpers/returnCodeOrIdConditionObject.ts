export default codeOrId => (+codeOrId ? { id: codeOrId } : { code: codeOrId });
