export default (objArray, objFieldNameForHashKey) => {
    const hash = {};
    if (!objArray.length) return hash;

    objArray.forEach(obj => {
        if (!hash[obj[objFieldNameForHashKey]]) hash[obj[objFieldNameForHashKey]] = obj;
    });

    return hash;
};
