export default (value: string | number, index, self) => {
    return self.indexOf(value) === index;
};
