export default function getUnixSec() {
    return +(Date.now() / 1000).toFixed();
}
