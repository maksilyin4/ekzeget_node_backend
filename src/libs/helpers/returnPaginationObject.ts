import pagination from '../../const/pagination';

interface IPaginationParam {
    page?: number;
    'per-page'?: number;
    totalCount: number;
    params?: any;
    validatePage?: boolean;
}

export default (paginationParam: IPaginationParam) => ({
    defaultPageSize: pagination.defaultPerPage,
    pageParam: 'page',
    pageSizeParam: 'per-page',

    currentPage: paginationParam.page || 1,
    pageSizeLimit: [
        paginationParam.page || 1,
        paginationParam['per-page'] || pagination.defaultPerPage,
    ],
    totalCount: paginationParam.totalCount,

    params: paginationParam.params || null,
    validatePage: paginationParam.validatePage || true,
});
