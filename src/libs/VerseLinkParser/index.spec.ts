import { VerseLinkParser } from './index';
import { ParsedShortLink } from './types';

describe('VerseLinkParser', () => {
    describe('#1 Examples', () => {
        it('Мк. 1', () => {
            const input = 'Мк. 1';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Мк',
                    chapters: [
                        {
                            chapterNumber: 1,
                            verses: [],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Мк. 1:1', () => {
            const input = 'Мк. 1:1';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Мк',
                    chapters: [
                        {
                            chapterNumber: 1,
                            verses: [
                                {
                                    equal: 1,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Мк. 12:34', () => {
            const input = 'Мк. 12:34';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Мк',
                    chapters: [
                        {
                            chapterNumber: 12,
                            verses: [
                                {
                                    equal: 34,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Мк. 12:34-56', () => {
            const input = 'Мк. 12:34-56';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Мк',
                    chapters: [
                        {
                            chapterNumber: 12,
                            verses: [
                                {
                                    from: 34,
                                    to: 56,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Мк. 12:34-56, 78-90', () => {
            const input = 'Мк. 12:34-56, 78-90';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Мк',
                    chapters: [
                        {
                            chapterNumber: 12,
                            verses: [
                                {
                                    from: 34,
                                    to: 56,
                                },
                                {
                                    from: 78,
                                    to: 90,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Деян. 6:8–7:5, 47–60', () => {
            const input = 'Деян. 6:8–7:5, 47–60';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Деян',
                    chapters: [
                        {
                            chapterNumber: 6,
                            verses: [
                                {
                                    from: 8,
                                },
                            ],
                        },
                        {
                            chapterNumber: 7,
                            verses: [
                                {
                                    to: 5,
                                },
                                {
                                    from: 47,
                                    to: 60,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Мк. 12:56-34, 78-90; Ам. 8:800-555, 35-35', () => {
            const input = 'Мк. 12:56-34, 78-90; Ам. 8:800-555, 35-35';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Мк',
                    chapters: [
                        {
                            chapterNumber: 12,
                            verses: [
                                {
                                    from: 34,
                                    to: 56,
                                },
                                {
                                    from: 78,
                                    to: 90,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: 'Ам',
                    chapters: [
                        {
                            chapterNumber: 8,
                            verses: [
                                {
                                    from: 555,
                                    to: 800,
                                },
                                {
                                    from: 35,
                                    to: 35,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });
    });

    describe('#2 Real cases', () => {
        it('Евр. 3:12-16;, за упокой: 1 Фес. 4:13-17', () => {
            const input = 'Евр. 3:12-16;, за упокой: 1 Фес. 4:13-17';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Евр',
                    chapters: [
                        {
                            chapterNumber: 3,
                            verses: [
                                {
                                    from: 12,
                                    to: 16,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: '1 Фес',
                    chapters: [
                        {
                            chapterNumber: 4,
                            verses: [
                                {
                                    from: 13,
                                    to: 17,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Евр. 1:1-12;, Вмч.: 2 Тим. 2:1-10', () => {
            const input = 'Евр. 1:1-12;, Вмч.: 2 Тим. 2:1-10';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Евр',
                    chapters: [
                        {
                            chapterNumber: 1,
                            verses: [
                                {
                                    from: 1,
                                    to: 12,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: '2 Тим',
                    chapters: [
                        {
                            chapterNumber: 2,
                            verses: [
                                {
                                    from: 1,
                                    to: 10,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Лк. 21:8-9, 25-27, 33-36; За упокой: Ин. 5:24-30', () => {
            const input = 'Лк. 21:8-9, 25-27, 33-36; За упокой: Ин. 5:24-30';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Лк',
                    chapters: [
                        {
                            chapterNumber: 21,
                            verses: [
                                {
                                    from: 8,
                                    to: 9,
                                },
                                {
                                    from: 25,
                                    to: 27,
                                },
                                {
                                    from: 33,
                                    to: 36,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: 'Ин',
                    chapters: [
                        {
                            chapterNumber: 5,
                            verses: [
                                {
                                    from: 24,
                                    to: 30,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Ин. 21:15-25;, за упокой: Ин. 6:35-39', () => {
            const input = 'Ин. 21:15-25;, за упокой: Ин. 6:35-39';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Ин',
                    chapters: [
                        {
                            chapterNumber: 21,
                            verses: [
                                {
                                    from: 15,
                                    to: 25,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: 'Ин',
                    chapters: [
                        {
                            chapterNumber: 6,
                            verses: [
                                {
                                    from: 35,
                                    to: 39,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Мф. 26:1-20; Ин. 13:3-17; Мф. 26:21-39; Лк. 22:43-45; Мф. 26:40-27:2', () => {
            const input = 'Мф. 26:1-20; Ин. 13:3-17; Мф. 26:21-39; Лк. 22:43-45; Мф. 26:40-27:2';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Мф',
                    chapters: [
                        {
                            chapterNumber: 26,
                            verses: [
                                {
                                    from: 1,
                                    to: 20,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: 'Ин',
                    chapters: [
                        {
                            chapterNumber: 13,
                            verses: [
                                {
                                    from: 3,
                                    to: 17,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: 'Мф',
                    chapters: [
                        {
                            chapterNumber: 26,
                            verses: [
                                {
                                    from: 21,
                                    to: 39,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: 'Лк',
                    chapters: [
                        {
                            chapterNumber: 22,
                            verses: [
                                {
                                    from: 43,
                                    to: 45,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: 'Мф',
                    chapters: [
                        {
                            chapterNumber: 26,
                            verses: [
                                {
                                    from: 40,
                                },
                            ],
                        },
                        {
                            chapterNumber: 27,
                            verses: [
                                {
                                    to: 2,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('2 Кор. 4:6-15; или Прпп.: Гал. 5:22-6:2', () => {
            const input = '2 Кор. 4:6-15; или Прпп.: Гал. 5:22-6:2';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: '2 Кор',
                    chapters: [
                        {
                            chapterNumber: 4,
                            verses: [
                                {
                                    from: 6,
                                    to: 15,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: 'Гал',
                    chapters: [
                        {
                            chapterNumber: 5,
                            verses: [
                                {
                                    from: 22,
                                },
                            ],
                        },
                        {
                            chapterNumber: 6,
                            verses: [
                                {
                                    to: 2,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Деян. 11:19-26, 29-30; или Богородице: Флп. 2:5-11', () => {
            const input = 'Деян. 11:19-26, 29-30; или Богородице: Флп. 2:5-11';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Деян',
                    chapters: [
                        {
                            chapterNumber: 11,
                            verses: [
                                {
                                    from: 19,
                                    to: 26,
                                },
                                {
                                    from: 29,
                                    to: 30,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: 'Флп',
                    chapters: [
                        {
                            chapterNumber: 2,
                            verses: [
                                {
                                    from: 5,
                                    to: 11,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Гал. 5:22-6:2; (совпадает с чтением прп. Амвросия)', () => {
            const input = 'Гал. 5:22-6:2; (совпадает с чтением прп. Амвросия)';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Гал',
                    chapters: [
                        {
                            chapterNumber: 5,
                            verses: [
                                {
                                    from: 22,
                                },
                            ],
                        },
                        {
                            chapterNumber: 6,
                            verses: [
                                {
                                    to: 2,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });

        it('Сб. пред Рожд.: Гал. 3:8-12; ряд.: Рим. 1:10-15', () => {
            const input = 'Сб. пред Рожд.: Гал. 3:8-12; ряд.: Рим. 1:10-15';
            const result = new VerseLinkParser().parseString(input);
            const expected: ParsedShortLink[] = [
                {
                    bookShortTitle: 'Гал',
                    chapters: [
                        {
                            chapterNumber: 3,
                            verses: [
                                {
                                    from: 8,
                                    to: 12,
                                },
                            ],
                        },
                    ],
                },
                {
                    bookShortTitle: 'Рим',
                    chapters: [
                        {
                            chapterNumber: 1,
                            verses: [
                                {
                                    from: 10,
                                    to: 15,
                                },
                            ],
                        },
                    ],
                },
            ];

            expect<ParsedShortLink[]>(result).toEqual(expected);
        });
    });
});
