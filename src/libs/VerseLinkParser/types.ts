export interface ValueOrRange<T> {
    equal?: T;
    from?: T;
    to?: T;
}

export interface ParsedChapter {
    // Номер главы (Verse.chapter)
    chapterNumber: number;

    // Стих в главе (Verse.number)
    // если массив пуст - надо возвращать все стихи по главе
    verses: ValueOrRange<number>[];
}

export interface ParsedShortLink {
    // Короткое название книги (Book.shortTitle)
    bookShortTitle: string;

    // Глава в книге
    // если массив пуст - надо возвращать по всем главам
    chapters: ParsedChapter[];
}
