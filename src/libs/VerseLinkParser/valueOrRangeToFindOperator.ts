import { Between, FindOperator, LessThanOrEqual, MoreThanOrEqual } from 'typeorm';
import { ValueOrRange } from './types';

export const valueOrRangeToFindOperator = <T>({
    equal,
    to,
    from,
}: ValueOrRange<T>): T | FindOperator<T> => {
    if (equal) {
        return equal;
    }

    if (to !== undefined && from !== undefined) {
        return Between(from, to);
    }

    if (from !== undefined && to === undefined) {
        return MoreThanOrEqual(from);
    }

    if (from === undefined && to !== undefined) {
        return LessThanOrEqual(to);
    }
};
