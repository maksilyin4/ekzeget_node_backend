import { sortASC } from '../helpers/sort';
import { bookShortTitles, VerseLinkParserDelimiters as Delimiters } from './enums';
import { ParsedChapter, ParsedShortLink, ValueOrRange } from './types';

export class VerseLinkParser {
    public parseString(verseLink: string): ParsedShortLink[] {
        const cleanLink = verseLink.replace(/[–—]/gi, Delimiters.Range).trim();

        const splitByBook = cleanLink.split(Delimiters.BookFromBook);
        return splitByBook
            .map((code: string) => this.parseSingleBookChapterVerse(code.trim()))
            .filter(Boolean);
    }

    private forwardParser(code: string): { bookShortTitle: string; chapterAndVerses: string } {
        const bookShortTitle: string = bookShortTitles.find(
            (title: string): boolean => code.indexOf(`${title}.`) !== -1
        );
        if (!bookShortTitle) {
            return { bookShortTitle: null, chapterAndVerses: null };
        }

        const getChapterAndVerses = (body: string, subStr: string): string =>
            body.slice(body.indexOf(subStr) + subStr.length + 1).trim();

        return {
            bookShortTitle,
            chapterAndVerses: getChapterAndVerses(code, bookShortTitle),
        };
    }

    private parseSingleBookChapterVerse(code: string): ParsedShortLink {
        const { bookShortTitle, chapterAndVerses } = this.forwardParser(code);
        if (!bookShortTitle || !chapterAndVerses) {
            return null;
        }

        const isOnlyChapter = !chapterAndVerses.includes(':');

        return {
            bookShortTitle,
            chapters: isOnlyChapter
                ? [{ chapterNumber: +chapterAndVerses, verses: [] }]
                : this.parseManyChaptersAndVerses(chapterAndVerses),
        };
    }

    /**
     * Парсит самую хаотичную часть строки
     * @example "2:1-3" => ["2:1-3"]
     * @example "1:1–10, 20-2:5" => ["1:1–10,20", "2:5"]
     * @example "10:35–11:7" => ["10:35", "11:7"]
     * @example "123:35, 15-15-11:701" => ["123:35,15-15", "11:701"]
     */
    private splitByChapter(str: string): string[] {
        return str.replace(/ /g, '').match(/\d+:[-,\d]+(?![\d:])/gi);
    }

    private parseManyChaptersAndVerses(chaptersAndVerses: string): ParsedChapter[] {
        const cavArray = this.splitByChapter(chaptersAndVerses);

        return cavArray.map((chapterAndVerses: string, index, { length }) =>
            this.parseSingleChapterAndVerses({
                chapterAndVerses,
                index,
                length,
            })
        );
    }

    private parseSingleChapterAndVerses({
        chapterAndVerses,
        index,
        length,
    }: {
        chapterAndVerses: string;
        index: number;
        length: number;
    }): ParsedChapter {
        const [chapterNumber, verses] = chapterAndVerses.split(Delimiters.ChapterFromVerse);
        const versesArray = verses.split(Delimiters.VerseFromVerse);

        return {
            chapterNumber: +chapterNumber,
            verses: versesArray.map(v => this.parseAnyVerses({ verses: v, index, length })),
        };
    }

    private parseAnyVerses({
        verses,
        index,
        length,
    }: {
        verses: string;
        index: number;
        length: number;
    }): ValueOrRange<number> {
        if (verses.includes(Delimiters.Range)) {
            const split = verses
                .split(Delimiters.Range)
                .map(s => +s.trim())
                .sort(sortASC);
            return {
                from: split[0],
                to: split[1],
            };
        }
        if (length > 1) {
            return index == 0 ? { from: +verses } : { to: +verses };
        }
        return {
            equal: +verses,
        };
    }
}
