import slugify from 'slugify';

export function generateSlug(code: string): string {
    return slugify(code);
}
