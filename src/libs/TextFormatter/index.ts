import Book from '../../entities/Book';
import CacheManager from '../../managers/CacheManager';
import { CacheManagerGlobalKeys } from '../../managers/CacheManager/enum';

type GenOpenTag = (href: string) => string;

export default class TextFormatter {
    constructor(private _text: string) {}

    public async format() {
        await this.initBookCache();
        this.wrapBold();
        this.wrapItalic();
        this.wrapH4();
        this.wrapNotes();
        this.wrapInterpretation();
        await this.wrapLinks();
        this.wrapGreek();
        this.wrapSupIndex();
        this.wrapList();
        this.wrapImportantClass();
        await this.wrapDogsAsLinks();
        return this._text;
    }

    private wrapBold() {
        const reg = /\|\|\|(.*?)\|\|\|/;
        const openTag = '<b>';
        const closedTag = '</b>';

        this.wrapInTagByRegexp(reg, { openTag, closedTag });
    }

    private wrapItalic() {
        const reg = /\/\/\/(.*?)\/\/\//;
        const openTag = '<i>';
        const closedTag = '</i>';

        this.wrapInTagByRegexp(reg, { openTag, closedTag });
    }

    private wrapH4() {
        const reg = /___(.*?)___/;
        const openTag = '<h4>';
        const closedTag = '</h4>';

        this.wrapInTagByRegexp(reg, { openTag, closedTag });
    }

    private async wrapLinks() {
        const regexp = /{{{(.*?)}}}/;
        const openTag: GenOpenTag = (href: string) =>
            `<a class="verse-link link_text" href="${href}">`;
        const closedTag = '</a>';

        await this.wrapLinksGenerated({ regexp, openTag, closedTag });
    }

    private wrapGreek() {
        const reg = /\[\[\[(.*?)]]]/;
        const openTag = '<span class="greek">';
        const closedTag = '</span>';

        this.wrapInTagByRegexp(reg, { openTag, closedTag });
    }

    private wrapNotes() {
        const reg = /===(.*?)==/;
        const openTag = `<br><div class="interpretation-note"><p class="interpretation-note-title">Примечания</p><span><ul>`;
        const closedTag = '</ul></span></div>';

        this.wrapInTagByRegexp(reg, { openTag, closedTag });
    }

    private wrapInterpretation() {
        const reg = /\+\+\+(.*?)\+\+/;
        const openTag = `<br><div class="interpretation-root"><p class="interpretation-root-title">Источник</p><span>`;
        const closedTag = '</span></div>';

        this.wrapInTagByRegexp(reg, { openTag, closedTag });
    }

    private wrapSupIndex() {
        const reg = /@(\d+)/;
        const openTag = '<sup>';
        const closedTag = '</sup>';

        this.wrapInTagByRegexp(reg, { openTag, closedTag });
    }

    private wrapImportantClass() {
        const reg = /\[(.*?)]/;
        const openTag = '<span class="is-important">';
        const closedTag = '</span>';

        this.wrapInTagByRegexp(reg, { openTag, closedTag });
    }

    private async wrapDogsAsLinks() {
        const regexp = /@(.*?)@/;
        const openTag: GenOpenTag = search =>
            `<a class="verse-link link_text" href="/search/bible/?search=${search}">`;
        const closedTag = '</a>';

        await this.wrapDogsGenerated({ regexp, openTag, closedTag });
    }

    private wrapList() {
        const reg = /^\*(.*?)\r/;
        const openTag = `<li>`;
        const closedTag = '</li>';

        this.wrapInTagByRegexp(reg, { openTag, closedTag });
    }

    private wrapInTagByRegexp(
        regexp,
        { openTag, closedTag }: { openTag: string; closedTag: string }
    ) {
        const matchedSubstrings = this._text.match(new RegExp(regexp, 'gsm'));
        if (!matchedSubstrings) return;

        const proceedSubstrings = matchedSubstrings.map(group => {
            const a = group.match(new RegExp(regexp, 's'));
            return openTag + a[1] + closedTag;
        });

        matchedSubstrings.forEach((substr, ind) => {
            this._text = this._text.replace(substr, proceedSubstrings[ind]);
        });
    }

    private async parseLinkText(text: string): Promise<string> {
        const protectedText = await this.pointMissingProtection(text);

        const [bookShortTitle, chapterAndVerse] = protectedText.split('.').map(s => s.trim());
        if (!bookShortTitle || !chapterAndVerse) {
            return `/`;
        }

        const books: Book[] = await CacheManager.get(CacheManagerGlobalKeys.books);
        const book: Book = books.find((b: Book) => b.short_title === bookShortTitle);
        if (!book) {
            return `/`;
        }

        const [chapter, verse] = chapterAndVerse.split(':').map(s => s.trim());

        return verse
            ? `/bible/${book.code}/glava-${chapter}/?verse=${chapterAndVerse}`
            : `/bible/${book.code}/glava-${chapter}/`;
    }

    private async pointMissingProtection(text: string) {
        if (text.indexOf('.') === -1) {
            for (let i = 1; i < text.length - 1; i++) {
                if (text[i].toLowerCase().match(/[а-я]/i) && text[i + 1] === ' ') {
                    return text.slice(0, i + 1) + '.' + text.slice(i + 1);
                }
            }
        }
        return text;
    }

    private async wrapLinksGenerated({
        regexp,
        openTag,
        closedTag,
    }: {
        regexp: RegExp;
        openTag: GenOpenTag;
        closedTag: string;
    }) {
        const matchedSubstrings = this._text.match(new RegExp(regexp, 'gsm'));
        if (!matchedSubstrings) return;

        const proceedSubstrings = await Promise.all(
            matchedSubstrings.map(async group => {
                const a = group.match(new RegExp(regexp, 's'));
                const href = await this.parseLinkText(a[1]);
                return openTag(href) + a[1] + closedTag;
            })
        );

        matchedSubstrings.forEach((substr, ind) => {
            this._text = this._text.replace(substr, proceedSubstrings[ind]);
        });
    }

    private async wrapDogsGenerated({
        regexp,
        openTag,
        closedTag,
    }: {
        regexp: RegExp;
        openTag: GenOpenTag;
        closedTag: string;
    }) {
        const matchedSubstrings = this._text.match(new RegExp(regexp, 'gsm'));
        if (!matchedSubstrings) return;

        const proceedSubstrings = await Promise.all(
            matchedSubstrings.map(async group => {
                const search = group.match(new RegExp(regexp, 's'));
                return openTag(search[1]) + search[1] + closedTag;
            })
        );

        matchedSubstrings.forEach((substr, ind) => {
            this._text = this._text.replace(substr, proceedSubstrings[ind]);
        });
    }

    private initBookCache = async (): Promise<void> => {
        if (await CacheManager.get(CacheManagerGlobalKeys.books)) return;

        const booksFromDB: Book[] = await Book.find();
        await CacheManager.set(CacheManagerGlobalKeys.books, booksFromDB);
    };
}
