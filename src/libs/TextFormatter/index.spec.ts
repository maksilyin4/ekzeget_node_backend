import TextFormatter from './index';
import CacheManager from '../../managers/CacheManager';

describe('Text formatter', () => {
    afterAll(async () => {
        jest.restoreAllMocks();
    });

    const cacheManagerSpy = jest
        .spyOn(CacheManager, 'get')
        .mockResolvedValue([[{ id: 1 }], [{ id: 2 }]]);

    const texts = [
        [
            'normal data Деян. 4:32',
            'дна душа/// ({{{Деян. 4:32}}}); и в том',
            "дна душа/// (<a class=\"verse-link link_text\" href=\"/\">Деян. 4:32</a>); и в том",
        ],
        [
            'normal data Пс. 126:1',
            ' преисподнюю» ///(}}, LXX). Не говорят раж///» ({{{Пс. 126:1}}}).',
            " преисподнюю» <i>(}}, LXX). Не говорят раж</i>» (<a class=\"verse-link link_text\" href=\"/\">Пс. 126:1</a>).",
        ],
        [
            'double data',
            ' с небесным качеством (См. {{{Иез. 37:1-14}}}; {{{1 Кор. 15:12-55}}}).',
            " с небесным качеством (См. <a class=\"verse-link link_text\" href=\"/\">Иез. 37:1-14</a>; <a class=\"verse-link link_text\" href=\"/\">1 Кор. 15:12-55</a>).",
        ],
        [
            'missing data',
            'ако эта ступень не достигается в этой жизни. Это относится к надежде, с кото',
            'ако эта ступень не достигается в этой жизни. Это относится к надежде, с кото',
        ],
        [
            'missing dot',
            'небесным качеством (См. {{{Иез 37:1-14}}}; ',
            "небесным качеством (См. <a class=\"verse-link link_text\" href=\"/\">Иез 37:1-14</a>; "
        ],
        [
            '@-decorators',
            'какой-то текст и ссылка на поиск по слову @Матфей@ ',
            `какой-то текст и ссылка на поиск по слову <a class="verse-link link_text" href="/search/bible/?search=Матфей">Матфей</a> `
        ],
    ];

    test.each(texts)('With %s', async (name, text: string, result: string) => {
        const formattedText = await new TextFormatter(text).format();
        expect(formattedText).toBe(result);
    });
});
