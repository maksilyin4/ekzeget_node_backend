const libxmljs = require('libxmljs2');

type FB2Tag = 'DESCRIPTION' | 'EPIGRAPH' | 'SECTION' | 'TITLE' | 'BINARY';
type FB2DescriptionSubTags = 'TITLE-INFO' | 'DOCUMENT-INFO' | 'PUBLISH-INFO';
type FB2TitleInfoSubTags =
    | 'GENRE'
    | 'AUTHOR'
    | 'BOOK-TITLE'
    | 'ANNOTATION'
    | 'LANG'
    | 'SRC-LANG'
    | 'SEQUENCE';
type FB2AuthorSubTags = 'FIRST-NAME' | 'MIDDLE-NAME' | 'LAST-NAME';

enum FB2ChapterNames {
    epigraph = 'Эпиграф',
    chapter = 'Глава',
    annotation = 'Аннотация',
    note = 'Примечания и сноски',
}

type FB2MetaProperties = {
    title: string;
    language: string;
    authors: string[];
    genres: string[];
};

export type FB2Chapter = {
    title: string;
    html: string;
};

type FB2Note = {
    id: string;
    title: string;
    content: string;
};

export class FB2Parser {
    private readonly xmlDoc;
    private readonly xmlns: string;
    private readonly metaProperties: FB2MetaProperties = {
        title: null,
        language: null,
        authors: [],
        genres: [],
    };

    private readonly chapters: FB2Chapter[] = [];
    private readonly notes: FB2Note[] = [];

    constructor(content: string) {
        if (!content) {
            throw new Error('FB2Parser: Empty content');
        }
        this.xmlDoc = libxmljs.parseXml(content);
        this.xmlns = this.xmlDoc
            .root()
            .namespace()
            .href();
    }

    public getChapters(): FB2Chapter[] {
        return this.chapters;
    }

    public getMeta(): FB2MetaProperties {
        return this.metaProperties;
    }

    public parse() {
        for (const element of this.extractFictionBookElements()) {
            this.extractChapter(element);
        }
        this.joinNotes();
        return this;
    }

    private extractFictionBookElements(): HTMLCollection {
        try {
            const children = this.xmlDoc.childNodes();
            for (let i = 0; i < children.length; i++) {
                if (children[i].name() === 'body' && children[i].attrs().length === 0) {
                    return children[i].childNodes();
                }
            }
            throw new Error('no FB2 body found');
        } catch (e) {
            throw new Error('FB2Parser: Unable to parse content ' + e.message);
        }
    }

    private extractChapter(element): void {
        if (!element.text().trim().length) {
            return;
        }
        switch (element.name().toUpperCase() as FB2Tag) {
            case 'DESCRIPTION':
                this.processDescription(element);
                break;
            case 'EPIGRAPH':
                this.processEpigraph(element);
                break;
            case 'SECTION':
                this.processSection(element);
                break;
            case 'BINARY':
            case 'TITLE':
                break;
            default:
                break;
        }
    }

    private processDescription(descriptionNode): void {
        for (const child of descriptionNode.childNodes()) {
            switch (child.name().toUpperCase() as FB2DescriptionSubTags) {
                case 'TITLE-INFO':
                    this.processTitleInfo(child);
                    break;
                case 'DOCUMENT-INFO':
                case 'PUBLISH-INFO':
                    break;
                default:
                    break;
            }
        }
    }

    private processTitleInfo(titleInfoNode): void {
        for (const child of titleInfoNode.childNodes()) {
            switch (child.name().toUpperCase() as FB2TitleInfoSubTags) {
                case 'ANNOTATION':
                    this.chapters.push({
                        title: FB2ChapterNames.annotation,
                        html: child.toString(),
                    });
                    break;
                case 'BOOK-TITLE':
                    this.metaProperties.title = child.text();
                    break;
                case 'GENRE':
                    this.metaProperties.genres.push(child.text());
                    break;
                case 'AUTHOR':
                    this.processAuthor(child);
                    break;
                case 'LANG':
                    this.metaProperties.language = child.text();
                    break;
                default:
                    break;
            }
        }
    }

    private processAuthor(authorNode): void {
        let firstName: string, middleName: string, lastName: string;
        for (const authorProperty of authorNode.childNodes()) {
            switch (authorProperty.name() as FB2AuthorSubTags) {
                case 'FIRST-NAME':
                    firstName = authorProperty.text();
                    break;
                case 'MIDDLE-NAME':
                    middleName = authorProperty.text();
                    break;
                case 'LAST-NAME':
                    lastName = authorProperty.text();
                    break;
                default:
                    break;
            }
        }

        const author = [firstName, middleName, lastName].join(' ').trim();
        if (author) {
            this.metaProperties.authors.push(author);
        }
    }

    private processEpigraph(epigraphNode): void {
        const currentEpigraphCount = this.chapters.filter(({ title }) =>
            title.startsWith(FB2ChapterNames.epigraph)
        ).length;

        this.chapters.push({
            title: !currentEpigraphCount
                ? FB2ChapterNames.epigraph
                : `${FB2ChapterNames.epigraph} ${currentEpigraphCount + 1}`,
            html: epigraphNode.toString(),
        });
    }

    private processSection(element): void {
        if (element.attr('id')) {
            // Заметки и примечания
            this.processNote(element);
        } else {
            // Нормальные главы
            this.processChapter(element);
        }
    }

    private processNote(sectionNode): void {
        const { title, titleElement } = FB2Parser.extractTitle(sectionNode);
        if (!title) {
            return;
        }
        titleElement?.remove();
        this.notes.push({
            id: sectionNode.attr('id').value(),
            title,
            content: sectionNode
                .childNodes()
                .join('')
                .trim(),
        });
    }

    private processChapter(sectionNode): void {
        const { title, titleElement } = FB2Parser.extractTitle(sectionNode);
        if (!title) {
            return;
        }
        titleElement?.remove();

        sectionNode
            .find('//xmlns:title', this.xmlns)
            .forEach(sectionHeader =>
                sectionHeader.replace(new libxmljs.Element(this.xmlDoc, 'h2', sectionHeader.text()))
            );

        sectionNode
            .find('//xmlns:subtitle', this.xmlns)
            .forEach(sectionHeader =>
                sectionHeader.replace(new libxmljs.Element(this.xmlDoc, 'h3', sectionHeader.text()))
            );

        this.chapters.push({
            title,
            html: sectionNode
                .childNodes()
                .join('')
                .trim(),
        });
    }

    private static extractTitle(sectionNode): { title: string; titleElement } {
        const titleElement = sectionNode
            .childNodes()
            .find(child => child.name().toUpperCase() === 'TITLE');
        let title = titleElement?.text().trim();
        if (!title) {
            title = sectionNode
                .get('.//*[1]')
                .text()
                .trim();
            if (title.length === 0 && sectionNode.text().trim().length > 0) {
                throw new Error('Malformed fb2 book');
            }
        }

        return { title, titleElement };
    }

    private joinNotes() {
        const chapterContentArray = [];
        this.notes.forEach(({ id, title, content }) => {
            chapterContentArray.push(`<p id="${id}">${title} - ${content}</p>`);
        });

        this.chapters.push({
            title: FB2ChapterNames.note,
            html: chapterContentArray.join(''),
        });
    }
}
