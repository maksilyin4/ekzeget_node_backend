import captcha from 'captchapng';

export default class Captcha {
    public captchaNumber;
    public captchaImageBase64;

    constructor() {
        this.captchaNumber = this.getRandomNumberToMax(9999);

        const firstColor = this.getRandomNumberToMax(255);
        const secondColor = this.getRandomNumberToMax(255);
        const thirdColor = this.getRandomNumberToMax(255);

        const p = new captcha(120, 50, this.captchaNumber); // width,height,numeric captcha

        p.color(firstColor, secondColor, thirdColor, 30); // First color: background (red, green, blue, alpha)
        p.color(thirdColor, secondColor, firstColor, 255); // Second color: paint (red, green, blue, alpha)

        this.captchaImageBase64 = p.getBase64();
    }

    private getRandomNumberToMax = max => Math.floor(Math.random() * Math.floor(max));
}
