import { Request, Response } from 'express';
import Errors from '../../../src/const/strings/auth';

export class ServerError extends Error {
    constructor(message?: string, public statusCode?: number) {
        super(message);
    }
}

class ErrorHandler {
    public handleFailedRequest(request: Request, response: Response, err: Error | ServerError) {
        if (err instanceof ServerError) {
            response.statusCode = err.statusCode;
            console.error(`Request failed [${err.statusCode}] ${request.method}`);
            console.error(request.url);
        } else {
            response.statusCode = 500;
            console.error(`Request failed [500] ${request.method}`);
            console.error(request.url);
        }

        console.error(err);
    }
}

export default new ErrorHandler();
