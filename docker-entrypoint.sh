#!/bin/sh

echo "Generating sitemap..."
npm run generate-map:prod
echo "Sitemap generated."

echo "Starting application..."
npm run start:prod
